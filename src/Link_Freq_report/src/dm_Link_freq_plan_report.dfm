object dmLink_freq_plan_report: TdmLink_freq_plan_report
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Left = 784
  Top = 463
  Height = 425
  Width = 681
  object ds_LInks: TDataSource
    DataSet = qry_LInks
    Left = 136
    Top = 72
  end
  object frxReport: TfrxReport
    Version = '6.2.11'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.ShowCaptions = True
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 41121.613084722200000000
    ReportOptions.LastChange = 41940.802632372700000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      '    '
      '    '
      ''
      ''
      '//------------------------------------------------------------'
      'function DegreeToStr (aValue: double): String;'
      '//------------------------------------------------------------'
      'const'
      '//  STR_BLPOINT_FORMAT1 = '#39'%d'#176'%.2d'#39#39'%s%1.2f'#39#39#39#39#39';'
      '  STR_BLPOINT_FORMAT1 = '#39'%d'#176' %.2d'#39#39' %1.2f'#39#39#39#39#39';'
      'var    '
      ' iDeg,iMin:integer; '
      ' eSec: Double;   '
      '           '
      'begin'
      '  geo_DecodeDegree_(aValue, iDeg,iMin,eSec);'
      '                           '
      ' // angle.Sec:=TruncFloat(angle.Sec,2);'
      ' '
      '    Result:=Format(STR_BLPOINT_FORMAT1, [iDeg, iMin, eSec]);'
      ''
      '  if aValue<0 then'
      '    Result:='#39'-'#39'+Result;'
      'end;'
      '  '
      ''
      ' //------------------------------------------------------------'
      
        'procedure geo_DecodeDegree_(aValue: Double; var aDeg,aMin:intege' +
        'r; var aSec:'
      '    Double);'
      '//------------------------------------------------------------'
      'var'
      '  bIsNegative: Boolean;'
      '  e: Double;'
      '  eSec: Double;'
      ''
      'begin'
      '  bIsNegative := aValue<0;'
      ''
      '  aValue :=Abs(aValue);'
      ''
      ''
      ''
      '   //    aValue:=99;                            '
      '          '
      ''
      '          '
      '  aDeg:=Trunc(aValue);'
      ''
      '  aMin:=Trunc(Frac(aValue)*60);'
      ''
      ''
      '//  e:= (aValue - Trunc(aValue));'
      '  e:= (aValue - Trunc(aValue)) * 60;'
      ''
      ''
      '  aMin:=Trunc(e);'
      '  aSec:=(((aValue - aDeg) *60) - aMin) * 60;'
      ''
      '  while aSec>(60-0.1) do'
      '  begin'
      '    aMin:=aMin+1;'
      '    aSec:=aSec-60;'
      '  end;    '
      ''
      '  while aMin>=60 do'
      '  begin'
      '      aMin:=aMin-60;'
      '      aDeg:=aDeg+1;'
      '  end;  '
      ''
      '  if aSec<0 then'
      '     aSec:=0;'
      ''
      '  if bIsNegative then'
      '    aDeg:=-aDeg;'
      ''
      ''
      ''
      'end;'
      '  '
      ''
      '                                 '
      ''
      '//------------------------------------------------------------'
      
        'procedure geo_DecodeDegree_1111(aValue: Double; var aDeg,aMin:in' +
        'teger; var aSec:'
      '    Double);'
      '//------------------------------------------------------------'
      'const'
      ' EXP_ = 0.000000001;'
      '     //97,033333333'
      'var'
      '  bIsNegative: Boolean;'
      '  e: Double;'
      '  eMinutes: Double;'
      '  eSec: Double;'
      '  eSeconds: Double;'
      'begin'
      '  bIsNegative := aValue<0;'
      ''
      '  aValue :=Abs(aValue);'
      ''
      '  aDeg:=Trunc(aValue);'
      ''
      ' // e:=Abs(aValue)*60;'
      ''
      '//  , Frac(X) = X - Int(X).'
      ''
      '  aMin:=Trunc(Frac(aValue)*60);'
      ''
      '//  Result.Sec:=Frac(Frac(Abs(aValue))*60)*60;'
      '  e:= (aValue - Trunc(aValue)) * 60;'
      '  aMin:=Trunc(e);'
      ''
      ' eMinutes := Trunc (e);'
      ' eSeconds := (e - Trunc (e)) * 60 ;'
      ''
      ''
      '  aSec:=(((aValue - aDeg) *60) - aMin) * 60;'
      ''
      '  if (aSec - 60) > EXP_ then'
      '  begin'
      '    Inc(aMin);'
      '    aSec := 0;'
      '  end;'
      ''
      ''
      '  if bIsNegative then'
      '    aDeg:=-aDeg;'
      ''
      '      '
      ''
      ''
      'end;'
      '        '
      ''
      '       '
      'begin'
      ''
      'end.')
    Left = 296
    Top = 24
    Datasets = <
      item
        DataSet = frxDBDataset_link
        DataSetName = 'link'
      end>
    Variables = <>
    Style = <
      item
        Name = 'Title'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Height = -16
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        Frame.Typ = []
        Fill.BackColor = clGray
      end
      item
        Name = 'Header'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clMaroon
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        Frame.Typ = []
      end
      item
        Name = 'Group header'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clMaroon
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        Frame.Typ = []
        Fill.BackColor = 16053492
      end
      item
        Name = 'Data'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        Frame.Typ = []
      end
      item
        Name = 'Group footer'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        Frame.Typ = []
      end
      item
        Name = 'Header line'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        Frame.Typ = [ftBottom]
        Frame.Width = 2.000000000000000000
      end>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      Orientation = poLandscape
      PaperWidth = 600.000000000000000000
      PaperHeight = 210.000000000000000000
      PaperSize = 256
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      PrintIfEmpty = False
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        Frame.Typ = []
        Height = 45.354360000000000000
        Top = 16.000000000000000000
        Width = 2192.127400000000000000
        StartNewPage = True
        object Memo8: TfrxMemoView
          Align = baWidth
          AllowVectorExport = True
          Width = 2192.127400000000000000
          Height = 37.795300000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8 = (
            
              #1056#1119#1057#1026#1056#1109#1056#181#1056#1108#1057#8218' '#1057#8225#1056#176#1057#1027#1057#8218#1056#1109#1057#8218#1056#1029#1056#1109'-'#1057#8218#1056#181#1057#1026#1057#1026#1056#1105#1057#8218#1056#1109#1057#1026#1056#1105#1056#176#1056#187#1057#1034#1056#1029#1056#1109#1056#1110#1056#1109' '#1056 +
              #1111#1056#187#1056#176#1056#1029#1056#176'  '#1056#1029#1056#176' '#1057#8218#1056#181#1057#1026#1057#1026#1056#1105#1057#8218#1056#1109#1057#1026#1056#1105#1056#1105)
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Frame.Typ = []
        Height = 120.944960000000000000
        Top = 216.000000000000000000
        Width = 2192.127400000000000000
        DataSet = frxDBDataset_link
        DataSetName = 'link'
        RowCount = 0
        object property1name: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 75.590600000000000000
          Width = 162.519790000000000000
          Height = 60.472440940000000000
          DataSetName = 'property1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[link."LinkEnd1_Property_name"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object property2name: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 75.590600000000000000
          Top = 60.472480000000000000
          Width = 162.519790000000000000
          Height = 60.472440940000000000
          DataSetName = 'property2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[link."LinkEnd2_Property_name"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line: TfrxMemoView
          AllowVectorExport = True
          Width = 30.236240000000000000
          Height = 120.944923390000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Line]')
          ParentFont = False
          VAlign = vaCenter
        end
        object property1address: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 238.110390000000000000
          Width = 226.771653540000000000
          Height = 60.472440940000000000
          DataSetName = 'property1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[link."LinkEnd1_Property_address"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object property2address: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 238.110390000000000000
          Top = 60.472480000000000000
          Width = 226.771653540000000000
          Height = 60.472440940000000000
          DataSetName = 'property2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[link."LinkEnd2_Property_address"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Linkend1LinkEndTypename: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 691.653697080000000000
          Width = 132.283464570000000000
          Height = 60.472440940000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd1
          DataSetName = 'LinkEnd1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[link."Linkend1_LinkEndType_name"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo17: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 691.653697080000000000
          Top = 60.472440940000000000
          Width = 132.344510160000000000
          Height = 60.472440940000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[link."Linkend2_LinkEndType_name"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Linkend1antenna_height: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1065.826969370000000000
          Width = 75.590551180000000000
          Height = 60.472440940000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd1
          DataSetName = 'LinkEnd1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[link."Linkend1_antenna_height"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo22: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1065.888014960000000000
          Top = 60.472480000000000000
          Width = 75.590551180000000000
          Height = 60.472440940000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd1
          DataSetName = 'LinkEnd1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[link."Linkend2_antenna_height"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Linkend2rx_freq_MHz: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 823.998207240000000000
          Top = 60.472480000000000000
          Width = 75.590551180000000000
          Height = 60.472440940000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[link."Linkend2_rx_freq_MHz"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo23: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 823.937161650000000000
          Width = 75.590551180000000000
          Height = 60.472440940000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[link."Linkend1_rx_freq_MHz"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Linkend2antenna_polarization: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1487.683336930000000000
          Width = 53.291338580000000000
          Height = 60.472440940000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[link."Linkend1_antenna_polarization"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo18: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1487.683336930000000000
          Top = 60.472480000000000000
          Width = 53.291338580000000000
          Height = 60.472440940000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[link."Linkend2_antenna_polarization"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Linkend2antenna_diameter: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1434.391998350000000000
          Width = 53.291338580000000000
          Height = 60.472440940000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[link."Linkend1_antenna_diameter"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo24: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1434.391998350000000000
          Top = 60.472480000000000000
          Width = 53.291338580000000000
          Height = 60.472440940000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[link."Linkend2_antenna_diameter"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo25: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 899.527712830000000000
          Width = 49.133858270000000000
          Height = 60.472440940000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[link."Linkend1_antenna_horz_width"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo26: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 899.588758420000000000
          Top = 60.472480000000000000
          Width = 49.133858270000000000
          Height = 60.472440940000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[link."Linkend2_antenna_horz_width"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo27: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 948.661571100000000000
          Width = 49.133858270000000000
          Height = 60.472440940000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[link."Linkend1_antenna_vert_width"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo28: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 948.722616690000000000
          Top = 60.472480000000000000
          Width = 49.133858270000000000
          Height = 60.472440940000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[link."Linkend2_antenna_vert_width"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo29: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 997.795429370000000000
          Width = 68.031540000000000000
          Height = 60.472440940000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd1
          DataSetName = 'LinkEnd1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[link."Linkend1_antenna_gain"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo30: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 997.856474960000000000
          Top = 60.472480000000000000
          Width = 68.031540000000000000
          Height = 60.472440940000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd1
          DataSetName = 'LinkEnd1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[link."Linkend2_antenna_gain"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object linklength_km: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1194.391951970000000000
          Width = 52.913385830000000000
          Height = 120.944960000000000000
          DataField = 'length_km'
          DataSet = frxDBDataset_link
          DataSetName = 'link'
          DisplayFormat.FormatStr = '%2.1f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[link."length_km"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Linkend1power_dBm: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1247.305337800000000000
          Width = 52.913385830000000000
          Height = 120.944960000000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd1
          DataSetName = 'LinkEnd1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[link."Linkend1_power_dBm"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Linkend2radiation_class: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1300.218723630000000000
          Top = 60.472480000000000000
          Width = 71.811070000000000000
          Height = 60.472480000000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[link."Linkend2_radiation_class"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo31: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1300.218723630000000000
          Width = 71.811070000000000000
          Height = 60.472480000000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[link."Linkend1_radiation_class"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Linkend2bitrate_Mbps: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1372.029793630000000000
          Top = 60.472480000000000000
          Width = 62.362204720000000000
          Height = 60.472480000000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[link."Linkend2_bitrate_Mbps"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo32: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1372.029793630000000000
          Width = 62.362204720000000000
          Height = 60.472480000000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[link."Linkend1_bitrate_Mbps"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object property2lat_str: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 464.882043540000000000
          Top = 60.472480000000000000
          Width = 113.385826770000000000
          Height = 60.472480000000000000
          DataSetName = 'property2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[DegreeToStr(<link."LinkEnd2_property_lat_gck_2011">)]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo38: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 464.882043540000000000
          Width = 113.385826770000000000
          Height = 60.472480000000000000
          DataSetName = 'property2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[DegreeToStr(<link."LinkEnd1_property_lat_gck_2011">)]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo39: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 578.267870310000000000
          Top = 60.472480000000000000
          Width = 113.385826770000000000
          Height = 60.472480000000000000
          DataSetName = 'property2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[DegreeToStr(<link."LinkEnd2_property_lon_gck_2011">)]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo40: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 578.267870310000000000
          Width = 113.385826770000000000
          Height = 60.472480000000000000
          DataSetName = 'property2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[DegreeToStr(<link."LinkEnd1_property_lon_gck_2011">)]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo41: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 30.236240000000000000
          Width = 45.354360000000000000
          Height = 60.472440940000000000
          DataSetName = 'property1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[link."linkend1_property_code"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo42: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 30.236240000000000000
          Top = 60.472480000000000000
          Width = 45.354360000000000000
          Height = 60.472440940000000000
          DataSetName = 'property1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[link."linkend2_property_code"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo46: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1141.417520550000000000
          Width = 52.913385830000000000
          Height = 60.472440940000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[link."Linkend1_antenna_azimuth"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo47: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1141.478566140000000000
          Top = 60.472480000000000000
          Width = 52.913385830000000000
          Height = 60.472440940000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[link."Linkend2_antenna_azimuth"]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object Header1: TfrxHeader
        FillType = ftBrush
        Frame.Typ = []
        Height = 75.590551180000000000
        Top = 120.000000000000000000
        Width = 2192.127400000000000000
        object Memo3: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 238.110390000000000000
          Width = 226.771653540000000000
          Height = 75.590551180000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Fill.BackColor = clSilver
          HAlign = haCenter
          Memo.UTF8 = (
            #1056#1106#1056#1169#1057#1026#1056#181#1057#1027)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 948.661571100000000000
          Width = 49.133858270000000000
          Height = 75.590551180000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Fill.BackColor = clSilver
          HAlign = haCenter
          Memo.UTF8 = (
            #1056#8221#1056#1116#1056#1106
            #1056#1030#1056#181#1057#1026#1057#8218'.')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo7: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 899.527712830000000000
          Width = 49.133858270000000000
          Height = 75.590551180000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Fill.BackColor = clSilver
          HAlign = haCenter
          Memo.UTF8 = (
            #1056#8221#1056#1116#1056#1106
            #1056#1110#1056#1109#1057#1026'.')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo9: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Width = 30.236240000000000000
          Height = 75.590551181102400000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Fill.BackColor = clSilver
          HAlign = haCenter
          Memo.UTF8 = (
            #1074#8222#8211
            #1056#1111'/'#1056#1111)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo10: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 464.882043540000000000
          Width = 113.385826770000000000
          Height = 75.590551180000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Fill.BackColor = clSilver
          HAlign = haCenter
          Memo.UTF8 = (
            #1056#1025#1056#1105#1057#1026#1056#1109#1057#8218#1056#176','
            #1057#1027'.'#1057#8364'.  ('#1056#8220'CK-2011)')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo11: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 691.653697080000000000
          Width = 132.283464570000000000
          Height = 75.590551180000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Fill.BackColor = clSilver
          HAlign = haCenter
          Memo.UTF8 = (
            #1056#1118#1056#1105#1056#1111' '
            #1056#1109#1056#177#1056#1109#1057#1026#1057#1107#1056#1169#1056#1109#1056#1030#1056#176#1056#1029#1056#1105#1057#1039)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo12: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 823.937161650000000000
          Width = 75.590551180000000000
          Height = 75.590551180000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Fill.BackColor = clSilver
          Memo.UTF8 = (
            #1056#152#1056#1029#1056#1169#1056#181#1056#1108#1057#1027
            '('#1056#1029#1056#1109#1056#1112#1056#1105#1056#1029#1056#176#1056#187#1057#8249' '#1057#8225#1056#176#1057#1027#1057#8218#1056#1109#1057#8218'), '#1056#1114#1056#8220#1057#8224)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo13: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 578.267870310000000000
          Width = 113.385826770000000000
          Height = 75.590551180000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Fill.BackColor = clSilver
          HAlign = haCenter
          Memo.UTF8 = (
            #1056#8221#1056#1109#1056#187#1056#1110#1056#1109#1057#8218#1056#176','
            #1056#1030'.'#1056#1169'.   ('#1056#8220'CK-2011)')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo14: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 30.236240000000000000
          Width = 45.354360000000000000
          Height = 75.590551180000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Fill.BackColor = clSilver
          HAlign = haCenter
          Memo.UTF8 = (
            #1074#8222#8211
            #1056#177'/'#1057#1027)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo20: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 997.795429370000000000
          Width = 68.031496060000000000
          Height = 75.590551180000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Fill.BackColor = clSilver
          HAlign = haCenter
          Memo.UTF8 = (
            #1056#1113#1057#1107','
            #1056#1169#1056#8216)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo21: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1065.826925430000000000
          Width = 75.590551180000000000
          Height = 75.590551180000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Fill.BackColor = clSilver
          HAlign = haCenter
          Memo.UTF8 = (
            #1056#8217#1057#8249#1057#1027#1056#1109#1057#8218#1056#176' '
            #1056#1111#1056#1109#1056#1169#1056#1030#1056#181#1057#1027#1056#176' '
            #1056#176#1056#1029#1057#8218#1056#181#1056#1029#1056#1029#1057#8249','
            ' '#1056#1112)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo4: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1486.126183440000000000
          Width = 53.291338580000000000
          Height = 75.590551180000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Fill.BackColor = clSilver
          HAlign = haCenter
          Memo.UTF8 = (
            #1056#1119#1056#1109#1056#187#1057#1039#1057#1026#1056#1105#1056#183#1056#176#1057#8224#1056#1105#1057#1039)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo19: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1432.834844860000000000
          Width = 53.291338580000000000
          Height = 75.590551180000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Fill.BackColor = clSilver
          HAlign = haCenter
          Memo.UTF8 = (
            #1056#8221#1056#1105#1056#176#1056#1112'.'
            #1056#176#1056#1029#1057#8218'., '#1056#1112)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo1: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1194.330862440000000000
          Width = 52.913385830000000000
          Height = 75.590551180000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Fill.BackColor = clSilver
          HAlign = haCenter
          Memo.UTF8 = (
            #1056#160#1056#176#1057#1027#1057#1027#1057#8218'., '#1056#1108#1056#1112)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo2: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1247.244248270000000000
          Width = 52.913385830000000000
          Height = 75.590551180000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Fill.BackColor = clSilver
          HAlign = haCenter
          Memo.UTF8 = (
            #1056#1114#1056#1109#1057#8240'-'
            #1056#1029#1056#1109#1057#1027#1057#8218#1057#1034', '
            #1056#1169#1056#8216#1056#1112)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo6: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1300.157634100000000000
          Width = 70.315006040000000000
          Height = 75.590551180000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Fill.BackColor = clSilver
          HAlign = haCenter
          Memo.UTF8 = (
            #1056#1113#1056#187#1056#176#1057#1027#1057#1027
            #1056#1105#1056#183#1056#187#1057#1107#1057#8225#1056#181#1056#1029#1056#1105#1057#1039)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1370.472640140000000000
          Width = 62.362204720000000000
          Height = 75.590551180000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Fill.BackColor = clSilver
          HAlign = haCenter
          Memo.UTF8 = (
            #1056#1038#1056#1108#1056#1109#1057#1026#1056#1109#1057#1027#1057#8218#1057#1034' '#1056#1111#1056#1109#1057#8218#1056#1109#1056#1108#1056#176', M'#1056#8216#1056#1105#1057#8218'/'#1057#1027)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo37: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 75.590600000000000000
          Width = 162.519790000000000000
          Height = 75.590551180000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Fill.BackColor = clSilver
          HAlign = haCenter
          Memo.UTF8 = (
            #1056#1116#1056#176#1056#183#1056#1030#1056#176#1056#1029#1056#1105#1056#181' ')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo44: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1141.417476610000000000
          Width = 52.913385830000000000
          Height = 75.590551180000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Fill.BackColor = clSilver
          HAlign = haCenter
          Memo.UTF8 = (
            #1056#1106#1056#183#1056#1105#1056#1112#1057#1107#1057#8218', '#1056#1110#1057#1026#1056#176#1056#1169'.')
          ParentFont = False
          VAlign = vaCenter
        end
      end
    end
  end
  object frxDBDataset_link: TfrxDBDataset
    UserName = 'link'
    CloseDataSource = False
    FieldAliases.Strings = (
      'name=name'
      'project_id=project_id'
      'LinkEnd1_Property_name=LinkEnd1_Property_name'
      'LinkEnd1_Property_lat=LinkEnd1_Property_lat'
      'LinkEnd1_Property_lon=LinkEnd1_Property_lon'
      'LinkEnd1_LinkEndType_name=LinkEnd1_LinkEndType_name'
      'LinkEnd1_power_Wt=LinkEnd1_power_Wt'
      'LinkEnd1_rx_freq_MHz=LinkEnd1_rx_freq_MHz'
      'LinkEnd1_tx_freq_MHz=LinkEnd1_tx_freq_MHz'
      'LinkEnd1_antenna_gain=LinkEnd1_antenna_gain'
      'LinkEnd1_antenna_height=LinkEnd1_antenna_height'
      'LinkEnd1_antenna_diameter=LinkEnd1_antenna_diameter'
      'LinkEnd1_antenna_polarization=LinkEnd1_antenna_polarization'
      'LinkEnd2_antenna_diameter=LinkEnd2_antenna_diameter'
      'LinkEnd2_antenna_polarization=LinkEnd2_antenna_polarization'
      'LinkEnd2_antenna_height=LinkEnd2_antenna_height'
      'LinkEnd2_antenna_gain=LinkEnd2_antenna_gain'
      'LinkEnd2_antenna_tilt=LinkEnd2_antenna_tilt'
      'LinkEnd2_rx_freq_MHz=LinkEnd2_rx_freq_MHz'
      'LinkEnd2_tx_freq_MHz=LinkEnd2_tx_freq_MHz'
      'LinkEnd2_Property_name=LinkEnd2_Property_name'
      'LinkEnd2_Property_lat=LinkEnd2_Property_lat'
      'LinkEnd2_Property_lon=LinkEnd2_Property_lon'
      'LinkEnd2_power_Wt=LinkEnd2_power_Wt'
      'LinkEnd2_Property_code=LinkEnd2_Property_code'
      'LinkEnd2_Property_address=LinkEnd2_Property_address'
      'LinkEnd1_Property_code=LinkEnd1_Property_code'
      'LinkEnd1_Property_address=LinkEnd1_Property_address'
      'LinkEnd1_antenna_Azimuth=LinkEnd1_antenna_Azimuth'
      'LinkEnd2_antenna_Azimuth=LinkEnd2_antenna_Azimuth'
      'LinkEnd2_LinkEndType_name=LinkEnd2_LinkEndType_name'
      'LinkEnd2_radiation_class=LinkEnd2_radiation_class'
      'LinkEnd1_radiation_class=LinkEnd1_radiation_class'
      'LinkEnd1_antenna_loss=LinkEnd1_antenna_loss'
      'LinkEnd2_antenna_loss=LinkEnd2_antenna_loss'
      'LinkEnd2_power_W=LinkEnd2_power_W'
      'LinkEnd1_power_W=LinkEnd1_power_W'
      'LinkEnd1_antenna_tilt=LinkEnd1_antenna_tilt'
      'LinkEnd2_antenna_ground_height=LinkEnd2_antenna_ground_height'
      'LinkEnd1_antenna_ground_height=LinkEnd1_antenna_ground_height'
      'LinkEnd1_Property_ground_height=LinkEnd1_Property_ground_height'
      'LinkEnd2_Property_ground_height=LinkEnd2_Property_ground_height'
      'LinkEnd2_bitrate_Mbps=LinkEnd2_bitrate_Mbps'
      'LinkEnd1_bitrate_Mbps=LinkEnd1_bitrate_Mbps'
      'length_km=length_km'
      'LinkEnd1_power_dBm=LinkEnd1_power_dBm'
      'LinkEnd2_power_dBm=LinkEnd2_power_dBm'
      'LinkEnd2_antenna_vert_width=LinkEnd2_antenna_vert_width'
      'LinkEnd2_antenna_horz_width=LinkEnd2_antenna_horz_width'
      'LinkEnd1_antenna_vert_width=LinkEnd1_antenna_vert_width'
      'LinkEnd1_antenna_horz_width=LinkEnd1_antenna_horz_width'
      'id=id'
      'LinkEnd1_name=LinkEnd1_name'
      'LinkEnd2_name=LinkEnd2_name'
      'LinkEnd1_bandwidth=LinkEnd1_bandwidth'
      'LinkEnd2_bandwidth=LinkEnd2_bandwidth'
      'Linkend1_property_lat_wgs=Linkend1_property_lat_wgs'
      'Linkend1_property_lon_wgs=Linkend1_property_lon_wgs'
      'Linkend2_property_lat_wgs=Linkend2_property_lat_wgs'
      'Linkend2_property_lon_wgs=Linkend2_property_lon_wgs'
      'LinkEnd1_band=LinkEnd1_band'
      'LinkEnd2_band=LinkEnd2_band'
      'LinkEnd1_LinkID=LinkEnd1_LinkID'
      'LinkEnd2_LinkID=LinkEnd2_LinkID'
      'LinkEnd1_modulation_type=LinkEnd1_modulation_type'
      'LinkEnd2_modulation_type=LinkEnd2_modulation_type'
      'LinkEnd1_property_lon_gck_2011=LinkEnd1_property_lon_gck_2011'
      'LinkEnd1_property_lat_gck_2011=LinkEnd1_property_lat_gck_2011'
      'LinkEnd2_property_lon_gck_2011=LinkEnd2_property_lon_gck_2011'
      'LinkEnd2_property_lat_gck_2011=LinkEnd2_property_lat_gck_2011'
      'LinkEnd1_slot=LinkEnd1_slot'
      'LinkEnd1_IP_address=LinkEnd1_IP_address'
      'LinkEnd2_slot=LinkEnd2_slot'
      'LinkEnd2_IP_address=LinkEnd2_IP_address'
      'rx_level_dBm=rx_level_dBm'
      'LinkEnd1_NE=LinkEnd1_NE'
      'LinkEnd2_NE=LinkEnd2_NE')
    DataSet = qry_LInks
    BCDToCurrency = False
    Left = 136
    Top = 128
  end
  object frxRTFExport1: TfrxRTFExport
    FileName = 'D:\222222222222333233333444222222.rtf'
    UseFileCache = True
    ShowProgress = True
    OverwritePrompt = False
    DataOnly = False
    PictureType = gpPNG
    OpenAfterExport = True
    Wysiwyg = True
    Creator = 'FastReport'
    SuppressPageHeadersFooters = False
    HeaderFooterMode = hfText
    AutoSize = False
    Left = 40
    Top = 280
  end
  object qry_LInks: TADOQuery
    Connection = ADOConnection1
    CursorType = ctStatic
    LockType = ltReadOnly
    OnCalcFields = qry_LInksCalcFields
    Parameters = <>
    SQL.Strings = (
      
        'select top 100 * from reports.View_Link_freq_order  where projec' +
        't_id=1531'
      '')
    Left = 136
    Top = 16
    object qry_LInksname: TStringField
      FieldName = 'name'
      Size = 100
    end
    object qry_LInksproject_id: TIntegerField
      FieldName = 'project_id'
    end
    object qry_LInksLinkEnd1_Property_name: TStringField
      FieldName = 'LinkEnd1_Property_name'
      Size = 100
    end
    object qry_LInksLinkEnd1_Property_lat: TFloatField
      FieldName = 'LinkEnd1_Property_lat'
    end
    object qry_LInksLinkEnd1_Property_lon: TFloatField
      FieldName = 'LinkEnd1_Property_lon'
    end
    object qry_LInksLinkEnd1_LinkEndType_name: TStringField
      FieldName = 'LinkEnd1_LinkEndType_name'
      Size = 50
    end
    object qry_LInksLinkEnd1_power_Wt: TFloatField
      FieldName = 'LinkEnd1_power_Wt'
      ReadOnly = True
    end
    object qry_LInksLinkEnd1_rx_freq_MHz: TFloatField
      FieldName = 'LinkEnd1_rx_freq_MHz'
    end
    object qry_LInksLinkEnd1_tx_freq_MHz: TFloatField
      FieldName = 'LinkEnd1_tx_freq_MHz'
    end
    object qry_LInksLinkEnd1_antenna_gain: TStringField
      FieldName = 'LinkEnd1_antenna_gain'
      Size = 50
    end
    object qry_LInksLinkEnd1_antenna_height: TStringField
      FieldName = 'LinkEnd1_antenna_height'
      Size = 50
    end
    object qry_LInksLinkEnd1_antenna_diameter: TStringField
      FieldName = 'LinkEnd1_antenna_diameter'
      Size = 50
    end
    object qry_LInksLinkEnd1_antenna_polarization: TStringField
      FieldName = 'LinkEnd1_antenna_polarization'
      Size = 50
    end
    object qry_LInksLinkEnd2_antenna_diameter: TStringField
      FieldName = 'LinkEnd2_antenna_diameter'
      Size = 50
    end
    object qry_LInksLinkEnd2_antenna_polarization: TStringField
      FieldName = 'LinkEnd2_antenna_polarization'
      Size = 50
    end
    object qry_LInksLinkEnd2_antenna_height: TStringField
      FieldName = 'LinkEnd2_antenna_height'
      Size = 50
    end
    object qry_LInksLinkEnd2_antenna_gain: TStringField
      FieldName = 'LinkEnd2_antenna_gain'
      Size = 50
    end
    object qry_LInksLinkEnd2_antenna_tilt: TStringField
      FieldName = 'LinkEnd2_antenna_tilt'
      Size = 50
    end
    object qry_LInksLinkEnd2_rx_freq_MHz: TFloatField
      FieldName = 'LinkEnd2_rx_freq_MHz'
    end
    object qry_LInksLinkEnd2_tx_freq_MHz: TFloatField
      FieldName = 'LinkEnd2_tx_freq_MHz'
    end
    object qry_LInksLinkEnd2_Property_name: TStringField
      FieldName = 'LinkEnd2_Property_name'
      Size = 100
    end
    object qry_LInksLinkEnd2_Property_lat: TFloatField
      FieldName = 'LinkEnd2_Property_lat'
    end
    object qry_LInksLinkEnd2_Property_lon: TFloatField
      FieldName = 'LinkEnd2_Property_lon'
    end
    object qry_LInksLinkEnd2_power_Wt: TFloatField
      FieldName = 'LinkEnd2_power_Wt'
      ReadOnly = True
    end
    object qry_LInksLinkEnd2_Property_code: TStringField
      FieldName = 'LinkEnd2_Property_code'
      Size = 50
    end
    object qry_LInksLinkEnd2_Property_address: TStringField
      FieldName = 'LinkEnd2_Property_address'
      Size = 250
    end
    object qry_LInksLinkEnd1_Property_code: TStringField
      FieldName = 'LinkEnd1_Property_code'
      Size = 50
    end
    object qry_LInksLinkEnd1_Property_address: TStringField
      FieldName = 'LinkEnd1_Property_address'
      Size = 250
    end
    object qry_LInksLinkEnd1_antenna_Azimuth: TFloatField
      FieldName = 'LinkEnd1_antenna_Azimuth'
      ReadOnly = True
    end
    object qry_LInksLinkEnd2_antenna_Azimuth: TFloatField
      FieldName = 'LinkEnd2_antenna_Azimuth'
      ReadOnly = True
    end
    object qry_LInksLinkEnd2_LinkEndType_name: TStringField
      FieldName = 'LinkEnd2_LinkEndType_name'
      Size = 50
    end
    object qry_LInksLinkEnd2_radiation_class: TStringField
      FieldName = 'LinkEnd2_radiation_class'
      Size = 50
    end
    object qry_LInksLinkEnd1_radiation_class: TStringField
      FieldName = 'LinkEnd1_radiation_class'
      Size = 50
    end
    object qry_LInksLinkEnd1_antenna_loss: TStringField
      FieldName = 'LinkEnd1_antenna_loss'
      Size = 50
    end
    object qry_LInksLinkEnd2_antenna_loss: TStringField
      FieldName = 'LinkEnd2_antenna_loss'
      Size = 50
    end
    object qry_LInksLinkEnd2_power_W: TFloatField
      FieldName = 'LinkEnd2_power_W'
    end
    object qry_LInksLinkEnd1_power_W: TFloatField
      FieldName = 'LinkEnd1_power_W'
    end
    object qry_LInksLinkEnd1_antenna_tilt: TStringField
      FieldName = 'LinkEnd1_antenna_tilt'
      Size = 50
    end
    object qry_LInksLinkEnd2_antenna_ground_height: TStringField
      FieldName = 'LinkEnd2_antenna_ground_height'
      Size = 50
    end
    object qry_LInksLinkEnd1_antenna_ground_height: TStringField
      FieldName = 'LinkEnd1_antenna_ground_height'
      Size = 50
    end
    object qry_LInksLinkEnd1_Property_ground_height: TFloatField
      FieldName = 'LinkEnd1_Property_ground_height'
    end
    object qry_LInksLinkEnd2_Property_ground_height: TFloatField
      FieldName = 'LinkEnd2_Property_ground_height'
    end
    object qry_LInksLinkEnd2_bitrate_Mbps: TFloatField
      FieldName = 'LinkEnd2_bitrate_Mbps'
    end
    object qry_LInksLinkEnd1_bitrate_Mbps: TFloatField
      FieldName = 'LinkEnd1_bitrate_Mbps'
    end
    object qry_LInkslength_km: TFloatField
      FieldName = 'length_km'
      ReadOnly = True
    end
    object qry_LInksLinkEnd1_power_dBm: TFloatField
      FieldName = 'LinkEnd1_power_dBm'
    end
    object qry_LInksLinkEnd2_power_dBm: TFloatField
      FieldName = 'LinkEnd2_power_dBm'
    end
    object qry_LInksLinkEnd2_antenna_vert_width: TStringField
      FieldName = 'LinkEnd2_antenna_vert_width'
      Size = 50
    end
    object qry_LInksLinkEnd2_antenna_horz_width: TStringField
      FieldName = 'LinkEnd2_antenna_horz_width'
      Size = 50
    end
    object qry_LInksLinkEnd1_antenna_vert_width: TStringField
      FieldName = 'LinkEnd1_antenna_vert_width'
      Size = 50
    end
    object qry_LInksLinkEnd1_antenna_horz_width: TStringField
      FieldName = 'LinkEnd1_antenna_horz_width'
      Size = 50
    end
    object qry_LInksid: TIntegerField
      FieldName = 'id'
    end
    object qry_LInksLinkEnd1_name: TStringField
      FieldName = 'LinkEnd1_name'
      Size = 100
    end
    object qry_LInksLinkEnd2_name: TStringField
      FieldName = 'LinkEnd2_name'
      Size = 100
    end
    object qry_LInksLinkEnd1_bandwidth: TFloatField
      FieldName = 'LinkEnd1_bandwidth'
    end
    object qry_LInksLinkEnd2_bandwidth: TFloatField
      FieldName = 'LinkEnd2_bandwidth'
    end
    object qry_LInksLinkend1_property_lat_wgs: TFloatField
      FieldKind = fkCalculated
      FieldName = 'Linkend1_property_lat_wgs'
      Calculated = True
    end
    object qry_LInksLinkend1_property_lon_wgs: TFloatField
      FieldKind = fkCalculated
      FieldName = 'Linkend1_property_lon_wgs'
      Calculated = True
    end
    object qry_LInksLinkend2_property_lat_wgs: TFloatField
      FieldKind = fkCalculated
      FieldName = 'Linkend2_property_lat_wgs'
      Calculated = True
    end
    object qry_LInksLinkend2_property_lon_wgs: TFloatField
      FieldKind = fkCalculated
      FieldName = 'Linkend2_property_lon_wgs'
      Calculated = True
    end
    object qry_LInksLinkEnd1_band: TWideStringField
      FieldName = 'LinkEnd1_band'
      Size = 50
    end
    object qry_LInksLinkEnd2_band: TWideStringField
      FieldName = 'LinkEnd2_band'
      Size = 50
    end
    object qry_LInksLinkEnd1_LinkID: TStringField
      FieldName = 'LinkEnd1_LinkID'
      Size = 50
    end
    object qry_LInksLinkEnd2_LinkID: TStringField
      FieldName = 'LinkEnd2_LinkID'
      Size = 50
    end
    object qry_LInksLinkEnd1_modulation_type: TStringField
      FieldName = 'LinkEnd1_modulation_type'
      Size = 50
    end
    object qry_LInksLinkEnd2_modulation_type: TStringField
      FieldName = 'LinkEnd2_modulation_type'
      Size = 50
    end
    object qry_LInksLinkEnd1_property_lon_gck_2011: TFloatField
      FieldKind = fkCalculated
      FieldName = 'LinkEnd1_property_lon_gck_2011'
      Calculated = True
    end
    object qry_LInksLinkEnd1_property_lat_gck_2011: TFloatField
      FieldKind = fkCalculated
      FieldName = 'LinkEnd1_property_lat_gck_2011'
      Calculated = True
    end
    object qry_LInksLinkEnd2_property_lon_gck_2011: TFloatField
      FieldKind = fkCalculated
      FieldName = 'LinkEnd2_property_lon_gck_2011'
      Calculated = True
    end
    object qry_LInksLinkEnd2_property_lat_gck_2011: TFloatField
      FieldKind = fkCalculated
      FieldName = 'LinkEnd2_property_lat_gck_2011'
      Calculated = True
    end
    object qry_LInksLinkEnd1_slot: TIntegerField
      FieldName = 'LinkEnd1_slot'
    end
    object qry_LInksLinkEnd1_IP_address: TStringField
      FieldName = 'LinkEnd1_IP_address'
      Size = 30
    end
    object qry_LInksLinkEnd2_slot: TIntegerField
      FieldName = 'LinkEnd2_slot'
    end
    object qry_LInksLinkEnd2_IP_address: TStringField
      FieldName = 'LinkEnd2_IP_address'
      Size = 30
    end
    object qry_LInksrx_level_dBm: TFloatField
      FieldName = 'rx_level_dBm'
    end
    object qry_LInksLinkEnd1_NE: TStringField
      FieldName = 'LinkEnd1_NE'
      Size = 50
    end
    object qry_LInksLinkEnd2_NE: TStringField
      FieldName = 'LinkEnd2_NE'
      Size = 50
    end
  end
  object frxXLSExport1: TfrxXLSExport
    UseFileCache = True
    ShowProgress = True
    OverwritePrompt = False
    DataOnly = False
    ExportEMF = True
    AsText = False
    Background = True
    FastExport = True
    PageBreaks = True
    EmptyLines = True
    SuppressPageHeadersFooters = False
    Left = 40
    Top = 216
  end
  object frxReport_tele2: TfrxReport
    Version = '6.2.11'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.ShowCaptions = True
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 41121.613084722200000000
    ReportOptions.LastChange = 42060.747135995400000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      '  '
      ''
      '  '
      '  '
      '//------------------------------------------------------------'
      'function DegreeToStr (aValue: double): String;'
      '//------------------------------------------------------------'
      'const'
      '//  STR_BLPOINT_FORMAT1 = '#39'%d'#176'%.2d'#39#39'%s%1.2f'#39#39#39#39#39';'
      '  STR_BLPOINT_FORMAT1 = '#39'%d'#176' %.2d'#39#39' %1.2f'#39#39#39#39#39';'
      'var    '
      ' iDeg,iMin:integer; '
      ' eSec: Double;   '
      '           '
      'begin'
      '  geo_DecodeDegree_(aValue, iDeg,iMin,eSec);'
      '                           '
      ' // angle.Sec:=TruncFloat(angle.Sec,2);'
      ' '
      '    Result:=Format(STR_BLPOINT_FORMAT1, [iDeg, iMin, eSec]);'
      ''
      '  if aValue<0 then'
      '    Result:='#39'-'#39'+Result;'
      'end;'
      ''
      ' //------------------------------------------------------------'
      'function ChangeName (aValue: String): String;'
      '//------------------------------------------------------------'
      'begin   '
      '  Result:=Copy(aValue, 1, 2) + Copy(aValue, 5, 4)'
      'end;  '
      '                     '
      ''
      '  var'
      '  LLine : integer;                                         '
      ''
      '  '
      '//------------------------------------------------------------'
      'function GetLine (): integer;'
      '//------------------------------------------------------------'
      'begin'
      '  Inc(LLine);                '
      '  Result:=LLine;  '
      'end;  '
      '                     '
      '  '
      '  '
      ''
      '    '
      ''
      ' //------------------------------------------------------------'
      
        'procedure geo_DecodeDegree_(aValue: Double; var aDeg,aMin:intege' +
        'r; var aSec:'
      '    Double);'
      '//------------------------------------------------------------'
      'var'
      '  bIsNegative: Boolean;'
      '  e: Double;'
      '  eSec: Double;'
      ''
      'begin'
      '  bIsNegative := aValue<0;'
      ''
      '  aValue :=Abs(aValue);'
      ''
      ''
      ''
      '   //    aValue:=99;                            '
      '          '
      ''
      '          '
      '  aDeg:=Trunc(aValue);'
      ''
      '  aMin:=Trunc(Frac(aValue)*60);'
      ''
      ''
      '//  e:= (aValue - Trunc(aValue));'
      '  e:= (aValue - Trunc(aValue)) * 60;'
      ''
      ''
      '  aMin:=Trunc(e);'
      '  aSec:=(((aValue - aDeg) *60) - aMin) * 60;'
      ''
      '  while aSec>(60-0.1) do'
      '  begin'
      '    aMin:=aMin+1;'
      '    aSec:=aSec-60;'
      '  end;    '
      ''
      '  while aMin>=60 do'
      '  begin'
      '      aMin:=aMin-60;'
      '      aDeg:=aDeg+1;'
      '  end;  '
      ''
      '  if aSec<0 then'
      '     aSec:=0;    '
      ''
      ''
      '  if bIsNegative then'
      '    aDeg:=-aDeg;'
      ''
      ''
      ''
      'end;'
      '                                   '
      ''
      '//------------------------------------------------------------'
      
        'procedure geo_DecodeDegree_1111(aValue: Double; var aDeg,aMin:in' +
        'teger; var aSec:'
      '    Double);'
      '//------------------------------------------------------------'
      'const'
      ' EXP_ = 0.000000001;'
      '     //97,033333333'
      'var'
      '  bIsNegative: Boolean;'
      '  e: Double;'
      '  eMinutes: Double;'
      '  eSec: Double;'
      '  eSeconds: Double;'
      'begin'
      '  bIsNegative := aValue<0;'
      ''
      '  aValue :=Abs(aValue);'
      ''
      '  aDeg:=Trunc(aValue);'
      ''
      ' // e:=Abs(aValue)*60;'
      ''
      '//  , Frac(X) = X - Int(X).'
      ''
      '  aMin:=Trunc(Frac(aValue)*60);'
      ''
      '//  Result.Sec:=Frac(Frac(Abs(aValue))*60)*60;'
      '  e:= (aValue - Trunc(aValue)) * 60;'
      '  aMin:=Trunc(e);'
      ''
      ' eMinutes := Trunc (e);'
      ' eSeconds := (e - Trunc (e)) * 60 ;'
      ''
      ''
      '  aSec:=(((aValue - aDeg) *60) - aMin) * 60;'
      ''
      '  if (aSec - 60) > EXP_ then'
      '  begin'
      '    Inc(aMin);'
      '    aSec := 0;'
      '  end;'
      ''
      ''
      '  if bIsNegative then'
      '    aDeg:=-aDeg;'
      '                   '
      ''
      ''
      'end;'
      '        '
      ''
      '       '
      'begin'
      ''
      'end.')
    Left = 424
    Top = 24
    Datasets = <
      item
        DataSet = frxDBDataset_link
        DataSetName = 'link'
      end>
    Variables = <>
    Style = <
      item
        Name = 'Title'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Height = -16
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        Frame.Typ = []
        Fill.BackColor = clGray
      end
      item
        Name = 'Header'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clMaroon
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        Frame.Typ = []
      end
      item
        Name = 'Group header'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clMaroon
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        Frame.Typ = []
        Fill.BackColor = 16053492
      end
      item
        Name = 'Data'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        Frame.Typ = []
      end
      item
        Name = 'Group footer'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        Frame.Typ = []
      end
      item
        Name = 'Header line'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        Frame.Typ = [ftBottom]
        Frame.Width = 2.000000000000000000
      end>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      Orientation = poLandscape
      PaperWidth = 429.291750000000000000
      PaperHeight = 209.999791670000000000
      PaperSize = 256
      LeftMargin = 10.001250000000000000
      RightMargin = 10.001250000000000000
      TopMargin = 10.001250000000000000
      BottomMargin = 10.001250000000000000
      Frame.Typ = []
      PrintIfEmpty = False
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        Frame.Typ = []
        Top = 16.000000000000000000
        Visible = False
        Width = 1546.920999052500000000
        StartNewPage = True
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Frame.Typ = []
        Height = 120.944923390000000000
        Top = 216.000000000000000000
        Width = 1546.920999052500000000
        DataSet = frxDBDataset_link
        DataSetName = 'link'
        RowCount = 0
        object property1name: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 45.354360000000000000
          Width = 113.385826770000000000
          Height = 60.472440940000000000
          DataSetName = 'property1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[link."Linkend1_property_name"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object property2name: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 45.354360000000000000
          Top = 60.472480000000000000
          Width = 113.385826770000000000
          Height = 60.472440940000000000
          DataSetName = 'property2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[link."Linkend1_property_name"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object property1address: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 272.126013540000000000
          Width = 200.000000000000000000
          Height = 60.472440940000000000
          DataSetName = 'property1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[link."Linkend1_property_address"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object property2address: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 272.126013540000000000
          Top = 60.472480000000000000
          Width = 200.000000000000000000
          Height = 60.472440940000000000
          DataSetName = 'property2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[link."Linkend1_property_address"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Linkend1LinkEndTypename: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 158.740186770000000000
          Width = 113.385826770000000000
          Height = 60.472440940000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd1
          DataSetName = 'LinkEnd1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[link."Linkend1_LinkEndType_name"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo17: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 158.740186770000000000
          Top = 60.472440940000000000
          Width = 113.385826770000000000
          Height = 60.472440940000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[link."Linkend2_LinkEndType_name"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Linkend1antenna_height: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 714.015777320000000000
          Width = 75.590551180000000000
          Height = 60.472440940000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd1
          DataSetName = 'LinkEnd1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[link."linkend1_Antenna_height"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo22: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 714.015777320000000000
          Top = 60.472480000000000000
          Width = 75.590551180000000000
          Height = 60.472440940000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd1
          DataSetName = 'LinkEnd1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[link."linkend2_Antenna_height"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo23: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 940.787430860000000000
          Width = 75.590551180000000000
          Height = 60.472440940000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[link."linkend1_antenna_loss"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Linkend2antenna_polarization: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1318.740186760000000000
          Width = 75.590551180000000000
          Height = 60.472440940000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[link."linkend1_antenna_polarization"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo18: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1319.149635580000000000
          Top = 60.472480000000000000
          Width = 75.590551180000000000
          Height = 60.472440940000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[link."linkend2_antenna_polarization"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo25: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1167.559084400000000000
          Width = 75.590551180000000000
          Height = 60.470000000000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[link."linkend1_antenna_tilt"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo27: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1167.559084400000000000
          Top = 60.472480000000000000
          Width = 75.590551180000000000
          Height = 60.470000000000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[link."linkend2_antenna_tilt"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo29: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 865.196879680000000000
          Width = 75.590551180000000000
          Height = 60.472440940000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd1
          DataSetName = 'LinkEnd1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[link."linkend1_antenna_gain"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo30: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 865.196879680000000000
          Top = 60.472480000000000000
          Width = 75.590551180000000000
          Height = 60.472440940000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd1
          DataSetName = 'LinkEnd1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[link."linkend2_antenna_gain"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Linkend1radiation_class: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1243.149635580000000000
          Width = 75.590551180000000000
          Height = 60.470000000000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[link."linkend1_radiation_class"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object property2lat_str: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 472.126013540000000000
          Top = 60.472480000000000000
          Width = 120.944881890000000000
          Height = 60.472480000000000000
          DataSetName = 'property2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[DegreeToStr(<link."Linkend2_property_lat">)]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo38: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 472.126013540000000000
          Width = 120.944881890000000000
          Height = 60.472480000000000000
          DataSetName = 'property2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[DegreeToStr(<link."Linkend1_property_lat">)]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo39: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 593.070895430000000000
          Top = 60.472480000000000000
          Width = 120.944881890000000000
          Height = 60.472480000000000000
          DataSetName = 'property2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[DegreeToStr(<link."Linkend2_property_lon">)]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo40: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 593.070895430000000000
          Width = 120.944881890000000000
          Height = 60.472480000000000000
          DataSetName = 'property2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[DegreeToStr(<link."Linkend1_property_lon">)]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo41: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Width = 45.354360000000000000
          Height = 60.472440940000000000
          DataSetName = 'property1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[GetLine()]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo42: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 60.472480000000000000
          Width = 45.354360000000000000
          Height = 60.472440940000000000
          DataSetName = 'property1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[GetLine()]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo46: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 789.606328500000000000
          Width = 75.590551180000000000
          Height = 60.472440940000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[link."linkend1_antenna_azimuth"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo47: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 789.606328500000000000
          Top = 60.472480000000000000
          Width = 75.590551180000000000
          Height = 60.472440940000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[link."linkend2_antenna_azimuth"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo9: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 940.787430860000000000
          Top = 60.472480000000000000
          Width = 75.590551180000000000
          Height = 60.472440940000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[link."linkend2_antenna_loss"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Linkend1power_dBm: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1016.377982040000000000
          Width = 75.590551180000000000
          Height = 60.472480000000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd1
          DataSetName = 'LinkEnd1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[link."linkend1_power_dBm"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo33: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1016.377982040000000000
          Top = 60.472480000000000000
          Width = 75.590551180000000000
          Height = 60.472480000000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd1
          DataSetName = 'LinkEnd1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[link."linkend2_power_dBm"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo34: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1091.968533220000000000
          Width = 75.590551180000000000
          Height = 60.472440940000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[link."linkend1_rx_freq_MHz"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo35: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1091.968533220000000000
          Top = 60.472480000000000000
          Width = 75.590551180000000000
          Height = 60.472440940000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[link."linkend2_rx_freq_MHz"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo1: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1394.330737940000000000
          Width = 75.590551180000000000
          Height = 60.472440940000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[link."linkend1_antenna_tilt"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1394.740186760000000000
          Top = 60.472480000000000000
          Width = 75.590551180000000000
          Height = 60.472440940000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[link."linkend2_antenna_tilt"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo19: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1243.149635580000000000
          Top = 60.470000000000000000
          Width = 76.000000000000000000
          Height = 60.470000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[link."linkend2_radiation_class"]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object Header1: TfrxHeader
        FillType = ftBrush
        Frame.Typ = []
        Height = 120.944911180000000000
        Top = 76.000000000000000000
        Width = 1546.920999052500000000
        object Memo3: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 272.126013541654000000
          Width = 200.000000000000000000
          Height = 120.944881890000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Fill.BackColor = clSilver
          HAlign = haCenter
          Memo.UTF8 = (
            #1056#1114#1056#181#1057#1027#1057#8218#1056#1109' '#1057#1026#1056#176#1056#183#1056#1112#1056#181#1057#8240#1056#181#1056#1029#1056#1105#1057#1039' '#1056#160#1056#160#1056#1038' ('#1056#176#1056#1169#1057#1026#1056#181#1057#1027')')
          ParentFont = False
          VAlign = vaBottom
        end
        object Memo7: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1167.559084406064000000
          Width = 75.590551181102400000
          Height = 120.944881889764000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Fill.BackColor = clSilver
          Memo.UTF8 = (
            
              #1056#1032#1056#1110#1056#1109#1056#187' '#1056#1112#1056#181#1057#1027#1057#8218#1056#176' '#1056#1110#1056#187#1056#176#1056#1030#1056#1029#1056#1109#1056#1110#1056#1109' '#1056#187#1056#181#1056#1111#1056#181#1057#1027#1057#8218#1056#1108#1056#176' '#1056#1105#1056#183#1056#187#1057#1107#1057#8225 +
              #1056#181#1056#1029#1056#1105#1057#1039', '#1056#1110#1057#1026#1056#176#1056#1169)
          ParentFont = False
          Rotation = 90
          VAlign = vaCenter
        end
        object Memo10: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 472.126013541654000000
          Top = 68.031540000000000000
          Width = 120.944881890000000000
          Height = 52.913371180000000000
          GroupIndex = 1
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Fill.BackColor = clSilver
          HAlign = haCenter
          Memo.UTF8 = (
            #1057#1027'.'#1057#8364'. ('#1056#1110#1057#1026#1056#176#1056#1169', '#1056#1112#1056#1105#1056#1029', '#1057#1027')')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo11: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 158.740186771654000000
          Width = 113.385826770000000000
          Height = 120.944881889764000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Fill.BackColor = clSilver
          HAlign = haCenter
          Memo.UTF8 = (
            #1056#1118#1056#1105#1056#1111' '#1056#160#1056#160#1056#1038)
          ParentFont = False
          VAlign = vaBottom
        end
        object Memo12: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1091.968533226064000000
          Width = 75.590551180000000000
          Height = 120.944881890000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Fill.BackColor = clSilver
          Memo.UTF8 = (
            #1056#167#1056#176#1057#1027#1057#8218#1056#1109#1057#8218#1057#8249' '#1056#1119#1056#160#1056#8221'/'#1056#1119#1056#160#1056#1114', '#1056#1114#1056#8220#1057#8224)
          ParentFont = False
          VAlign = vaBottom
        end
        object Memo13: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 593.070895431654000000
          Top = 68.031540000000000000
          Width = 120.944881890000000000
          Height = 52.913371180000000000
          GroupIndex = 1
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Fill.BackColor = clSilver
          HAlign = haCenter
          Memo.UTF8 = (
            #1056#1030'.'#1056#1169'. ('#1056#1110#1057#1026#1056#176#1056#1169', '#1056#1112#1056#1105#1056#1029', '#1057#1027')')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo14: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Width = 45.354360000000000000
          Height = 120.944881889764000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Fill.BackColor = clSilver
          HAlign = haCenter
          Memo.UTF8 = (
            #1074#8222#8211' '
            #1056#1111'/'#1056#1111'.')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo20: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 865.196879683858800000
          Width = 75.590551181102400000
          Height = 120.944886770000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Fill.BackColor = clSilver
          Memo.UTF8 = (
            #1056#1113#1056#1109#1057#1036#1057#8222#1057#8222#1056#1105#1057#8224#1056#1105#1056#181#1056#1029#1057#8218' '#1057#1107#1057#1027#1056#1105#1056#187#1056#181#1056#1029#1056#1105#1057#1039' '#1056#176#1056#1029#1057#8218#1056#181#1056#1029#1056#1029#1057#8249', dBi')
          ParentFont = False
          Rotation = 90
          VAlign = vaCenter
        end
        object Memo21: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 714.015777321654000000
          Width = 75.590551181102400000
          Height = 120.944881890000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Fill.BackColor = clSilver
          Memo.UTF8 = (
            
              #1056#8217#1057#8249#1057#1027#1056#1109#1057#8218#1056#176' '#1056#176#1056#1029#1057#8218#1056#181#1056#1029#1056#1029' '#1056#160#1056#160#1056#1038' '#1056#1109#1057#8218' '#1056#1111#1056#1109#1056#1030#1056#181#1057#1026#1057#8230#1056#1029#1056#1109#1057#1027#1057#8218#1056#1105' '#1056#8212#1056 +
              #181#1056#1112#1056#187#1056#1105', '#1056#1112)
          ParentFont = False
          Rotation = 90
          VAlign = vaCenter
        end
        object Memo4: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1318.740186768268000000
          Width = 75.590551181102400000
          Height = 120.944911180000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Fill.BackColor = clSilver
          Memo.UTF8 = (
            #1056#1119#1056#1109#1056#187#1057#1039#1057#1026#1056#1105#1056#183#1056#176#1057#8224#1056#1105#1057#1039' '#1056#1119#1056#160#1056#8221'/'#1056#1119#1056#160#1056#1114)
          ParentFont = False
          Rotation = 90
          VAlign = vaCenter
        end
        object Memo2: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1016.377982046064000000
          Width = 75.590551180000000000
          Height = 120.944911180000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Fill.BackColor = clSilver
          HAlign = haCenter
          Memo.UTF8 = (
            #1056#1114#1056#1109#1057#8240#1056#1029#1056#1109#1057#1027#1057#8218#1057#1034' '#1056#1111#1056#181#1057#1026#1056#181#1056#1169#1056#176#1057#8218#1057#8225#1056#1105#1056#1108#1056#176', dBm')
          ParentFont = False
          Rotation = 90
          VAlign = vaCenter
        end
        object Memo6: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1243.149635587166000000
          Width = 75.590551181102400000
          Height = 120.944911180000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Fill.BackColor = clSilver
          Memo.UTF8 = (
            #1056#1113#1056#187#1056#176#1057#1027#1057#1027
            #1056#1105#1056#183#1056#187#1057#1107#1057#8225#1056#181#1056#1029#1056#1105#1057#1039)
          ParentFont = False
          Rotation = 90
          VAlign = vaCenter
        end
        object Memo37: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 45.354360000000000000
          Width = 113.385826771654000000
          Height = 120.944881889764000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Fill.BackColor = clSilver
          Memo.UTF8 = (
            #1074#8222#8211' '#1056#1038#1057#8218#1056#176#1056#1029#1057#8224#1056#1105#1056#1105' ('#1056#1109#1056#177#1056#1109#1056#183#1056#1029#1056#176#1057#8225#1056#181#1056#1029#1056#1105#1056#181' '#1056#1030' '#1057#1027#1056#181#1057#8218#1056#1105')')
          ParentFont = False
          Rotation = 90
          VAlign = vaCenter
        end
        object Memo44: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 789.606328502756400000
          Width = 75.590551181102400000
          Height = 120.944881890000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Fill.BackColor = clSilver
          Memo.UTF8 = (
            
              #1056#1106#1056#183#1056#1105#1056#1112#1057#1107#1057#8218' '#1056#1110#1056#187#1056#176#1056#1030#1056#1029#1056#1109#1056#1110#1056#1109' '#1056#187#1056#181#1056#1111#1056#181#1057#1027#1057#8218#1056#1108#1056#176' '#1056#1105#1056#183#1056#187#1057#1107#1057#8225#1056#181#1056#1029#1056#1105#1057 +
              #1039' '#1056#176#1056#1029#1057#8218#1056#181#1056#1029#1056#1029#1057#8249', '#1056#1110#1057#1026#1056#176#1056#1169)
          ParentFont = False
          Rotation = 90
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 472.126013541654000000
          Width = 241.889846770000000000
          Height = 68.031491180000000000
          GroupIndex = 1
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Fill.BackColor = clSilver
          HAlign = haCenter
          Memo.UTF8 = (
            #1056#8220#1056#181#1056#1109#1056#1110#1057#1026#1056#176#1057#8222#1056#1105#1057#8225#1056#181#1057#1027#1056#1108#1056#1105#1056#181' '#1056#1108#1056#1109#1056#1109#1057#1026#1056#1169#1056#1105#1056#1029#1056#176#1057#8218#1057#8249' ('#1056#1038#1056#1113'-42)')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo8: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 940.787430864961200000
          Width = 75.590551181102400000
          Height = 120.944911180000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Fill.BackColor = clSilver
          Memo.UTF8 = (
            #1056#1119#1056#1109#1057#8218#1056#181#1057#1026#1056#1105' '#1056#1030' '#1056#176#1056#1029#1057#8218#1056#181#1056#1029#1056#1029#1056#1109'-'#1057#8222#1056#1105#1056#1169#1056#181#1057#1026#1056#1029#1056#1109#1056#1112' '#1057#8218#1057#1026#1056#176#1056#1108#1057#8218#1056#181', dB')
          ParentFont = False
          Rotation = 90
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1394.330737949371000000
          Width = 75.590551180000000000
          Height = 120.944911180000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Fill.BackColor = clSilver
          Memo.UTF8 = (
            #1056#1032#1056#1110#1056#1109#1056#187' '#1056#1112#1056#181#1057#1027#1057#8218#1056#176)
          ParentFont = False
          Rotation = 90
          VAlign = vaCenter
        end
      end
    end
  end
  object frxPDFExport1: TfrxPDFExport
    UseFileCache = True
    ShowProgress = True
    OverwritePrompt = False
    DataOnly = False
    OpenAfterExport = False
    PrintOptimized = False
    Outline = False
    Background = False
    HTMLTags = True
    Quality = 95
    Transparency = False
    Author = 'FastReport'
    Subject = 'FastReport PDF export'
    ProtectionFlags = [ePrint, eModify, eCopy, eAnnot]
    HideToolbar = False
    HideMenubar = False
    HideWindowUI = False
    FitWindow = False
    CenterWindow = False
    PrintScaling = False
    PdfA = False
    Left = 136
    Top = 216
  end
  object frxDesigner1: TfrxDesigner
    DefaultScriptLanguage = 'PascalScript'
    DefaultFont.Charset = DEFAULT_CHARSET
    DefaultFont.Color = clWindowText
    DefaultFont.Height = -13
    DefaultFont.Name = 'Arial'
    DefaultFont.Style = []
    DefaultLeftMargin = 10.000000000000000000
    DefaultRightMargin = 10.000000000000000000
    DefaultTopMargin = 10.000000000000000000
    DefaultBottomMargin = 10.000000000000000000
    DefaultPaperSize = 9
    DefaultOrientation = poPortrait
    TemplatesExt = 'fr3'
    Restrictions = []
    RTLLanguage = False
    MemoParentFont = False
    OnGetTemplateList = frxDesigner1GetTemplateList
    Left = 136
    Top = 280
  end
  object frxReport_Tele2_CK42: TfrxReport
    Version = '6.2.11'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.AllowEdit = False
    PreviewOptions.Buttons = [pbPrint, pbExport, pbZoom, pbTools, pbNavigator, pbExportQuick, pbNoFullScreen]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 41121.613084722200000000
    ReportOptions.LastChange = 42090.555932268500000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      '//------------------------------------------------------------'
      '//function dBm_to_Wt_ (aValue: variant): double;'
      '//------------------------------------------------------------'
      '//begin'
      '//  if aValue = null then'
      '//    Result:=0'
      '//  else'
      '//    Result:=dBm_to_Wt (aValue);           '
      '  '
      '  '
      '//end;'
      '  '
      'var'
      '  LLine : integer;                                         '
      ''
      '  '
      '//------------------------------------------------------------'
      'function GetLine (): integer;'
      '//------------------------------------------------------------'
      'begin'
      '  Inc(LLine);                '
      '  Result:=LLine;  '
      'end;  '
      '                     '
      '  '
      '  '
      ''
      '//------------------------------------------------------------'
      'function ChangeName (aValue: String): String;'
      '//------------------------------------------------------------'
      'begin   '
      '  Result:=Copy(aValue, 1, 2) + Copy(aValue, 5, 4)'
      'end;  '
      '                     '
      '  '
      '  '
      '//------------------------------------------------------------'
      'function DegreeToStr (aValue: double): String;'
      '//------------------------------------------------------------'
      'const'
      '//  STR_BLPOINT_FORMAT1 = '#39'%d'#176'%.2d'#39#39'%1.2f'#39#39#39#39#39';'
      '  STR_BLPOINT_FORMAT1 = '#39'%d %.2d %1.0f'#39';'
      'var'
      ' iDeg,iMin:integer; '
      ' eSec: Double;   '
      '           '
      'begin'
      '  geo_DecodeDegree_(aValue, iDeg,iMin,eSec);'
      '                           '
      ' // angle.Sec:=TruncFloat(angle.Sec,2);'
      ' '
      '    Result:=Format(STR_BLPOINT_FORMAT1, [iDeg, iMin, eSec]);'
      ''
      '  if aValue<0 then'
      '    Result:='#39'-'#39'+Result;'
      'end;'
      ''
      ''
      ' //------------------------------------------------------------'
      
        'procedure geo_DecodeDegree_(aValue: Double; var aDeg,aMin:intege' +
        'r; var aSec:'
      '    Double);'
      '//------------------------------------------------------------'
      'var'
      '  bIsNegative: Boolean;'
      '  e: Double;'
      '  eSec: Double;'
      ''
      'begin'
      '  bIsNegative := aValue<0;'
      ''
      '  aValue :=Abs(aValue);'
      ''
      ''
      ''
      '   //    aValue:=99;                            '
      '          '
      ''
      '          '
      '  aDeg:=Trunc(aValue);'
      ''
      '  aMin:=Trunc(Frac(aValue)*60);'
      ''
      ''
      '//  e:= (aValue - Trunc(aValue));'
      '  e:= (aValue - Trunc(aValue)) * 60;'
      ''
      ''
      '  aMin:=Trunc(e);'
      '  aSec:=(((aValue - aDeg) *60) - aMin) * 60;'
      ''
      '   while aSec>(60-0.1) do'
      '  begin'
      '    aMin:=aMin+1;'
      '    aSec:=aSec-60;'
      '  end;    '
      ''
      '  while aMin>=60 do'
      '  begin'
      '      aMin:=aMin-60;'
      '      aDeg:=aDeg+1;'
      '  end;  '
      ''
      '  if aSec<0 then'
      '     aSec:=0;    '
      ''
      ''
      '  if bIsNegative then'
      '    aDeg:=-aDeg;'
      ''
      ''
      ''
      'end;'
      '  '
      '                              '
      '           '
      ''
      '//------------------------------------------------------------'
      
        'procedure geo_DecodeDegree_111111(aValue: Double; var aDeg,aMin:' +
        'integer; var aSec:'
      '    Double);'
      '//------------------------------------------------------------'
      'const'
      ' EXP_ = 0.000000001;'
      '     //97,033333333'
      'var'
      '  bIsNegative: Boolean;'
      '  e: Double;'
      '  eMinutes: Double;'
      '  eSec: Double;'
      '  eSeconds: Double;'
      'begin'
      '  bIsNegative := aValue<0;'
      ''
      '  aValue :=Abs(aValue);'
      ''
      '  aDeg:=Trunc(aValue);'
      ''
      ' // e:=Abs(aValue)*60;'
      ''
      '//  , Frac(X) = X - Int(X).'
      ''
      '  aMin:=Trunc(Frac(aValue)*60);'
      ''
      '//  Result.Sec:=Frac(Frac(Abs(aValue))*60)*60;'
      '  e:= (aValue - Trunc(aValue)) * 60;'
      '  aMin:=Trunc(e);'
      ''
      ' eMinutes := Trunc (e);'
      ' eSeconds := (e - Trunc (e)) * 60 ;'
      ''
      ''
      '  aSec:=(((aValue - aDeg) *60) - aMin) * 60;'
      ''
      '  if (aSec - 60) > EXP_ then'
      '  begin'
      '    Inc(aMin);'
      '    aSec := 0;'
      '  end;'
      ''
      ''
      '  if bIsNegative then'
      '    aDeg:=-aDeg;'
      '                   '
      ''
      ''
      'end;'
      '        '
      ''
      '       '
      'begin'
      ''
      'end.')
    Left = 558
    Top = 24
    Datasets = <
      item
        DataSet = frxDBDataset_link
        DataSetName = 'link'
      end>
    Variables = <>
    Style = <
      item
        Name = 'Title'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Height = -16
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        Frame.Typ = []
        Fill.BackColor = clGray
      end
      item
        Name = 'Header'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clMaroon
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        Frame.Typ = []
      end
      item
        Name = 'Group header'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clMaroon
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        Frame.Typ = []
        Fill.BackColor = 16053492
      end
      item
        Name = 'Data'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        Frame.Typ = []
      end
      item
        Name = 'Group footer'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        Frame.Typ = []
      end
      item
        Name = 'Header line'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        Frame.Typ = [ftBottom]
        Frame.Width = 2.000000000000000000
      end>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      Orientation = poLandscape
      PaperWidth = 429.291750000000000000
      PaperHeight = 839.999166670000000000
      PaperSize = 256
      LeftMargin = 10.001250000000000000
      RightMargin = 10.001250000000000000
      TopMargin = 10.001250000000000000
      BottomMargin = 10.001250000000000000
      Frame.Typ = []
      PrintIfEmpty = False
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        Frame.Typ = []
        Top = 16.000000000000000000
        Visible = False
        Width = 1546.920999052500000000
        StartNewPage = True
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Frame.Typ = []
        Height = 40.944960000000000000
        Top = 224.000000000000000000
        Width = 1546.920999052500000000
        DataSet = frxDBDataset_link
        DataSetName = 'link'
        RowCount = 0
        object property1name: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 80.000000000000000000
          Width = 61.385826770000000000
          Height = 20.472440940000000000
          DataSetName = 'property1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[link."Linkend1_property_name"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object property2name: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 80.000000000000000000
          Top = 20.472480000000000000
          Width = 61.385826770000000000
          Height = 20.472440940000000000
          DataSetName = 'property2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[link."Linkend2_property_name"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object property1address: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 254.771653540000000000
          Width = 636.000000000000000000
          Height = 20.472440940000000000
          DataSetName = 'property1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[link."Linkend1_property_address"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object property2address: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 254.771653540000000000
          Top = 20.472480000000000000
          Width = 636.000000000000000000
          Height = 20.472440940000000000
          DataSetName = 'property2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[link."Linkend1_property_address"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Linkend1LinkEndTypename: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 141.385826770000000000
          Width = 113.385826770000000000
          Height = 20.472440940000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd1
          DataSetName = 'LinkEnd1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[link."Linkend1_LinkEndType_name"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo17: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 141.385826770000000000
          Top = 20.472440940000000000
          Width = 113.385826770000000000
          Height = 20.472440940000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[link."Linkend2_LinkEndType_name"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Linkend1antenna_height: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1060.661417320000000000
          Width = 51.590551180000000000
          Height = 20.472440940000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd1
          DataSetName = 'LinkEnd1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[link."Linkend1_Antenna_height"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo22: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1060.661417320000000000
          Top = 20.472480000000000000
          Width = 51.590551180000000000
          Height = 20.472440940000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd1
          DataSetName = 'LinkEnd1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[link."Linkend2_Antenna_height"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo23: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1215.433070860000000000
          Width = 51.590551180000000000
          Height = 20.472440940000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[link."Linkend1_antenna_loss"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Linkend2antenna_polarization: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1473.385826760000000000
          Width = 35.590551180000000000
          Height = 20.472440940000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[link."linkend1_antenna_polarization"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo18: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1473.385826760000000000
          Top = 20.472480000000000000
          Width = 35.590551180000000000
          Height = 20.472440940000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[link."linkend2_antenna_polarization"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo25: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1350.204724400000000000
          Width = 51.590551180000000000
          Height = 20.472440940000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[link."Linkend1_antenna_tilt"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo27: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1350.204724400000000000
          Top = 20.472480000000000000
          Width = 51.590551180000000000
          Height = 20.472440940000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[link."Linkend2_antenna_tilt"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo29: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1163.842519680000000000
          Width = 51.590551180000000000
          Height = 20.472440940000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd1
          DataSetName = 'LinkEnd1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[link."Linkend1_antenna_gain"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo30: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1163.842519680000000000
          Top = 20.472480000000000000
          Width = 51.590551180000000000
          Height = 20.472440940000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd1
          DataSetName = 'LinkEnd1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[link."Linkend2_antenna_gain"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Linkend2radiation_class: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1401.795275580000000000
          Top = 20.472480000000000000
          Width = 71.590551180000000000
          Height = 20.472480000000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[link."Linkend2_radiation_class"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo31: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1401.795275580000000000
          Width = 71.590551180000000000
          Height = 20.472480000000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[link."Linkend1_radiation_class"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object property2lat_str: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 890.771653540000000000
          Top = 20.472480000000000000
          Width = 84.944881890000000000
          Height = 20.472480000000000000
          DataSetName = 'property2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[DegreeToStr(<link."LinkEnd2_property_lat">)]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo38: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 890.771653540000000000
          Width = 84.944881890000000000
          Height = 20.472480000000000000
          DataSetName = 'property2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[DegreeToStr(<link."LinkEnd1_property_lat">)]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo39: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 975.716535430000000000
          Top = 20.472480000000000000
          Width = 84.944881890000000000
          Height = 20.472480000000000000
          DataSetName = 'property2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[DegreeToStr(<link."LinkEnd2_property_lon">)]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo40: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 975.716535430000000000
          Width = 84.944881890000000000
          Height = 20.472480000000000000
          DataSetName = 'property2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[DegreeToStr(<link."LinkEnd1_property_lon">)]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo41: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Width = 80.000000000000000000
          Height = 20.472440940000000000
          DataSetName = 'property1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[GetLine()]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo42: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 20.472480000000000000
          Width = 80.000000000000000000
          Height = 20.472440940000000000
          DataSetName = 'property1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[GetLine()]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo46: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1112.251968500000000000
          Width = 51.590551180000000000
          Height = 20.472440940000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          DisplayFormat.FormatStr = '%2.0f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[link."Linkend1_antenna_azimuth"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo47: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1112.251968500000000000
          Top = 20.472480000000000000
          Width = 51.590551180000000000
          Height = 20.472440940000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          DisplayFormat.FormatStr = '%2.0f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[link."Linkend2_antenna_azimuth"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo9: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1215.433070860000000000
          Top = 20.472480000000000000
          Width = 51.590551180000000000
          Height = 20.472440940000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[link."Linkend2_antenna_loss"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Linkend1power_dBm: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1267.023622040000000000
          Width = 39.590551180000000000
          Height = 20.472480000000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd1
          DataSetName = 'LinkEnd1'
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[link."Linkend1_power_W"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo33: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1267.023622040000000000
          Top = 20.472480000000000000
          Width = 39.590551180000000000
          Height = 20.472480000000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd1
          DataSetName = 'LinkEnd1'
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[link."Linkend2_power_W"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo34: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1306.614173220000000000
          Width = 43.590551180000000000
          Height = 20.472440940000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[link."Linkend1_rx_freq_MHz"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo35: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1306.614173220000000000
          Top = 20.472480000000000000
          Width = 43.590551180000000000
          Height = 20.472440940000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[link."Linkend2_rx_freq_MHz"]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object Header1: TfrxHeader
        FillType = ftBrush
        Frame.Typ = []
        Height = 128.944911180000000000
        Top = 76.000000000000000000
        Width = 1546.920999052500000000
        object Memo3: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 254.771653540000000000
          Width = 636.000000000000000000
          Height = 128.944881890000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            #1056#1114#1056#181#1057#1027#1057#8218#1056#1109' '#1057#1026#1056#176#1056#183#1056#1112#1056#181#1057#8240#1056#181#1056#1029#1056#1105#1057#1039' '#1056#160#1056#160#1056#1038' ('#1056#176#1056#1169#1057#1026#1056#181#1057#1027')')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo7: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1350.204749080000000000
          Width = 51.590551180000000000
          Height = 128.944881890000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            
              #1056#1032#1056#1110#1056#1109#1056#187' '#1056#1112#1056#181#1057#1027#1057#8218#1056#176' '#1056#1110#1056#187#1056#176#1056#1030#1056#1029#1056#1109#1056#1110#1056#1109' '#1056#187#1056#181#1056#1111#1056#181#1057#1027#1057#8218#1056#1108#1056#176' '#1056#1105#1056#183#1056#187#1057#1107#1057#8225 +
              #1056#181#1056#1029#1056#1105#1057#1039', '#1056#1110#1057#1026#1056#176#1056#1169)
          ParentFont = False
          Rotation = 90
          VAlign = vaCenter
        end
        object Memo10: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 890.771653540000000000
          Top = 72.531541820000000000
          Width = 84.944894230000000000
          Height = 56.413369360000000000
          GroupIndex = 1
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            #1057#1027'.'#1057#8364'. ('#1056#1110#1057#1026#1056#176#1056#1169', '#1056#1112#1056#1105#1056#1029', '#1057#1027')')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo11: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 141.385826770000000000
          Width = 113.385826770000000000
          Height = 128.944881890000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            #1056#1118#1056#1105#1056#1111' '#1056#160#1056#160#1056#1038)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo12: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1306.614197900000000000
          Width = 43.590551180000000000
          Height = 128.944881890000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#167#1056#176#1057#1027#1057#8218#1056#1109#1057#8218#1057#8249' '#1056#1119#1056#160#1056#8221'/'#1056#1119#1056#160#1056#1114', '#1056#1114#1056#8220#1057#8224)
          ParentFont = False
          Rotation = 90
          VAlign = vaCenter
        end
        object Memo13: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 975.716547770000000000
          Top = 72.531541820000000000
          Width = 84.944894230000000000
          Height = 56.413369360000000000
          GroupIndex = 1
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            #1056#1030'.'#1056#1169'. ('#1056#1110#1057#1026#1056#176#1056#1169', '#1056#1112#1056#1105#1056#1029', '#1057#1027')')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo14: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Width = 80.000000000000000000
          Height = 128.944881890000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            #1074#8222#8211' '
            #1056#1111'/'#1056#1111'.')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo20: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1163.842544360000000000
          Width = 51.590551180000000000
          Height = 128.944886770000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1113#1056#1109#1057#1036#1057#8222#1057#8222#1056#1105#1057#8224#1056#1105#1056#181#1056#1029#1057#8218' '#1057#1107#1057#1027#1056#1105#1056#187#1056#181#1056#1029#1056#1105#1057#1039' '#1056#176#1056#1029#1057#8218#1056#181#1056#1029#1056#1029#1057#8249', dBi')
          ParentFont = False
          Rotation = 90
          VAlign = vaCenter
        end
        object Memo21: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1060.661442000000000000
          Width = 51.590551180000000000
          Height = 128.944881890000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            
              #1056#8217#1057#8249#1057#1027#1056#1109#1057#8218#1056#176' '#1056#176#1056#1029#1057#8218#1056#181#1056#1029#1056#1029' '#1056#160#1056#160#1056#1038' '#1056#1109#1057#8218' '#1056#1111#1056#1109#1056#1030#1056#181#1057#1026#1057#8230#1056#1029#1056#1109#1057#1027#1057#8218#1056#1105' '#1056#8212#1056 +
              #181#1056#1112#1056#187#1056#1105', '#1056#1112)
          ParentFont = False
          Rotation = 90
          VAlign = vaCenter
        end
        object Memo4: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1473.385851440000000000
          Width = 35.590551180000000000
          Height = 128.944911180000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1119#1056#1109#1056#187#1057#1039#1057#1026#1056#1105#1056#183#1056#176#1057#8224#1056#1105#1057#1039' '#1056#1119#1056#160#1056#8221'/'#1056#1119#1056#160#1056#1114)
          ParentFont = False
          Rotation = 90
          VAlign = vaCenter
        end
        object Memo2: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1267.023646720000000000
          Width = 39.590551180000000000
          Height = 128.944911180000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1114#1056#1109#1057#8240#1056#1029#1056#1109#1057#1027#1057#8218#1057#1034' '#1056#1111#1056#181#1057#1026#1056#181#1056#1169#1056#176#1057#8218#1057#8225#1056#1105#1056#1108#1056#176', '#1056#8217#1057#8218)
          ParentFont = False
          Rotation = 90
          VAlign = vaCenter
        end
        object Memo6: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1401.795300260000000000
          Width = 71.590551180000000000
          Height = 128.944911180000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1113#1056#187#1056#176#1057#1027#1057#1027
            #1056#1105#1056#183#1056#187#1057#1107#1057#8225#1056#181#1056#1029#1056#1105#1057#1039)
          ParentFont = False
          Rotation = 90
          VAlign = vaCenter
        end
        object Memo37: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 80.000000000000000000
          Width = 61.385826770000000000
          Height = 128.944881890000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1074#8222#8211' '#1056#1038#1057#8218#1056#176#1056#1029#1057#8224#1056#1105#1056#1105' ('#1056#1109#1056#177#1056#1109#1056#183#1056#1029#1056#176#1057#8225#1056#181#1056#1029#1056#1105#1056#181' '#1056#1030' '#1057#1027#1056#181#1057#8218#1056#1105')')
          ParentFont = False
          Rotation = 90
          VAlign = vaCenter
        end
        object Memo44: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1112.251993180000000000
          Width = 51.590551180000000000
          Height = 128.944881890000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            
              #1056#1106#1056#183#1056#1105#1056#1112#1057#1107#1057#8218' '#1056#1110#1056#187#1056#176#1056#1030#1056#1029#1056#1109#1056#1110#1056#1109' '#1056#187#1056#181#1056#1111#1056#181#1057#1027#1057#8218#1056#1108#1056#176' '#1056#1105#1056#183#1056#187#1057#1107#1057#8225#1056#181#1056#1029#1056#1105#1057 +
              #1039' '#1056#176#1056#1029#1057#8218#1056#181#1056#1029#1056#1029#1057#8249', '#1056#1110#1057#1026#1056#176#1056#1169)
          ParentFont = False
          Rotation = 90
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 890.771653540000000000
          Width = 169.889846770000000000
          Height = 72.531489760000000000
          GroupIndex = 1
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            #1056#8220#1056#181#1056#1109#1056#1110#1057#1026#1056#176#1057#8222#1056#1105#1057#8225#1056#181#1057#1027#1056#1108#1056#1105#1056#181' '#1056#1108#1056#1109#1056#1109#1057#1026#1056#1169#1056#1105#1056#1029#1056#176#1057#8218#1057#8249' ('#1056#1038#1056#1113'-42)')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo8: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1215.433095540000000000
          Width = 51.590551180000000000
          Height = 128.944911180000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1119#1056#1109#1057#8218#1056#181#1057#1026#1056#1105' '#1056#1030' '#1056#176#1056#1029#1057#8218#1056#181#1056#1029#1056#1029#1056#1109'-'#1057#8222#1056#1105#1056#1169#1056#181#1057#1026#1056#1029#1056#1109#1056#1112' '#1057#8218#1057#1026#1056#176#1056#1108#1057#8218#1056#181', dB')
          ParentFont = False
          Rotation = 90
          VAlign = vaCenter
        end
      end
    end
  end
  object frxReport_new: TfrxReport
    Version = '6.2.11'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.AllowEdit = False
    PreviewOptions.Buttons = [pbPrint, pbExport, pbZoom, pbTools, pbNavigator, pbExportQuick, pbNoFullScreen]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 41121.613084722200000000
    ReportOptions.LastChange = 43224.645577314810000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      '//------------------------------------------------------------'
      '//function dBm_to_Wt_ (aValue: variant): double;'
      '//------------------------------------------------------------'
      '//begin'
      '//  if aValue = null then'
      '//    Result:=0'
      '//  else'
      '//    Result:=dBm_to_Wt (aValue);           '
      '  '
      '  '
      '//end;'
      '  '
      ''
      '  '
      '  '
      '//------------------------------------------------------------'
      'function DegreeToStr (aValue: double): String;'
      '//------------------------------------------------------------'
      'const'
      '//  STR_BLPOINT_FORMAT1 = '#39'%d'#176'%.2d'#39#39'%1.2f'#39#39#39#39#39';'
      '  STR_BLPOINT_FORMAT1 = '#39'%d %.2d %1.0f'#39';'
      'var'
      ' iDeg,iMin:integer; '
      ' eSec: Double;   '
      '           '
      'begin'
      '  geo_DecodeDegree_(aValue, iDeg,iMin,eSec);'
      '                           '
      ' // angle.Sec:=TruncFloat(angle.Sec,2);'
      ' '
      '    Result:=Format(STR_BLPOINT_FORMAT1, [iDeg, iMin, eSec]);'
      ''
      '  if aValue<0 then'
      '    Result:='#39'-'#39'+Result;'
      'end;'
      '  '
      ''
      ''
      ' //------------------------------------------------------------'
      
        'procedure geo_DecodeDegree_(aValue: Double; var aDeg,aMin:intege' +
        'r; var aSec:'
      '    Double);'
      '//------------------------------------------------------------'
      'var'
      '  bIsNegative: Boolean;'
      '  e: Double;'
      '  eSec: Double;'
      ''
      'begin'
      '  bIsNegative := aValue<0;'
      ''
      '  aValue :=Abs(aValue);'
      ''
      ''
      ''
      '   //    aValue:=99;                            '
      '          '
      ''
      '          '
      '  aDeg:=Trunc(aValue);'
      ''
      '  aMin:=Trunc(Frac(aValue)*60);'
      ''
      ''
      '//  e:= (aValue - Trunc(aValue));'
      '  e:= (aValue - Trunc(aValue)) * 60;'
      ''
      ''
      '  aMin:=Trunc(e);'
      '  aSec:=(((aValue - aDeg) *60) - aMin) * 60;'
      ''
      
        '  //ShowMessage( FloatToStr(aSec));                             ' +
        '                                 '
      '                         '
      '  while aSec>(60-0.1) do'
      '  begin'
      '    aMin:=aMin+1;'
      '    aSec:=aSec-60;'
      '  end;    '
      ''
      '  while aMin>=60 do'
      '  begin'
      '      aMin:=aMin-60;'
      '      aDeg:=aDeg+1;'
      '  end;  '
      ''
      '  if aSec<0 then'
      '     aSec:=0;'
      '  '
      '  '
      '   if (aSec>=60) then'
      
        '     showMessage('#39'(aSec>=60)'#39');                                 ' +
        '                                '
      ''
      '             '
      ''
      '  if bIsNegative then'
      '    aDeg:=-aDeg;'
      ''
      ''
      ''
      'end;'
      '  '
      '                       '
      ''
      '//------------------------------------------------------------'
      
        'procedure geo_DecodeDegree_11111111(aValue: Double; var aDeg,aMi' +
        'n:integer; var aSec:'
      '    Double);'
      '//------------------------------------------------------------'
      'const'
      ' EXP_ = 0.000000001;'
      '     //97,033333333'
      'var'
      '  bIsNegative: Boolean;'
      '  e: Double;'
      '  eMinutes: Double;'
      '  eSec: Double;'
      '  eSeconds: Double;'
      'begin'
      '  bIsNegative := aValue<0;'
      ''
      '  aValue :=Abs(aValue);'
      ''
      '  aDeg:=Trunc(aValue);'
      ''
      ' // e:=Abs(aValue)*60;'
      ''
      '//  , Frac(X) = X - Int(X).'
      ''
      '  aMin:=Trunc(Frac(aValue)*60);'
      ''
      '//  Result.Sec:=Frac(Frac(Abs(aValue))*60)*60;'
      '  e:= (aValue - Trunc(aValue)) * 60;'
      '  aMin:=Trunc(e);'
      ''
      ' eMinutes := Trunc (e);'
      ' eSeconds := (e - Trunc (e)) * 60 ;'
      ''
      ''
      '  aSec:=(((aValue - aDeg) *60) - aMin) * 60;'
      ''
      '  if (aSec - 60) > EXP_ then'
      '  begin'
      '    Inc(aMin);'
      '    aSec := 0;'
      '  end;'
      ''
      ''
      '  if bIsNegative then'
      '    aDeg:=-aDeg;'
      '                   '
      ''
      ''
      'end;'
      '        '
      ''
      '       '
      'begin'
      ''
      'end.')
    Left = 302
    Top = 116
    Datasets = <
      item
        DataSet = frxDBDataset_link
        DataSetName = 'link'
      end>
    Variables = <>
    Style = <
      item
        Name = 'Title'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Height = -16
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        Frame.Typ = []
        Fill.BackColor = clGray
      end
      item
        Name = 'Header'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clMaroon
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        Frame.Typ = []
      end
      item
        Name = 'Group header'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clMaroon
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        Frame.Typ = []
        Fill.BackColor = 16053492
      end
      item
        Name = 'Data'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        Frame.Typ = []
      end
      item
        Name = 'Group footer'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        Frame.Typ = []
      end
      item
        Name = 'Header line'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        Frame.Typ = [ftBottom]
        Frame.Width = 2.000000000000000000
      end>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      Orientation = poLandscape
      PaperWidth = 429.291750000000000000
      PaperHeight = 839.999166670000000000
      PaperSize = 256
      LeftMargin = 10.001250000000000000
      RightMargin = 10.001250000000000000
      TopMargin = 10.001250000000000000
      BottomMargin = 10.001250000000000000
      Frame.Typ = []
      PrintIfEmpty = False
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        Frame.Typ = []
        Height = 86.929190000000000000
        Top = 16.000000000000000000
        Visible = False
        Width = 1546.920999052500000000
        StartNewPage = True
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Left = 8.098992860000000000
          Top = 5.219350950000000000
          Width = 1305.557648570000000000
          Height = 73.970801430000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -19
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8 = (
            
              #1056#1119#1056#160#1056#1115#1056#8226#1056#1113#1056#1118' '#1056#167#1056#1106#1056#1038#1056#1118#1056#1115#1056#1118#1056#1116#1056#1115'-'#1056#1118#1056#8226#1056#160#1056#160#1056#152#1056#1118#1056#1115#1056#160#1056#152#1056#1106#1056#8250#1056#172#1056#1116#1056#1115#1056#8220#1056#1115' '#1056 +
              #1119#1056#8250#1056#1106#1056#1116#1056#1106' '#1056#160#1056#173#1056#1038
            
              #1056#160#1056#1106#1056#8221#1056#152#1056#1115#1056#160#1056#8226#1056#8250#1056#8226#1056#8482#1056#1116#1056#171#1056#1168' '#1056#1038#1056#1118#1056#1106#1056#1116#1056#166#1056#152#1056#8482' '#1056#152' '#1056#1038#1056#8226#1056#1118#1056#8226#1056#8482' '#1056#8216#1056#8226#1056#1038#1056#1119 +
              #1056#160#1056#1115#1056#8217#1056#1115#1056#8221#1056#1116#1056#1115#1056#8220#1056#1115' '#1056#160#1056#1106#1056#8221#1056#152#1056#1115#1056#8221#1056#1115#1056#1038#1056#1118#1056#1032#1056#1119#1056#1106
            
              #1056#1119#1056#1115' '#1056#1038#1056#1168#1056#8226#1056#1114#1056#8226' '#1042#171#1056#1118#1056#1115#1056#167#1056#1113#1056#1106' - '#1056#1118#1056#1115#1056#167#1056#1113#1056#1106#1042#187', '#1056#1038#1056#1118#1056#1106#1056#166#1056#152#1056#1115#1056#1116#1056#1106#1056#160#1056 +
              #1116#1056#171#1056#1168' '#1056#152' '#1056#1119#1056#8226#1056#160#1056#8226#1056#8221#1056#8217#1056#152#1056#8211#1056#1116#1056#171#1056#1168' '#1056#160#1056#8226#1056#1119#1056#1115#1056#160#1056#1118#1056#1106#1056#8211#1056#1116#1056#171#1056#1168' '#1056#1118#1056#8226#1056#8250#1056#8226#1056 +
              #8217#1056#152#1056#8212#1056#152#1056#1115#1056#1116#1056#1116#1056#171#1056#1168' '#1056#1038#1056#1118#1056#1106#1056#1116#1056#166#1056#152#1056#8482)
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 1395.739799052500000000
          Top = 3.779530000000000000
          Width = 151.181200000000000000
          Height = 37.795300000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -19
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8 = (
            #1056#1118#1056#176#1056#177#1056#187#1056#1105#1057#8224#1056#176' '#1056#164#1056#1038'-1')
          ParentFont = False
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Frame.Typ = []
        Height = 22.677165350000000000
        Top = 512.000000000000000000
        Width = 1546.920999052500000000
        Child = frxReport_new.Child3
        DataSet = frxDBDataset_link
        DataSetName = 'link'
        RowCount = 0
        object property1name: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 49.133858267716500000
          Width = 130.000000000000000000
          Height = 22.677165350000000000
          DataSetName = 'property1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[link."Linkend1_property_name"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object property1address: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 309.133858267716500000
          Width = 400.000000000000000000
          Height = 22.677165350000000000
          DataSetName = 'property1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[link."Linkend1_property_address"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Linkend1LinkEndTypename: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 179.133858267716500000
          Width = 130.000000000000000000
          Height = 22.677165350000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd1
          DataSetName = 'LinkEnd1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[link."Linkend1_LinkEndType_name"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Linkend1antenna_height: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1009.133858267717000000
          Width = 50.000000000000000000
          Height = 22.677165350000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd1
          DataSetName = 'LinkEnd1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[link."Linkend1_Antenna_height"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo23: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1159.133858267717000000
          Width = 50.000000000000000000
          Height = 22.677165350000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[link."Linkend1_antenna_loss"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Linkend2antenna_polarization: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1409.133858267717000000
          Width = 50.000000000000000000
          Height = 22.677165350000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[link."Linkend1_antenna_polarization"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo25: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1309.133858267717000000
          Width = 50.000000000000000000
          Height = 22.677165350000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[link."Linkend1_antenna_tilt"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo29: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1109.133858267717000000
          Width = 50.000000000000000000
          Height = 22.677165350000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd1
          DataSetName = 'LinkEnd1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[link."Linkend1_antenna_gain"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo31: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1359.133858267717000000
          Width = 50.000000000000000000
          Height = 22.677165350000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[link."Linkend1_radiation_class"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo41: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Width = 49.133858267716500000
          Height = 22.677165350000000000
          DataSetName = 'property1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo46: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1059.133858267717000000
          Width = 50.000000000000000000
          Height = 22.677165350000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          DisplayFormat.FormatStr = '%2.0f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[link."Linkend1_antenna_azimuth"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Linkend1power_dBm: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1209.133858267717000000
          Width = 50.000000000000000000
          Height = 22.677165350000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd1
          DataSetName = 'LinkEnd1'
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[link."Linkend1_power_W"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo34: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1259.133858267717000000
          Width = 50.000000000000000000
          Height = 22.677165350000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[link."Linkend1_rx_freq_MHz"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo10: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 709.133858267716500000
          Width = 100.000000000000000000
          Height = 22.677165354330700000
          DataSetName = 'property2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo58: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 909.133858267716500000
          Width = 100.000000000000000000
          Height = 22.677165350000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[DegreeToStr(<link."LinkEnd1_property_lon_gck_2011">)]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo59: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 809.133858267716500000
          Width = 100.000000000000000000
          Height = 22.677165350000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[DegreeToStr(<link."LinkEnd1_property_lat_gck_2011">)]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object Header1: TfrxHeader
        FillType = ftBrush
        Frame.Typ = []
        Height = 200.000000000000000000
        Top = 164.000000000000000000
        Width = 1546.920999052500000000
        Child = frxReport_new.Child1
        object Memo3: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 309.133858270000000000
          Width = 500.000000000000000000
          Height = 30.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            #1056#1114#1056#181#1057#1027#1057#8218#1056#1109' '#1057#1107#1057#1027#1057#8218#1056#176#1056#1029#1056#1109#1056#1030#1056#1108#1056#1105' '#1056#160#1056#173#1056#1038)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo7: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1309.133858270000000000
          Width = 50.000000000000000000
          Height = 200.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            
              #1056#1032#1056#1110#1056#1109#1056#187' '#1056#1112#1056#181#1057#1027#1057#8218#1056#176' '#1056#1110#1056#187#1056#176#1056#1030#1056#1029#1056#1109#1056#1110#1056#1109' '#1056#187#1056#181#1056#1111#1056#181#1057#1027#1057#8218#1056#1108#1056#176' '#1056#1105#1056#183#1056#187#1057#1107#1057#8225 +
              #1056#181#1056#1029#1056#1105#1057#1039', '#1056#1110#1057#1026#1056#176#1056#1169)
          ParentFont = False
          Rotation = 90
          VAlign = vaCenter
        end
        object Memo11: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 179.133858270000000000
          Width = 130.000000000000000000
          Height = 200.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            #1056#1116#1056#176#1056#1105#1056#1112#1056#181#1056#1029#1056#1109#1056#1030#1056#176#1056#1029#1056#1105#1056#181' '#1056#160#1056#173#1056#1038)
          ParentFont = False
          Rotation = 90
          VAlign = vaCenter
        end
        object Memo12: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1259.133858270000000000
          Width = 50.000000000000000000
          Height = 200.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#167#1056#176#1057#1027#1057#8218#1056#1109#1057#8218#1057#8249' '#1056#1119#1056#160#1056#8221'/'#1056#1119#1056#160#1056#1114', '#1056#1114#1056#8220#1057#8224)
          ParentFont = False
          Rotation = 90
          VAlign = vaCenter
        end
        object Memo14: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Width = 49.133858270000000000
          Height = 200.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            #1074#8222#8211' '
            #1056#1111'/'#1056#1111'.')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo20: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1109.133858270000000000
          Width = 50.000000000000000000
          Height = 200.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1113#1056#1109#1057#1036#1057#8222#1057#8222#1056#1105#1057#8224#1056#1105#1056#181#1056#1029#1057#8218' '#1057#1107#1057#1027#1056#1105#1056#187#1056#181#1056#1029#1056#1105#1057#1039' '#1056#176#1056#1029#1057#8218#1056#181#1056#1029#1056#1029#1057#8249', dBi')
          ParentFont = False
          Rotation = 90
          VAlign = vaCenter
        end
        object Memo21: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1009.133858270000000000
          Width = 50.000000000000000000
          Height = 200.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            
              #1056#8217#1057#8249#1057#1027#1056#1109#1057#8218#1056#176' '#1056#176#1056#1029#1057#8218#1056#181#1056#1029#1056#1029' '#1056#160#1056#160#1056#1038' '#1056#1109#1057#8218' '#1056#1111#1056#1109#1056#1030#1056#181#1057#1026#1057#8230#1056#1029#1056#1109#1057#1027#1057#8218#1056#1105' '#1056#8212#1056 +
              #181#1056#1112#1056#187#1056#1105', '#1056#1112)
          ParentFont = False
          Rotation = 90
          VAlign = vaCenter
        end
        object Memo4: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1409.133858270000000000
          Width = 50.000000000000000000
          Height = 200.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1119#1056#1109#1056#187#1057#1039#1057#1026#1056#1105#1056#183#1056#176#1057#8224#1056#1105#1057#1039' '#1056#1119#1056#160#1056#8221'/'#1056#1119#1056#160#1056#1114)
          ParentFont = False
          Rotation = 90
          VAlign = vaCenter
        end
        object Memo2: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1209.133858270000000000
          Width = 50.000000000000000000
          Height = 200.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1114#1056#1109#1057#8240#1056#1029#1056#1109#1057#1027#1057#8218#1057#1034' '#1056#1111#1056#181#1057#1026#1056#181#1056#1169#1056#176#1057#8218#1057#8225#1056#1105#1056#1108#1056#176', '#1056#8217#1057#8218)
          ParentFont = False
          Rotation = 90
          VAlign = vaCenter
        end
        object Memo6: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1359.133858270000000000
          Width = 50.000000000000000000
          Height = 200.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1113#1056#187#1056#176#1057#1027#1057#1027
            #1056#1105#1056#183#1056#187#1057#1107#1057#8225#1056#181#1056#1029#1056#1105#1057#1039)
          ParentFont = False
          Rotation = 90
          VAlign = vaCenter
        end
        object Memo37: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 49.133858270000000000
          Width = 130.000000000000000000
          Height = 200.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            #1074#8222#8211' '#1056#1038#1057#8218#1056#176#1056#1029#1057#8224#1056#1105#1056#1105' ('#1056#1109#1056#177#1056#1109#1056#183#1056#1029#1056#176#1057#8225#1056#181#1056#1029#1056#1105#1056#181' '#1056#1030' '#1057#1027#1056#181#1057#8218#1056#1105')')
          ParentFont = False
          Rotation = 90
          VAlign = vaCenter
        end
        object Memo44: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1059.133858270000000000
          Width = 50.000000000000000000
          Height = 200.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            
              #1056#1106#1056#183#1056#1105#1056#1112#1057#1107#1057#8218' '#1056#1110#1056#187#1056#176#1056#1030#1056#1029#1056#1109#1056#1110#1056#1109' '#1056#187#1056#181#1056#1111#1056#181#1057#1027#1057#8218#1056#1108#1056#176' '#1056#1105#1056#183#1056#187#1057#1107#1057#8225#1056#181#1056#1029#1056#1105#1057 +
              #1039' '#1056#176#1056#1029#1057#8218#1056#181#1056#1029#1056#1029#1057#8249', '#1056#1110#1057#1026#1056#176#1056#1169)
          ParentFont = False
          Rotation = 90
          VAlign = vaCenter
        end
        object Memo8: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1159.133858270000000000
          Width = 50.000000000000000000
          Height = 200.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1119#1056#1109#1057#8218#1056#181#1057#1026#1056#1105' '#1056#1030' '#1056#176#1056#1029#1057#8218#1056#181#1056#1029#1056#1029#1056#1109'-'#1057#8222#1056#1105#1056#1169#1056#181#1057#1026#1056#1029#1056#1109#1056#1112' '#1057#8218#1057#1026#1056#176#1056#1108#1057#8218#1056#181', dB')
          ParentFont = False
          Rotation = 90
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 309.133858270000000000
          Top = 30.236240000000000000
          Width = 400.000000000000000000
          Height = 170.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            #1056#1106#1056#1169#1057#1026#1056#181#1057#1027' '#1057#1107#1057#1027#1057#8218#1056#176#1056#1029#1056#1109#1056#1030#1056#1108#1056#1105)
          ParentFont = False
          Rotation = 90
          VAlign = vaCenter
        end
        object Memo19: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 709.133858270000000000
          Top = 30.236240000000000000
          Width = 100.000000000000000000
          Height = 170.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            
              #1056#8221#1056#1109#1056#1111#1056#1109#1056#187#1056#1029#1056#1105#1057#8218#1056#181#1056#187#1057#1034#1056#1029#1056#176#1057#1039' '#1056#1105#1056#1029#1057#8222#1056#1109#1057#1026#1056#1112#1056#176#1057#8224#1056#1105#1057#1039' '#1056#1109' '#1056#1112#1056#181#1057#1027#1057#8218#1056#181' ' +
              #1057#1107#1057#1027#1057#8218#1056#176#1056#1029#1056#1109#1056#1030#1056#1108#1056#1105' '#1056#160#1056#173#1056#1038)
          ParentFont = False
          Rotation = 90
          VAlign = vaCenter
        end
        object Memo69: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 909.133858270000000000
          Width = 100.000000000000000000
          Height = 200.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#8221#1056#1109#1056#187#1056#1110#1056#1109#1057#8218#1056#176','
            #1056#1030'.'#1056#1169'.   ('#1056#8220'CK-2011)')
          ParentFont = False
          Rotation = 90
          VAlign = vaCenter
        end
        object Memo73: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 809.133858270000000000
          Width = 100.000000000000000000
          Height = 200.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1025#1056#1105#1057#1026#1056#1109#1057#8218#1056#176','
            #1057#1027'.'#1057#8364'.  ('#1056#8220'CK-2011)')
          ParentFont = False
          Rotation = 90
          VAlign = vaCenter
        end
      end
      object Child1: TfrxChild
        FillType = ftBrush
        Frame.Typ = []
        Height = 42.015691890000000000
        Top = 384.000000000000000000
        Width = 1546.920999052500000000
        Child = frxReport_new.Child2
        ToNRows = 0
        ToNRowsMode = rmCount
        object Memo24: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Width = 49.133858267716500000
          Height = 42.015691890000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo26: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 49.133858267716500000
          Width = 130.000000000000000000
          Height = 42.015691890000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo28: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 179.133858267716500000
          Width = 130.000000000000000000
          Height = 42.015691890000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo32: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 309.133858267716500000
          Width = 400.000000000000000000
          Height = 42.015691890000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo36: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 709.133858267716500000
          Width = 100.000000000000000000
          Height = 42.015691890000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo52: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1009.133858267717000000
          Width = 50.000000000000000000
          Height = 42.015691890000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            #1056#1112)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo53: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1059.133858267717000000
          Width = 50.000000000000000000
          Height = 42.015691890000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            #1056#1110#1057#1026#1056#176#1056#1169'.')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo54: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1109.133858267717000000
          Width = 50.000000000000000000
          Height = 42.015691890000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            #1056#1169#1056#8216#1056#1105)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo55: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1159.133858267717000000
          Width = 50.000000000000000000
          Height = 42.015691890000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            #1056#1169#1056#8216)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo56: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1209.133858267717000000
          Width = 50.000000000000000000
          Height = 42.015691890000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            #1056#8217#1057#8218)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo57: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1259.133858267717000000
          Width = 50.000000000000000000
          Height = 42.015691890000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            #1056#1114#1056#8220#1057#8224)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo63: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1309.133858267717000000
          Width = 50.000000000000000000
          Height = 42.015691890000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            #1056#1110#1057#1026#1056#176#1056#1169'.')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo64: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1359.133858267717000000
          Width = 50.000000000000000000
          Height = 42.015691890000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo65: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1409.133858267717000000
          Width = 50.000000000000000000
          Height = 42.015691890000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo74: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 809.133858267716500000
          Width = 100.000000000000000000
          Height = 42.015691890000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo75: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 909.133858267716500000
          Width = 100.000000000000000000
          Height = 42.015691890000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object Child2: TfrxChild
        FillType = ftBrush
        Frame.Typ = []
        Height = 42.015691890000000000
        Top = 448.000000000000000000
        Width = 1546.920999052500000000
        ToNRows = 0
        ToNRowsMode = rmCount
        object Memo39: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Width = 49.133858267716500000
          Height = 42.015691890000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '1')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo40: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 49.133858267716500000
          Width = 130.000000000000000000
          Height = 42.015691890000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '2')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo45: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 179.133858267716500000
          Width = 130.000000000000000000
          Height = 42.015691890000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '3')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo49: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 309.133858267716500000
          Width = 400.000000000000000000
          Height = 42.015691890000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '4')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo50: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 709.133858267716500000
          Width = 100.000000000000000000
          Height = 42.015691890000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '4')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo60: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1009.133858267717000000
          Width = 50.000000000000000000
          Height = 42.015691890000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '7')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo61: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1059.133858267717000000
          Width = 50.000000000000000000
          Height = 42.015691890000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '8')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo62: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1109.133858267717000000
          Width = 50.000000000000000000
          Height = 42.015691890000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '9')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo66: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1159.133858267717000000
          Width = 50.000000000000000000
          Height = 42.015691890000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '10')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo67: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1209.133858267717000000
          Width = 50.000000000000000000
          Height = 42.015691890000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '11')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo68: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1259.133858267717000000
          Width = 50.000000000000000000
          Height = 42.015691890000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '12')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo70: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1309.133858267717000000
          Width = 50.000000000000000000
          Height = 42.015691890000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '13')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo71: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1359.133858267717000000
          Width = 50.000000000000000000
          Height = 42.015691890000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '14')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo72: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1409.133858267717000000
          Width = 50.000000000000000000
          Height = 42.015691890000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '15')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo76: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 809.133858267716500000
          Width = 100.000000000000000000
          Height = 42.015691890000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '5')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo77: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 909.133858267716500000
          Width = 100.000000000000000000
          Height = 42.015691890000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '6')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object Child3: TfrxChild
        FillType = ftBrush
        Frame.Typ = []
        Height = 22.677165350000000000
        Top = 556.000000000000000000
        Width = 1546.920999052500000000
        ToNRows = 0
        ToNRowsMode = rmCount
        object property2name: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 49.133858267716500000
          Width = 130.000000000000000000
          Height = 22.677165350000000000
          DataSetName = 'property2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[link."Linkend2_property_name"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object property2address: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 309.133858267716500000
          Width = 400.000000000000000000
          Height = 22.677165350000000000
          DataSetName = 'property2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[link."Linkend2_property_address"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo17: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 179.133858267716500000
          Width = 130.000000000000000000
          Height = 22.677165350000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[link."Linkend2_LinkEndType_name"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo22: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1009.133858267717000000
          Width = 50.000000000000000000
          Height = 22.677165350000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd1
          DataSetName = 'LinkEnd1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[link."Linkend2_antenna_height"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo18: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1409.133858267717000000
          Width = 50.000000000000000000
          Height = 22.677165350000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[link."Linkend2_antenna_polarization"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo27: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1309.133858267717000000
          Width = 50.000000000000000000
          Height = 22.677165350000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[link."Linkend2_antenna_tilt"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo30: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1109.133858267717000000
          Width = 50.000000000000000000
          Height = 22.677165350000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd1
          DataSetName = 'LinkEnd1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[link."Linkend2_antenna_gain"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Linkend2radiation_class: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1359.133858267717000000
          Width = 50.000000000000000000
          Height = 22.677165350000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[link."Linkend2_radiation_class"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo42: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Width = 49.133858267716500000
          Height = 22.677165350000000000
          DataSetName = 'property1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo47: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1059.133858267717000000
          Width = 50.000000000000000000
          Height = 22.677165350000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          DisplayFormat.FormatStr = '%2.0f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[link."Linkend2_antenna_azimuth"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo9: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1159.133858267717000000
          Width = 50.000000000000000000
          Height = 22.677165350000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[link."Linkend2_antenna_loss"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo33: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1209.133858267717000000
          Width = 50.000000000000000000
          Height = 22.677165350000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd1
          DataSetName = 'LinkEnd1'
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[link."Linkend2_power_W"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo35: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1259.133858267717000000
          Width = 50.000000000000000000
          Height = 22.677165350000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[link."Linkend2_rx_freq_MHz"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo13: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 709.133858267716500000
          Width = 100.000000000000000000
          Height = 22.677165354330700000
          DataSetName = 'property2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo78: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 909.133858267716500000
          Width = 100.000000000000000000
          Height = 22.677165350000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[DegreeToStr(<link."LinkEnd2_property_lon_gck_2011">)]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo79: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 809.133858267716500000
          Width = 100.000000000000000000
          Height = 22.677165350000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[DegreeToStr(<link."LinkEnd2_property_lat_gck_2011">)]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
    end
  end
  object frxReport_MTS_new: TfrxReport
    Version = '6.2.11'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.AllowEdit = False
    PreviewOptions.Buttons = [pbPrint, pbExport, pbZoom, pbTools, pbNavigator, pbExportQuick, pbNoFullScreen]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 41121.613084722200000000
    ReportOptions.LastChange = 43212.534449386600000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      '//------------------------------------------------------------'
      '//function dBm_to_Wt_ (aValue: variant): double;'
      '//------------------------------------------------------------'
      '//begin'
      '//  if aValue = null then'
      '//    Result:=0'
      '//  else'
      '//    Result:=dBm_to_Wt (aValue);           '
      '  '
      '  '
      '//end;'
      '  '
      '   '
      ''
      '            '
      '  '
      '//------------------------------------------------------------'
      'function DegreeToStr (aValue: double): String;'
      '//------------------------------------------------------------'
      'const'
      '//  STR_BLPOINT_FORMAT1 = '#39'%d'#176'%.2d'#39#39'%1.2f'#39#39#39#39#39';'
      '  STR_BLPOINT_FORMAT1 = '#39'%d %.2d %1.0f'#39';'
      'var'
      ' iDeg,iMin:integer; '
      ' eSec: Double;   '
      '           '
      'begin'
      '  geo_DecodeDegree_(aValue, iDeg,iMin,eSec);'
      '                           '
      ' // angle.Sec:=TruncFloat(angle.Sec,2);'
      ' '
      '    Result:=Format(STR_BLPOINT_FORMAT1, [iDeg, iMin, eSec]);'
      ''
      '  if aValue<0 then'
      '    Result:='#39'-'#39'+Result;'
      'end;'
      '  '
      ''
      '//------------------------------------------------------------'
      'function ChangeName (aValue: String): String;'
      '//------------------------------------------------------------'
      'begin   '
      '  Result:=Copy(aValue, 1, 2) + Copy(aValue, 5, 4)'
      'end;  '
      ''
      '                    '
      ''
      ' //------------------------------------------------------------'
      
        'procedure geo_DecodeDegree_(aValue: Double; var aDeg,aMin:intege' +
        'r; var aSec:'
      '    Double);'
      '//------------------------------------------------------------'
      'var'
      '  bIsNegative: Boolean;'
      '  e: Double;'
      '  eSec: Double;'
      ''
      'begin'
      '  bIsNegative := aValue<0;'
      ''
      '  aValue :=Abs(aValue);'
      ''
      ''
      ''
      '   //    aValue:=99;                            '
      '          '
      ''
      '          '
      '  aDeg:=Trunc(aValue);'
      ''
      '  aMin:=Trunc(Frac(aValue)*60);'
      ''
      ''
      '//  e:= (aValue - Trunc(aValue));'
      '  e:= (aValue - Trunc(aValue)) * 60;'
      ''
      ''
      '  aMin:=Trunc(e);'
      '  aSec:=(((aValue - aDeg) *60) - aMin) * 60;'
      ''
      
        '  //ShowMessage( FloatToStr(aSec));                             ' +
        '                                 '
      '                         '
      '  while aSec>(60-0.1) do'
      '  begin'
      '    aMin:=aMin+1;'
      '    aSec:=aSec-60;'
      '  end;    '
      ''
      '  while aMin>=60 do'
      '  begin'
      '      aMin:=aMin-60;'
      '      aDeg:=aDeg+1;'
      '  end;  '
      ''
      '  if aSec<0 then'
      '     aSec:=0;'
      '  '
      '  '
      '   if (aSec>=60) then'
      
        '     showMessage('#39'(aSec>=60)'#39');                                 ' +
        '                                '
      ''
      '             '
      ''
      '  if bIsNegative then'
      '    aDeg:=-aDeg;'
      ''
      ''
      ''
      'end;'
      '  '
      '                       '
      ''
      '//------------------------------------------------------------'
      
        'procedure geo_DecodeDegree_11111111(aValue: Double; var aDeg,aMi' +
        'n:integer; var aSec:'
      '    Double);'
      '//------------------------------------------------------------'
      'const'
      ' EXP_ = 0.000000001;'
      '     //97,033333333'
      'var'
      '  bIsNegative: Boolean;'
      '  e: Double;'
      '  eMinutes: Double;'
      '  eSec: Double;'
      '  eSeconds: Double;'
      'begin'
      '  bIsNegative := aValue<0;'
      ''
      '  aValue :=Abs(aValue);'
      ''
      '  aDeg:=Trunc(aValue);'
      ''
      ' // e:=Abs(aValue)*60;'
      ''
      '//  , Frac(X) = X - Int(X).'
      ''
      '  aMin:=Trunc(Frac(aValue)*60);'
      ''
      '//  Result.Sec:=Frac(Frac(Abs(aValue))*60)*60;'
      '  e:= (aValue - Trunc(aValue)) * 60;'
      '  aMin:=Trunc(e);'
      ''
      ' eMinutes := Trunc (e);'
      ' eSeconds := (e - Trunc (e)) * 60 ;'
      ''
      ''
      '  aSec:=(((aValue - aDeg) *60) - aMin) * 60;'
      ''
      '  if (aSec - 60) > EXP_ then'
      '  begin'
      '    Inc(aMin);'
      '    aSec := 0;'
      '  end;'
      ''
      ''
      '  if bIsNegative then'
      '    aDeg:=-aDeg;'
      '                   '
      ''
      ''
      'end;'
      '        '
      ''
      '       '
      'begin'
      ''
      'end.')
    Left = 304
    Top = 180
    Datasets = <
      item
        DataSet = frxDBDataset_link
        DataSetName = 'link'
      end>
    Variables = <>
    Style = <
      item
        Name = 'Title'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Height = -16
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        Frame.Typ = []
        Fill.BackColor = clGray
      end
      item
        Name = 'Header'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clMaroon
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        Frame.Typ = []
      end
      item
        Name = 'Group header'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clMaroon
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        Frame.Typ = []
        Fill.BackColor = 16053492
      end
      item
        Name = 'Data'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        Frame.Typ = []
      end
      item
        Name = 'Group footer'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        Frame.Typ = []
      end
      item
        Name = 'Header line'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        Frame.Typ = [ftBottom]
        Frame.Width = 2.000000000000000000
      end>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      Orientation = poLandscape
      PaperWidth = 693.875083330000000000
      PaperHeight = 839.999166670000000000
      PaperSize = 256
      LeftMargin = 10.001250000000000000
      RightMargin = 10.001250000000000000
      TopMargin = 10.001250000000000000
      BottomMargin = 10.001250000000000000
      Frame.Typ = []
      PrintIfEmpty = False
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        Frame.Typ = []
        Height = 55.000000000000000000
        Top = 16.000000000000000000
        Visible = False
        Width = 2546.921644873235000000
        StartNewPage = True
        Stretched = True
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Frame.Typ = []
        Height = 22.677165350000000000
        Top = 252.000000000000000000
        Width = 2546.921644873235000000
        Child = frxReport_MTS_new.Child3
        DataSet = frxDBDataset_link
        DataSetName = 'link'
        RowCount = 0
        Stretched = True
        object property1name: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Width = 250.000000000000000000
          Height = 22.677165350000000000
          DataSetName = 'property1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[link."Linkend1_name"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Linkend1LinkEndTypename: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 250.000000000000000000
          Width = 130.000000000000000000
          Height = 22.677165350000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd1
          DataSetName = 'LinkEnd1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[link."Linkend1_property_name"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Linkend1antenna_height: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 950.000000000000000000
          Width = 90.000000000000000000
          Height = 22.677165350000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd1
          DataSetName = 'LinkEnd1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[link."LinkEnd1_Antenna_height"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo23: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1160.000000000000000000
          Width = 60.000000000000000000
          Height = 22.677165350000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[link."LinkEnd1_antenna_diameter"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo25: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1570.000000000000000000
          Width = 70.000000000000000000
          Height = 22.677165350000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[link."Linkend1_Bandwidth"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo29: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1100.000000000000000000
          Width = 60.000000000000000000
          Height = 22.677165350000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd1
          DataSetName = 'LinkEnd1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[link."LinkEnd1_antenna_tilt"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo46: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1040.000000000000000000
          Width = 60.000000000000000000
          Height = 22.677165350000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          DisplayFormat.FormatStr = '%2.0f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[link."LinkEnd1_antenna_azimuth"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Linkend1power_dBm: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1220.000000000000000000
          Width = 120.000000000000000000
          Height = 22.677165350000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd1
          DataSetName = 'LinkEnd1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[link."LinkEnd1_linkendtype_name"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo34: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1410.000000000000000000
          Width = 70.000000000000000000
          Height = 22.677165350000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[link."LinkEnd1_rx_freq_MHz"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo73: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 510.000000000000000000
          Width = 100.000000000000000000
          Height = 22.677165350000000000
          DataSetName = 'property2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[DegreeToStr(<link."Linkend1_property_lat_wgs">) +  '#39#1057#1027#1057#8364#39']')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo1: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 380.000000000000000000
          Width = 130.000000000000000000
          Height = 22.677165350000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd1
          DataSetName = 'LinkEnd1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[link."Linkend1_property_code"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo13: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 610.000000000000000000
          Width = 100.000000000000000000
          Height = 22.677165350000000000
          DataSetName = 'property2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[DegreeToStr(<link."Linkend1_property_lon_wgs">)]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo19: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 710.000000000000000000
          Width = 120.000000000000000000
          Height = 22.677165350000000000
          DataSetName = 'property2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[link."Linkend1_property_lat_wgs"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo24: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 830.000000000000000000
          Width = 120.000000000000000000
          Height = 22.677165350000000000
          DataSetName = 'property2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[link."Linkend1_property_lon_wgs"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1340.000000000000000000
          Width = 70.000000000000000000
          Height = 22.677165350000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[link."LinkEnd1_band"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1480.000000000000000000
          Width = 90.000000000000000000
          Height = 22.677165350000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[link."LinkEnd1_antenna_polarization"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo39: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1640.000000000000000000
          Width = 70.000000000000000000
          Height = 22.677165350000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[link."Linkend1_LinkID"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo42: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1710.000000000000000000
          Width = 80.000000000000000000
          Height = 22.677165350000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[link."Linkend2_modulation_type"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo49: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1790.000000000000000000
          Width = 80.000000000000000000
          Height = 22.677165350000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[link."Linkend1_Property_ground_height"]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object Header1: TfrxHeader
        FillType = ftBrush
        Frame.Typ = []
        Height = 100.000000000000000000
        Top = 132.000000000000000000
        Width = 2546.921644873235000000
        object Memo11: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 250.000000000000000000
          Width = 130.000000000000000000
          Height = 100.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            #1056#1111#1056#187#1056#1109#1057#8240#1056#176#1056#1169#1056#1108#1056#176)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo12: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1410.000000000000000000
          Width = 70.000000000000000000
          Height = 100.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            #1056#167#1056#176#1057#1027#1057#8218#1056#1109#1057#8218#1057#8249' '#1056#1119#1056#160#1056#8221', '#1056#1114#1056#8220#1057#8224)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo20: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1100.000000000000000000
          Width = 60.000000000000000000
          Height = 100.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            #1057#1107#1056#1110#1056#1109#1056#187' '#1056#1112#1056#181#1057#1027#1057#8218#1056#176)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo21: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 950.000000000000000000
          Width = 90.000000000000000000
          Height = 100.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            #1056#1030#1057#8249#1057#1027#1056#1109#1057#8218#1056#176' '#1056#1111#1056#1109#1056#1169#1056#1030#1056#181#1057#1027#1056#176',m')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo2: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1220.000000000000000000
          Width = 120.000000000000000000
          Height = 100.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            #1057#8218#1056#1105#1056#1111' '#1056#1109#1056#177#1056#1109#1057#1026#1057#1107#1056#1169#1056#1109#1056#1030#1056#176#1056#1029#1056#1105#1057#1039)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo37: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Width = 250.000000000000000000
          Height = 100.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            #1056#1116#1056#176#1056#183#1056#1030#1056#176#1056#1029#1056#1105#1056#181' '#1056#187#1056#1105#1056#1029#1056#1108#1056#176)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo44: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1040.000000000000000000
          Width = 60.000000000000000000
          Height = 100.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            #1056#1106#1056#183#1056#1105#1056#1112#1057#1107#1057#8218)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo8: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1160.000000000000000000
          Width = 60.000000000000000000
          Height = 100.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            #1056#1169#1056#1105#1056#176#1056#1112#1056#181#1057#8218#1057#1026' '#1056#176#1056#1029#1057#8218#1056#181#1056#1029#1057#8249)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo74: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 510.000000000000000000
          Width = 100.000000000000000000
          Height = 100.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            #1056#1108#1056#1109#1056#1109#1057#1026#1056#1169#1056#1105#1056#1029#1056#176#1057#8218#1056#176' WGS')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo3: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 380.000000000000000000
          Width = 130.000000000000000000
          Height = 100.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            #1074#8222#8211' '#1056#1111#1056#187#1056#1109#1057#8240#1056#176#1056#1169#1056#1108#1056#1105' ('#1056#1113#1056#1109#1056#1169')')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo10: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 610.000000000000000000
          Width = 100.000000000000000000
          Height = 100.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            #1056#1108#1056#1109#1056#1109#1057#1026#1056#1169#1056#1105#1056#1029#1056#176#1057#8218#1056#176' WGS')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo26: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 710.000000000000000000
          Width = 120.000000000000000000
          Height = 100.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            #1057#8364#1056#1105#1057#1026#1056#1109#1057#8218#1056#176)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo28: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 830.000000000000000000
          Width = 120.000000000000000000
          Height = 100.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            #1056#1169#1056#1109#1056#187#1056#1110#1056#1109#1057#8218#1056#176)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo6: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1570.000000000000000000
          Width = 70.000000000000000000
          Height = 100.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            #1056#1025#1056#1105#1057#1026#1056#1105#1056#1029#1056#176' '#1056#1111#1056#1109#1056#187#1056#1109#1057#1027#1057#8249' MHz')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo7: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1340.000000000000000000
          Width = 70.000000000000000000
          Height = 100.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            #1056#1169#1056#1105#1056#176#1056#1111#1056#176#1056#183#1056#1109#1056#1029' GHz')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo18: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1480.000000000000000000
          Width = 90.000000000000000000
          Height = 100.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            #1056#1111#1056#1109#1056#187#1057#1039#1057#1026#1056#1105#1056#183#1056#176#1057#8224#1056#1105#1057#1039)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo40: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1640.000000000000000000
          Width = 70.000000000000000000
          Height = 100.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            'Link ID')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo45: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1710.000000000000000000
          Width = 80.000000000000000000
          Height = 100.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            #1056#1114#1056#1109#1056#1169#1057#1107#1056#187#1057#1039#1057#8224#1056#1105#1057#1039)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo50: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1790.000000000000000000
          Width = 80.000000000000000000
          Height = 100.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            #1056#1030#1057#8249#1057#1027#1056#176#1057#8218#1056#176' '#1056#1029#1056#176#1056#1169' '#1057#1107#1057#1026#1056#1109#1056#1030#1056#1029#1056#181#1056#1112' '#1056#1112#1056#1109#1057#1026#1057#1039)
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object Child3: TfrxChild
        FillType = ftBrush
        Frame.Typ = []
        Height = 46.677165350000000000
        Top = 296.000000000000000000
        Width = 2546.921644873235000000
        Stretched = True
        ToNRows = 0
        ToNRowsMode = rmCount
        object property2name: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Width = 250.000000000000000000
          Height = 22.677165350000000000
          DataSetName = 'property2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[link."Linkend2_name"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo17: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 250.000000000000000000
          Width = 130.000000000000000000
          Height = 22.677165350000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[link."Linkend1_property_name"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo22: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 950.000000000000000000
          Width = 90.000000000000000000
          Height = 22.677165350000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd1
          DataSetName = 'LinkEnd1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[link."LinkEnd2_antenna_height"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo27: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1570.000000000000000000
          Width = 70.000000000000000000
          Height = 22.677165350000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[link."Linkend2_Bandwidth"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo30: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1100.000000000000000000
          Width = 60.000000000000000000
          Height = 22.677165350000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd1
          DataSetName = 'LinkEnd1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[link."LinkEnd2_antenna_diameter"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo47: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1040.000000000000000000
          Width = 60.000000000000000000
          Height = 22.677165350000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          DisplayFormat.FormatStr = '%2.0f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[link."LinkEnd2_antenna_azimuth"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo9: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1160.000000000000000000
          Width = 60.000000000000000000
          Height = 22.677165350000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[link."LinkEnd2_antenna_loss"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo33: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1220.000000000000000000
          Width = 120.000000000000000000
          Height = 22.677165350000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd1
          DataSetName = 'LinkEnd1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[link."LinkEnd2_linkendtype_name"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo35: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1410.000000000000000000
          Width = 70.000000000000000000
          Height = 22.677165350000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[link."LinkEnd2_rx_freq_MHz"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo77: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 510.000000000000000000
          Width = 100.000000000000000000
          Height = 22.677165350000000000
          DataSetName = 'property2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[DegreeToStr(  <link."Linkend2_property_lat_wgs">)]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo14: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 380.000000000000000000
          Width = 130.000000000000000000
          Height = 22.677165350000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[link."Linkend1_property_code"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 610.000000000000000000
          Width = 100.000000000000000000
          Height = 22.677165350000000000
          DataSetName = 'property2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[DegreeToStr(<link."Linkend2_property_lon_wgs">)]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo32: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 710.000000000000000000
          Width = 120.000000000000000000
          Height = 22.677165350000000000
          DataSetName = 'property2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[link."Linkend2_property_lat_wgs"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo36: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 830.000000000000000000
          Width = 120.000000000000000000
          Height = 22.677165350000000000
          DataSetName = 'property2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[link."Linkend2_property_lon_wgs"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo31: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1340.000000000000000000
          Width = 70.000000000000000000
          Height = 22.677165350000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[link."LinkEnd2_band"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo38: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1480.000000000000000000
          Width = 90.000000000000000000
          Height = 22.677165350000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[link."LinkEnd2_antenna_polarization"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo41: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1640.000000000000000000
          Width = 70.000000000000000000
          Height = 22.677165350000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[link."Linkend2_LinkID"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1710.000000000000000000
          Width = 80.000000000000000000
          Height = 22.677165350000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[link."Linkend2_modulation_type"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo51: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 1790.000000000000000000
          Width = 80.000000000000000000
          Height = 22.677165350000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[link."Linkend2_Property_ground_height"]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
    end
  end
  object ADOConnection1: TADOConnection
    ConnectionString = 
      'Provider=SQLOLEDB.1;Password=sa;Persist Security Info=True;User ' +
      'ID=sa;Initial Catalog=onega_link;Data Source=server1'
    LoginPrompt = False
    Provider = 'SQLOLEDB.1'
    Left = 40
    Top = 16
  end
end
