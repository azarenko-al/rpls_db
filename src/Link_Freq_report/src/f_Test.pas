unit f_Test;

interface

uses
  Classes, Controls, Forms,
  DBGrids, ComCtrls, Grids, frxClass, frxDBSet, DB, ADODB, frxDesgn,
  frxExportPDF, frxExportXLS, frxExportRTF;

type
  Tfrm_Test = class(TForm)
    DBGrid8: TDBGrid;
    DBGrid7: TDBGrid;
    DBGrid1: TDBGrid;
    ADOConnection_MDB: TADOConnection;
    t_Property1: TADOTable;
    t_LinkEnd1: TADOTable;
    t_Property2: TADOTable;
    t_LinkEnd2: TADOTable;
    t_Antennas1111: TADOTable;
    t_Antennas2222: TADOTable;
    t_Link: TADOTable;
    ds_property1: TDataSource;
    ds_LInks: TDataSource;
    ds_property2: TDataSource;
    ds_Linkend1: TDataSource;
    ds_Linkend2: TDataSource;
    frxReport: TfrxReport;
    frxReport_tele2: TfrxReport;
    frxReport_Tele2_CK42: TfrxReport;
    frxReport_new: TfrxReport;
    frxReport_MTS_new: TfrxReport;
    frxDBDataset_link: TfrxDBDataset;
    frxDBDataset_property1: TfrxDBDataset;
    frxDBDataset_property2: TfrxDBDataset;
    frxDBDataset_q_Linkend1: TfrxDBDataset;
    frxDBDataset_q_Linkend2: TfrxDBDataset;
    DBGrid2: TDBGrid;
    DBGrid3: TDBGrid;
    frxRTFExport1: TfrxRTFExport;
    frxXLSExport1: TfrxXLSExport;
    frxPDFExport1: TfrxPDFExport;
    frxDesigner1: TfrxDesigner;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    class procedure ExecDlg;
    { Public declarations }
  end;

var
  frm_Test: Tfrm_Test;

implementation





{$R *.dfm}

procedure Tfrm_Test.FormCreate(Sender: TObject);
begin

end;



//--------------------------------------------------------------------
class procedure Tfrm_Test.ExecDlg;
//--------------------------------------------------------------------
begin
  with Tfrm_Test.Create(Application) do
  try
    ShowModal;

  finally
    Free;
  end;

//  if Result then


end;

end.



//--------------------------------------------------------------------
class function Tdlg_Setup.ExecDlg(aID_str: string): boolean;
//--------------------------------------------------------------------
begin
  with Tdlg_Setup.Create(Application) do
  try
    FID_str :=aID_str;

    Result:=(ShowModal=mrOk);

    if result then
      Exec;
      
  finally
    Free;
  end;

//  if Result then


end;

