unit u_Link_freq_report_classes;

interface

uses
  classes, DB, SysUtils,

  
  u_func,
  u_const_db;


type

  // ---------------------------------------------------------------
  TAntenna = class(TCollectionItem)
  // ---------------------------------------------------------------
  public
   // Ground_H           : double;

    Ground_Height      : Double;
    Height_with_ground : Double;

    Height             : double;

    Azimuth            : double;

    Loss            : double;
    Tilt               : double;
    Gain               : double;

    vert_width         : Double;
    horz_width         : Double;

    Diameter           : Double;


    POLARIZATION_STR : string;
  end;


  // ---------------------------------------------------------------
  TAntennaList = class(TCollection)
  // ---------------------------------------------------------------
  private
    function GetHeightStr: string;
    function GetDiameterStr: string;
    function GetPOLARIZATIONStr: string;

 //   function GetAntennaHeightArr: u_Func.TDoubleArray;

    function GetItems(Index: Integer): TAntenna;
  public
    constructor Create;
    function AddItem: TAntenna;

    function GetMaxHeight_with_ground: double;

    function GetValue(aParam: string): string;

    procedure LoadFromDataset(aDataSet: TDataSet; aDataSet_Property: TDataSet);

    property Items[Index: Integer]: TAntenna read GetItems; default;

  end;

implementation



function TAntennaList.GetPOLARIZATIONStr: string;
begin
  Result := GetValue('POLARIZATION');
end;

function TAntennaList.GetDiameterStr: string;
begin
  Result := GetValue('Diameter');
end;

function TAntennaList.GetHeightStr: string;
begin
  Result := GetValue('Height');
end;

// ---------------------------------------------------------------
function TAntennaList.GetValue(aParam: string): string;
// ---------------------------------------------------------------
var
  I: Integer;
begin
  for I := 0 to Count - 1 do
  begin

(*
    if Eq(aParam,'Height')   then eValue:=Items[i].Height else
    if Eq(aParam,'diameter') then eValue:=Items[i].diameter else
    if Eq(aParam,'loss')     then eValue:=Items[i].loss else
    if Eq(aParam,'tilt')     then eValue:=Items[i].tilt else
    if Eq(aParam,'azimuth')  then eValue:=Items[i].azimuth
    else
      raise Exception.Create('');

    Result := Result + Format('/%1.1f', [eValue]);
*)
    Result := Result + '/';


    if Eq(aParam,'Height_with_ground') then
//      Result := Result + Format('/%1.1f', [Items[i].Height_with_ground]) else
      Result := Result + FloatToStr(Items[i].Height_with_ground) else

    if Eq(aParam,'vert_width') then
//      Result := Result + Format('/%1.1f', [Items[i].vert_width]) else
      Result := Result +  FloatToStr(Items[i].vert_width) else

    if Eq(aParam,'horz_width') then
//      Result := Result + Format('/%1.1f', [Items[i].horz_width]) else
      Result := Result +  FloatToStr(Items[i].horz_width) else

    if Eq(aParam,'azimuth') then
//      Result := Result + Format('/%1.1f', [Items[i].azimuth]) else
      Result := Result +  FloatToStr(Items[i].azimuth) else

    if Eq(aParam,'gain') then
//      Result := Result + Format('/%1.1f', [Items[i].gain]) else
      Result := Result +  FloatToStr(Items[i].gain) else


    if Eq(aParam,'loss') then
//      Result := Result + Format('/%1.1f', [Items[i].loss]) else
      Result := Result +  FloatToStr(Items[i].Loss) else

    if Eq(aParam,'tilt') then
//      Result := Result + Format('/%1.1f', [Items[i].tilt]) else
      Result := Result +  FloatToStr(Items[i].tilt) else

    if Eq(aParam,'Height') then
//      Result := Result + Format('/%1.1f', [Items[i].Height]) else
      Result := Result +  FloatToStr(Items[i].Height) else

    if Eq(aParam,'diameter') then
  //    Result := Result + Format('/%1.1f', [Items[i].diameter]) else
      Result := Result +  FloatToStr(Items[i].diameter) else

    if Eq(aParam,'POLARIZATION') then
      Result := Result + Format('%s', [Items[i].POLARIZATION_STR])

    else
      raise Exception.Create(aParam);

  end;

  Result := Copy(Result, 2, 100);

end;

constructor TAntennaList.Create;
begin
  inherited Create(TAntenna);
end;

function TAntennaList.AddItem: TAntenna;
begin
  Result := TAntenna (inherited Add);
end;

function TAntennaList.GetItems(Index: Integer): TAntenna;
begin
  Result := TAntenna(inherited Items[Index]);
end;


//--------------------------------------------------------------------
function TAntennaList.GetMaxHeight_with_ground: double;
//--------------------------------------------------------------------
var
  i: integer;
begin
  Result := 0;

  for I := 0 to Count - 1 do
    if result< Items[i].Height_with_ground  then
      result := Items[i].Height_with_ground;

end;




//--------------------------------------------------------------------
procedure TAntennaList.LoadFromDataset(aDataSet: TDataSet; aDataSet_Property:
    TDataSet);
//--------------------------------------------------------------------
var
  eGround_Height: Double;
  //eGround_Height: Double;
  i: integer;
  oAntenna: TAntenna;
 // vGround_Height: Variant;
begin
  Clear;

  Assert(aDataSet.RecordCount>0);

//  Setlength(Result, aDataSet.RecordCount);

  eGround_Height:=aDataSet_Property.FieldByName(FLD_Ground_Height).AsFloat;

  with aDataSet do
    while not Eof do
    begin
      oAntenna:=AddItem();

      oAntenna.POLARIZATION_STR:=FieldByName(FLD_POLARIZATION_STR).AsString;
      oAntenna.Height:=FieldByName(FLD_HEIGHT).AsFloat;
      oAntenna.Diameter:=FieldByName(FLD_Diameter).AsFloat;

      oAntenna.Loss:=FieldByName(FLD_Loss).AsFloat;
      oAntenna.Gain:=FieldByName(FLD_Gain).AsFloat;
      oAntenna.Tilt:=FieldByName(FLD_Tilt).AsFloat;

      oAntenna.Azimuth:=FieldByName(FLD_Azimuth).AsFloat;

      oAntenna.vert_width:=FieldByName(FLD_vert_width).AsFloat;
      oAntenna.horz_width:=FieldByName(FLD_horz_width).AsFloat;

      oAntenna.Ground_Height:=eGround_Height;

    //  if not VarIsNull(oAntenna.Ground_Height) then
      oAntenna.Height_with_ground:=eGround_Height + oAntenna.Height;

      Next;
    end;


end;

end.



(*//--------------------------------------------------------------------
function TAntennaList.GetAntennaHeightArr: u_Func.TDoubleArray;
//--------------------------------------------------------------------
var
  i: integer;
begin
  Setlength(Result, Count);

  for I := 0 to Count - 1 do
    Result[i]:= Items[i].Height;

  DoubleArraySort (Result);
end;
*)
