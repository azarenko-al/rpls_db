unit dm_Link_freq_plan;

interface

uses
  SysUtils, Classes, ADODB, DB, Forms, Dialogs,

  dm_Main,

  u_db,
  u_const_db
  
  ;

type
  TdmLink_freq_plan1 = class(TDataModule)
    ds_Reports: TDataSource;
    qry_Reports: TADOQuery;
    sp_Reports: TADOStoredProc;
    procedure DataModuleCreate(Sender: TObject);
  private
  //  procedure SetADOConnection(aADOConnection: TADOConnection);
    { Private declarations }
  public
    function Check_Report_ID(aID: Integer): Boolean;
    class procedure Init;

    procedure Open;
//    procedure SetConnectionObject(aConnectionObject: _Connection);

  end;

var
  dmLink_freq_plan1: TdmLink_freq_plan1;

implementation

{$R *.dfm}

procedure TdmLink_freq_plan1.DataModuleCreate(Sender: TObject);
begin
//  if ADOConnection1.Connected then
//    ShowMessage('procedure TdmLink_freq_plan.FormCreate(Sender: TObject); - ADOConnection1.Connected ');

  db_SetComponentADOConnection(Self, dmMain.ADOConnection1 );
//  db_SetComponentADOConnection(Self, ADOConnection1 );

end;


class procedure TdmLink_freq_plan1.Init;
begin
  if not Assigned(dmLink_freq_plan1) then
    dmLink_freq_plan1 := TdmLink_freq_plan1.Create(Application);

end;


// ---------------------------------------------------------------
procedure TdmLink_freq_plan1.Open;
// ---------------------------------------------------------------
const
  DEF_link_freq_plan = 'link_freq_plan';
begin
 // Assert (ADOConnection1.Connected, 'db_OpenQuery:aQuery.Connection.Connected');


  dmOnega_DB_data.OpenQuery(qry_Reports,
      Format('select * from %s where category=''%s''',
             [TBL_REPORT, DEF_link_freq_plan])); // 'select * from property where (id=:property2_id)');

end;


// ---------------------------------------------------------------
function TdmLink_freq_plan1.Check_Report_ID(aID: Integer): Boolean;
// ---------------------------------------------------------------
begin
  result := qry_Reports.Locate(FLD_ID, aID, []);
end;


 {
procedure TdmLink_freq_plan.SetADOConnection(aADOConnection: TADOConnection);
begin
  db_SetComponentADOConnection(Self, aADOConnection );
end;

}


end.

