unit f_Beeline_FreqPlan;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, frxClass,
   frxDBSet, DB, ADODB, StdCtrls, ComCtrls,
  Grids, DBGrids, cxInplaceContainer, cxTL, cxDBTL, cxControls,

  frxExportRTF,
  frxExportXLS,
  frxExportPDF,

  dm_report,


  u_const_db,

  u_files,

  dm_Main,
  u_db,
  cxTLData, frxDesgn
  ;

type
  Tfrm_Beeline_FreqPlan = class(TForm)
    DBGrid2: TDBGrid;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    DBGrid1: TDBGrid;
    DBGrid3: TDBGrid;
    TabSheet2: TTabSheet;
    DBGrid5: TDBGrid;
    DBGrid6: TDBGrid;
    Button1: TButton;
    ADOConnection1: TADOConnection;
    qry_property1: TADOQuery;
    ds_property1: TDataSource;
    ds_LInks: TDataSource;
    frxReport: TfrxReport;
    frxDBDataset_link: TfrxDBDataset;
    frxRTFExport1: TfrxRTFExport;
    qry_property2: TADOQuery;
    ds_property2: TDataSource;
    frxDBDataset_property1: TfrxDBDataset;
    frxDBDataset_property2: TfrxDBDataset;
    q_Linkend1: TADOQuery;
    ds_Linkend1: TDataSource;
    q_Linkend2: TADOQuery;
    ds_Linkend2: TDataSource;
    frxDBDataset_q_Linkend1: TfrxDBDataset;
    frxDBDataset_q_Linkend2: TfrxDBDataset;
    TabSheet3: TTabSheet;
    DBGrid4: TDBGrid;
    qry_LInks_new: TADOQuery;
    frxXLSExport1: TfrxXLSExport;
    frxReport_tele2: TfrxReport;
    cxDBTreeList1: TcxDBTreeList;
    cxDBTreeList1cxDBTreeListColumn1: TcxDBTreeListColumn;
    cxDBTreeList1cxDBTreeListColumn2: TcxDBTreeListColumn;
    cxDBTreeList1cxDBTreeListColumn3: TcxDBTreeListColumn;
    cxDBTreeList1cxDBTreeListColumn4: TcxDBTreeListColumn;
    cxDBTreeList1cxDBTreeListColumn5: TcxDBTreeListColumn;
    frxPDFExport1: TfrxPDFExport;
    frxDesigner1: TfrxDesigner;
    procedure Button1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    FOldConnectionObject: _Connection;
  public
    Params: record
              //FileType : string;
              FileType : (ftExcel1,ftWord1,ftPDF1);
              FileName : string;

              IsPreview : Boolean;
              IsOpenAfterDone : Boolean;

              Report_ID : Integer;
            end;


    procedure DesignReport;

    procedure Execute(aID_str: string);

    procedure SetADOConnection(aADOConnection: TADOConnection);
    procedure SetConnectionObject(aConnectionObject: _Connection);

//    procedure Open_Designer;

  end;

var
  frm_Beeline_FreqPlan: Tfrm_Beeline_FreqPlan;

implementation
{$R *.dfm}

// ---------------------------------------------------------------
procedure Tfrm_Beeline_FreqPlan.FormCreate(Sender: TObject);
// ---------------------------------------------------------------
begin
  if ADOConnection1.Connected then
    ShowMessage('procedure Tfrm_Beeline_FreqPlan.FormCreate(Sender: TObject); - ADOConnection1.Connected ');


  db_SetComponentADOConnection(Self, dmMain.ADOConnection1 );

  Params.IsPreview := True;
  Params.FileType := ftExcel1;

  qry_Property1.SQL.Text:= 'select * from '+TBL_property+' where (id=:property1_id)';
  qry_Property2.SQL.Text:= 'select * from '+TBL_property+' where (id=:property2_id)';

  q_Linkend1.SQL.Text:= 'select * from '+ view_Linkend_report +' where (id=:Linkend1_id)';
  q_Linkend2.SQL.Text:= 'select * from '+ view_Linkend_report +' where (id=:Linkend2_id)';


end;

//
//procedure Tfrm_Beeline_FreqPlan.Open_Designer;
//begin
//  frxReport.DesignReport();
//end;



procedure Tfrm_Beeline_FreqPlan.SetADOConnection(aADOConnection:
    TADOConnection);
begin
  db_SetComponentADOConnection(Self, aADOConnection );
end;

// ---------------------------------------------------------------
procedure Tfrm_Beeline_FreqPlan.SetConnectionObject(aConnectionObject: _Connection);
// ---------------------------------------------------------------
begin
  if not Assigned(FOldConnectionObject) then
  begin
    FOldConnectionObject:= AdoConnection1.ConnectionObject;
    AdoConnection1.ConnectionObject:=aConnectionObject;
  end;

end;


// ---------------------------------------------------------------
procedure Tfrm_Beeline_FreqPlan.DesignReport;
// ---------------------------------------------------------------
begin
  frxReport.DesignReport();
end;



// ---------------------------------------------------------------
procedure Tfrm_Beeline_FreqPlan.Execute(aID_str: string);
// ---------------------------------------------------------------

    procedure DoExecExportByFilter(aExportFilter: TfrxCustomExportFilter; aExt:
        String);
    begin
      assert(params.FileName<>'');

      aExportFilter.FileName:= ChangeFileExt(params.FileName, aExt);
      aExportFilter.ShowDialog:= False;
      
      frxReport.Export(aExportFilter);
//      aExportFilter.ShowDialog:= True;
    end;


var
  FTempFileName: string;
  s: string;
begin
//  s:=Format('exec %s ''%s''', [sp_Link_report, aID_str]);
  Assert(aID_str<>'');    

  s:=Format('SELECT * FROM %s where id IN (%s) order by name', [view_Link, aID_str]);

  //  SELECT * FROM	view_Link  where id IN (@ID_STR)  order by name

  db_OpenQuery(qry_LInks_new, s);  

  db_OpenQuery(qry_Property1, 'select * from '+TBL_property+' where (id=:property1_id)');
  db_OpenQuery(qry_Property2, 'select * from '+TBL_property+' where (id=:property2_id)');


  db_OpenQuery(q_Linkend1,
     'select * from '+ view_Linkend_report +' where (id=:Linkend1_id)');

  db_OpenQuery(q_Linkend2,
     'select * from '+ view_Linkend_report +' where (id=:Linkend2_id)');

//  FTempFileName :=  'd:\1111111111111111.rep';
  FTempFileName := GetTempFileNameWithExt('.rep');
  Assert(Params.Report_ID>0);

  dmReport.SaveToFile(Params.Report_ID, FTempFileName);




 // sHOWModal;

  Assert(qry_Property1.RecordCount>0);
  Assert(qry_Property1.RecordCount>0);
  Assert(q_Linkend1.RecordCount>0);
  Assert(q_Linkend2.RecordCount>0);

//  dmMain.ADOConnection1.Execute(s);

  frxReport.LoadFromFile(FTempFileName);

  DeleteFile(FTempFileName);

  
//
//  Open_Designer;
//
//  Exit;
//


  if Params.IsPreview then
    frxReport.ShowReport

  else begin
    frxReport.PrepareReport;

 // frxReport_tele2.ShowReport;

    case Params.FileType of
      ftExcel1 : DoExecExportByFilter(frxXLSExport1, '.xls');
      ftWord1  : DoExecExportByFilter(frxRTFExport1, '.doc');
      ftPDF1   : DoExecExportByFilter(frxPDFExport1, '.pdf');
    end;
  end;



(*
    if (ExpMode < 0) then
    // ������������ (����������� ������ ��� �������)
    frRep.ShowReport
  else
    begin
    // ������� ��� �������������
    frRep.PrepareReport;
    case ExpMode of
    0 : ExportFilter(frePDF, '.pdf');
  //  1 : ExportFilter(freHTM, '.htm');
    2 : ExportFilter(freRTF, '.rtf');
    3 : ExportFilter(freXLS, '.xls');
 //   4 : ExportFilter(freGIF, '.gif');
  //  5 : ExportFilter(freJPG, '.jpg');
    end;//case
    end;
*)



//  exec sp_Link_report '109902,109926,109903,109933,109904,'

end;


procedure Tfrm_Beeline_FreqPlan.Button1Click(Sender: TObject);
begin
//  frxReport2.ShowReport;
  frxReport_tele2.ShowReport;

end;



end.

{
  dm_Main
exec sp_Link_report '109902,109926,109903,109933,109904,'

