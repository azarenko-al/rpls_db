unit f_Report_bee;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, frxExportXLS, frxClass, frxExportRTF, frxDBSet, DB, ADODB, ComCtrls,
  StdCtrls, Grids, DBGrids;

type
  Tfrm_Report_bee = class(TForm)
    DBGrid2: TDBGrid;
    Button1: TButton;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    DBGrid1: TDBGrid;
    DBGrid3: TDBGrid;
    TabSheet2: TTabSheet;
    DBGrid5: TDBGrid;
    DBGrid6: TDBGrid;
    TabSheet3: TTabSheet;
    DBGrid7: TDBGrid;
    DBGrid8: TDBGrid;
    ADOConnection1: TADOConnection;
    qry_property1: TADOQuery;
    qry_LInks: TADOQuery;
    ds_property1: TDataSource;
    ds_LInks: TDataSource;
    frxReport1: TfrxReport;
    frxDBDataset_link: TfrxDBDataset;
    frxRTFExport1: TfrxRTFExport;
    frxXLSExport1: TfrxXLSExport;
    qry_property2: TADOQuery;
    ds_property2: TDataSource;
    frxDBDataset_property1: TfrxDBDataset;
    frxDBDataset_property2: TfrxDBDataset;
    q_Linkend1: TADOQuery;
    ds_Linkend1: TDataSource;
    q_Linkend2: TADOQuery;
    ds_Linkend2: TDataSource;
    frxDBDataset_q_Linkend1: TfrxDBDataset;
    frxDBDataset_q_Linkend2: TfrxDBDataset;
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frm_Report_bee: Tfrm_Report_bee;

implementation

{$R *.dfm}

procedure Tfrm_Report_bee.Button1Click(Sender: TObject);
begin
  frxReport1.ShowReport();
end;

end.
