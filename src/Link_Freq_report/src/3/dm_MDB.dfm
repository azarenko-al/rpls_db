object dmMDB: TdmMDB
  OldCreateOrder = True
  OnCreate = DataModuleCreate
  Left = 839
  Top = 469
  Height = 380
  Width = 959
  object ADOConnection_MDB: TADOConnection
    ConnectionString = 
      'Provider=Microsoft.Jet.OLEDB.4.0;Data Source=D:\1.mdb;Persist Se' +
      'curity Info=False;'
    LoginPrompt = False
    Mode = cmShareDenyNone
    Provider = 'Microsoft.Jet.OLEDB.4.0'
    Left = 60
    Top = 12
  end
  object t_Property1: TADOTable
    Connection = ADOConnection_MDB
    CursorType = ctStatic
    IndexFieldNames = 'id'
    MasterFields = 'Property1_id'
    TableName = 'property1'
    Left = 184
    Top = 104
  end
  object t_LinkEnd1: TADOTable
    Connection = ADOConnection_MDB
    CursorType = ctStatic
    IndexFieldNames = 'id'
    MasterFields = 'linkend1_id'
    TableName = 'LinkEnd1'
    Left = 384
    Top = 104
  end
  object t_Property2: TADOTable
    Connection = ADOConnection_MDB
    CursorType = ctStatic
    IndexFieldNames = 'id'
    MasterFields = 'Property2_id'
    TableName = 'property2'
    Left = 264
    Top = 104
  end
  object t_LinkEnd2: TADOTable
    Connection = ADOConnection_MDB
    CursorType = ctStatic
    IndexFieldNames = 'id'
    MasterFields = 'linkend2_id'
    TableName = 'LinkEnd2'
    Left = 464
    Top = 104
  end
  object ds_Property2: TDataSource
    DataSet = t_Property2
    Left = 264
    Top = 192
  end
  object ds_Property1: TDataSource
    DataSet = t_Property1
    Left = 176
    Top = 192
  end
  object ds_LinkEnd1: TDataSource
    DataSet = t_LinkEnd1
    Left = 384
    Top = 184
  end
  object ds_LinkEnd2: TDataSource
    DataSet = t_LinkEnd2
    Left = 466
    Top = 184
  end
  object t_Antennas1: TADOTable
    Connection = ADOConnection_MDB
    CursorType = ctStatic
    IndexFieldNames = 'link_id'
    MasterFields = 'id'
    TableName = 'Antennas1'
    Left = 584
    Top = 104
  end
  object t_Antennas2: TADOTable
    Connection = ADOConnection_MDB
    CursorType = ctStatic
    IndexFieldNames = 'link_id'
    MasterFields = 'id'
    TableName = 'Antennas2'
    Left = 664
    Top = 104
  end
  object DataSource1: TDataSource
    Left = 584
    Top = 184
  end
  object DataSource2: TDataSource
    Left = 666
    Top = 184
  end
  object t_Link: TADOTable
    Connection = ADOConnection_MDB
    CursorType = ctStatic
    TableName = 'Link'
    Left = 80
    Top = 104
  end
  object ds_Link: TDataSource
    DataSet = t_Link
    Left = 80
    Top = 192
  end
end
