unit dm_App_link_freq_report;

interface

uses
  Classes, Dialogs,  SysUtils,

 // dm_MDB_fp,

  dm_Link_freq_plan_report,

//  dm_Link_freq_plan,

  d_Setup,

  //f_Beeline_FreqPlan,
  u_classes,


  u_ini_Link_freq_report_params,

  u_vars,


  dm_Main,

  DB, ADODB

  ;
type
  TdmMain_app__freq_report = class(TDataModule)
    procedure DataModuleCreate(Sender: TObject);
  public
  end;

var
  dmMain_app__freq_report: TdmMain_app__freq_report;


implementation

 {$R *.DFM}


//--------------------------------------------------------------
procedure TdmMain_app__freq_report.DataModuleCreate(Sender: TObject);
//--------------------------------------------------------------
const
  DEF_TEMP_FILENAME = 'link_report.ini';

  REGISTRY_ROOT = 'Software\Onega\RPLS_DB\';

var
  oParam: TIni_Link_freq_Report_Param;
//  sConnectionString: string;
  sID: string;
  sIniFileName: string;
 // sMDB_FileName: string;
//  sReportFileName: string;

begin
  sIniFileName:=ParamStr(1);
  if sIniFileName='' then
    sIniFileName:=g_ApplicationDataDir+ DEF_TEMP_FILENAME;

  if not FileExists(sIniFileName) then
  begin
    ShowMessage(sIniFileName);
    Exit;
  end;

  // -------------------------------------------------------------------




  TdmMain.Init;

  if not dmMain.OpenDB_reg then    // True
    Exit;


//  ShowMessage('class procedure TLink_run.Freq_Report(aSelectedIDList: TIDList);');

 // sMDB_FileName:=g_ApplicationDataDir+ 'report_freq_plan.mdb';

//  TdmMDB.Init;
//  dmMDB.CreateMDB(sMDB_FileName);




  {$DEFINE use_dll111}

  {$IFDEF use_dll}

(*
  if Load_ILink_report() then
  begin
    ILink_report.InitADO(dmMain.ADOConnection1.ConnectionObject);
 //   ILink_graph.InitAppHandle(Application.Handle);
    ILink_report.Execute(sFile);

    UnLoad_ILink_report();
  end;

  Exit;
*)

  {$ENDIF}



  oParam:=TIni_Link_freq_Report_Param.Create;
  oParam.LoadFromINI(sIniFileName);


  sID:=oParam.IDList.MakeWhereString;//(',');

//  sConnectionString:=oParam.ConnectionString;

//  dmLink_freq_plan.ADOConnection1.ConnectionString:=oParam.ConnectionString;


(*
  sID:='154013';
*)

  Assert(sID<>'');

  FreeAndNil(oParam);


//  ADOConnection



  try

//    dmMain.Open_ADOConnection (sConnectionString);


//    TdmLink_freq_plan.Init;
    TdmLink_freq_plan_report.Init;

//    dmLink_freq_plan.Open;
    dmLink_freq_plan_report.Open1;


    Tdlg_Setup.ExecDlg(sID);

  finally // wrap up

  end;    // try/finally


end;


end.


