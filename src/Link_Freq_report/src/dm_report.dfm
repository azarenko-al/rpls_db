object dmLink_report: TdmLink_report
  OldCreateOrder = False
  Height = 597
  Width = 687
  object ADOConnection1: TADOConnection
    Connected = True
    ConnectionString = 
      'Provider=MSDASQL.1;Persist Security Info=False;User ID=sa;Data S' +
      'ource=SQL_ONEGA'
    LoginPrompt = False
    Left = 40
    Top = 24
  end
  object qry_property1: TADOQuery
    Connection = ADOConnection1
    CursorType = ctStatic
    LockType = ltReadOnly
    DataSource = ds_LInks
    Parameters = <
      item
        Name = 'property1_id'
        DataType = ftInteger
        Precision = 10
        Value = 530209
      end>
    SQL.Strings = (
      'select * from property'
      ''
      'where (id=:property1_id)'
      '')
    Left = 120
    Top = 88
  end
  object qry_LInks: TADOQuery
    Connection = ADOConnection1
    CursorType = ctStatic
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      'select  top 10  * from  view_link'
      'where  '
      ' '
      '  id in (select  link_id from linkCalcResults )'
      '')
    Left = 40
    Top = 88
  end
  object ds_property1: TDataSource
    DataSet = qry_property1
    Left = 120
    Top = 152
  end
  object ds_LInks: TDataSource
    DataSet = qry_LInks
    Left = 40
    Top = 152
  end
  object frxReport1: TfrxReport
    Version = '4.13.1'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 41121.613084722200000000
    ReportOptions.LastChange = 41138.831869317100000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      ''
      'begin'
      ''
      'end.')
    Left = 256
    Top = 272
    Datasets = <
      item
        DataSet = frxDBDataset_link
        DataSetName = 'link'
      end
      item
        DataSet = frxDBDataset_q_Linkend1
        DataSetName = 'Linkend1'
      end
      item
        DataSet = frxDBDataset_q_Linkend2
        DataSetName = 'Linkend2'
      end
      item
        DataSet = frxDBDataset_property1
        DataSetName = 'property1'
      end
      item
        DataSet = frxDBDataset_property2
        DataSetName = 'property2'
      end>
    Variables = <>
    Style = <
      item
        Name = 'Title'
        Color = clGray
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Height = -16
        Font.Name = 'Arial'
        Font.Style = [fsBold]
      end
      item
        Name = 'Header'
        Color = clNone
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clMaroon
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold]
      end
      item
        Name = 'Group header'
        Color = 16053492
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clMaroon
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold]
      end
      item
        Name = 'Data'
        Color = clNone
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
      end
      item
        Name = 'Group footer'
        Color = clNone
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold]
      end
      item
        Name = 'Header line'
        Color = clNone
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        Frame.Typ = [ftBottom]
        Frame.Width = 2.000000000000000000
      end>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      Orientation = poLandscape
      PaperWidth = 600.000000000000000000
      PaperHeight = 210.000000000000000000
      PaperSize = 256
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      PageCount = 99999
      PrintIfEmpty = False
      object ReportTitle1: TfrxReportTitle
        Height = 45.354360000000000000
        Top = 18.897650000000000000
        Width = 2192.127400000000000000
        StartNewPage = True
        object Memo8: TfrxMemoView
          Align = baWidth
          Width = 2192.127400000000000000
          Height = 37.795300000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            #1055#1088#1086#1077#1082#1090' '#1095#1072#1089#1090#1086#1090#1085#1086'-'#1090#1077#1088#1088#1080#1090#1086#1088#1080#1072#1083#1100#1085#1086#1075#1086' '#1087#1083#1072#1085#1072'  '#1085#1072' '#1090#1077#1088#1088#1080#1090#1086#1088#1080#1080)
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object MasterData1: TfrxMasterData
        Height = 120.944923390000000000
        Top = 234.330860000000000000
        Width = 2192.127400000000000000
        DataSet = frxDBDataset_link
        DataSetName = 'link'
        RowCount = 0
        object property1name: TfrxMemoView
          Align = baLeft
          Left = 68.031540000000000000
          Width = 162.519790000000000000
          Height = 60.472440940000000000
          ShowHint = False
          DataField = 'name'
          DataSet = frxDBDataset_property1
          DataSetName = 'property1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[property1."name"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object property2name: TfrxMemoView
          Align = baLeft
          Left = 68.031540000000000000
          Top = 60.472480000000000000
          Width = 162.519790000000000000
          Height = 60.472440940000000000
          ShowHint = False
          DataField = 'name'
          DataSet = frxDBDataset_property2
          DataSetName = 'property2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[property2."name"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line: TfrxMemoView
          Width = 30.236240000000000000
          Height = 120.944923390000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[Line]')
          ParentFont = False
          VAlign = vaCenter
        end
        object property1address: TfrxMemoView
          Align = baLeft
          Left = 230.551330000000000000
          Width = 132.283464570000000000
          Height = 60.472440940000000000
          ShowHint = False
          DataField = 'address'
          DataSet = frxDBDataset_property1
          DataSetName = 'property1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[property1."address"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object property2address: TfrxMemoView
          Align = baLeft
          Left = 230.551330000000000000
          Top = 60.472480000000000000
          Width = 132.283464570000000000
          Height = 60.472440940000000000
          ShowHint = False
          DataField = 'address'
          DataSet = frxDBDataset_property2
          DataSetName = 'property2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[property2."address"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Linkend1LinkEndTypename: TfrxMemoView
          Align = baLeft
          Left = 514.015896930000000000
          Width = 132.283464570000000000
          Height = 60.472440940000000000
          ShowHint = False
          DataField = 'LinkEndType.name'
          DataSet = frxDBDataset_q_Linkend1
          DataSetName = 'Linkend1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[Linkend1."LinkEndType.name"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo17: TfrxMemoView
          Align = baLeft
          Left = 514.015896930000000000
          Top = 60.472440939999900000
          Width = 132.344510160000000000
          Height = 60.472440940000000000
          ShowHint = False
          DataField = 'LinkEndType.name'
          DataSet = frxDBDataset_q_Linkend2
          DataSetName = 'Linkend2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[Linkend2."LinkEndType.name"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Linkend1antenna_height: TfrxMemoView
          Align = baLeft
          Left = 907.087200000000000000
          Width = 68.031540000000000000
          Height = 60.472440940000000000
          ShowHint = False
          DataField = 'antenna_height'
          DataSet = frxDBDataset_q_Linkend1
          DataSetName = 'Linkend1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[Linkend1."antenna_height"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo22: TfrxMemoView
          Align = baLeft
          Left = 907.087200000000000000
          Top = 60.472480000000000000
          Width = 68.031540000000000000
          Height = 60.472440940000000000
          ShowHint = False
          DataSet = frxDBDataset_q_Linkend1
          DataSetName = 'Linkend1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[Linkend2."antenna_height"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Linkend2rx_freq_MHz: TfrxMemoView
          Align = baLeft
          Left = 646.360407090000000000
          Top = 60.472480000000000000
          Width = 79.370130000000000000
          Height = 60.472440940000000000
          ShowHint = False
          DataField = 'rx_freq_MHz'
          DataSet = frxDBDataset_q_Linkend2
          DataSetName = 'Linkend2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[Linkend2."rx_freq_MHz"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo23: TfrxMemoView
          Align = baLeft
          Left = 646.299361500000000000
          Width = 79.370130000000000000
          Height = 60.472440940000000000
          ShowHint = False
          DataSet = frxDBDataset_q_Linkend2
          DataSetName = 'Linkend2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[Linkend1."rx_freq_MHz"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Linkend2antenna_polarization: TfrxMemoView
          Align = baLeft
          Left = 1360.630800000000000000
          Width = 45.354360000000000000
          Height = 60.472440940000000000
          ShowHint = False
          DataSet = frxDBDataset_q_Linkend2
          DataSetName = 'Linkend2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[Linkend1."antenna_polarization"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo18: TfrxMemoView
          Align = baLeft
          Left = 1360.630800000000000000
          Top = 60.472480000000000000
          Width = 45.354360000000000000
          Height = 60.472440940000000000
          ShowHint = False
          DataField = 'antenna_polarization'
          DataSet = frxDBDataset_q_Linkend2
          DataSetName = 'Linkend2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[Linkend2."antenna_polarization"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Linkend2antenna_diameter: TfrxMemoView
          Align = baLeft
          Left = 1311.496910000000000000
          Width = 49.133890000000000000
          Height = 60.472440940000000000
          ShowHint = False
          DataSet = frxDBDataset_q_Linkend2
          DataSetName = 'Linkend2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[Linkend1."antenna_diameter"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo24: TfrxMemoView
          Align = baLeft
          Left = 1311.496910000000000000
          Top = 60.472480000000000000
          Width = 49.133890000000000000
          Height = 60.472440940000000000
          ShowHint = False
          DataField = 'antenna_diameter'
          DataSet = frxDBDataset_q_Linkend2
          DataSetName = 'Linkend2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[Linkend2."antenna_diameter"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo25: TfrxMemoView
          Left = 725.669760000000000000
          Width = 56.692950000000000000
          Height = 60.472440940000000000
          ShowHint = False
          DataSet = frxDBDataset_q_Linkend2
          DataSetName = 'Linkend2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[Linkend1."antenna_horz_width"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo26: TfrxMemoView
          Left = 725.669760000000000000
          Top = 60.472480000000000000
          Width = 56.692950000000000000
          Height = 60.472440940000000000
          ShowHint = False
          DataSet = frxDBDataset_q_Linkend2
          DataSetName = 'Linkend2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[Linkend2."antenna_horz_width"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo27: TfrxMemoView
          Align = baLeft
          Left = 782.362710000000000000
          Width = 56.692950000000000000
          Height = 60.472440940000000000
          ShowHint = False
          DataSet = frxDBDataset_q_Linkend2
          DataSetName = 'Linkend2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[Linkend1."antenna_vert_width"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo28: TfrxMemoView
          Align = baLeft
          Left = 782.362710000000000000
          Top = 60.472480000000000000
          Width = 56.692950000000000000
          Height = 60.472440940000000000
          ShowHint = False
          DataSet = frxDBDataset_q_Linkend2
          DataSetName = 'Linkend2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[Linkend2."antenna_vert_width"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo29: TfrxMemoView
          Align = baLeft
          Left = 839.055660000000000000
          Width = 68.031540000000000000
          Height = 60.472440940000000000
          ShowHint = False
          DataSet = frxDBDataset_q_Linkend1
          DataSetName = 'Linkend1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[Linkend1."antenna_gain"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo30: TfrxMemoView
          Align = baLeft
          Left = 839.055660000000000000
          Top = 60.472480000000000000
          Width = 68.031540000000000000
          Height = 60.472440940000000000
          ShowHint = False
          DataSet = frxDBDataset_q_Linkend1
          DataSetName = 'Linkend1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[Linkend2."antenna_gain"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object linklength_km: TfrxMemoView
          Left = 1073.386520000000000000
          Width = 52.913420000000000000
          Height = 120.944960000000000000
          ShowHint = False
          DataField = 'length_km'
          DataSet = frxDBDataset_link
          DataSetName = 'link'
          DisplayFormat.FormatStr = '%2.1f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            '[link."length_km"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Linkend1power_dBm: TfrxMemoView
          Align = baLeft
          Left = 1126.299940000000000000
          Width = 49.133890000000000000
          Height = 120.944960000000000000
          ShowHint = False
          DataField = 'power_dBm'
          DataSet = frxDBDataset_q_Linkend1
          DataSetName = 'Linkend1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            '[Linkend1."power_dBm"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Linkend2radiation_class: TfrxMemoView
          Align = baLeft
          Left = 1175.433830000000000000
          Top = 60.472480000000000000
          Width = 71.811070000000000000
          Height = 60.472480000000000000
          ShowHint = False
          DataField = 'radiation_class'
          DataSet = frxDBDataset_q_Linkend2
          DataSetName = 'Linkend2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[Linkend2."radiation_class"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo31: TfrxMemoView
          Align = baLeft
          Left = 1175.433830000000000000
          Width = 71.811070000000000000
          Height = 60.472480000000000000
          ShowHint = False
          DataSet = frxDBDataset_q_Linkend2
          DataSetName = 'Linkend2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[Linkend1."radiation_class"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Linkend2bitrate_Mbps: TfrxMemoView
          Align = baLeft
          Left = 1247.244900000000000000
          Top = 60.472480000000000000
          Width = 64.252010000000000000
          Height = 60.472480000000000000
          ShowHint = False
          DataField = 'bitrate_Mbps'
          DataSet = frxDBDataset_q_Linkend2
          DataSetName = 'Linkend2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[Linkend2."bitrate_Mbps"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo32: TfrxMemoView
          Align = baLeft
          Left = 1247.244900000000000000
          Width = 64.252010000000000000
          Height = 60.472480000000000000
          ShowHint = False
          DataSet = frxDBDataset_q_Linkend2
          DataSetName = 'Linkend2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[Linkend1."bitrate_Mbps"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo35: TfrxMemoView
          Align = baLeft
          Left = 1405.985160000000000000
          Width = 102.047310000000000000
          Height = 120.944911180000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo36: TfrxMemoView
          Align = baLeft
          Left = 1508.032470000000000000
          Width = 49.133890000000000000
          Height = 120.944911180000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          ParentFont = False
          VAlign = vaCenter
        end
        object property2lat_str: TfrxMemoView
          Align = baLeft
          Left = 362.834794570000000000
          Top = 60.472480000000000000
          Width = 75.590551180000000000
          Height = 60.472480000000000000
          ShowHint = False
          DataField = 'lat_str'
          DataSet = frxDBDataset_property2
          DataSetName = 'property2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            '[property2."lat_str"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo38: TfrxMemoView
          Align = baLeft
          Left = 362.834794570000000000
          Width = 75.590551180000000000
          Height = 60.472480000000000000
          ShowHint = False
          DataSet = frxDBDataset_property2
          DataSetName = 'property2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            '[property1."lat_str"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo39: TfrxMemoView
          Align = baLeft
          Left = 438.425345750000000000
          Top = 60.472480000000000000
          Width = 75.590551180000000000
          Height = 60.472480000000000000
          ShowHint = False
          DataSet = frxDBDataset_property2
          DataSetName = 'property2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            '[property2."lon_str"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo40: TfrxMemoView
          Align = baLeft
          Left = 438.425345750000000000
          Width = 75.590551180000000000
          Height = 60.472480000000000000
          ShowHint = False
          DataSet = frxDBDataset_property2
          DataSetName = 'property2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            '[property1."lon_str"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo41: TfrxMemoView
          Align = baLeft
          Left = 30.236240000000000000
          Width = 37.795300000000000000
          Height = 60.472440940000000000
          ShowHint = False
          DataSet = frxDBDataset_property1
          DataSetName = 'property1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[property1."id"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo42: TfrxMemoView
          Align = baLeft
          Left = 30.236240000000000000
          Top = 60.472480000000000000
          Width = 37.795300000000000000
          Height = 60.472440940000000000
          ShowHint = False
          DataSet = frxDBDataset_property1
          DataSetName = 'property1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[property2."id"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Linkend2antenna_ground_height: TfrxMemoView
          Align = baLeft
          Left = 975.118740000000000000
          Width = 60.472480000000000000
          Height = 60.472480000000000000
          ShowHint = False
          DataSet = frxDBDataset_q_Linkend2
          DataSetName = 'Linkend2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[Linkend1."antenna_ground_height"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo43: TfrxMemoView
          Align = baLeft
          Left = 975.118740000000000000
          Top = 60.472480000000000000
          Width = 60.472480000000000000
          Height = 60.472480000000000000
          ShowHint = False
          DataField = 'antenna_ground_height'
          DataSet = frxDBDataset_q_Linkend2
          DataSetName = 'Linkend2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[Linkend2."antenna_ground_height"]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object Header1: TfrxHeader
        Height = 79.370081180000000000
        Top = 128.504020000000000000
        Width = 2192.127400000000000000
        object Memo3: TfrxMemoView
          Align = baLeft
          Left = 230.551330000000000000
          Width = 132.283464570000000000
          Height = 75.590551180000000000
          ShowHint = False
          Color = clSilver
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            #1040#1076#1088#1077#1089)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          Align = baLeft
          Left = 774.803650000000000000
          Width = 52.913420000000000000
          Height = 75.590551180000000000
          ShowHint = False
          Color = clSilver
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            #1044#1053#1040
            #1074#1077#1088#1090'.')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo7: TfrxMemoView
          Left = 725.669760000000000000
          Width = 49.133890000000000000
          Height = 75.590551180000000000
          ShowHint = False
          Color = clSilver
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            #1044#1053#1040
            #1075#1086#1088'.')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo9: TfrxMemoView
          Align = baLeft
          Width = 30.236240000000000000
          Height = 75.590551181102400000
          ShowHint = False
          Color = clSilver
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            #8470
            #1087'/'#1087)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo10: TfrxMemoView
          Align = baLeft
          Left = 362.834794570000000000
          Width = 75.590600000000000000
          Height = 75.590551181102400000
          ShowHint = False
          Color = clSilver
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            #1064#1080#1088#1086#1090#1072','
            #1089'.'#1096'.')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo11: TfrxMemoView
          Align = baLeft
          Left = 514.015994570000000000
          Width = 132.283464570000000000
          Height = 75.590551181102400000
          ShowHint = False
          Color = clSilver
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            #1058#1080#1087' '
            #1086#1073#1086#1088#1091#1076#1086#1074#1072#1085#1080#1103)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo12: TfrxMemoView
          Align = baLeft
          Left = 646.299459140000000000
          Width = 79.370130000000000000
          Height = 75.590551180000000000
          ShowHint = False
          Color = clSilver
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            #1048#1085#1076#1077#1082#1089
            '('#1085#1086#1084#1080#1085#1072#1083#1099' '#1095#1072#1089#1090#1086#1090'), '#1052#1043#1094)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo13: TfrxMemoView
          Align = baLeft
          Left = 438.425394570000000000
          Width = 75.590600000000000000
          Height = 75.590551181102400000
          ShowHint = False
          Color = clSilver
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            #1044#1086#1083#1075#1086#1090#1072','
            #1074'.'#1076'.')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo14: TfrxMemoView
          Align = baLeft
          Left = 30.236240000000000000
          Width = 37.795300000000000000
          Height = 75.590551181102400000
          ShowHint = False
          Color = clSilver
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            #8470
            #1073'/'#1089)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo20: TfrxMemoView
          Align = baLeft
          Left = 827.717070000000000000
          Width = 66.141775000000100000
          Height = 75.590551181102400000
          ShowHint = False
          Color = clSilver
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            #1050#1091','
            #1076#1041)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo21: TfrxMemoView
          Align = baLeft
          Left = 893.858845000000100000
          Width = 85.039425000000000000
          Height = 75.590551180000000000
          ShowHint = False
          Color = clSilver
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            #1042#1099#1089#1086#1090#1072' '
            #1087#1086#1076#1074#1077#1089#1072' '
            #1072#1085#1090#1077#1085#1085#1099','
            ' '#1084)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo4: TfrxMemoView
          Align = baLeft
          Left = 1359.449696875000000000
          Width = 44.409477500000000000
          Height = 75.590551181102400000
          ShowHint = False
          Color = clSilver
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            #1055#1086#1083#1103#1088#1080#1079#1072#1094#1080#1103)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo19: TfrxMemoView
          Align = baLeft
          Left = 1311.339429583333000000
          Width = 48.110267291666700000
          Height = 75.590551181102400000
          ShowHint = False
          Color = clSilver
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            #1044#1080#1072#1084'.'
            #1072#1085#1090'., '#1084)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo1: TfrxMemoView
          Left = 1073.386520000000000000
          Width = 52.913420000000000000
          Height = 75.590551181102400000
          ShowHint = False
          Color = clSilver
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            #1056#1072#1089#1089#1090'., '#1082#1084)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo2: TfrxMemoView
          Align = baLeft
          Left = 1126.299940000000000000
          Width = 51.811057083333300000
          Height = 75.590551181102400000
          ShowHint = False
          Color = clSilver
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            #1052#1086#1097'-'
            #1085#1086#1089#1090#1100', '
            #1076#1041#1084)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo6: TfrxMemoView
          Align = baLeft
          Left = 1178.110997083333000000
          Width = 70.315006041666700000
          Height = 75.590551181102400000
          ShowHint = False
          Color = clSilver
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            #1050#1083#1072#1089#1089
            #1080#1079#1083#1091#1095#1077#1085#1080#1103)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          Align = baLeft
          Left = 1248.426003125000000000
          Width = 62.913426458333300000
          Height = 75.590551181102400000
          ShowHint = False
          Color = clSilver
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            #1057#1082#1086#1088#1086#1089#1090#1100' '#1087#1086#1090#1086#1082#1072', '#1084#1041#1080#1090'/'#1089)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo33: TfrxMemoView
          Align = baLeft
          Left = 1403.859174375000000000
          Width = 104.173295620000000000
          Height = 75.590551181102400000
          ShowHint = False
          Color = clSilver
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            #1060#1048#1054' '
            #1089#1076#1077#1083#1072#1074#1096#1077#1075#1086' '
            #1079#1072#1087#1080#1089#1100)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo34: TfrxMemoView
          Align = baLeft
          Left = 1508.032469995000000000
          Width = 49.133890000000000000
          Height = 75.590551181102400000
          ShowHint = False
          Color = clSilver
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            #1055#1088#1080#1084'.')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo37: TfrxMemoView
          Align = baLeft
          Left = 68.031540000000000000
          Width = 162.519790000000000000
          Height = 75.590551180000000000
          ShowHint = False
          Color = clSilver
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            #1053#1072#1079#1074#1072#1085#1080#1077' ')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          Align = baLeft
          Left = 978.898270000000100000
          Width = 66.141775000000000000
          Height = 75.590551180000000000
          ShowHint = False
          Color = clSilver
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            'H '#1072#1085#1090'.'
            #1086#1090' '#1091#1088'. '#1084#1086#1088#1103', '
            #1084)
          ParentFont = False
          VAlign = vaCenter
        end
      end
    end
  end
  object frxDBDataset_link: TfrxDBDataset
    UserName = 'link'
    CloseDataSource = False
    DataSet = qry_LInks
    BCDToCurrency = False
    Left = 72
    Top = 272
  end
  object frxRTFExport1: TfrxRTFExport
    FileName = 'D:\222222222222333233333444222222.rtf'
    UseFileCache = True
    ShowProgress = True
    OverwritePrompt = False
    CreationTime = 41141.726405983800000000
    DataOnly = False
    PictureType = gpPNG
    OpenAfterExport = True
    Wysiwyg = True
    Creator = 'FastReport'
    SuppressPageHeadersFooters = False
    HeaderFooterMode = hfText
    AutoSize = False
    Left = 376
    Top = 272
  end
  object frxXLSExport1: TfrxXLSExport
    FileName = 'D:\3333333333.xls'
    UseFileCache = True
    ShowProgress = True
    OverwritePrompt = False
    CreationTime = 41141.725477650460000000
    DataOnly = False
    ExportEMF = True
    OpenExcelAfterExport = True
    AsText = False
    Background = True
    FastExport = True
    PageBreaks = True
    EmptyLines = True
    SuppressPageHeadersFooters = False
    Left = 376
    Top = 328
  end
  object qry_property2: TADOQuery
    Connection = ADOConnection1
    CursorType = ctStatic
    LockType = ltReadOnly
    DataSource = ds_LInks
    Parameters = <
      item
        Name = 'property2_id'
        DataType = ftInteger
        Precision = 10
        Value = 530227
      end>
    SQL.Strings = (
      'select * from property'
      ''
      'where (id=:property2_id)'
      '')
    Left = 200
    Top = 88
  end
  object ds_property2: TDataSource
    DataSet = qry_property2
    Left = 200
    Top = 152
  end
  object frxDBDataset_property1: TfrxDBDataset
    UserName = 'property1'
    CloseDataSource = False
    DataSet = qry_property1
    BCDToCurrency = False
    Left = 72
    Top = 336
  end
  object frxDBDataset_property2: TfrxDBDataset
    UserName = 'property2'
    CloseDataSource = False
    DataSet = qry_property2
    BCDToCurrency = False
    Left = 72
    Top = 384
  end
  object q_Linkend1: TADOQuery
    Connection = ADOConnection1
    CursorType = ctStatic
    LockType = ltReadOnly
    DataSource = ds_LInks
    Parameters = <
      item
        Name = 'Linkend1_id'
        DataType = ftInteger
        Precision = 10
        Value = 191473
      end>
    SQL.Strings = (
      'select * from view_Linkend_report'
      ''
      'where (id=:Linkend1_id)'
      '')
    Left = 296
    Top = 88
  end
  object ds_Linkend1: TDataSource
    DataSet = q_Linkend1
    Left = 296
    Top = 152
  end
  object q_Linkend2: TADOQuery
    Connection = ADOConnection1
    CursorType = ctStatic
    LockType = ltReadOnly
    DataSource = ds_LInks
    Parameters = <
      item
        Name = 'Linkend2_id'
        DataType = ftInteger
        Precision = 10
        Value = 191474
      end>
    SQL.Strings = (
      'select * from  view_Linkend_report'
      ''
      'where (id=:Linkend2_id)'
      '')
    Left = 368
    Top = 88
  end
  object ds_Linkend2: TDataSource
    DataSet = q_Linkend2
    Left = 368
    Top = 152
  end
  object frxDBDataset_q_Linkend1: TfrxDBDataset
    UserName = 'Linkend1'
    CloseDataSource = False
    DataSet = q_Linkend1
    BCDToCurrency = False
    Left = 72
    Top = 440
  end
  object frxDBDataset_q_Linkend2: TfrxDBDataset
    UserName = 'Linkend2'
    CloseDataSource = False
    DataSet = q_Linkend2
    BCDToCurrency = False
    Left = 72
    Top = 496
  end
  object ADOQuery1: TADOQuery
    Connection = ADOConnection1
    CursorType = ctStatic
    LockType = ltReadOnly
    DataSource = ds_LInks
    Parameters = <
      item
        Name = 'Linkend2_id'
        DataType = ftInteger
        Precision = 10
        Value = 191474
      end>
    SQL.Strings = (
      'select * from  view_Linkend_report'
      ''
      'where (id=:Linkend2_id)'
      '')
    Left = 368
    Top = 88
  end
end
