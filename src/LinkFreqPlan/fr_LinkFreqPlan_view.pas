unit fr_LinkFreqPlan_view;

interface
{.$define test}


uses
  SysUtils, Classes, Controls, Forms,  ComCtrls,   rxPlacemnt,
  cxPropertiesStore,  ExtCtrls, Menus, ImgList, ActnList,

  dm_User_Security,

  dm_Onega_DB_data,

  fr_View_base,

  fr_DBInspector_Container,

  fr_LinkFreqPlan_LinkEnd,
  fr_LinkFreqPlan_Report,
  fr_LinkFreqPlan_Resource,

  fr_LinkFreqPlan_NeightBors,

{
  fr_LinkFreqPlan_LinkEnd_,
  fr_LinkFreqPlan_NeightBors_,
}


  u_func,

  u_const_db,

  u_reg,

  u_types,


  dxBar, cxBarEditItem, cxClasses, cxLookAndFeels, cxCheckBox, cxButtonEdit
  ;

type
  Tframe_LinkFreqPlan_view = class(Tframe_View_Base)
    FormPlacement1: TFormPlacement;
    PageControl1: TPageControl;
    TabSheet_Insp: TTabSheet;
    TabSheet_Neighbors: TTabSheet;
    TabSheet_Resource: TTabSheet;
    TabSheet_LinkEnd: TTabSheet;
    TabSheet_Report: TTabSheet;
    Panel2: TPanel;
    TabSheet_Neighbors_: TTabSheet;
    TabSheet_LinkEnd_: TTabSheet;

    procedure FormDestroy(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure PageControl1Change(Sender: TObject);
  private
    FId: integer;
    FActivPage: integer;

    Ffrm_Inspector: Tframe_DBInspector_Container;
    Ffrm_LinkFreqPlan_LinkEnd: Tframe_LinkFreqPlan_LinkEnd;
    Ffrm_LinkFreqPlan_Report: Tframe_LinkFreqPlan_Report;
    Ffrm_LinkFreqPlan_NeightBors: Tframe_LinkFreqPlan_NeightBors;
    Ffrm_LinkFreqPlan_Resource: Tframe_LinkFreqPlan_Resource;

 //   Ffrm_LinkFreqPlan_LinkEnd_: Tframe_LinkFreqPlan_LinkEnd_;
//    Ffrm_LinkFreqPlan_NeightBors_: Tframe_LinkFreqPlan_NeightBors_;


  public
    procedure View (aID: integer; aGUID: string); override;
  end;


//====================================================================
// implementation
//====================================================================

implementation

{$R *.DFM}



//--------------------------------------------------------------------
procedure Tframe_LinkFreqPlan_view.FormCreate(Sender: TObject);
//--------------------------------------------------------------------
var
  i: Integer;
begin
  inherited;
  FActivPage:= -1;

  ObjectName:=OBJ_LinkFreqPlan;

  TableName:=TBL_LinkFreqPlan;



  PageControl1.Align:=alClient;

  CreateChildForm(Tframe_DBInspector_Container, Ffrm_Inspector, TabSheet_Insp);
  Ffrm_Inspector.PrepareViewForObject (OBJ_LinkFreqPlan);

  CreateChildForm(Tframe_LinkFreqPlan_LinkEnd,    Ffrm_LinkFreqPlan_LinkEnd, TabSheet_LinkEnd);
  CreateChildForm(Tframe_LinkFreqPlan_Report,     Ffrm_LinkFreqPlan_Report,  TabSheet_Report);
  CreateChildForm(Tframe_LinkFreqPlan_Resource,   Ffrm_LinkFreqPlan_Resource, TabSheet_Resource);

  CreateChildForm(Tframe_LinkFreqPlan_NeightBors, Ffrm_LinkFreqPlan_NeightBors, TabSheet_Neighbors);


  TabSheet_Neighbors_.TabVisible := False;
  TabSheet_LinkEnd_.TabVisible := False;


  {$IFDEF test}
{  CreateChildForm(Tframe_LinkFreqPlan_NeightBors_, Ffrm_LinkFreqPlan_NeightBors_, TabSheet_Neighbors_);
  CreateChildForm(Tframe_LinkFreqPlan_LinkEnd_,    Ffrm_LinkFreqPlan_LinkEnd_, TabSheet_LinkEnd_);

  TabSheet_Neighbors_.TabVisible := True;
  TabSheet_LinkEnd_.TabVisible := True;
}
  {$ENDIF}



  AddComponentProp (PageControl1, PROP_ACTIVE_PAGE);

  i:=PageControl1.ActivePageIndex;

  cxPropertiesStore.RestoreFrom;

  Assert(Assigned(PageControl1.OnChange));

end;


procedure Tframe_LinkFreqPlan_view.FormDestroy(Sender: TObject);
begin

  FreeAndNil(Ffrm_Inspector);
  FreeAndNil(Ffrm_LinkFreqPlan_LinkEnd);
  FreeAndNil(Ffrm_LinkFreqPlan_Report);

  FreeAndNil(Ffrm_LinkFreqPlan_NeightBors);

  FreeAndNil(Ffrm_LinkFreqPlan_Resource);

  {$IFDEF test}
{  FreeAndNil(Ffrm_LinkFreqPlan_LinkEnd_);
  FreeAndNil(Ffrm_LinkFreqPlan_NeightBors_);
}
  {$ENDIF}



  inherited;
end;

// ---------------------------------------------------------------
procedure Tframe_LinkFreqPlan_view.View (aID: integer; aGUID: string);
// ---------------------------------------------------------------
//var
//  bReadOnly: Boolean;
var
  bReadOnly: Boolean;
begin
  FId := aID;
  FActivPage:= -1;

  if not RecordExists (aID) then
    Exit;


  dmOnega_DB_data.LinkFreqPlan_init (aID);

//  Assert(Assigned(dmUser_Security));

   bReadOnly:=dmUser_Security.Object_Edit_ReadOnly();



//  bReadOnly := dmUser_Security.ProjectIsReadOnly;

  Ffrm_Inspector.SetReadOnly (bReadOnly);
  Ffrm_LinkFreqPlan_LinkEnd.SetReadOnly (bReadOnly);
//  Ffrm_LinkFreqPlan_Report.SetReadOnly (b);
  Ffrm_LinkFreqPlan_NeightBors.SetReadOnly (bReadOnly);
  Ffrm_LinkFreqPlan_Resource.SetReadOnly (bReadOnly);

  {$IFDEF test}
{  Ffrm_LinkFreqPlan_LinkEnd_.SetReadOnly (bReadOnly);
  Ffrm_LinkFreqPlan_NeightBors_.SetReadOnly (bReadOnly);
}
  {$ENDIF}

  Ffrm_LinkFreqPlan_NeightBors.Close_Data;


  PageControl1Change(nil);

//  GSPages1Click (GSPages1);
end;


//----------------------------------------------------------------
procedure Tframe_LinkFreqPlan_view.PageControl1Change(Sender: TObject);
// ---------------------------------------------------------------
begin
  if FID=0 then
    Exit;

  if FActivPage=PageControl1.ActivePageIndex then
    Exit;

  FActivPage:= PageControl1.ActivePageIndex;

 // tb_Main.MenuBar:= false;

//  if PageControl1.ActivePage=TabSheet_Insp then Ffrm_Inspector.View (FID) else

  with PageControl1 do
    if ActivePage=TabSheet_Insp       then Ffrm_Inspector.View (FID) else
    if ActivePage=TabSheet_Neighbors  then Ffrm_LinkFreqPlan_NeightBors.View (FID) else
    if ActivePage=TabSheet_Resource   then Ffrm_LinkFreqPlan_Resource.View (FID) else
    if ActivePage=TabSheet_LinkEnd    then Ffrm_LinkFreqPlan_LinkEnd.View (FID) else
    if ActivePage=TabSheet_Report     then Ffrm_LinkFreqPlan_Report.View (FID) else

    {$IFDEF test}
 {   if ActivePage=TabSheet_Neighbors_ then
       Ffrm_LinkFreqPlan_NeightBors_.View (FID) else

    if ActivePage=TabSheet_LinkEnd_   then
      Ffrm_LinkFreqPlan_LinkEnd_.View (FID) else
}
   {$ENDIF}


end;

end.