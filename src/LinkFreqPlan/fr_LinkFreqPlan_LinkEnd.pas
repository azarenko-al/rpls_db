unit fr_LinkFreqPlan_LinkEnd;

interface

uses
  Windows, SysUtils, Classes, Graphics, Controls, Forms, Registry,  ExtCtrls, ComCtrls,      ADODB, ActnList, Menus,
  Db, dxmdaset, TB2Item, TB2Dock, TB2Toolbar,
StdCtrls, Variants,
  cxControls, cxGridCustomView, cxClasses, cxGridLevel, cxGrid, Dialogs,
  cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, cxStyles, dxSkinsCore,
  dxSkinsDefaultPainters, dxSkinscxPCPainter, cxCustomData, cxFilter,
  cxData, cxDataStorage, cxEdit, cxDBData, cxTextEdit, cxDropDownEdit,
  ImgList, ToolWin, cxGridCustomTableView,  cxGridTableView, cxGridBandedTableView,
  cxGridDBBandedTableView,

  u_dxbar,

  dm_Act_LinkEnd,

  cxPropertiesStore,

  u_Storage,

  dm_Onega_DB_data,

//  f_Custom,

 // dm_Language,


  u_LinkFreqPlan_classes,


  u_cx,
  u_cx_grid,

 // u_cx_ini,
  
  dm_LinkFreqPlan_Tools,

  dm_LinkFreqPlan_LinkEnd_View,

  u_LinkFreqPlan_const,

  u_common_const,
  u_Classes,
  u_db,
  u_dlg,
 // u_dx,

  u_func,

//  u_ini,
  u_const_db,
  u_const_str,

  Grids, DBGrids, cxGridDBTableView, dxBar, cxCurrencyEdit,
  dxSkinsdxBarPainter
  ;

type
 // Tframe_LinkFreqPlan_LinkEnd = class(Tfrm_Custom)
  Tframe_LinkFreqPlan_LinkEnd = class(TForm)
    PopupMenu11111: TPopupMenu;
    N1: TMenuItem;
    N2: TMenuItem;
    N3: TMenuItem;
    N4: TMenuItem;
    ActionList111: TActionList;
    act_Set_Common_requirements: TAction;
    act_Set_Freq_count_by_Fixed: TAction;
    act_Forbid_Freqs: TAction;
    act_Allow_Freqs: TAction;
    act_Set_Priority: TAction;
    N9: TMenuItem;
    N10: TMenuItem;
    N11: TMenuItem;
    act_Set_Traffic: TAction;
    N6: TMenuItem;
    act_Set_Distribute: TAction;
    act_Clear_Distribute: TAction;
    act_Set_Fixed: TAction;
    act_Clear_Fixed: TAction;
    N12: TMenuItem;
    N13: TMenuItem;
    N14: TMenuItem;
    N15: TMenuItem;
    N16: TMenuItem;
    N17: TMenuItem;
    act_Interference: TAction;
    act_Link_del1: TAction;
    act_Link_Add1: TAction;
    ds_mem_LinkEnd: TDataSource;
    mem_LinkEnd1: TdxMemData;
    TBDock: TTBDock;
    TBToolbar: TTBToolbar;
    TBSubmenuItem1: TTBSubmenuItem;
    TBItem1: TTBItem;
    TBItem2: TTBItem;
    TBSeparatorItem1: TTBSeparatorItem;
    TBItem6: TTBItem;
    TBItem5: TTBItem;
    TBSubmenuItem2: TTBSubmenuItem;
    TBItem7: TTBItem;
    TBItem8: TTBItem;
    TBItem9: TTBItem;
    TBItem10: TTBItem;
    StateImages: TImageList;
    TBItem3: TTBItem;
    TBSeparatorItem2: TTBSeparatorItem;
    act_Save_Fixed: TAction;
    TBToolbar2: TTBToolbar;
    tb_Set_Distr: TTBItem;
    tb_Clear_Distr: TTBItem;
    tb_Set_Fix: TTBItem;
    tb_Clear_Fix: TTBItem;
    img_Tools: TImageList;
    TBSubmenuItem4: TTBSubmenuItem;
    act_Calc_Interference: TAction;
    TBItem11: TTBItem;
    act_Restore_Fixed: TAction;
    TBItem12: TTBItem;
    tb_distributed: TTBItem;
    tb_fixed: TTBItem;
    TBSeparatorItem3: TTBSeparatorItem;
    TBItem13: TTBItem;
    act_CreateMap_Interference: TAction;
    act_Save_to_Excel: TAction;
    ADOQuery1: TADOQuery;
    Panel1: TPanel;
    StatusBar1: TStatusBar;
    PN_BOTTOM: TPanel;
    GroupBox1: TGroupBox;
    Button1: TButton;
    cb_CalcCIA_after_Distrib: TCheckBox;
    cb_Make_CIA_maps: TCheckBox;
    cb_Channel_type: TComboBox;
    GroupBox2: TGroupBox;
    Button3: TButton;
    Button4: TButton;
    Button5: TButton;
    Button6: TButton;
    Button8: TButton;
    GroupBox3: TGroupBox;
    Button2: TButton;
    Button10: TButton;
    cxGrid1: TcxGrid;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1DBBandedTableView1: TcxGridDBBandedTableView;
    col__Property_Name: TcxGridDBBandedColumn;
    col__Link_Name: TcxGridDBBandedColumn;
    col__linkend_name: TcxGridDBBandedColumn;
    col__LinkEnd_ID: TcxGridDBBandedColumn;
    col__Length_km: TcxGridDBBandedColumn;
    col__freq_req_count: TcxGridDBBandedColumn;
    col__priority: TcxGridDBBandedColumn;
    col__band: TcxGridDBBandedColumn;
    col__subband: TcxGridDBBandedColumn;
    col__Freq_Distributed_Count: TcxGridDBBandedColumn;
    col__Freq_Distributed: TcxGridDBBandedColumn;
    col__freq_distributed_tx: TcxGridDBBandedColumn;
    col__freq_distributed_rx: TcxGridDBBandedColumn;
    col__Calc_Order: TcxGridDBBandedColumn;
    col__Distributed_Min_raznos: TcxGridDBBandedColumn;
    col__Freq_Fixed_Count: TcxGridDBBandedColumn;
    col__Freq_Fixed: TcxGridDBBandedColumn;
    col__freq_fixed_tx: TcxGridDBBandedColumn;
    col__freq_fixed_rx: TcxGridDBBandedColumn;
    col__Fixed_Min_raznos: TcxGridDBBandedColumn;
    col__Freq_Forbidden: TcxGridDBBandedColumn;
    col__Freq_Allow: TcxGridDBBandedColumn;
    col__CIA: TcxGridDBBandedColumn;
    col__CI: TcxGridDBBandedColumn;
    col__CA: TcxGridDBBandedColumn;
    col__threshold_degradation_cia: TcxGridDBBandedColumn;
    col__LinkEndType_band_ID: TcxGridDBBandedColumn;
    col__linkFreqPlan_threshold_degradation: TcxGridDBBandedColumn;
    col__linkFreqPlan_min_CI_db: TcxGridDBBandedColumn;
    col__PROPERTY_ID: TcxGridDBBandedColumn;
    col__linkendtype_name: TcxGridDBBandedColumn;
    col__linkendtype_band_id1: TcxGridDBBandedColumn;
    col__linkendtype_mode_id: TcxGridDBBandedColumn;
    col__FREQ_MIN_LOW: TcxGridDBBandedColumn;
    col__CHANNEL_WIDTH: TcxGridDBBandedColumn;
    col__TXRX_SHIFT: TcxGridDBBandedColumn;
    col__CHANNEL_COUNT: TcxGridDBBandedColumn;
    col__BANDWIDTH: TcxGridDBBandedColumn;
    col__BANDWIDTH_TX_3: TcxGridDBBandedColumn;
    col__BANDWIDTH_TX_30: TcxGridDBBandedColumn;
    SaveDialog1: TSaveDialog;
    cxStyleRepository1: TcxStyleRepository;
    cxStyle_Red: TcxStyle;
    cxStyle1: TcxStyle;
    col_channel_type: TcxGridDBBandedColumn;
    col_channel_number: TcxGridDBBandedColumn;
    col_TX_FREQ_MHZ: TcxGridDBBandedColumn;
    col_RX_FREQ_MHZ: TcxGridDBBandedColumn;
    col_LinkEndType_name: TcxGridDBBandedColumn;
    col_ID: TcxGridDBBandedColumn;
    N5: TMenuItem;
    act_Linkend_Set_high: TAction;
    act_Linkend_Set_low: TAction;
    actLinkendSethigh1: TMenuItem;
    actLinkendSetlow1: TMenuItem;
    N18: TMenuItem;
    act_Get_LinkendType: TAction;
    actGetLinkendType1: TMenuItem;
    col_lat: TcxGridDBBandedColumn;
    col_lon: TcxGridDBBandedColumn;
    col_location_type: TcxGridDBBandedColumn;
    dxBarManager1: TdxBarManager;
    dxBarManager1Bar1: TdxBar;
    dxBarButton1: TdxBarButton;
    dxBarButton2: TdxBarButton;
    dxBarSubItem1: TdxBarSubItem;
    dxBarSubItem2: TdxBarSubItem;
    dxBarSubItem3: TdxBarSubItem;
    dxBarButton3: TdxBarButton;
    dxBarButton4: TdxBarButton;
    dxBarButton5: TdxBarButton;
    dxBarButton6: TdxBarButton;
    dxBarButton7: TdxBarButton;
    dxBarButton8: TdxBarButton;
    dxBarSubItem4: TdxBarSubItem;
    dxBarButton9: TdxBarButton;
    dxBarButton10: TdxBarButton;
    dxBarButton11: TdxBarButton;
    dxBarButton12: TdxBarButton;
    dxBarButton13: TdxBarButton;
    dxBarButton14: TdxBarButton;
    dxBarButton15: TdxBarButton;
    dxBarButton16: TdxBarButton;
    dxBarButton17: TdxBarButton;
    dxBarButton18: TdxBarButton;

//    procedure dxDBGridCustomDrawCell(Sender: TObject;
//      ACanvas: TCanvas; ARect: TRect; ANode: TdxTreeListNode;
//      AColumn: TdxTreeListColumn; ASelected, AFocused,
//      ANewItemRow: Boolean; var AText: String; var AColor: TColor;
//      AFont: TFont; var AAlignment: TAlignment; var ADone: Boolean);

    procedure FormCreate(Sender: TObject);
  //  procedure dxDBGridEdited(Sender: TObject; Node: TdxTreeListNode);
    procedure _All_Actions(Sender: TObject);
    procedure ActionList111Update(Action: TBasicAction; var Handled: Boolean);
    procedure ADOQuery1sdfgsChange(Sender: TField);
    procedure FormDestroy(Sender: TObject);
    procedure TBItemClick(Sender: TObject);
//    procedure col_ch_type_Change(Sender: TObject);
  //  procedure dxDBGridHotTrackNode(Sender: TObject;
  //    AHotTrackInfo: TdxTreeListHotTrackInfo; var ACursor: TCursor);
    procedure cb_Channel_typeChange(Sender: TObject);
//    procedure col_channel_type_distrCloseUp(Sender: TObject; var Value: Variant; var Accept: Boolean);
//    procedure col_channel_type_distrValidate(Sender: TObject; var ErrorText: string; var Accept: Boolean);
//    procedure col_channel_type_fixedCloseUp(Sender: TObject; var Value: Variant; var Accept: Boolean);
//    procedure mem_LinkEndCalcFields(DataSet: TDataSet);
    procedure cxGrid1DBBandedTableView1StylesGetContentStyle(
      Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
      AItem: TcxCustomGridTableItem; out AStyle: TcxStyle);
    procedure col__Freq_DistributedStylesGetContentStyle(
      Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
      AItem: TcxCustomGridTableItem; out AStyle: TcxStyle);
    procedure col__Freq_FixedStylesGetContentStyle(
      Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
      AItem: TcxCustomGridTableItem; out AStyle: TcxStyle);
//    procedure col__bandStylesGetContentStyle(
 //     Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
   //   AItem: TcxCustomGridTableItem; out AStyle: TcxStyle);
  private
    FID: integer;

    FRegPath : string;

    FDataset: TDataset;

//    FLinkFreqPlan_LinkEnd_List: TLinkFreqPlan_LinkEnd_List_;


//    FLinkFreqPlan_Min_CI: double;
//    FLinkFreqPlanDegradation: double;

 //   FArrLinkEndRec: TLinkFreqPlan_LinkEnd_RecArray;

//    procedure ChangeChannelType(Sender: TObject; aNewValue: string);

//    function FoundIn_LinkEnd_Array(aLinkEndID, aPropertyID: integer; aIsFixed:
 //       Boolean): double;

//    procedure RefreshSummary;
  public
    procedure SetReadOnly(Value: Boolean);
    procedure Set_Distribute;
    procedure View(aFreqPlan_Link_ID: integer);
  end;


//===========================================================================//
implementation {$R *.dfm}
//===========================================================================//


resourcestring
  BAND_CAPTION_main        = '��� ���';
  BAND_CAPTION_distributed1 = '������������ (%d �� %d)';
  BAND_CAPTION_fixed1       = '���������� (%d �� %d)';

  BAND_CAPTION_distributed = '������������';
  BAND_CAPTION_fixed       = '����������';


  BAND_CAPTION_forbidden   = '����������� �������';
  BAND_CAPTION_allow       = '����������� �������';
  BAND_CAPTION_interference= '�������������';


  CAPTION_RecNo               = '�';
  CAPTION_LinkEnd_Name        = '���';
  CAPTION_Link_Name           = '��� ���';
  CAPTION_Property_Name       = '��� ��������';
  CAPTION_LinkEnd_ID          = '��� ID';
  CAPTION_LENGTH_m            = '����� [m]';
  CAPTION_LENGTH_km           = '����� [km]';
  CAPTION_Freq_required_count = '����';
  CAPTION_Priority            = '���������';

  CAPTION_Count               = '���-��';
  CAPTION_Calc_Order          = 'N ����';

  CAPTION_Freqs_Count         = '������ ������';
  CAPTION_Freqs_Raznos        = '������ ������';
  CAPTION_Freqs_Values        = '�������� ������';

  CAPTION_BAND             = '�������� [GHz]';
  CAPTION_SUBBAND_NAME     = '�����������';
  CAPTION_CH_TYPE          = '��� ������';
  CAPTION_threshold_degradation = '���������� ���������������� [dB]';

  ACTION_AddLink           = '�������� �� ��������';
  ACTION_DelLink           = '������� �� ��������';

const
  BAND_distributed = 1;
  BAND_fixed       = 2;
  BAND_forbidden   = 3;

(*const
  COLOR_ODD_RECORD  = clWindow;
  COLOR_EVEN_RECORD = clInfoBk;
*)

//--------------------------------------------------------------------
procedure Tframe_LinkFreqPlan_LinkEnd.FormCreate(Sender: TObject);
//--------------------------------------------------------------------
var
  k: Integer;
begin
  inherited;

  bar_Init(dxBarManager1);

  FRegPath := g_Storage.GetPathByClass(ClassName) + '1\';;

//  FDataset:= mem_LinkEnd;

  TdmLinkFreqPlan_LinkEnd_View.Init;

//   k:=dmLinkFreqPlan_LinkEnd_View.sp_LinkFreqPlan_LinkEnd_.Fields.Count;


  FDataset:= dmLinkFreqPlan_LinkEnd_View.sp_LinkFreqPlan_LinkEnd_;
  FDataset.OnCalcFields:= nil;

  dmLinkFreqPlan_LinkEnd_View.Data.Linkends.Clear;


  ds_mem_LinkEnd.DataSet:=FDataset;





 //  k:=dmLinkFreqPlan_LinkEnd_View.sp_LinkFreqPlan_LinkEnd_.Fields.Count;


//  FRegPath :=FRegPath + '1';

  act_Get_LinkendType.Caption:='������� ������������';



  Panel1.Align := alClient;
 // dxDBGrid.Align := alClient;

  tb_Set_Distr.Hint  := '������������';
  tb_Clear_Distr.Hint:= '�������� ��������������';
  tb_Set_Fix.Hint    := '���������';
  tb_Clear_Fix.Hint  := '�������� ������������';

  // -------------------------------------------------------------------

//  cxGrid1DBBandedTableView1.

// �������� BAND-��
  with cxGrid1DBBandedTableView1 do
  begin
    Bands[0].Caption    := BAND_CAPTION_main;
    Bands[1].Caption    := BAND_CAPTION_distributed;
    Bands[2].Caption    := BAND_CAPTION_fixed;
    Bands[3].Caption    := BAND_CAPTION_forbidden;
    Bands[4].Caption    := BAND_CAPTION_allow;
    Bands[5].Caption    := BAND_CAPTION_interference;
  end;


///////  act_Grid_Restore_default.Caption := STR_GRID_Restore_default;


// �������� �������
//  col_RecNo.Caption                  := CAPTION_RecNo;
  col__LinkEnd_Name.Caption           := CAPTION_LinkEnd_Name;
  col__Property_Name.Caption          := CAPTION_Property_Name;
  col__Link_Name.Caption              := CAPTION_Link_Name;
  col__LinkEnd_ID.Caption             := CAPTION_LinkEnd_ID;
  col__Length_km.Caption              := CAPTION_LENGTH_km;
  col__Freq_req_count.Caption         := CAPTION_Freq_required_count;
  col__Priority.Caption               := CAPTION_Priority;

  col__Freq_Distributed_Count.Caption := CAPTION_Count;
  col__Freq_Distributed.Caption       := CAPTION_Freqs_Count + ' (�����)';
  col__Calc_Order.Caption             := CAPTION_Calc_Order;
  col__Distributed_Min_raznos.Caption := CAPTION_Freqs_Raznos + ' (�����)';

  col__freq_distributed_tx.Caption    := '������� TX (MHz)';
  col__freq_distributed_rx.Caption    := '������� RX (MHz)';

  col__freq_Fixed_Count.Caption            := CAPTION_Count;
  col__freq_Fixed.Caption                  := CAPTION_Freqs_Count + ' (����)';

//  col__freq_Fixed_Min_raznos.Caption       := CAPTION_Freqs_Raznos + ' (����)';

  col__freq_fixed_tx.Caption          := '������� TX (MHz)';
  col__freq_fixed_rx.Caption          := '������� RX (MHz)';

  col__Freq_Forbidden.Caption        := CAPTION_Freqs_Values + ' (����)';
  col__Freq_Allow.Caption            := CAPTION_Freqs_Values + ' (����)';

  col__Band.Caption                  := CAPTION_BAND;
  col__subband.Caption             := CAPTION_SUBBAND_NAME;
//  col__channel_type_Distributed.Caption    := CAPTION_CH_TYPE + ' (�����)';

//  col__channel_type_fixed.Caption    := CAPTION_CH_TYPE + ' (����)';

//  col__channel_type_distr.Items.Text := ''+ CRLF + 'low'+ CRLF +'high';
//  col__channel_type_fixed.Items.Text := col__channel_type_distr.Items.Text;

  col__CIA.Caption := 'CIA [dB]';
  col__CI.Caption  := 'CI [dB]' ;
  col__CA.Caption  := 'CA [dB]' ;
  col__threshold_degradation_cia.Caption:= CAPTION_threshold_degradation;


  col_channel_type.Caption:= CAPTION_CH_TYPE;

  // -------------------------------------------------------------------

  //cxGrid1DBBandedTableView1.RestoreFromRegistry(FRegPath + cxGrid1DBBandedTableView1.Name);

(*
//  col__RecNo.Caption                  := CAPTION_RecNo;
  col__LinkEnd_Name.Caption           := CAPTION_LinkEnd_Name;
  col__Property_Name.Caption          := CAPTION_Property_Name;
  col__Link_Name.Caption              := CAPTION_Link_Name;
  col__LinkEnd_ID.Caption             := CAPTION_LinkEnd_ID;
  col__Length_km.Caption              := CAPTION_LENGTH_km;
  col__Freq_req_count.Caption         := CAPTION_Freq_required_count;
  col__Priority.Caption               := CAPTION_Priority;

  col__Freq_Distributed_Count.Caption      := CAPTION_Count;
  col__Freq_Distributed.Caption            := CAPTION_Freqs_Count + ' (�����)';
  col__Calc_Order.Caption             := CAPTION_Calc_Order;
  col__Freq_Distrubuted_Min_raznos.Caption := CAPTION_Freqs_Raznos + ' (�����)';

  col__freq_distributed_tx.Caption    := '������� ��� (MHz)';
  col__freq_distributed_rx.Caption    := '������� ��� (MHz)';

  col__freq_Fixed_Count.Caption            := CAPTION_Count;
  col__freq_Fixed.Caption                  := CAPTION_Freqs_Count + ' (����)';
  col__freq_Fixed_Min_raznos.Caption       := CAPTION_Freqs_Raznos + ' (����)';

  col__freq_fixed_tx.Caption          := '������� ��� (MHz)';
  col__freq_fixed_rx.Caption          := '������� ��� (MHz)';

  col__Freq_Forbidden.Caption        := CAPTION_Freqs_Values + ' (����)';                             CAPTION_CH_TYPE
  col__Freq_Allow.Caption            := CAPTION_Freqs_Values + ' (����)';

  col__range.Caption               := CAPTION_BAND;
  col__subband.Caption           := CAPTION_SUBBAND_NAME;
  col__ch_type_distributed.Caption :=  + ' (�����)';
  col__ch_type_fixed.Caption       := CAPTION_CH_TYPE + ' (����)';

*)

 // col__ch_type_distributed.str

 // col__ch_type_distributed.Properties.i Items.Text := ''+ CRLF + 'Low'+ CRLF +'High';
 // col__ch_type_fixed.Items.Text := col__ch_type_distr.Items.Text;
(*
  col__CIA.Caption := 'CIA [dB]';
  col__CI.Caption  := 'CI [dB]' ;
  col__CA.Caption  := 'CA [dB]' ;
  col__threshold_degradation_cia.Caption:= CAPTION_threshold_degradation;

  // -------------------------------------------------------------------

*)

  //act_Link_Add.Caption            := ACTION_AddLink;
//  act_Link_del.Caption            := ACTION_DelLink;


  act_Linkend_Set_high.Caption := '���������� HIGH';
  act_Linkend_Set_low.Caption := '���������� LOW';





  with cb_Channel_type do
  begin
    Items.Clear;
    Items.Add('�� �����. �.');
    Items.Add('�� ����. �.');
  end;

//  AddComponentProp(GSPages11, 'ActivePage');
//  AddComponentProp(cb_Channel_type, 'ItemIndex');

(*
  AddComponentProp(cb_Channel_type, 'Text');
  AddComponentProp(cb_CalcCIA_after_Distrib, 'Checked');
  AddComponentProp(cb_Make_CIA_maps,         'Checked');
  cxPropertiesStore.RestoreFrom;

*)


  cb_Channel_type.ItemIndex:=0;

  col__priority.Visible :=False;





{  with gl_Reg do
  begin
  //  CurrentRegPath:= FRegPath;
    CurrentRegPath := FRegPath;

    BeginGroup(Self, 'Forms\'+ClassName);
    AddControl(GSPages11,  [PROP_ACTIVE_PAGE_INDEX]);
    AddControl(cb_Channel_type,     ['ItemIndex']);

  end;}



  with TRegIniFile.Create (FRegPath) do
  begin
    cb_CalcCIA_after_Distrib.Checked:= ReadBool('', cb_CalcCIA_after_Distrib.Name, False);
    cb_Make_CIA_maps.Checked        := ReadBool('', cb_Make_CIA_maps.Name, True);
    Free;
  end;



  tb_fixed.Checked       := IIF(cb_Channel_type.ItemIndex=1, True, False);
  tb_distributed.Checked := IIF(cb_Channel_type.ItemIndex=0, True, False);

  tb_fixed.ImageIndex       := IIF(tb_fixed.Checked, 1, 0);
  tb_distributed.ImageIndex := IIF(tb_fixed.Checked, 0, 1);


  cxGrid1.Align:=alClient;


 // dxDBGrid.SaveToRegistry (FRegPath+ dxDBGrid.Name+'_default');

//
//
//  if dx_CheckRegistry(   FRegPath+ dxDBGrid.Name) then
//    dxDBGrid.LoadFromRegistry (FRegPath+ dxDBGrid.Name)
//  else
//    dxDBGrid.ApplyBestFit(nil);



//  dmLinkFreqPlan_LinkEnd_View.InitDB (FDataset);




//////////////  dx_CheckColumnSizes_DBGrid(dxDBGrid);

  PN_BOTTOM.Height:= 113;

 // DBGrid2.DataSource := dmLinkFreqPlan_LinkEnd_View.DataSource1;


//  FLinkFreqPlan_LinkEnd_List:=TLinkFreqPlan_LinkEnd_List_.Create;

/////////  TdmLanguage.CheckForm(Self);


  col__Distributed_Min_raznos.DataBinding.FieldName:= FLD_Distributed_min_raznos;


  col__freq_distributed_tx.DataBinding.FieldName:= FLD_FREQ_DISTRIBUTED_TX_STR;
  col__freq_distributed_rx.DataBinding.FieldName:= FLD_FREQ_DISTRIBUTED_RX_STR;

  col__freq_fixed_tx.DataBinding.FieldName:= FLD_FREQ_FIXED_TX_STR;
  col__freq_fixed_rx.DataBinding.FieldName:= FLD_FREQ_FIXED_RX_STR;

end;

//--------------------------------------------------------------------
procedure Tframe_LinkFreqPlan_LinkEnd.FormDestroy(Sender: TObject);
//--------------------------------------------------------------------
begin
//  FreeAndNil(FLinkFreqPlan_LinkEnd_List);




 // cxGrid1DBBandedTableView1.StoreToRegistry(FRegPath + cxGrid1DBBandedTableView1.Name);


 // gl_Reg.SaveAndClearGroup (Self);


  with TRegIniFile.Create (FRegPath) do
  begin
    WriteBool('', cb_CalcCIA_after_Distrib.Name,  cb_CalcCIA_after_Distrib.Checked);
    WriteBool('', cb_Make_CIA_maps.Name,          cb_Make_CIA_maps.Checked);
    Free;
  end;


//////////  dxDBGrid.SaveToRegistry (FRegPath+ dxDBGrid.Name+'_LinkFreqPlan');

  inherited;
end;

//-------------------------------------------------------------------
procedure Tframe_LinkFreqPlan_LinkEnd.View(aFreqPlan_Link_ID: integer);
//-------------------------------------------------------------------
var
  k: Integer;
begin
  FID := aFreqPlan_Link_ID;

  ////dmLinkFreqPlan_LinkEnd_View.OpenDB (FID, FDataset);


  dmLinkFreqPlan_LinkEnd_View.OpenDB_new (FID );

  k:=dmLinkFreqPlan_LinkEnd_View.Data.Linkends.Count ;


//  FDataset.First;

  //cxGrid1DBBandedTableView1.



 // cxGrid2DBTableView1.Cre  R estoreDefaults;


//  Exit;


/////  RefreshSummary;


//  FLinkFreqPlan_LinkEnd_List.LoadFromDataset (mem_LinkEnd);

  cx_Grid_Check (cxGrid1DBBandedTableView1);


(*  FLinkFreqPlan_Min_CI    := dmLinkFreqPlan.GetDoubleFieldValue(FID, FLD_MIN_CI_dB);
  FLinkFreqPlanDegradation:= dmLinkFreqPlan.GetDoubleFieldValue(FID, FLD_THRESHOLD_DEGRADATION);
*)
end;




//--------------------------------------------------------------------
procedure Tframe_LinkFreqPlan_LinkEnd._All_Actions(Sender: TObject);
//--------------------------------------------------------------------
//
//    //--------------------------------------------------
//    procedure SetValue(aFieldName: string; aValue: Variant);
//    begin
//      with dxDBGrid.DataSource.DataSet do
//      begin
//        Edit;
//        FieldByName(aFieldName).Value := aValue;
//        Post;
//      end;
//    end;


var i: Integer;
   
  bIsHigh: Boolean;

  iID: Integer;
  sFileName: string;
    oIDList: TIDList;
  s: string;

  oSList: TStringList;

begin
 // CursorHourGlass;

//
//  if Sender = act_GRID_Restore_default then
//    dxDBGrid.LoadFromRegistry (FRegPath+ dxDBGrid.Name+'_default')
//  else

  s:=Sender.ClassName;



//
//// ---------------------------------------------------------------
//function cx_BandedTableView_GetSelectedList(aBandedTableView: TcxGridDBBandedTableView):
//    TStringList;



  //---------
  if (Sender = act_Get_LinkendType) then
  //---------
  begin
    oSList:=cx_BandedTableView_GetSelectedList(cxGrid1DBBandedTableView1, FLD_LINKEND_ID);

    oIDList:=TIDList.Create;

    for I := 0 to oSList.Count - 1 do    // Iterate
      oIDList.AddID ( Integer( oSList.Objects[i] ));

    dmAct_LinkEnd.Dlg_Change_LinkEndType (nil, oIDList);

    View(FID);


    FreeAndNil(oIDList);
    FreeAndNil(oSList);


  end else


  //---------
  if (Sender = act_Linkend_Set_high)  or ((Sender = act_Linkend_Set_low)) then
  //---------
  begin
    bIsHigh:=Sender = act_Linkend_Set_high;

  //  s:=IIF(bIsHigh,'high','low');

    oSList:=cx_BandedTableView_GetSelectedList(cxGrid1DBBandedTableView1, FLD_LINKEND_ID);


    for I := 0 to oSList.Count - 1 do    // Iterate
    begin
      iID:= Integer ( oSList.Objects[i]);

      dmOnega_DB_data.ExecStoredProc_ ('sp_LinkEnd_Update',
          [FLD_ID, iID,
           FLD_CHANNEL_TYPE, IIF(bIsHigh,'high','low')
          ] );

    end;    // for


    FreeAndNil(oSList);

    View(FID);

//    if dmLinkFreqPlan_Tools.Set_Common_requirements(FID) then
 //     View(FID);
  end else


  //---------
  if Sender = act_Set_Common_requirements then
  begin
    if dmLinkFreqPlan_Tools.Set_Common_requirements(FID) then
      View(FID);
  end else

  // ---------------------------------------------------------------
  if Sender = act_Set_Priority then
  // -------------------------------------------------------------------
  begin
    if dmLinkFreqPlan_Tools.Set_Priority(FDataset) then
      View(FID);
  end else

  //---------
  if Sender = act_Set_Freq_count_by_Fixed then
  begin
    dmLinkFreqPlan_Tools.Set_Freq_count_by_Fixed(FDataset);
//      View(FID);
  end else

  // -------------------------------------------------------------------
  if Sender = act_Forbid_Freqs then
  // -------------------------------------------------------------------
  begin
    dmLinkFreqPlan_Tools.Forbid_Freqs(FDataset, FID);
//      View(FID);
  end else

  // -------------------------------------------------------------------
  if Sender = act_Allow_Freqs then
  // -------------------------------------------------------------------
  begin
     dmLinkFreqPlan_Tools.Allow_Freqs(FDataset, FID);
    //  View(FID);
  end else

  // ---------------------------------------------------------------
  if Sender = act_Calc_Interference then
  // -------------------------------------------------------------------
  begin
/////    with dmLinkFreqPlan_CIA.Options do
/////      ChannelsKind:= IIF(tb_fixed.Checked, opUseFixedChannels, opUseDistrChannels);

    dmLinkFreqPlan_Tools.Calc_Interference(FID, tb_fixed.Checked);

    if cb_Make_CIA_maps.Checked then
      dmLinkFreqPlan_Tools.CreateMap_Interference(FID);

    View(FID);

  end else

  // -------------------------------------------------------------------
  if Sender = act_CreateMap_Interference then
  // -------------------------------------------------------------------
  begin
    dmLinkFreqPlan_Tools.CreateMap_Interference(FID);
  end else

  // -------------------------------------------------------------------
  if Sender = act_Set_Distribute then
  // -------------------------------------------------------------------
  begin
    Set_Distribute();

{    oIDList:= TIDList.Create;

    for i := 0 to dxDBGrid.Count-1 do
      oIDList.AddID(dxDBGrid.Items[i].Values[col_LinkEnd_ID.Index]);

    dmLinkFreqPlan_Tools.Set_Distribute(FID, oIDList);

    View(FID);

    if cb_CalcCIA_after_Distrib.Checked then
    begin
//      with dmLinkFreqPlan_CIA.Options do
 //       ChannelsKind:= IIF(tb_fixed.Checked, opUseFixedChannels, opUseDistrChannels);

      dmLinkFreqPlan_Tools.Calc_Interference(FID, tb_fixed.Checked);

      if cb_Make_CIA_maps.Checked then
        dmLinkFreqPlan_Tools.CreateMap_Interference(FID);
    end;

    View(FID);
    oIDList.Free;
}
  end else

  //---------
  if Sender = act_Clear_Distribute then
  begin
    if dmLinkFreqPlan_Tools.Distributed_Clear(FID, tb_distributed.Checked) then
      View(FID);
  end else

  //---------
  if Sender = act_Set_Fixed then
  begin
    if dmLinkFreqPlan_Tools.Set_Fixed(FID) then
      View(FID);
  end else

  //---------
  if Sender = act_Restore_Fixed then
  begin
     if dmLinkFreqPlan_Tools.Restore_Fixed(FID) then
       View(FID);
  end else

{
 //------------
  if Sender = act_Setup_Grid then begin
   //   dx_Dlg_Customize_DBGrid (dxDBGrid);
  end else
}
  //------------
  if Sender = act_Save_to_Excel then
  begin

    if SaveDialog1.Execute then
    begin
      cx_ExportGridToExcel(SaveDialog1.FileName, cxGrid1);

  //    ExportGrid4ToExcel(SaveDialog1.FileName, cxGrid1);

      ShellExec(SaveDialog1.FileName);
    end;


  //  sFileName:= ShowSaveFileDialog('���������� � Excel', '*.xls', FILTER_EXCEL, '��� ���'); //
  //  dxDBGrid.SaveAllToTextFile(sFileName);
  end else

  //---------
  if Sender = act_Clear_Fixed then
  begin
    if dmLinkFreqPlan_Tools.Clear_Fixed(FID, tb_fixed.Checked) then
      View(FID);
  end else

  //---------
  if Sender = act_Save_Fixed then
  begin
    if dmLinkFreqPlan_Tools.Fixed_Save(FID) then
      View(FID);

  end else
     raise Exception.Create(Sender.ClassName) ;

 // CursorDefault;
end;

//--------------------------------------------------------------------
procedure Tframe_LinkFreqPlan_LinkEnd.ActionList111Update(Action: TBasicAction;
  var Handled: Boolean);
//--------------------------------------------------------------------
begin

 // act_Link_del.Enabled := not (dxDBGrid.DataSource.DataSet.IsEmpty);
  Handled := True;

end;

procedure Tframe_LinkFreqPlan_LinkEnd.ADOQuery1sdfgsChange(Sender: TField);
begin
  
end;


//-------------------------------------------------------------------
procedure Tframe_LinkFreqPlan_LinkEnd.TBItemClick(Sender: TObject);
//-------------------------------------------------------------------
var oItem: TTBItem;
begin
  oItem:= TTBItem(sender);

  if oItem.Checked then oItem.ImageIndex:= 1
                   else oItem.ImageIndex:= 0;

  //---------------------------
  if Sender=tb_distributed then
    tb_fixed.Checked:= IIF(tb_distributed.Checked, False, True);

  //---------------------------
  if Sender=tb_fixed then
    tb_distributed.Checked:= IIF(tb_fixed.Checked, False, True);

  tb_fixed.ImageIndex       := IIF(tb_fixed.Checked, 1, 0);
  tb_distributed.ImageIndex := IIF(tb_fixed.Checked, 0, 1);

  cb_Channel_type.ItemIndex:= IIF(tb_fixed.Checked, 1, 0);

//  dxDBGrid.Refresh;

end;

//-------------------------------------------------------------------
procedure Tframe_LinkFreqPlan_LinkEnd.cb_Channel_typeChange(Sender: TObject);
//-------------------------------------------------------------------
begin
  inherited;

  case cb_Channel_type.ItemIndex of
     0: begin tb_distributed.Checked:= True;  tb_fixed.Checked:= False; end;
     1: begin tb_distributed.Checked:= False; tb_fixed.Checked:= True; end;
  end;

  tb_fixed.ImageIndex       := IIF(tb_fixed.Checked, 1, 0);
  tb_distributed.ImageIndex := IIF(tb_fixed.Checked, 0, 1);

 // dxDBGrid.Refresh;

end;



procedure Tframe_LinkFreqPlan_LinkEnd.SetReadOnly(Value: Boolean);
begin
//  cxGrid1DBTableView1.OptionsData.Editing := not Value;
//  FReadOnly := Value;

 // ADOStoredProc1.LockType := IIF(Value, ltReadOnly, ltBatchOptimistic);
  //SetFormActionsEnabled(Self, not Value);

//  ADOStoredProc1.LockType := IIF(dmUser_Security.ProjectIsReadOnly, ltReadOnly, ltBatchOptimistic);
//  SetFormActionsEnabled(Self, not dmUser_Security.ProjectIsReadOnly);

end;


//-------------------------------------------------------------------
procedure Tframe_LinkFreqPlan_LinkEnd.Set_Distribute;
//-------------------------------------------------------------------
var
//  oIDList: TIDList;
  i: Integer;
begin

 // oIDList:= TIDList.Create;

{
  for i := 0 to dxDBGrid.Count-1 do
    oIDList.AddID(dxDBGrid.Items[i].Values[col_LinkEnd_ID.Index]);
}
  dmLinkFreqPlan_Tools.Run_Distribute(FID);//, oIDList);

//  View(FID);

  if cb_CalcCIA_after_Distrib.Checked then
  begin
//      with dmLinkFreqPlan_CIA.Options do
//       ChannelsKind:= IIF(tb_fixed.Checked, opUseFixedChannels, opUseDistrChannels);

    dmLinkFreqPlan_Tools.Calc_Interference(FID, tb_fixed.Checked);

    if cb_Make_CIA_maps.Checked then
      dmLinkFreqPlan_Tools.CreateMap_Interference(FID);
  end;

  View(FID);

 // oIDList.Free;

end;

procedure Tframe_LinkFreqPlan_LinkEnd.cxGrid1DBBandedTableView1StylesGetContentStyle(
  Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
  AItem: TcxCustomGridTableItem; out AStyle: TcxStyle);
var
  v: Variant;
begin
//

  v:=ARecord.Values[col__LinkEndType_band_ID.Index];
  if VarIsNull(v) then
    AStyle:=cxStyle_Red;

end;


procedure Tframe_LinkFreqPlan_LinkEnd.col__Freq_DistributedStylesGetContentStyle(
  Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
  AItem: TcxCustomGridTableItem; out AStyle: TcxStyle);
var
  iID: Integer;
begin
  iID:=ARecord.Values[col_ID.Index];

//  Assert( dmLinkFreqPlan_LinkEnd_View.Data.Linkends.Count > 0);

  if not dmLinkFreqPlan_LinkEnd_View.Data.Linkends.IsValid(iID, 'distr') then
   AStyle:=cxStyle_Red;

//
end;

procedure Tframe_LinkFreqPlan_LinkEnd.col__Freq_FixedStylesGetContentStyle(
  Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
  AItem: TcxCustomGridTableItem; out AStyle: TcxStyle);
var
  iID: Integer;
begin
  iID:=ARecord.Values[col_ID.Index];

 // Assert( dmLinkFreqPlan_LinkEnd_View.Data.Linkends.Count > 0);

  if not dmLinkFreqPlan_LinkEnd_View.Data.Linkends.IsValid(iID, 'fixed') then
   AStyle:=cxStyle_Red;


end;

end.

