unit u_LinkFreqPlan_classes;

interface

uses
  Classes, SysUtils,  DB,
  
  u_db
  ;




  //TLinkFreqPlan_ = class



implementation




end.




constructor TLinkFreqPlan_LinkEnd_List_.Create;
begin
  inherited Create(TLinkFreqPlan_LinkEnd_);
end;

function TLinkFreqPlan_LinkEnd_List_.AddItem: TLinkFreqPlan_LinkEnd_;
begin
  Result := TLinkFreqPlan_LinkEnd_(inherited Add);
end;
//
//procedure TLinkFreqPlan_LinkEnd_List_.LinkFreqPlan_LoadFromDataset(aDataset:
//    TDataset);
//begin
////  with aDataset do
////  begin
////    Min_CI_db :=FieldByName('Min_CI_db').AsFloat;
////    Threshold_degradation_db:=FieldByName('Threshold_degradation_db').AsFloat;
////  end;
//end;



//-------------------------------------------------------------------
procedure TLinkFreqPlan_LinkEnd_List_.LoadFromDataset(aDataset: TDataset);
//-------------------------------------------------------------------
var
  oLinkEnd: TLinkFreqPlan_LinkEnd_;
begin
//  db_View(aDataset);

  // ---------------------------------------------------------------


  aDataset.First;

  with aDataset do
    if not IsEmpty then
  begin
    oLinkEnd :=AddItem;

    assert(FieldByName('PROPERTY_ID').AsInteger>0);


    oLinkEnd.LinkEnd_ID :=FieldByName('LinkEnd_ID').AsInteger;
    oLinkEnd.PROPERTY_ID:=FieldByName('PROPERTY_ID').AsInteger;

    Next;
  end;
end;



procedure TLinkFreqPlan_.LoadFromDataset(aDataset: TDataset);
const
  FLD_Min_CI_db = 'Min_CI_db';
  FLD_Threshold_degradation = 'Threshold_degradation';

begin
  with aDataset do
    if not IsEmpty then
  begin
    Min_CI :=FieldByName(FLD_Min_CI_db).AsFloat;
    Threshold_degradation_db:=FieldByName(FLD_Threshold_degradation).AsFloat;
//    Threshold_degradation_db:=FieldByName(FLD_Threshold_degradation_db).AsFloat;

  end;

end;


constructor TLinkFreqPlan_Params.Create;
begin
  inherited;
  LinkFreqPlan := TLinkFreqPlan_.Create();
  LinkEnds := TLinkFreqPlan_LinkEnd_List_.Create();
end;

destructor TLinkFreqPlan_Params.Destroy;
begin
  FreeAndNil(LinkEnds);
  FreeAndNil(LinkFreqPlan);
  inherited Destroy;
end;



end.

(*
//--------------------------------------------------------------------
function Tframe_LinkFreqPlan_LinkEnd.FoundIn_LinkEnd_Array(aLinkEndID,
                              aPropertyID: integer; aIsFixed: Boolean): double;
//--------------------------------------------------------------------
var i: Integer;
begin
  for i:=0 to High(FArrLinkEndRec) do
    if ((FArrLinkEndRec[i].linkend_id<>aLinkEndID) and
        (FArrLinkEndRec[i].property_id=aPropertyID))
    then
    begin
      Result:=IIF(aIsFixed, FArrLinkEndRec[i].freq_fixed_rx,
                                    FArrLinkEndRec[i].freq_distr_rx);
      Exit;
    end;
end;

*)



  TLinkFreqPlan_1 = class
  public
    Min_CI : Double;
    Threshold_degradation_db : Double;


(*    LinkEnd_ID: Integer;
    PROPERTY_ID : Integer;

    freq_fixed_rx : double;
    freq_distr_rx : double;
*)

    procedure LoadFromDataset(aDataset: TDataset);

  end;


  TLinkFreqPlan_LinkEnd_1 = class(TcollectionItem)
  public
    LinkEnd_ID: Integer;
    PROPERTY_ID : Integer;

    freq_fixed_rx : double;
    freq_distr_rx : double;

  end;



  TLinkFreqPlan_LinkEnd_List_ = class(Tcollection)
  private
    function AddItem: TLinkFreqPlan_LinkEnd_;
  public
//
//    LinkFreqPlan : record
//                     min_CI : Double;
//                     threshold_degradation : Double;
//                   end;

    constructor Create;

    procedure LoadFromDataset(aDataset: TDataset);
//    procedure LinkFreqPlan_LoadFromDataset(aDataset: TDataset);
  end;


  TLinkFreqPlan_Params = class
  public
    LinkFreqPlan: TLinkFreqPlan_;
    LinkEnds: TLinkFreqPlan_LinkEnd_List_;

    constructor Create;
    destructor Destroy; override;

  end;


