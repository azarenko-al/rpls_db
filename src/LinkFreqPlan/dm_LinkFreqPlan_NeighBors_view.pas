unit dm_LinkFreqPlan_NeighBors_view;

interface

uses
  Classes, Forms,  Db, ADODB, dialogs,  SysUtils,

  dm_Onega_DB_data,

//  dm_Custom,

  dm_Main,
  
  u_db,

  u_const_db,
  u_LinkFreqPlan_const,

  AppEvnts
  ;


type
  TdmLinkFreqPlan_NeighBors_view = class(TDataModule)
    ADOStoredProc1: TADOStoredProc;
    ApplicationEvents1: TApplicationEvents;
    procedure DataModuleCreate(Sender: TObject);
  private
    FID : integer;
    FMaxTreeID: integer;
    FCI,
    FExtraLoss: double;


    procedure mem_ItemsBeforePost(DataSet: TDataset);
    procedure mem_ItemsBeforeEdit(DataSet: TDataset);

  public
    Params: record
              IsUpdated: boolean;
            end;

    procedure InitDB (aDataset: TDataset);

    procedure OpenDB (aDataset: TDataset; aLinkFreqPlanID: integer);

    procedure ExpandNeighBours (aDataset: TDataset;
                    aLinkFreqPlanID, aLinkEndID, aTreeParentID: integer);

  end;


const
  TYPE_NEIGHBORS_BY_LINK = 1;  //����� �� Link
  TYPE_NEIGHBORS_BY_PROP = 2;  //����� �� Property
  TYPE_NEIGHBORS_BY_CI   = 3;  //����� �� ����������



function dmLinkFreqPlan_NeighBors_view: TdmLinkFreqPlan_NeighBors_view;

//===================================================================
implementation {$R *.DFM}
//===================================================================



var
  FdmLinkFreqPlan_NeighBors_view: TdmLinkFreqPlan_NeighBors_view;


// ---------------------------------------------------------------
function dmLinkFreqPlan_NeighBors_view: TdmLinkFreqPlan_NeighBors_view;
// ---------------------------------------------------------------
begin
  if not Assigned(FdmLinkFreqPlan_NeighBors_view) then
    FdmLinkFreqPlan_NeighBors_view := TdmLinkFreqPlan_NeighBors_view.Create(Application);

  Result := FdmLinkFreqPlan_NeighBors_view;
end;                                                   


//-------------------------------------------------------------------
procedure TdmLinkFreqPlan_NeighBors_view.DataModuleCreate(Sender: TObject);
//-------------------------------------------------------------------
begin
  inherited;

  db_SetComponentADOConnection(Self, dmMain.ADOConnection);

  FMaxTreeID:=10000000;
end;

//-------------------------------------------------------------------
procedure TdmLinkFreqPlan_NeighBors_view.InitDB (aDataset: TDataset);
//-------------------------------------------------------------------
begin
  db_CreateField(aDataset,
    [
//      db_Field( FLD_NUMBER,          ftInteger),
      db_Field( FLD_RX_LinkEnd_ID, ftInteger),

      db_Field( FLD_RX_LinkEnd_id, ftInteger, 50),
      db_Field( FLD_TX_LinkEnd_id, ftInteger, 50),


      db_Field( FLD_RX_LinkEnd_name, ftString, 50),
      db_Field( FLD_TX_LinkEnd_name, ftString, 50),


      db_Field( FLD_LINKEND_ID,      ftInteger),
//      db_Field( FLD_DISTANCE_km,     ftFloat),

      db_Field( FLD_DISTANCE_m,      ftFloat),

//      db_Field( FLD_LAT,      ftFloat),
//      db_Field( FLD_LON,      ftFloat),


      db_Field( FLD_BAND, ftString, 10),

      db_Field( FLD_NEIGHBOUR_COUNT, ftInteger),

      db_Field( FLD_CI,              ftFloat),

      db_Field( FLD_Rx_level_dBm,    ftFloat),
      db_Field( FLD_DIAGRAM_LEVEL,    ftFloat),

      db_Field( FLD_Freq_Spacing_MHz,    ftFloat),


      // ������
      db_Field( FLD_INTERFERENCE,    ftFloat),  //FieldValues[FLD_POWER_dBm] - FieldValues[FLD_DIAGRAM_LEVEL]),

      db_Field( FLD_IS_EXPANDED,     ftBoolean),

      db_Field( FLD_CIA,             ftFloat),
      db_Field( FLD_CI_,             ftFloat),
      db_Field( FLD_CA,              ftFloat),

      db_Field( FLD_POWER_noise,              ftFloat),
      db_Field( FLD_POWER_CIA,              ftFloat),

      db_Field( FLD_AZIMUTH,         ftFloat),

      db_Field( FLD_EXTRA_LOSS,                 ftFloat),
      db_Field( FLD_threshold_degradation,      ftFloat),
      db_Field( FLD_threshold_degradation_cia,  ftFloat),

      db_Field( FLD_DXtree_id,        ftInteger),
      db_Field( FLD_DXtree_parent_id, ftInteger),

      db_Field( FLD_TYPE,             ftInteger)

    ]);

  aDataset.BeforePost:=mem_ItemsBeforePost;
  aDataset.BeforeEdit:=mem_ItemsBeforeEdit;
end;

//--------------------------------------------------------------------
procedure TdmLinkFreqPlan_NeighBors_view.mem_ItemsBeforePost(DataSet: TDataset);
//-------------------------------------------------------------------
const
  SQL_UPDATE_EXTRA_LOSS =
         'UPDATE ' + TBL_LinkFreqPlan_NEIGHBORS +
         ' SET extra_loss=:extra_loss, ci=:ci ' +
         ' WHERE (LinkFreqPlan_id=:LinkFreqPlan_id) ' +
         '       and (tx_id=:tx_id) and (rx_link_id=:rx_link_id)';
begin
  if Params.IsUpdated then
    Exit;

  with DataSet do
   if (FieldByName(FLD_EXTRA_LOSS).AsFloat<>FExtraLoss) then
  begin
    gl_DB.ExecCommand(SQL_UPDATE_EXTRA_LOSS,
           [db_Par(FLD_LinkFreqPlan_ID, FID),

            db_Par(FLD_TX_LINKEND_ID,  DataSet[FLD_LINKEND_ID]),
            db_par(FLD_RX_LINKEND_ID,  DataSet[FLD_DXtree_parent_id]),

            db_Par(FLD_EXTRA_LOSS, DataSet[FLD_EXTRA_LOSS]),

            db_Par(FLD_CI, FCI - FieldByName(FLD_EXTRA_LOSS).AsFloat + FExtraLoss)
            ]);

    Params.IsUpdated:= True;
    FieldValues[FLD_CI]:= FCI - FieldByName(FLD_EXTRA_LOSS).AsFloat + FExtraLoss;
    Params.IsUpdated:= False;
  end;
end;

//-----------------------------------------------------------------------------
procedure TdmLinkFreqPlan_NeighBors_view.mem_ItemsBeforeEdit(DataSet: TDataset);
//-----------------------------------------------------------------------------
begin
  with Dataset do
  begin
    FExtraLoss := FieldByName(FLD_EXTRA_LOSS).AsFloat;
    FCI        := FieldByName(FLD_CI).AsFloat;
  end;
end;

//-------------------------------------------------------------------
procedure TdmLinkFreqPlan_NeighBors_view.ExpandNeighBours (aDataset: TDataset;
                     aLinkFreqPlanID, aLinkEndID, aTreeParentID: integer);
//-------------------------------------------------------------------
//const

{  SQL_SELECT_RRL_NEIGHBOURS_LINKEND =
      'SELECT *, '+
      ' (SELECT name FROM '+ TBL_LINKEND +
      '   WHERE id='+TBL_LinkFreqPlan_NEIGHBORS+'.tx_id) as [linkend_name] '+
      ' FROM ' + TBL_LinkFreqPlan_NEIGHBORS +
      ' WHERE (LinkFreqPlan_id=:LinkFreqPlan_id) and ' +
      '       (RX_Link_id=:linkend_id) and (IsNeigh=1)';
//      '       (TX_Link_id=:linkend_id) and (IsNeigh=1)';
}
var
//  dDistance_m: double;
  iNB_type: integer;
begin

// db_OpenQuery(qry_Items, 'exec '+sp_LinkFreqPlan_Select_Linkends+' :LinkFreqPlan_ID',
  //             [db_Par(FLD_LinkFreqPlan_ID, aLinkFreqPlanID)]);


//  ShowMessage (Format('%d - %d', [aLinkFreqPlanID, aLinkEndID] ));



  dmOnega_DB_data.LinkFreqPlan_Select_Linkend_NEIGHBORS(ADOStoredProc1,
     aLinkFreqPlanID, aLinkEndID);

//  db_View(ADOStoredProc1);


  //dmOnega_DB_data.LinkFreqPlan_Select_Linkend_NEIGHBORS(qry_Items,
 //    aLinkFreqPlanID, aLinkEndID);

(*
  db_Ope nQuery(qry_Items,  'exec ' +sp_LinkFreqPlan_Select_Linkend_NEIGHBORS +
                           '  :LinkFreqPlan_ID, :LINKEND_ID',
                 [db_Par(FLD_LinkFreqPlan_ID, aLinkFreqPlanID),
                  db_Par(FLD_LINKEND_ID,       aLinkEndID) ]);
*)


  with ADOStoredProc1 do
    while not Eof do
  begin
{
    if FieldByName(FLD_IsLink).AsBoolean then
      dDistance_m:= aDataset.FieldByName(FLD_DISTANCE_m).AsFloat
    else
      dDistance_m:=0;
}

//const
//  TYPE_NEIGHBORS_BY_CI      = 1;  //����� �� ����������
//  TYPE_NEIGHBORS_BY_LINK = 2;  //����� �� Link
//  TYPE_NEIGHBORS_BY_PROP = 3;  //����� �� Property



    if FieldByName(FLD_IsLink).AsBoolean then
      db_AddRecord_ (aDataset,
                [FLD_DXtree_id,          FMaxTreeID,
                 FLD_DXtree_parent_id,   aTreeParentID,

                 FLD_TX_LINKEND_NAME,    FieldValues[FLD_LinkEnd_name], //dmLinkEnd.GetNameByID(FieldValues[FLD_TX_ID]) ),


              //    db_par(FLD_BAND,         FieldValues[FLD_BAND]),

                 FLD_POWER_noise,          FieldValues[FLD_POWER_noise],
                 FLD_POWER_CIA,            FieldValues[FLD_POWER_CIA],


            //     db_par(FLD_IsLink,            FieldValues[FLD_IsLink]),

         //    FLD_CI,                FieldValues[FLD_CI],

         //    FLD_Rx_level_dBm,      FieldValues[FLD_Rx_level_dBm],
         //    FLD_DIAGRAM_LEVEL,     FieldValues[FLD_DIAGRAM_LEVEL],

        //     FLD_INTERFERENCE,      FieldBYName(FLD_Rx_level_dBm).AsFloat - FieldBYName(FLD_DIAGRAM_LEVEL).AsFloat,

        //     FLD_AZIMUTH,           FieldValues[FLD_AZIMUTH],




  //             FLD_RX_LINKEND_ID,         FieldValues[FLD_LINKEND_ID],
             //    db_par(FLD_LINKEND_ID,         FieldValues[FLD_TX_LINKEND_ID]),

//                 FLD_TX_LINKEND_ID,      FieldValues[FLD_LinkEnd_ID], //dmLinkEnd.GetNameByID(FieldValues[FLD_TX_ID]) ),

                 FLD_DISTANCE_m,  FieldValues[FLD_DISTANCE_m], //  IIF(dDistance_m>0, dDistance_m, 0),

//                 FLD_DISTANCE_m,  IIF(dDistance_m>0, dDistance_m, 0),
              //   dDistance_m, //IIF(FieldByName(FLD_IsLink).AsBoolean, dDistance_m, 0)),
//                 db_par(FLD_DISTANCE_km,          dDistance_m / 1000 ), //IIF(FieldByName(FLD_IsLink).AsBoolean, dDistance_m, 0)),



            //     FLD_Freq_Spacing_MHz,   FieldValues[FLD_Freq_Spacing_MHz],

                 FLD_TYPE,               TYPE_NEIGHBORS_BY_LINK,

                 FLD_CIA,                    FieldValues[FLD_CIA],
                 FLD_CI_,                    FieldValues[FLD_CI_],
                 FLD_CA,                     FieldValues[FLD_CA],
                 FLD_EXTRA_LOSS,             FieldValues[FLD_EXTRA_LOSS],

//                 FLD_POWER_noise,          FieldValues[FLD_POWER_noise],
           //      FLD_threshold_degradation,     FieldValues[FLD_threshold_degradation],

  //               FLD_POWER_CIA,            FieldValues[FLD_POWER_CIA],

                 FLD_THRESHOLD_DEGRADATION_CIA, FieldValues[FLD_THRESHOLD_DEGRADATION_CIA]


                 ])
    else

    begin
      if FieldByName(FLD_IsPropertyOne).AsBoolean then
        iNB_type := TYPE_NEIGHBORS_BY_PROP
      else
        iNB_type := TYPE_NEIGHBORS_BY_CI;


      if FieldByName(FLD_IsPropertyOne).AsBoolean then
        db_AddRecord_ (aDataset,
            [FLD_DXtree_id,            FMaxTreeID,
             FLD_DXtree_parent_id,     aTreeParentID,


             FLD_TX_LINKEND_NAME,      FieldValues[FLD_LinkEnd_name], //dmLinkEnd.GetNameByID(FieldValues[FLD_TX_ID]) ),

             FLD_TYPE,                 iNB_type,



     //        FLD_AZIMUTH,       FieldValues[FLD_AZIMUTH],





//             FLD_LAT,                 FieldValues[FLD_MAP_LAT],
//             FLD_LON,                 FieldValues[FLD_MAP_LON],


//             FLD_RX_LINKEND_ID,         FieldValues[FLD_LINKEND_ID],



          //    db_par(FLD_IsPropertyOne,            FieldValues[FLD_IsPropertyOne]),
       //       db_par(FLD_IsPropertyOne,            FieldValues[FLD_IsPropertyOne]),



       //      db_par(FLD_BAND,                 FieldValues[FLD_BAND]),
         //
//                 db_par(FLD_BAND,         FieldValues[FLD_BAND]),

      //       db_par(FLD_TX_LINKEND_ID,        FieldValues[FLD_TX_LINKEND_ID]),
        //     db_par(FLD_LINKEND_ID,           FieldValues[FLD_TX_LINKEND_ID]),
//             FLD_TX_LINKEND_ID,      FieldValues[FLD_LinkEnd_ID], //dmLinkEnd.GetNameByID(FieldValues[FLD_TX_ID]) ),

             FLD_DISTANCE_m,           FieldValues[FLD_DISTANCE_m]
     //        db_par(FLD_DISTANCE_km,          FieldValues[FLD_DISTANCE_km]),

            ]);



      if not FieldByName(FLD_IsPropertyOne).AsBoolean then
        db_AddRecord_ (aDataset,
            [FLD_DXtree_id,            FMaxTreeID,
             FLD_DXtree_parent_id,     aTreeParentID,

             FLD_TX_LINKEND_NAME,      FieldValues[FLD_LinkEnd_name], //dmLinkEnd.GetNameByID(FieldValues[FLD_TX_ID]) ),


             FLD_TYPE,                 iNB_type,

//             FLD_AZIMUTH,       FieldValues[FLD_AZIMUTH],

             FLD_Freq_Spacing_MHz,   FieldValues[FLD_Freq_Spacing_MHz],


//             FLD_LAT,                 FieldValues[FLD_MAP_LAT],
//             FLD_LON,                 FieldValues[FLD_MAP_LON],


//             FLD_RX_LINKEND_ID,         FieldValues[FLD_LINKEND_ID],



          //    db_par(FLD_IsPropertyOne,            FieldValues[FLD_IsPropertyOne]),
       //       db_par(FLD_IsPropertyOne,            FieldValues[FLD_IsPropertyOne]),



       //      db_par(FLD_BAND,                 FieldValues[FLD_BAND]),
         //
//                 db_par(FLD_BAND,         FieldValues[FLD_BAND]),

      //       db_par(FLD_TX_LINKEND_ID,        FieldValues[FLD_TX_LINKEND_ID]),
        //     db_par(FLD_LINKEND_ID,           FieldValues[FLD_TX_LINKEND_ID]),
//             FLD_TX_LINKEND_ID,      FieldValues[FLD_LinkEnd_ID], //dmLinkEnd.GetNameByID(FieldValues[FLD_TX_ID]) ),

             FLD_DISTANCE_m,           FieldValues[FLD_DISTANCE_m],
     //        db_par(FLD_DISTANCE_km,          FieldValues[FLD_DISTANCE_km]),

             FLD_CI,                   FieldValues[FLD_CI],
             FLD_INTERFERENCE,         FieldValues[FLD_Rx_level_dBm], //FieldValues[FLD_POWER_dBm] - FieldValues[FLD_DIAGRAM_LEVEL]),

//             db_par(FLD_INTERFERENCE,      FieldValues[FLD_POWER_dBm] - FieldValues[FLD_DIAGRAM_LEVEL]),

          //   FLD_Rx_level_dBm,      FieldValues[FLD_Rx_level_dBm],
             FLD_DIAGRAM_LEVEL,     FieldValues[FLD_DIAGRAM_LEVEL],

//             FLD_INTERFERENCE,      FieldBYName(FLD_Rx_level_dBm).AsFloat - FieldBYName(FLD_DIAGRAM_LEVEL).AsFloat,


             FLD_CIA,                    FieldValues[FLD_CIA],
             FLD_CI_,                    FieldValues[FLD_CI_],
             FLD_CA,                     FieldValues[FLD_CA],
             FLD_EXTRA_LOSS,             FieldValues[FLD_EXTRA_LOSS],

             FLD_POWER_noise,          FieldValues[FLD_POWER_noise],
             FLD_threshold_degradation,     FieldValues[FLD_threshold_degradation],


             FLD_POWER_CIA,            FieldValues[FLD_POWER_CIA],

             FLD_THRESHOLD_DEGRADATION_CIA, FieldValues[FLD_THRESHOLD_DEGRADATION_CIA]
            ]);

            // db_par(FLD_TYPE,         TYPE_NEIGHBORS_BY_PROP) ]);

(*      if FieldByName(FLD_IsPropertyOne).AsBoolean then
        db_UpdateRecord (aDataset,  [db_par(FLD_TYPE, TYPE_NEIGHBORS_BY_PROP) ])
      else
        db_UpdateRecord (aDataset,  [db_par(FLD_TYPE, TYPE_NEIGHBORS_BY_CI) ]);
*)

    end;

{
    if FieldByName(FLD_IsPropertyOne).AsBoolean then
      db_AddRecord (aDataset,
                [db_par(FLD_DXtree_id,        FMaxTreeID),
                 db_par(FLD_DXtree_parent_id, aTreeParentID),

                 db_par(FLD_LINKEND_ID,   FieldValues[FLD_TX_ID]),
                 db_par(FLD_TX_LINKEND_NAME, FieldValues[FLD_LinkEnd_name]), //dmLinkEnd.GetNameByID(FieldValues[FLD_TX_ID]) ),
                 db_par(FLD_DISTANCE,     FieldValues[FLD_DISTANCE]),
                 db_par(FLD_CI,           FieldValues[FLD_CI]),
                 db_par(FLD_INTERFERENCE, FieldValues[FLD_POWER] - FieldValues[FLD_DIAGRAM_LEVEL]),
                 db_par(FLD_CIA,          FieldValues[FLD_CIA]),
                 db_par(FLD_CI_,          FieldValues[FLD_CI_]),
                 db_par(FLD_CA,           FieldValues[FLD_CA]),
                 db_par(FLD_EXTRA_LOSS,   FieldValues[FLD_EXTRA_LOSS]),
                 db_par(FLD_threshold_degradation, FieldValues[FLD_threshold_degradation]),
                 db_par(FLD_THRESHOLD_DEGRADATION_CIA, FieldValues[FLD_THRESHOLD_DEGRADATION_CIA]),
                 db_par(FLD_TYPE,         TYPE_NEIGHBORS_BY_PROP) ])
    else
      db_AddRecord (aDataset,
                [db_par(FLD_DXtree_id,        FMaxTreeID),
                 db_par(FLD_DXtree_parent_id, aTreeParentID),

                 db_par(FLD_LINKEND_ID,   FieldValues[FLD_TX_ID]),
                 db_par(FLD_TX_LINKEND_NAME, FieldValues[FLD_LinkEnd_name]), //dmLinkEnd.GetNameByID (FieldValues[FLD_TX_ID]) ),
                 db_par(FLD_DISTANCE,     FieldValues[FLD_DISTANCE]),
                 db_par(FLD_CI,           FieldValues[FLD_CI]),
                 db_par(FLD_INTERFERENCE, FieldValues[FLD_POWER] - FieldValues[FLD_DIAGRAM_LEVEL]),
                 db_par(FLD_CIA,          FieldValues[FLD_CIA]),
                 db_par(FLD_CI_,          FieldValues[FLD_CI_]),
                 db_par(FLD_CA,           FieldValues[FLD_CA]),
                 db_par(FLD_EXTRA_LOSS,   FieldValues[FLD_EXTRA_LOSS]),
                 db_par(FLD_threshold_degradation, FieldValues[FLD_threshold_degradation]),
                 db_par(FLD_THRESHOLD_DEGRADATION_CIA, FieldValues[FLD_THRESHOLD_DEGRADATION_CIA]),
                 db_par(FLD_TYPE,         TYPE_NEIGHBORS_BY_CI) ]);
}

    FMaxTreeID:= FMaxTreeID+1;
    Next;
  end;


 // db_View (aDataset);

end;

//-------------------------------------------------------------------
procedure TdmLinkFreqPlan_NeighBors_view.OpenDB (aDataset: TDataset;
                                            aLinkFreqPlanID: integer);
//-------------------------------------------------------------------
{

const
  SQL_SELECT_LinkFreqPlan_LINKEND =
    'SELECT '+
    ' (SELECT name FROM LinkEnd WHERE id=LINKFREQPLAN_LINKEND.linkend_id) as [linkend_name], '+
    ' (SELECT [power] FROM LINKFREQPLAN_NEIGHBORS '+
    '   WHERE (LinkFreqPlan_id=:LinkFreqPlan_id) AND (IsLink = 1) AND '+
    '         (rx_link_id = LINKFREQPLAN_LINKEND.linkend_id)) AS [power], '+
    ' (SELECT length FROM Link '+
    '   WHERE  '+
     '         ((linkend1_id=LINKFREQPLAN_LINKEND.linkend_id) OR '+
    '          (linkend2_id=LINKFREQPLAN_LINKEND.linkend_id))) AS [length], '+
    ' (SELECT Count(*) FROM LINKFREQPLAN_NEIGHBORS '+
    '   WHERE (LinkFreqPlan_id=:LinkFreqPlan_id) and '+
    '         (RX_Link_id=LINKFREQPLAN_LINKEND.linkend_id) and (IsNeigh=1)) as [count], '+
    ' id, cia, ci, ca, linkend_id, threshold_degradation, threshold_degradation_cia '+
    'FROM '+ TBL_LinkFreqPlan_LINKEND +
    ' WHERE (Enabled = 1) AND (LinkFreqPlan_id=:LinkFreqPlan_id)';

}

//var dPower: double;
begin
  FID:= aLinkFreqPlanID;

  aDataset.DisableControls;
  aDataset.Close;
  aDataset.Open;


  //fr_LinkFreqPlan_NeightBors
  dmOnega_DB_data.LinkFreqPlan_Select_NB_Linkends(ADOStoredProc1, aLinkFreqPlanID);



// db_View(ADOStoredProc1);


// showMessage ('procedure TdmLinkFreqPlan_NeighBors_view.OpenDB (aDataset: TDataset; ;');



//  db_OpenQuery(qry_Items, 'exec '+ sp_LinkFreqPlan_Select_Linkends+' :LinkFreqPlan_ID',
  //             [db_Par(FLD_LinkFreqPlan_ID, aLinkFreqPlanID)]);


//  db_ViewDataSet(qry_Items);




  //////////////////////////////////////////////
{
  db_OpenQuery(qry_Items,  'exec ' +sp_LinkFreqPlan_Select_Linkend_NEIGHBORS +
                           '  :LinkFreqPlan_ID, :LINKEND_ID',
                 [db_Par(FLD_LinkFreqPlan_ID, aLinkFreqPlanID),
                  db_Par(FLD_LINKEND_ID,       34) ]);
}

 //---------------------------------------------------------

{  try
    db_OpenQuery(qry_Items, SQL_SELECT_LinkFreqPlan_LINKEND,
                 [db_Par(FLD_LinkFreqPlan_ID, aLinkFreqPlanID)]);

  finally

  end;
}

  with AdoStoredProc1 do
    while not Eof do
  begin
    db_AddRecord (aDataset,
        [db_par(FLD_DXtree_id,                 FieldValues[FLD_LINKEND_ID]),
         db_par(FLD_DXtree_parent_id,          0),

     //    db_par(FLD_NUMBER,                    RecNo),

         db_par(FLD_BAND,                      FieldValues[FLD_BAND]),

//         db_par(FLD_LAT,                      FieldValues[FLD_LAT]),
//         db_par(FLD_LON,                      FieldValues[FLD_LON]),


         db_par(FLD_TX_LINKEND_ID,             FieldValues[FLD_LINKEND_ID]),
         db_par(FLD_TX_LinkEnd_name,           FieldValues[FLD_LinkEnd_name]),

         db_par(FLD_RX_LinkEnd_name,           FieldValues[FLD_RX_LinkEnd_name]),


//         db_par(FLD_TX_LINKEND_ID,             FieldValues[FLD_LINKEND_ID]),

         db_par(FLD_LINKEND_ID,                FieldValues[FLD_LINKEND_ID]),


         db_par(FLD_Rx_level_dBm,              FieldValues[FLD_Rx_level_dBm]),
         db_par(FLD_DISTANCE_m,                FieldValues[FLD_LENGTH]),
//         db_par(FLD_DISTANCE_km,               FieldValues[FLD_LENGTH_KM]),

         db_par(FLD_CIA,                       FieldValues[FLD_CIA]),
         db_par(FLD_CI_,                       FieldValues[FLD_CI]),
         db_par(FLD_CA,                        FieldValues[FLD_CA]),

   //      db_par(FLD_THRESHOLD_DEGRADATION,     FieldValues[FLD_THRESHOLD_DEGRADATION]),

         db_par(FLD_THRESHOLD_DEGRADATION_CIA, FieldValues[FLD_THRESHOLD_DEGRADATION_CIA]),

         db_par(FLD_NEIGHBOUR_COUNT,           FieldValues[FLD_NEIGHBOUR_COUNT]),

         db_par(FLD_AZIMUTH,       FieldValues[FLD_AZIMUTH]),


         db_par(FLD_IS_EXPANDED,               False)
         ]);

    Next;
  end;

  aDataset.EnableControls;
end;


begin

end.