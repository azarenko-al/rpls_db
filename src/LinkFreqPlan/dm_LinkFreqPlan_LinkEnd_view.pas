unit dm_LinkFreqPlan_LinkEnd_view;

interface

uses
  Classes, Forms, Dialogs,  Db, ADODB, Variants,  SysUtils,

  u_LinkFreqPlan_classes,


  dm_LinkEnd_ex,

  dm_Onega_DB_data,

 // dm_Custom,

  dm_Main,

  u_Func_arrays,
  u_func,
  u_db,
  u_const_db,

  u_types,

  u_LinkFreqPlan_const
  ;

type
{
  TLinkFreqPlan_LinkEnd_Rec1 = record
    linkend_id: Integer;
    property_id: integer;

    freq_fixed_rx: Double;
    freq_distr_rx : double;
  end;
 }

//  TLinkFreqPlan_LinkEnd_RecArray = array of TLinkFreqPlan_LinkEnd_Rec1;


  TdmLinkFreqPlan_LinkEnd_View = class(TDataModule)
    ADOQuery1: TADOQuery;
    sp_LinkFreqPlan_LinkEnd_: TADOStoredProc;
    qry_LinkFreqPlan_LinkEnds: TADOQuery;
    procedure DataModuleDestroy(Sender: TObject);
    procedure ADOQuery1AfterPost(DataSet: TDataSet);
    procedure DataModuleCreate(Sender: TObject);

  private
{
    FStrDistrFreqs: string;
    FStrFixedFreqs: string;
    FStrForbidFreqs: string;
    FStrAllowFreqs: string;
    FFreqCount : integer;
 }


//    FIsUpdateAnotherLinkEnd: boolean;
    FIsUpdated: boolean;

    procedure mem_LinkEndBeforePost(DataSet: TDataset);
//    procedure mem_LinkEndBeforeEdit(DataSet: TDataset);

//    procedure UpdateAnotherLinkEnd(aDataSet: TDataset; aLinkEndID: integer;
  //                                           aDistrFreqs, aFixedFreqs: string);

    procedure Calc_Carriers(aDataset: TDataset);
    procedure DoOnCalcFields(aDataset: TDataset);

//    procedure DoOnChange(Sender: TField);
//    procedure DoOnValidate(Sender: TField);
    procedure UpdateFreqTxRx(aDataset: TDataset);

 //   procedure InitDB (aDataset: TDataset);
//    procedure OpenDB (aLinkFreqPlanID: integer;  aDataset: TDataset);


  public
    Data: TnbData;

//    LinkFreqPlan_Params: TLinkFreqPlan_Params;

    procedure OpenDB_new(aLinkFreqPlanID: integer);


    class procedure Init;

  end;


//function
var
  dmLinkFreqPlan_LinkEnd_View: TdmLinkFreqPlan_LinkEnd_View;

//=====================================================
implementation {$R *.DFM}
//=====================================================



// ---------------------------------------------------------------
class procedure TdmLinkFreqPlan_LinkEnd_View.Init;
// ---------------------------------------------------------------
begin
  if not Assigned(dmLinkFreqPlan_LinkEnd_View) then
    dmLinkFreqPlan_LinkEnd_View := TdmLinkFreqPlan_LinkEnd_View.Create(Application);

//  Result := FdmLinkFreqPlan_LinkEnd_View;
end;




procedure TdmLinkFreqPlan_LinkEnd_View.DataModuleDestroy(Sender: TObject);
begin

  FreeAndNil(Data);

//  FreeAndNil(LinkFreqPlan_Params);
end;

//-------------------------------------------------------------------
procedure TdmLinkFreqPlan_LinkEnd_View.DataModuleCreate(Sender: TObject);
//-------------------------------------------------------------------
var
  k: Integer;
begin
  inherited;

//  FIsUpdateAnotherLinkEnd:= True;

  db_SetComponentADOConnection(Self, dmMain.ADOConnection);

  Data:=TnbData.Create;


 // k:=sp_LinkFreqPlan_LinkEnd_.Fields.Count;


//  LinkFreqPlan_Params:=TLinkFreqPlan_Params.Create;


end;



//-------------------------------------------------------------------
procedure TdmLinkFreqPlan_LinkEnd_View.OpenDB_new(aLinkFreqPlanID: integer);
// ---------------------------------------------------------------
var
  k: Integer;
  oDataset: TDataset;

begin
 // k:=sp_LinkFreqPlan_LinkEnd_.Fields.Count;


  oDataset:=sp_LinkFreqPlan_LinkEnd_;
  oDataset.DisableControls;

  oDataset.OnCalcFields:=nil;




  if sp_LinkFreqPlan_LinkEnd_.Fields.Count=0 then
  begin                                //-------------------------------------------------------------------

    // SELECT * FROM VIEW_LINKFREQPLAN_LINKEND  	WHERE LINKFREQPLAN_id=@id

//    dmOnega_DB_data.OpenQuery(ADOQuery1, Format('SELECT * FROM VIEW_LINKFREQPLAN_LINKEND 	WHERE LINKFREQPLAN_id=%d', [aLinkFreqPlanID]));

    k:=dmOnega_DB_data.LinkFreqPlan_SELECT (sp_LinkFreqPlan_LinkEnd_, 0);


   // dmOnega_DB_data.

  //  Assert(Assigned( oDataset.Fields.FindField(FLD_FREQ_DISTRIBUTED)));


  //  db_View(oDataset);

    db_CreateFieldsForDataset(oDataset);


    db_CreateCalculatedField(oDataset, FLD_Freq_Distributed_Count, ftInteger);
    db_CreateCalculatedField(oDataset, FLD_FREQ_FIXED_COUNT, ftInteger);

    db_CreateCalculatedField(oDataset, FLD_Distributed_min_raznos, ftInteger);
    db_CreateCalculatedField(oDataset, FLD_Fixed_min_raznos, ftInteger);


    db_CreateCalculatedField(oDataset, FLD_FREQ_DISTRIBUTED_TX_STR, ftString);
    db_CreateCalculatedField(oDataset, FLD_FREQ_DISTRIBUTED_RX_STR, ftString);

    db_CreateCalculatedField(oDataset, FLD_FREQ_FIXED_TX_STR, ftString);
    db_CreateCalculatedField(oDataset, FLD_FREQ_FIXED_RX_STR, ftString);

  end;
  

  dmOnega_DB_data.LinkFreqPlan_SELECT (sp_LinkFreqPlan_LinkEnd_, aLinkFreqPlanID);

 // k:=sp_LinkFreqPlan_LinkEnd_.Fields.Count;

 //  Assert(Assigned( oDataset.Fields.FindField(FLD_FREQ_DISTRIBUTED)));



  {
  arrDistr := StringToIntArray(FieldByName(FLD_FREQ_DISTRIBUTED).AsString);
    arrFixed := StringToIntArray(FieldByName(FLD_FREQ_FIXED).AsString);

    FieldByName(FLD_Freq_Distributed_Count).Value := Length(arrDistr);
    FieldByName(FLD_FREQ_FIXED_COUNT).Value       := Length(arrFixed);

    FieldByName(FLD_Distributed_min_raznos).Value := IIF(IntArrayMinRaznos(arrDistr)>=0,
                               IntArrayMinRaznos(arrDistr), null);
    FieldByName(FLD_Fixed_min_raznos).Value       := IIF(IntArrayMinRaznos(arrFixed)>=0,
                               IntArrayMinRaznos(arrFixed), null);


  }


//db_View(sp_LinkFreqPlan_LinkEnd);

 {
  dmOnega_DB_data.OpenQuery (qry_LinkFreqPlan,
       'SELECT * FROM ' + TBL_LINKFREQPLAN + ' WHERE id=:id' ,
            [db_Par(FLD_ID, aLinkFreqPlanID) ]
     );
  }

 // Data.LinkFreqPlan.LoadFromDataset(qry_LinkFreqPlan);



  Data.Linkends.LoadFromDataset(sp_LinkFreqPlan_LinkEnd_);
          
  oDataset.First;

//  sp_LinkFreqPlan_LinkEnd_.First;


////////////   Data.Linkends.Show;

  // ---------------------------------------------------------------


  oDataset.BeforePost:=mem_LinkEndBeforePost;
  oDataset.OnCalcFields:=DoOnCalcFields;

  oDataset.EnableControls;



 // db_View(oDataset);

end;



//-------------------------------------------------------------------
procedure TdmLinkFreqPlan_LinkEnd_View.DoOnCalcFields(aDataset: TDataset);
//-------------------------------------------------------------------
var
  k: Integer;
begin
  k:=aDataset.RecordCount;

  Calc_Carriers (aDataset);
  UpdateFreqTxRx(aDataSet);
end;


//-------------------------------------------------------------------
procedure TdmLinkFreqPlan_LinkEnd_View.Calc_Carriers(aDataset: TDataset);
//-------------------------------------------------------------------
var
  arrDistr, arrFixed: TIntArray;
  k: Integer;
begin
  Assert(Assigned( aDataset.Fields.FindField(FLD_FREQ_DISTRIBUTED)));

  k:=aDataset.RecordCount;


  k:=Data.Linkends.Count;



  with aDataset do
  begin
    arrDistr := StringToIntArray(FieldByName(FLD_FREQ_DISTRIBUTED).AsString);
    arrFixed := StringToIntArray(FieldByName(FLD_FREQ_FIXED).AsString);

    FieldByName(FLD_Freq_Distributed_Count).Value := Length(arrDistr);
    FieldByName(FLD_FREQ_FIXED_COUNT).Value       := Length(arrFixed);

    FieldByName(FLD_Distributed_min_raznos).Value := IIF(IntArrayMinRaznos(arrDistr)>=0,
                               IntArrayMinRaznos(arrDistr), null);
    FieldByName(FLD_Fixed_min_raznos).Value       := IIF(IntArrayMinRaznos(arrFixed)>=0,
                               IntArrayMinRaznos(arrFixed), null);

  end;


//  UpdateFreqTxRx(aDataSet);
  
end;


//-------------------------------------------------------------------
procedure TdmLinkFreqPlan_LinkEnd_View.UpdateFreqTxRx(aDataset: TDataset);
//-------------------------------------------------------------------
var
  arrDistr, arrFixed: TIntArray;

  arrDistr_TX_str,
  arrDistr_RX_str,
  arrFixed_TX_str,
  arrFixed_RX_str  : TStrArray;

   // iChTypeDistr, iChTypeFixed,
 // iChCount1: integer;
  iChCount: integer;
 // iLinkEndID: integer;

  sCHANNEL_TYPE : string;

//  dChannelWidth1, dFreqMin_MHZ1, dTxRx_Shift1: double;

  dChannelWidth, dFreqMinLow_MHZ, dTxRx_Shift: double;
  dDistrTxFreq, dDistrRxFreq, dFixedTxFreq, dFixedRxFreq: double;
  i: Integer;



    // ---------------------------------------------------------------
    procedure DoCalcFreqs(aChannelNumber: integer; aLowOrHigh: string; var aTxFreq, aRxFreq:
        double);
    // ---------------------------------------------------------------
//    procedure DoCalcFreqs(aChannelNumber, aChType: integer; var aTxFreq, aRxFreq: double);
    begin
      Assert(aLowOrHigh<>'');

      if Eq(aLowOrHigh,'low') then
      begin  //low
        aTxFreq:= dFreqMinLow_MHZ + dChannelWidth*(aChannelNumber - 1);
        aRxFreq:= aTxFreq + dTxRx_Shift;
      end else
      begin  //high
        aRxFreq:= dFreqMinLow_MHZ + dChannelWidth*(aChannelNumber - 1);
        aTxFreq:= aRxFreq + dTxRx_Shift;   //!!!
      end;


  (*    case aChType of
          0: begin  //low
               aTxFreq:= dFreqMinLow_MHZ + dChannelWidth*(aChannelNumber - 1);
               aRxFreq:= aTxFreq + dTxRx_Shift;
             end;
          1: begin  //high
               aRxFreq:= dFreqMinLow_MHZ + dChannelWidth*(aChannelNumber - 1);
               aTxFreq:= aRxFreq + dTxRx_Shift;
             end;
      end;
*)
    end;    //

var
  iCount: Integer;
  iID: Integer;
  k: Integer;

  oLinkend: TnbLinkFreqPlan_Linkend;
 // oLinkend1: TnbLinkFreqPlan_Linkend;


  rInfo: TnbLinkFreqPlan_Linkend_Get_Info;

begin

  k:=aDataset.RecordCount;


//  TnbLinkFreqPlan_Linkend.Get: TnbLinkFreqPlan_Linkend_Get_Info;

  k:=Data.Linkends.Count;


  iID:=aDataset.FieldByName(FLD_ID).AsInteger;

 // iLinkendID:=aDataset.FieldByName(FLD_Linkend_ID).AsInteger;

 // oLinkend1:=Data.Linkends.FindByLinkendID(iLinkendID);

  oLinkend:=Data.Linkends.FindByID1(iID);

  if not Assigned(oLinkend) then
   begin
     ShowMessage(  Format('Linkend id: %d', [iID]) );


     oLinkend:=Data.Linkends.FindByID1(iID);

     exit;
   end;


 // Assert(Assigned(oLinkend),  Format('id: %d', [iID]) );




  rInfo:=oLinkend.Get();
                          

  with aDataset do
  begin


    FieldByName(FLD_FREQ_DISTRIBUTED_TX_STR).Value := StrArrayToString(rInfo.Distr_TX_arr);
    FieldByName(FLD_FREQ_DISTRIBUTED_RX_STR).Value := StrArrayToString(rInfo.Distr_RX_arr);


    FieldByName(FLD_FREQ_FIXED_TX_STR).Value := StrArrayToString(rInfo.Fixed_TX_arr);
    FieldByName(FLD_FREQ_FIXED_RX_STR).Value := StrArrayToString(rInfo.Fixed_RX_arr);


    exit;

  end;


  with aDataset do
  begin
    sCHANNEL_TYPE:=FieldByName(FLD_CHANNEL_TYPE).AsString;

    if sCHANNEL_TYPE='' then
      Exit;


    arrDistr := StringToIntArray(FieldByName(FLD_FREQ_DISTRIBUTED).AsString);
    arrFixed := StringToIntArray(FieldByName(FLD_FREQ_FIXED).AsString);


//    sChTypeDistr:= FieldByName(FLD_CHANNEL_TYPE_DISTRIBUTED).AsString;
//    sChTypeFixed:= FieldByName(FLD_CHANNEL_TYPE_FIXED).AsString;





(*
  iChTypeDistr:= IIF(FieldByName(FLD_CH_TYPE_DISTRIBUTED).AsString='Low', 0, 1);
    iChTypeFixed:= IIF(FieldByName(FLD_CH_TYPE_FIXED).AsString='Low', 0, 1);
*)

//    iLinkEndID:= FieldByName(FLD_LINKEND_ID).AsInteger;



    dChannelWidth  := FieldByName(FLD_CHANNEL_WIDTH).AsFloat;
    dTxRx_Shift    := FieldByName(FLD_TXRX_SHIFT).AsFloat;
    dFreqMinLow_MHZ:= FieldByName(FLD_FREQ_MIN_LOW).AsFloat;
    iChCount       := FieldByName(FLD_CHANNEL_COUNT).AsInteger;


    {
    dmLinkEnd_ex.GetBand_Params(iLinkEndID,
      dTxRx_Shift, dChannelWidth, dFreqMinLow_MHZ, iChCount, OBJ_LINKEND);

     Assert(dTxRx_Shift=dTxRx_Shift1);
     Assert(dChannelWidth=dChannelWidth1);
     Assert(dFreqMinLow_MHZ=dFreqMin_MHZ1);
     Assert(iChCount=iChCount1);

    }

    if (Length(arrDistr)=0) and (Length(arrFixed)=0) then
      exit;

    iCount:=Length(arrDistr);

    if iCount>0 then
    begin
      SetLength(arrDistr_TX_str, iCount);
      SetLength(arrDistr_RX_str, iCount);



      for i:=0 to iCount-1 do
      begin
        DoCalcFreqs(arrDistr[i], sCHANNEL_TYPE, dDistrTxFreq, dDistrRxFreq);

        arrDistr_TX_str[i]:= AsString(dDistrTxFreq);
        arrDistr_RX_str[i]:= AsString(dDistrRxFreq);
      end;

      FieldByName(FLD_FREQ_DISTRIBUTED_TX_STR).Value := StrArrayToString(arrDistr_TX_str);
      FieldByName(FLD_FREQ_DISTRIBUTED_RX_STR).Value := StrArrayToString(arrDistr_RX_str);

    end;


    iCount:=Length(arrFixed);

    if iCount>0 then
    begin
      SetLength(arrFixed_TX_str, iCount);
      SetLength(arrFixed_RX_str, iCount);


      for i:=0 to iCount-1 do
      begin
        DoCalcFreqs(arrFixed[i], sCHANNEL_TYPE, dFixedTxFreq, dFixedRxFreq);

        arrFixed_TX_str[i]:= AsString(dFixedTxFreq);
        arrFixed_RX_str[i]:= AsString(dFixedRxFreq);

      end;



      FieldByName(FLD_FREQ_FIXED_TX_STR).Value := StrArrayToString(arrFixed_TX_str);
      FieldByName(FLD_FREQ_FIXED_RX_STR).Value := StrArrayToString(arrFixed_RX_str);

    end;

  end;








end;

//--------------------------------------------------------------------
procedure TdmLinkFreqPlan_LinkEnd_View.mem_LinkEndBeforePost(DataSet: TDataset);
//-------------------------------------------------------------------

//
//   //--------------------------------------
//   procedure DoUpdateCarriers (aStrCarriers, aFieldNameCarriers: string);
//   //--------------------------------------
//   var id:integer;
//   begin
//     with Dataset do
//       if aStrCarriers<>FieldByName(aFieldNameCarriers).AsString then
//       begin
//         id:=FieldValues[FLD_ID];
//         gl_DB.UpdateRecord(TBL_LINKFREQPLAN_LINKEND, id,
//                     [db_par(aFieldNameCarriers,
//                      FieldByName(aFieldNameCarriers).AsString)
//                      ]);
//       end;
//   end;


var
  iID: Integer;

  oLinkend: TnbLinkFreqPlan_Linkend;
  sDistr_TX: string;
  sFixed_TX: string;

  arrFixed_TX:  TStrArray;
  arrFixed_RX:  TStrArray;

  arrDistr_TX:  TStrArray;
  arrDistr_RX:  TStrArray;

  eDistr_RX: Double;
  eDistr_TX: Double;
  eFixed_RX: Double;
  eFixed_TX: Double;



begin
  if FIsUpdated then
    Exit;

{
    FStrDistrFreqs := FieldByName(FLD_FREQ_DISTRIBUTED).AsString;
    FStrFixedFreqs := FieldByName(FLD_FREQ_FIXED).AsString;
    FStrForbidFreqs:= FieldByName(FLD_FREQ_FORBIDDEN).AsString;
    FStrAllowFreqs := FieldByName(FLD_FREQ_ALLOW).AsString;

    FFreqCount := FieldByName(FLD_freq_req_count).AsInteger;



  DoUpdateCarriers (FStrDistrFreqs,  FLD_FREQ_DISTRIBUTED);
  DoUpdateCarriers (FStrFixedFreqs,  FLD_FREQ_FIXED);
  DoUpdateCarriers (FStrForbidFreqs, FLD_FREQ_FORBIDDEN);
  DoUpdateCarriers (FStrAllowFreqs,  FLD_FREQ_ALLOW);

 }

  iID:=Dataset[FLD_ID];

  oLinkend:=Data.Linkends.FindByID1(iID);
  Assert(Assigned(oLinkend));


{
    db_CreateCalculatedField(oDataset, FLD_FREQ_DISTRIBUTED_TX_STR, ftString);
    db_CreateCalculatedField(oDataset, FLD_FREQ_DISTRIBUTED_RX_STR, ftString);

    db_CreateCalculatedField(oDataset, FLD_FREQ_FIXED_TX_STR, ftString);
    db_CreateCalculatedField(oDataset, FLD_FREQ_FIXED_RX_STR, ftString);
}


  // arrDistr := StringToIntArray(FieldByName(FLD_FREQ_DISTRIBUTED).AsString);

  oLinkend.FreqReqCount:= DataSet.FieldByName(FLD_freq_req_count).AsInteger;

  oLinkend.freq_distributed := DataSet.FieldByName(FLD_FREQ_DISTRIBUTED).AsString;
  oLinkend.FREQ_ALLOW       := DataSet.FieldByName(FLD_FREQ_ALLOW).AsString;
  oLinkend.FREQ_FIXED       := DataSet.FieldByName(FLD_FREQ_FIXED).AsString;
  oLinkend.FREQ_FORBIDDEN   := DataSet.FieldByName(FLD_FREQ_FORBIDDEN).AsString;


  arrDistr_TX:=StringToStrArray(DataSet.FieldByName(FLD_FREQ_DISTRIBUTED_TX_STR).AsString);
  arrDistr_RX:=StringToStrArray(DataSet.FieldByName(FLD_FREQ_DISTRIBUTED_RX_STR).AsString);

  arrFixed_TX:=StringToStrArray(DataSet.FieldByName(FLD_FREQ_FIXED_TX_STR).AsString);
  arrFixed_RX:=StringToStrArray(DataSet.FieldByName(FLD_FREQ_FIXED_RX_STR).AsString);


//  oLinkend.

   if Length(arrDistr_TX)>0 then eDistr_TX:=AsFloat(arrDistr_TX[0]) else eDistr_TX:=0;
   if Length(arrDistr_RX)>0 then eDistr_RX:=AsFloat(arrDistr_RX[0]) else eDistr_RX:=0;

   if Length(arrFixed_TX)>0 then eFixed_TX:=AsFloat(arrFixed_TX[0]) else eFixed_TX:=0;
   if Length(arrFixed_RX)>0 then eFixed_TX:=AsFloat(arrFixed_RX[0]) else eFixed_RX:=0;




//
//  sFieldNameTx:= IIF(Params.ChannelsKind=opUseDistrChannels, 'freq_distributed_tx', 'freq_fixed_tx');
//  sFieldNameRx:= IIF(Params.ChannelsKind=opUseDistrChannels, 'freq_distributed_rx', 'freq_fixed_rx');






  gl_DB.UpdateRecord(TBL_LINKFREQPLAN_LINKEND, Dataset[FLD_ID],
        [
         db_Par(FLD_freq_req_count,   DataSet[FLD_freq_req_count]),

         db_par(FLD_FREQ_DISTRIBUTED, Dataset[FLD_FREQ_DISTRIBUTED]),
         db_par(FLD_FREQ_FIXED,       Dataset[FLD_FREQ_FIXED]),
         db_par(FLD_FREQ_FORBIDDEN,   Dataset[FLD_FREQ_FORBIDDEN]),
         db_par(FLD_FREQ_ALLOW,       Dataset[FLD_FREQ_ALLOW]),

//....,,         db_par(FLD_FREQ_ALLOW,       Dataset[FLD_FREQ_ALLOW])
//..,         db_par(FLD_FREQ_ALLOW,       Dataset[FLD_FREQ_ALLOW])


         db_par(FLD_freq_distributed_tx,  eDistr_TX),
         db_par(FLD_freq_distributed_rx,  eDistr_RX),

         db_par(FLD_freq_fixed_tx,        eFixed_TX),
         db_par(FLD_freq_fixed_rx,        eFixed_RX)





//         db_par(FLD_CHANNEL_TYPE_DISTRIBUTED,   Dataset[FLD_CHANNEL_TYPE_DISTRIBUTED]),
//         db_par(FLD_CHANNEL_TYPE_FIXED,         Dataset[FLD_CHANNEL_TYPE_FIXED]),


        ]);

 {

  if FFreqCount<>Dataset.FieldByName(FLD_freq_req_count).AsInteger then
    gl_DB.UpdateRecord(TBL_LINKFREQPLAN_LINKEND, Dataset[FLD_ID],
                       [db_par(FLD_freq_req_count,
                         Dataset.FieldByName(FLD_freq_req_count).AsInteger)]);

  gl_DB.UpdateRecord(TBL_LINKFREQPLAN_LINKEND, Dataset[FLD_ID],
                       [db_par(FLD_Priority,
                            Dataset.FieldByName(FLD_Priority).AsInteger)]);
}

//  Calc_Carriers(DataSet);
//  UpdateFreqTxRx(DataSet);

{ TODO : Locate �������� ����������� (��������� ������ ������ � dbGrid) !!! }

(*  if FIsUpdateAnotherLinkEnd then
    UpdateAnotherLinkEnd(DataSet, DataSet[FLD_LINKEND_ID],
                       DataSet.FieldByName(FLD_FREQ_DISTRIBUTED).AsString,
                       DataSet.FieldByName(FLD_FREQ_FIXED).AsString);
*)
//  Clear_Interference(aLink_FreqPlan_ID);
end;



// ---------------------------------------------------------------
procedure TdmLinkFreqPlan_LinkEnd_View.ADOQuery1AfterPost(DataSet: TDataSet);
// ---------------------------------------------------------------
begin
  Exit;

  dmOnega_DB_data.ExecStoredProc(sp_LinkFreqPlan_Update_Item,
         [
           db_Par(FLD_ID,               DataSet[FLD_ID]),
        //   db_Par(FLD_PRIORITY,         DataSet[FLD_PRIORITY]),

           db_Par(FLD_FREQ_DISTRIBUTED, DataSet[FLD_FREQ_DISTRIBUTED]),
           db_Par(FLD_FREQ_FIXED,       DataSet[FLD_FREQ_FIXED]),
           db_Par(FLD_FREQ_FORBIDDEN,   DataSet[FLD_FREQ_FORBIDDEN]),
           db_Par(FLD_FREQ_ALLOW,       DataSet[FLD_FREQ_ALLOW]),

//           db_Par(FLD_FREQ_FIXED_TX,  DataSet[FLD_FREQ_DISTRIBUTED_TX_STR]),
//           db_Par(FLD_FREQ_FIXED_RX,  DataSet[FLD_FREQ_DISTRIBUTED_RX_STR]),

//    FieldByName(FLD_FREQ_DISTRIBUTED_TX_STR).Value := StrArrayToString(arrDistr_TX_str);
//    FieldByName(FLD_FREQ_DISTRIBUTED_RX_STR).Value := StrArrayToString(arrDistr_RX_str);

           db_Par(FLD_freq_req_count,   DataSet[FLD_freq_req_count])
         ]);

(*           db_par(FLD_FREQ_DISTRIBUTED_TX, dDistrTxFreq),
           db_par(FLD_FREQ_DISTRIBUTED_RX, dDistrRxFreq),
           db_par(FLD_FREQ_FIXED_TX,       dFixedTxFreq),
           db_par(FLD_FREQ_FIXED_RX,       dFixedRxFreq)
*)

(*
    FStrDistrFreqs := FieldByName(FLD_FREQ_DISTRIBUTED).AsString;
    FStrFixedFreqs := FieldByName(FLD_FREQ_FIXED).AsString;
    FStrForbidFreqs:= FieldByName(FLD_FREQ_FORBIDDEN).AsString;
    FStrAllowFreqs := FieldByName(FLD_FREQ_ALLOW).AsString;

    FFreqCount := FieldByName(FLD_freq_req_count).AsInteger;
*)

//  function TOnega_DB_data.(aProcName: string; aParams: array of TdbParamRec):


end;



begin

end.

