unit dm_act_LinkFreqPlan_Link;

interface

uses
  Classes,  Menus,  Forms, 

  I_Object,
  
  u_GEO,
  
  u_types,

  u_func,

  fr_Link_view,

  dm_act_Base,
  
  dm_LinkFreqPlan_link,
  dm_Link, ActnList
  ;

type
  TdmAct_LinkFreqPlan_Link = class(TdmAct_Base)
    procedure DataModuleCreate(Sender: TObject);

  private
//    procedure Dlg_UpdateLinks1(aLinkFreqPlan_ID: integer);
  protected
    procedure GetPopupMenu (aPopupMenu: TPopupMenu; aMenuType: TMenuType); override;
    function  GetViewForm  (aOwnerForm: TForm): TForm; override;

    procedure DoAction (Sender: TObject);

    function ItemDel(aID: integer; aName: string = ''): boolean; override;

    procedure ShowOnMap (aID: integer);
  public
    class procedure Init;
  end;


var
  dmAct_LinkFreqPlan_Link: TdmAct_LinkFreqPlan_Link;

//==================================================================
implementation {$R *.dfm}
//==================================================================
                 

//--------------------------------------------------------------------
class procedure TdmAct_LinkFreqPlan_Link.Init;
//--------------------------------------------------------------------
begin
  if not Assigned(dmAct_LinkFreqPlan_link) then
    dmAct_LinkFreqPlan_link:=TdmAct_LinkFreqPlan_link.Create(Application);
end;

//--------------------------------------------------------------------
procedure TdmAct_LinkFreqPlan_Link.DataModuleCreate(Sender: TObject);
//--------------------------------------------------------------------
begin
  inherited;

  ObjectName:=OBJ_LINKFREQPLAN_LINK;

  SetActionsExecuteProc ([act_Object_ShowOnMap], DoAction);
end;

//--------------------------------------------------------------------
procedure TdmAct_LinkFreqPlan_Link.ShowOnMap (aID: integer);
//--------------------------------------------------------------------
var iLinkID: integer;
  blVector: TblVector;
begin
  iLinkID:=dmLinkFreqPlan_Link.GetLinkID (aID);

  if dmLink.GetBLVector(iLinkID, blVector) then
    ShowVectorOnMap (blVector, aID);

end;

//--------------------------------------------------------------------
// actions
//--------------------------------------------------------------------
procedure TdmAct_LinkFreqPlan_Link.DoAction(Sender: TObject);
begin
  if Sender=act_Object_ShowOnMap then
    ShowOnMap (FFocusedID);
end;


//--------------------------------------------------------------------
function TdmAct_LinkFreqPlan_Link.ItemDel(aID: integer; aName: string = ''):
    boolean;
//--------------------------------------------------------------------
begin
  Result:=dmLinkFreqPlan_link.Del (aID);
end;

//--------------------------------------------------------------------
function TdmAct_LinkFreqPlan_Link.GetViewForm (aOwnerForm: TForm): TForm;
//--------------------------------------------------------------------
begin
  Result:=Tframe_Link_View.Create(aOwnerForm);
  (Result as Tframe_Link_View).ViewMode:=vmLinkFreqPlan_Link;
end;

//--------------------------------------------------------------------
procedure TdmAct_LinkFreqPlan_Link.GetPopupMenu;
//--------------------------------------------------------------------
begin
  case aMenuType of
    mtItem:   begin
                AddMenuItem (aPopupMenu, act_Object_ShowOnMap);
 //               AddMenuItem (aPopupMenu, nil);
 //               AddMenuItem (aPopupMenu, act_Del_);
              end;
 //   mtList:
  //              AddMenuItem (aPopupMenu, act_Del_list);
  end;
end;


begin

end.

