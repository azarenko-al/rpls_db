unit f_LinkFreqPlan_linkend_edit;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxControls, cxInplaceContainer, cxVGrid, cxDBVGrid, DB,
  rxPlacemnt;

type
  Tfrm_LinkFreqPlan_linkend_edit = class(TForm)
    cxDBVerticalGrid1: TcxDBVerticalGrid;
    DataSource1: TDataSource;
    FormPlacement1: TFormPlacement;
  private
    { Private declarations }
  public
    class procedure ExecDlg(aDataset: TDataSet);

  end;

(*var
  frm_LinkFreqPlan_linkend_edit: Tfrm_LinkFreqPlan_linkend_edit;
*)

implementation

{$R *.dfm}


class procedure Tfrm_LinkFreqPlan_linkend_edit.ExecDlg(aDataset: TDataSet);
begin
  with Tfrm_LinkFreqPlan_linkend_edit.Create(Application) do
  begin
    DataSource1.Dataset := aDataset;

    ShowModal;
  end;

end;

end.
