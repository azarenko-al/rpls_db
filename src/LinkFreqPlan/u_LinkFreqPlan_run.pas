unit u_LinkFreqPlan_run;

interface

uses
  IniFiles, SysUtils, Dialogs,

//  dm_LinkFreqPlan_CIA,


  u_files,
  u_const,
  u_func,

  dm_Main,

  u_ini_LinkFreqPlan_CIA_params,

  u_ini_LinkFreqPlan_nb_calc,

  u_Vars;



type
  TLinkFreqPlan_run = class(TObject)

  private
  public
    class procedure CreateMap_Interference(aLinkFreqPlan_ID: integer);
    class procedure Exec_Calc(aLinkFreqPlan_ID: integer);
    class procedure RunCalc_CIA(aLinkFreqPlan_ID: integer; aUseFixedChannels:
        boolean);

    class procedure RunCalc_FREQPLAN_NB_calc(aLinkFreqPlan_ID: integer;
        aIsFullCalc: boolean);
  end;

implementation



//--------------------------------------------------------------------
class procedure TLinkFreqPlan_run.Exec_Calc(aLinkFreqPlan_ID: integer);
//--------------------------------------------------------------------
const
  TEMP_INI_FILE = 'LinkFreqPlan_calc.ini';
var
  iCode: Integer;
  oInifile: TIniFile;
  sIniFile: string;
begin
  sIniFile := g_ApplicationDataDir + TEMP_INI_FILE;

  DeleteFile(sIniFile);

  oInifile:=TIniFile.Create (sIniFile);

  oInifile.WriteInteger ('main', 'LinkFreqPlanID',  aLinkFreqPlan_ID);
  oInifile.WriteString ('main', 'ConnectionString',  dmMain.GetConnectionString);

//  oInifile.WriteInteger ('main', 'ProjectID',       dmMain.ProjectID);
  oInifile.Free;

  RunApp (GetApplicationDir() + EXE_LINKFREQPLAN_CALC, DoubleQuotedStr(sIniFile), iCode);

end;

//--------------------------------------------------------------------
class procedure TLinkFreqPlan_run.CreateMap_Interference(aLinkFreqPlan_ID:
    integer);
//--------------------------------------------------------------------
const
  TEMP_FILENAME = 'LinkFreqPlan_CIA_maps.ini';
var
  iCode: Integer;
  oInifile: TIniFile;
  sIniFile: string;
begin
  sIniFile := g_ApplicationDataDir + TEMP_FILENAME;

  DeleteFile(sIniFile);

  oInifile:=TIniFile.Create (sIniFile);
  oInifile.WriteInteger ('main', 'LinkFreqPlanID',  aLinkFreqPlan_ID);
  oInifile.WriteInteger ('main', 'ProjectID',       dmMain.ProjectID);

  oInifile.WriteString ('main', 'ConnectionString',  dmMain.GetConnectionString);


  FreeAndNil(oInifile);

  RunApp (GetApplicationDir() + EXE_LINKFREQPLAN_CIA_maps, DoubleQuotedStr(sIniFile), iCode);

end;

//--------------------------------------------------------------------
class procedure TLinkFreqPlan_run.RunCalc_FREQPLAN_NB_calc(aLinkFreqPlan_ID:
    integer; aIsFullCalc: boolean);
//--------------------------------------------------------------------
const
  TEMP_INI_FILE = 'LinkFreqPlan_NB_Calc.ini';
var
  iCode: Integer;
  oParams: TIni_LinkFreqPlan_nb_calc_Params;
  sIniFile: string;
begin
  sIniFile := g_ApplicationDataDir + TEMP_INI_FILE;

  oParams:=TIni_LinkFreqPlan_nb_calc_Params.Create;

  oParams.LinkFreqPlanID := aLinkFreqPlan_ID;
  oParams.IsFullCalc := aIsFullCalc;
  oParams.ProjectID := dmMain.ProjectID;
  oParams.ConnectionString:=dmMain.GetConnectionString;

  oParams.SaveToFile(sIniFile);

  FreeAndNil(oParams);


  RunApp (GetApplicationDir() + EXE_LINK_FREQPLAN_NB_CALC, DoubleQuotedStr(sIniFile), iCode);

end;



//--------------------------------------------------------------------
class procedure TLinkFreqPlan_run.RunCalc_CIA(aLinkFreqPlan_ID: integer;
    aUseFixedChannels: boolean);
//--------------------------------------------------------------------
const
  TEMP_INI_FILE = 'LinkFreqPlan_cia1.ini';
var
  iCode: Integer;
 // oInifile: TIniFile;
  sIniFile: string;

  oParams: TIni_LinkFreqPlan_CIA_params;

begin
 // Clear_Interference(aLinkFreqPlan_ID);


  // ---------------------------------------------------------------
  sIniFile := g_ApplicationDataDir + TEMP_INI_FILE;


 // ShowMessage(sIniFile);


  DeleteFile(sIniFile);


  oParams:=TIni_LinkFreqPlan_CIA_params.Create;

  oParams.LinkFreqPlanID:= aLinkFreqPlan_ID;
  oParams.ChannelsKind:= IIF (aUseFixedChannels, 'fixed', 'Distributed');
  oParams.ConnectionString:=dmMain.GetConnectionString;

  oParams.SaveToFile(sIniFile);

  FreeAndNil(oParams);



  RunApp (GetApplicationDir() + EXE_LINK_FREQPLAN_CIA, DoubleQuotedStr(sIniFile) , iCode);
//          Format('%d %s',[aLinkFreqPlan_ID, aIsFullCalc]) );

  {


  TdmLinkFreqPlan_CIA.Init;


  with dmLinkFreqPlan_CIA do
  begin
    Params.LinkFreqPlan_ID:= aLinkFreqPlan_ID;

    if aUseFixedChannels then
      Params.ChannelsKind := opUseFixedChannels
    else
      Params.ChannelsKind := opUseDistrChannels;


//    Params.ChannelsKind := IIF (sChannelType = bUseFixedChannels, opUseDistrChannels, opUseFixedChannels);

  end;


 // dmLinkFreqPlan_CIA1.ExecuteProc();

  dmLinkFreqPlan_CIA.ExecuteDlg ('������ �������������');


  FreeAndNil(dmLinkFreqPlan_CIA);


  }

end;





end.

      {

//--------------------------------------------------------------------
procedure TdmLinkFreqPlan_Tools.Calc_Interference(aLinkFreqPlan_ID: integer;
    aUseFixedChannels: boolean);
//--------------------------------------------------------------------
const
  TEMP_INI_FILE = 'LinkFreqPlan_cia.ini';
var
 // oInifile: TIniFile;
  sIniFile: string;

  oParams: TIni_LinkFreqPlan_CIA_params;

begin
  Clear_Interference(aLinkFreqPlan_ID);

  // ---------------------------------------------------------------
  sIniFile := g_ApplicationDataDir + TEMP_INI_FILE;


  DeleteFile(sIniFile);


  oParams:=TIni_LinkFreqPlan_CIA_params.Create;
  oParams.LinkFreqPlanID:= aLinkFreqPlan_ID;
  oParams.ChannelsKind:= IIF (aUseFixedChannels, 'fixed', 'Distributed');

  oParams.SaveToFile(sIniFile);

  FreeAndNil(oParams);

 {

  oInifile:=TIniFile.Create (sIniFile);
  oInifile.WriteInteger ('main', 'ID',  aLinkFreqPlan_ID);
  oInifile.WriteInteger ('main', 'LinkFreqPlanID',  aLinkFreqPlan_ID);
  oInifile.WriteString  ('main', 'ChannelsKind', IIF (aUseFixedChannels, 'fixed', 'Distributed'));
  oInifile.WriteInteger ('main', 'ProjectID',       dmMain.ProjectID);

  FreeAndNil(oInifile);
//  oInifile.Free;
  }

  //RunApp (GetApplicationDir() + EXE_LINK_FREQPLAN_CIA, DoubleQuotedStr(sIniFile));
//          Format('%d %s',[aLinkFreqPlan_ID, aIsFullCalc]) );


{

  with dmLinkFreqPlan_CIA do begin
    Options.Link_FreqPlan_ID:= aLinkFreqPlan_ID;
    Options.ChannelsKind:= IIF (aUseFixedChannels, opUseFixedChannels, opUseDistrChannels);

    ExecuteDlg ('������ �������������');
  end;}

 // CreateMap_Interference(aLinkFreqPlan_ID);
end;

