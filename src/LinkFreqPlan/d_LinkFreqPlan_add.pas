unit d_LinkFreqPlan_add;

interface

uses
  Classes, Controls, Forms, cxVGrid, ActnList, Dialogs,

  dm_User_Security,

 // dm_Language,
  dm_Main,
             
  d_Wizard_add_with_Inspector,

  dm_LinkFreqPlan,

//  u_types,
  u_const_str,

  u_cx_VGrid, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, dxSkinsCore, dxSkinsDefaultPainters, cxStyles,
  cxEdit, cxCurrencyEdit, cxInplaceContainer, cxPropertiesStore,
  rxPlacemnt, ComCtrls, StdCtrls, ExtCtrls ;

type
  Tdlg_LinkFreqPlan_add = class(Tdlg_Wizard_add_with_Inspector)
    act_Add: TAction;
    cxVerticalGrid1: TcxVerticalGrid;
    row_max_distanse_km: TcxEditorRow;
    row_Min_CI_dB: TcxEditorRow;

    procedure FormCreate(Sender: TObject);
    procedure act_OkExecute(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure ActionList1Update(Action: TBasicAction; var Handled: Boolean);

  private
    FRec: TdmFreqPlanRRLAddRec;

  private
    FID  : integer;

  public
    class function ExecDlg(aLinkNetID: Integer): integer;
  end;


//====================================================================
implementation  {$R *.dfm}

//--------------------------------------------------------------------
class function Tdlg_LinkFreqPlan_add.ExecDlg(aLinkNetID: Integer): integer;
//--------------------------------------------------------------------
begin
{
  // ---------------------------------------------------------------
  if dmUser_Security.ProjectIsReadOnly then
  begin
    ShowMessage('���������� �������� ������. ������ ������ ��� ������.');
    Result:=0;
    Exit;
  end;
  // ---------------------------------------------------------------
}

  with Tdlg_LinkFreqPlan_add.Create(Application) do
  try
    FRec.LinkNet_ID:= aLinkNetID;

    ed_Name_.Text:=dmLinkFreqPlan.GetNewName();

    if (ShowModal=mrOk) then
      Result:=FID
    else
      Result:=0;

  finally
    Free;
  end;
end;


//-------------------------------------------------------------------
procedure Tdlg_LinkFreqPlan_add.FormCreate(Sender: TObject);
//-------------------------------------------------------------------
begin
  inherited;

  SetActionName (STR_DLG_ADD_Freq_Plan_RRL);

 // ts_Params.Caption:= '���������';

  cx_InitVerticalGrid (cxVerticalGrid1);


 // TdmLanguage.CheckForm(Self);


  act_Ok.OnExecute:=act_OkExecute;
end;

//-------------------------------------------------------------------
procedure Tdlg_LinkFreqPlan_add.FormDestroy(Sender: TObject);
//-------------------------------------------------------------------
begin

  inherited;
end;

//--------------------------------------------------------------------
procedure Tdlg_LinkFreqPlan_add.act_OkExecute(Sender: TObject);
//--------------------------------------------------------------------
begin

  FRec.NewName            := ed_Name_.Text;
  FRec.MaxCalcDistanse_km := row_max_distanse_km.Properties.Value;
  FRec.Min_CI_dB          := row_Min_CI_dB.Properties.Value;

  FID:=dmLinkFreqPlan.Add (FRec);



//
////  if aFolderID=0 then
//    sFolderGUID:=g_Obj.ItemByName[OBJ_PROPERTY].RootFolderGUID;
// // else
//  //  sFolderGUID:=dmFolder.GetGUIDByID(aFolderID);
//
//  g_ShellEvents.Shell_UpdateNodeChildren_ByGUID (sFolderGUID);

end;

//-------------------------------------------------------------------
procedure Tdlg_LinkFreqPlan_add.ActionList1Update(Action: TBasicAction;
  var Handled: Boolean);
//-------------------------------------------------------------------
begin
  inherited;

  act_Ok.Enabled:=
    (row_max_distanse_km.Properties.Value > 0) and
    (row_Min_CI_dB.Properties.Value > 0)
    ;

end;


end.