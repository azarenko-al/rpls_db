object frame_LinkFreqPlan_NeightBors: Tframe_LinkFreqPlan_NeightBors
  Left = 655
  Top = 702
  Width = 1148
  Height = 484
  Caption = 'frame_LinkFreqPlan_NeightBors'
  Color = clBtnFace
  ParentFont = True
  OldCreateOrder = True
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 429
    Width = 1140
    Height = 27
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 0
    DesignSize = (
      1140
      27)
    object pnLegend1: TPanel
      Left = 3
      Top = 4
      Width = 160
      Height = 21
      Anchors = [akLeft, akBottom]
      BevelOuter = bvSpace
      Caption = #1057#1086#1089#1077#1076#1089#1090#1074#1086' '#1087#1086' CI'
      Color = 8421631
      TabOrder = 0
    end
    object pnLegend2: TPanel
      Left = 166
      Top = 4
      Width = 160
      Height = 21
      Anchors = [akLeft, akBottom]
      BevelOuter = bvSpace
      Caption = 'pnLegend2'
      Color = 8454143
      TabOrder = 1
    end
    object pnLegend3: TPanel
      Left = 329
      Top = 4
      Width = 160
      Height = 21
      Anchors = [akLeft, akBottom]
      BevelOuter = bvSpace
      Caption = 'pnLegend3'
      Color = 8454016
      TabOrder = 2
    end
  end
  object cxDBTreeList1: TcxDBTreeList
    Left = 0
    Top = 28
    Width = 1140
    Height = 119
    Align = alTop
    Bands = <
      item
        Visible = False
      end
      item
        Caption.AlignHorz = taCenter
        Caption.Text = #1052#1072#1090#1088#1080#1094#1072' '#1074#1079#1072#1080#1084#1085#1086#1075#1086' '#1074#1083#1080#1103#1085#1080#1103
      end
      item
        Caption.AlignHorz = taCenter
        Caption.Text = #1069#1052#1057
      end
      item
        Caption.AlignHorz = taCenter
        Visible = False
      end>
    DataController.DataSource = ds_Items
    DataController.ParentField = 'tree_parent_id'
    DataController.KeyField = 'tree_id'
    LookAndFeel.Kind = lfFlat
    OptionsBehavior.GoToNextCellOnTab = True
    OptionsBehavior.ImmediateEditor = False
    OptionsBehavior.AutoDragCopy = True
    OptionsBehavior.DragCollapse = False
    OptionsBehavior.ExpandOnIncSearch = True
    OptionsBehavior.ShowHourGlass = False
    OptionsCustomizing.BandCustomizing = False
    OptionsCustomizing.BandVertSizing = False
    OptionsCustomizing.ColumnVertSizing = False
    OptionsData.Deleting = False
    OptionsSelection.HideFocusRect = False
    OptionsSelection.InvertSelect = False
    OptionsView.CellTextMaxLineCount = -1
    OptionsView.ShowEditButtons = ecsbFocused
    OptionsView.Bands = True
    OptionsView.GridLineColor = clBtnShadow
    OptionsView.GridLines = tlglBoth
    OptionsView.Indicator = True
    ParentColor = False
    PopupMenu = PopupMenu1
    Preview.AutoHeight = False
    Preview.MaxLineCount = 2
    RootValue = -1
    Styles.OnGetContentStyle = cxDBTreeList1StylesGetContentStyle
    TabOrder = 1
    OnExpanding = cxDBTreeList1Expanding
    object col__tree_id: TcxDBTreeListColumn
      Tag = -1
      PropertiesClassName = 'TcxTextEditProperties'
      Properties.Alignment.Horz = taLeftJustify
      Properties.MaxLength = 0
      Properties.ReadOnly = True
      Visible = False
      DataBinding.FieldName = 'tree_id'
      Options.Customizing = False
      Options.Editing = False
      Width = 51
      Position.ColIndex = 15
      Position.RowIndex = 0
      Position.BandIndex = 1
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object col__tree_parent_id: TcxDBTreeListColumn
      Tag = -1
      PropertiesClassName = 'TcxTextEditProperties'
      Properties.Alignment.Horz = taLeftJustify
      Properties.MaxLength = 0
      Properties.ReadOnly = True
      Visible = False
      DataBinding.FieldName = 'tree_parent_id'
      Options.Customizing = False
      Options.Editing = False
      Width = 92
      Position.ColIndex = 14
      Position.RowIndex = 0
      Position.BandIndex = 1
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object col__TX_LinkEnd_Name: TcxDBTreeListColumn
      PropertiesClassName = 'TcxTextEditProperties'
      Properties.Alignment.Horz = taLeftJustify
      Properties.MaxLength = 0
      Properties.ReadOnly = True
      DataBinding.FieldName = 'TX_LinkEnd_Name'
      Options.Editing = False
      Width = 103
      Position.ColIndex = 0
      Position.RowIndex = 0
      Position.BandIndex = 1
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object col__RX_LinkEnd_Name: TcxDBTreeListColumn
      PropertiesClassName = 'TcxTextEditProperties'
      Properties.Alignment.Horz = taLeftJustify
      Properties.MaxLength = 0
      Properties.ReadOnly = True
      DataBinding.FieldName = 'RX_LinkEnd_Name'
      Options.Editing = False
      Width = 136
      Position.ColIndex = 1
      Position.RowIndex = 0
      Position.BandIndex = 1
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object col__Band: TcxDBTreeListColumn
      PropertiesClassName = 'TcxTextEditProperties'
      Properties.Alignment.Horz = taLeftJustify
      Properties.MaxLength = 0
      Properties.ReadOnly = True
      DataBinding.FieldName = 'Band'
      Options.Editing = False
      Width = 61
      Position.ColIndex = 4
      Position.RowIndex = 0
      Position.BandIndex = 1
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object col__CI: TcxDBTreeListColumn
      PropertiesClassName = 'TcxTextEditProperties'
      Properties.Alignment.Horz = taRightJustify
      Properties.MaxLength = 0
      Properties.ReadOnly = True
      DataBinding.FieldName = 'CI'
      Options.Editing = False
      Width = 51
      Position.ColIndex = 5
      Position.RowIndex = 0
      Position.BandIndex = 1
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object col__Rx_level_dBm: TcxDBTreeListColumn
      PropertiesClassName = 'TcxTextEditProperties'
      Properties.Alignment.Horz = taRightJustify
      Properties.MaxLength = 0
      Properties.ReadOnly = True
      DataBinding.FieldName = 'Rx_level_dBm'
      Options.Editing = False
      Width = 136
      Position.ColIndex = 6
      Position.RowIndex = 0
      Position.BandIndex = 1
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object col__Neighbour_Count: TcxDBTreeListColumn
      PropertiesClassName = 'TcxTextEditProperties'
      Properties.Alignment.Horz = taRightJustify
      Properties.MaxLength = 0
      Properties.ReadOnly = True
      DataBinding.FieldName = 'Neighbour_Count'
      Options.Editing = False
      Width = 94
      Position.ColIndex = 8
      Position.RowIndex = 0
      Position.BandIndex = 1
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object col__is_expanded: TcxDBTreeListColumn
      Tag = -1
      PropertiesClassName = 'TcxTextEditProperties'
      Properties.Alignment.Horz = taLeftJustify
      Properties.MaxLength = 0
      Properties.ReadOnly = True
      Visible = False
      DataBinding.FieldName = 'is_expanded'
      Options.Editing = False
      Width = 91
      Position.ColIndex = 13
      Position.RowIndex = 0
      Position.BandIndex = 1
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object col__Extra_Loss: TcxDBTreeListColumn
      PropertiesClassName = 'TcxTextEditProperties'
      Properties.Alignment.Horz = taLeftJustify
      Properties.MaxLength = 0
      Properties.ReadOnly = True
      DataBinding.FieldName = 'Extra_Loss'
      Width = 61
      Position.ColIndex = 9
      Position.RowIndex = 0
      Position.BandIndex = 1
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object col__CIA: TcxDBTreeListColumn
      PropertiesClassName = 'TcxCurrencyEditProperties'
      Properties.DisplayFormat = ',0.0'
      DataBinding.FieldName = 'CIA'
      Options.Editing = False
      Width = 53
      Position.ColIndex = 0
      Position.RowIndex = 0
      Position.BandIndex = 2
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object col__CI_: TcxDBTreeListColumn
      PropertiesClassName = 'TcxCurrencyEditProperties'
      Properties.DisplayFormat = ',0.0'
      DataBinding.FieldName = 'CI_'
      Options.Editing = False
      Width = 50
      Position.ColIndex = 1
      Position.RowIndex = 0
      Position.BandIndex = 2
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object col__CA: TcxDBTreeListColumn
      PropertiesClassName = 'TcxCurrencyEditProperties'
      Properties.DisplayFormat = ',0.0'
      DataBinding.FieldName = 'CA'
      Options.Editing = False
      Width = 70
      Position.ColIndex = 2
      Position.RowIndex = 0
      Position.BandIndex = 2
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object col__threshold_degradation: TcxDBTreeListColumn
      PropertiesClassName = 'TcxTextEditProperties'
      Properties.Alignment.Horz = taRightJustify
      Properties.MaxLength = 0
      Properties.ReadOnly = True
      DataBinding.FieldName = 'threshold_degradation'
      Options.Editing = False
      Width = 116
      Position.ColIndex = 19
      Position.RowIndex = 0
      Position.BandIndex = 1
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object col__threshold_degradation_cia: TcxDBTreeListColumn
      PropertiesClassName = 'TcxCurrencyEditProperties'
      Properties.DisplayFormat = ',0.0'
      DataBinding.FieldName = 'threshold_degradation_cia'
      Options.Editing = False
      Width = 131
      Position.ColIndex = 3
      Position.RowIndex = 0
      Position.BandIndex = 2
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object col__Distance_M: TcxDBTreeListColumn
      PropertiesClassName = 'TcxTextEditProperties'
      Properties.Alignment.Horz = taRightJustify
      Properties.MaxLength = 0
      Properties.ReadOnly = True
      DataBinding.FieldName = 'Distance_M'
      Options.Editing = False
      Width = 86
      Position.ColIndex = 3
      Position.RowIndex = 0
      Position.BandIndex = 1
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object col__TX_LinkEnd_ID: TcxDBTreeListColumn
      PropertiesClassName = 'TcxTextEditProperties'
      Properties.Alignment.Horz = taLeftJustify
      Properties.MaxLength = 0
      Properties.ReadOnly = True
      Visible = False
      DataBinding.FieldName = 'TX_LinkEnd_ID'
      Options.Customizing = False
      Width = 86
      Position.ColIndex = 11
      Position.RowIndex = 0
      Position.BandIndex = 1
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object col__LinkEnd_ID: TcxDBTreeListColumn
      PropertiesClassName = 'TcxTextEditProperties'
      Properties.Alignment.Horz = taLeftJustify
      Properties.MaxLength = 0
      Properties.ReadOnly = True
      Visible = False
      DataBinding.FieldName = 'LinkEnd_ID'
      Width = 65
      Position.ColIndex = 10
      Position.RowIndex = 0
      Position.BandIndex = 1
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object col__POWER_CIA: TcxDBTreeListColumn
      PropertiesClassName = 'TcxTextEditProperties'
      Properties.Alignment.Horz = taLeftJustify
      Properties.MaxLength = 0
      Properties.ReadOnly = True
      Visible = False
      DataBinding.FieldName = 'POWER_CIA'
      Width = 70
      Position.ColIndex = 0
      Position.RowIndex = 0
      Position.BandIndex = 3
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object col__type: TcxDBTreeListColumn
      PropertiesClassName = 'TcxTextEditProperties'
      Properties.Alignment.Horz = taLeftJustify
      Properties.MaxLength = 0
      Properties.ReadOnly = True
      Visible = False
      DataBinding.FieldName = 'type'
      Options.Customizing = False
      Options.Editing = False
      Width = 31
      Position.ColIndex = 12
      Position.RowIndex = 0
      Position.BandIndex = 1
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object col_FLD_DIAGRAM_LEVEL: TcxDBTreeListColumn
      Caption.Text = #1054#1089#1083#1072#1073#1083#1077#1085#1080#1077' '#1087#1086' '#1044#1053#1040' (dB)'
      DataBinding.FieldName = 'DIAGRAM_LEVEL'
      Options.Editing = False
      Width = 145
      Position.ColIndex = 16
      Position.RowIndex = 0
      Position.BandIndex = 1
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object col_Azimuth: TcxDBTreeListColumn
      Visible = False
      DataBinding.FieldName = 'Azimuth'
      Position.ColIndex = 2
      Position.RowIndex = 0
      Position.BandIndex = 1
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object col_TX_LinkEnd_id: TcxDBTreeListColumn
      DataBinding.FieldName = 'TX_LinkEnd_id'
      Position.ColIndex = 0
      Position.RowIndex = 0
      Position.BandIndex = 0
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object col_RX_LinkEnd_id: TcxDBTreeListColumn
      DataBinding.FieldName = 'RX_LinkEnd_id'
      Position.ColIndex = 1
      Position.RowIndex = 0
      Position.BandIndex = 0
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object col_Freq_Spacing_MHz: TcxDBTreeListColumn
      Caption.Text = #1063#1072#1089#1090#1086#1090#1085#1099#1081' '#1088#1072#1079#1085#1086#1089' (MHz)'
      DataBinding.FieldName = 'Freq_Spacing_MHz'
      Options.Editing = False
      Width = 137
      Position.ColIndex = 17
      Position.RowIndex = 0
      Position.BandIndex = 1
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object col_POWER_noise: TcxDBTreeListColumn
      PropertiesClassName = 'TcxCurrencyEditProperties'
      Properties.DisplayFormat = ',0.0'
      DataBinding.FieldName = 'POWER_noise'
      Options.Editing = False
      Width = 100
      Position.ColIndex = 18
      Position.RowIndex = 0
      Position.BandIndex = 1
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object col_INTERFERENCE: TcxDBTreeListColumn
      DataBinding.FieldName = 'INTERFERENCE'
      Width = 100
      Position.ColIndex = 7
      Position.RowIndex = 0
      Position.BandIndex = 1
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
  end
  object qry_NeighBours1111: TADOQuery
    MarshalOptions = moMarshalModifiedOnly
    EnableBCD = False
    Parameters = <>
    Left = 564
    Top = 237
  end
  object ds_Items: TDataSource
    DataSet = mem_Items
    Left = 432
    Top = 237
  end
  object mem_Items: TdxMemData
    Indexes = <>
    SortOptions = []
    Left = 376
    Top = 237
  end
  object ActionList11111: TActionList
    Left = 704
    Top = 240
    object act_Neighbors_calc: TAction
      Caption = 'act_Neighbors_calc'
    end
    object act_Calc_CI: TAction
      Caption = 'act_Calc_CI'
    end
    object act_Expand: TAction
      Caption = 'act_Expand'
    end
    object act_Collapse: TAction
      Caption = 'act_Collapse'
    end
    object act_Setup_Grid: TAction
      Caption = #1053#1072#1089#1090#1088#1086#1081#1082#1072
    end
    object act_Clear_Extra_Loss: TAction
      Caption = #1054#1095#1080#1089#1090#1080#1090#1100' '#1076#1086#1087'. '#1086#1089#1083#1072#1073#1083#1077#1085#1080#1077
    end
  end
  object PopupMenu1: TPopupMenu
    Left = 232
    Top = 240
    object actExpand1: TMenuItem
      Action = act_Expand
    end
    object actCollapse1: TMenuItem
      Action = act_Collapse
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object N1: TMenuItem
      Action = act_Clear_Extra_Loss
      Caption = #1054#1095#1080#1089#1090#1080#1090#1100' '#1076#1086#1087#1086#1083#1085'. '#1086#1089#1083#1072#1073#1083#1077#1085#1080#1077
    end
  end
  object cxStyleRepository1: TcxStyleRepository
    Left = 64
    Top = 176
    PixelsPerInch = 96
    object cxStyle_red: TcxStyle
      AssignedValues = [svColor]
      Color = 8421631
    end
    object cxStyle_LINK: TcxStyle
      AssignedValues = [svColor]
      Color = clLime
    end
    object cxStyle_PROP: TcxStyle
      AssignedValues = [svColor]
      Color = clYellow
    end
  end
  object ADOStoredProc1: TADOStoredProc
    Parameters = <>
    Left = 872
    Top = 248
  end
  object DataSource1: TDataSource
    DataSet = ADOStoredProc1
    Left = 872
    Top = 328
  end
  object FormStorage1: TFormStorage
    Active = False
    IniFileName = 'Software\Onega\'
    Options = []
    StoredValues = <>
    Left = 1016
    Top = 248
  end
  object dxBarManager1: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    Categories.Strings = (
      'Default')
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    LookAndFeel.Kind = lfFlat
    PopupMenuLinks = <>
    SunkenBorder = True
    UseSystemFont = True
    Left = 64
    Top = 296
    DockControlHeights = (
      0
      0
      28
      0)
    object dxBarManager1Bar1: TdxBar
      AllowQuickCustomizing = False
      Caption = 'Custom 1'
      CaptionButtons = <>
      DockedDockingStyle = dsTop
      DockedLeft = 0
      DockedTop = 0
      DockingStyle = dsTop
      FloatLeft = 801
      FloatTop = 229
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarButton2'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarButton1'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarButton3'
        end>
      OneOnRow = True
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarButton1: TdxBarButton
      Action = act_Calc_CI
      Category = 0
    end
    object dxBarButton2: TdxBarButton
      Action = act_Neighbors_calc
      Category = 0
    end
    object dxBarButton3: TdxBarButton
      Action = act_Setup_Grid
      Category = 0
    end
  end
end
