inherited frame_LinkFreqPlan_view: Tframe_LinkFreqPlan_view
  Left = 783
  Top = 269
  Width = 977
  Height = 529
  Caption = 'frame_LinkFreqPlan_view'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited Panel1: TPanel
    Width = 961
    Height = 56
  end
  inherited pn_Caption: TPanel
    Top = 83
    Width = 961
  end
  inherited pn_Main: TPanel
    Top = 100
    Width = 961
    Height = 175
    object PageControl1: TPageControl
      Left = 1
      Top = 4
      Width = 959
      Height = 113
      ActivePage = TabSheet_Insp
      Align = alTop
      Style = tsFlatButtons
      TabOrder = 0
      OnChange = PageControl1Change
      object TabSheet_Insp: TTabSheet
        Caption = #1055#1072#1088#1072#1084#1077#1090#1088#1099
      end
      object TabSheet_Neighbors: TTabSheet
        Caption = #1057#1086#1089#1077#1076#1080
        ImageIndex = 1
      end
      object TabSheet_Resource: TTabSheet
        Caption = #1063#1072#1089#1090#1086#1090#1085#1099#1081' '#1088#1077#1089#1091#1088#1089
        ImageIndex = 2
      end
      object TabSheet_LinkEnd: TTabSheet
        Caption = #1063#1072#1089#1090#1086#1090#1085#1086#1077' '#1088#1072#1089#1087#1088#1077#1076#1077#1083#1077#1085#1080#1077
        ImageIndex = 3
      end
      object TabSheet_Report: TTabSheet
        Caption = #1054#1090#1095#1077#1090
        ImageIndex = 4
      end
      object TabSheet_Neighbors_: TTabSheet
        Caption = #1057#1086#1089#1077#1076#1080'_'
        ImageIndex = 5
      end
      object TabSheet_LinkEnd_: TTabSheet
        Caption = #1063#1072#1089#1090#1086#1090#1085#1086#1077' '#1088#1072#1089#1087#1088#1077#1076#1077#1083#1077#1085#1080#1077'_'
        ImageIndex = 6
      end
    end
    object Panel2: TPanel
      Left = 1
      Top = 1
      Width = 959
      Height = 3
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 1
    end
  end
  inherited MainActionList: TActionList
    Left = 64
    Top = 4
  end
  inherited ImageList1: TImageList
    Left = 4
    Top = 4
  end
  inherited PopupMenu1: TPopupMenu
    Left = 36
    Top = 4
  end
  inherited cxPropertiesStore: TcxPropertiesStore
    Left = 208
    Top = 4
  end
  inherited dxBarManager1: TdxBarManager
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    DockControlHeights = (
      0
      0
      27
      0)
  end
  object FormPlacement1: TFormPlacement
    IniFileName = 'Software\Onega\'
    Left = 172
    Top = 5
  end
end
