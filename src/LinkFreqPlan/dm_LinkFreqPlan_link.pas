unit dm_LinkFreqPlan_link;

interface

uses
  Classes, Forms, Db, ADODB, Variants,

  dm_Main,
  u_db,
  u_classes,

  u_const_db
  ;


type
  TdmLinkFreqPlan_link = class(TDataModule)
    qry_LinkEnd: TADOQuery;
    qry_Temp: TADOQuery;
    procedure DataModuleCreate(Sender: TObject);
  private
    procedure AddList1(aLinkFreqPlan_ID: integer; aIDList: TIDList);
// TODO: DelByLinkID1
//  procedure DelByLinkID1(aLinkFreqPlan_ID, aLinkID: integer);


  public
// TODO: Del
  function  Del (aID: integer): boolean;

    function GetLinkID(aID: integer): Integer;
    procedure GetLinkIDList(aLinkFreqPlan_ID: integer; var aIDList: TIDList);
  end;

  
function dmLinkFreqPlan_link: TdmLinkFreqPlan_link;

//===================================================================
implementation

uses dm_Onega_db_data; {$R *.DFM}
//===================================================================

var
  FdmLinkFreqPlan_link: TdmLinkFreqPlan_link;


// ---------------------------------------------------------------
function dmLinkFreqPlan_link: TdmLinkFreqPlan_link;
// ---------------------------------------------------------------
begin
  if not Assigned(FdmLinkFreqPlan_link) then
    FdmLinkFreqPlan_link := TdmLinkFreqPlan_link.Create(Application);

  Result := FdmLinkFreqPlan_link;
end;

procedure TdmLinkFreqPlan_link.DataModuleCreate(Sender: TObject);
begin
  inherited;

  db_SetComponentADOConnection(Self, dmMain.ADOConnection);
  
end;

// -------------------------------------------------------------------
function TdmLinkFreqPlan_link.GetLinkID(aID: integer): Integer;
// -------------------------------------------------------------------
begin
  Result:=gl_DB.GetIntFieldValue (TBL_LINKFREQPLAN_LINK, FLD_LINK_ID, [db_Par(FLD_ID, aID)]);
end;

//-------------------------------------------------------------------
procedure TdmLinkFreqPlan_link.GetLinkIDList(aLinkFreqPlan_ID: integer; var aIDList:
    TIDList);
//-------------------------------------------------------------------
const
  SQL_SELECT_LINKS =
    'SELECT LinkFreqPlan_id, Link.id, Link.guid, Link.name '+
    ' FROM ' + TBL_Link +
    '        RIGHT OUTER JOIN LinkFreqPlan_Link ON Link.id = LinkFreqPlan_Link.link_id '+
    ' WHERE LinkFreqPlan_id =:id';

var
  oItem: TIDListItem;
begin
  aIDList.Clear;

  dmOnega_DB_data.OpenQuery(qry_Temp,
               SQL_SELECT_LINKS,
              [FLD_ID, aLinkFreqPlan_ID]);

  with qry_Temp do
    while not EOF do
  begin
    oItem:=aIDList.AddID (FieldValues[FLD_ID]);
    oItem.Name:=FieldValues[FLD_NAME];
    oItem.GUID:=FieldValues[FLD_GUID];

    Next;
  end;
end;


//--------------------------------------------------------------------
procedure TdmLinkFreqPlan_link.AddList1(aLinkFreqPlan_ID: integer; aIDList:
    TIDList);
//--------------------------------------------------------------------
var
  i: integer;
begin

(*  for i:=0 to aIDList.Count-1 do
    Add1 (aLinkFreqPlan_ID, aIDList[i].ID);
*)

end;


//--------------------------------------------------------------------
function TdmLinkFreqPlan_link.Del (aID: integer): boolean;
//--------------------------------------------------------------------
begin
  Result:= gl_db.DeleteRecordByID(TBL_LINKFREQPLAN_LINK, aID);
end;


// TODO: Del

// TODO: DelByLinkID1
////--------------------------------------------------------------------
//procedure TdmLinkFreqPlan_link.DelByLinkID1(aLinkFreqPlan_ID, aLinkID: integer);
////--------------------------------------------------------------------
//
//  procedure DoDelLinkEnd(aID: integer);
//  begin
//    gl_DB.DeleteRecords (TBL_LINKFREQPLAN_LINKEND,
//                          [db_par(FLD_LINKFREQPLAN_ID, aLinkFreqPlan_ID),
//                           db_par(FLD_LINKEND_ID,       aID)]);
//  end;
//
//  procedure DoDelLinkEnd_Neighbors(aID: integer);
//  begin
//    gl_DB.DeleteRecords (TBL_LINKFREQPLAN_NEIGHBORS,
//                          [db_par(FLD_LINKFREQPLAN_ID, aLinkFreqPlan_ID),
//                           db_par(FLD_TX_LINKEND_ID,      aID)]);
//    gl_DB.DeleteRecords (TBL_LINKFREQPLAN_NEIGHBORS,
//                          [db_par(FLD_LINKFREQPLAN_ID, aLinkFreqPlan_ID),
//                           db_par(FLD_RX_LINKEND_ID,       aID)]);
//  end;
//
//var
//aLinkEndID1,aLinkEndID2: integer;
//begin
//dmLink.GetLinkEndID (aLinkID, aLinkEndID1,aLinkEndID2);
//
//DoDelLinkEnd(aLinkEndID1);
//DoDelLinkEnd(aLinkEndID2);
//
//DoDelLinkEnd_Neighbors(aLinkEndID1);
//DoDelLinkEnd_Neighbors(aLinkEndID2);
//
//gl_DB.DeleteRecords (TBL_LINKFREQPLAN_LINK,
//                     [db_par(FLD_LINKFREQPLAN_ID, aLinkFreqPlan_ID),
//                      db_par(FLD_LINK_ID,          aLinkID)]);
//
//end;


begin
end.



//----------------------------------------------------------------
//procedure TdmLinkFreqPlan_link.DoGetLinkEnd_Neighbor_Links1(aLinkEndID: integer; out aLinkIDList: TIDList);
// ---------------------------------------------------------------
//const
//  SQL_SELECT_LINK_FOR_LINKEND =
//        'SELECT id FROM ' + TBL_LINK +  //*
//        ' WHERE ' +    //(project_id=:project_id) and  //alex
//        ' ((linkend1_id=:linkend_id) OR (linkend2_id=:linkend_id))';
//
//var iPropID: integer;
//begin
//  iPropID:= dmLinkEnd.GetPropertyID(aLinkEndID);
//  db_O penQuery(qry_LinkEnd, 'SELECT id FROM '+ TBL_LINKEND +   //*
//                            ' WHERE (property_id=:property_id) and '+
                        //    '       (project_id=:project_id) and ' +
//                            '       (NOT (channel_number IS NULL)) and' +
//                            '       (channel_number > 0)',
//                            '       (channel_number <> 0)',
//                            [db_par(FLD_PROPERTY_ID, iPropID)
                            // db_par(FLD_PROJECT_ID,  dmMain.ProjectID)
//                             ]);
//
//  with qry_LinkEnd do
//    while not Eof do
//  begin
//    db_OpenQuery(gl_DB.Query, SQL_SELECT_LINK_FOR_LINKEND,
//                           [//db_par(FLD_PROJECT_ID, dmMain.ProjectID), /alex
//                            db_par(FLD_LINKEND_ID, FieldValues[FLD_ID])]);
//    if gl_DB.Query.RecordCount>0 then
//      aLinkIDList.AddID(gl_DB.Query[FLD_ID]);
//
//    Next;
//  end;
//
//end;




// TODO: Add1111111
//  procedure Add1111111(aLinkFreqPlan_ID, aLinkID: integer);
//    procedure DoGetLinkEnd_Neighbor_Links1(aLinkEndID: integer; out aLinkIDList: TIDList);
//    procedure DoAddLink(aLinkFreqPlan_ID, aLinkID, aLinkEndID1, aLinkEndID2: integer);


// TODO: Add1111111
////--------------------------------------------------------------------
//procedure TdmLinkFreqPlan_link.Add1111111(aLinkFreqPlan_ID, aLinkID: integer);
////--------------------------------------------------------------------
//
//(*    // ---------------------------------------------------------------
//  procedure DoAddLink(aLinkFreqPlan_ID, aLinkID, aLinkEndID1, aLinkEndID2: integer);
//  // ---------------------------------------------------------------
//  var iFreqFixed: integer;
//  begin
//    with dmLinkEnd do
//    begin
//      iFreqFixed:= GetIntFieldValue__(aLinkEndID1, OBJ_LINKEND, FLD_CHANNEL_NUMBER);
//
//      gl_DB.AddRecord(TBL_LINKFREQPLAN_LINKEND,
//            [db_par(FLD_LINKFREQPLAN_ID, aLinkFreqPlan_ID),
//             db_par(FLD_LINKEND_ID,       aLinkEndID1),
//            // db_par(FLD_ENABLED,          aEnabled),
//             db_par(FLD_FREQ_FIXED,       IIF_NULL(iFreqFixed)), //>0, iFreqFixed, null)),
//             db_par(FLD_FREQ_FIXED_TX,    IIF(iFreqFixed>0,
//                  GetIntFieldValue__(aLinkEndID1, OBJ_LINKEND, FLD_TX_FREQ_MHz), null)),
//             db_par(FLD_FREQ_FIXED_RX,    IIF(iFreqFixed>0,
//                  GetIntFieldValue__(aLinkEndID1, OBJ_LINKEND, FLD_RX_FREQ_MHz), null)),
//             db_par(FLD_CH_TYPE_FIXED,
//                  GetIntFieldValue__(aLinkEndID1, OBJ_LINKEND, FLD_CHANNEL_NUMBER_TYPE)),
//             db_par(FLD_FREQ_REQ_COUNT,   1)]);
//
//      iFreqFixed:= GetIntFieldValue__(aLinkEndID2, OBJ_LINKEND, FLD_CHANNEL_NUMBER);
//
//      gl_DB.AddRecord(TBL_LINKFREQPLAN_LINKEND,
//            [db_par(FLD_LINKFREQPLAN_ID, aLinkFreqPlan_ID),
//             db_par(FLD_LINKEND_ID,       aLinkEndID2),
//           //  db_par(FLD_ENABLED,          aEnabled),
//             db_par(FLD_FREQ_FIXED,       IIF_NULL(iFreqFixed)), //>0, iFreqFixed, null)),
//             db_par(FLD_FREQ_FIXED_TX,    IIF(iFreqFixed>0,
//                  GetIntFieldValue__(aLinkEndID2, OBJ_LINKEND, FLD_TX_FREQ_MHz), null)),
//             db_par(FLD_FREQ_FIXED_RX,    IIF(iFreqFixed>0,
//                  GetIntFieldValue__(aLinkEndID2, OBJ_LINKEND, FLD_RX_FREQ_MHz), null)),
//             db_par(FLD_CH_TYPE_FIXED,
//                  GetIntFieldValue__(aLinkEndID2, OBJ_LINKEND, FLD_CHANNEL_NUMBER_TYPE)),
//             db_par(FLD_FREQ_REQ_COUNT,   1)]);
//
//      gl_DB.AddRecord(TBL_LINKFREQPLAN_LINK,
//            [db_par(FLD_LINKFREQPLAN_ID, aLinkFreqPlan_ID),
//             db_par(FLD_LINK_ID,          aLinkID)
//           //  db_par(FLD_ENABLED,          aEnabled)
//
//             ]);
//
//    end;
//  end;
//
//
//
//const
//SQL_UPDATE_FREQPLAN_LINKEND =
//  'UPDATE '+ TBL_LINKFREQPLAN_LINKEND +
//  '  SET enabled=:enabled                       '+
//  '  WHERE (linkend_id=:linkend_id) and         '+
//  '        (LinkFreqPlan_id=:LinkFreqPlan_id) ';
//
//var
//i, iID: integer;
//iLinkEndID1,iLinkEndID2: integer;
//oLinkIDList: TIDList;
//*)
//begin
//
//(*  CursorHourGlass;
//
//oLinkIDList:= TIDList.Create;
//iID := gl_DB.GetIntFieldValue(TBL_LINKFREQPLAN_LINK, FLD_ID,
//               [db_par(FLD_LINKFREQPLAN_ID, aLinkFreqPlan_ID),
//                db_Par(FLD_LINK_ID,          aLinkID)]);
//
//if iID > 0  then
//begin
//  dmLink.GetLinkEndID (aLinkID, iLinkEndID1,iLinkEndID2);
//
//  gl_DB.ExecCommand (SQL_UPDATE_FREQPLAN_LINKEND,
//                            [
//                             //db_Par(FLD_ENABLED,    1),
//                             db_par(FLD_LINKFREQPLAN_ID, aLinkFreqPlan_ID),
//                             db_Par(FLD_LINKEND_ID,       iLinkEndID1)]);
//
//  gl_DB.ExecCommand (SQL_UPDATE_FREQPLAN_LINKEND,
//                            [
//                             //db_Par(FLD_ENABLED,    1),
//                             db_par(FLD_LINKFREQPLAN_ID, aLinkFreqPlan_ID),
//                             db_Par(FLD_LINKEND_ID,       iLinkEndID2)]);
//
////    gl_DB.UpdateRecord(TBL_LINK_FREQPLAN_LINK, iID, [db_par(FLD_ENABLED, 1)]);
//end
//else
//begin
//  dmLink.GetLinkEndID (aLinkID, iLinkEndID1,iLinkEndID2);
//  DoAddLink(aLinkFreqPlan_ID, aLinkID, iLinkEndID1, iLinkEndID2);
//
////    g_Log.AddRecord('', '' );
// //   g_Log.AddRecord('', Format('link-%d, %d, %d', [aLinkID, iLinkEndID1, iLinkEndID2]) );
//
//
//  DoGetLinkEnd_Neighbor_Links1(iLinkEndID1, oLinkIDList);
//  DoGetLinkEnd_Neighbor_Links1(iLinkEndID2, oLinkIDList);
//
//  for i := 0 to oLinkIDList.Count-1 do
//  begin
//    dmLink.GetLinkEndID (oLinkIDList[i].ID, iLinkEndID1,iLinkEndID2);
//
//    if gl_DB.GetIntFieldValue(TBL_LINKFREQPLAN_LINK, FLD_ID,
//               [db_par(FLD_LINKFREQPLAN_ID, aLinkFreqPlan_ID),
//                db_Par(FLD_LINK_ID,          oLinkIDList[i].ID)])=0
//    then
//   //   g_Log.AddRecord('', Format('link-%d, %d, %d', [oLinkIDList[i].ID, iLinkEndID1, iLinkEndID2]) );
//
//      DoAddLink(aLinkFreqPlan_ID, oLinkIDList[i].ID, iLinkEndID1, iLinkEndID2);
//  end;
//
//  oLinkIDList.Clear;
//end;
//oLinkIDList.Free;
//
//CursorDefault;
//*)
//end;



