inherited dlg_LinkFreqPlan_add: Tdlg_LinkFreqPlan_add
  Left = 731
  Top = 247
  Width = 487
  Height = 441
  Caption = 'dlg_LinkFreqPlan_add'
  PixelsPerInch = 96
  TextHeight = 13
  inherited pn_Buttons: TPanel
    Top = 378
    Width = 479
    inherited Bevel1: TBevel
      Width = 479
    end
    inherited Panel3: TPanel
      Left = 283
      inherited btn_Ok: TButton
        Left = 316
      end
      inherited btn_Cancel: TButton
        Left = 400
      end
    end
  end
  inherited pn_Top_: TPanel
    Width = 479
    inherited Bevel2: TBevel
      Width = 479
    end
    inherited pn_Header: TPanel
      Width = 479
      Height = 55
    end
  end
  inherited Panel1: TPanel
    Width = 479
    inherited ed_Name_: TEdit
      Width = 467
    end
  end
  inherited Panel2: TPanel
    Width = 479
    Height = 252
    inherited PageControl1: TPageControl
      Width = 469
      Height = 242
      inherited TabSheet1: TTabSheet
        object cxVerticalGrid1: TcxVerticalGrid
          Left = 0
          Top = 0
          Width = 461
          Height = 211
          BorderStyle = cxcbsNone
          Align = alClient
          LookAndFeel.Kind = lfStandard
          OptionsView.CellTextMaxLineCount = 3
          OptionsView.AutoScaleBands = False
          OptionsView.PaintStyle = psDelphi
          OptionsView.GridLineColor = clBtnShadow
          OptionsView.RowHeaderMinWidth = 30
          OptionsView.RowHeaderWidth = 306
          OptionsView.ValueWidth = 40
          TabOrder = 0
          Version = 1
          object row_max_distanse_km: TcxEditorRow
            Expanded = False
            Properties.Caption = #1052#1072#1082#1089#1080#1084#1072#1083#1100#1085#1072#1103' '#1076#1072#1083#1100#1085#1086#1089#1090#1100' '#1088#1072#1089#1095#1077#1090#1072' '#1089#1086#1089#1077#1076#1089#1090#1074#1072', ['#1082#1084']'
            Properties.EditPropertiesClassName = 'TcxCurrencyEditProperties'
            Properties.EditProperties.AssignedValues.DisplayFormat = True
            Properties.DataBinding.ValueType = 'Integer'
            Properties.Value = 100
            ID = 0
            ParentID = -1
            Index = 0
            Version = 1
          end
          object row_Min_CI_dB: TcxEditorRow
            Expanded = False
            Properties.Caption = 'Min '#1076#1086#1087#1091#1089#1090#1080#1084#1086#1077' '#1086#1090#1085#1086#1096#1077#1085#1080#1077' '#1089#1080#1075#1085#1072#1083'/'#1087#1086#1084#1077#1093#1072' (CI) [dB]'
            Properties.EditPropertiesClassName = 'TcxCurrencyEditProperties'
            Properties.EditProperties.AssignedValues.DisplayFormat = True
            Properties.DataBinding.ValueType = 'String'
            Properties.Value = '30'
            ID = 1
            ParentID = -1
            Index = 1
            Version = 1
          end
        end
      end
    end
  end
  inherited ActionList1: TActionList
    OnUpdate = ActionList1Update
    Left = 447
    object act_Add: TAction
      Caption = 'Add'
    end
  end
end
