object frame_LinkFreqPlan_Report: Tframe_LinkFreqPlan_Report
  Left = 863
  Top = 298
  Width = 829
  Height = 429
  Caption = 'frame_LinkFreqPlan_Report'
  Color = clBtnFace
  ParentFont = True
  OldCreateOrder = True
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object cxGrid1: TcxGrid
    Left = 0
    Top = 218
    Width = 813
    Height = 148
    Align = alBottom
    PopupMenu = PopupMenu
    TabOrder = 1
    LookAndFeel.Kind = lfFlat
    LookAndFeel.NativeStyle = False
    object cxGrid1DBTableView1: TcxGridDBTableView
      NavigatorButtons.ConfirmDelete = False
      DataController.DataSource = ds_Report
      DataController.Filter.MaxValueListCount = 1000
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      Filtering.ColumnPopup.MaxDropDownItemCount = 12
      OptionsCustomize.ColumnsQuickCustomization = True
      OptionsData.Editing = False
      OptionsSelection.HideFocusRectOnExit = False
      OptionsSelection.InvertSelect = False
      OptionsSelection.MultiSelect = True
      OptionsView.GroupByBox = False
      OptionsView.GroupFooters = gfVisibleWhenExpanded
      OptionsView.HeaderAutoHeight = True
      OptionsView.Indicator = True
      Preview.AutoHeight = False
      Preview.MaxLineCount = 2
      object col__Property_name: TcxGridDBColumn
        DataBinding.FieldName = 'Property_name'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = False
        HeaderAlignmentHorz = taCenter
        Options.Editing = False
        Options.Filtering = False
        Width = 80
      end
      object col__Link_name: TcxGridDBColumn
        DataBinding.FieldName = 'Link_name'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = False
        Options.Filtering = False
        Width = 81
      end
      object col__LinkEnd_Name: TcxGridDBColumn
        DataBinding.FieldName = 'LinkEnd_Name'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = False
        HeaderAlignmentHorz = taCenter
        Options.Editing = False
        Options.Filtering = False
        Width = 86
      end
      object col__Lon_str: TcxGridDBColumn
        DataBinding.FieldName = 'Lon_str'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = False
        HeaderAlignmentHorz = taCenter
        Options.Editing = False
        Options.Filtering = False
        Width = 102
      end
      object col__Lat_str: TcxGridDBColumn
        DataBinding.FieldName = 'Lat_str'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = False
        HeaderAlignmentHorz = taCenter
        Options.Editing = False
        Options.Filtering = False
        Width = 87
      end
      object col_Lat: TcxGridDBColumn
        DataBinding.FieldName = 'Lat'
        Options.Filtering = False
      end
      object col_Lon: TcxGridDBColumn
        DataBinding.FieldName = 'Lon'
        Options.Filtering = False
      end
      object col__band: TcxGridDBColumn
        DataBinding.FieldName = 'band'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = False
        HeaderAlignmentHorz = taCenter
        Options.Editing = False
        Options.Filtering = False
        Width = 46
      end
      object col__subband: TcxGridDBColumn
        DataBinding.FieldName = 'subband'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = False
        HeaderAlignmentHorz = taCenter
        Options.Editing = False
        Options.Filtering = False
        Width = 62
      end
      object col__tx_freq: TcxGridDBColumn
        DataBinding.FieldName = 'tx_freq_MHz'
        PropertiesClassName = 'TcxCurrencyEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.AssignedValues.MaxValue = True
        Properties.AssignedValues.MinValue = True
        Properties.DisplayFormat = ',0.00'
        Properties.Nullable = False
        Properties.ReadOnly = False
        HeaderAlignmentHorz = taCenter
        Options.Editing = False
        Options.Filtering = False
        Width = 83
      end
      object col__rx_freq: TcxGridDBColumn
        DataBinding.FieldName = 'rx_freq_MHz'
        PropertiesClassName = 'TcxCurrencyEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.AssignedValues.MaxValue = True
        Properties.AssignedValues.MinValue = True
        Properties.DisplayFormat = ',0.00'
        Properties.Nullable = False
        Properties.ReadOnly = False
        HeaderAlignmentHorz = taCenter
        Options.Editing = False
        Options.Filtering = False
        Width = 78
      end
      object col__channel_number: TcxGridDBColumn
        DataBinding.FieldName = 'channel_number'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = False
        HeaderAlignmentHorz = taCenter
        Options.Editing = False
        Options.Filtering = False
        Width = 97
      end
      object col__ch_type: TcxGridDBColumn
        DataBinding.FieldName = 'channel_type'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = False
        HeaderAlignmentHorz = taCenter
        Options.Editing = False
        Options.Filtering = False
        Width = 67
      end
      object col__Antenna_Type: TcxGridDBColumn
        DataBinding.FieldName = 'AntennaType.name'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = False
        HeaderAlignmentHorz = taCenter
        Options.Editing = False
        Options.Filtering = False
        Width = 83
      end
      object col__LinkEnd_Type: TcxGridDBColumn
        DataBinding.FieldName = 'LinkEndType_name'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = False
        HeaderAlignmentHorz = taCenter
        Options.Editing = False
        Options.Filtering = False
        Width = 96
      end
      object col__Height: TcxGridDBColumn
        DataBinding.FieldName = 'height'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = False
        HeaderAlignmentHorz = taCenter
        Options.Editing = False
        Options.Filtering = False
        Width = 49
      end
      object col__Azimuth: TcxGridDBColumn
        DataBinding.FieldName = 'azimuth'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = False
        HeaderAlignmentHorz = taCenter
        Options.Editing = False
        Options.Filtering = False
        Width = 47
      end
      object col__diameter: TcxGridDBColumn
        DataBinding.FieldName = 'diameter'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = False
        HeaderAlignmentHorz = taCenter
        Options.Editing = False
        Options.Filtering = False
        Width = 46
      end
      object col__Feeder_losses: TcxGridDBColumn
        DataBinding.FieldName = 'loss'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = False
        HeaderAlignmentHorz = taCenter
        Options.Editing = False
        Options.Filtering = False
        Width = 81
      end
      object col__Line_name: TcxGridDBColumn
        DataBinding.FieldName = 'Link_name'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = False
        Options.Filtering = False
        Width = 58
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1DBTableView1
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 366
    Width = 813
    Height = 24
    Align = alBottom
    BevelOuter = bvLowered
    TabOrder = 0
    object DBStatusLabel1: TDBStatusLabel
      Left = 6
      Top = 5
      Width = 85
      Height = 13
      DataSource = ds_Report
      Style = lsRecordNo
      CalcRecCount = True
      ShowOptions = doBoth
    end
  end
  object PopupMenu: TPopupMenu
    Left = 328
    Top = 58
    object actSaveToExcel1: TMenuItem
      Action = act_Save_To_Excel
    end
  end
  object SaveDialog1: TSaveDialog
    Filter = '*.xls|*.xls'
    Options = [ofOverwritePrompt, ofHideReadOnly, ofEnableSizing]
    Left = 268
    Top = 58
  end
  object ActionList11: TActionList
    Left = 44
    Top = 44
    object act_Save_To_Excel: TAction
      Caption = 'act_Save_To_Excel'
      OnExecute = act_Save_To_ExcelExecute
    end
  end
  object qry_Report: TADOQuery
    Connection = ADOConnection1
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'SELECT * FROM  VIEW_LINKFREQPLAN_REPORt')
    Left = 148
    Top = 56
  end
  object ds_Report: TDataSource
    DataSet = qry_Report
    Left = 149
    Top = 104
  end
  object ADOConnection1: TADOConnection
    ConnectionString = 
      'Provider=MSDASQL.1;Persist Security Info=False;User ID=sa;Data S' +
      'ource=onega_link;'
    LoginPrompt = False
    Provider = 'MSDASQL.1'
    Left = 40
    Top = 112
  end
  object dxBarManager1: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = []
    Categories.Strings = (
      'Default')
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    PopupMenuLinks = <>
    SunkenBorder = True
    UseSystemFont = True
    Left = 504
    Top = 80
    DockControlHeights = (
      0
      0
      0
      0)
    object dxBarManager1Bar1: TdxBar
      AllowQuickCustomizing = False
      Caption = 'Custom 1'
      CaptionButtons = <>
      DockedDockingStyle = dsTop
      DockedLeft = 0
      DockedTop = 0
      DockingStyle = dsTop
      FloatLeft = 1599
      FloatTop = 937
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarButton1'
        end>
      OneOnRow = True
      Row = 0
      UseOwnFont = False
      Visible = False
      WholeRow = False
    end
    object dxBarButton1: TdxBarButton
      Action = act_Save_To_Excel
      Category = 0
    end
  end
end
