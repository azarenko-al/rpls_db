unit dm_LinkFreqPlan_view;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, RxMemDS, ADODB, DBTables,

  u_LinkFreqPlan_const,

  dm_LinkFreqPlan_LinkEnd_View,
  dm_Main,

  u_func,
  u_const_db,
  u_db,

   dxmdaset
  ;

type
  TdmLinkFreqPlan_view = class(TDataModule)
    qry_Temp: TADOQuery;
    mem_Temp: TdxMemData;
    procedure DataModuleCreate(Sender: TObject);

  private
    procedure OpenFreqReport1(aLinkFreqPlan_ID: integer; aDataSet: TDataSet);
    procedure MemFieldsCreate1(aDataset: TDataset);

  public

  end;



function dmLinkFreqPlan_view: TdmLinkFreqPlan_view;


//==================================================================
implementation {$R *.dfm}
//==================================================================

var
  FdmLinkFreqPlan_view: TdmLinkFreqPlan_view;


// ---------------------------------------------------------------
function dmLinkFreqPlan_view: TdmLinkFreqPlan_view;
// ---------------------------------------------------------------
begin
  if not Assigned(FdmLinkFreqPlan_view) then
      FdmLinkFreqPlan_view := TdmLinkFreqPlan_view.Create(Application);

   Result := FdmLinkFreqPlan_view;
end;


//--------------------------------------------------------------------
procedure TdmLinkFreqPlan_view.DataModuleCreate(Sender: TObject);
//--------------------------------------------------------------------
begin
  db_SetComponentADOConnection(Self, dmMain.AdoConnection);

end;

//--------------------------------------------------------------------
procedure TdmLinkFreqPlan_view.OpenFreqReport1(aLinkFreqPlan_ID: integer;
    aDataSet: TDataSet);
//--------------------------------------------------------------------
var iNumChannel, i, m: integer;
    dChannelWidth, dFirstChannelFreq, dDuplexFreqDelta: double;
    arrDistrChannel: TIntArray;
    arrDistrFreq: TDoubleArray;
begin
  aDataset.Close;
  aDataset.Open;

  dmOnega_db_data.OpenQuery (qry_Temp, SQL_SELECT_FREQREPORT,
                            [FLD_LINKFREQPLAN_ID, aLinkFreqPlan_ID]);

  db_OpenQuery (gl_DB.Query, TBL_LINK_FREQPLAN, FLD_ID, aLinkFreqPlan_ID);

  with qry_Temp do while not Eof do
  begin
    dChannelWidth    := gl_DB.Query.FieldByName(FLD_ChannelWidth).AsFloat;
    dFirstChannelFreq:= gl_DB.Query.FieldByName(FLD_FirstChannelFreq).AsFloat;
    dDuplexFreqDelta := gl_DB.Query.FieldByName(FLD_DuplexFreqDelta).AsFloat;
    m:= Round(dDuplexFreqDelta/dChannelWidth);

    SetArrayLength (arrDistrFreq, 0);
//    ClearArray(arrDistrChannel);
    arrDistrChannel:= StringToIntArray(FieldByName(FLD_FREQ_DISTRIBUTED).AsString);
    for i := 0 to Length(arrDistrChannel)-1 do
    begin
      iNumChannel:= arrDistrChannel[i];
      if iNumChannel-1 < m then
        DoubleArrayAddValue(arrDistrFreq, dFirstChannelFreq +
                                             dChannelWidth*(iNumChannel-1))
      else
        DoubleArrayAddValue(arrDistrFreq, dFirstChannelFreq + dDuplexFreqDelta +
                              dChannelWidth*(iNumChannel-1-m));

    end;

    aDataSet.Append;
    db_SetFieldsValues (qry_Temp, aDataset);
    aDataSet.FieldValues[FLD_AZIMUTH]:=
                      TruncFloat(qry_Temp.FieldByName(FLD_AZIMUTH).AsFloat,1);
    aDataSet.Post;
    
    db_UpdateRecord(aDataSet, [db_par(FLD_FREQ_DISTRIBUTED,
                                       DoubleArrayToString(arrDistrFreq))]);
    Next;
  end;

end;                                                                   

//-------------------------------------------------------------------
procedure TdmLinkFreqPlan_view.MemFieldsCreate1(aDataset: TDataset);
//-------------------------------------------------------------------
begin
  db_CreateField(aDataset,
    [
     db_Field(FLD_NUMBER,             ftInteger),
     db_Field(FLD_LinkEnd_name,       ftString),
     db_Field(FLD_LINKEND_ID,         ftInteger),
     db_Field(FLD_LAT,                ftFloat),
     db_Field(FLD_LON,                ftFloat),
     db_Field(FLD_FREQ_REQ_COUNT,     ftInteger),
     db_Field(FLD_FREQ_DISTRIBUTED,   ftString),
     db_Field(FLD_CHANNEL_NUMBER,     ftInteger),
     db_Field(FLD_PROPERTY_NAME,      ftString),
     db_Field(FLD_LinkEnd_Type_Name,  ftString),
     db_Field(FLD_antennatype_name,   ftString),
     db_Field(FLD_HEIGHT,             ftFloat),
     db_Field(FLD_AZIMUTH,            ftFloat),
     db_Field(FLD_DIAMETER,           ftFloat),
     db_Field(FLD_LOSS,               ftFloat)
     ]);
end;



begin 
end.
