unit fr_LinkFreqPlan_Report;

interface

uses
  Classes, Controls, Forms,
  Dialogs, Menus,
  DB, ADODB,  ActnList,
  cxGridLevel, cxGrid,
  cxGridCustomTableView,
  cxGridDBTableView, cxGridCustomView,


  u_func,

  u_cx,

  u_Storage,

 // dm_Language,

//  dm_Main,

  dm_Onega_DB_data,

//  f_Custom,

 //, cxGridTableView, cxClasses, cxControls, cxPropertiesStore, ToolWin,
  //ComCtrls, cxGridTableView, cxClasses, cxControls, cxPropertiesStore,
  //ToolWin dm_LinkFreqPlan_Report,

  u_const_db,
  
  u_db,
  
  
  
  
  RXDBCtrl,
  ExtCtrls, RXCtrls, cxGridTableView, cxClasses, cxControls,
  cxPropertiesStore, ToolWin, ComCtrls, f_Custom, cxGraphics,
  cxLookAndFeels, cxLookAndFeelPainters, cxStyles, dxSkinsCore,
  dxSkinsDefaultPainters, dxSkinscxPCPainter, cxCustomData, cxFilter,
  cxData, cxDataStorage, cxEdit, cxDBData, cxTextEdit, cxCurrencyEdit,
  dxBar

  ;


type
//  Tframe_LinkFreqPlan_Report = class(Tfrm_Custom)
  Tframe_LinkFreqPlan_Report = class(TForm)
    PopupMenu: TPopupMenu;
    SaveDialog1: TSaveDialog;
    ActionList11: TActionList;
    act_Save_To_Excel: TAction;
    cxGrid1: TcxGrid;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1DBTableView1: TcxGridDBTableView;
    col__Property_name: TcxGridDBColumn;
    col__Link_name: TcxGridDBColumn;
    col__LinkEnd_Name: TcxGridDBColumn;
    col__Lon_str: TcxGridDBColumn;
    col__Lat_str: TcxGridDBColumn;
    col__band: TcxGridDBColumn;
    col__subband: TcxGridDBColumn;
    col__tx_freq: TcxGridDBColumn;
    col__rx_freq: TcxGridDBColumn;
    col__channel_number: TcxGridDBColumn;
    col__ch_type: TcxGridDBColumn;
    col__Antenna_Type: TcxGridDBColumn;
    col__LinkEnd_Type: TcxGridDBColumn;
    col__Height: TcxGridDBColumn;
    col__Azimuth: TcxGridDBColumn;
    col__diameter: TcxGridDBColumn;
    col__Feeder_losses: TcxGridDBColumn;
    col__Line_name: TcxGridDBColumn;
    qry_Report: TADOQuery;
    ds_Report: TDataSource;
    ADOConnection1: TADOConnection;
    Panel1: TPanel;
    DBStatusLabel1: TDBStatusLabel;
    col_Lon: TcxGridDBColumn;
    col_Lat: TcxGridDBColumn;
    actSaveToExcel1: TMenuItem;
    dxBarManager1: TdxBarManager;
    dxBarManager1Bar1: TdxBar;
    dxBarButton1: TdxBarButton;

    procedure act_Save_To_ExcelExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);

    procedure FormDestroy(Sender: TObject);
  private
 //   FID: integer;

  public
    procedure View(aLinkFreqPlan_ID: integer);
  end;


//===========================================================================//
implementation {$R *.dfm}
//===========================================================================//

const
//  COLOR_ODD_RECORD  = clWindow;
//  COLOR_EVEN_RECORD = clInfoBk;

//  CAPTION_Number           = '�';
  CAPTION_LinkEnd_Name     = '���';
  CAPTION_Coord_N          = '������ [����42]';
  CAPTION_Coord_E          = '������� [����42]';
  CAPTION_TX_FREQ          = '������� ��� [MHz]';
  CAPTION_RX_FREQ          = '������� ��� [MHz]';
  CAPTION_CHANNEL_Number   = '����� ������';
  CAPTION_Link_Name        = '��� ���';
  CAPTION_Property_Name    = '��� ��������';
  CAPTION_LinkEnd_Type     = '��� ���';
  CAPTION_Antenna_Type     = '��� �������';
  CAPTION_Height           = '������ ������� [m]';
  CAPTION_Azimuth          = '������ [����]';
  CAPTION_Feeder_losses    = '��������� ������ [dB]';
  CAPTION_Diameter         = '������� ������� [m]';

  CAPTION_RANGE            = '�������� [GHz]';
  CAPTION_SUBBAND          = '�����������';
  CAPTION_CH_TYPE          = '��� ������';
  CAPTION_LINE_NAME        = '�����';


  FILTER_EXCEL = 'XLS (*.xls)|*.xls';

// ---------------------------------------------------------------
procedure Tframe_LinkFreqPlan_Report.act_Save_To_ExcelExecute(Sender: TObject);
// ---------------------------------------------------------------
begin
  if Sender=act_Save_To_Excel then
  begin
    if SaveDialog1.Execute then
    begin
      cx_ExportGridToExcel(SaveDialog1.FileName, cxGrid1);

  //    ExportGrid4ToExcel(SaveDialog1.FileName, cxGrid1);

      ShellExec(SaveDialog1.FileName);
    end;
  end;

end;

//--------------------------------------------------------------------------
procedure Tframe_LinkFreqPlan_Report.FormCreate(Sender: TObject);
//--------------------------------------------------------------------------
begin
  Assert(not ADOConnection1.Connected);

  inherited;

//  dmLinkFreqPlan_Report.InitDB (mem_Report);

  act_Save_To_Excel.Caption:= '��������� � Excel';
//  act_Setup_Grid.Caption   := '��������� �������';

//  DBGrid.Align:=alClient;

  col_Lat.Caption:='������';
  col_Lon.Caption:='�������';


 // col__N.Caption                 := CAPTION_Number;
  col__Lon_str.Caption           := CAPTION_Coord_N;
  col__Lat_str.Caption           := CAPTION_Coord_E;

  col_Lon.Caption           := CAPTION_Coord_N;
  col_Lat.Caption           := CAPTION_Coord_E;

  col__LinkEnd_Name.Caption      := CAPTION_LinkEnd_Name;
  col__Link_Name.Caption         := CAPTION_Link_Name;
  col__tx_freq.Caption           := CAPTION_TX_FREQ;
  col__rx_freq.Caption           := CAPTION_RX_FREQ;
  col__channel_number.Caption    := CAPTION_CHANNEL_Number;
  col__Property_name.Caption     := CAPTION_Property_Name;
  col__LinkEnd_Type.Caption      := CAPTION_LinkEnd_Type;
  col__Antenna_Type.Caption      := CAPTION_Antenna_Type;
  col__Height.Caption            := CAPTION_Height;
  col__Azimuth.Caption           := CAPTION_Azimuth;
  col__Feeder_losses.Caption     := CAPTION_Feeder_losses;
  col__diameter.Caption          := CAPTION_Diameter;
  col__band.Caption              := CAPTION_RANGE;
  col__subband.Caption           := CAPTION_SUBBAND;
  col__ch_type.Caption           := CAPTION_CH_TYPE;
  col__Line_name.Caption         := CAPTION_LINE_NAME;



  g_Storage.RestoreFromRegistry (cxGrid1DBTableView1, ClassName+'_default1');

//  g_Storage.RestoreFromRegistry (cxGrid1DBTableView1, ClassName);



//  cxGrid1DBTableView1.RestoreFromRegistry(FRegPath + cxGrid1DBTableView1.Name+'2');

  cxGrid1.Align:=alClient;


//  TdmLanguage.CheckForm(Self);


  col__Lon_str.Visible := False;
  col__Lat_str.Visible := False;



{
  if dx_CheckRegistry(DBGrid, FRegPath+ DBGrid.Name) then
    DBGrid.LoadFromRegistry (FRegPath+ DBGrid.Name)
  else
    DBGrid.ApplyBestFit(nil);
}

 // SetActionsExecuteProc ([act_Save_To_Excel, act_Setup_Grid], DoAction);
end;

//--------------------------------------------------------------------------
procedure Tframe_LinkFreqPlan_Report.FormDestroy(Sender: TObject);
//--------------------------------------------------------------------------
begin
//  cxGrid1DBTableView1.StoreToRegistry(FRegPath + cxGrid1DBTableView1.Name+'2');
  g_Storage.StoreToRegistry (cxGrid1DBTableView1, ClassName);

  inherited;
end;

//--------------------------------------------------------------------------
procedure Tframe_LinkFreqPlan_Report.View(aLinkFreqPlan_ID: integer);
//----------------------------------------------------------------
const
  SQL_SELECT_FREQREPORT =
      'SELECT * FROM ' + VIEW_LINKFREQPLAN_REPORT +
      ' WHERE LinkFreqPlan_id=:LinkFreqPlan_id';

begin
 // FID := aLinkFreqPlan_ID;

  dmOnega_DB_data.OpenQuery (qry_Report,
              SQL_SELECT_FREQREPORT,
              [FLD_LINKFREQPLAN_ID, aLinkFreqPlan_ID]);

  //db_View(qry_Report);

end;


end.