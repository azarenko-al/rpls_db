unit dm_LinkFreqPlan_Tools;

interface

uses
  SysUtils, Classes, Controls, Forms, Dialogs, Db, ADODB, IniFiles,


  u_Ini_LinkFreqPlan_CIA_params,

  u_LinkFreqPlan_run,

  dm_Onega_DB_data,
  u_vars,

  dm_Main,

  dm_LinkFreqPLan,

  dm_Link,
  dm_LinkEnd_ex,

  u_Func_arrays,
  u_func,


  u_files,
  u_db,


  u_LinkFreqPlan_const,
  u_const,
  u_const_db,
  

  u_types,

  dm_Custom
  ;

type
  TdmLinkFreqPlan_Tools = class(TdmCustom)
    qry_FreqPlan_Link: TADOQuery;
    qry_Band: TADOQuery;
    qry_Temp: TADOQuery;
    procedure DataModuleCreate(Sender: TObject);
//    procedure DataModuleCreate(Sender: TObject);
   // procedure DataModuleDestroy(Sender: TObject);
  private
    FForbidArr: TIntArray;


    procedure ForEachProc_Set_Freq_count_by_Fixed(aDataset: TDataSet; var
        aTerminated: boolean);
  public
    procedure RunCalc_FREQPLAN_NB(aLinkFreqPlan_ID: integer; aIsFullCalc: boolean);

    function Allow_Freqs(aDataset: TDataset; aLinkFreqPlan_ID: integer): Boolean;
    procedure Exec_Calc(aLinkFreqPlan_ID: integer);
    procedure Calc_Interference(aLinkFreqPlan_ID: integer; aUseFixedChannels: boolean);
    procedure Clear_Interference(aLinkFreqPlan_ID: integer);
    function Forbid_Freqs(aDataset: TDataset; aLinkFreqPlan_ID: integer): Boolean;
    function Restore_Fixed(aLinkFreqPlan_ID: integer): Boolean;
    function Set_Common_requirements(aLinkFreqPlan_ID: integer): Boolean;
    function Set_Priority(aDataset: TDataset): Boolean;

    procedure Run_Distribute(aLinkFreqPlan_ID: Integer);
    function Set_Fixed(aLinkFreqPlan_ID: integer): Boolean;

    function Clear_Fixed(aLinkFreqPlan_ID: integer; aIsClearInterference: boolean):
        Boolean;
    function Distributed_Clear(aLinkFreqPlan_ID: integer; aIsClearInterference:
        boolean): Boolean;

    function Fixed_Save(aLinkFreqPlan_ID: integer): Boolean;

    procedure CreateMap_Interference(aLinkFreqPlan_ID: integer);
    function Set_Freq_count_by_Fixed(aDataset: TDataset): Boolean;

  end;

function dmLinkFreqPlan_Tools: TdmLinkFreqPlan_Tools;


//=======================================================
implementation

uses dm_LinkFreqPlan_LinkEnd_view, u_LinkFreqPlan_classes;{$R *.DFM}


var
  FdmLinkFreqPlan_Tools: TdmLinkFreqPlan_Tools;


// ---------------------------------------------------------------
function dmLinkFreqPlan_Tools: TdmLinkFreqPlan_Tools;
// ---------------------------------------------------------------
begin
  if not Assigned(FdmLinkFreqPlan_Tools) then
    FdmLinkFreqPlan_Tools := TdmLinkFreqPlan_Tools.Create(Application);

  Result := FdmLinkFreqPlan_Tools;
end;


procedure TdmLinkFreqPlan_Tools.DataModuleCreate(Sender: TObject);
begin
  inherited;
end;

//--------------------------------------------------------------------
function TdmLinkFreqPlan_Tools.Set_Common_requirements(aLinkFreqPlan_ID:
    integer): Boolean;
//--------------------------------------------------------------------
var
  k: Integer;
  sValue: string;
begin
  sValue:='1';

  Result:= InputQuery('���������� �������','������� ���������� ��������� ������', sValue);

  if Result then
    k:=dmOnega_DB_data.LinkFreqPlan_Tools(aLinkFreqPlan_ID,
      DEF_Set_Common_requirements, AsInteger(sValue));

 {
  Result:=False;

  sValue:='1';
  if not InputQuery('���������� �������','������� ���������� ��������� ������', sValue)
    then Exit;
                         //freq_req_count
  k:=dmOnega_DB_data.LinkFreqPlan_Tools(aLinkFreqPlan_ID,
      DEF_Set_Common_requirements, AsInteger(sValue));

  Result:=True;
  }
end;



// ---------------------------------------------------------------
procedure TdmLinkFreqPlan_Tools.ForEachProc_Set_Freq_count_by_Fixed(aDataset:
    TDataSet; var aTerminated: boolean);
// ---------------------------------------------------------------
var
  iCount: Integer;
begin
   iCount:=aDataset.FieldByName(FLD_Freq_FIXED_Count).AsInteger;

   db_UpdateRecord(aDataset, [db_par(FLD_freq_req_count, iCount)]);
end;


//--------------------------------------------------------------------
function TdmLinkFreqPlan_Tools.Set_Freq_count_by_Fixed(aDataset: TDataset):
    Boolean;
//--------------------------------------------------------------------
var arrFixed: TIntArray;
  iCount: Integer;
begin
//  Result:=False;

  Result:=ConfirmDlg('���������� ��������� ���������� ������ �� ������������?');

//  dmOnega_DB_data.LinkFreqPlan_Tools(aLinkFreqPlan_ID,'Clear_Interference');

//procedure db_UpdateAllRecords (aDataset: TDataset; aParams: array of TDBParamRec);

  if Result then
    db_ForEach (aDataset, ForEachProc_Set_Freq_count_by_Fixed);
   {
    with aDataset do
    begin
      First;
      while not Eof do
      begin
       // arrFixed:=StringToIntArray(FieldByName(FLD_FREQ_FIXED).AsString);

        iCount:=FieldByName(FLD_Freq_FIXED_Count).AsInteger;


  //        db_CreateCalculatedField(aDataset, FLD_Freq_Distributed_Count, ftInteger);
   // db_CreateCalculatedField(aDataset, FLD_FREQ_FIXED_COUNT, ftInteger);


        db_UpdateRecord(aDataset, [db_par(FLD_freq_req_count, iCount)]);
        Next;
      end;
    end;
    }

 // Result:=True;
end;

//--------------------------------------------------------------------
function TdmLinkFreqPlan_Tools.Forbid_Freqs(aDataset: TDataset;
    aLinkFreqPlan_ID: integer): Boolean;
//--------------------------------------------------------------------
 {
    procedure ForEachProc (aDataset: TDataSet; var aTerminated: boolean);
    var
      intArr, arrForbid: TIntArray;
      s: string;
      sValue: string;

    begin
      arrForbid:=StringToIntArray(aDataset.FieldByName(FLD_FREQ_FORBIDDEN).AsString);
      IntArrayJoinValues(arrForbid, intArr);

      s:= IntArrayToString(arrForbid);

      db_UpdateRecord(aDataset, [db_par(FLD_FREQ_FORBIDDEN, s)]);


    end;
   }

{
type
  TForEachProc = procedure(aDataset: TDataSet; var aTerminated: boolean) of object;

  function db_ForEach(aDataset: TDataset; aProc: TForEachProc): boolean;
}


var intArr, arrForbid: TIntArray;
  s: string;
    sValue: string;
begin
//  result:=False;

  sValue:='';

  result:=InputQuery('��������� ������� ��� ���� �� ���',
                     '������� ����������� ������� (����: 3;5;9)', sValue);


//  if (not InputQuery('��������� ������� ��� ���� �� ���',
//                     '������� ����������� ������� (����: 3;5;9)', sValue))
//    then Exit;

  if Result then
  begin
      intArr  := StringToIntArray (sValue);


     // db_ForEach (aDataset, ForEachProc);

      aDataset.First;
      with aDataset do
        while not Eof do
      begin
          arrForbid:=StringToIntArray(FieldByName(FLD_FREQ_FORBIDDEN).AsString);
          IntArrayJoinValues(arrForbid, intArr);

          s:= IntArrayToString(arrForbid);

          db_UpdateRecord(aDataset, [db_par(FLD_FREQ_FORBIDDEN, s)]);
    //                                        IntArrayToString(arrForbid))]);
          Next;
      end;

  end;

//  result:=True;


end;

//--------------------------------------------------------------------
function TdmLinkFreqPlan_Tools.Allow_Freqs(aDataset: TDataset;
    aLinkFreqPlan_ID: integer): Boolean;
//--------------------------------------------------------------------
var sValue: string;
    intArr, arrForbid: TIntArray;
begin
  result:=False;

  sValue:='';
  if (not InputQuery('��������� ������� ��� ���� �� ���',
                     '������� ����������� ������� (����: 3;5;9)', sValue))
    then Exit;

  intArr:=StringToIntArray (sValue);

  aDataset.First;

  with aDataset do
    while not Eof do
  begin
    arrForbid:=StringToIntArray(FieldByName(FLD_FREQ_FORBIDDEN).AsString);
    IntArrayRemoveValues(arrForbid, intArr);

   //  s:= IntArrayToString(arrForbid));

    db_UpdateRecord(aDataset, [db_par(FLD_FREQ_FORBIDDEN,
                                        IntArrayToString(arrForbid))]);
    Next;
  end;

 result:=True;
end;

//--------------------------------------------------------------------
function TdmLinkFreqPlan_Tools.Set_Priority(aDataset: TDataset): Boolean;
//--------------------------------------------------------------------
var sValue: string;
begin
  sValue := '1';

  result:=InputQuery ('��������� ��������', '������� ��������� ��������', sValue);

 // result:=False;

 // sValue := '1';
 // if not InputQuery ('��������� ��������', '������� ��������� ��������', sValue) then
  //  Exit;

  if result then
    dmLinkFreqPlan.UpdateLinkEnd111(aDataset[FLD_ID],
                          [db_Par(FLD_Priority, AsInteger(sValue))]);
 // result:=True;
end;

//-------------------------------------------------------------------------
procedure TdmLinkFreqPlan_Tools.Clear_Interference(aLinkFreqPlan_ID: integer);
//--------------------------------------------------------------------
begin
  dmOnega_DB_data.LinkFreqPlan_Tools(aLinkFreqPlan_ID, DEF_Clear_Interference);
end;


//--------------------------------------------------------------------
function TdmLinkFreqPlan_Tools.Set_Fixed(aLinkFreqPlan_ID: integer): Boolean;
//--------------------------------------------------------------------
begin
  result:=ConfirmDlg('��������� �������?');

  if result then
    dmOnega_DB_data.LinkFreqPlan_Tools(aLinkFreqPlan_ID, DEF_Set_Fixed);
end;



//--------------------------------------------------------------------
function TdmLinkFreqPlan_Tools.Restore_Fixed(aLinkFreqPlan_ID: integer):
    Boolean;
//--------------------------------------------------------------------
var
  iCh: Integer;
begin
  result:=  ConfirmDlg('������������ �������?') ;

  if result then
  begin
  //  db_ForEach (aDataset, ForEachProc_Set_Freq_count_by_Fixed);
//    db_UpdateAllRecords_();

{
      aDataset.First;

      with aDataset do
        while not Eof do             //FREQ_FIXED
      begin
        iCh:= FieldByName(FLD_CHANNEL_NUMBER).AsInteger;

        db_UpdateRecord(aDataset, [db_par(FLD_FREQ_FIXED,  iCh ) ]);
        Next;
      end;

 }

   dmOnega_DB_data.LinkFreqPlan_Tools(aLinkFreqPlan_ID, DEF_Restore_Fixed);

//    dmOnega_DB_data.LinkFreqPlan_Restore_Fixed(aLinkFreqPlan_ID);

(*
    gl_DB.ExecCommand(
      'UPDATE ' + TBL_LINKFREQPLAN_LINKEND +
      ' SET LinkFreqPlan_LinkEnd.freq_fixed=LinkEnd.channel_number, '+
      '     LinkFreqPlan_LinkEnd.freq_fixed_tx=LinkEnd.tx_freq_MHz, '+
      '     LinkFreqPlan_LinkEnd.freq_fixed_rx=LinkEnd.rx_freq_MHz, '+
      '     LinkFreqPlan_LinkEnd.ch_type_fixed=LinkEnd.channel_number_type '+
      ' FROM LinkEnd RIGHT OUTER JOIN '+
      '      LinkFreqPlan_LinkEnd ON LinkEnd.id = LinkFreqPlan_LinkEnd.linkend_id '+
      ' WHERE (LinkFreqPlan_LinkEnd.LinkFreqPlan_id=:LinkFreqPlan_id)',
                      [db_par(FLD_LINKFREQPLAN_ID, aLinkFreqPlan_ID)]);
*)
  end;
end;

//--------------------------------------------------------------------
function TdmLinkFreqPlan_Tools.Distributed_Clear(aLinkFreqPlan_ID: integer;
    aIsClearInterference: boolean): Boolean;
//--------------------------------------------------------------------
begin
   result:=ConfirmDlg('�������� ��������������?');

  if result then
  begin
//    dmOnega_DB_data.LinkFreqPlan_Clear_Distribute(aLinkFreqPlan_ID);
    dmOnega_DB_data.LinkFreqPlan_Tools(aLinkFreqPlan_ID, DEF_Clear_Distribute);


(*    gl_DB.ExecCommand('UPDATE ' + TBL_LINKFREQPLAN_LINKEND +
                    ' SET freq_distributed=null, ' +
                    '     freq_distributed_tx=null, '+
                    '     freq_distributed_rx=null, '+
                    '     ch_type_distributed=null '+
                    ' WHERE LinkFreqPlan_id=:LinkFreqPlan_id',
                    [db_par(FLD_LINKFREQPLAN_ID, aLinkFreqPlan_ID)]);

*)
    if aIsClearInterference then
      dmOnega_DB_data.LinkFreqPlan_TOOLS(aLinkFreqPlan_ID, DEF_Clear_Interference);

//      dmOnega_DB_data.LinkFreqPlan_Clear_Interference(aLinkFreqPlan_ID);

    //  Clear_Interference(aLinkFreqPlan_ID);
  end;
end;

//--------------------------------------------------------------------
function TdmLinkFreqPlan_Tools.Clear_Fixed(aLinkFreqPlan_ID: integer;
    aIsClearInterference: boolean): Boolean;
//--------------------------------------------------------------------
begin
  result:= ConfirmDlg('�������� ������������?');

  if result then
  begin
    dmOnega_DB_data.LinkFreqPlan_Tools(aLinkFreqPlan_ID, DEF_Clear_Fixed);
//    dmOnega_DB_data.LinkFreqPlan_Clear_Fixed(aLinkFreqPlan_ID);


(*

    gl_DB.ExecCommand('UPDATE ' + TBL_LINKFREQPLAN_LINKEND +
                    ' SET freq_fixed=null, ' +
                    '     freq_fixed_tx=null, '+
                    '     freq_fixed_rx=null, '+
                    '     ch_type_fixed=null '+
                    ' WHERE LinkFreqPlan_id=:LinkFreqPlan_id',
                    [db_par(FLD_LINKFREQPLAN_ID, aLinkFreqPlan_ID)]);
*)
    if aIsClearInterference then
      dmOnega_DB_data.LinkFreqPlan_Tools(aLinkFreqPlan_ID, DEF_Clear_Interference);
//      dmOnega_DB_data.LinkFreqPlan_Clear_Interference(aLinkFreqPlan_ID);

//      Clear_Interference(aLinkFreqPlan_ID);
  end;
end;

//--------------------------------------------------------------------
function TdmLinkFreqPlan_Tools.Fixed_Save(aLinkFreqPlan_ID: integer): Boolean;
//--------------------------------------------------------------------
//
//
//const
//
//  SQL_SELECT_LINK_FREQPLAN_LINK =
//        'SELECT * FROM '+ TBL_LINKFREQPLAN_LINK +
//        ' WHERE  (LinkFreqPlan_id = :LinkFreqPlan_id) ';

var
  oLinkend: TnbLinkFreqPlan_Linkend;

  arr: TIntArray;

  eTxFreq, eRxFreq: double;
  i,iChannel: Integer;
  s: string;

begin
  Result:=ConfirmDlg('��������� ������������ �������?');

  if not Result then
    Exit;


   for i:=0 to dmLinkFreqPlan_LinkEnd_View.Data.Linkends.Count-1 do
   begin
     oLinkend:=dmLinkFreqPlan_LinkEnd_View.Data.Linkends[i];

     s:=oLinkend.FREQ_FIXED;

     arr:=StringToIntArray(oLinkend.FREQ_FIXED);

     if Length(arr)>0 then
     begin
       iChannel:=arr[0];

       oLinkend.ChannelNumberToFreqMHz(iChannel,  eTxFreq, eRxFreq );


       dmOnega_DB_data.UpdateRecordByID(TBL_LINKEND, oLinkend.Linkend_id,

   //    gl_db.UpdateRecord(TBL_LINKEND,   oLinkend.Linkend_id,
                [db_par(FLD_TX_FREQ_MHz,  eTxFreq),
                 db_par(FLD_RX_FREQ_MHz,  eRxFreq),
           //      db_par(FLD_CHANNEL_TYPE, sChTypeFixed),
               //  db_par(FLD_CHANNEL_NUMBER_TYPE, iChTypeFixed),
                 db_par(FLD_CHANNEL_NUMBER,      iChannel)
                 ]);

     end;


   end;

//
//   exit;
//
//  { TODO : ����������!!!!!!!!!!!!!!!!!! }
//
//  db_OpenQuery (qry_FreqPlan_Link, SQL_SELECT_LINK_FREQPLAN_LINK,
//                     [db_Par(FLD_LINKFREQPLAN_ID, aLinkFreqPlan_ID)]);
//
//  with qry_FreqPlan_Link do
//    while not Eof do
//  begin
//    dmLink.GetLinkEndID (qry_FreqPlan_Link[FLD_Link_ID], iLinkEndID1, iLinkEndID2);
//
//    dmLinkEnd_ex.GetBand_Params (iLinkEndID1,
//                dDuplexFreqDelta, dChannelWidth,
//                dFirstChannelFreq, iChCount, OBJ_LINKEND);
//
//  { TODO : m:= Round( FR / dF )-1 ???
//    m:= Round(dDuplexFreqDelta/dChannelWidth)-1;   }
//
//    DoGet_LinkEnd_ChParams (iLinkEndID1, iChNum, sChTypeFixed, dTxFreq, dRxFreq);
////    DoGet_LinkEnd_ChParams (iLinkEndID1, iChNum, iChTypeFixed, dTxFreq, dRxFreq);
//
//    gl_db.UpdateRecord(TBL_LINKEND,   iLinkEndID1,
//            [db_par(FLD_TX_FREQ_MHz,  dTxFreq),
//             db_par(FLD_RX_FREQ_MHz,  dRxFreq),
//       //      db_par(FLD_CHANNEL_TYPE, sChTypeFixed),
//           //  db_par(FLD_CHANNEL_NUMBER_TYPE, iChTypeFixed),
//             db_par(FLD_CHANNEL_NUMBER,      iChNum)]);
//
//    gl_db.UpdateRecord(TBL_LINKEND,  iLinkEndID2,
//          [db_par(FLD_TX_FREQ_MHz,   dRxFreq),
//           db_par(FLD_RX_FREQ_MHz,   dTxFreq),
//      //     db_par(FLD_CHANNEL_TYPE,  sChTypeFixed),
////           db_par(FLD_CHANNEL_NUMBER_TYPE, IIF(iChTypeFixed=0, 1, 0)),
//           db_par(FLD_CHANNEL_NUMBER, iChNum)]);
//    Next;
//  end;
end;

//--------------------------------------------------------------------
procedure TdmLinkFreqPlan_Tools.RunCalc_FREQPLAN_NB(aLinkFreqPlan_ID: integer;
    aIsFullCalc: boolean);
//--------------------------------------------------------------------
begin
  TLinkFreqPlan_run.RunCalc_FREQPLAN_NB_calc(aLinkFreqPlan_ID, aIsFullCalc);
end;


//--------------------------------------------------------------------
procedure TdmLinkFreqPlan_Tools.CreateMap_Interference(aLinkFreqPlan_ID: integer);
//--------------------------------------------------------------------
begin
  TLinkFreqPlan_run.CreateMap_Interference(aLinkFreqPlan_ID);



 // sIniFile := g_ApplicationDataDir + TEMP_FILENAME;
//
//  DeleteFile(sIniFile);
//
//  oInifile:=TIniFile.Create (sIniFile);
//  oInifile.WriteInteger ('main', 'LinkFreqPlanID',  aLinkFreqPlan_ID);
//  oInifile.WriteInteger ('main', 'ProjectID',       dmMain.ProjectID);
//
//  FreeAndNil(oInifile);
//
////  Assert(Assigned(IMapAct));
//
//
////zzzzzzzzzzzz  dmAct_Map.MAP_SAVE_AND_CLOSE_WORKSPACE;
//
////  PostEvent (WE_MAP_SAVE_AND_CLOSE_WORKSPACE);
//
//  RunApp (GetApplicationDir() + EXE_LINKFREQPLAN_CIA_maps, DoubleQuotedStr(sIniFile));
//
////zzzzzzzzzzz  dmAct_Map.MAP_RESTORE_WORKSPACE;
//
// // PostEvent (WE_MAP_RESTORE_WORKSPACE);

end;

//--------------------------------------------------------------------
procedure TdmLinkFreqPlan_Tools.Calc_Interference(aLinkFreqPlan_ID: integer;
    aUseFixedChannels: boolean);
//--------------------------------------------------------------------
const
  TEMP_INI_FILE = 'LinkFreqPlan_cia.ini';
var
 // oInifile: TIniFile;
  sIniFile: string;

  oParams: TIni_LinkFreqPlan_CIA_params;

begin
  Clear_Interference(aLinkFreqPlan_ID);


  TLinkFreqPlan_run.RunCalc_CIA(aLinkFreqPlan_ID, aUseFixedChannels);


  {
  // ---------------------------------------------------------------
  sIniFile := g_ApplicationDataDir + TEMP_INI_FILE;


  DeleteFile(sIniFile);


  oParams:=TIni_LinkFreqPlan_CIA_params.Create;
  oParams.LinkFreqPlanID:= aLinkFreqPlan_ID;
  oParams.ChannelsKind:= IIF (aUseFixedChannels, 'fixed', 'Distributed');

  oParams.SaveToFile(sIniFile);

  FreeAndNil(oParams);
  }
 {

  oInifile:=TIniFile.Create (sIniFile);
  oInifile.WriteInteger ('main', 'ID',  aLinkFreqPlan_ID);
  oInifile.WriteInteger ('main', 'LinkFreqPlanID',  aLinkFreqPlan_ID);
  oInifile.WriteString  ('main', 'ChannelsKind', IIF (aUseFixedChannels, 'fixed', 'Distributed'));
  oInifile.WriteInteger ('main', 'ProjectID',       dmMain.ProjectID);

  FreeAndNil(oInifile);
//  oInifile.Free;
  }

//  RunApp (GetApplicationDir() + EXE_LINK_FREQPLAN_CIA, DoubleQuotedStr(sIniFile));
//          Format('%d %s',[aLinkFreqPlan_ID, aIsFullCalc]) );


{

  with dmLinkFreqPlan_CIA do begin
    Options.Link_FreqPlan_ID:= aLinkFreqPlan_ID;
    Options.ChannelsKind:= IIF (aUseFixedChannels, opUseFixedChannels, opUseDistrChannels);

    ExecuteDlg ('������ �������������');
  end;}

  //CreateMap_Interference(aLinkFreqPlan_ID);

 // TLinkFreqPlan_run.CreateMap_Interference(aLinkFreqPlan_ID);
end;



//--------------------------------------------------------------------
procedure TdmLinkFreqPlan_Tools.Exec_Calc(aLinkFreqPlan_ID: integer);
//--------------------------------------------------------------------
//const
//  TEMP_INI_FILE = 'LinkFreqPlan_calc.ini';
//var
//  oInifile: TIniFile;
//  sIniFile: string;

begin
  TLinkFreqPlan_run.Exec_Calc(aLinkFreqPlan_ID);

//  sIniFile := g_ApplicationDataDir + TEMP_INI_FILE;
//
//  DeleteFile(sIniFile);
//
//  oInifile:=TIniFile.Create (sIniFile);
////  oInifile.WriteInteger ('main', 'ID',  aLinkFreqPlan_ID);
//  oInifile.WriteInteger ('main', 'LinkFreqPlanID',  aLinkFreqPlan_ID);
//  oInifile.WriteInteger ('main', 'ProjectID',       dmMain.ProjectID);
//  oInifile.Free;
//
////  PostEvent (WE_MAP_SAVE_AND_CLOSE_WORKSPACE);
//
//  RunApp (GetApplicationDir() + EXE_LINKFREQPLAN_CALC, DoubleQuotedStr(sIniFile));
//
// // PostEvent (WE_MAP_RESTORE_WORKSPACE);

end;


//--------------------------------------------------------------------
procedure TdmLinkFreqPlan_Tools.Run_Distribute(aLinkFreqPlan_ID: Integer);
//    aIDList: TIDList);
//--------------------------------------------------------------------
const
  SQL_SELECT_RRL_IS_NEIGHBOURS =
      'SELECT Count(*) FROM ' + TBL_LINKFREQPLAN_NEIGHBORS +
      ' WHERE linkfreqPlan_id=:linkfreqPlan_id';

//var
 // oLink_IDList_full: TIDList;
//  i: Integer;
//  i, iLinkEnd1_ID, iLinkEnd2_ID: integer;
 // oLinkEnd_IDList: TIDList;

begin
//  FLink_FreqPlan_ID:= aLinkFreqPlan_ID;

  dmOnega_DB_data.OpenQuery(qry_Temp, SQL_SELECT_RRL_IS_NEIGHBOURS,
              [FLD_LINKFREQPLAN_ID, aLinkFreqPlan_ID]);

  if qry_Temp.Fields[0].AsInteger=0 then
  begin
    RunCalc_FREQPLAN_NB (aLinkFreqPlan_ID, False);

//    RunApp (GetApplicationDir() + EXE_LINK_FREQPLAN_NB_CALC,
  //        Format('%d %s',[aLinkFreqPlan_ID, 'True']) );

   // if dmLinkFreqPlan_Neighbors.Terminated then
   //   Exit;
  end;


  Exec_Calc(aLinkFreqPlan_ID);


(*
  oLink_IDList_full:= TIDList.Create;
  oLinkEnd_IDList:= TIDList.Create;

//  FLinkEnd_IDList.Clear;
  //FLinkEnd_IDList.Assign(aIDList);

  dmLinkFreqPlan_link.GetLinkIDList(aLinkFreqPlan_ID, oLink_IDList_full);

 // if not FLinkEnd_IDList.Compare(oLink_IDList_full) then
  for i := 0 to oLink_IDList_full.Count-1 do
  begin
    dmLink.GetLinkEndID (oLink_IDList_full[i].ID, iLinkEnd1_ID, iLinkEnd2_ID);

    oLinkEnd_IDList.AddID(iLinkEnd1_ID);
    oLinkEnd_IDList.AddID(iLinkEnd2_ID);
  end;
*)

{  db_OpenQuery(qry_Temp, SQL_SELECT_RRL_IS_NEIGHBOURS,
              [db_par(FLD_LINK_FREQPLAN_ID, aLinkFreqPlan_ID)]);

  if qry_Temp.Fields[0].AsInteger=0 then
  begin
    RunCalc_FREQPLAN_NB (aLinkFreqPlan_ID, False);
//    RunApp (GetApplicationDir() + EXE_LINK_FREQPLAN_NB_CALC,
  //        Format('%d %s',[aLinkFreqPlan_ID, 'True']) );

   // if dmLinkFreqPlan_Neighbors.Terminated then
   //   Exit;
  end;
}
  //--------------
  // RUN program
  //--------------

(*  dmLinkFreqPlan_calc.Params.LinkFreqPlan_ID:=aLinkFreqPlan_ID;
  dmLinkFreqPlan_calc.Execute(oLinkEnd_IDList);

  oLink_IDList_full.Free;
  oLinkEnd_IDList.Free;
*)


  Clear_Interference(aLinkFreqPlan_ID);

end;


begin

end.


(*


{
//--------------------------------------------------------------------
procedure TdmLinkFreqPlan_Tools.DataModuleCreate(Sender: TObject);
//--------------------------------------------------------------------
begin


end;

//--------------------------------------------------------------------
procedure TdmLinkFreqPlan_Tools.DataModuleDestroy(Sender: TObject);
//--------------------------------------------------------------------
begin
//  FLinkEnd_IDList.Free;
  inherited;
end;

}


//------------------------
    procedure DoGet_LinkEnd_ChParams(aLinkEnd1_ID: integer;

                 var aChNum : integer;  //, aChType
                 var aChannelType : string;
                 var aTxFreq, aRxFreq: double);

    //------------------------
    begin
      aTxFreq:=0;
      aRxFreq:=0;
      aChNum:=0;
//      aChType:=0;
      aChannelType:='';

      sChannelFixed:= gl_DB.GetStringFieldValue(TBL_LINKFREQPLAN_LINKEND, FLD_FREQ_FIXED,
                              [db_Par(FLD_LINKFREQPLAN_ID, aLinkFreqPlan_ID),
                               db_Par(FLD_LINKEND_ID,       aLinkEnd1_ID)]);
      if sChannelFixed = '' then
        Exit;

      {iChTypeFixed}
      aChannelType:= gl_DB.GetStringFieldValue(TBL_LINKFREQPLAN_LINKEND, FLD_CHANNEL_TYPE_FIXED, //FLD_CH_TYPE_FIXED,
                              [db_Par(FLD_LINKFREQPLAN_ID, aLinkFreqPlan_ID),
                               db_Par(FLD_LINKEND_ID,       aLinkEnd1_ID)]);

      ClearArray(arrFixedChannel);
      arrFixedChannel:= StringToIntArray(sChannelFixed);

      if Length(arrFixedChannel) > 0 then
      begin
        aChNum:= arrFixedChannel[0];

        if Eq(aChannelType,'low') then
        begin  //low
             aTxFreq:= dFirstChannelFreq + dChannelWidth*(aChNum - 1);
             aRxFreq:= aTxFreq + dDuplexFreqDelta;
        end else
        begin  //high
             aRxFreq:= dFirstChannelFreq + dChannelWidth*(aChNum - 1);
             aTxFreq:= aRxFreq + dDuplexFreqDelta;
        end;

(*
        case aChType of
          0: begin  //low
               aTxFreq:= dFirstChannelFreq + dChannelWidth*(aChNum - 1);
               aRxFreq:= aTxFreq + dDuplexFreqDelta;
             end;
          1: begin  //high
               aRxFreq:= dFirstChannelFreq + dChannelWidth*(aChNum - 1);
               aTxFreq:= aRxFreq + dDuplexFreqDelta;
             end;
        end;
*)
      end;
    end;    //