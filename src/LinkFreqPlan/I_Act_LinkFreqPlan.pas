unit I_Act_LinkFreqPlan;

interface

type
  IAct_LinkFreqPlan_X = interface(IInterface)
  ['{26CF88B7-7BA5-4FE1-A61C-FDA0183120EB}']

      function Dlg_AddForLinkNet (aLinkNetID, aFolderID: integer): Integer;
  end;

var
  IAct_LinkFreqPlan: IAct_LinkFreqPlan_X;

implementation

end.
