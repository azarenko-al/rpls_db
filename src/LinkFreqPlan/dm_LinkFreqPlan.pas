unit dm_LinkFreqPlan;

interface

uses
  SysUtils, Classes, Forms, Db, RxMemDS, ADODB, FileCtrl, Variants,

  dm_Onega_DB_data,

  dm_Main,
  dm_Object_base,

  u_db,

  u_files,
  u_types,

  u_const_db,

  u_LinkFreqPlan_const

//  dm_CalcMap
  ;


type
  TdmLinkFreqPlanFileType = (ftCI,ftCA,ftCIA);


  //-------------------------------------------------------------------
  TdmFreqPlanRRLAddRec = record
  //-------------------------------------------------------------------
    NewName           : string;
    LinkNet_ID        : integer;
    MaxCalcDistanse_km: double;     //max дистанция расчета соседства
    Min_CI_dB:           double;     //защитное соотношение
  end;


  TdmLinkFreqPlan = class(TdmObject_base)
    qry_Link: TADOQuery;
    ds_Link: TDataSource;
    mem_FreqPlan_Add: TRxMemoryData;
    mem_FreqPlan_AddID: TIntegerField;
    mem_FreqPlan_AddName: TStringField;
    ds_FreqPlan_Add: TDataSource;

    qry_Net: TADOQuery;
    qry_LinkFreqPlan: TADOQuery;
    qry_Temp: TADOQuery;
    procedure DataModuleCreate(Sender: TObject);
  private
  public
    function Add(aRec: TdmFreqPlanRRLAddRec): Integer;

    function  Del (aID: Integer): boolean;

    function GetFileDir (aLinkFreqPlanID: Integer): string;
    function GetMapFileName (aLinkFreqPlanID: Integer; aFileType: TdmLinkFreqPlanFileType): string;

    procedure UpdateLinkEnd111(aID: integer; aParams: array of TDBParamRec);

  end;


function dmLinkFreqPlan: TdmLinkFreqPlan;


//==========================================================
implementation {$R *.dfm}
//==========================================================

var
  FdmLinkFreqPlan: TdmLinkFreqPlan;


// ---------------------------------------------------------------
function dmLinkFreqPlan: TdmLinkFreqPlan;
// ---------------------------------------------------------------
begin
  if not Assigned(FdmLinkFreqPlan) then
    FdmLinkFreqPlan := TdmLinkFreqPlan.Create(Application);

  Result := FdmLinkFreqPlan;
end;


//-------------------------------------------------------------------
procedure TdmLinkFreqPlan.DataModuleCreate(Sender: TObject);
//-------------------------------------------------------------------
begin
  inherited;
  TableName  := TBL_LinkFreqPlan;
  ObjectName := OBJ_LinkFreqPlan;

end;


//-------------------------------------------------------------------
function TdmLinkFreqPlan.Add(aRec: TdmFreqPlanRRLAddRec): Integer;
//-------------------------------------------------------------------
begin
  Result := dmOnega_DB_data.ExecStoredProc_ (SP_LINKFREQPLAN_ADD,
                  [
                   FLD_LINKNET_ID,         aRec.LinkNet_ID,

                   FLD_NAME,               aRec.NewName,
                   FLD_MaxCalcDistance_km, aRec.MaxCalcDistanse_km,
                   FLD_Min_CI_dB,          aRec.Min_CI_dB

                  ]);

  Assert(Result>0, 'Result>0');
end;

//-------------------------------------------------------------------
function TdmLinkFreqPlan.Del (aID: Integer): boolean;
//-------------------------------------------------------------------
var  sFileDir: string;
  k: Integer;
   // iLinkNetID,
   // iCalcMapID: integer;
begin
  try
  //  iLinkNetID   := GetIntFieldValue(aID, FLD_LINKNET_ID);
  //  sKey:= Format('LinkNet.%d-LinkFreqPlan.%d', [iLinkNetID, aID]);
(*    iCalcMapID := dmCalcMap.FindByKey(sKey);

    if iCalcMapID > 0 then
      dmCalcMap.Delete(iCalcMapID);
*)

{
    sFileDir:= GetFileDir(aID);

    if DirectoryExists(sFileDir) then
      ClearDir(sFileDir);
}

   k:=dmOnega_DB_data.LinkFreqPlan_Del(aID);


  //  inherited Del(aID);

    Result:=True;
  except
    Result:=False;
  end;

end;


//--------------------------------------------------------------------
procedure TdmLinkFreqPlan.UpdateLinkEnd111(aID: integer; aParams: array of
    TDBParamRec);
//--------------------------------------------------------------------
// procedure TdmLinkFreqPlan_Tools.Set_Priority(aDataset: TDataset);
begin
  gl_DB.UpdateRecord(TBL_LINKFREQPLAN_LINKEND, aID, aParams);
end;


//--------------------------------------------------------------------
function TdmLinkFreqPlan.GetFileDir(aLinkFreqPlanID: Integer): string;
//--------------------------------------------------------------------
var iLinknetID: integer;
begin
  iLinknetID:= GetIntFieldValue(aLinkFreqPlanID, FLD_LINKNET_ID);

  Result:= dmMain.Dirs.ProjectCalcDir+  Format(
           'LinkNet.%d\LinkFreqPlan.%d', [iLinknetID, ALinkFreqPlanID]);
end;

//--------------------------------------------------------------------
function TdmLinkFreqPlan.GetMapFileName(aLinkFreqPlanID: integer;
                                       aFileType: TdmLinkFreqPlanFileType): string;
//--------------------------------------------------------------------
begin
  Result:=GetFileDir(aLinkFreqPlanID);

  case aFileType of
    ftCA  : Result:=Result+'\ca.tab';
    ftCI  : Result:=Result+'\ci.tab';
    ftCIA : Result:=Result+'\cia.tab';
  else
    Result:= '';
  end;
end;


begin 
end.
