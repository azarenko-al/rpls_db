inherited dmLinkFreqPlan: TdmLinkFreqPlan
  Left = 553
  Top = 416
  Height = 372
  Width = 431
  inherited ADOStoredProc1: TADOStoredProc
    Left = 143
    Top = 79
  end
  object qry_Link: TADOQuery
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'id'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 5
      end>
    SQL.Strings = (
      'select   id,name'
      ' from freqplan'
      'where id=:id')
    Left = 28
    Top = 4
  end
  object ds_Link: TDataSource
    DataSet = qry_Link
    Left = 28
    Top = 52
  end
  object mem_FreqPlan_Add: TRxMemoryData
    FieldDefs = <>
    Left = 253
    Top = 14
    object mem_FreqPlan_AddID: TIntegerField
      FieldName = 'ID'
    end
    object mem_FreqPlan_AddName: TStringField
      FieldName = 'Name'
      Size = 200
    end
  end
  object ds_FreqPlan_Add: TDataSource
    DataSet = mem_FreqPlan_Add
    Left = 260
    Top = 168
  end
  object qry_Net: TADOQuery
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'id'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 5
      end>
    SQL.Strings = (
      'select   id,name'
      ' from freqplan'
      'where id=:id')
    Left = 120
    Top = 144
  end
  object qry_LinkFreqPlan: TADOQuery
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'id'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 5
      end>
    SQL.Strings = (
      'select   id,name'
      ' from freqplan'
      'where id=:id')
    Left = 26
    Top = 195
  end
  object qry_Temp: TADOQuery
    LockType = ltReadOnly
    Parameters = <>
    Left = 92
    Top = 246
  end
end
