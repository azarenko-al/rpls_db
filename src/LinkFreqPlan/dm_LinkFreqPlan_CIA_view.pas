unit dm_LinkFreqPlan_CIA_view;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, ADODB, dxmdaset, Variants,

  dm_Onega_DB_data,

  dm_Custom,

  u_types,

  dm_Main,
  dm_LinkEnd,
  dm_LinkFreqPlan,

  u_func,
  u_const_db,
 // u_const_msg,
  u_db,
//  u_func_msg,
  u_geo,
  //u_geo_classes,

  u_LinkFreqPlan_const,

  d_Progress
  ;                                                                   

type
  TdmLinkFreqPlan_CIA_view = class(TDataModule)
    qry_Items: TADOQuery;
    qry_Count: TADOQuery;
    qry_Distance111: TADOQuery;
    qry_LinkEnd: TADOQuery;
    qry_Temp1: TADOQuery;
    qry_Temp: TADOQuery;
    ADOStoredProc1: TADOStoredProc;
    procedure DataModuleCreate(Sender: TObject);
   
  private
    FMaxTreeID: integer;

  //  FBLPointArr: TBLPointArrayF;
    FInterference_ChannelsKind : string;
    FFieldName_TXFreq : string;
    FFieldName_RXFreq : string;

    procedure DoAddNeighBours (aDataset: TDataset;
                    aLinkFreqPlanID, aLinkEndID, aTreeParentID: integer);

  public
    Params: record
              IsOnly_Interference : boolean;
              Admitted_CI : double;  //����������
              Admitted_Degradation : double;
            end;

    procedure InitData(aDataset: TDataset);

    procedure InitDB(aDataset: TDataset);
    procedure OpenDB (aDataset: TDataset; aLinkFreqPlanID, aLinkEndID: integer);

    procedure Get_Pair_Vliyan(aLinkEndID1, aLinkEndID2: integer; var aBLPointArr: TBLPointArrayF);
                            //   aIsMapIncrease: boolean);

    procedure Load_LinkFreqPlan(aDataset: TDataset; aLinkFreqPlanID: integer);

    procedure ExpandNeighBours2(aDataset: TDataset; aLinkFreqPlanID, aLinkEndID, aTreeParentID: integer);

  end;

(*
const
  TYPE_NEIGHBORS      = 1;  //����� �� ����������
  TYPE_NEIGHBORS_LINK = 2;  //����� �� Link
  TYPE_NEIGHBORS_PROP = 3;  //����� �� Property
*)

function dmLinkFreqPlan_CIA_view: TdmLinkFreqPlan_CIA_view;


//=================================================================
implementation

uses dm_Object_base; {$R *.DFM}
//=================================================================


//  LAYER_BEZIER = 'Temp_Bezier';



var
  FdmLinkFreqPlan_CIA_view: TdmLinkFreqPlan_CIA_view;


// ---------------------------------------------------------------
function dmLinkFreqPlan_CIA_view: TdmLinkFreqPlan_CIA_view;
// ---------------------------------------------------------------
begin
  if not Assigned(FdmLinkFreqPlan_CIA_view) then
    FdmLinkFreqPlan_CIA_view := TdmLinkFreqPlan_CIA_view.Create(Application);

  Result := FdmLinkFreqPlan_CIA_view;
end;

//-------------------------------------------------------------------
procedure TdmLinkFreqPlan_CIA_view.DataModuleCreate(Sender: TObject);
//-------------------------------------------------------------------
begin
  inherited;

  db_SetComponentADOConnection(Self, dmMain.ADOConnection);


  FMaxTreeID:=10000000;
end;


//-------------------------------------------------------------------
procedure TdmLinkFreqPlan_CIA_view.InitDB(aDataset: TDataset);
//-------------------------------------------------------------------
begin
  db_CreateField(aDataset,
    [
      db_Field( FLD_DXtree_id,        ftInteger),
      db_Field( FLD_DXtree_parent_id, ftInteger),

      db_Field( FLD_RX_LinkEnd_name, ftString),
      db_Field( FLD_TX_LinkEnd_name, ftString),



      db_Field( FLD_TX_LINKEND_ID,   ftInteger),
      db_Field( FLD_RX_LINKEND_ID,   ftInteger),

      db_Field( FLD_LINKEND_ID,      ftInteger),
      db_Field( FLD_LinkEnd_name,    ftString),

    //  db_Field( FLD_DISTANCE_M,     ftFloat),
      db_Field( FLD_DISTANCE_KM,     ftFloat),
      db_Field( FLD_Rx_Level_dBm,    ftFloat),

//      db_Field( FLD_POWER_dBm,       ftFloat),
      db_Field( 'POWER_NEIGHBORS',   ftFloat),
      db_Field( FLD_FREQ_MHZ,         ftString),

      db_Field( FLD_CIA,             ftFloat),
      db_Field( FLD_CI,              ftFloat),
      db_Field( FLD_CA,              ftFloat),

//      db_Field( FLD_INTERFERENCE,    ftInteger),
      db_Field( FLD_NUMBER,          ftInteger),


      db_Field( FLD_NEIGHBOUR_COUNT, ftInteger),
      db_Field( 'NEIGHBOUR_COUNT1',  ftInteger),
      db_Field( FLD_THRESHOLD_DEGRADATION, ftFloat),
      db_Field( FLD_THRESHOLD_DEGRADATION_CIA, ftFloat),
      db_Field( FLD_IS_EXPANDED,     ftBoolean),
      db_Field( FLD_IsNeigh,         ftBoolean)
    ]);
end;

//-------------------------------------------------------------------
procedure TdmLinkFreqPlan_CIA_view.InitData(aDataset: TDataset);
//-------------------------------------------------------------------
begin
  db_CreateField(aDataset,
        [db_field('no',             ftInteger),
         db_field(FLD_ID,           ftInteger),
         db_field(FLD_LINKEND_ID,   ftInteger),
         db_field(FLD_LINKEND_NAME, ftString),
         db_field(FLD_LAT,          ftFloat),
         db_field(FLD_LON,          ftFloat),
         db_Field(FLD_THRESHOLD_DEGRADATION_CIA, ftFloat),
         db_field(FLD_CI,           ftFloat),
         db_field(FLD_CA,           ftFloat),
         db_field(FLD_CIA,          ftFloat)]);

end;

//-------------------------------------------------------------------
procedure TdmLinkFreqPlan_CIA_view.DoAddNeighBours (aDataset: TDataset;
                     aLinkFreqPlanID, aLinkEndID, aTreeParentID: integer);
//-------------------------------------------------------------------
const
  SQL_SELECT_LINKFREQPLAN_NEIGHBORS =
      'SELECT * FROM ' + TBL_LINKFREQPLAN_NEIGHBORS +
      ' WHERE (LinkFreqPlan_id=:LinkFreqPlan_id) and ' +
//      '       (RX_Link_id=:linkend_id)'; // and (IsNeigh=1)';
      '       (RX_Linkend_id=:linkend_id)'; // and (IsNeigh=1)';



var bIsAddRecord: boolean;
  sLinkEnd_name: string;
begin
  dmOnega_db_data.OpenQuery(qry_Temp, SQL_SELECT_LINKFREQPLAN_NEIGHBORS,
                 [FLD_LINKFREQPLAN_ID, aLinkFreqPlanID,
                  FLD_LINKEND_ID, aLinkEndID
                  ]);

  with qry_Temp do
    while not Eof do
  begin
    bIsAddRecord:= False;

    if not (FieldByName(FLD_IsLink).AsBoolean ) then
      bIsAddRecord:= True;

    if (FieldValues[FLD_CIA] = null) and
       (FieldValues[FLD_THRESHOLD_DEGRADATION_CIA] = null)
    then
      bIsAddRecord:= False;

    if Params.IsOnly_Interference then
    begin
      bIsAddRecord:=
         ((FieldValues[FLD_CIA] <> null) and
          (FieldByName(FLD_CIA).AsFloat < Params.Admitted_CI))
          or
         (FieldByName(FLD_THRESHOLD_DEGRADATION_CIA).AsFloat > Params.Admitted_Degradation);

 //     then
  //      bIsAddRecord:= True
   //   else
    //    bIsAddRecord:= False;
    end;

    if bIsAddRecord then
    begin
      dmOnega_db_data.OpenQuery(qry_LinkEnd,
          'SELECT * FROM '+ TBL_LINKFREQPLAN_LINKEND +
          ' WHERE (LinkFreqPlan_id=:LinkFreqPlan_id) and ' +
          '       (LinkEnd_id=:LinkEnd_id)',
         [FLD_LINKEND_ID,       aLinkEndID,
          FLD_LINKFREQPLAN_ID, aLinkFreqPlanID]);


      sLinkEnd_name := dmLinkEnd.GetNameByID (FieldValues[FLD_TX_LINKEND_ID]);

      db_AddRecord (aDataset,
            [db_par(FLD_DXtree_id,        FMaxTreeID),
             db_par(FLD_DXtree_parent_id, aTreeParentID),


             db_par('POWER_NEIGHBORS',
             //   FieldValues[FLD_POWER_dBm] - FieldValues[FLD_DIAGRAM_LEVEL]),
                FieldValues[FLD_RX_LEVEL_dBm] - FieldValues[FLD_DIAGRAM_LEVEL]),

             db_par(FLD_LINKEND_NAME,  dmLinkEnd.GetNameByID (FieldValues[FLD_TX_LINKEND_ID]) ),

             db_par(FLD_FREQ_MHZ, qry_LinkEnd.FieldByName(FFieldName_TXFreq).AsString+'/'+
                                  qry_LinkEnd.FieldByName(FFieldName_RXFreq).AsString),

             db_par(FLD_DISTANCE_KM,    FieldByName(FLD_DISTANCE_M).AsFloat / 1000),

             db_par(FLD_THRESHOLD_DEGRADATION_CIA,
                              FieldValues[FLD_THRESHOLD_DEGRADATION_CIA]),

             db_par(FLD_CIA,           FieldValues[FLD_CIA]),
             db_par(FLD_CI,            FieldValues[FLD_CI_]),
             db_par(FLD_CA,            FieldValues[FLD_CA])
             ]);
    end;

    FMaxTreeID:= FMaxTreeID+1;
    Next;
  end;
end;

//-------------------------------------------------------------------
procedure TdmLinkFreqPlan_CIA_view.OpenDB (aDataset: TDataset;
                                           aLinkFreqPlanID, aLinkEndID: integer);
//-------------------------------------------------------------------
const

(*  SQL_SELECT_LINK_DISTANCE1 =
        'SELECT length FROM ' + TBL_Link +
//        ' WHERE (project_id=:project_id) and ' +
        ' WHERE  ' +
        ' ((linkend1_id=:linkend_id) OR (linkend2_id=:linkend_id))';

*)


  SQL_SELECT_LINK_FREQPLAN_LINKEND =
     'SELECT * FROM '+ VIEW_LINKFREQPLAN_CIA +
     ' WHERE (LinkFreqPlan_id=:LinkFreqPlan_id) and ' +
     '       (LinkEnd_id=:LinkEnd_id) ';

var
  dRx_Level_dBm: double;

begin
  aDataset.DisableControls;

  aDataset.Close;
  aDataset.Open;

  dmOnega_db_data.OpenQuery(qry_Items, SQL_SELECT_LINK_FREQPLAN_LINKEND,
               [
                FLD_LINKFREQPLAN_ID, aLinkFreqPlanID,
                FLD_LINKEND_ID,      aLinkEndID

                ]);

  if not qry_Items.IsEmpty then
    with qry_Items do
  begin

//      dRX_LEVEL_dBm:= gl_DB.GetDoubleFieldValue(TBL_LINKFREQPLAN_NEIGHBORS, FLD_RX_LEVEL_dBm, //FLD_POWER_dBm,



//    dRx_Level_dBm:= gl_DB.GetDoubleFieldValue(TBL_LINKFREQPLAN_NEIGHBORS, FLD_POWER_dBm,
    dRx_Level_dBm:= gl_DB.GetDoubleFieldValue(TBL_LINKFREQPLAN_NEIGHBORS, FLD_Rx_Level_dBm,
                [db_par(FLD_LINKFREQPLAN_ID, aLinkFreqPlanID),
                 db_par(FLD_IsLink,          1),
                 db_par(FLD_TX_LINKEND_ID,   aLinkEndID)
                 ]);

  (*  db_OpenQuery(qry_Distance,
    FFieldName_RXFreq:= IIF( = STR_FIXED, FLD_FREQ_FIXED_RX,
                                                         FLD_FREQ_DISTRIBUTED_RX);
    db_AddRecord (aDataset,            SQL_SELECT_LINK_DISTANCE,
                    [db_par(FLD_PROJECT_ID, dmMain.ProjectID),
                     db_par(FLD_LINKEND_ID, aLinkEndID)
                    ]);*)

    FInterference_ChannelsKind:= dmLinkFreqPlan.GetStringFieldValue(aLinkFreqPlanID,
                               FLD_Interference_ChannelsKind1);

    FFieldName_TXFreq:= IIF(FInterference_ChannelsKind = STR_FIXED, FLD_FREQ_FIXED_TX,
                                                         FLD_FREQ_DISTRIBUTED_TX);

    FFieldName_RXFreq:= IIF(FInterference_ChannelsKind = STR_FIXED, FLD_FREQ_FIXED_RX,
                                                         FLD_FREQ_DISTRIBUTED_RX);

(*
    FInterference_ChannelsKind:= dmLinkFreqPlan.GetStringFieldValue(aLinkFreqPlanID,
                               FLD_Interference_ChannelsKind);

    FFieldName_TXFreq:= IIF(FInteFInterference_ChannelsKindrference_ChannelsKind = STR_FIXED, FLD_FREQ_FIXED_TX,
                                                         FLD_FREQ_DISTRIBUTED_TX);
*)
    db_AddRecord (aDataset,
        [
         db_par(FLD_DXtree_id,        FieldValues[FLD_ID]),
         db_par(FLD_DXTREE_PARENT_ID, 0),

         db_par(FLD_TX_LinkEnd_ID,    FieldValues[FLD_ID]),

         db_par(FLD_LinkEnd_name,     FieldByName(FLD_LinkEnd_name).AsString ),

         db_par(FLD_Rx_Level_dBm,     dRx_Level_dBm),

         db_par(FLD_DISTANCE,         FieldByName(FLD_LENGTH).AsFloat ),

//         db_par(FLD_DISTANCE,        qry_Distance.FieldByName(FLD_LENGTH).AsFloat),

         db_par(FLD_FREQ_MHZ,  FieldByName(FFieldName_TXFreq).AsString+'/'+
                               FieldByName(FFieldName_RXFreq).AsString),

         db_par(FLD_THRESHOLD_DEGRADATION_CIA,
                          FieldValues[FLD_THRESHOLD_DEGRADATION_CIA]),

         db_par(FLD_CIA,             FieldValues[FLD_CIA]),
         db_par(FLD_CI,              FieldValues[FLD_CI]),
         db_par(FLD_CA,              FieldValues[FLD_CA])
         ]);

    DoAddNeighBours(aDataset, aLinkFreqPlanID, aLinkEndID, FieldValues[FLD_ID]);

  end;
  aDataset.EnableControls;

end;

//------------------------------------------------------------
procedure TdmLinkFreqPlan_CIA_view.Load_LinkFreqPlan(aDataset: TDataset; aLinkFreqPlanID:
    integer);
//------------------------------------------------------------
//const

{  SQL_SELECT_LINK_FREQPLAN_LINKEND =
    'SELECT '+
    ' (SELECT name FROM LinkEnd WHERE id=LINKFREQPLAN_LINKEND.linkend_id) as [linkend_name], '+

    ' (SELECT [power] FROM LINKFREQPLAN_NEIGHBORS '+
    '   WHERE (Link_FreqPlan_id=:Link_FreqPlan_id) AND (IsLink = 1) AND '+
    '         (rx_link_id = LINKFREQPLAN_LINKEND.linkend_id)) AS [power], '+

    ' (SELECT length FROM Link '+
    '   WHERE (project_id=:project_id) and '+
    '         ((linkend1_id=LINKFREQPLAN_LINKEND.linkend_id) OR '+
    '          (linkend2_id=LINKFREQPLAN_LINKEND.linkend_id))) AS [length], '+

    ' (SELECT Count(*) FROM LINKFREQPLAN_NEIGHBORS '+
    '   WHERE (IsLink<>1) and '+
    '         (Link_FreqPlan_id=:Link_FreqPlan_id) and '+
    '         (RX_Link_id=LINKFREQPLAN_LINKEND.linkend_id) ) as [count], '+

    ' (SELECT Count(*) FROM LINKFREQPLAN_NEIGHBORS '+
    '   WHERE (IsNeigh=1) and (IsLink<>1) and '+
    '         (Link_FreqPlan_id=:Link_FreqPlan_id) and '+
    '         (RX_Link_id=LINKFREQPLAN_LINKEND.linkend_id)) as [count1], '+

    ' id, cia, ci, ca, linkend_id, threshold_degradation '+

    'FROM '+ TBL_LINK_FREQPLAN_LINKEND +
    ' WHERE (Enabled = 1) AND (Link_FreqPlan_id=:Link_FreqPlan_id)';
}
//   sp_LinkFreqPlan_Linkend = 'sp_LinkFreqPlan_Linkend';

begin
  aDataset.DisableControls;
  aDataset.Close;
  aDataset.Open;

  dmOnega_DB_data.LinkFreqPlan_Linkend1(ADOStoredProc1, aLinkFreqPlanID);


/////  db_OpenQuery(qry_temp1, 'exec '+ sp_LinkFreqPlan_Linkend +' '+ IntToStr(aLinkFreqPlanID));


{  db_OpenQuery(qry_temp1, SQL_SELECT_LINK_FREQPLAN_LINKEND,
                          [db_par(FLD_PROJECT_ID,       dmMain.ProjectID),
                           db_Par(FLD_LINK_FREQPLAN_ID, aLinkFreqPlanID)]);
}
//  CursorHourGlass;
  with ADOStoredProc1 do
    while not Eof do
  begin
    db_AddRecord (aDataset,
                [db_par (FLD_DXtree_id,             FieldValues[FLD_LINKEND_ID]),
             //    db_par (FLD_DXtree_parent_id,       0),

                 db_par (FLD_LINKEND_ID,            FieldValues[FLD_LINKEND_ID]),

                 db_par (FLD_TX_LINKEND_ID,         FieldValues[FLD_LINKEND_ID]),
                 db_par (FLD_TX_LinkEnd_name,       FieldValues[FLD_LinkEnd_name]),

//                 db_par (FLD_RX_LINKEND_ID,         FieldValues[FLD_LINKEND_ID]),
                 db_par (FLD_RX_LinkEnd_name,       FieldValues[FLD_RX_LinkEnd_name]),

                 db_par (FLD_Rx_Level_dBm,          FieldValues[FLD_Rx_Level_dBm]),

//                 db_par (FLD_POWER_dBm,             FieldValues[FLD_POWER_dBm]),
//                 db_par (FLD_DISTANCE_M,           FieldValues[FLD_LENGTH]),
                 db_par (FLD_DISTANCE_KM,           FieldValues[FLD_LENGTH] / 1000),
                 db_par (FLD_NEIGHBOUR_COUNT,       FieldValues[FLD_COUNT]),
                 db_par ('NEIGHBOUR_COUNT1',        FieldValues['COUNT1']),
                 db_par (FLD_THRESHOLD_DEGRADATION, FieldValues[FLD_THRESHOLD_DEGRADATION]),

                 db_par (FLD_IS_EXPANDED,           False)
                 ]);

    Next;
  end;

  aDataset.EnableControls;
//  CursorDefault;
end;

//-------------------------------------------------------------------
procedure TdmLinkFreqPlan_CIA_view.ExpandNeighBours2(aDataset: TDataset;
    aLinkFreqPlanID, aLinkEndID, aTreeParentID: integer);
//-------------------------------------------------------------------
const
  SQL_SELECT =
      'SELECT *, '+
      ' (SELECT name FROM LinkEnd '+
      '   WHERE id='+TBL_LINKFREQPLAN_NEIGHBORS+'.tx_LinkEnd_id) as [tx_linkend_name] '+
      ' FROM ' + TBL_LINKFREQPLAN_NEIGHBORS +
      ' WHERE (LinkFreqPlan_id=:LinkFreqPlan_id) and ' +
      '       (RX_Linkend_id=:linkend_id) and (IsLink<>1)';
//      '       (TX_Link_id=:linkend_id) and (IsNeigh=1)';

begin
  dmOnega_db_data.OpenQuery(qry_Items, SQL_SELECT,
                 [FLD_LINKFREQPLAN_ID, aLinkFreqPlanID,
                  FLD_LINKEND_ID,       aLinkEndID ]);

  with qry_Items do
    while not Eof do
  begin
    db_AddRecord (aDataset,
        [db_par(FLD_DXtree_id,        aTreeParentID + FMaxTreeID),
         db_par(FLD_DXtree_parent_id, aTreeParentID),

         db_par(FLD_RX_LINKEND_ID,    FieldValues[FLD_RX_LINKEND_ID]),

         db_par(FLD_TX_LINKEND_ID,    FieldValues[FLD_TX_LINKEND_ID]),
         db_par(FLD_TX_LINKEND_NAME,  FieldValues[FLD_TX_LinkEnd_name]),

         db_par(FLD_LINKEND_ID,       FieldValues[FLD_TX_LINKEND_ID]),
                                                                             
         db_par(FLD_DISTANCE_km,      FieldByName(FLD_DISTANCE_m).AsFloat / 1000  ),

         db_par(FLD_CI,               FieldValues[FLD_CI]),
         db_par(FLD_IsNeigh,          FieldByName(FLD_IsNeigh).AsBoolean),

         db_par(FLD_THRESHOLD_DEGRADATION,
                                      FieldValues[FLD_THRESHOLD_DEGRADATION])

//         db_par(FLD_INTERFERENCE,     FieldByName(FLD_POWER_dBm).AsFloat -


{
       db_par(FLD_INTERFERENCE,
             FieldByName(FLD_RX_LEVEL_dBm).AsFloat -
             FieldByName(FLD_DIAGRAM_LEVEL).AsFloat +
             FieldByName(FLD_EXTRA_LOSS).AsFloat)
}

         ]);

    Next;
  end;
end;

//------------------------------------------------------------
procedure TdmLinkFreqPlan_CIA_view.Get_Pair_Vliyan(aLinkEndID1, aLinkEndID2: integer; var aBLPointArr: TBLPointArrayF);
//------------------------------------------------------------

  //-------------------------------------------------------------------
  function DoGetLinkDistance_m(aLinkEndID: integer): double;
  //-------------------------------------------------------------------
  var blVector: TBLVector;
  begin
    db_OpenQuery (qry_Temp1,
                 'SELECT id, lat1, lon1, lat2, lon2 '+
                 ' FROM '+ view_link +
                 ' WHERE (LinkEnd1_id=:id) or (LinkEnd2_id=:id)',
                 [db_Par(FLD_ID, aLinkEndID)]);

    if qry_Temp1.IsEmpty then
      Result:=0
    else begin
      blVector:= db_ExtractBLVector(qry_Temp1);
      Result:=geo_Distance_m (blVector);
    end;
  end;

  //-------------------------------------------------------------------
  function DoGetAzimuth_LinkEndID (aLinkEndID: integer): double;
  // ---------------------------------------------------------------
  begin
    dmOnega_db_data.OpenQuery(qry_Temp1, 'SELECT azimuth FROM ' + TBL_LINKEND_ANTENNA +
                           ' WHERE linkend_id=:linkend_id',
                             [FLD_LINKEND_ID, aLinkEndID]);

    Result :=  qry_Temp1.FieldByName(FLD_AZIMUTH).AsFloat;
  end;

 { //----------------------�������� �� ����� ����� --------------------
  procedure DoShowLineOnMap (var aBLPoints: TBLPointArrayF; aColor:integer);
  // ---------------------------------------------------------------
  begin
    PostEvent(WE_MAP_CLEAR_TEMP_LAYER, [app_Par(PAR_NAME, LAYER_BEZIER)]);

    PostEvent(WE_MAP_ADD_TEMP_LAYER_BEZIER,
                     [app_Par(PAR_NAME, LAYER_BEZIER),
                      app_Par(PAR_BLPOINT_ARR,  @aBLPoints),
                      app_Par(PAR_COLOR,         aColor)
                     ]);
  end;}

var blPoint1, blPoint2, blPoint3, blPoint4: TBLPoint;
    dAz1, dAz2 : double;
  //  blRect: TBLRect;
begin
//  FBLPoints.Clear;
  if aLinkEndID2=0 then
    Exit;

  if aLinkEndID1=0 then
    Exit;


  blPoint1:= dmLinkEnd.GetPos(aLinkEndID1, OBJ_LINKEND);
  blPoint4:= dmLinkEnd.GetPos(aLinkEndID2, OBJ_LINKEND);

{
  blPoint1:= dmLinkEnd.GetPropertyPos(aLinkEndID1);
  blPoint4:= dmLinkEnd.GetPropertyPos(aLinkEndID2);
}

{  blPoint4:= dmLinkEnd.GetPropertyPos(aLinkEndID1);
  blPoint1:= dmLinkEnd.GetPropertyPos(aLinkEndID2);
}
//  blPoint1:= dmLinkEnd.GetPropertyPos(aLinkEndID1);
//  blPoint4:= dmLinkEnd.GetPropertyPos(aLinkEndID2);

  dAz1:= DoGetAzimuth_LinkEndID(aLinkEndID1);
  dAz2:= DoGetAzimuth_LinkEndID(aLinkEndID2);

  blPoint2:=geo_RotateByAzimuth(blPoint1, DoGetLinkDistance_m(aLinkEndID1){/8}, dAz1);
  blPoint3:=geo_RotateByAzimuth(blPoint4, DoGetLinkDistance_m(aLinkEndID2){/2}, dAz2);

(*  dAz5:= IIF(Abs(dAz1-dAz2)<=90, -dAz2, dAz1-(Abs(dAz1-dAz2)-180));
  dAz6:= IIF(Abs(dAz2-dAz1)<=90, -dAz1, dAz2-(Abs(dAz2-dAz1)-180));
  if dAz5>359 then dAz5:= dAz5-360;
  if dAz6>359 then dAz6:= dAz6-360;

  blPoint5:=geo_RotateByAzimuth(blPoint1, DoGetLinkDistance_m(iLinkEndID1)/2, dAz5);
  blPoint6:=geo_RotateByAzimuth(blPoint4, DoGetLinkDistance_m(iLinkEndID2)/2, dAz6);
*)
  aBLPointArr.Count:=4;
  aBLPointArr.Items[0]:=blPoint1;
  aBLPointArr.Items[1]:=blPoint2;
  aBLPointArr.Items[2]:=blPoint3;
  aBLPointArr.Items[3]:=blPoint4;

//  FBLPoints.AddItem(blPoint1);
 // FBLPoints.AddItem(blPoint2);

(*  blPointArr.AddItem(blPoint5);
  blPointArr.AddItem(blPoint6);
*)
//  FBLPoints.AddItem(blPoint3);
 // FBLPoints.AddItem(blPoint4);
{
  if aIsMapIncrease then
  begin
   // FBLPoints.MakeBLPointArray(blPointArr);
    blRect:= geo_DefineRoundBLRect(FBLPointArr);//blPointArr);
    PostBLRect(WE_MAP_SHOW_RECT, blRect);
  end;

  DoShowLineOnMap (FBLPointArr, clRed);
  }


//  DoShowLineOnMap (FBLPoints, clRed);
end;


begin


end.