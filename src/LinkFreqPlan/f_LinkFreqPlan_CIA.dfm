inherited frm_LinkFreqPlan_CIA: Tfrm_LinkFreqPlan_CIA
  Left = 1047
  Top = 195
  Width = 853
  Height = 791
  Caption = 'CI, CA, CIA'
  ParentFont = True
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited ToolBar1: TToolBar
    Width = 837
    Height = 161
  end
  inherited StatusBar1: TStatusBar
    Top = 712
    Width = 837
    Visible = False
  end
  object bar_Region: TStatusBar [2]
    Left = 0
    Top = 161
    Width = 837
    Height = 19
    Align = alTop
    Constraints.MaxHeight = 19
    Panels = <
      item
        Bevel = pbNone
        Text = #1056#1056' '#1089#1077#1090#1100
        Width = 50
      end
      item
        Width = 170
      end>
    ParentShowHint = False
    ShowHint = True
    SizeGrip = False
  end
  object bar_EMP: TStatusBar [3]
    Left = 0
    Top = 731
    Width = 837
    Height = 21
    Panels = <
      item
        Text = #1042#1089#1077#1075#1086' '#1056#1056#1057':'
        Width = 120
      end
      item
        Text = #1090#1088#1077#1073'. CI:'
        Width = 100
      end
      item
        Text = #1090#1088#1077#1073'. '#1044#1077#1075#1088#1072#1076'.[dB]:'
        Width = 50
      end>
    SizeGrip = False
  end
  object bar_FreqPlan: TStatusBar [4]
    Left = 0
    Top = 180
    Width = 837
    Height = 19
    Align = alTop
    Constraints.MaxHeight = 19
    Panels = <
      item
        Bevel = pbNone
        Text = #1063#1058#1055
        Width = 50
      end
      item
        Width = 170
      end>
    ParentShowHint = False
    ShowHint = True
    SizeGrip = False
  end
  object Panel1: TPanel [5]
    Left = 0
    Top = 199
    Width = 837
    Height = 5
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 3
  end
  object PageControl1: TPageControl [6]
    Left = 0
    Top = 204
    Width = 837
    Height = 413
    ActivePage = TabSheet2
    Align = alTop
    TabOrder = 6
    TabPosition = tpBottom
    object TabSheet1: TTabSheet
      Caption = 'CIA'
      object pn_Top: TPanel
        Left = 0
        Top = 0
        Width = 829
        Height = 145
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object Panel3: TPanel
          Left = 0
          Top = 123
          Width = 829
          Height = 22
          Align = alBottom
          BevelOuter = bvLowered
          TabOrder = 0
          object cb_Only_Interf: TCheckBox
            Left = 4
            Top = 4
            Width = 205
            Height = 17
            Caption = #1090#1086#1083#1100#1082#1086' '#1087#1086#1084#1077#1093#1086#1074#1099#1077' '#1089#1077#1082#1090#1086#1088#1072
            Checked = True
            State = cbChecked
            TabOrder = 0
            OnClick = cb_Only_InterfClick
          end
        end
        object cxDBTreeList1: TcxDBTreeList
          Left = 0
          Top = 0
          Width = 829
          Height = 89
          Align = alTop
          Bands = <
            item
              Caption.AlignHorz = taCenter
            end>
          DataController.DataSource = ds_TreeView
          DataController.ParentField = 'tree_parent_id'
          DataController.KeyField = 'tree_id'
          LookAndFeel.Kind = lfFlat
          OptionsBehavior.CellHints = True
          OptionsBehavior.GoToNextCellOnTab = True
          OptionsBehavior.AutoDragCopy = True
          OptionsBehavior.DragCollapse = False
          OptionsBehavior.ExpandOnIncSearch = True
          OptionsBehavior.ShowHourGlass = False
          OptionsCustomizing.BandCustomizing = False
          OptionsCustomizing.BandVertSizing = False
          OptionsCustomizing.ColumnVertSizing = False
          OptionsData.Editing = False
          OptionsSelection.CellSelect = False
          OptionsSelection.HideFocusRect = False
          OptionsSelection.InvertSelect = False
          OptionsView.CellTextMaxLineCount = -1
          OptionsView.ShowEditButtons = ecsbFocused
          OptionsView.ColumnAutoWidth = True
          OptionsView.TreeLineStyle = tllsNone
          ParentColor = False
          Preview.AutoHeight = False
          Preview.MaxLineCount = 2
          RootValue = -1
          Styles.OnGetContentStyle = cxDBTreeList1StylesGetContentStyle
          Styles.Preview = cxStyle1_blue
          TabOrder = 1
          object col1_LinkEnd_Name: TcxDBTreeListColumn
            PropertiesClassName = 'TcxTextEditProperties'
            Properties.Alignment.Horz = taLeftJustify
            Properties.MaxLength = 0
            Properties.ReadOnly = True
            DataBinding.FieldName = 'LinkEnd_Name'
            Width = 80
            Position.ColIndex = 1
            Position.RowIndex = 0
            Position.BandIndex = 0
            Summary.FooterSummaryItems = <>
            Summary.GroupFooterSummaryItems = <>
          end
          object col1_Freq: TcxDBTreeListColumn
            PropertiesClassName = 'TcxTextEditProperties'
            Properties.Alignment.Horz = taLeftJustify
            Properties.MaxLength = 0
            Properties.ReadOnly = True
            DataBinding.FieldName = 'FREQ_MHZ'
            Position.ColIndex = 2
            Position.RowIndex = 0
            Position.BandIndex = 0
            Summary.FooterSummaryItems = <>
            Summary.GroupFooterSummaryItems = <>
          end
          object col1_rx_level: TcxDBTreeListColumn
            PropertiesClassName = 'TcxTextEditProperties'
            Properties.Alignment.Horz = taLeftJustify
            Properties.MaxLength = 0
            Properties.ReadOnly = True
            DataBinding.FieldName = 'Rx_level_dBm'
            Position.ColIndex = 3
            Position.RowIndex = 0
            Position.BandIndex = 0
            Summary.FooterSummaryItems = <>
            Summary.GroupFooterSummaryItems = <>
          end
          object col1_POWER_NEIGHBORS: TcxDBTreeListColumn
            PropertiesClassName = 'TcxTextEditProperties'
            Properties.Alignment.Horz = taLeftJustify
            Properties.MaxLength = 0
            Properties.ReadOnly = True
            DataBinding.FieldName = 'POWER_NEIGHBORS'
            Position.ColIndex = 4
            Position.RowIndex = 0
            Position.BandIndex = 0
            Summary.FooterSummaryItems = <>
            Summary.GroupFooterSummaryItems = <>
          end
          object col1_distance_km: TcxDBTreeListColumn
            PropertiesClassName = 'TcxTextEditProperties'
            Properties.Alignment.Horz = taLeftJustify
            Properties.MaxLength = 0
            Properties.ReadOnly = True
            DataBinding.FieldName = 'distance_km'
            Position.ColIndex = 5
            Position.RowIndex = 0
            Position.BandIndex = 0
            Summary.FooterSummaryItems = <>
            Summary.GroupFooterSummaryItems = <>
          end
          object col1_cia: TcxDBTreeListColumn
            PropertiesClassName = 'TcxTextEditProperties'
            Properties.Alignment.Horz = taLeftJustify
            Properties.MaxLength = 0
            Properties.ReadOnly = True
            DataBinding.FieldName = 'cia'
            Position.ColIndex = 8
            Position.RowIndex = 0
            Position.BandIndex = 0
            Summary.FooterSummaryItems = <>
            Summary.GroupFooterSummaryItems = <>
          end
          object col1_ci: TcxDBTreeListColumn
            PropertiesClassName = 'TcxTextEditProperties'
            Properties.Alignment.Horz = taLeftJustify
            Properties.MaxLength = 0
            Properties.ReadOnly = True
            DataBinding.FieldName = 'ci'
            Position.ColIndex = 6
            Position.RowIndex = 0
            Position.BandIndex = 0
            Summary.FooterSummaryItems = <>
            Summary.GroupFooterSummaryItems = <>
          end
          object col1_ca: TcxDBTreeListColumn
            PropertiesClassName = 'TcxTextEditProperties'
            Properties.Alignment.Horz = taLeftJustify
            Properties.MaxLength = 0
            Properties.ReadOnly = True
            DataBinding.FieldName = 'ca'
            Position.ColIndex = 7
            Position.RowIndex = 0
            Position.BandIndex = 0
            Summary.FooterSummaryItems = <>
            Summary.GroupFooterSummaryItems = <>
          end
          object col1_Threshold_Degradation_cia: TcxDBTreeListColumn
            PropertiesClassName = 'TcxTextEditProperties'
            Properties.Alignment.Horz = taLeftJustify
            Properties.MaxLength = 0
            Properties.ReadOnly = True
            DataBinding.FieldName = 'Threshold_Degradation_cia'
            Position.ColIndex = 9
            Position.RowIndex = 0
            Position.BandIndex = 0
            Summary.FooterSummaryItems = <>
            Summary.GroupFooterSummaryItems = <>
          end
          object col1_tree_id: TcxDBTreeListColumn
            Tag = -1
            PropertiesClassName = 'TcxTextEditProperties'
            Properties.Alignment.Horz = taLeftJustify
            Properties.MaxLength = 0
            Properties.ReadOnly = True
            Visible = False
            DataBinding.FieldName = 'tree_id'
            Position.ColIndex = 10
            Position.RowIndex = 0
            Position.BandIndex = 0
            Summary.FooterSummaryItems = <>
            Summary.GroupFooterSummaryItems = <>
          end
          object col1_Tree_parent_id: TcxDBTreeListColumn
            Tag = -1
            PropertiesClassName = 'TcxTextEditProperties'
            Properties.Alignment.Horz = taLeftJustify
            Properties.MaxLength = 0
            Properties.ReadOnly = True
            Visible = False
            DataBinding.FieldName = 'Tree_parent_id'
            Position.ColIndex = 0
            Position.RowIndex = 0
            Position.BandIndex = 0
            Summary.FooterSummaryItems = <>
            Summary.GroupFooterSummaryItems = <>
          end
        end
      end
      object pn_Bottom: TPanel
        Left = 0
        Top = 237
        Width = 829
        Height = 150
        Align = alBottom
        BevelOuter = bvNone
        TabOrder = 1
        object Panel2: TPanel
          Left = 0
          Top = 121
          Width = 829
          Height = 29
          Align = alBottom
          BevelOuter = bvNone
          TabOrder = 0
          object Button1: TButton
            Left = 4
            Top = 4
            Width = 245
            Height = 21
            Action = act_Load_LinkEnd_List
            TabOrder = 0
          end
        end
        object cxGrid1: TcxGrid
          Left = 0
          Top = 0
          Width = 729
          Height = 121
          Align = alLeft
          TabOrder = 1
          LookAndFeel.Kind = lfFlat
          object cxGrid1DBTableView1: TcxGridDBTableView
            NavigatorButtons.ConfirmDelete = False
            OnFocusedRecordChanged = cxGrid1DBTableView1FocusedRecordChanged
            DataController.DataSource = ds_Data
            DataController.Filter.MaxValueListCount = 1000
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <>
            DataController.Summary.SummaryGroups = <>
            Filtering.ColumnPopup.MaxDropDownItemCount = 12
            OptionsSelection.CellSelect = False
            OptionsSelection.HideFocusRectOnExit = False
            OptionsSelection.InvertSelect = False
            OptionsView.ColumnAutoWidth = True
            OptionsView.GroupByBox = False
            OptionsView.GroupFooters = gfVisibleWhenExpanded
            Preview.AutoHeight = False
            Preview.MaxLineCount = 2
            object col3_LinkEnd: TcxGridDBColumn
              Caption = #1056#1056#1057
              DataBinding.FieldName = 'LinkEnd_name'
              PropertiesClassName = 'TcxTextEditProperties'
              Properties.Alignment.Horz = taLeftJustify
              Properties.MaxLength = 0
              Properties.ReadOnly = True
              Options.Editing = False
              Options.Filtering = False
              SortIndex = 0
              SortOrder = soAscending
              Width = 114
            end
            object col3_CI: TcxGridDBColumn
              DataBinding.FieldName = 'CI'
              PropertiesClassName = 'TcxTextEditProperties'
              Properties.Alignment.Horz = taLeftJustify
              Properties.MaxLength = 0
              Properties.ReadOnly = True
              Options.Editing = False
              Options.Filtering = False
              Width = 66
            end
            object col3_CA: TcxGridDBColumn
              DataBinding.FieldName = 'CA'
              PropertiesClassName = 'TcxTextEditProperties'
              Properties.Alignment.Horz = taLeftJustify
              Properties.MaxLength = 0
              Properties.ReadOnly = True
              Options.Editing = False
              Options.Filtering = False
              Width = 66
            end
            object col3_CIA: TcxGridDBColumn
              DataBinding.FieldName = 'CIA'
              PropertiesClassName = 'TcxTextEditProperties'
              Properties.Alignment.Horz = taLeftJustify
              Properties.MaxLength = 0
              Properties.ReadOnly = True
              Options.Editing = False
              Options.Filtering = False
              Width = 62
            end
            object col3_threshold_degradation_cia: TcxGridDBColumn
              DataBinding.FieldName = 'threshold_degradation_cia'
              PropertiesClassName = 'TcxTextEditProperties'
              Properties.Alignment.Horz = taLeftJustify
              Properties.MaxLength = 0
              Properties.ReadOnly = True
              Options.Editing = False
              Options.Filtering = False
              Width = 131
            end
            object col3_linkend_id_bottom: TcxGridDBColumn
              DataBinding.FieldName = 'linkend_id'
              VisibleForCustomization = False
            end
          end
          object cxGrid1Level1: TcxGridLevel
            GridView = cxGrid1DBTableView1
          end
        end
      end
      object cxSplitter: TcxSplitter
        Left = 0
        Top = 229
        Width = 8
        Height = 8
        HotZoneClassName = 'TcxSimpleStyle'
        AlignSplitter = salBottom
        Control = pn_Bottom
      end
    end
    object TabSheet2: TTabSheet
      Caption = #1055#1072#1088#1085#1086#1077' '#1074#1083#1080#1103#1085#1080#1077
      ImageIndex = 1
      object Panel5: TPanel
        Left = 0
        Top = 365
        Width = 829
        Height = 22
        Align = alBottom
        BevelOuter = bvLowered
        TabOrder = 0
        object cb_Map_Increase: TCheckBox
          Left = 4
          Top = 4
          Width = 253
          Height = 17
          Caption = #1087#1086#1082#1072#1079#1099#1074#1072#1090#1100' '#1085#1072' '#1082#1072#1088#1090#1077' '#1074#1099#1076#1077#1083#1077#1085#1085#1099#1077' '#1086#1073#1098#1077#1082#1090#1099
          Checked = True
          State = cbChecked
          TabOrder = 0
          OnClick = cb_Only_InterfClick
        end
      end
      object cxDBTreeList21331_pairs: TcxDBTreeList
        Left = 0
        Top = 0
        Width = 829
        Height = 70
        Align = alTop
        Bands = <
          item
            Caption.AlignHorz = taCenter
          end>
        DataController.DataSource = ds_Pair
        DataController.ParentField = 'tree_parent_id'
        DataController.KeyField = 'tree_id'
        LookAndFeel.Kind = lfFlat
        OptionsBehavior.CellHints = True
        OptionsBehavior.GoToNextCellOnTab = True
        OptionsBehavior.AutoDragCopy = True
        OptionsBehavior.DragCollapse = False
        OptionsBehavior.ExpandOnIncSearch = True
        OptionsBehavior.ShowHourGlass = False
        OptionsCustomizing.BandCustomizing = False
        OptionsCustomizing.BandVertSizing = False
        OptionsCustomizing.ColumnVertSizing = False
        OptionsData.Deleting = False
        OptionsSelection.HideFocusRect = False
        OptionsSelection.InvertSelect = False
        OptionsView.CellTextMaxLineCount = -1
        OptionsView.ShowEditButtons = ecsbFocused
        OptionsView.GridLineColor = clBtnShadow
        OptionsView.GridLines = tlglBoth
        OptionsView.Indicator = True
        ParentColor = False
        Preview.AutoHeight = False
        Preview.MaxLineCount = 2
        RootValue = -1
        TabOrder = 1
        OnExpanded = cxDBTreeList21331_pairsExpanded
        OnExpanding = cxDBTreeList21331_pairsExpanding
        OnFocusedNodeChanged = cxDBTreeList21331_pairsFocusedNodeChanged
        object col2_number: TcxDBTreeListColumn
          PropertiesClassName = 'TcxTextEditProperties'
          Properties.Alignment.Horz = taLeftJustify
          Properties.MaxLength = 0
          Properties.ReadOnly = True
          Visible = False
          DataBinding.FieldName = 'number'
          Options.Editing = False
          Width = 58
          Position.ColIndex = 0
          Position.RowIndex = 0
          Position.BandIndex = 0
          SortOrder = soAscending
          Summary.FooterSummaryItems = <>
          Summary.GroupFooterSummaryItems = <>
        end
        object col2_RX_LinkEnd_Name: TcxDBTreeListColumn
          PropertiesClassName = 'TcxTextEditProperties'
          Properties.Alignment.Horz = taLeftJustify
          Properties.MaxLength = 0
          Properties.ReadOnly = True
          DataBinding.FieldName = 'RX_LinkEnd_Name'
          Options.Editing = False
          Width = 87
          Position.ColIndex = 2
          Position.RowIndex = 0
          Position.BandIndex = 0
          Summary.FooterSummaryItems = <>
          Summary.GroupFooterSummaryItems = <>
        end
        object col2_TX_LinkEnd_Name: TcxDBTreeListColumn
          PropertiesClassName = 'TcxTextEditProperties'
          Properties.Alignment.Horz = taLeftJustify
          Properties.MaxLength = 0
          Properties.ReadOnly = True
          DataBinding.FieldName = 'TX_LinkEnd_Name'
          Options.Editing = False
          Width = 62
          Position.ColIndex = 1
          Position.RowIndex = 0
          Position.BandIndex = 0
          Summary.FooterSummaryItems = <>
          Summary.GroupFooterSummaryItems = <>
        end
        object col2_LinkEnd_ID: TcxDBTreeListColumn
          Tag = -1
          PropertiesClassName = 'TcxTextEditProperties'
          Properties.Alignment.Horz = taLeftJustify
          Properties.MaxLength = 0
          Properties.ReadOnly = True
          Visible = False
          DataBinding.FieldName = 'LinkEnd_ID'
          Options.Editing = False
          Width = 74
          Position.ColIndex = 13
          Position.RowIndex = 0
          Position.BandIndex = 0
          Summary.FooterSummaryItems = <>
          Summary.GroupFooterSummaryItems = <>
        end
        object col2_Distance_km: TcxDBTreeListColumn
          PropertiesClassName = 'TcxTextEditProperties'
          Properties.Alignment.Horz = taLeftJustify
          Properties.MaxLength = 0
          Properties.ReadOnly = True
          Caption.Text = 'Distance'
          DataBinding.FieldName = 'Distance_km'
          Options.Editing = False
          Width = 58
          Position.ColIndex = 3
          Position.RowIndex = 0
          Position.BandIndex = 0
          Summary.FooterSummaryItems = <>
          Summary.GroupFooterSummaryItems = <>
        end
        object col2_CI: TcxDBTreeListColumn
          PropertiesClassName = 'TcxTextEditProperties'
          Properties.Alignment.Horz = taLeftJustify
          Properties.MaxLength = 0
          Properties.ReadOnly = True
          DataBinding.FieldName = 'CI'
          Options.Editing = False
          Width = 20
          Position.ColIndex = 4
          Position.RowIndex = 0
          Position.BandIndex = 0
          Summary.FooterSummaryItems = <>
          Summary.GroupFooterSummaryItems = <>
        end
        object col2_Rx_Level_dBm: TcxDBTreeListColumn
          PropertiesClassName = 'TcxTextEditProperties'
          Properties.Alignment.Horz = taLeftJustify
          Properties.MaxLength = 0
          Properties.ReadOnly = True
          Caption.Text = 'power'
          DataBinding.FieldName = 'Rx_Level_dBm'
          Options.Editing = False
          Width = 41
          Position.ColIndex = 5
          Position.RowIndex = 0
          Position.BandIndex = 0
          Summary.FooterSummaryItems = <>
          Summary.GroupFooterSummaryItems = <>
        end
        object col2_Neighbour_Count: TcxDBTreeListColumn
          PropertiesClassName = 'TcxTextEditProperties'
          Properties.Alignment.Horz = taLeftJustify
          Properties.MaxLength = 0
          Properties.ReadOnly = True
          DataBinding.FieldName = 'Neighbour_Count'
          Options.Editing = False
          Width = 90
          Position.ColIndex = 6
          Position.RowIndex = 0
          Position.BandIndex = 0
          Summary.FooterSummaryItems = <>
          Summary.GroupFooterSummaryItems = <>
        end
        object col2_Neighbour_Count1: TcxDBTreeListColumn
          PropertiesClassName = 'TcxTextEditProperties'
          Properties.Alignment.Horz = taLeftJustify
          Properties.MaxLength = 0
          Properties.ReadOnly = True
          DataBinding.FieldName = 'Neighbour_Count1'
          Options.Editing = False
          Width = 95
          Position.ColIndex = 7
          Position.RowIndex = 0
          Position.BandIndex = 0
          Summary.FooterSummaryItems = <>
          Summary.GroupFooterSummaryItems = <>
        end
        object col2_is_expanded: TcxDBTreeListColumn
          Tag = -1
          PropertiesClassName = 'TcxTextEditProperties'
          Properties.Alignment.Horz = taLeftJustify
          Properties.MaxLength = 0
          Properties.ReadOnly = True
          Visible = False
          DataBinding.FieldName = 'is_expanded'
          Options.Editing = False
          Width = 61
          Position.ColIndex = 12
          Position.RowIndex = 0
          Position.BandIndex = 0
          Summary.FooterSummaryItems = <>
          Summary.GroupFooterSummaryItems = <>
        end
        object col2_type: TcxDBTreeListColumn
          Tag = -1
          PropertiesClassName = 'TcxTextEditProperties'
          Properties.Alignment.Horz = taLeftJustify
          Properties.MaxLength = 0
          Properties.ReadOnly = True
          Visible = False
          DataBinding.FieldName = 'type'
          Options.Editing = False
          Width = 20
          Position.ColIndex = 11
          Position.RowIndex = 0
          Position.BandIndex = 0
          Summary.FooterSummaryItems = <>
          Summary.GroupFooterSummaryItems = <>
        end
        object col2_tree_id: TcxDBTreeListColumn
          Tag = -1
          PropertiesClassName = 'TcxTextEditProperties'
          Properties.Alignment.Horz = taLeftJustify
          Properties.MaxLength = 0
          Properties.ReadOnly = True
          Visible = False
          DataBinding.FieldName = 'tree_id'
          Options.Editing = False
          Width = 46
          Position.ColIndex = 9
          Position.RowIndex = 0
          Position.BandIndex = 0
          Summary.FooterSummaryItems = <>
          Summary.GroupFooterSummaryItems = <>
        end
        object col2_tree_parent_id: TcxDBTreeListColumn
          Tag = -1
          PropertiesClassName = 'TcxTextEditProperties'
          Properties.Alignment.Horz = taLeftJustify
          Properties.MaxLength = 0
          Properties.ReadOnly = True
          Visible = False
          DataBinding.FieldName = 'tree_parent_id'
          Options.Editing = False
          Width = 74
          Position.ColIndex = 10
          Position.RowIndex = 0
          Position.BandIndex = 0
          Summary.FooterSummaryItems = <>
          Summary.GroupFooterSummaryItems = <>
        end
        object col2_threshold_degradation_CIA: TcxDBTreeListColumn
          PropertiesClassName = 'TcxTextEditProperties'
          Properties.Alignment.Horz = taLeftJustify
          Properties.MaxLength = 0
          Properties.ReadOnly = True
          DataBinding.FieldName = 'threshold_degradation'
          Options.Editing = False
          Width = 111
          Position.ColIndex = 8
          Position.RowIndex = 0
          Position.BandIndex = 0
          Summary.FooterSummaryItems = <>
          Summary.GroupFooterSummaryItems = <>
        end
        object col2_IsNeigh: TcxDBTreeListColumn
          Tag = -1
          PropertiesClassName = 'TcxTextEditProperties'
          Properties.Alignment.Horz = taLeftJustify
          Properties.MaxLength = 0
          Properties.ReadOnly = True
          Visible = False
          DataBinding.FieldName = 'IsNeigh'
          Options.Editing = False
          Width = 42
          Position.ColIndex = -1
          Position.RowIndex = -1
          Position.BandIndex = -1
          Summary.FooterSummaryItems = <>
          Summary.GroupFooterSummaryItems = <>
        end
      end
    end
  end
  inherited FormStorage1: TFormStorage
    Left = 90
    Top = 5
  end
  object ActionList1: TActionList
    Left = 28
    Top = 53
    object act_Stay_On_Top: TAction
      Caption = #1056#1072#1089#1087#1086#1083#1086#1078#1077#1085#1080#1077' '#1086#1082#1085#1072
      ImageIndex = 0
    end
    object act_Load_LinkEnd_List: TAction
      Caption = #1054#1090#1086#1073#1088#1072#1079#1080#1090#1100' '#1089#1087#1080#1089#1086#1082' '#1056#1056#1057' c '#1080#1085#1090#1077#1088#1092#1077#1088#1077#1085#1094#1080#1077#1081
      OnExecute = act_Load_LinkEnd_ListExecute
    end
    object act_Setup_TreeList: TAction
      Caption = #1053#1072#1089#1090#1088#1086#1081#1082#1072
    end
  end
  object mem_Data: TdxMemData
    Indexes = <>
    SortOptions = []
    SortedField = 'CIA'
    Left = 224
    Top = 5
  end
  object ds_Data: TDataSource
    DataSet = mem_Data
    Left = 224
    Top = 54
  end
  object mem_TreeView: TdxMemData
    Indexes = <>
    SortOptions = []
    Left = 303
    Top = 6
  end
  object ds_TreeView: TDataSource
    DataSet = mem_TreeView
    Left = 303
    Top = 54
  end
  object qry_LinkEnd_NeighBors: TADOQuery
    Parameters = <>
    Left = 360
    Top = 114
  end
  object mem_Pair: TdxMemData
    Indexes = <>
    SortOptions = []
    Left = 132
    Top = 62
  end
  object ds_Pair: TDataSource
    DataSet = mem_Pair
    Left = 132
    Top = 110
  end
  object PopupMenu1: TPopupMenu
    Left = 142
    Top = 5
    object N1: TMenuItem
      Action = act_Setup_TreeList
    end
  end
  object qry_Temp: TADOQuery
    Parameters = <>
    Left = 448
    Top = 74
  end
  object ADOStoredProc1: TADOStoredProc
    ConnectionString = 
      'Provider=MSDASQL.1;Persist Security Info=False;User ID=sa;Data S' +
      'ource=onega;Initial Catalog=onega_link'
    CursorType = ctStatic
    ProcedureName = 'sp_LinkFreqPlan_Neighbors;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@ID'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 342
      end>
    Left = 608
    Top = 64
  end
  object DataSource1: TDataSource
    DataSet = ADOStoredProc1
    Left = 608
    Top = 16
  end
  object cxStyleRepository1: TcxStyleRepository
    Left = 32
    Top = 8
    PixelsPerInch = 96
    object cxStyle1_blue: TcxStyle
      AssignedValues = [svFont, svTextColor]
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlue
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      TextColor = clBlue
    end
  end
end
