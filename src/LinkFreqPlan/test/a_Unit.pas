unit a_Unit;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Db, RxMemDS,   dxDBCtrl,   dxDBTL, ComCtrls,
  rxPlacemnt, ToolWin, ExtCtrls,


  fr_LinkFreqPlan_LinkEnd,
  
//  dm_Act_View_Engine,
  dm_Act_LinkFreqPlan,
  dm_Act_Project,
  dm_Act_Explorer,
  dm_Act_Map_Engine,
 // dm_Act_LinkFreqPlan,
//  dm_Act_CalcRegion,
//  dm_Act_Site,

  dm_Main,
  dm_Main,
//  dm_Main_local,
 // dm_Main_res,

  u_func_msg,
  u_reg,
  u_const,
  u_const_db,
  u_const_msg,

  u_func,
  u_db,
  u_dlg,
  u_types,

  f_Log,


  fr_Browser,
  fr_LinkFreqPlan_View,
  fr_Explorer

//  fr_ch_plan
  ;



type
  TfrmMain1 = class(TForm)
    Panel1: TPanel;
    Splitter1: TSplitter;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    pn_Browser: TPanel;
    ToolBar1: TToolBar;
    Add: TButton;
    Button1: TButton;
    Import_: TButton;
    View: TButton;
    Button2: TButton;
    FormStorage1: TFormStorage;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure Button2Click(Sender: TObject);
  private
//    id: integer;
 //   Fframe_Explorer: Tframe_Explorer;
  //  Fframe_Browser : Tframe_Browser;
  public
    { Public declarations }
  end;

var
  frmMain1: TfrmMain1;

//====================================================================
implementation {$R *.DFM}
//====================================================================

//----------------------------------------------------------------
procedure TfrmMain1.FormCreate(Sender: TObject);
//----------------------------------------------------------------
var
  i: Integer;
begin


  frmMain1.Caption:= '��������� �����';
  Panel1.Align:= alClient;

  Tfrm_Log.CreateForm();

  TdmMain.Init;
  dmMain.OpenDlg;

  dmAct_Map_Engine.DebugMode:=True;

//  TdmAct_Map_Engine.Init;
  TdmAct_Project.Init;
  TdmAct_Explorer.Init;
  TdmAct_LinkFreqPlan.Init;


  TdmAct_LinkFreqPlan.Init;



  dmAct_Project.LoadLastProject;

{  CreateChildForm(Tframe_Explorer, Fframe_Explorer, TabSheet1);

 // Fframe_Explorer := Tframe_Explorer.CreateChildForm ( TabSheet1);
  Fframe_Explorer.SetViewObjects([otLinkFreqPlan, otLink]);
  Fframe_Explorer.RegPath:='FreqPlan_RRL';
  Fframe_Explorer.Load;


  CreateChildForm(Tframe_Browser, Fframe_Browser, pn_Browser);
//  Fframe_Browser := Tframe_Browser.CreateChildForm( pn_Browser);
  Fframe_Explorer.OnNodeFocused:=Fframe_Browser.ViewObjectByPIDL;
}

   i:=gl_DB.GetMaxID1(TBL_LINK_FREQ_PLAN );


  with Tframe_LinkFreqPlan_LinkEnd.Create(nil) do
  begin
    view (i);
    ShowModal;
  end;
end;

//----------------------------------------------------------------
procedure TfrmMain1.FormDestroy(Sender: TObject);
//----------------------------------------------------------------
begin
//  Fframe_Explorer.Free;
//  Fframe_Browser.Free;
end;


procedure TfrmMain1.Button2Click(Sender: TObject);
begin
//  frame_ch_plan.ShowModal;
end;

end.