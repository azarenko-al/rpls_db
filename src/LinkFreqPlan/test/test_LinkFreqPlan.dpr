program test_LinkFreqPlan;

uses
  a_Unitw124312 in 'a_Unitw124312.pas' {frmMain1},
  d_LinkFreqPlan_add in '..\d_LinkFreqPlan_add.pas' {dlg_LinkFreqPlan_add},
  dm_act_LinkFreqPlan in '..\dm_act_LinkFreqPlan.pas' {dmAct_LinkFreqPlan: TDataModule},
  dm_act_LinkFreqPlan_Link in '..\dm_act_LinkFreqPlan_Link.pas' {dmAct_LinkFreqPlan_Link: TDataModule},
  dm_Custom in '..\..\.common\dm_Custom.pas' {dmCustom: TDataModule},
  dm_LinkFreqPlan in '..\dm_LinkFreqPlan.pas' {dmLinkFreqPlan: TDataModule},
  dm_LinkFreqPlan_link in '..\dm_LinkFreqPlan_link.pas' {dmLinkFreqPlan_link: TDataModule},
  dm_LinkFreqPlan_LinkEnd_view in '..\dm_LinkFreqPlan_LinkEnd_view.pas' {dmLinkFreqPlan_LinkEnd_View: TDataModule},
  dm_LinkFreqPlan_NeighBors_view in '..\dm_LinkFreqPlan_NeighBors_view.pas' {dmLinkFreqPlan_NeighBors_view: TDataModule},
  dm_LinkFreqPlan_Tools in '..\dm_LinkFreqPlan_Tools.pas' {dmLinkFreqPlan_Tools: TDataModule},
  f_Custom in '..\..\.common\f_Custom.pas' {frm_Custom},
  f_LinkFreqPlan_CIA in '..\f_LinkFreqPlan_CIA.pas' {frm_LinkFreqPlan_CIA},
  Forms,
  fr_LinkFreqPlan_LinkEnd in '..\fr_LinkFreqPlan_LinkEnd.pas' {frame_LinkFreqPlan_LinkEnd},
  fr_LinkFreqPlan_LinkEnd_ in '..\fr_LinkFreqPlan_LinkEnd_.pas' {frame_LinkFreqPlan_LinkEnd_},
  fr_LinkFreqPlan_NeightBors in '..\fr_LinkFreqPlan_NeightBors.pas' {frame_LinkFreqPlan_NeightBors},
  fr_LinkFreqPlan_NeightBors_ in '..\fr_LinkFreqPlan_NeightBors_.pas' {frame_LinkFreqPlan_NeightBors_},
  fr_LinkFreqPlan_Report in '..\fr_LinkFreqPlan_Report.pas' {frame_LinkFreqPlan_Report},
  fr_LinkFreqPlan_Resource in '..\fr_LinkFreqPlan_Resource.pas' {frame_LinkFreqPlan_Resource},
  fr_LinkFreqPlan_view in '..\fr_LinkFreqPlan_view.pas' {frame_LinkFreqPlan_view},
  u_LinkFreqPlan_const in '..\u_LinkFreqPlan_const.pas',
  u_LinkFreqPlan_run in '..\u_LinkFreqPlan_run.pas',
  u_cx_VGrid_export in '..\..\..\..\common7\Package_Common\u_cx_VGrid_export.pas',
  u_ini_LinkFreqPlan_CIA_params in '..\..\LinkFreqPlan_CIA\src shared\u_ini_LinkFreqPlan_CIA_params.pas',
  u_LinkFreqPlan_classes in '..\..\LinkFreqPlan_common_classes\u_LinkFreqPlan_classes.pas';

{$R *.RES}

 

begin
  Application.Initialize;
  Application.CreateForm(TfrmMain1, frmMain1);
  Application.Run;
end.
