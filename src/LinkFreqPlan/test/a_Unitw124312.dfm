object frmMain1: TfrmMain1
  Left = 393
  Top = 223
  Width = 573
  Height = 395
  Caption = 'frmMain1'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object ToolBar1: TToolBar
    Left = 0
    Top = 0
    Width = 565
    Height = 29
    ButtonHeight = 25
    Caption = 'ToolBar1'
    TabOrder = 0
    object Add: TButton
      Left = 0
      Top = 2
      Width = 73
      Height = 25
      Caption = 'Add'
      TabOrder = 0
    end
    object Button1: TButton
      Left = 73
      Top = 2
      Width = 75
      Height = 25
      Caption = 'Button1'
      TabOrder = 3
    end
    object Import_: TButton
      Left = 148
      Top = 2
      Width = 75
      Height = 25
      Caption = 'Import_'
      TabOrder = 2
    end
    object View: TButton
      Left = 223
      Top = 2
      Width = 75
      Height = 25
      Caption = 'View'
      TabOrder = 1
    end
    object Button2: TButton
      Left = 298
      Top = 2
      Width = 75
      Height = 25
      Caption = 'Button2'
      TabOrder = 4
      OnClick = Button2Click
    end
  end
  object FormStorage1: TFormStorage
    IniFileName = 'Software\Onega\'
    StoredValues = <>
    Left = 420
    Top = 6
  end
end
