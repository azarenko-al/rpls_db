object frmMain1: TfrmMain1
  Left = 393
  Top = 223
  Width = 573
  Height = 395
  Caption = 'frmMain1'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 29
    Width = 565
    Height = 298
    Align = alTop
    Anchors = [akLeft, akTop, akRight, akBottom]
    BevelOuter = bvNone
    BorderWidth = 5
    TabOrder = 0
    object Splitter1: TSplitter
      Left = 185
      Top = 5
      Height = 288
    end
    object PageControl1: TPageControl
      Left = 5
      Top = 5
      Width = 180
      Height = 288
      ActivePage = TabSheet1
      Align = alLeft
      TabOrder = 0
      object TabSheet1: TTabSheet
        Caption = 'TabSheet1'
      end
    end
    object pn_Browser: TPanel
      Left = 188
      Top = 5
      Width = 372
      Height = 288
      Align = alClient
      Caption = 'pn_Browser'
      TabOrder = 1
    end
  end
  object ToolBar1: TToolBar
    Left = 0
    Top = 0
    Width = 565
    Height = 29
    ButtonHeight = 25
    Caption = 'ToolBar1'
    TabOrder = 1
    object Add: TButton
      Left = 0
      Top = 2
      Width = 73
      Height = 25
      Caption = 'Add'
      TabOrder = 0
    end
    object Button1: TButton
      Left = 73
      Top = 2
      Width = 75
      Height = 25
      Caption = 'Button1'
      TabOrder = 3
    end
    object Import_: TButton
      Left = 148
      Top = 2
      Width = 75
      Height = 25
      Caption = 'Import_'
      TabOrder = 2
    end
    object View: TButton
      Left = 223
      Top = 2
      Width = 75
      Height = 25
      Caption = 'View'
      TabOrder = 1
    end
    object Button2: TButton
      Left = 298
      Top = 2
      Width = 75
      Height = 25
      Caption = 'Button2'
      TabOrder = 4
      OnClick = Button2Click
    end
  end
  object FormStorage1: TFormStorage
    IniFileName = 'Software\Onega\'
    StoredValues = <>
    Left = 420
    Top = 6
  end
end
