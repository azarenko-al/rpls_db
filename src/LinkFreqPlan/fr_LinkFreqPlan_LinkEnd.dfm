object frame_LinkFreqPlan_LinkEnd: Tframe_LinkFreqPlan_LinkEnd
  Left = 658
  Top = 205
  AutoScroll = False
  Caption = 'frame_LinkFreqPlan_LinkEnd'
  ClientHeight = 725
  ClientWidth = 996
  Color = clBtnFace
  ParentFont = True
  OldCreateOrder = True
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object TBDock: TTBDock
    Left = 0
    Top = 0
    Width = 996
    Height = 28
    BoundLines = [blTop, blBottom]
    Color = clScrollBar
    object TBToolbar: TTBToolbar
      Left = 102
      Top = 0
      Caption = 'TBToolbar'
      DockPos = 94
      TabOrder = 0
      object TBSubmenuItem1: TTBSubmenuItem
        Caption = #1058#1088#1077#1073#1086#1074#1072#1085#1080#1103' '#1087#1086' '#1082#1086#1083'-'#1074#1091' '#1095#1072#1089#1090#1086#1090
        object TBItem1: TTBItem
          Action = act_Set_Common_requirements
        end
        object TBItem2: TTBItem
          Action = act_Set_Freq_count_by_Fixed
        end
        object TBSeparatorItem1: TTBSeparatorItem
          Visible = False
        end
        object TBItem6: TTBItem
          Action = act_Allow_Freqs
          Visible = False
        end
        object TBItem5: TTBItem
          Action = act_Forbid_Freqs
          Visible = False
        end
      end
      object TBSubmenuItem2: TTBSubmenuItem
        Caption = #1054#1087#1077#1088#1072#1094#1080#1080' '#1089' '#1063#1058#1055
        Options = [tboDropdownArrow]
        object TBItem7: TTBItem
          Action = act_Set_Distribute
        end
        object TBItem8: TTBItem
          Action = act_Clear_Distribute
        end
        object TBItem9: TTBItem
          Action = act_Set_Fixed
        end
        object TBItem10: TTBItem
          Action = act_Clear_Fixed
        end
        object TBSeparatorItem2: TTBSeparatorItem
        end
        object TBItem3: TTBItem
          Action = act_Save_Fixed
        end
        object TBItem12: TTBItem
          Action = act_Restore_Fixed
        end
      end
      object TBSubmenuItem4: TTBSubmenuItem
        Caption = #1048#1085#1090#1077#1088#1092#1077#1088#1077#1085#1094#1080#1103
        Options = [tboDropdownArrow]
        object TBItem11: TTBItem
          Action = act_Calc_Interference
        end
        object tb_distributed: TTBItem
          AutoCheck = True
          Caption = #1087#1086' '#1088#1072#1089#1087#1088'. '#1095#1072#1089#1090#1086#1090#1072#1084
          ImageIndex = 0
          Images = StateImages
          OnClick = TBItemClick
        end
        object tb_fixed: TTBItem
          AutoCheck = True
          Caption = #1087#1086' '#1079#1072#1082#1088'. '#1095#1072#1089#1090#1086#1090#1072#1084
          ImageIndex = 0
          Images = StateImages
          OnClick = TBItemClick
        end
        object TBSeparatorItem3: TTBSeparatorItem
        end
        object TBItem13: TTBItem
          Action = act_CreateMap_Interference
        end
      end
    end
    object TBToolbar2: TTBToolbar
      Left = 0
      Top = 0
      Caption = 'TBToolbar2'
      DockPos = -5
      TabOrder = 1
      object tb_Set_Distr: TTBItem
        Action = act_Set_Distribute
        ImageIndex = 0
        Images = img_Tools
        MaskOptions = [tboLongHintInMenuOnly, tboShowHint]
        Options = [tboLongHintInMenuOnly, tboShowHint]
      end
      object tb_Clear_Distr: TTBItem
        Action = act_Clear_Distribute
        ImageIndex = 2
        Images = img_Tools
        Options = [tboShowHint]
      end
      object tb_Set_Fix: TTBItem
        Action = act_Set_Fixed
        ImageIndex = 3
        Images = img_Tools
        Options = [tboShowHint]
      end
      object tb_Clear_Fix: TTBItem
        Action = act_Clear_Fixed
        ImageIndex = 5
        Images = img_Tools
        Options = [tboShowHint]
      end
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 55
    Width = 996
    Height = 381
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object StatusBar1: TStatusBar
      Left = 0
      Top = 225
      Width = 996
      Height = 19
      Panels = <>
      SimplePanel = True
      SimpleText = 'Rec '
    end
    object PN_BOTTOM: TPanel
      Left = 0
      Top = 244
      Width = 996
      Height = 137
      Align = alBottom
      BevelOuter = bvNone
      TabOrder = 2
      object GroupBox1: TGroupBox
        Left = 394
        Top = 0
        Width = 209
        Height = 137
        Align = alLeft
        Caption = ' '#1048#1085#1090#1077#1088#1092#1077#1088#1077#1085#1094#1080#1103
        TabOrder = 0
        object Button1: TButton
          Left = 6
          Top = 19
          Width = 88
          Height = 21
          Action = act_Calc_Interference
          TabOrder = 0
        end
        object cb_CalcCIA_after_Distrib: TCheckBox
          Left = 8
          Top = 50
          Width = 186
          Height = 17
          Caption = #1056#1072#1089#1095#1077#1090' '#1087#1086#1089#1083#1077' '#1088#1072#1089#1087#1088#1077#1076#1077#1083#1077#1085#1080#1103
          TabOrder = 1
        end
        object cb_Make_CIA_maps: TCheckBox
          Left = 8
          Top = 74
          Width = 185
          Height = 17
          Caption = #1060#1086#1088#1084#1080#1088#1086#1074#1072#1090#1100' '#1082#1072#1088#1090#1099
          Checked = True
          State = cbChecked
          TabOrder = 2
        end
        object cb_Channel_type: TComboBox
          Left = 102
          Top = 19
          Width = 96
          Height = 21
          Style = csDropDownList
          ItemHeight = 13
          ItemIndex = 0
          TabOrder = 3
          Text = #1087#1086' '#1088#1072#1089#1087#1088'. '#1095'.'
          OnChange = cb_Channel_typeChange
          Items.Strings = (
            #1087#1086' '#1088#1072#1089#1087#1088'. '#1095'.'
            #1087#1086' '#1079#1072#1082#1088'. '#1095'.')
        end
      end
      object GroupBox2: TGroupBox
        Left = 170
        Top = 0
        Width = 224
        Height = 137
        Align = alLeft
        Caption = #1054#1087#1077#1088#1072#1094#1080#1080' '#1089' '#1063#1058#1055' '
        TabOrder = 1
        object Button3: TButton
          Left = 8
          Top = 19
          Width = 100
          Height = 21
          Action = act_Set_Distribute
          TabOrder = 0
        end
        object Button4: TButton
          Left = 117
          Top = 19
          Width = 100
          Height = 21
          Action = act_Set_Fixed
          TabOrder = 1
        end
        object Button5: TButton
          Left = 8
          Top = 48
          Width = 100
          Height = 21
          Action = act_Clear_Distribute
          TabOrder = 2
        end
        object Button6: TButton
          Left = 117
          Top = 48
          Width = 100
          Height = 21
          Action = act_Clear_Fixed
          TabOrder = 3
        end
        object Button8: TButton
          Left = 8
          Top = 76
          Width = 209
          Height = 21
          Action = act_Save_to_Excel
          TabOrder = 4
        end
      end
      object GroupBox3: TGroupBox
        Left = 0
        Top = 0
        Width = 170
        Height = 137
        Align = alLeft
        Caption = ' '#1058#1088#1077#1073#1086#1074#1072#1085#1080#1103' '#1087#1086' '#1082#1086#1083'-'#1074#1091' '#1095#1072#1089#1090#1086#1090
        TabOrder = 2
        object Button2: TButton
          Left = 15
          Top = 19
          Width = 140
          Height = 21
          Action = act_Set_Common_requirements
          TabOrder = 0
        end
        object Button10: TButton
          Left = 15
          Top = 48
          Width = 140
          Height = 21
          Action = act_Set_Freq_count_by_Fixed
          TabOrder = 1
        end
      end
    end
    object cxGrid1: TcxGrid
      Left = 0
      Top = 0
      Width = 996
      Height = 177
      Align = alTop
      TabOrder = 1
      LookAndFeel.Kind = lfFlat
      object cxGrid1DBBandedTableView1: TcxGridDBBandedTableView
        PopupMenu = PopupMenu11111
        NavigatorButtons.ConfirmDelete = False
        DataController.DataSource = ds_mem_LinkEnd
        DataController.Filter.MaxValueListCount = 1000
        DataController.KeyFieldNames = 'id'
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        Filtering.ColumnPopup.MaxDropDownItemCount = 12
        OptionsCustomize.ColumnGrouping = False
        OptionsCustomize.ColumnsQuickCustomization = True
        OptionsCustomize.BandsQuickCustomization = True
        OptionsSelection.MultiSelect = True
        OptionsView.GroupByBox = False
        Preview.AutoHeight = False
        Preview.MaxLineCount = 2
        Styles.OnGetContentStyle = cxGrid1DBBandedTableView1StylesGetContentStyle
        Bands = <
          item
            FixedKind = fkLeft
          end
          item
          end
          item
          end
          item
          end
          item
          end
          item
          end
          item
            Caption = 'linkendtype'
            Visible = False
          end
          item
            Caption = #1056#1056' '#1080#1085#1090#1077#1088#1074#1072#1083
          end
          item
            Caption = #1056#1056#1057
            Visible = False
          end
          item
            Caption = #1040#1085#1090#1077#1085#1085#1072
          end>
        object col__Property_Name: TcxGridDBBandedColumn
          DataBinding.FieldName = 'Property_Name'
          PropertiesClassName = 'TcxTextEditProperties'
          Properties.Alignment.Horz = taLeftJustify
          Properties.MaxLength = 0
          Properties.ReadOnly = True
          Options.Editing = False
          Width = 79
          Position.BandIndex = 0
          Position.ColIndex = 2
          Position.RowIndex = 0
        end
        object col__Link_Name: TcxGridDBBandedColumn
          DataBinding.FieldName = 'Link_Name'
          PropertiesClassName = 'TcxTextEditProperties'
          Properties.Alignment.Horz = taLeftJustify
          Properties.MaxLength = 0
          Properties.ReadOnly = True
          Options.Editing = False
          Options.Filtering = False
          Width = 60
          Position.BandIndex = 7
          Position.ColIndex = 1
          Position.RowIndex = 0
        end
        object col__linkend_name: TcxGridDBBandedColumn
          Caption = 'LinkEnd_name'
          DataBinding.FieldName = 'linkend_name'
          PropertiesClassName = 'TcxTextEditProperties'
          Properties.Alignment.Horz = taLeftJustify
          Properties.MaxLength = 0
          Properties.ReadOnly = True
          Options.Editing = False
          Width = 78
          Position.BandIndex = 0
          Position.ColIndex = 1
          Position.RowIndex = 0
        end
        object col__LinkEnd_ID: TcxGridDBBandedColumn
          Tag = -1
          DataBinding.FieldName = 'LinkEnd_ID'
          PropertiesClassName = 'TcxTextEditProperties'
          Properties.Alignment.Horz = taLeftJustify
          Properties.MaxLength = 0
          Properties.ReadOnly = True
          Visible = False
          Options.Editing = False
          Options.Filtering = False
          Options.Grouping = False
          Width = 52
          Position.BandIndex = 7
          Position.ColIndex = 0
          Position.RowIndex = 0
        end
        object col__Length_km: TcxGridDBBandedColumn
          DataBinding.FieldName = 'Length_km'
          PropertiesClassName = 'TcxTextEditProperties'
          Properties.Alignment.Horz = taLeftJustify
          Properties.MaxLength = 0
          Properties.ReadOnly = True
          Options.Editing = False
          Options.Filtering = False
          Width = 33
          Position.BandIndex = 7
          Position.ColIndex = 2
          Position.RowIndex = 0
        end
        object col__freq_req_count: TcxGridDBBandedColumn
          DataBinding.FieldName = 'freq_req_count'
          PropertiesClassName = 'TcxTextEditProperties'
          Properties.Alignment.Horz = taLeftJustify
          Properties.MaxLength = 0
          Properties.ReadOnly = False
          Width = 64
          Position.BandIndex = 0
          Position.ColIndex = 3
          Position.RowIndex = 0
        end
        object col__priority: TcxGridDBBandedColumn
          DataBinding.FieldName = 'priority'
          PropertiesClassName = 'TcxTextEditProperties'
          Properties.Alignment.Horz = taLeftJustify
          Properties.MaxLength = 0
          Properties.ReadOnly = True
          Width = 47
          Position.BandIndex = 0
          Position.ColIndex = 4
          Position.RowIndex = 0
        end
        object col__band: TcxGridDBBandedColumn
          DataBinding.FieldName = 'band'
          PropertiesClassName = 'TcxTextEditProperties'
          Properties.Alignment.Horz = taLeftJustify
          Properties.MaxLength = 0
          Properties.ReadOnly = True
          Options.Editing = False
          Width = 43
          Position.BandIndex = 0
          Position.ColIndex = 5
          Position.RowIndex = 0
        end
        object col__subband: TcxGridDBBandedColumn
          DataBinding.FieldName = 'subband'
          PropertiesClassName = 'TcxTextEditProperties'
          Properties.Alignment.Horz = taLeftJustify
          Properties.MaxLength = 0
          Properties.ReadOnly = True
          Options.Editing = False
          Width = 60
          Position.BandIndex = 0
          Position.ColIndex = 10
          Position.RowIndex = 0
        end
        object col__Freq_Distributed_Count: TcxGridDBBandedColumn
          DataBinding.FieldName = 'Freq_Distributed_Count'
          PropertiesClassName = 'TcxTextEditProperties'
          Properties.Alignment.Horz = taCenter
          Properties.MaxLength = 0
          Properties.ReadOnly = True
          Options.Editing = False
          Width = 72
          Position.BandIndex = 1
          Position.ColIndex = 0
          Position.RowIndex = 0
        end
        object col__Freq_Distributed: TcxGridDBBandedColumn
          DataBinding.FieldName = 'Freq_Distributed'
          PropertiesClassName = 'TcxTextEditProperties'
          Properties.Alignment.Horz = taLeftJustify
          Properties.MaxLength = 0
          Properties.ReadOnly = False
          Styles.OnGetContentStyle = col__Freq_DistributedStylesGetContentStyle
          Width = 89
          Position.BandIndex = 1
          Position.ColIndex = 1
          Position.RowIndex = 0
        end
        object col__freq_distributed_tx: TcxGridDBBandedColumn
          DataBinding.FieldName = 'freq_distributed_tx_str'
          PropertiesClassName = 'TcxTextEditProperties'
          Properties.Alignment.Horz = taLeftJustify
          Properties.MaxLength = 0
          Properties.ReadOnly = True
          Options.Editing = False
          Styles.OnGetContentStyle = col__Freq_DistributedStylesGetContentStyle
          Width = 128
          Position.BandIndex = 1
          Position.ColIndex = 2
          Position.RowIndex = 0
        end
        object col__freq_distributed_rx: TcxGridDBBandedColumn
          DataBinding.FieldName = 'freq_distributed_rx_str'
          PropertiesClassName = 'TcxTextEditProperties'
          Properties.Alignment.Horz = taLeftJustify
          Properties.MaxLength = 0
          Properties.ReadOnly = True
          Options.Editing = False
          Styles.OnGetContentStyle = col__Freq_DistributedStylesGetContentStyle
          Width = 98
          Position.BandIndex = 1
          Position.ColIndex = 3
          Position.RowIndex = 0
        end
        object col__Calc_Order: TcxGridDBBandedColumn
          DataBinding.FieldName = 'Calc_Order'
          PropertiesClassName = 'TcxTextEditProperties'
          Properties.Alignment.Horz = taLeftJustify
          Properties.MaxLength = 0
          Properties.ReadOnly = True
          Options.Editing = False
          Width = 58
          Position.BandIndex = 1
          Position.ColIndex = 5
          Position.RowIndex = 0
        end
        object col__Distributed_Min_raznos: TcxGridDBBandedColumn
          DataBinding.FieldName = 'Distributed_Min_raznos'
          PropertiesClassName = 'TcxTextEditProperties'
          Properties.Alignment.Horz = taLeftJustify
          Properties.MaxLength = 0
          Properties.ReadOnly = True
          Visible = False
          Options.Editing = False
          Options.Filtering = False
          Width = 146
          Position.BandIndex = 1
          Position.ColIndex = 4
          Position.RowIndex = 0
        end
        object col__Freq_Fixed_Count: TcxGridDBBandedColumn
          DataBinding.FieldName = 'Freq_Fixed_Count'
          PropertiesClassName = 'TcxTextEditProperties'
          Properties.Alignment.Horz = taCenter
          Properties.MaxLength = 0
          Properties.ReadOnly = True
          Options.Editing = False
          Width = 111
          Position.BandIndex = 2
          Position.ColIndex = 0
          Position.RowIndex = 0
        end
        object col__Freq_Fixed: TcxGridDBBandedColumn
          DataBinding.FieldName = 'Freq_Fixed'
          PropertiesClassName = 'TcxTextEditProperties'
          Properties.Alignment.Horz = taLeftJustify
          Properties.MaxLength = 0
          Properties.ReadOnly = False
          Options.Filtering = False
          Styles.OnGetContentStyle = col__Freq_FixedStylesGetContentStyle
          Width = 103
          Position.BandIndex = 2
          Position.ColIndex = 1
          Position.RowIndex = 0
        end
        object col__freq_fixed_tx: TcxGridDBBandedColumn
          DataBinding.FieldName = 'freq_fixed_tx'
          PropertiesClassName = 'TcxTextEditProperties'
          Properties.Alignment.Horz = taLeftJustify
          Properties.MaxLength = 0
          Properties.ReadOnly = True
          Options.Editing = False
          Options.Filtering = False
          Width = 114
          Position.BandIndex = 2
          Position.ColIndex = 2
          Position.RowIndex = 0
        end
        object col__freq_fixed_rx: TcxGridDBBandedColumn
          DataBinding.FieldName = 'freq_fixed_rx'
          PropertiesClassName = 'TcxTextEditProperties'
          Properties.Alignment.Horz = taLeftJustify
          Properties.MaxLength = 0
          Properties.ReadOnly = True
          Options.Editing = False
          Options.Filtering = False
          Width = 66
          Position.BandIndex = 2
          Position.ColIndex = 4
          Position.RowIndex = 0
        end
        object col__Fixed_Min_raznos: TcxGridDBBandedColumn
          DataBinding.FieldName = 'Fixed_Min_raznos'
          PropertiesClassName = 'TcxTextEditProperties'
          Properties.Alignment.Horz = taLeftJustify
          Properties.MaxLength = 0
          Properties.ReadOnly = True
          Visible = False
          Options.Editing = False
          Options.Filtering = False
          Width = 61
          Position.BandIndex = 2
          Position.ColIndex = 3
          Position.RowIndex = 0
        end
        object col__Freq_Forbidden: TcxGridDBBandedColumn
          DataBinding.FieldName = 'Freq_Forbidden'
          PropertiesClassName = 'TcxTextEditProperties'
          Properties.Alignment.Horz = taLeftJustify
          Properties.MaxLength = 0
          Properties.ReadOnly = False
          Options.Filtering = False
          Width = 78
          Position.BandIndex = 3
          Position.ColIndex = 0
          Position.RowIndex = 0
        end
        object col__Freq_Allow: TcxGridDBBandedColumn
          DataBinding.FieldName = 'Freq_Allow'
          PropertiesClassName = 'TcxTextEditProperties'
          Properties.Alignment.Horz = taLeftJustify
          Properties.MaxLength = 0
          Properties.ReadOnly = False
          Options.Filtering = False
          Width = 58
          Position.BandIndex = 4
          Position.ColIndex = 0
          Position.RowIndex = 0
        end
        object col__CIA: TcxGridDBBandedColumn
          DataBinding.FieldName = 'CIA'
          PropertiesClassName = 'TcxCurrencyEditProperties'
          Properties.DisplayFormat = ',0.0'
          Options.Editing = False
          Options.Filtering = False
          Width = 23
          Position.BandIndex = 5
          Position.ColIndex = 0
          Position.RowIndex = 0
        end
        object col__CI: TcxGridDBBandedColumn
          DataBinding.FieldName = 'CI'
          PropertiesClassName = 'TcxCurrencyEditProperties'
          Properties.DisplayFormat = ',0.0'
          Options.Editing = False
          Options.Filtering = False
          Width = 20
          Position.BandIndex = 5
          Position.ColIndex = 1
          Position.RowIndex = 0
        end
        object col__CA: TcxGridDBBandedColumn
          DataBinding.FieldName = 'CA'
          PropertiesClassName = 'TcxCurrencyEditProperties'
          Properties.DisplayFormat = ',0.0'
          Options.Editing = False
          Options.Filtering = False
          Width = 20
          Position.BandIndex = 5
          Position.ColIndex = 2
          Position.RowIndex = 0
        end
        object col__threshold_degradation_cia: TcxGridDBBandedColumn
          DataBinding.FieldName = 'threshold_degradation_cia'
          PropertiesClassName = 'TcxCurrencyEditProperties'
          Properties.DisplayFormat = ',0.0'
          Options.Editing = False
          Options.Filtering = False
          Width = 131
          Position.BandIndex = 5
          Position.ColIndex = 3
          Position.RowIndex = 0
        end
        object col__LinkEndType_band_ID: TcxGridDBBandedColumn
          DataBinding.FieldName = 'LinkEndType_band_ID'
          PropertiesClassName = 'TcxTextEditProperties'
          Properties.Alignment.Horz = taLeftJustify
          Properties.MaxLength = 0
          Properties.ReadOnly = True
          Visible = False
          Options.Filtering = False
          Width = 116
          Position.BandIndex = 0
          Position.ColIndex = 9
          Position.RowIndex = 0
        end
        object col__linkFreqPlan_threshold_degradation: TcxGridDBBandedColumn
          DataBinding.FieldName = 'linkFreqPlan_threshold_degradation'
          PropertiesClassName = 'TcxTextEditProperties'
          Properties.Alignment.Horz = taLeftJustify
          Properties.MaxLength = 0
          Properties.ReadOnly = True
          Visible = False
          Options.Filtering = False
          Width = 175
          Position.BandIndex = 0
          Position.ColIndex = 8
          Position.RowIndex = 0
        end
        object col__linkFreqPlan_min_CI_db: TcxGridDBBandedColumn
          DataBinding.FieldName = 'linkFreqPlan_min_CI_db'
          PropertiesClassName = 'TcxTextEditProperties'
          Properties.Alignment.Horz = taLeftJustify
          Properties.MaxLength = 0
          Properties.ReadOnly = True
          Visible = False
          Options.Filtering = False
          Width = 120
          Position.BandIndex = 0
          Position.ColIndex = 7
          Position.RowIndex = 0
        end
        object col__PROPERTY_ID: TcxGridDBBandedColumn
          DataBinding.FieldName = 'PROPERTY_ID'
          PropertiesClassName = 'TcxTextEditProperties'
          Properties.Alignment.Horz = taLeftJustify
          Properties.MaxLength = 0
          Properties.ReadOnly = True
          Visible = False
          Options.Filtering = False
          Width = 82
          Position.BandIndex = 0
          Position.ColIndex = 6
          Position.RowIndex = 0
        end
        object col__linkendtype_name: TcxGridDBBandedColumn
          DataBinding.FieldName = 'linkendtype_name'
          PropertiesClassName = 'TcxTextEditProperties'
          Properties.Alignment.Horz = taLeftJustify
          Properties.MaxLength = 0
          Properties.ReadOnly = True
          Options.Filtering = False
          Width = 92
          Position.BandIndex = 6
          Position.ColIndex = 0
          Position.RowIndex = 0
        end
        object col__linkendtype_band_id1: TcxGridDBBandedColumn
          DataBinding.FieldName = 'linkendtype_band_id'
          PropertiesClassName = 'TcxTextEditProperties'
          Properties.Alignment.Horz = taLeftJustify
          Properties.MaxLength = 0
          Properties.ReadOnly = True
          Options.Editing = False
          Options.Filtering = False
          Width = 104
          Position.BandIndex = 6
          Position.ColIndex = 1
          Position.RowIndex = 0
        end
        object col__linkendtype_mode_id: TcxGridDBBandedColumn
          DataBinding.FieldName = 'linkendtype_mode_id'
          PropertiesClassName = 'TcxTextEditProperties'
          Properties.Alignment.Horz = taLeftJustify
          Properties.MaxLength = 0
          Properties.ReadOnly = True
          Options.Editing = False
          Options.Filtering = False
          Width = 106
          Position.BandIndex = 6
          Position.ColIndex = 2
          Position.RowIndex = 0
        end
        object col__FREQ_MIN_LOW: TcxGridDBBandedColumn
          DataBinding.FieldName = 'FREQ_MIN_LOW'
          PropertiesClassName = 'TcxTextEditProperties'
          Properties.Alignment.Horz = taLeftJustify
          Properties.MaxLength = 0
          Properties.ReadOnly = True
          Options.Editing = False
          Options.Filtering = False
          Width = 92
          Position.BandIndex = 6
          Position.ColIndex = 3
          Position.RowIndex = 0
        end
        object col__CHANNEL_WIDTH: TcxGridDBBandedColumn
          DataBinding.FieldName = 'CHANNEL_WIDTH'
          PropertiesClassName = 'TcxTextEditProperties'
          Properties.Alignment.Horz = taLeftJustify
          Properties.MaxLength = 0
          Properties.ReadOnly = True
          Options.Editing = False
          Options.Filtering = False
          Width = 100
          Position.BandIndex = 6
          Position.ColIndex = 4
          Position.RowIndex = 0
        end
        object col__TXRX_SHIFT: TcxGridDBBandedColumn
          DataBinding.FieldName = 'TXRX_SHIFT'
          PropertiesClassName = 'TcxTextEditProperties'
          Properties.Alignment.Horz = taLeftJustify
          Properties.MaxLength = 0
          Properties.ReadOnly = True
          Options.Editing = False
          Options.Filtering = False
          Width = 72
          Position.BandIndex = 6
          Position.ColIndex = 5
          Position.RowIndex = 0
        end
        object col__CHANNEL_COUNT: TcxGridDBBandedColumn
          DataBinding.FieldName = 'CHANNEL_COUNT'
          PropertiesClassName = 'TcxTextEditProperties'
          Properties.Alignment.Horz = taLeftJustify
          Properties.MaxLength = 0
          Properties.ReadOnly = True
          Options.Editing = False
          Options.Filtering = False
          Width = 101
          Position.BandIndex = 6
          Position.ColIndex = 6
          Position.RowIndex = 0
        end
        object col__BANDWIDTH: TcxGridDBBandedColumn
          DataBinding.FieldName = 'BANDWIDTH'
          PropertiesClassName = 'TcxTextEditProperties'
          Properties.Alignment.Horz = taLeftJustify
          Properties.MaxLength = 0
          Properties.ReadOnly = True
          Options.Editing = False
          Options.Filtering = False
          Width = 73
          Position.BandIndex = 6
          Position.ColIndex = 7
          Position.RowIndex = 0
        end
        object col__BANDWIDTH_TX_3: TcxGridDBBandedColumn
          DataBinding.FieldName = 'BANDWIDTH_TX_3'
          PropertiesClassName = 'TcxTextEditProperties'
          Properties.Alignment.Horz = taLeftJustify
          Properties.MaxLength = 0
          Properties.ReadOnly = True
          Options.Editing = False
          Options.Filtering = False
          Width = 105
          Position.BandIndex = 6
          Position.ColIndex = 8
          Position.RowIndex = 0
        end
        object col__BANDWIDTH_TX_30: TcxGridDBBandedColumn
          DataBinding.FieldName = 'BANDWIDTH_TX_30'
          PropertiesClassName = 'TcxTextEditProperties'
          Properties.Alignment.Horz = taLeftJustify
          Properties.MaxLength = 0
          Properties.ReadOnly = True
          Options.Editing = False
          Options.Filtering = False
          SortIndex = 0
          SortOrder = soAscending
          Width = 111
          Position.BandIndex = 6
          Position.ColIndex = 9
          Position.RowIndex = 0
        end
        object col_channel_type: TcxGridDBBandedColumn
          DataBinding.FieldName = 'channel_type'
          Options.Editing = False
          Width = 30
          Position.BandIndex = 0
          Position.ColIndex = 11
          Position.RowIndex = 0
        end
        object col_channel_number: TcxGridDBBandedColumn
          Caption = #1050#1072#1085#1072#1083
          DataBinding.FieldName = 'channel_number'
          Options.Filtering = False
          Position.BandIndex = 8
          Position.ColIndex = 1
          Position.RowIndex = 0
        end
        object col_TX_FREQ_MHZ: TcxGridDBBandedColumn
          DataBinding.FieldName = 'TX_FREQ_MHZ'
          Options.Filtering = False
          Position.BandIndex = 8
          Position.ColIndex = 2
          Position.RowIndex = 0
        end
        object col_RX_FREQ_MHZ: TcxGridDBBandedColumn
          DataBinding.FieldName = 'RX_FREQ_MHZ'
          Options.Filtering = False
          Position.BandIndex = 8
          Position.ColIndex = 3
          Position.RowIndex = 0
        end
        object col_LinkEndType_name: TcxGridDBBandedColumn
          DataBinding.FieldName = 'LinkEndType_name'
          Position.BandIndex = 8
          Position.ColIndex = 0
          Position.RowIndex = 0
        end
        object col_ID: TcxGridDBBandedColumn
          DataBinding.FieldName = 'id'
          Visible = False
          Options.Filtering = False
          Options.GroupFooters = False
          Options.Grouping = False
          Width = 34
          Position.BandIndex = 0
          Position.ColIndex = 0
          Position.RowIndex = 0
        end
        object col_lat: TcxGridDBBandedColumn
          DataBinding.FieldName = 'lat'
          Position.BandIndex = 9
          Position.ColIndex = 1
          Position.RowIndex = 0
        end
        object col_lon: TcxGridDBBandedColumn
          DataBinding.FieldName = 'lon'
          Position.BandIndex = 9
          Position.ColIndex = 0
          Position.RowIndex = 0
        end
        object col_location_type: TcxGridDBBandedColumn
          DataBinding.FieldName = 'location_type'
          Position.BandIndex = 9
          Position.ColIndex = 2
          Position.RowIndex = 0
        end
      end
      object cxGrid1Level1: TcxGridLevel
        GridView = cxGrid1DBBandedTableView1
      end
    end
  end
  object PopupMenu11111: TPopupMenu
    Left = 632
    Top = 452
    object N9: TMenuItem
      Caption = #1058#1088#1077#1073#1086#1074#1072#1085#1080#1103
      object N10: TMenuItem
        Action = act_Set_Common_requirements
      end
      object N11: TMenuItem
        Action = act_Set_Freq_count_by_Fixed
      end
    end
    object N1: TMenuItem
      Action = act_Allow_Freqs
      Visible = False
    end
    object N2: TMenuItem
      Action = act_Forbid_Freqs
      Visible = False
    end
    object N17: TMenuItem
      Caption = '-'
    end
    object N6: TMenuItem
      Caption = #1063#1072#1089#1090#1086#1090#1099
      object N12: TMenuItem
        Action = act_Set_Distribute
      end
      object N13: TMenuItem
        Action = act_Clear_Distribute
      end
      object N14: TMenuItem
        Caption = '-'
      end
      object N15: TMenuItem
        Action = act_Set_Fixed
      end
      object N16: TMenuItem
        Action = act_Clear_Fixed
      end
    end
    object N3: TMenuItem
      Caption = '-'
    end
    object N4: TMenuItem
      Action = act_Set_Priority
    end
    object N5: TMenuItem
      Caption = '-'
    end
    object actLinkendSethigh1: TMenuItem
      Action = act_Linkend_Set_high
    end
    object actLinkendSetlow1: TMenuItem
      Action = act_Linkend_Set_low
    end
    object N18: TMenuItem
      Caption = '-'
    end
    object actGetLinkendType1: TMenuItem
      Action = act_Get_LinkendType
    end
  end
  object ActionList111: TActionList
    Images = img_Tools
    OnUpdate = ActionList111Update
    Left = 528
    Top = 452
    object act_Set_Common_requirements: TAction
      Caption = #1054#1073#1097#1080#1077' '#1090#1088#1077#1073#1086#1074#1072#1085#1080#1103
      OnExecute = _All_Actions
    end
    object act_Set_Freq_count_by_Fixed: TAction
      Caption = #1055#1086' '#1079#1072#1082#1088#1077#1087#1083#1077#1085#1085#1099#1084
      Hint = #1059#1089#1090#1072#1085#1086#1074#1080#1090#1100' '#1090#1088#1077#1073' '#1087#1086' '#1079#1072#1082#1088#1077#1087#1083#1077#1085#1085#1099#1084
      OnExecute = _All_Actions
    end
    object act_Forbid_Freqs: TAction
      Caption = #1047#1072#1087#1088#1077#1090#1080#1090#1100' '#1095#1072#1089#1090#1086#1090#1099
      OnExecute = _All_Actions
    end
    object act_Allow_Freqs: TAction
      Caption = #1056#1072#1079#1088#1077#1096#1080#1090#1100' '#1095#1072#1089#1090#1086#1090#1099
      OnExecute = _All_Actions
    end
    object act_Set_Priority: TAction
      Caption = #1059#1089#1090#1072#1085#1086#1074#1080#1090#1100' '#1087#1088#1080#1086#1088#1080#1090#1077#1090
      Hint = #1059#1089#1090#1072#1085#1086#1074#1080#1090#1100' '#1087#1088#1080#1086#1088#1080#1090#1077#1090
      OnExecute = _All_Actions
    end
    object act_Set_Traffic: TAction
      Caption = #1059#1089#1090#1072#1085#1086#1074#1080#1090#1100' '#1090#1088#1072#1092#1080#1082
      OnExecute = _All_Actions
    end
    object act_Set_Distribute: TAction
      Caption = #1056#1072#1089#1087#1088#1077#1076#1077#1083#1080#1090#1100
      Hint = #1059#1089#1090#1072#1085#1086#1074#1080#1090#1100' '#1088#1072#1089#1087#1088#1077#1076#1077#1083#1077#1085#1085#1099#1077' '#1095#1072#1089#1090#1086#1090#1099
      OnExecute = _All_Actions
    end
    object act_Clear_Distribute: TAction
      Caption = #1054#1095#1080#1089#1090#1080#1090#1100' '#1088#1072#1089#1087#1088'.'
      Hint = #1054#1095#1080#1089#1090#1080#1090#1100' '#1088#1072#1089#1087#1088#1077#1076#1077#1083#1077#1085#1085#1099#1077' '#1095#1072#1089#1090#1086#1090#1099
      OnExecute = _All_Actions
    end
    object act_Set_Fixed: TAction
      Caption = #1047#1072#1082#1088#1077#1087#1080#1090#1100
      Hint = #1059#1089#1090#1072#1085#1086#1074#1080#1090#1100' '#1079#1072#1082#1088#1077#1087#1083#1077#1085#1085#1099#1077' '#1095#1072#1089#1090#1086#1090#1099
      OnExecute = _All_Actions
    end
    object act_Clear_Fixed: TAction
      Caption = #1054#1095#1080#1089#1090#1080#1090#1100' '#1079#1072#1082#1088'.'
      Hint = #1054#1095#1080#1089#1090#1080#1090#1100' '#1079#1072#1082#1088#1077#1087#1083#1077#1085#1085#1099#1077' '#1095#1072#1089#1090#1086#1090#1099
      OnExecute = _All_Actions
    end
    object act_Interference: TAction
      Caption = #1056#1072#1089#1095#1077#1090' '#1080#1085#1090#1077#1088#1092#1077#1088#1077#1085#1094#1080#1080
      OnExecute = _All_Actions
    end
    object act_Link_del1: TAction
      Caption = 'Link_Delete'
      OnExecute = _All_Actions
    end
    object act_Link_Add1: TAction
      Caption = 'Link_Add'
      OnExecute = _All_Actions
    end
    object act_Save_Fixed: TAction
      Caption = #1057#1086#1093#1088#1072#1085#1080#1090#1100' '#1079#1072#1082#1088#1077#1087#1083#1077#1085#1085#1099#1077' '#1095#1072#1089#1090#1086#1090#1099' '#1074' '#1056#1056#1057
      Hint = #1057#1086#1093#1088#1072#1085#1080#1090#1100' '#1079#1072#1082#1088#1077#1087#1083#1077#1085#1085#1099#1077' '#1095#1072#1089#1090#1086#1090#1099' '#1074' '#1056#1056#1057
      OnExecute = _All_Actions
    end
    object act_Calc_Interference: TAction
      Caption = #1056#1072#1089#1095#1077#1090
      OnExecute = _All_Actions
    end
    object act_Restore_Fixed: TAction
      Caption = #1042#1086#1089#1089#1090#1072#1085#1086#1074#1080#1090#1100' '#1079#1072#1082#1088#1077#1087#1083#1077#1085#1085#1099#1077' '#1095#1072#1089#1090#1086#1090#1099' '#1080#1079' '#1056#1056#1057
      OnExecute = _All_Actions
    end
    object act_CreateMap_Interference: TAction
      Caption = #1057#1092#1086#1088#1084#1080#1088#1086#1074#1072#1090#1100' '#1082#1072#1088#1090#1099
      OnExecute = _All_Actions
    end
    object act_Save_to_Excel: TAction
      Caption = #1057#1086#1093#1088#1072#1085#1080#1090#1100' '#1074' Excel'
      OnExecute = _All_Actions
    end
    object act_Linkend_Set_high: TAction
      Category = 'Linkend'
      Caption = 'act_Linkend_Set_high'
      OnExecute = _All_Actions
    end
    object act_Linkend_Set_low: TAction
      Category = 'Linkend'
      Caption = 'act_Linkend_Set_low'
      OnExecute = _All_Actions
    end
    object act_Get_LinkendType: TAction
      Caption = 'act_Get_LinkendType'
      OnExecute = _All_Actions
    end
  end
  object ds_mem_LinkEnd: TDataSource
    DataSet = mem_LinkEnd1
    Left = 200
    Top = 440
  end
  object mem_LinkEnd1: TdxMemData
    Indexes = <>
    SortOptions = []
    Left = 200
    Top = 500
  end
  object StateImages: TImageList
    Left = 38
    Top = 439
    Bitmap = {
      494C010103000400040010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000001000000001002000000000000010
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000848484008484
      8400848484008484840084848400848484008484840084848400848484008484
      8400848484008484840000000000000000000000000000000000848484008484
      8400848484008484840084848400848484008484840084848400848484008484
      8400848484008484840000000000000000000000000000000000848484008484
      8400848484008484840084848400848484008484840084848400848484008484
      8400848484008484840000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000008484840000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000008484840000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000008484840000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFC6C600FFC6
      C600FFC6C600FFC6C600FFC6C600FFC6C600FFC6C600FFC6C600FFC6C600FFC6
      C600000000008484840000000000000000000000000000000000FFC6C600FFC6
      C600FFC6C600FFC6C600FFC6C600FFC6C600FFC6C600FFC6C600FFC6C600FFC6
      C600000000008484840000000000000000000000000000000000FFC6C600FFC6
      C600FFC6C600FFC6C600FFC6C600FFC6C600FFC6C600FFC6C600FFC6C600FFC6
      C600000000008484840000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFC6C6000000
      000000000000000000000000000000000000000000000000000000000000FFC6
      C600000000008484840000000000000000000000000000000000FFC6C6000000
      00000000000000000000FF00000000000000000000000000000000000000FFC6
      C600000000008484840000000000000000000000000000000000FFC6C6000000
      000000000000000000000000000000000000000000000000000000000000FFC6
      C600000000008484840000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFC6C6000000
      000000000000000000000000000000000000000000000000000000000000FFC6
      C600000000008484840000000000000000000000000000000000FFC6C6000000
      000000000000FF000000FF000000FF000000000000000000000000000000FFC6
      C600000000008484840000000000000000000000000000000000FFC6C6000000
      000000000000000000000000000000000000000000000000000000000000FFC6
      C600000000008484840000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFC6C6000000
      000000000000000000000000000000000000000000000000000000000000FFC6
      C600000000008484840000000000000000000000000000000000FFC6C6000000
      0000FF000000FF000000FF000000FF000000FF0000000000000000000000FFC6
      C600000000008484840000000000000000000000000000000000FFC6C6000000
      000000000000000000000000000000000000000000000000000000000000FFC6
      C600000000008484840000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFC6C6000000
      000000000000000000000000000000000000000000000000000000000000FFC6
      C600000000008484840000000000000000000000000000000000FFC6C600FF00
      0000FF000000FF00000000000000FF000000FF000000FF00000000000000FFC6
      C600000000008484840000000000000000000000000000000000FFC6C6000000
      000000000000000000000000000000000000000000000000000000000000FFC6
      C600000000008484840000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFC6C6000000
      000000000000000000000000000000000000000000000000000000000000FFC6
      C600000000008484840000000000000000000000000000000000FFC6C600FF00
      0000FF000000000000000000000000000000FF000000FF000000FF000000FFC6
      C600000000008484840000000000000000000000000000000000FFC6C6000000
      000000000000000000000000000000000000000000000000000000000000FFC6
      C600000000008484840000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFC6C6000000
      000000000000000000000000000000000000000000000000000000000000FFC6
      C600000000008484840000000000000000000000000000000000FFC6C6000000
      00000000000000000000000000000000000000000000FF000000FF000000FFC6
      C600000000008484840000000000000000000000000000000000FFC6C6000000
      000000000000000000000000000000000000000000000000000000000000FFC6
      C600000000008484840000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFC6C6000000
      000000000000000000000000000000000000000000000000000000000000FFC6
      C600000000008484840000000000000000000000000000000000FFC6C6000000
      0000000000000000000000000000000000000000000000000000FF000000FFC6
      C600000000008484840000000000000000000000000000000000FFC6C6000000
      000000000000000000000000000000000000000000000000000000000000FFC6
      C600000000008484840000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFC6C6000000
      000000000000000000000000000000000000000000000000000000000000FFC6
      C600000000008484840000000000000000000000000000000000FFC6C6000000
      000000000000000000000000000000000000000000000000000000000000FFC6
      C600000000008484840000000000000000000000000000000000FFC6C6000000
      000000000000000000000000000000000000000000000000000000000000FFC6
      C600000000008484840000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFC6C600FFC6
      C600FFC6C600FFC6C600FFC6C600FFC6C600FFC6C600FFC6C600FFC6C600FFC6
      C600000000008484840000000000000000000000000000000000FFC6C600FFC6
      C600FFC6C600FFC6C600FFC6C600FFC6C600FFC6C600FFC6C600FFC6C600FFC6
      C600000000008484840000000000000000000000000000000000FFC6C600FFC6
      C600FFC6C600FFC6C600FFC6C600FFC6C600FFC6C600FFC6C600FFC6C600FFC6
      C600000000008484840000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000100000000100010000000000800000000000000000000000
      000000000000000000000000FFFFFF00FFFFFFFFFFFF0000FFFFFFFFFFFF0000
      C003C003C0030000800380038003000080038003800300009FE39DE39FE30000
      9FE398E39FE300009FE390639FE300009FE382239FE300009FE387039FE30000
      9FE39F839FE300009FE39FC39FE300009FE39FE39FE300008003800380030000
      8007800780070000FFFFFFFFFFFF000000000000000000000000000000000000
      000000000000}
  end
  object img_Tools: TImageList
    Left = 40
    Top = 500
    Bitmap = {
      494C010108000900040010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000003000000001002000000000000030
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000FF000000FF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000FF000000FF000000FF000000FF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000FF000000FF000000FF000000FF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000FF000000FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000FF000000FF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000FF000000FF00000000000000FF000000FF000000FF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000FF000000FF000000FF000000FF000000FF000000FF000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000021000000FF00000021000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000FF000000
      FF00000000000000000000000000000000000000000000000000000000000000
      00000000FF000000FF000000000000000000000000000000FF000000FF000000
      FF00000000000000000000000000000000000000000000000000000000000000
      0000000021000000FF000000FF0000000000000000000000FF000000FF000000
      FF00000000000000000000000000000000000000000000000000000000000000
      00000000FF000000FF0000002100000000000000000000000000000000000000
      0000000000000000000000FF000000FF000000FF000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      FF000000FF000000000000000000000000000000000000000000000000000000
      FF000000FF0000000000000000000000000000000000000000000000FF000000
      FF000000FF000000000000000000000000000000000000000000000000000000
      FF000000FF000000FF00000000000000000000000000000000000000FF000000
      FF000000FF000000000000000000000000000000000000000000000000000000
      FF000000FF000000210000000000000000000000000000000000000000000000
      00000000000000FF000000FF000000FF000000FF000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000FF000000FF00000000000000000000000000000000000000FF000000
      FF00000000000000000000000000000000000000000000000000000000000000
      21000000FF000000FF0000000000000000000000000000000000000021000000
      FF00000021000000000000000000000000000000000000000000000000000000
      FF000000FF0000002100000000000000000000000000000000000000FF000000
      FF00000021000000000000000000000000000000000000000000000000000000
      000000FF000000FF000000FF00000000000000FF000000FF0000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000FF000000FF0000000000000000000000FF000000FF000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000FF000000FF000000FF0000000000000000000000FF000000FF000000
      FF00000000000000000000000000000000000000000000000000000000000000
      0000000021000000FF000000210000000000000000000000FF000000FF000000
      2100000000000000000000000000000000000000000000000000000000000021
      000000FF000000FF0000000000000000000000FF000000FF0000001000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000FF000000FF000000FF000000FF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000FF000000FF00000021000000FF000000FF00000021000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000FF000000FF000000FF000000FF000000FF00000021000000
      00000000000000000000000000000000000000000000000000000000000000FF
      000000FF00000000000000000000000000000000000000FF000000FF00000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FF000000FF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000FF000000FF000000FF000000FF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000FF000000FF000000FF0000002100000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000FF00000052
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000FF000000FF000000FF000000FF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000021000000FF000000FF0000002100000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000FF000000FF000000FF0000002100000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000FF000000FF
      000000FF00000000000000000000000000000000000000000000000000000000
      0000000000000000FF000000FF0000000000000000000000FF000000FF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000FF000000FF00000021000000FF000000FF000000FF000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000021000000FF000000FF000000FF000000FF000000FF000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000000FF
      000000AD00000000000000000000000000000000000000000000000000000000
      00000000FF000000FF00000000000000000000000000000000000000FF000000
      FF00000000000000000000000000000000000000000000000000000000000000
      00000000FF000000FF000000FF0000000000000000000000FF000000FF000000
      2100000000000000000000000000000000000000000000000000000000000000
      00000000FF000000FF000000FF0000000000000000000000FF000000FF000000
      FF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000FF000000FF000000000000000000000000000000000000000000000000
      FF000000FF000000000000000000000000000000000000000000000000000000
      FF000000FF000000000000000000000000000000000000000000000000000000
      21000000FF000000FF0000000000000000000000000000000000000021000000
      FF000000FF000000000000000000000000000000000000000000000000000000
      FF000000FF000000FF00000000000000000000000000000000000000FF000000
      FF000000FF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000FF000000FF0000000000000000000000000000000000000000FF000000
      FF00000000000000000000000000000000000000000000000000000000000000
      00000000FF000000FF00000000000000000000000000000000000000FF000000
      FF000000FF000000000000000000000000000000000000000000000000000000
      21000000FF000000FF00000000000000000000000000000000000000FF000000
      FF000000FF000000000000000000000000000000000000000000000000000000
      FF000000FF000000FF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000FF00000000000000000000000000000000FF000000FF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000FF000000FF0000000000000000000000FF000000FF000000
      2100000000000000000000000000000000000000000000000000000000000000
      0000000021000000FF000000FF000000000000000000000021000000FF000000
      2100000000000000000000000000000000000000000000000000000000000000
      00000000FF000000FF000000FF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000FF000000FF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000FF000000FF000000FF000000FF000000FF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000FF000000FF000000FF000000FF000000FF000000FF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000FF000000FF00000021000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000FF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FF000000FF000000FF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000FF000000FF000000FF000000FF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000210000007B000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000FF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FF000000FF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000FF000000FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000FF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000FF00000000000000FF000000FF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000FF000000FF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000FF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000FF000000000000000000000000000000FF000000FF000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000FF000000FF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      FF00000000000000000000000000000000000000000000000000000000000000
      00000000FF0000000000000000000000000000000000000000000000FF000000
      FF00000000000000000000000000000000000000000000000000000000000000
      FF000000FF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000FF000000000000000000000000000000000000000000000000000000
      FF00000000000000000000000000000000000000000000000000000000000000
      FF000000FF0000000000000000000000000000000000000000000000FF000000
      FF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000FF00000000000000000000000000000000000000FF000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000FF000000FF000000000000000000000000000000FF000000FF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000FF0000000000000000000000FF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000FF000000FF00000000000000FF000000FF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FF000000FF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000FF000000FF000000FF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FF000000FF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000FF000000FF000000FF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000FF0000000000000000000000FF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000FF000000FF00000000000000FF000000FF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000FF00000000000000000000000000000000000000FF000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000FF000000FF000000000000000000000000000000FF000000FF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000FF000000000000000000000000000000000000000000000000000000
      FF00000000000000000000000000000000000000000000000000000000000000
      FF000000FF0000000000000000000000000000000000000000000000FF000000
      FF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      FF00000000000000000000000000000000000000000000000000000000000000
      00000000FF0000000000000000000000000000000000000000000000FF000000
      FF00000000000000000000000000000000000000000000000000000000000000
      FF000000FF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000FF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000FF000000000000000000000000000000FF000000FF000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000FF000000FF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000FF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000FF00000000000000FF000000FF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000FF000000FF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000FF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FF000000FF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000FF000000FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000300000000100010000000000800100000000000000000000
      000000000000000000000000FFFFFF0000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FEFF3EFC3EFC3FFCFEFF9EF91EF81FF8
      DEF7CEF38EF10DF0EC77E6E7C6E385E2C047C2C7C2C701C7E017E897E087B18F
      C217C417C017101BE69FFE7FFC3FFC39000000000000FC00FEC7F89FF81FA019
      EEC7E2C7E087A18BEED3E6C7E2C7A3C7EEE3CEE3C6E303E3CEF38EF18EF183F0
      EEC72EC40EC01FF8FEFF7EFE3EFC3FF8FFFF7FFE7FFCFEFFFFF8BFF83FF8FEFF
      7DFC5DF81DF0DEF7BDFAADF28DE2EEF711F711E701C7C6C7B5EFB1CFB18FEED7
      15DB159B111BC6D7FFB9FE39FC39FEFFFF00FE00FC000000A3B9A1B9A139FEFF
      AFDBABDBA39BEEC7B7EFB7EFA7CFEEDF3BF72BF70BE7EEEFA3FA83FA83F2CEF7
      FFFCBFFC3FF8EEC7FFF87FF87FF8FEFF00000000000000000000000000000000
      000000000000}
  end
  object ADOQuery1: TADOQuery
    Parameters = <>
    Left = 432
    Top = 455
  end
  object SaveDialog1: TSaveDialog
    Filter = '*.xls|*.xls'
    Left = 744
    Top = 456
  end
  object cxStyleRepository1: TcxStyleRepository
    Left = 896
    Top = 456
    PixelsPerInch = 96
    object cxStyle_Red: TcxStyle
      AssignedValues = [svTextColor]
      TextColor = clRed
    end
    object cxStyle1: TcxStyle
    end
  end
  object dxBarManager1: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = []
    Categories.Strings = (
      'Default')
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    LookAndFeel.Kind = lfFlat
    NotDocking = [dsNone, dsLeft, dsTop, dsRight]
    PopupMenuLinks = <>
    SunkenBorder = True
    UseSystemFont = True
    Left = 320
    Top = 456
    DockControlHeights = (
      0
      0
      27
      0)
    object dxBarManager1Bar1: TdxBar
      AllowQuickCustomizing = False
      Caption = 'Custom 1'
      CaptionButtons = <>
      DockedDockingStyle = dsTop
      DockedLeft = 0
      DockedTop = 0
      DockingStyle = dsTop
      FloatLeft = 801
      FloatTop = 229
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarSubItem4'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarSubItem2'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarSubItem3'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarSubItem1'
        end>
      OneOnRow = True
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarButton1: TdxBarButton
      Action = act_Calc_Interference
      Category = 0
    end
    object dxBarButton2: TdxBarButton
      Caption = #1087#1086' '#1088#1072#1089#1087#1088'. '#1095#1072#1089#1090#1086#1090#1072#1084
      Category = 0
      Hint = #1087#1086' '#1088#1072#1089#1087#1088'. '#1095#1072#1089#1090#1086#1090#1072#1084
      Visible = ivAlways
    end
    object dxBarSubItem1: TdxBarSubItem
      Caption = #1048#1085#1090#1077#1088#1092#1077#1088#1077#1085#1094#1080#1103
      Category = 0
      Visible = ivAlways
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarButton1'
        end
        item
          Visible = True
          ItemName = 'dxBarButton2'
        end
        item
          Visible = True
          ItemName = 'dxBarButton17'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarButton16'
        end>
    end
    object dxBarSubItem2: TdxBarSubItem
      Caption = #1058#1088#1077#1073#1086#1074#1072#1085#1080#1103' '#1087#1086' '#1082#1086#1083'-'#1074#1091' '#1095#1072#1089#1090#1086#1090
      Category = 0
      Visible = ivAlways
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarButton6'
        end
        item
          Visible = True
          ItemName = 'dxBarButton8'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarButton7'
        end
        item
          Visible = True
          ItemName = 'dxBarButton12'
        end>
    end
    object dxBarSubItem3: TdxBarSubItem
      Caption = #1054#1087#1077#1088#1072#1094#1080#1080' '#1089' '#1063#1058#1055
      Category = 0
      Visible = ivAlways
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarButton3'
        end
        item
          Visible = True
          ItemName = 'dxBarButton5'
        end
        item
          Visible = True
          ItemName = 'dxBarButton4'
        end
        item
          Visible = True
          ItemName = 'dxBarButton13'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarButton14'
        end
        item
          Visible = True
          ItemName = 'dxBarButton15'
        end>
    end
    object dxBarButton3: TdxBarButton
      Action = act_Set_Distribute
      Category = 0
    end
    object dxBarButton4: TdxBarButton
      Action = act_Set_Fixed
      Category = 0
    end
    object dxBarButton5: TdxBarButton
      Action = act_Clear_Distribute
      Category = 0
    end
    object dxBarButton6: TdxBarButton
      Action = act_Set_Common_requirements
      Category = 0
    end
    object dxBarButton7: TdxBarButton
      Action = act_Allow_Freqs
      Category = 0
    end
    object dxBarButton8: TdxBarButton
      Action = act_Set_Freq_count_by_Fixed
      Category = 0
    end
    object dxBarSubItem4: TdxBarSubItem
      Caption = #1063#1072#1089#1090#1086#1090#1099
      Category = 0
      Visible = ivAlways
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarButton9'
        end
        item
          Visible = True
          ItemName = 'dxBarButton11'
        end
        item
          Visible = True
          ItemName = 'dxBarButton10'
        end
        item
          Visible = True
          ItemName = 'dxBarButton18'
        end>
    end
    object dxBarButton9: TdxBarButton
      Action = act_Set_Distribute
      Category = 0
    end
    object dxBarButton10: TdxBarButton
      Action = act_Set_Fixed
      Category = 0
    end
    object dxBarButton11: TdxBarButton
      Action = act_Clear_Distribute
      Category = 0
    end
    object dxBarButton12: TdxBarButton
      Action = act_Forbid_Freqs
      Category = 0
    end
    object dxBarButton13: TdxBarButton
      Action = act_Clear_Fixed
      Category = 0
    end
    object dxBarButton14: TdxBarButton
      Action = act_Save_Fixed
      Category = 0
    end
    object dxBarButton15: TdxBarButton
      Action = act_Restore_Fixed
      Category = 0
    end
    object dxBarButton16: TdxBarButton
      Action = act_CreateMap_Interference
      Category = 0
    end
    object dxBarButton17: TdxBarButton
      Caption = #1087#1086' '#1079#1072#1082#1088'. '#1095#1072#1089#1090#1086#1090#1072#1084
      Category = 0
      Hint = #1087#1086' '#1079#1072#1082#1088'. '#1095#1072#1089#1090#1086#1090#1072#1084
      Visible = ivAlways
    end
    object dxBarButton18: TdxBarButton
      Action = act_Clear_Fixed
      Category = 0
    end
  end
end
