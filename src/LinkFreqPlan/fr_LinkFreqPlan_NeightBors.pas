unit fr_LinkFreqPlan_NeightBors;

interface

uses
 Windows, SysUtils, Classes, Graphics, Controls, Forms,
  ExtCtrls,   Db,  cxGraphics, cxTextEdit, cxTLdxBarBuiltInMenu, dxSkinsCore,
  ADODB, 
  ActnList, Menus,  cxStyles, cxTL,  cxPropertiesStore, ToolWin,
  ComCtrls, cxInplaceContainer, cxDBTL, cxControls, cxTLData,
  dxmdaset, cxLookAndFeels, cxLookAndFeelPainters, cxCustomData,


  u_Storage,

//  dm_Language,

 // dm_Main,
 // f_Custom,


   u_LinkFreqPlan_const,


  dm_Onega_DB_data,

  dm_LinkFreqPlan_NeighBors_view,

//  dm_LinkFreqPlan,
  dm_LinkFreqPlan_Tools,

  u_Func_arrays,
  u_func,
  u_db,
  u_cx,
//  u_dx,

  u_reg,
  u_dlg,

   dxSkinsDefaultPainters, Grids, DBGrids, rxPlacemnt, dxBar, cxClasses
  ;


type
  Tframe_LinkFreqPlan_NeightBors = class(TForm)
    qry_NeighBours1111: TADOQuery;
    ds_Items: TDataSource;
    mem_Items: TdxMemData;
    Panel1: TPanel;
    pnLegend1: TPanel;
    pnLegend2: TPanel;
    pnLegend3: TPanel;
    ActionList11111: TActionList;
    act_Neighbors_calc: TAction;
    act_Calc_CI: TAction;
    act_Expand: TAction;
    act_Collapse: TAction;
    PopupMenu1: TPopupMenu;
    actExpand1: TMenuItem;
    actCollapse1: TMenuItem;
    act_Setup_Grid: TAction;
    act_Clear_Extra_Loss: TAction;
    N1: TMenuItem;
    N2: TMenuItem;
    cxStyleRepository1: TcxStyleRepository;
    cxStyle_red: TcxStyle;
    cxStyle_LINK: TcxStyle;
    cxStyle_PROP: TcxStyle;
    cxDBTreeList1: TcxDBTreeList;
    col__tree_id: TcxDBTreeListColumn;
    col__tree_parent_id: TcxDBTreeListColumn;
    col__TX_LinkEnd_Name: TcxDBTreeListColumn;
    col__RX_LinkEnd_Name: TcxDBTreeListColumn;
    col__Band: TcxDBTreeListColumn;
    col__CI: TcxDBTreeListColumn;
    col__Rx_level_dBm: TcxDBTreeListColumn;
    col__Neighbour_Count: TcxDBTreeListColumn;
    col__is_expanded: TcxDBTreeListColumn;
    col__type: TcxDBTreeListColumn;
    col__Extra_Loss: TcxDBTreeListColumn;
    col__CIA: TcxDBTreeListColumn;
    col__CI_: TcxDBTreeListColumn;
    col__CA: TcxDBTreeListColumn;
    col__threshold_degradation: TcxDBTreeListColumn;
    col__threshold_degradation_cia: TcxDBTreeListColumn;
    col__Distance_M: TcxDBTreeListColumn;
    col__TX_LinkEnd_ID: TcxDBTreeListColumn;
    col__LinkEnd_ID: TcxDBTreeListColumn;
    col__POWER_CIA: TcxDBTreeListColumn;
    ADOStoredProc1: TADOStoredProc;
    DataSource1: TDataSource;
    FormStorage1: TFormStorage;
    col_FLD_DIAGRAM_LEVEL: TcxDBTreeListColumn;
    col_Azimuth: TcxDBTreeListColumn;
    col_TX_LinkEnd_id: TcxDBTreeListColumn;
    col_RX_LinkEnd_id: TcxDBTreeListColumn;
    col_Freq_Spacing_MHz: TcxDBTreeListColumn;
    col_POWER_noise: TcxDBTreeListColumn;
    col_INTERFERENCE: TcxDBTreeListColumn;
    dxBarManager1: TdxBarManager;
    dxBarManager1Bar1: TdxBar;
    dxBarButton1: TdxBarButton;
    dxBarButton2: TdxBarButton;
    dxBarButton3: TdxBarButton;
    procedure cxDBTreeList11Collapsing(Sender: TObject; ANode: TcxTreeListNode; var Allow: Boolean);
    procedure cxDBTreeList11CustomDrawCell(Sender: TObject; ACanvas: TcxCanvas; AViewInfo:
        TcxTreeListEditCellViewInfo; var ADone: Boolean);
    procedure cxDBTreeList11Editing(Sender: TObject; AColumn: TcxTreeListColumn; var Allow: Boolean);
(*    procedure dxDBTreeListCustomDrawCell(Sender: TObject;
      ACanvas: TCanvas; ARect: TRect; ANode: TdxTreeListNode;
      AColumn: TdxTreeListColumn; ASelected, AFocused,
      ANewItemRow: Boolean; var AText: String; var AColor: TColor;
      AFont: TFont; var AAlignment: TAlignment; var ADone: Boolean);
*)
   procedure FormCreate(Sender: TObject);
//    procedure dxDBTreeListExpanding(Sender: TObject; Node: TdxTreeListNode;
//      var Allow: Boolean);
//    procedure dxDBTreeListCollapsing(Sender: TObject; Node: TdxTreeListNode;
//      var Allow: Boolean);

    procedure FormDestroy(Sender: TObject);
    procedure DoAction (Sender: TObject);
//    procedure dxDBTreeListEditing(Sender: TObject; Node: TdxTreeListNode; var
//        Allow: Boolean);
//    procedure dxDBTreeListHotTrackNode(Sender: TObject;
//      AHotTrackInfo: TdxTreeListHotTrackInfo; var ACursor: TCursor);
//    procedure cxDBTreeList11StylesGetContentStyle(Sender, AItem: TObject;
 //     ANode: TcxTreeListNode; var AStyle: TcxStyle);
    procedure cxDBTreeList1Collapsing(Sender: TObject; ANode: TcxTreeListNode; var
        Allow: Boolean);
//    procedure cxDBTreeList1CustomDrawCell(Sender: TObject; ACanvas: TcxCanvas;
 //       AViewInfo: TcxTreeListEditCellViewInfo; var ADone: Boolean);
//    procedure cxDBTreeList1Editing(Sender: TObject; AColumn: TcxTreeListColumn; var
 //       Allow: Boolean);
    procedure cxDBTreeList1Expanding(Sender: TcxCustomTreeList; ANode:
        TcxTreeListNode; var Allow: Boolean);
    procedure cxDBTreeList1StylesGetContentStyle(Sender: TcxCustomTreeList;
      AColumn: TcxTreeListColumn; ANode: TcxTreeListNode;
      var AStyle: TcxStyle);

//
//    procedure cxDBTreeList1StylesGetContentStyle(Sender: TcxCustomTreeList;
//      AColumn: TcxTreeListColumn; ANode: TcxTreeListNode;
//      var AStyle: TcxStyle);


  private
    FID: integer;
    FIsUpdated: boolean;

    FRegPath : string;

    FLinkEndIDArr_Expand: u_func.TIntArray;


//    procedure cxDBTreeList1Expanding1(Sender: TObject; ANode: TcxTreeListNode; var
  //      Allow: Boolean);
    
  public
    procedure SetReadOnly(Value: Boolean);
    procedure View (aLinkFreqPlanID: integer);

    procedure Close_Data;

  end;


//===========================================================
implementation {$R *.DFM}
//===========================================================

const
  CAPTION_Rec_No            = '�';
  CAPTION_TX_LINKEND_Name   = '��� ���';
  CAPTION_RX_LINKEND_Name   = '��� ���';
  CAPTION_Distance_m        = 'R [m]';
  CAPTION_Distance_km       = 'R [km]';
  CAPTION_RX_LEVEL_dBm      = '������ [dBm]';
  CAPTION_INTERFERENCE_dBm  = '������ [dBm]';
  CAPTION_CI                = 'CI [dB]';
  CAPTION_Neighbor_count    = '���-�� �������';
  CAPTION_Extra_Loss        = '������. ���������� [dB]';
  CAPTION_threshold_degradation = '���������� ���������������� [dB]';

  COLOR_ODD_RECORD  = clWindow;
  COLOR_EVEN_RECORD = clInfoBk;

  COLOR_NEIGHBOR      = $008080FF; //clRed;
  COLOR_NEIGHBOR_LINK = $0080FF80; //clLime;
  COLOR_NEIGHBOR_PROP = $0080FFFF; //clYellow;



procedure Tframe_LinkFreqPlan_NeightBors.Close_Data;
begin
   mem_Items.Close;
end;


procedure Tframe_LinkFreqPlan_NeightBors.cxDBTreeList11Collapsing(Sender: TObject; ANode:
    TcxTreeListNode; var Allow: Boolean);
begin
  inherited;

{  IntArrayRemoveValue(FLinkEndIDArr_Expand, Node.Values[col_LinkEnd_ID.Index]);
  reg_WriteString(FRegPath, 'LinkFreqPlanID:'+AsString(FID), IntArrayToString(FLinkEndIDArr_Expand));
}
end;


procedure Tframe_LinkFreqPlan_NeightBors.cxDBTreeList11CustomDrawCell(Sender: TObject; ACanvas:
    TcxCanvas; AViewInfo: TcxTreeListEditCellViewInfo; var ADone: Boolean);
var
  iType: integer;
  iColor: integer;
begin

 (* if not AViewInfo.Selected then
  begin
    iType := AsInteger(AViewInfo.Node.Values[col__Type.ItemIndex]);

    case iType of
      TYPE_NEIGHBORS_BY_LINK: iColor := COLOR_NEIGHBOR_LINK;
      TYPE_NEIGHBORS_BY_PROP: iColor := COLOR_NEIGHBOR_PROP;
      TYPE_NEIGHBORS_BY_CI:      iColor := COLOR_NEIGHBOR;
    else
      Exit;
    end;

    ACanvas.Brush.Color :=iColor;

  end;

*)
(*
    if AViewInfo.Node.Values[col__Type.ItemIndex] = OBJ_ANTENNA then
      ACanvas.Brush.Color := clInfoBk
    else
      ACanvas.Brush.Color := clWindow;
*)

(*

if not AViewInfo.Selected then
//   if AViewInfo.Node.Values[col__Type.ItemIndex] = OBJ_ANTENNA then
//      ACanvas.Brush.Color := clInfoBk
//    else
//      ACanvas.Brush.Color := clWindow;

  AViewInfo.

  if (not ASelected) then
  begin
    iType := AsInteger(ANode.Values[col_type.Index]);

    case iType of
      TYPE_NEIGHBORS:      AColor := COLOR_NEIGHBOR;
      TYPE_NEIGHBORS_LINK: AColor := COLOR_NEIGHBOR_LINK;
      TYPE_NEIGHBORS_PROP: AColor := COLOR_NEIGHBOR_PROP;
    end;

  end;*)


//  AViewInfo

{
   inherited;
  if (not ASelected) then
    if ANode.Values[col__type.index] = TYPE_NEIGHBORS      then
      AColor := COLOR_NEIGHBOR       else
    if ANode.Values[col__type.index] = TYPE_NEIGHBORS_LINK then
      AColor := COLOR_NEIGHBOR_LINK  else
    if ANode.Values[col__type.index] = TYPE_NEIGHBORS_PROP then
      AColor := COLOR_NEIGHBOR_PROP;

  if (AColumn=col_RecNo) and
     (ANode.Values[col__tree_parent_id.Index] = 0) then
    aText:=IntToStr(aNode.Index+1);
}

end;



procedure Tframe_LinkFreqPlan_NeightBors.cxDBTreeList11Editing(Sender: TObject; AColumn:
    TcxTreeListColumn; var Allow: Boolean);
begin
{
  inherited;

  if dxDBTreeList.FocusedAbsoluteIndex = col_Extra_Loss.Index then
    with dxDBTreeList do
      if (FocusedNode.Values[col_tree_parent_id.Index] = 0) or
         (FocusedNode.Values[col_type.index] = TYPE_NEIGHBORS_LINK) then
        col_Extra_Loss.DisableEditor:= True
      else
        col_Extra_Loss.DisableEditor:= False;

}
end;

//-------------------------------------------------------------------
procedure Tframe_LinkFreqPlan_NeightBors.FormCreate(Sender: TObject);
//-------------------------------------------------------------------
begin
  inherited;

  FRegPath := g_Storage.GetPathByClass(ClassName) + '1';





  pnLegend1.Caption:= '��������� �� CI';
  pnLegend2.Caption:= '��������� �� ��������';
  pnLegend3.Caption:= '��������� �� ���������';

  act_Neighbors_calc.Caption:= 'Pac��� CI';
  act_Calc_CI.Caption       := '������ ��������� �� CI';
  act_Collapse.Caption      := '�������� ���';
  act_Expand.Caption        := '�������� ���';

 // dxDBTreeList.Align:= alClient;

 cxDBTreeList1.Align:= alClient;


  // -------------------------------------------------------------------
   {
  with dxDBTreeList do
  begin
    Bands[1].Caption  := '���';
  end;
  }



 // col_RecNo.Caption             := CAPTION_Rec_No;
  col__RX_LinkEnd_Name.Caption.Text  := CAPTION_RX_LINKEND_Name;
  col__TX_LinkEnd_Name.Caption.Text  := CAPTION_TX_LINKEND_Name;
  col__Distance_m.Caption.Text        := CAPTION_Distance_m;
//  col__Distance_km.Caption.Text       := CAPTION_Distance_km;
  col__CI.Caption.Text                := CAPTION_CI;


  col__Rx_level_dBm.Caption.Text      := CAPTION_RX_LEVEL_dBm;
//  col__Interference.Caption.Text      := 'Interference '+ CAPTION_INTERFERENCE_dBm;

{  col__Rx_level_dBm.Caption.Text      := col__Rx_level_dBm.Caption.Text +'-'+ CAPTION_RX_LEVEL_dBm;
  col__Interference.Caption.Text      := col__Interference.Caption.Text + '-' + CAPTION_INTERFERENCE_dBm;
}
  col_Interference.Caption.Text      :='�������������';

  col__Band.Caption.Text      :='��������';

  col_POWER_noise.Caption.Text  :='������� ����';


  col__Neighbour_Count.Caption.Text   := CAPTION_Neighbor_count;
  col__Extra_Loss.Caption.Text        := CAPTION_Extra_Loss;
  col__threshold_degradation.Caption.Text:=  CAPTION_threshold_degradation;
  col__threshold_degradation_cia.Caption.Text:= CAPTION_threshold_degradation;

  col__CIA.Caption.Text := 'CIA [dB]';
  col__CI_.Caption.Text := 'CI [dB]' ;
  col__CA.Caption.Text  := 'CA [dB]' ;


  col_FLD_DIAGRAM_LEVEL.DataBinding.FieldName:=FLD_DIAGRAM_LEVEL;



  // -------------------------------------------------------------------
// cxDBTreeList1.RestoreFromRegistry(FRegPath + cxDBTreeList1.Name);

 (* col__RecNo.Caption.Text             := CAPTION_Rec_No;
  col__RX_LinkEnd_Name.Caption.Text   := CAPTION_RX_LINKEND_Name;
  col__TX_LinkEnd_Name.Caption.Text   := CAPTION_TX_LINKEND_Name;
  col__Distance_m.Caption.Text        := CAPTION_Distance_m;
  col__CI.Caption.Text                := CAPTION_CI;
  col__Rx_level_dBm.Caption.Text      := CAPTION_RX_LEVEL_dBm;
  col__Interference.Caption.Text      := CAPTION_INTERFERENCE_dBm;
  col__Neighbors_Count.Caption.Text   := CAPTION_Neighbor_count;
  col__Extra_Loss.Caption.Text        := CAPTION_Extra_Loss;
  col__threshold_degradation.Caption.Text:= CAPTION_threshold_degradation;
  col__threshold_degradation_cia.Caption.Text:= CAPTION_threshold_degradation;

  col__CIA.Caption.Text := 'CIA [dB]';
  col__CI_.Caption.Text := 'CI [dB]' ;
  col__CA.Caption.Text  := 'CA [dB]' ;
*)

  // -------------------------------------------------------------------

  dmLinkFreqPlan_NeighBors_view.InitDB (mem_Items);

//  dx_CheckRegistry(dxDBTreeList, FRegPath + dxDBTreeList.Name);
//  dxDBTreeList.LoadFromRegistry (FRegPath + dxDBTreeList.Name);

 // dx_CheckColumnSizes_DBTreeList(dxDBTreeList);

  SetComponentActionsExecuteProc (Self, DoAction);

 {
  col_tree_id.Visible:=False;
  col_tree_Parent_id.Visible:=False;
  }

/////////  cxDBTreeList11.RestoreFromRegistry(FRegPath + cxDBTreeList11.Name);

{  with gl_Reg do
  begin
  // CurrentRegPath:= FRegPath;
    CurrentRegPath := FRegPath;

    BeginGroup(Self, 'Forms\'+ClassName);
  end;
}


 // TdmLanguage.CheckForm(Self);


end;

//-----------------------------------------------------------------
procedure Tframe_LinkFreqPlan_NeightBors.FormDestroy(Sender: TObject);
//-----------------------------------------------------------------
begin
//  reg_WriteInteger(FRegPath,);
  //
  reg_WriteString(FRegPath, 'LinkFreqPlanID:'+AsString(FID), IntArrayToString(FLinkEndIDArr_Expand));

//
 // dxDBTreeList.SaveToRegistry(FRegPath + dxDBTreeList.Name);
 // cxDBTreeList11.StoreToRegistry(FRegPath + cxDBTreeList11.Name);

  inherited;
end;

//-------------------------------------------------------------------
procedure Tframe_LinkFreqPlan_NeightBors.View (aLinkFreqPlanID: integer);
//-------------------------------------------------------------------
var  i: integer;
  iID: Integer;

  oNode: TcxTreeListNode;
begin


  dmOnega_DB_data.openStoredProc(ADOStoredProc1, sp_LinkFreqPlan_Neighbors,
     [FLD_ID, aLinkFreqPlanID]);




(*
  dmOnega_DB_data.openStoredProc(ADOStoredProc1, sp_LinkFreqPlan_Neighbors,
     [db_Par(FLD_ID, aLinkFreqPlanID)]);


  db_SetFieldCaptions (ADOStoredProc1,
        [
         SArr(FLD_NUMBER,                    CAPTION_Rec_No),

         SArr(FLD_BAND,                      'band'),

         SArr(FLD_TX_LinkEnd_name,           CAPTION_TX_LINKEND_Name),
         SArr(FLD_RX_LinkEnd_name,           CAPTION_RX_LINKEND_Name),

         SArr(FLD_Rx_level_dBm,              CAPTION_RX_LEVEL_dBm),
         SArr(FLD_DISTANCE_m,                CAPTION_Distance_m),
         SArr(FLD_DISTANCE_km,               CAPTION_Distance_km),

         SArr(FLD_CIA,                       'CIA [dB]'),
         SArr(FLD_CI_,                       'CI [dB]'),
         SArr(FLD_CA,                        'CA [dB]'),

         SArr(FLD_THRESHOLD_DEGRADATION,     CAPTION_threshold_degradation),
         SArr(FLD_THRESHOLD_DEGRADATION_CIA, CAPTION_threshold_degradation),

         SArr(FLD_NEIGHBOUR_COUNT,           CAPTION_Neighbor_count)

        ]);

*)

(*
  col_RecNo.Caption             := CAPTION_Rec_No;
  col_RX_LinkEnd_Name.Caption   := CAPTION_RX_LINKEND_Name;
  col_TX_LinkEnd_Name.Caption   := CAPTION_TX_LINKEND_Name;
  col_Distance_m.Caption        := CAPTION_Distance_m;
  col_CI.Caption                := CAPTION_CI;
  col_Rx_level_dBm.Caption      := CAPTION_RX_LEVEL_dBm;
  col_Interference.Caption      := CAPTION_INTERFERENCE_dBm;
  col_Neighbors_Count.Caption   := CAPTION_Neighbor_count;
  col_Extra_Loss.Caption        := CAPTION_Extra_Loss;

  col_threshold_degradation.Caption:= CAPTION_threshold_degradation;
  col_threshold_degradation_cia.Caption:= CAPTION_threshold_degradation;

  col_CIA.Caption := 'CIA [dB]';
  col_CI_.Caption := 'CI [dB]' ;
  col_CA.Caption  := 'CA [dB]' ;

*)

 // cxDBTreeList11.FullExpand;

 // db_viewDataset(ADOStoredProc1);


  if (FID<>aLinkFreqPlanID) or (Length(FLinkEndIDArr_Expand)=0) then
    FLinkEndIDArr_Expand:= StringToIntArray(
         reg_ReadString(FRegPath, 'LinkFreqPlanID:'+AsString(aLinkFreqPlanID), ''));

  FID := aLinkFreqPlanID;

  dmLinkFreqPlan_NeighBors_view.Params.IsUpdated:= True;

  dmLinkFreqPlan_NeighBors_view.OpenDB (mem_Items, FID);

 // db_View(mem_Items);


  mem_Items.DisableControls;


  for i := 0 to cxDBTreeList1.Count - 1 do
  begin
    oNode :=cxDBTreeList1.Items[i];

    oNode.HasChildren := (oNode.Values[col__Neighbour_Count.ItemIndex]>0);

  //  iID :=oNode.Values[col__LinkEnd_id.ItemIndex];

  //  if FindInIntArray(iID, FLinkEndIDArr_Expand) > -1 then
  //    oNode.Expand(True);
  end;




  {
  for i := 0 to dxDBTreeList.Count - 1 do
  begin
    oNode :=dxDBTreeList.Items[i];

    oNode.HasChildren := (oNode.Values[col_Neighbors_Count.Index]>0);

    iID :=oNode.Values[col_LinkEnd_id.Index];

    if FindInIntArray(iID, FLinkEndIDArr_Expand) > -1 then
      oNode.Expand(True);
  end;
  }

(*
    with dxDBTreeList.Items[i] do
  begin
    HasChildren := (Values[col_Neighbors_Count.Index]>0);

//    if FindInIntArray(Values[col_LinkEnd_ID.Index], FLinkEndIDArr_Expand) > -1 then

   iID :=Values[col_LinkEnd_id.Index];

    if FindInIntArray(iID, FLinkEndIDArr_Expand) > -1 then
      Expand(True);
  end;
  *)


  dmLinkFreqPlan_NeighBors_view.Params.IsUpdated:= False;

  mem_Items.First;
  mem_Items.EnableControls;
  
end;

procedure Tframe_LinkFreqPlan_NeightBors.cxDBTreeList1Collapsing(Sender:
    TObject; ANode: TcxTreeListNode; var Allow: Boolean);
begin
  IntArrayRemoveValue(FLinkEndIDArr_Expand, aNode.Values[col__LinkEnd_ID.ItemIndex]);
//  IntArrayRemoveValue(FLinkEndIDArr_Expand, Node.Values[col_TX_LinkEnd_ID.Index]);
  reg_WriteString(FRegPath, 'LinkFreqPlanID:'+AsString(FID), IntArrayToString(FLinkEndIDArr_Expand));

  //
end;



//-----------------------------------------------------------------
procedure Tframe_LinkFreqPlan_NeightBors.DoAction (Sender: TObject);
//-----------------------------------------------------------------
var i: integer;
begin
  //-----------------------
  if Sender=act_Neighbors_calc then
  begin
    dmLinkFreqPlan_Tools.RunCalc_FREQPLAN_NB (FID, True);

    View(FID);
  end else

  //-----------------------
  if Sender=act_Calc_CI then
  begin
    dmLinkFreqPlan_Tools.RunCalc_FREQPLAN_NB (FID, False);

    View(FID);
  end else

  //------------
  if Sender = act_Setup_Grid then
  begin
    cxDBTreeList1.Customizing.Visible:=True;
  //  dx_Dlg_Customize_DBTreeList (dxDBTreeList);
  end else

  //------------
  if Sender = act_Clear_Extra_Loss then
  begin
    dmOnega_DB_data.LinkFreqPlan_tools(FID, DEF_Clear_Extra_Loss);

    if ConfirmDlg('���������� ���������� ������ ��������� �� CI!'+#10#13+
                  '���������?')
    then
      dmLinkFreqPlan_Tools.RunCalc_FREQPLAN_NB (FID, False);

    View(FID);
  end else


  //---------------------------------------
  if Sender=act_Collapse then begin
  //---------------------------------------
//    SetLength(FLinkEndIDArr_Expand, 0);
//    reg_WriteString(FRegPath, 'LinkFreqPlanID:'+AsString(FID),  '');

    cxDBTreeList1.FullCollapse;
  end else


  //---------------------------------------
  if Sender=act_Expand then begin
  //---------------------------------------
    cxDBTreeList1.FullExpand;


  {
    for i := 0 to dxDBTreeList.Count - 1 do
      with dxDBTreeList.Items[i] do
        AddToIntArray(Values[col_LinkEnd_ID.Index], FLinkEndIDArr_Expand);
//        AddToIntArray(Values[col_tx_LinkEnd_ID.Index], FLinkEndIDArr_Expand);


    reg_WriteString(FRegPath, 'LinkFreqPlanID:'+AsString(FID), IntArrayToString(FLinkEndIDArr_Expand));

    mem_Items.DisableControls;
    dxDBTreeList.FullExpand;
    mem_Items.EnableControls;
   }

  end else
    raise Exception.Create('');

end;



// ---------------------------------------------------------------
procedure Tframe_LinkFreqPlan_NeightBors.cxDBTreeList1Expanding(Sender:
    TcxCustomTreeList; ANode: TcxTreeListNode; var Allow: Boolean);
// ---------------------------------------------------------------


var
  v: Variant;
begin
  if FIsUpdated then
    Exit;

  dmLinkFreqPlan_NeighBors_view.Params.IsUpdated:= True;

  //    iType := AsInteger(AViewInfo.Node.Values[col__Type.ItemIndex]);


  v:=aNode.Values[col__is_expanded.ItemIndex];

  with mem_Items do
    if aNode.Values[col__is_expanded.ItemIndex] <> True then
  begin
    DisableControls;
    Locate (FLD_DXTREE_ID, aNode.Values[col__tree_id.ItemIndex],[]);

    Edit;
    FieldValues['is_expanded']:=True;
    Post;

    FIsUpdated:=True;
    dmLinkFreqPlan_NeighBors_view.ExpandNeighBours (mem_Items, FID,
                                    //  Node.Values[col_TX_LinkEnd_ID.Index],
                                      aNode.Values[col__LinkEnd_ID.ItemIndex],
                                      aNode.Values[col__tree_id.ItemIndex]);
    FIsUpdated:=False;
    EnableControls;
  end;

  dmLinkFreqPlan_NeighBors_view.Params.IsUpdated:= False;

  AddToIntArray(aNode.Values[col__LinkEnd_ID.ItemIndex], FLinkEndIDArr_Expand);
//  AddToIntArray(Node.Values[col_TX_LinkEnd_ID.Index], FLinkEndIDArr_Expand);

  reg_WriteString(FRegPath, 'LinkFreqPlanID:'+AsString(FID), IntArrayToString(FLinkEndIDArr_Expand));

end;



procedure Tframe_LinkFreqPlan_NeightBors.SetReadOnly(Value: Boolean);
begin
//  cxGrid1DBTableView1.OptionsData.Editing := not Value;
//  FReadOnly := Value;

 // ADOStoredProc1.LockType := IIF(Value, ltReadOnly, ltBatchOptimistic);
  SetFormActionsEnabled(Self, not Value);

//  ADOStoredProc1.LockType := IIF(dmUser_Security.ProjectIsReadOnly, ltReadOnly, ltBatchOptimistic);
//  SetFormActionsEnabled(Self, not dmUser_Security.ProjectIsReadOnly);

end;



procedure Tframe_LinkFreqPlan_NeightBors.cxDBTreeList1StylesGetContentStyle(
  Sender: TcxCustomTreeList; AColumn: TcxTreeListColumn;
  ANode: TcxTreeListNode; var AStyle: TcxStyle);

var
  iType: integer;
begin
 //  Exit;


 // if (not ASelected) then
  begin
    iType := AsInteger(ANode.Values[col__type.ItemIndex]);

    case iType of
      TYPE_NEIGHBORS_BY_CI:   AStyle := cxStyle_red;
      TYPE_NEIGHBORS_BY_LINK: AStyle := cxStyle_LINK;
      TYPE_NEIGHBORS_BY_PROP: AStyle := cxStyle_PROP;
    end;
  end;
end;


end.


{
//-------------------------------------------------------------------
procedure Tframe_LinkFreqPlan_NeightBors.dxDBTreeListCollapsing(Sender: TObject;
  Node: TdxTreeListNode; var Allow: Boolean);
//-------------------------------------------------------------------
begin
  IntArrayRemoveValue(FLinkEndIDArr_Expand, Node.Values[col_LinkEnd_ID.Index]);
//  IntArrayRemoveValue(FLinkEndIDArr_Expand, Node.Values[col_TX_LinkEnd_ID.Index]);
  reg_WriteString(FRegPath, 'LinkFreqPlanID:'+AsString(FID), IntArrayToString(FLinkEndIDArr_Expand));
end;


//-------------------------------------------------------------------
procedure Tframe_LinkFreqPlan_NeightBors.dxDBTreeListExpanding(Sender: TObject;
  Node: TdxTreeListNode; var Allow: Boolean);
//-------------------------------------------------------------------
begin
  if FIsUpdated then Exit;

  dmLinkFreqPlan_NeighBors_view.Params.IsUpdated:= True;

  with mem_Items do
    if Node.Strings[col_is_expanded.Index] = 'False' then
  begin
    DisableControls;
    Locate (FLD_DXTREE_ID, Node.Values[col_tree_id.Index],[]);

    Edit;
    FieldValues['is_expanded']:=True;
    Post;

    FIsUpdated:=True;
    dmLinkFreqPlan_NeighBors_view.ExpandNeighBours (mem_Items, FID,
                                    //  Node.Values[col_TX_LinkEnd_ID.Index],
                                      Node.Values[col_LinkEnd_ID.Index],
                                      Node.Values[col_tree_id.Index]);
    FIsUpdated:=False;
    EnableControls;
  end;

  dmLinkFreqPlan_NeighBors_view.Params.IsUpdated:= False;

  AddToIntArray(Node.Values[col_LinkEnd_ID.Index], FLinkEndIDArr_Expand);
//  AddToIntArray(Node.Values[col_TX_LinkEnd_ID.Index], FLinkEndIDArr_Expand);

  reg_WriteString(FRegPath, 'LinkFreqPlanID:'+AsString(FID), IntArrayToString(FLinkEndIDArr_Expand));

end;
}


(*
      db_Field( FLD_POWER_noise,              ftFloat),
      db_Field( FLD_POWER_CIA,              ftFloat),
*)




{
//-----------------------------------------------------------------
procedure Tframe_LinkFreqPlan_NeightBors.dxDBTreeListEditing(Sender: TObject;
    Node: TdxTreeListNode; var Allow: Boolean);
//-----------------------------------------------------------------
begin
  inherited;

  if dxDBTreeList.FocusedAbsoluteIndex = col_Extra_Loss.Index then
     with dxDBTreeList do
       col_Extra_Loss.DisableEditor:=
           (FocusedNode.Values[col_tree_parent_id.Index] = 0) or
           (FocusedNode.Values[col_type.index] = TYPE_NEIGHBORS_BY_LINK);

    //    col_Extra_Loss.DisableEditor:= True
     // else
      //  col_Extra_Loss.DisableEditor:= False;

end;



//-----------------------------------------------------------------
procedure Tframe_LinkFreqPlan_NeightBors.dxDBTreeListHotTrackNode(
  Sender: TObject; AHotTrackInfo: TdxTreeListHotTrackInfo;
  var ACursor: TCursor);
//-----------------------------------------------------------------
begin
  inherited;

//  dx_ShowHint(dxDBTreeList, AHotTrackInfo,
//                             [col_RX_LinkEnd_Name, col_TX_LinkEnd_Name]);

end;



procedure Tframe_LinkFreqPlan_NeightBors.cxDBTreeList1CustomDrawCell(Sender:
    TObject; ACanvas: TcxCanvas; AViewInfo: TcxTreeListEditCellViewInfo; var
    ADone: Boolean);

var
  iType: integer;
begin
///////  if (not ASelected) then
 (* begin
    iType := AsInteger(ANode.Values[col__type.Index]);

    case iType of
      TYPE_NEIGHBORS_BY_CI:   AColor := COLOR_NEIGHBOR;
      TYPE_NEIGHBORS_BY_LINK: AColor := COLOR_NEIGHBOR_LINK;
      TYPE_NEIGHBORS_BY_PROP: AColor := COLOR_NEIGHBOR_PROP;
    end;


  end;

  if (AColumn=col__RecNo) and
     (ANode.Values[col__tree_parent_id.Index] = 0) then
    aText:=IntToStr(aNode.Index+1);

*)

(*    if ANode.Values[col_type.index] = TYPE_NEIGHBORS      then
      AColor := COLOR_NEIGHBOR       else
    if ANode.Values[col_type.index] = TYPE_NEIGHBORS_LINK then
      AColor := COLOR_NEIGHBOR_LINK  else
    if ANode.Values[col_type.index] = TYPE_NEIGHBORS_PROP then
      AColor := COLOR_NEIGHBOR_PROP;
*)
end;



  }





     (*
  
procedure Tframe_LinkFreqPlan_NeightBors.cxDBTreeList1CustomDrawCell(Sender:
    TObject; ACanvas: TcxCanvas; AViewInfo: TcxTreeListEditCellViewInfo; var
    ADone: Boolean);

var
  iType: integer;
begin
///////  if (not ASelected) then
 (* begin
    iType := AsInteger(ANode.Values[col__type.Index]);

    case iType of
      TYPE_NEIGHBORS_BY_CI:   AColor := COLOR_NEIGHBOR;
      TYPE_NEIGHBORS_BY_LINK: AColor := COLOR_NEIGHBOR_LINK;
      TYPE_NEIGHBORS_BY_PROP: AColor := COLOR_NEIGHBOR_PROP;
    end;


  end;

  if (AColumn=col__RecNo) and
     (ANode.Values[col__tree_parent_id.Index] = 0) then
    aText:=IntToStr(aNode.Index+1);

*)

(*    if ANode.Values[col_type.index] = TYPE_NEIGHBORS      then
      AColor := COLOR_NEIGHBOR       else
    if ANode.Values[col_type.index] = TYPE_NEIGHBORS_LINK then
      AColor := COLOR_NEIGHBOR_LINK  else
    if ANode.Values[col_type.index] = TYPE_NEIGHBORS_PROP then
      AColor := COLOR_NEIGHBOR_PROP;
*)
end;


       {

procedure Tframe_LinkFreqPlan_NeightBors.cxDBTreeList1StylesGetContentStyle(
  Sender: TcxCustomTreeList; AColumn: TcxTreeListColumn;
  ANode: TcxTreeListNode; var AStyle: TcxStyle);
var
  iType: Integer;
begin
{
  iType := AsInteger(ANode.Values[col__type.ItemIndex]);

  case iType of
    TYPE_NEIGHBORS_BY_CI:   AStyle := cxStyle_red;
    TYPE_NEIGHBORS_BY_LINK: AStyle := cxStyle_LINK;
    TYPE_NEIGHBORS_BY_PROP: AStyle := cxStyle_PROP;
  end;
  }

end;


        {

procedure Tframe_LinkFreqPlan_NeightBors.cxDBTreeList11StylesGetContentStyle(
  Sender, AItem: TObject; ANode: TcxTreeListNode; var AStyle: TcxStyle);

var
  iType: integer;
begin
 //  Exit;


 // if (not ASelected) then
  begin
    iType := AsInteger(ANode.Values[col__type.ItemIndex]);

    case iType of
      TYPE_NEIGHBORS_BY_CI:   AStyle := cxStyle_red;
      TYPE_NEIGHBORS_BY_LINK: AStyle := cxStyle_LINK;
      TYPE_NEIGHBORS_BY_PROP: AStyle := cxStyle_PROP;
    end;


(*  COLOR_NEIGHBOR      = $008080FF; //clRed;
  COLOR_NEIGHBOR_LINK = $0080FF80; //clLime;
  COLOR_NEIGHBOR_PROP = $0080FFFF; //clYellow;


*)

(*    if ANode.Values[col_type.index] = TYPE_NEIGHBORS      then
      AColor := COLOR_NEIGHBOR       else
    if ANode.Values[col_type.index] = TYPE_NEIGHBORS_LINK then
      AColor := COLOR_NEIGHBOR_LINK  else
    if ANode.Values[col_type.index] = TYPE_NEIGHBORS_PROP then
      AColor := COLOR_NEIGHBOR_PROP;
*)

  end;
(*
  if (AColumn=col_RecNo) and
     (ANode.Values[col_tree_parent_id.Index] = 0) then
    aText:=IntToStr(aNode.Index+1);
*)
end;






procedure Tframe_LinkFreqPlan_NeightBors.cxDBTreeList1Editing(Sender: TObject;
    AColumn: TcxTreeListColumn; var Allow: Boolean);
begin

(*  cxDBTreeList1.f


  if dxDBTreeList.FocusedAbsoluteIndex = col__Extra_Loss.Index then
     with dxDBTreeList do
       col__Extra_Loss.DisableEditor:=
           (FocusedNode.Values[col__tree_parent_id.Index] = 0) or
           (FocusedNode.Values[col__type.index] = TYPE_NEIGHBORS_BY_LINK);

*)

end;



{
//-------------------------------------------------------------------
procedure Tframe_LinkFreqPlan_NeightBors.dxDBTreeListCustomDrawCell(Sender: TObject;
  ACanvas: TCanvas; ARect: TRect; ANode: TdxTreeListNode;
  AColumn: TdxTreeListColumn; ASelected, AFocused, ANewItemRow: Boolean;
  var AText: String; var AColor: TColor; AFont: TFont;
  var AAlignment: TAlignment; var ADone: Boolean);
//-------------------------------------------------------------------
var
  iType: integer;
begin
  if (not ASelected) then
  begin
    iType := AsInteger(ANode.Values[col_type.Index]);

    case iType of
      TYPE_NEIGHBORS_BY_CI:   AColor := COLOR_NEIGHBOR;
      TYPE_NEIGHBORS_BY_LINK: AColor := COLOR_NEIGHBOR_LINK;
      TYPE_NEIGHBORS_BY_PROP: AColor := COLOR_NEIGHBOR_PROP;
    end;



  end;

  if (AColumn=col_RecNo) and
     (ANode.Values[col_tree_parent_id.Index] = 0) then
    aText:=IntToStr(aNode.Index+1);

end;
 }

