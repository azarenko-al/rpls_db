unit u_LinkFreqPlan_const;

interface




//const
{  SQL_SELECT_RRL_NEIGHBORS =
     'SELECT * FROM '+ TBL_LINK_FREQPLAN_NEIGHBORS +
     ' WHERE Link_FreqPlan_id=:Link_FreqPlan_id';

}
  { TODO : SQL ������ ��������� !!! }
 
const
    STR_DISTRIBUTED              = 'distributed';
    STR_FIXED                    = 'fixed';
    FLD_Interference_ChannelsKind1= 'Interference_ChannelsKind'; //�� ����� �������� ��������� �������������

    FLD_Polarization_Level_dB = 'Polarization_Level_dB';

    FLD_CHANNEL_NUMBER           = 'channel_number';
    FLD_CHANNEL_NUMBER_TYPE      = 'channel_number_type';
    FLD_CH_TYPE                  = 'ch_type';

  //  FLD_SUBBAND                  = 'FLD_SUBBAND';

    FLD_LinkFreqPlan_MIN_CI_dB    = 'LinkFreqPlan_MIN_CI_dB';
    FLD_LinkFreqPlan_THRESHOLD_DEGRADATION = 'LinkFreqPlan_THRESHOLD_DEGRADATION';

    FLD_Min_CI_dB = 'Min_CI_dB';

//    FLD_LINK_FREQPLAN_ID         = 'Link_FreqPlan_ID';

    FLD_ChannelsAllowed          = 'ChannelsAllowed';
    FLD_MaxChannelsResource      = 'MaxChannelsResource';
    FLD_MaxCalcDistance_KM       = 'MaxCalcDistance_KM';
    FLD_FirstChannelFreq         = 'FirstChannelFreq';

  
  //--------------------------------------------------------------
  // 1. ��������� �����������
  //--------------------------------------------------------------
    //1.1
    FLD_DuplexFreqDelta     = 'DuplexFreqDelta';
    //1.2
    FLD_ChannelWidth        = 'ChannelWidth';
    //1.3. ���������� ������, ����������� ��� �������� �����
  FLD_SitesFreqSpacing    = 'SitesFreqSpacing'; // : integer;
    //1.4. �in ������ ������ � ����� �������
  FLD_SectorFreqSpacing   = 'SectorFreqSpacing'; //: integer;
    //1.5. �in ������ ������ ����� ��������� ����� ��
  FLD_SiteFreqSpacing     = 'SiteFreqSpacing'; // : integer;
    //1.7. �������� �� ����������� [dB]
  FLD_PolarizationLevel     = 'PolarizationLevel'; // : float;

  //--------------------------------------------------------------
  // 2. ��������� �������� ��������
  //--------------------------------------------------------------
  //2.1. ��������� ������ [1 - ������ ������ � �.�., 0 - ����� �������]
  FLD_StartSectorNum = 'StartSectorNum'; //: integer;

  //2.2. ����������� ��������
  FLD_PereborQueueType = 'PereborQueueType';
    VAL_BySectorOrder=0;     // 0-� ������� ��������� ���� ��������
    VAL_ByNeibOrder  =1;     // 1-� ������� ��������� ����� �������
    VAL_ByNeibRandomOrder=2; // 2-� ��������� ������� ����� �������
    VAL_ByNeibLess   =3;     // 3-� ������� �������� ����� �������

  //2.3. ���������� ������ ������� � ��������� �������
  FLD_FreqDistribType = 'FreqDistribType';
    VAL_OneFreqForSector = 0; // 0-�� 1 ������� �� ������
    VAL_AllFreqsForSector= 1; // 1-��� ������� �� ������

  //2.4. Max ���-�� �������� (0-���� ������ �� ���� ��������
  FLD_MaxIterCount = 'MaxIterCount'; //: integer;

  //--------------------------------------------------------------
  // 3. ��������� �������� ������
  //--------------------------------------------------------------
  //3.1. ����������� �������� ������
  FLD_PereborOrderType ='PereborOrderType';
    VAL_Forward =0; // ������
    VAL_Backward=1; // ��������
    VAL_Random  =2; // ���������

  //3.2. Max ���������� �������� ����� �������
  FLD_MaxFreqRepeat       = 'MaxFreqRepeat'; //: integer;


  FLD_RecNo                 = 'RecNo';
  FLD_LinkEnd_name          = 'LinkEnd_name';
  FLD_Distance              = 'Distance';
  FLD_DIAGRAM_LEVEL         = 'diagram_level';
  FLD_IsNeigh               = 'IsNeigh';
  FLD_IsLink                = 'IsLink';
  FLD_IsPropertyOne         = 'IsPropertyOne';

//  FLD_FREQ_REQ_COUNT        = 'Freq_req_count';

  FLD_Distributed_Min_raznos= 'Distrubuted_Min_raznos';
  FLD_Distributed_Count     = 'Distributed_Count';
  FLD_Distributed           = 'Distributed';

  FLD_Forbidden             = 'Forbidden';

  FLD_Calc_order            = 'Calc_order';
  FLD_Fixed                 = 'Fixed';
  FLD_Fixed_Min_raznos      = 'Fixed_Min_raznos';
  FLD_Fixed_count           = 'Fixed_count';
  FLD_RowColor              = 'RowColor';
  FLD_Freqs_Forbidden_Count = 'Freqs_Forbidden_Count';
  FLD_Freqs_Forbidden       = 'Freqs_Forbidden';
  FLD_Sorted_Freqs          = 'Sorted_Freqs';

  FLD_FREQ_FIXED_COUNT      = 'freq_fixed_count';
  FLD_FREQ_DISTRIBUTED_COUNT= 'freq_distributed_count';
  FLD_FREQ_FORBIDDEN_COUNT  = 'freq_forbidden_count';

  FLD_FREQ_DISTRIBUTED    = 'Freq_Distributed'; //: TIntArray; // ������ �������������� ������
  FLD_FREQ_DISTRIBUTED_TX = 'freq_Distributed_tx';  //������� ������� ���
  FLD_FREQ_DISTRIBUTED_RX = 'freq_Distributed_rx';  //������� ������� ���
  FLD_FREQ_FORBIDDEN      = 'freq_forbidden';  //: TIntArray; // ������ ����������� ������
  FLD_FREQ_FIXED          = 'freq_fixed';      //: TIntArray; // ������ ������������ ������
  FLD_FREQ_FIXED_TX       = 'freq_fixed_tx';  //������� ������� ���
  FLD_FREQ_FIXED_RX       = 'freq_fixed_rx';  //������� ������� ���

  FLD_FREQ_DISTRIBUTED_TX_STR = 'freq_Distributed_tx_STR';  //������� ������� ���
  FLD_FREQ_DISTRIBUTED_RX_STR = 'freq_Distributed_rx_STR';  //������� ������� ���


  FLD_FREQ_FIXED_TX_STR   = 'freq_fixed_tx_STR';  //������� ������� ���
  FLD_FREQ_FIXED_RX_STR   = 'freq_fixed_rx_STR';  //������� ������� ���


  FLD_FREQ_ALLOW          = 'freq_allow';

  FLD_freq_req_count      = 'freq_req_count';
  FLD_FREQ_NUMBER      = 'freq_number';
  FLD_REPEAT           = 'repeat';

  FLD_TXRX_SHIFT       = 'txrx_shift';
  FLD_CI_              =  'ci_';
//  FLD_CI_0             = 'ci_0';

  FLD_CHANNEL_TYPE_DISTRIBUTED = 'CHANNEL_type_distributed';
  FLD_CHANNEL_TYPE_FIXED       = 'CHANNEL_type_fixed';

(*  FLD_CH_TYPE_DISTRIBUTED = 'ch_type_distributed';
  FLD_CH_TYPE_FIXED       = 'ch_type_fixed';
*)
//  FLD_priority         = 'priority';
  FLD_EXTRA_LOSS          = 'extra_loss';

  FLD_RX_LinkEnd_name = 'RX_LinkEnd_name';
  FLD_TX_LinkEnd_name = 'TX_LinkEnd_name';

  FLD_THRESHOLD_DEGRADATION_CIA = 'threshold_degradation_cia';
  FLD_POWER_CIA                 = 'power_cia';

implementation

end.