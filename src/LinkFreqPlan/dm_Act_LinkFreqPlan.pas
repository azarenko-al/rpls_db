unit dm_act_LinkFreqPlan;

interface

uses
  Classes, Forms,  Menus, ActnList,  SysUtils,

  u_func_msg,

  dm_Main,

  dm_User_Security,

 //  d_Audit,

  I_Object,

//  I_LinkFreqPlan,
  I_Shell,
  u_Shell_new,

  u_const_db,
  u_const_str,
  u_const_msg,

  u_Types,

  u_func,

 // I_Act_LinkFreqPlan,

  dm_LinkFreqPlan,
//  dm_LinkFreqPlan_map_view,


  d_LinkFreqPlan_add,
  fr_LinkFreqPlan_view,
  f_LinkFreqPlan_CIA,

  dm_act_Base
  ;

type
  TdmAct_LinkFreqPlan = class(TdmAct_Base) //, IAct_LinkFreqPlan_X) //, ILinkFreqPlanX)
    act_Add_Link: TAction;
    act_Create_freq_map: TAction;
    act_Create_Tool_CIA: TAction;
    ActionList2: TActionList;
    act_Audit1: TAction;
 //   procedure DataModuleDestroy(Sender: TObject);
//    procedure DataModuleDestroy(Sender: TObject);
    procedure DataModuleCreate(Sender: TObject);

    procedure DoAction (Sender: TObject);

  private
    FLinkNetID: integer;
  protected
    Fstrings: TStringList;

//    FLinkIDs: TIDList;

    function  GetViewForm  (aOwnerForm: TForm): TForm; override;
    procedure GetPopupMenu (aPopupMenu: TPopupMenu; aMenuType: TMenuType); override;

    function ItemDel(aID: integer; aName: string = ''): boolean; override;
    function ItemAdd (aFolderID: integer): integer; override;

  public
    procedure AddByNet (aFolderID, aLinkNetID: integer);
//    procedure Update_LinkList1(aLinkNetID: integer);

    function Dlg_AddForLinkNet (aLinkNetID, aFolderID: integer): integer;

    class procedure Init;

  end;

var
  dmAct_LinkFreqPlan: TdmAct_LinkFreqPlan;

//===================================================================
// implementation
//===================================================================
implementation
 {$R *.dfm}


//-------------------------------------------------------------------
class procedure TdmAct_LinkFreqPlan.Init;
//-------------------------------------------------------------------
begin
  if not Assigned(dmAct_LinkFreqPlan) then
  begin
    dmAct_LinkFreqPlan:=TdmAct_LinkFreqPlan.Create(Application);

//    dmAct_LinkFreqPlan.GetInterface(IAct_LinkFreqPlan_X, IAct_LinkFreqPlan);
//    Assert(Assigned(IAct_LinkFreqPlan));

  end;
end;



//-------------------------------------------------------------------
procedure TdmAct_LinkFreqPlan.DataModuleCreate(Sender: TObject);
//-------------------------------------------------------------------
begin
  inherited;
  ObjectName:=OBJ_LINKFREQPLAN;

//  act_Create_freq_map.Caption:= '������� ����� ������. ������';
  act_Create_Tool_CIA.Caption:= '���'; //'CIA';

{  act_Audit.Caption:=STR_Audit;
  act_Audit.Enabled:=False;
}

  //-----------------------------------------------------------------
 /// act_Create_freq_map.Visible:= false;
  //-----------------------------------------------------------------

  SetActionsExecuteProc ([act_Create_freq_map,
                          act_Create_Tool_CIA

                       //    act_Audit

                          ],
                          DoAction);

 SetCheckedActionsArr([
        act_Add,
        act_Del_,
        act_Del_list,

        act_Create_Tool_CIA

//        act_Create_freq_map
     ]);


  FStrings:=TStringList.create;


end;




//--------------------------------------------------------------------
procedure TdmAct_LinkFreqPlan.AddByNet (aFolderID, aLinkNetID: integer);
//--------------------------------------------------------------------
begin
  FLinkNetID:= aLinkNetID;
  inherited Add (aFolderID);
end;


//--------------------------------------------------------------------
function TdmAct_LinkFreqPlan.ItemAdd (aFolderID: integer): integer;
//--------------------------------------------------------------------
begin
  Result:=Tdlg_LinkFreqPlan_add.ExecDlg (FLinkNetID);//, aFolderID);

  if Result>0 then
    g_Shell.UpdateNodeChildren_ByGUID(dmLinkFreqPlan.GetGUIDByID(Result));

//    sh_PostUpdateNodeChildren(dmLinkFreqPlan.GetGUIDByID(Result));
end;


//--------------------------------------------------------------------
function TdmAct_LinkFreqPlan.Dlg_AddForLinkNet (aLinkNetID, aFolderID: integer): integer;
//--------------------------------------------------------------------
begin
  Result:=Tdlg_LinkFreqPlan_add.ExecDlg (aLinkNetID); //, aFolderID);
end;


//--------------------------------------------------------------------
function TdmAct_LinkFreqPlan.ItemDel(aID: integer; aName: string = ''): boolean;
//--------------------------------------------------------------------
begin
//  FStrings.text:= Format('LinkFreqPlan.%d', [aID]);

 // PostUserMessage (WM__DEL_MAP_BY_MASK,  Integer( FStrings)  );

  Result:=dmLinkFreqPlan.Del (aID);


//  if Result then
//  begin
    FStrings.text:= Format('LinkFreqPlan.%d', [aID]);

    g_EventManager.PostEvent_(et_DEL_MAP_BY_MASK, [ 'Files', Integer( FStrings)]);
    
//    PostUserMessage (WM__DEL_MAP_BY_MASK,  Integer( FStrings)  );

//  end;



end;

//--------------------------------------------------------------------
// actions
//--------------------------------------------------------------------
procedure TdmAct_LinkFreqPlan.DoAction (Sender: TObject);
begin

{  if Sender=act_Audit then
     dlg_Audit (TBL_LinkFreqPlan, FFocusedID) else

}
  //-----------------------
  if Sender=act_Add then
  //-----------------------
    ItemAdd(FFolderID)
  else

  //-----------------------
  if Sender=act_Del_ then
  //-----------------------
    ItemDel(FFocusedID)
  else

 {
  //-----------------------
  if Sender=act_Create_freq_map then
  //-----------------------
  begin
    dmLinkFreqPlan_map_view.CreateFreqMap111 (FFocusedID);
  end else
  }


  //-----------------------
  if Sender=act_Create_Tool_CIA then
    Tfrm_LinkFreqPlan_CIA.ShowWindow(FFocusedID);

end;

//--------------------------------------------------------------------
function TdmAct_LinkFreqPlan.GetViewForm (aOwnerForm: TForm): TForm;
//--------------------------------------------------------------------
begin
  Result:=Tframe_LinkFreqPlan_view.Create(aOwnerForm);
end;


//--------------------------------------------------------------------
procedure TdmAct_LinkFreqPlan.GetPopupMenu;
//--------------------------------------------------------------------
begin
//  CheckActionsEnable();

  case aMenuType of
    mtFolder: begin
                AddFolderMenu_Create (aPopupMenu);
             //   AddFolderPopupMenu (aPopupMenu);
              end;

    mtItem: begin
                AddMenuItem (aPopupMenu, act_Create_Tool_CIA);
                AddMenuItem (aPopupMenu, nil);
              //  AddMenuItem (aPopupMenu, act_Create_freq_map);
              //  AddMenuItem (aPopupMenu, nil);
                AddMenuItem (aPopupMenu, act_Del_);

              //  AddMenuItem (aPopupMenu, act_Audit);


              end;
    mtList: begin
                AddMenuItem (aPopupMenu, act_Del_list);
            end;

  end;
end;

 
end.