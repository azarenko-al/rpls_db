unit f_LinkFreqPlan_CIA;

interface

uses
  Windows, SysUtils, Classes, Graphics, Controls, Forms,
   Menus, StdCtrls, ComCtrls, ActnList,  cxGraphics,
  ExtCtrls, cxSplitter,   Variants,
  cxGridCustomTableView,     Registry,
  cxInplaceContainer, cxTLData, cxGridTableView,
  cxClasses, cxControls, cxGridCustomView,  rxPlacemnt,
  cxTL, cxDBTL, cxGridDBTableView, cxGridLevel, cxGrid, Db, ADODB, dxmdaset,

  ToolWin, cxStyles, cxLookAndFeels, cxLookAndFeelPainters, dxSkinsCore,
  dxSkinsDefaultPainters, dxSkinscxPCPainter, cxCustomData, cxFilter,

  u_const_msg,


  MapXLib_TLB,



  f_map_Tool_base,

  dm_Onega_DB_data,

  I_MapAct,
  I_MapView,

  dm_Act_Map,

 // u_Map_Classes,


  dm_Main,

  u_func,
  u_db,
 // u_dx,


  u_geo,

  u_const_db,
  u_LinkFreqPlan_const,

  dm_LinkNet,
  dm_LinkEnd,

  dm_LinkFreqPlan_CIA_view,

  cxData, cxDataStorage, cxEdit, cxDBData, cxTextEdit, cxTLdxBarBuiltInMenu,
  cxPC;



type

  Tfrm_LinkFreqPlan_CIA = class(Tfrm_Map_Tool_base)
    ActionList1: TActionList;
    act_Stay_On_Top: TAction;
    bar_Region: TStatusBar;
    bar_EMP: TStatusBar;
    bar_FreqPlan: TStatusBar;
    Panel1: TPanel;
    mem_Data: TdxMemData;
    ds_Data: TDataSource;
    mem_TreeView: TdxMemData;
    ds_TreeView: TDataSource;
    act_Load_LinkEnd_List: TAction;
    qry_LinkEnd_NeighBors: TADOQuery;
    mem_Pair: TdxMemData;
    ds_Pair: TDataSource;
    act_Setup_TreeList: TAction;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    qry_Temp: TADOQuery;
    ADOStoredProc1: TADOStoredProc;
    DataSource1: TDataSource;
    cxStyleRepository1: TcxStyleRepository;
    cxStyle1_blue: TcxStyle;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Panel5: TPanel;
    cb_Map_Increase: TCheckBox;
    cxDBTreeList21331_pairs: TcxDBTreeList;
    col2_number: TcxDBTreeListColumn;
    col2_RX_LinkEnd_Name: TcxDBTreeListColumn;
    col2_TX_LinkEnd_Name: TcxDBTreeListColumn;
    col2_LinkEnd_ID: TcxDBTreeListColumn;
    col2_Distance_km: TcxDBTreeListColumn;
    col2_CI: TcxDBTreeListColumn;
    col2_Rx_Level_dBm: TcxDBTreeListColumn;
    col2_Neighbour_Count: TcxDBTreeListColumn;
    col2_Neighbour_Count1: TcxDBTreeListColumn;
    col2_is_expanded: TcxDBTreeListColumn;
    col2_type: TcxDBTreeListColumn;
    col2_tree_id: TcxDBTreeListColumn;
    col2_tree_parent_id: TcxDBTreeListColumn;
    col2_threshold_degradation_CIA: TcxDBTreeListColumn;
    col2_IsNeigh: TcxDBTreeListColumn;
    pn_Top: TPanel;
    Panel3: TPanel;
    cb_Only_Interf: TCheckBox;
    cxDBTreeList1: TcxDBTreeList;
    col1_LinkEnd_Name: TcxDBTreeListColumn;
    col1_Freq: TcxDBTreeListColumn;
    col1_rx_level: TcxDBTreeListColumn;
    col1_POWER_NEIGHBORS: TcxDBTreeListColumn;
    col1_distance_km: TcxDBTreeListColumn;
    col1_cia: TcxDBTreeListColumn;
    col1_ci: TcxDBTreeListColumn;
    col1_ca: TcxDBTreeListColumn;
    col1_Threshold_Degradation_cia: TcxDBTreeListColumn;
    col1_tree_id: TcxDBTreeListColumn;
    col1_Tree_parent_id: TcxDBTreeListColumn;
    pn_Bottom: TPanel;
    Panel2: TPanel;
    Button1: TButton;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    col3_LinkEnd: TcxGridDBColumn;
    col3_CI: TcxGridDBColumn;
    col3_CA: TcxGridDBColumn;
    col3_CIA: TcxGridDBColumn;
    col3_threshold_degradation_cia: TcxGridDBColumn;
    col3_linkend_id_bottom: TcxGridDBColumn;
    cxGrid1Level1: TcxGridLevel;
    cxSplitter: TcxSplitter;
    procedure FormDestroy(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);

  //  procedure dxDBGrid_BootomChangeNode(Sender: TObject; OldNode,
   //   Node: TdxTreeListNode);

    procedure act_Load_LinkEnd_ListExecute(Sender: TObject);
  //  procedure act_Setup_TreeListExecute(Sender: TObject);
   // procedure dxDBTreeList_TopHotTrackNode(Sender: TObject;
  //    AHotTrackInfo: TdxTreeListHotTrackInfo; var ACursor: TCursor);
//    procedure dxDBTreeList_TopCustomDrawCell(Sender: TObject;
//      ACanvas: TCanvas; ARect: TRect; ANode: TdxTreeListNode;
//      AColumn: TdxTreeListColumn; ASelected, AFocused,
//      ANewItemRow: Boolean; var AText: String; var AColor: TColor;
//      AFont: TFont; var AAlignment: TAlignment; var ADone: Boolean);
    procedure cb_Only_InterfClick(Sender: TObject);
//    procedure cxDBTreeList1CustomDrawCell(Sender: TObject; ACanvas: TcxCanvas;
 //       AViewInfo: TcxTreeListEditCellViewInfo; var ADone: Boolean);
//    procedure cxDBTreeList1Expanding(Sender: TcxCustomTreeList; ANode:
//        TcxTreeListNode; var Allow: Boolean);
//    procedure cxDBTreeList1FocusedNodeChanged(Sender: TcxCustomTreeList;
 //       APrevFocusedNode, AFocusedNode: TcxTreeListNode);
//    procedure cxDBTreeList211CustomDrawCell(Sender: TObject; ACanvas: TcxCanvas; AViewInfo:
 //       TcxTreeListEditCellViewInfo; var ADone: Boolean);
//    procedure cxDBTreeList211Expanding(Sender: TObject; ANode: TcxTreeListNode; var
 //       Allow: Boolean);
    procedure cxDBTreeList21331_pairsFocusedNodeChanged(Sender: TcxCustomTreeList;
        APrevFocusedNode, AFocusedNode: TcxTreeListNode);
//    procedure cxDBTreeList211FocusedNodeChanged(Sender: TObject; APrevFocusedNode, AFocusedNode:
//        TcxTreeListNode);
    procedure cxGrid1DBTableView1CustomDrawCell(Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
        AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
    procedure cxGrid1DBTableView1FocusedRecordChanged(Sender: TcxCustomGridTableView;
        APrevFocusedRecord, AFocusedRecord: TcxCustomGridRecord; ANewItemRecordFocusingChanged:
        Boolean);
    procedure GSPagesClick(Sender: TObject);
  //  procedure dxDBTreeListChangeNode(Sender: TObject; OldNode,
  //    Node: TdxTreeListNode);
//    procedure dxDBTreeListCustomDrawCell(Sender: TObject; ACanvas: TCanvas; ARect:
//        TRect; ANode: TdxTreeListNode; AColumn: TdxTreeListColumn; ASelected,
//        AFocused, ANewItemRow: Boolean; var AText: string; var AColor: TColor;
//        AFont: TFont; var AAlignment: TAlignment; var ADone: Boolean);
//    procedure dxDBTreeListExpanding(Sender: TObject; Node: TdxTreeListNode; var
//        Allow: Boolean);

    procedure LoadLinkEnd(aID: Integer);
    procedure PageControl1Change(Sender: TObject);
    procedure cxDBTreeList1StylesGetContentStyle(Sender: TcxCustomTreeList;
      AColumn: TcxTreeListColumn; ANode: TcxTreeListNode;
      var AStyle: TcxStyle);
    procedure cxDBTreeList21331_pairsExpanded(Sender: TcxCustomTreeList; ANode:
        TcxTreeListNode);
    procedure cxDBTreeList21331_pairsExpanding(Sender: TcxCustomTreeList; ANode:
        TcxTreeListNode; var Allow: Boolean);
  //  procedure pn_BottomClick(Sender: TObject);

  private
  //  FProcIndex: integer;


    FID  : integer;
    FLinkEndID: integer;
    FLinkNetID: integer;

    FAdmitted_CI : double; // ���������� ������� ������
    FAdmitted_Degradation: double; //���������� ���������� ����������������
    FIsUpdated: boolean;

    procedure Show_CIA (aLinkEndID: integer);
    procedure SetLinkFreqPlan   (aLinkFreqPlanID: integer);
    procedure LoadLinkEndList();



    procedure LoadInterference(aLinkEndID: integer);
    procedure Load_Pair_Vliyan();

    procedure ShowPairVliyan(aLinkEnd1, aLinkEnd2: Integer);
 //   procedure DoOnMapSelection(Sender: TObject);
//    procedure DoOnMapSelection(Sender: TObject);

  protected
//    procedure DoOnMapEvent(Sender: TObject; aEventIndex: integer); override;

  public

    class procedure ShowWindow (aLinkFreqPlanID: integer);

  end;



//=========================================================
implementation {$R *.DFM}
//=========================================================

const
  COLOR_CI_SECTOR    = clRed;

  CAPTION_Rec_No                = '�';
  CAPTION_RX_LINKEND_Name       = 'RX ���';
  CAPTION_TX_LINKEND_Name       = 'TX ���';
  CAPTION_Distance_km           = 'R [km]';
  CAPTION_Distance_m            = 'R [m]';
  CAPTION_rx_Level_dBm          = '������ [dBm]';
  CAPTION_INTERFERENCE          = '������ [dBm]';
  CAPTION_CI                    = 'CI [dB]';
  CAPTION_Neighbor_count        = '���-�� "��������" �������';
  CAPTION_Neighbor_count1       = '���-�� "��������" �������';
  CAPTION_threshold_degradation = '���������� ���������������� [dB]';
  CAPTION_FREQ                  = '������� TX/RX [MHz]';
  CAPTION_LINKEND_NAME          = '���';

  CAPTION_RX_LEVEL = 'Rx Level';


{
TODO : �������� ������������� !!! }

(*  SQL_VIEW_LINKEND_NEIGHBORS =
    'SELECT * FROM '+ view_LinkFreqPlan_Neighbors +
    ' WHERE (LinkFreqPlan_id=:LinkFreqPlan_id) AND '+
    '       (rx_link_id=:rx_link_id)';
*)

//  REG_SECTION = 'tool_from_CIA_LinkFreqPlan';

  LAYER_ARROW  = 'Temp_Arrow';
  LAYER_BEZIER = 'Temp_Bezier';


//---------------------------------------------------
class procedure Tfrm_LinkFreqPlan_CIA.ShowWindow(aLinkFreqPlanID: integer);
//---------------------------------------------------
var
  oForm: Tfrm_LinkFreqPlan_CIA;
  vIMapView: IMapViewX;
{

  vLayer_bezier: CMapxLayer;
  vLayer_arrow: CMapxLayer;
  }

begin
  dmAct_Map.ShowForm();

(*  if not Assigned(IActiveMapView) then
//    if Assigned(IMapAct) then
      dmAct_Map.ShowForm()
    else
      Exit;
*)


{  Assert(Assigned(IMapAct));
  if IMapAct
}

  oForm:=Tfrm_LinkFreqPlan_CIA(IsFormExists (Application, Tfrm_LinkFreqPlan_CIA.ClassName));

  if not Assigned(oForm) then
  begin
    vIMapView:=IActiveMapView;

    oForm:=Tfrm_LinkFreqPlan_CIA.Create(Application);
    oForm.SetIMapView(vIMapView);

  //  oForm.IMapView:=IMainMapView;
  end;

  oForm.Show;



  with oForm do
  begin
//    vLayer_bezier:=FIMapView.TempLayer_Create(LAYER_BEZIER);
//    vLayer_arrow:=FIMapView.TempLayer_Create(LAYER_ARROW);


//    Assert(vLayer_bezier <> nil);

   // FProcIndexOnSelect:=

//    FProcIndex:=FIMapView.RegisterNotifyEvent(oForm, DoOnMapEvent);


   { if not Assigned(IMapView) then begin
      Free;
      Exit;
    end;}


 //   IMapView:=IMapViewX(dmObject_Manager.IMainMap);

//    IMapView.MAP_CREATE_TEMP_LAYER(LAYER_BEZIER);
  //  IMapView.MAP_CREATE_TEMP_LAYER(LAYER_ARROW);

 //   PostEvent(WE_MAP_CREATE_TEMP_LAYER,  [app_Par(PAR_NAME, LAYER_BEZIER)]);
 //   PostEvent(WE_MAP_CREATE_TEMP_LAYER,  [app_Par(PAR_NAME, LAYER_ARROW)]);

//    FMapViewIndex:=IMapView.UnRegisterFreeNotifyEvent(FMapViewIndex);


    SetLinkFreqPlan (aLinkFreqPlanID);

  end;


end;

//-----------------------------------------------
procedure Tfrm_LinkFreqPlan_CIA.FormCreate(Sender: TObject);
//-----------------------------------------------
begin
  inherited;

  Caption:= '���';

  db_SetComponentADOConnection (Self, dmMain.ADOConnection);

  cxGrid1.Align:=alClient;


  pn_Top.Align:=alClient;
  PageControl1.Align      := alClient;
//  pn_Pair.Align      := alClient;

  cxDBTreeList1.Align := alClient;
  cxDBTreeList21331_pairs.Align := alClient;


  col3_linkend_id_bottom.visible:=False;

  col1_rx_level.Caption.Text   := CAPTION_RX_LEVEL;


  {
  dxDBTreeList.Align := alClient;
  dxDBGrid_Bootom.Align    := alClient;
  dxDBTreeList_Top.Align:= alClient;


//  cxDBTreeList_Top.Align:=alClient;
 //aa cxGrid2.Align:=alClient;
//  cxDBTreeList1.Align:=alClient;

  dxDBTreeList.LoadFromRegistry  (FRegPath + dxDBTreeList.Name);
  dxDBTreeList_Top.LoadFromRegistry (FRegPath + dxDBTreeList_Top.Name);
  }


  cxDBTreeList21331_pairs.RestoreFromRegistry (FRegPath + cxDBTreeList21331_pairs.name);



  col1_LinkEnd_Name.Caption.Text             := CAPTION_LINKEND_NAME;
  col1_Freq.Caption.Text                     := CAPTION_FREQ;
 // col1_Rx_Level_dBm.Caption.Text             := CAPTION_Rx_Level_dBm;
  col1_Power_Neighbors.Caption.Text          := CAPTION_INTERFERENCE;
  col1_Distance_km.Caption.Text              := CAPTION_Distance_km;
  col1_CI.Caption.Text                      := 'CI';
  col1_CA.Caption.Text                      := 'CA';
  col1_CIA.Caption.Text                     := 'CIA';
  col1_threshold_degradation_CIA.Caption.Text:= CAPTION_threshold_degradation;

  //cx
  //---------------------------------------------------------
  col2_threshold_degradation_cia.Caption.Text:= CAPTION_threshold_degradation;
//  col1_RecNo.Caption.Text                    := CAPTION_Rec_No;
  col2_RX_LinkEnd_Name.Caption.Text          := CAPTION_RX_LINKEND_Name;
  col2_TX_LinkEnd_Name.Caption.Text          := CAPTION_TX_LINKEND_Name;
  col2_Distance_km.Caption.Text              := CAPTION_Distance_km;
  col2_CI.Caption.Text                      := CAPTION_CI;
  col2_rx_Level_dBm.Caption.Text             := CAPTION_Rx_Level_dBm;
//  col2_Interference.Caption.Text             := CAPTION_INTERFERENCE;
  col2_Neighbour_Count.Caption.Text          := CAPTION_Neighbor_count;
  col2_Neighbour_Count1.Caption.Text         := CAPTION_Neighbor_count1;
//  col2_threshold_degradation_CIA.Caption.Text    := CAPTION_threshold_degradation;

(*
  //---------------------------------------------------------
  col2_number.Caption.Text                  := Caption_Rec_No;
  col2_RX_LinkEnd_Name.Caption.Text         := Caption_RX_LINKEND_Name;
  col2_TX_LinkEnd_Name.Caption.Text         := Caption_TX_LINKEND_Name;
  col2_Distance_km.Caption.Text              := Caption_Distance_km;
  col2_CI.Caption.Text                      := Caption_CI;
  col2_rx_Level_dBm.Caption.Text            := Caption_rx_Level_dBm;
  col2_Interference.Caption.Text            := Caption_INTERFERENCE;
  col2_Neighbour_Count.Caption.Text         := Caption_Neighbor_count;
  col2_Neighbour_Count1.Caption.Text        := Caption_Neighbor_count1;
  col2_threshold_degradation.Caption.Text   := Caption_threshold_degradation;
*)


  col3_CI.Caption                        := 'CI';
  col3_CA.Caption                        := 'CA';
  col3_CIA.Caption                       := 'CIA';
  col3_threshold_degradation_cia.Caption := CAPTION_threshold_degradation;



  dmLinkFreqPlan_CIA_view.InitDB(mem_TreeView);
  dmLinkFreqPlan_CIA_view.InitDB(mem_Pair);
  dmLinkFreqPlan_CIA_view.InitData(mem_Data);

//  IMapView.MAP_CREATE_TEMP_LAYER(LAYER_BEZIER);
//  IMapView.MAP_CREATE_TEMP_LAYER(LAYER_ARROW);


 // cxGridDBTableView1.RestoreFromRegistry(FRegPath + cxGridDBTableView1.Name);
//  cxDBTreeList_Top.RestoreFromRegistry(FRegPath + cxDBTreeList_Top.Name);
 // cxDBTreeList1.RestoreFromRegistry(FRegPath + cxDBTreeList1.Name);


  FIsUpdated:=True;
  PageControl1.ActivePageIndex:= 0;
  FIsUpdated:=False;


   with TRegIniFile.Create(FRegPath) do
   begin

    cb_Only_Interf.Checked :=ReadBool('',cb_Only_Interf.Name, False );
    cb_Map_Increase.Checked:=ReadBool('',cb_Map_Increase.Name, False );

    pn_Bottom.Height:=ReadInteger('',pn_Bottom.Name, 100 );
    Free;
  end;

(*
  col_tree_id1.Visible := False;
  col_tree_parent_id1.Visible := False;
  col_LinkEnd_ID_.Visible := False;
  col_RecNo.Visible := False;

*)

{
  PostEvent(WE_MAP_CREATE_TEMP_LAYER,  [app_Par(PAR_NAME, LAYER_BEZIER)]);
  PostEvent(WE_MAP_CREATE_TEMP_LAYER,  [app_Par(PAR_NAME, LAYER_ARROW)]);
}
end;

//-----------------------------------------------
procedure Tfrm_LinkFreqPlan_CIA.FormDestroy(Sender: TObject);
//-----------------------------------------------
begin
 // cxGridDBTableView1.StoreToRegistry(FRegPath + cxGridDBTableView1.Name);
 // cxDBTreeList_Top.StoreToRegistry(FRegPath + cxDBTreeList_Top.Name);
//  cxDBTreeList1.StoreToRegistry(FRegPath + cxDBTreeList1.Name);


  cxDBTreeList21331_pairs.StoreToRegistry (FRegPath + cxDBTreeList21331_pairs.name);


   with TRegIniFile.Create(FRegPath) do
   begin
    WriteBool('',cb_Only_Interf.Name, cb_Only_Interf.Checked );
    WriteBool('',cb_Map_Increase.Name, cb_Map_Increase.Checked );

    WriteInteger('',pn_Bottom.Name, pn_Bottom.Height );

    Free;
  end;


//  UnRegisterProc (FProcIndex);
//  IMapView.UnRegisterFreeNotifyEvent(FMapViewIndex);
//  IMainMapView.UnRegisterSender(FProcIndex);

//  IMapView.UnRegisterNotifyEvent(MAP_EVENT_ON_SELECT, FProcIndexOnSelect);


//  gl_Reg.SaveAndClearGroup (Self);

(*
  dxDBTreeList.SaveToRegistry (FRegPath + dxDBTreeList.Name);
  dxDBTreeList_Top.SaveToRegistry (FRegPath + dxDBTreeList_Top.Name);
*)


  inherited;
end;

//------------------------------------------------------------
procedure Tfrm_LinkFreqPlan_CIA.Load_Pair_Vliyan();
//------------------------------------------------------------
var i: integer;
begin
  FIsUpdated :=True;

  dmLinkFreqPlan_CIA_view.Load_LinkFreqPlan(mem_Pair, FID);  //FID-LinkFreqPlan

  if mem_Pair.IsEmpty then
  begin
    bar_EMP.Panels[0].Text:='����� �������: 0';
    Exit;
  end;

  bar_EMP.Panels[0].Text:=Format('����� �������: %d', [mem_Pair.RecordCount]);

(*
  for i := 0 to dxDBTreeList.Count - 1 do
    with dxDBTreeList.Items[i] do
      HasChildren := (Values[col_Neighbors_Count.Index] > 0);
*)

 // cxDBTreeList211_pairs


  for i := 0 to cxDBTreeList21331_pairs.Count - 1 do
    with cxDBTreeList21331_pairs.Items[i] do
      HasChildren := (Values[col2_Neighbour_Count.ItemIndex] > 0);


  FIsUpdated :=False;


(*
  for i := 0 to dxDBTreeList.Count - 1 do
    with dxDBTreeList.Items[i] do
      HasChildren := (Values[col_Neighbors_Count.Index] > 0);


*)

end;


//------------------------------------------------------------
procedure Tfrm_LinkFreqPlan_CIA.LoadLinkEndList();
//------------------------------------------------------------
begin
  dmOnega_DB_data.OpenQuery(qry_Temp,
        'SELECT * FROM '+ VIEW_LINKFREQPLAN_CIA +
        ' WHERE LinkFreqPlan_id=:LinkFreqPlan_id',
        [FLD_LINKFREQPLAN_ID, FID]);

  if qry_Temp.IsEmpty then
  begin
    bar_EMP.Panels[0].Text:='����� �������: 0';
    Exit;
  end;

//  CursorHourGlass;

  mem_Data.Close;
  mem_Data.Open;
  mem_Data.DisableControls;

  with qry_Temp do
    while not Eof do
  begin
    if ((qry_Temp[FLD_CIA]<>null) and
        (FieldByName(FLD_CIA).AsFloat < FAdmitted_CI)) or
        (FieldByName(FLD_THRESHOLD_DEGRADATION_CIA).AsFloat > FAdmitted_Degradation) then
    begin
      mem_Data.Append;

      db_SetFieldsValues (qry_Temp, mem_Data);

      mem_Data.Post;
    end;

    Next;
  end;

  mem_Data.First;
  mem_Data.EnableControls;

  bar_EMP.Panels[0].Text:=Format('����� �������: %d', [mem_Data.RecordCount]);

 // CursorDefault;
end;

//------------------------------------------------------
procedure Tfrm_LinkFreqPlan_CIA.LoadInterference(aLinkEndID: integer);
//------------------------------------------------------
begin
  FLinkEndID:= aLinkEndID;
  if FLinkEndID <= 0 then
  begin
    db_Clear(mem_TreeView);
    Exit;
  end;

  dmLinkFreqPlan_CIA_view.Params.IsOnly_Interference:= cb_Only_Interf.Checked;
  dmLinkFreqPlan_CIA_view.OpenDB(mem_TreeView, FID, FLinkEndID);

  {
  with dmLinkFreqPlan_CIA_view do
  begin
    Params.IsOnly_Interference:= cb_Only_Interf.Checked;

    OpenDB(mem_TreeView, FID, FLinkEndID);
  end;    // with
  }

  mem_TreeView.First;

  Show_CIA(FLinkEndID);
end;

//--------------------------------------------------
procedure Tfrm_LinkFreqPlan_CIA.Show_CIA (aLinkEndID: integer);
//--------------------------------------------------
const
  STYLE_LINE  = 1;
  STYLE_ARROW = 2;

   //�������� �� ����� ����� �� ��������� ��������
   procedure DoShowLineOnMap (Point1,Point2: TBLPoint; aColor, aStyle:integer;
                               aLabel: string; aWidth: integer=1);
   begin
     FIMapView.MAP_ADD_LAYER_LINE (LAYER_ARROW,  MakeBLVector(Point1, Point2),
                 aColor, IIF(aStyle=STYLE_LINE, -1, 55), aWidth, aLabel);
  end;


const
  SQL_VIEW_LINKEND_NEIGHBORS =
    'SELECT * FROM '+ view_LinkFreqPlan_Neighbors +
    ' WHERE (LinkFreqPlan_id=:LinkFreqPlan_id) AND '+
 //   '       (rx_link_id=:rx_link_id)';
    '       (rx_linkend_id=:rx_linkend_id)';

var
  blPoint1, blPoint2: TBLPoint;
  dCIA, dCI, dCA :double;
  sLabel: string;
  iLinkEnd2ID: integer;

  blPointArr: TBLPointArrayF;
  iID1: Integer;



  vLayer_bezier: CMapxLayer;
  vLayer_arrow: CMapxLayer;

begin
  vLayer_bezier:=FIMapView.TempLayer_Create(LAYER_BEZIER);
  vLayer_arrow :=FIMapView.TempLayer_Create(LAYER_ARROW);


  FIMapView.TempLayer_Clear(LAYER_ARROW);


  dmOnega_DB_data.OpenQuery(qry_LinkEnd_NeighBors, SQL_VIEW_LINKEND_NEIGHBORS,
                        [FLD_LINKFREQPLAN_ID, FID,
                         FLD_RX_LINKEND_ID, aLinkEndID]);

  if qry_LinkEnd_NeighBors.IsEmpty then
    Exit;

  blPoint1.B:= qry_LinkEnd_NeighBors.FieldByName(FLD_LAT1).AsFloat;
  blPoint1.L:= qry_LinkEnd_NeighBors.FieldByName(FLD_LON1).AsFloat;

  iLinkEnd2ID := dmOnega_DB_data.LinkEnd_Get_Next_LinkEnd_ID (aLinkEndID);
  //dmLinkEnd.GetAnotherLinkEndID(aLinkEndID);


//  Result:= dmOnega_DB_data.LinkEnd_Get_Next_LinkEnd_ID (aID);

  blPoint2:= dmLinkEnd.GetPropertyPos(iLinkEnd2ID);

  // ��������� "��������" ��� ���������� ���������
  DoShowLineOnMap (blPoint1, blPoint2, 1004429, STYLE_ARROW, '', 3);


 // db_ViewDataSet(qry_LinkEnd_NeighBors);

  FIMapView.TempLayer_Clear (LAYER_BEZIER);


  with qry_LinkEnd_NeighBors do
    while not Eof do
  begin
    // ���� ��� ������ -> �� ����������
    if (qry_LinkEnd_NeighBors[FLD_CIA] = NULL) and
       (qry_LinkEnd_NeighBors[FLD_CI_] = NULL) and
       (qry_LinkEnd_NeighBors[FLD_CA]  = NULL) and
       (qry_LinkEnd_NeighBors[FLD_THRESHOLD_DEGRADATION_CIA] = NULL) then
    begin
      Next; Continue;
    end;

    blPoint2.B:= FieldByName(FLD_LAT2).AsFloat;
    blPoint2.L:= FieldByName(FLD_LON2).AsFloat;

    sLabel:= '';
    dCIA:= FieldByName(FLD_CIA).AsFloat;
    dCI := FieldByName(FLD_CI_).AsFloat;
    dCA := FieldByName(FLD_CA).AsFloat;

    if dCIA<>0 then sLabel:= sLabel+Format('CIA=%3.2fdB ', [dCIA]);
    if dCI<>0  then sLabel:= sLabel+Format('CI=%3.2fdB ', [dCI]);
    if dCA<>0  then sLabel:= sLabel+Format('CA=%3.2fdB',  [dCA]);

    if (qry_LinkEnd_NeighBors[FLD_CIA] < FAdmitted_CI) or
       (qry_LinkEnd_NeighBors[FLD_THRESHOLD_DEGRADATION_CIA] > FAdmitted_Degradation)
    then
    begin
      DoShowLineOnMap (blPoint1, blPoint2, 7843316, STYLE_LINE, sLabel);

      iID1:=qry_LinkEnd_NeighBors[FLD_TX_LINKEND_ID];

      dmLinkFreqPlan_CIA_view.Get_Pair_Vliyan(iID1, aLinkEndID, blPointArr);

      FIMapView.MAP_ADD_LAYER_BEZIER (LAYER_BEZIER, blPointArr, clRed);

    end
    else begin
      if not cb_Only_Interf.Checked then
        DoShowLineOnMap (blPoint1, blPoint2, 7843316, STYLE_LINE, sLabel)
      else
        begin Next; Continue; end;
    end;

    Next;
  end;
end;

//--------------------------------------------------
procedure Tfrm_LinkFreqPlan_CIA.SetLinkFreqPlan (aLinkFreqPlanID: integer);
//--------------------------------------------------
begin
  FID:= aLinkFreqPlanID;
  if FID <= 0 then begin
    Close; Exit;
  end;

  db_OpenQueryByFieldValue(qry_Temp, TBL_LINKFREQPLAN, FLD_ID, FID);

  FAdmitted_CI          := qry_Temp.FieldByName(FLD_MIN_CI_dB).AsFloat;
//  FAdmitted_CI          := qry_Temp.FieldByName(FLD_MIN_CI).AsFloat;
  FLinkNetID            := qry_Temp.FieldByName(FLD_LINKNET_ID).AsInteger;
  FAdmitted_Degradation := qry_Temp.FieldByName(FLD_threshold_degradation).AsFloat;

  bar_FreqPlan.Panels[1].Text:= qry_Temp.FieldByName(FLD_NAME).AsString;

  dmLinkFreqPlan_CIA_view.Params.Admitted_CI:= FAdmitted_CI;
  dmLinkFreqPlan_CIA_view.Params.Admitted_Degradation:= FAdmitted_Degradation;

  bar_EMP.Panels[1].Text:=Format ('���� CI: %f', [FAdmitted_CI]);
  bar_EMP.Panels[2].Text:=Format ('���� ������.[dB]: %f', [FAdmitted_Degradation]);

  bar_Region.Panels[1].Text:= dmLinkNet.GetNameByID(FLinkNetID);
end;

//-------------------------------------------------------------------
procedure Tfrm_LinkFreqPlan_CIA.FormClose(Sender: TObject; var Action: TCloseAction);
//-------------------------------------------------------------------
begin
  FIMapView.RemoveLayerByName(LAYER_BEZIER);
  FIMapView.RemoveLayerByName(LAYER_ARROW);

  Action :=caFree;

  inherited;
end;

//-------------------------------------------------------------------
procedure Tfrm_LinkFreqPlan_CIA.act_Load_LinkEnd_ListExecute(Sender: TObject);
//-------------------------------------------------------------------
begin
  inherited;

  LoadLinkEndList;
end;



//-------------------------------------------------------------------
procedure Tfrm_LinkFreqPlan_CIA.cb_Only_InterfClick(Sender: TObject);
//-------------------------------------------------------------------
begin
  inherited;

  LoadInterference(FLinkEndID);
end;




procedure Tfrm_LinkFreqPlan_CIA.cxDBTreeList21331_pairsFocusedNodeChanged(Sender:
    TcxCustomTreeList; APrevFocusedNode, AFocusedNode: TcxTreeListNode);



// ---------------------------------------------------------------
//procedure Tfrm_LinkFreqPlan_CIA.cxDBTreeList1FocusedNodeChanged(Sender:
//    TcxCustomTreeList; APrevFocusedNode, AFocusedNode: TcxTreeListNode);
// ---------------------------------------------------------------
//begin
//  inherited;
//end;


//------------------------------------------------------------
//procedure Tfrm_LinkFreqPlan_CIA.cxDBTreeList211FocusedNodeChanged(Sender: TObject; APrevFocusedNode, AFocusedNode: TcxTreeListNode);
//------------------------------------------------------------
{var
  blPointArr: TBLPointArrayF;
  blRect: TblRect;
  }
  
var
  iLinkEnd_ID: integer;
  iTree_parent_id: integer;
begin
  if FIsUpdated then
    Exit;


  if (mem_Pair.IsEmpty) then
    Exit;


  if (AFocusedNode.Values[col2_tree_parent_id.ItemIndex] = 0) then
  begin
    FIMapView.TempLayer_Clear (LAYER_BEZIER);

    Exit;
  end;


  iLinkEnd_ID     :=AsInteger(AFocusedNode.Values[col2_LinkEnd_ID.ItemIndex]);
  iTree_parent_id :=AsInteger(AFocusedNode.Values[col2_tree_parent_id.ItemIndex]);


//  iLinkEnd_ID     :=AsInteger(Node.Values[col_LinkEnd_ID_.Index]);
//  iTree_parent_id :=AsInteger(Node.Values[col_tree_parent_id1.Index]);

  if iTree_parent_id >0 then
    ShowPairVliyan(iLinkEnd_ID, iTree_parent_id);



//  Assert(iLinkEnd_ID>0, 'Value <=0');
 // Assert(iTree_parent_id>0, 'Value <=0');

  /////////////////ShowPairVliyan(iLinkEnd_ID,itree_parent_id);


{  dmLinkFreqPlan_CIA_view.Show_Pair_Vliyan(Node.Values[col_LinkEnd_ID.Index],
                                           Node.Values[col_tree_parent_id1.Index],
                                           blPointArr);
                                           //cb_Map_Increase.Checked);

  if cb_Map_Increase.Checked then
  begin
   // FBLPoints.MakeBLPointArray(blPointArr);
    blRect:= geo_DefineRoundBLRect(blPointArr);//blPointArr);
    PostBLRect(WE_MAP_SHOW_RECT, blRect);
  end;


  PostEvent(WE_MAP_CLEAR_TEMP_LAYER, [app_Par(PAR_NAME, LAYER_BEZIER)]);

  PostEvent(WE_MAP_ADD_TEMP_LAYER_BEZIER,
                   [app_Par(PAR_NAME, LAYER_BEZIER),
                    app_Par(PAR_BLPOINT_ARR,  @blPointArr),
                    app_Par(PAR_COLOR,         clRed)
                   ]);
}
{
 //----------------------�������� �� ����� ����� --------------------
  procedure DoShowLineOnMap (var aBLPoints: TBLPointArrayF; aColor:integer);
  // ---------------------------------------------------------------
  begin
    PostEvent(WE_MAP_CLEAR_TEMP_LAYER, [app_Par(PAR_NAME, LAYER_BEZIER)]);

    PostEvent(WE_MAP_ADD_TEMP_LAYER_BEZIER,
                     [app_Par(PAR_NAME, LAYER_BEZIER),
                      app_Par(PAR_BLPOINT_ARR,  @aBLPoints),
                      app_Par(PAR_COLOR,         aColor)
                     ]);
  end;
}
  //DoShowLineOnMap (blPointArr, clRed);

end;



procedure Tfrm_LinkFreqPlan_CIA.cxGrid1DBTableView1CustomDrawCell(Sender: TcxCustomGridTableView;
    ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
//var
//  i: integer;
begin
  inherited;

//  ACanvas.

//  if AViewInfo.GridRecord.Values[col__Is_Added.Index] = True then

{
  i:=col__Tree_parent_id1.ItemIndex;

  if AViewInfo.GridRecord.Values[col__Tree_parent_id1.ItemIndex] = 0 then
//  if ANode.Values[col_Tree_parent_id.Index] = 0 then
  begin
    ACanvas.Brush.Color:=1004429;
    ACanvas.Font.Color:=clWhite;
  end
  else

  if ((AViewInfo.GridRecord.Values[col__CIA1.ItemIndex] <> null) and
      (AViewInfo.GridRecord.Values[col__CIA1.ItemIndex] < FAdmitted_CI)) or
     ((AViewInfo.GridRecord.Values[col__Threshold_Degradation_cia1.ItemIndex] <> null) and
      (AViewInfo.GridRecord.Values[col__Threshold_Degradation_cia1.ItemIndex] > FAdmitted_Degradation)) then
  begin
    ACanvas.Brush.Color:=clRed; // $008080FF;
    ACanvas.Font.Color:=clWhite;
  end;
}

end;



//-------------------------------------------------------------------
procedure Tfrm_LinkFreqPlan_CIA.cxGrid1DBTableView1FocusedRecordChanged(Sender:
    TcxCustomGridTableView; APrevFocusedRecord, AFocusedRecord: TcxCustomGridRecord;
    ANewItemRecordFocusingChanged: Boolean);
//-------------------------------------------------------------------

var
  i_linkend_id: Integer;
begin
  if mem_Data.IsEmpty then
    Exit;

//  i_linkend_id:=AsInteger(AFocusedRecord.Values[col3_linkend_id_bottom.Index]);

  if Assigned(AFocusedRecord) then
  begin
    i_linkend_id:=AsInteger(AFocusedRecord.Values[col3_linkend_id_bottom.Index]);

    LoadInterference(i_linkend_id);
  end;

end;




procedure Tfrm_LinkFreqPlan_CIA.cxDBTreeList1StylesGetContentStyle(
  Sender: TcxCustomTreeList; AColumn: TcxTreeListColumn;
  ANode: TcxTreeListNode; var AStyle: TcxStyle);
begin
  inherited;
//
end;





//-------------------------------------------------------------------
procedure Tfrm_LinkFreqPlan_CIA.ShowPairVliyan(aLinkEnd1, aLinkEnd2: Integer);
//-------------------------------------------------------------------
var
  blPointArr: TBLPointArrayF;
  blRect: TblRect;

  vLayer_bezier: CMapxLayer;
  vLayer_arrow: CMapxLayer;
begin
  if aLinkEnd2=0 then
    Exit;


  vLayer_bezier:=FIMapView.TempLayer_Create(LAYER_BEZIER);
  vLayer_arrow :=FIMapView.TempLayer_Create(LAYER_ARROW);



  dmLinkFreqPlan_CIA_view.Get_Pair_Vliyan (aLinkEnd1, aLinkEnd2, blPointArr);

  if cb_Map_Increase.Checked then
  begin
   // FBLPoints.MakeBLPointArray(blPointArr);
 //   blRect:= geo_DefineRoundBLRect(blPointArr);//blPointArr);
    blRect := geo_RoundBLPointsToBLRect_F(blPointArr);


    FIMapView.MAP_SHOW_BLRECT(blRect);
  //  PostBLRect(WE_MAP_SHOW_RECT, blRect);
  end;

  FIMapView.TempLayer_Clear (LAYER_BEZIER);
  FIMapView.MAP_ADD_LAYER_BEZIER (LAYER_BEZIER, blPointArr, clRed);

 // PostEvent(WE_MAP_CLEAR_TEMP_LAYER, [app_Par(PAR_NAME, LAYER_BEZIER)]);

//  IMapView.MAP_ADD_TEMP_LAYER_BEZIER (LAYER_BEZIER, blPointArr, clRed);

 {   PostEvent(WE_MAP_ADD_TEMP_LAYER_BEZIER,
                     [app_Par(PAR_NAME,         LAYER_BEZIER),
                      app_Par(PAR_BLPOINT_ARR,  @blPointArr),
                      app_Par(PAR_COLOR,        clRed)
                     ]);
}
 // PostEvent(WE_ADD_TEMP_LAYER_BEZIER, [app_Par(PAR_NAME, LAYER_BEZIER),
  //                                     ]);

end;

//-------------------------------------------------------------------
procedure Tfrm_LinkFreqPlan_CIA.GSPagesClick(Sender: TObject);
//-------------------------------------------------------------------
begin
  inherited;

  if FIsUpdated then
    Exit;


  case PageControl1.ActivePageIndex of
    0: begin
         bar_EMP.Panels[0].Text:=Format('����� �������: %d', [mem_Data.RecordCount]);

         FIMapView.TempLayer_Clear(LAYER_BEZIER);
//         PostEvent(WE_MAP_CLEAR_TEMP_LAYER,  [app_Par(PAR_NAME, LAYER_BEZIER)]);

         if not mem_Data.IsEmpty then
           LoadInterference(mem_Data.FieldValues[FLD_LINKEND_ID])
         else
           db_Clear(mem_TreeView);

       end;
    1: begin
         Load_Pair_Vliyan();


         FIMapView.TempLayer_Clear(LAYER_ARROW);
//         PostEvent(WE_MAP_CLEAR_TEMP_LAYER,  [app_Par(PAR_NAME, LAYER_ARROW)]);
       end;
  end;

end;



procedure Tfrm_LinkFreqPlan_CIA.LoadLinkEnd(aID: Integer);
begin
      case PageControl1.ActivePageIndex of

        0: LoadInterference (aID);


{        1:   if (dxDBTreeList.FocusedNode.Values[col_tree_parent_id1.Index] > 0)
             then
               ShowPairVliyan(
//               dmLinkFreqPlan_CIA_view.Show_Pair_Vliyan(
                     dxDBTreeList.FocusedNode.Values[col_tree_parent_id1.Index],
                     dxDBTreeList.FocusedNode.Values[col_LinkEnd_ID.Index]

                    // cb_Map_Increase.Checked
                     );
}
      end;
end;

// ---------------------------------------------------------------
procedure Tfrm_LinkFreqPlan_CIA.PageControl1Change(Sender: TObject);
// ---------------------------------------------------------------
begin
  inherited;

  if FIsUpdated then
    Exit;


  case PageControl1.ActivePageIndex of
    0: begin
         bar_EMP.Panels[0].Text:=Format('����� �������: %d', [mem_Data.RecordCount]);

         FIMapView.TempLayer_Clear(LAYER_BEZIER);
//         PostEvent(WE_MAP_CLEAR_TEMP_LAYER,  [app_Par(PAR_NAME, LAYER_BEZIER)]);

         if not mem_Data.IsEmpty then
           LoadInterference(mem_Data.FieldValues[FLD_LINKEND_ID])
         else
           db_Clear(mem_TreeView);

       end;
    1: begin
         Load_Pair_Vliyan();


         FIMapView.TempLayer_Clear(LAYER_ARROW);
//         PostEvent(WE_MAP_CLEAR_TEMP_LAYER,  [app_Par(PAR_NAME, LAYER_ARROW)]);
       end;
  end;

end;



// ---------------------------------------------------------------
procedure Tfrm_LinkFreqPlan_CIA.cxDBTreeList21331_pairsExpanding(Sender:
    TcxCustomTreeList; ANode: TcxTreeListNode; var Allow: Boolean);
// ---------------------------------------------------------------
var
  iLinkEnd_ID: Integer;
  iTree_id: Integer;
  v: Variant;
begin
  inherited;

 // IsUpdated:= True;
   FIsUpdated:=True;

  v:=  aNode.Values [ col2_is_expanded.ItemIndex];

  iTree_id:=  aNode.Values [ col2_tree_id.ItemIndex];

  iLinkEnd_ID := AsInteger(aNode.Values[col2_LinkEnd_ID.ItemIndex]);
  iTree_id    := AsInteger(aNode.Values[col2_tree_id.ItemIndex]);




  with mem_Pair do
    if v = False then
  begin
    DisableControls;
//    Locate (FLD_DXTREE_ID, Node.Values[col_tree_id1.Index],[]);
    Locate (FLD_DXTREE_ID, iTree_id, []);

    Edit;
    FieldValues[FLD_is_expanded]:=True;
    Post;




    dmLinkFreqPlan_CIA_view.ExpandNeighBours2 (mem_Pair, FID, iLinkEnd_ID, iTree_id);
     //                                 Node.Values[col_LinkEnd_ID_.Index],
       //                               Node.Values[col_tree_id1.Index]);
   

    EnableControls;
  end;


   FIsUpdated:=False;

 // IsUpdated:= False;
end;



// ---------------------------------------------------------------

procedure Tfrm_LinkFreqPlan_CIA.cxDBTreeList21331_pairsExpanded(Sender:
    TcxCustomTreeList; ANode: TcxTreeListNode);
// ---------------------------------------------------------------
var
  iLinkEnd_ID: Integer;
  iTree_id: Integer;
  v: Variant;
begin

  exit;

  inherited;

 // IsUpdated:= True;
   FIsUpdated:=True;

  v:=  aNode.Values [ col2_is_expanded.ItemIndex];

  iTree_id:=  aNode.Values [ col2_tree_id.ItemIndex];

  iLinkEnd_ID := AsInteger(aNode.Values[col2_LinkEnd_ID.ItemIndex]);
  iTree_id    := AsInteger(aNode.Values[col2_tree_id.ItemIndex]);




  with mem_Pair do
    if v = False then
  begin
    DisableControls;
//    Locate (FLD_DXTREE_ID, Node.Values[col_tree_id1.Index],[]);
    Locate (FLD_DXTREE_ID, iTree_id, []);

    Edit;
    FieldValues[FLD_is_expanded]:=True;
    Post;




    dmLinkFreqPlan_CIA_view.ExpandNeighBours2 (mem_Pair, FID, iLinkEnd_ID, iTree_id);
     //                                 Node.Values[col_LinkEnd_ID_.Index],
       //                               Node.Values[col_tree_id1.Index]);
   

    EnableControls;
  end;


   FIsUpdated:=False;

 // IsUpdated:= False;
end;





end.


(*


   {

//-------------------------------------------------------------------
procedure Tfrm_LinkFreqPlan_CIA.dxDBTreeListExpanding(Sender: TObject; Node:
    TdxTreeListNode; var Allow: Boolean);
//-------------------------------------------------------------------
var
  iLinkEnd_ID: integer;
  iTree_id: integer;
begin
  if FIsUpdated then Exit;

//  dmLinkFreqPlan_NeighBors_view.Params.IsUpdated:= True;
  with mem_Pair do
    if Node.Strings[col_is_expanded.Index] = 'False' then
  begin
    DisableControls;
    Locate (FLD_DXTREE_ID, Node.Values[col_tree_id1.Index],[]);

    Edit;
    FieldValues[FLD_is_expanded]:=True;
    Post;

    FIsUpdated:=True;

    iLinkEnd_ID := AsInteger(Node.Values[col_LinkEnd_ID_.Index]);
    iTree_id    := AsInteger(Node.Values[col_tree_id1.Index]);

    dmLinkFreqPlan_CIA_view.ExpandNeighBours (mem_Pair, FID, iLinkEnd_ID, iTree_id);
     //                                 Node.Values[col_LinkEnd_ID_.Index],
       //                               Node.Values[col_tree_id1.Index]);
    FIsUpdated:=False;

    EnableControls;
  end;

//  dmLinkFreqPlan_NeighBors_view.Params.IsUpdated:= False;
end;






//  PostEvent(WE_MAP_ADD_TEMP_LAYER_BEZIER,
//                   [app_Par(PAR_NAME, LAYER_BEZIER),
  //                  app_Par(PAR_BLPOINT_ARR,  @blPointArr),
    //                app_Par(PAR_COLOR,         clRed)
      //             ]);

{
//-------------------------------------------------------------------
procedure Tfrm_LinkFreqPlan_CIA.GetMessage (aMsg: integer; aParams: TEventParamList; var aHandled: Boolean);
//-------------------------------------------------------------------
begin
  case aMsg of
 //   WE_LINK_FREQ_PLAN_CHANGED : SetLinkFreqPlan (aParams.IntByName(PAR_ID));

    WE_INFO_LOAD_CI_LINKEND   :
      case GSPages.ActivePageIndex of

        0: LoadInterference (aParams.IntByName(PAR_ID));

        1:   if (dxDBTreeList.FocusedNode.Values[col_tree_parent_id1.Index] > 0)
             then
               ShowPairVliyan(
//               dmLinkFreqPlan_CIA_view.Show_Pair_Vliyan(
                     dxDBTreeList.FocusedNode.Values[col_tree_parent_id1.Index],
                     dxDBTreeList.FocusedNode.Values[col_LinkEnd_ID.Index]

                    // cb_Map_Increase.Checked
                     );

      end;

    WE_EMP_CLOSE_WINDOW : Close;
  end;
end;

}



{
procedure Tfrm_LinkFreqPlan_CIA.DoOnMapSelection(Sender: TObject );
var
  vIMapView: IMapViewX;
begin
  vIMapView:=IMapView;

  with IMapView.GetSelectedDataset do
    if Locate(FLD_NAME, 'Arrow', []) then
      LoadLinkEnd (FieldBYName(FLD_ID).AsInteger);

//if mem_Selection.Locate(FLD_NAME, 'Arrow', []) then
//begin
//  iID:=mem_Selection.FieldBYName(FLD_ID).AsInteger;
  //    if iID > 0 then
//    PostEvent(WE_INFO_LOAD_CI_LINKEND,
//            [app_Par(PAR_ID, mem_Selection[FLD_ID])]);
//end;
//if not mem_Selection.Locate(FLD_OBJNAME, OBJ_LINK, []) then
//  exit;

//  FIMapView.RegisterNotifyEvent(0,DoOnMapFree);

end;}


(*


procedure Tfrm_LinkFreqPlan_CIA.DoOnMapEvent(Sender: TObject;  aEventIndex: integer);
//var
//  vIMapView: IMapViewX;
(*var
  oList: TMapSelectedItemList;
  oItem: TMapSelectedItem;*)
begin
  inherited;

  case aEventIndex of

    MAP_EVENT_ON_SELECT:
                      begin
                      //  vIMapView:=IMapView;


(*                        oList:=FIMapView.GetSelectedItemList;

                        oItem :=oList.FindByName('Arrow');
                        if Assigned(oItem) then
                          LoadLinkEnd (FieldBYName(FLD_ID).AsInteger);
*)

                       // with FIMapView.GetSelectedDataset do
                        //  if Locate(FLD_NAME, 'Arrow', []) then
                         //   LoadLinkEnd (FieldBYName(FLD_ID).AsInteger);

                      end;
  end;
end;


(*
//------------------------------------------------------------
procedure Tfrm_LinkFreqPlan_CIA.dxDBTreeListChangeNode(Sender: TObject;
                                                OldNode, Node: TdxTreeListNode);
//------------------------------------------------------------
//var
//  blPointArr: TBLPointArrayF;
//  blRect: TblRect;
var
  iLinkEnd_ID: integer;
  iTree_parent_id: integer;
begin
  if (mem_Pair.IsEmpty) then
    Exit;

  if (Node.Values[col_tree_parent_id1.Index] = 0) then
  begin
    FIMapView.TempLayer_Clear (LAYER_BEZIER);

    Exit;
  end;

  iLinkEnd_ID     :=AsInteger(Node.Values[col_LinkEnd_ID_.Index]);
  iTree_parent_id :=AsInteger(Node.Values[col_tree_parent_id1.Index]);

  if iTree_parent_id >0 then
    ShowPairVliyan(iLinkEnd_ID, iTree_parent_id);


{  dmLinkFreqPlan_CIA_view.Show_Pair_Vliyan(Node.Values[col_LinkEnd_ID.Index],
                                           Node.Values[col_tree_parent_id1.Index],
                                           blPointArr);
                                           //cb_Map_Increase.Checked);

  if cb_Map_Increase.Checked then
  begin
   // FBLPoints.MakeBLPointArray(blPointArr);
    blRect:= geo_DefineRoundBLRect(blPointArr);//blPointArr);
    PostBLRect(WE_MAP_SHOW_RECT, blRect);
  end;


  PostEvent(WE_MAP_CLEAR_TEMP_LAYER, [app_Par(PAR_NAME, LAYER_BEZIER)]);

  PostEvent(WE_MAP_ADD_TEMP_LAYER_BEZIER,
                   [app_Par(PAR_NAME, LAYER_BEZIER),
                    app_Par(PAR_BLPOINT_ARR,  @blPointArr),
                    app_Par(PAR_COLOR,         clRed)
                   ]);
}
{
 //----------------------�������� �� ����� ����� --------------------
  procedure DoShowLineOnMap (var aBLPoints: TBLPointArrayF; aColor:integer);
  // ---------------------------------------------------------------
  begin
    PostEvent(WE_MAP_CLEAR_TEMP_LAYER, [app_Par(PAR_NAME, LAYER_BEZIER)]);

    PostEvent(WE_MAP_ADD_TEMP_LAYER_BEZIER,
                     [app_Par(PAR_NAME, LAYER_BEZIER),
                      app_Par(PAR_BLPOINT_ARR,  @aBLPoints),
                      app_Par(PAR_COLOR,         aColor)
                     ]);
  end;
}
  //DoShowLineOnMap (blPointArr, clRed);

end;

procedure Tfrm_LinkFreqPlan_CIA.pn_BottomClick(Sender: TObject);
begin
  inherited;
end;




//-------------------------------------------------------------------
procedure Tfrm_LinkFreqPlan_CIA.dxDBTreeList_TopHotTrackNode(Sender: TObject;
  AHotTrackInfo: TdxTreeListHotTrackInfo; var ACursor: TCursor);
//-------------------------------------------------------------------
begin
  inherited;
(*
  if sender = dxDBTreeList_Top then
    dx_ShowHint(dxDBTreeList_Top , AHotTrackInfo, [col_LinkEnd_Name])
  else

  if sender = dxDBTreeList then
    dx_ShowHint(dxDBTreeList , AHotTrackInfo,
                 [col_RX_LinkEnd_Name, col_TX_LinkEnd_Name]);
*)
end;

  (*


//-------------------------------------------------------------------
procedure Tfrm_LinkFreqPlan_CIA.dxDBTreeList_TopCustomDrawCell(
  Sender: TObject; ACanvas: TCanvas; ARect: TRect; ANode: TdxTreeListNode;
  AColumn: TdxTreeListColumn; ASelected, AFocused, ANewItemRow: Boolean;
  var AText: String; var AColor: TColor; AFont: TFont;
  var AAlignment: TAlignment; var ADone: Boolean);
//-------------------------------------------------------------------
begin
  inherited;

  if ANode.Values[col_Tree_parent_id.Index] = 0 then
  begin
    AColor:=1004429;
    AFont.Color:=clWhite;
  end
  else

  if ((ANode.Values[col_CIA_.Index] <> null) and
      (ANode.Values[col_CIA_.Index] < FAdmitted_CI)) or
     ((ANode.Values[col_Threshold_Degradation_cia.Index] <> null) and
      (ANode.Values[col_Threshold_Degradation_cia.Index] > FAdmitted_Degradation)) then
  begin
    AColor:=clRed; // $008080FF;
    AFont.Color:=clWhite;
  end;

end;



 {

//-------------------------------------------------------------------
procedure Tfrm_LinkFreqPlan_CIA.dxDBTreeListCustomDrawCell(Sender: TObject;
    ACanvas: TCanvas; ARect: TRect; ANode: TdxTreeListNode; AColumn:
    TdxTreeListColumn; ASelected, AFocused, ANewItemRow: Boolean; var AText:
    string; var AColor: TColor; AFont: TFont; var AAlignment: TAlignment; var
    ADone: Boolean);
//-------------------------------------------------------------------
begin
  inherited;

  if (AColumn=col_RecNo) and
     (ANode.Values[col_tree_parent_id1.Index] = 0) then
    aText:=IntToStr(aNode.Index+1);

  if (not ASelected) then
    if (ANode.Values[col_tree_parent_id1.Index] <> 0) and
       AsBoolean(ANode.Values[col_IsNeigh.index]) then
      aColor:= $008080FF; // clRed;
end;
   }

   


//-------------------------------------------------------------------
procedure Tfrm_LinkFreqPlan_CIA.act_Setup_TreeListExecute(Sender: TObject);
//-------------------------------------------------------------------
begin
  inherited;

(*  case GSPages.ActivePageIndex of
    0:   dx_Dlg_Customize_DBTreeList (dxDBTreeList_Top);
    1:   dx_Dlg_Customize_DBTreeList (dxDBTreeList);
  end;
*)
end;

{
//------------------------------------------------------------
procedure Tfrm_LinkFreqPlan_CIA.dxDBGrid_BootomChangeNode(Sender: TObject; OldNode,
                                                        Node: TdxTreeListNode);
//------------------------------------------------------------
var
  i_linkend_id: Integer;
begin
  if mem_Data.IsEmpty then
    Exit;

  i_linkend_id:=AsInteger(Node.Values[col3_linkend_id_bottom.Index]);

  LoadInterference(i_linkend_id); //mem_Data.FieldValues['linkend_id']
end;
  }

              {

  

procedure Tfrm_LinkFreqPlan_CIA.cxDBTreeList211CustomDrawCell(Sender: TObject; ACanvas: TcxCanvas;
    AViewInfo: TcxTreeListEditCellViewInfo; var ADone: Boolean);
begin
  inherited;

//AViewInfo.GridRecord.Values[col__FREQ_MIN_LOW.Index];
  // eFREQ_MAX_LOW :=AViewInfo.GridRecor

{
  if (AViewInfo.Column=col_RecNo) and
     (AViewInfo.GridRecor.Values[col_tree_parent_id1.Index] = 0) then
    aText:=IntToStr(aNode.Index+1);
   }

 //  AViewInfo.Node.Values

(*

//  if (not ACanvas.Selected) False
    if (AViewInfo.Node.Values[col___tree_parent_id22.ItemIndex] <> 0) and
       AsBoolean(AViewInfo.Node.Values[col__IsNeigh.ItemIndex])
    then
      ACanvas.Brush.Color:= $008080FF; // clRed;
*)

end;




// ---------------------------------------------------------------
procedure Tfrm_LinkFreqPlan_CIA.cxDBTreeList1Expanding(Sender:
    TcxCustomTreeList; ANode: TcxTreeListNode; var Allow: Boolean);
// ---------------------------------------------------------------

(*
begin
  inherited;
end;
*)


/////////////////////////////////////////////////////////////////////////////
//      procedure Tfrm_LinkFreqPlan_CIA.cxDBTreeList211Expanding(Sender: TObject;
//    ANode: TcxTreeListNode; var Allow: Boolean);
/////////////////////////////////////////////////////////////////////////////    
var
  iLinkEnd_ID: integer;
  iTree_id: integer;
begin
{
  if FIsUpdated then Exit;

//  dmLinkFreqPlan_NeighBors_view.Params.IsUpdated:= True;
  with mem_Pair do
    if aNode.Values[col1_is_expanded.Index] = 'False' then
  begin
    DisableControls;
    Locate (FLD_DXTREE_ID, Node.Values[col1_tree_id1.Index],[]);

    Edit;
    FieldValues[FLD_is_expanded]:=True;
    Post;

    FIsUpdated:=True;

    iLinkEnd_ID := AsInteger(Node.Values[col1_LinkEnd_ID_.Index]);
    iTree_id    := AsInteger(Node.Values[col1_tree_id1.Index]);

    dmLinkFreqPlan_CIA_view.ExpandNeighBours (mem_Pair, FID, iLinkEnd_ID, iTree_id);
     //                                 Node.Values[col_LinkEnd_ID_.Index],
       //                               Node.Values[col_tree_id1.Index]);
    FIsUpdated:=False;

    EnableControls;
  end;

  }

//  dmLinkFreqPlan_NeighBors_view.Params.IsUpdated:= False;
end;



   {

procedure Tfrm_LinkFreqPlan_CIA.cxDBTreeList1CustomDrawCell(Sender: TObject;
    ACanvas: TcxCanvas; AViewInfo: TcxTreeListEditCellViewInfo; var ADone:
    Boolean);
begin
 inherited;

{

  if ANode.Values[col__Tree_parent_id.Index] = 0 then
  begin
    AColor:=1004429;
    AFont.Color:=clWhite;
  end
  else

  if ((ANode.Values[col__CIA_.Index] <> null) and
      (ANode.Values[col__CIA_.Index] < FAdmitted_CI)) or
     ((ANode.Values[col__Threshold_Degradation_cia.Index] <> null) and
      (ANode.Values[col__Threshold_Degradation_cia.Index] > FAdmitted_Degradation)) then
  begin
    AColor:=clRed; // $008080FF;
    AFont.Color:=clWhite;
  end;
  }


end;

