object frame_LinkFreqPlan_Resource: Tframe_LinkFreqPlan_Resource
  Left = 727
  Top = 307
  Width = 550
  Height = 382
  Caption = 'frame_LinkFreqPlan_Resource'
  Color = clBtnFace
  ParentFont = True
  OldCreateOrder = True
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object cxGrid1: TcxGrid
    Left = 0
    Top = 230
    Width = 534
    Height = 113
    Align = alBottom
    TabOrder = 0
    LookAndFeel.Kind = lfFlat
    object cxGrid1DBTableView1: TcxGridDBTableView
      NavigatorButtons.ConfirmDelete = False
      DataController.DataSource = ds_Resource
      DataController.KeyFieldNames = 'id'
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsCustomize.ColumnsQuickCustomization = True
      OptionsView.GroupByBox = False
      object col_ID: TcxGridDBColumn
        DataBinding.FieldName = 'ID'
        Visible = False
        Options.Editing = False
        Options.Filtering = False
      end
      object col_band1: TcxGridDBColumn
        DataBinding.FieldName = 'band'
        Options.Editing = False
        Options.Filtering = False
        Width = 70
      end
      object col_freq_allow1: TcxGridDBColumn
        DataBinding.FieldName = 'freq_allow'
        Options.Filtering = False
        Width = 199
      end
      object col_freq_forbidden1: TcxGridDBColumn
        DataBinding.FieldName = 'freq_forbidden'
        Options.Filtering = False
        Width = 222
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1DBTableView1
    end
  end
  object qry_Resource: TADOQuery
    Connection = ADOConnection1
    Parameters = <>
    SQL.Strings = (
      'SELECT * FROM   LINKFREQPLAN_RESOURCE  '
      ''
      ' WHERE (LinkFreqPlan_id = :LinkFreqPlan_id)')
    Left = 136
    Top = 16
  end
  object ds_Resource: TDataSource
    DataSet = qry_Resource
    Left = 136
    Top = 64
  end
  object ADOConnection1: TADOConnection
    ConnectionString = 
      'Provider=MSDASQL.1;Persist Security Info=False;User ID=sa;Data S' +
      'ource=onega_1'
    LoginPrompt = False
    Provider = 'MSDASQL.1'
    Left = 48
    Top = 16
  end
end
