unit fr_LinkFreqPlan_Resource;

interface

uses
  Classes, Controls, Forms, Db, ADODB, cxPropertiesStore, ActnList, ToolWin,
  cxGridTableView, cxClasses, cxControls, cxGridCustomView,
  cxGridCustomTableView, cxGridDBTableView, cxGridLevel,
  cxGrid,

//  dm_Language,

//  f_Custom,

//  dm_Library,

//  u_Worksheet,

  u_db,

  dm_Onega_DB_data,

  u_const_db,

   ComCtrls, Grids, DBGrids, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, dxSkinsDefaultPainters,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxEdit, cxDBData  ;


type
  Tframe_LinkFreqPlan_Resource = class(TForm)
    qry_Resource: TADOQuery;
    ds_Resource: TDataSource;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    ADOConnection1: TADOConnection;
    col_band1: TcxGridDBColumn;
    col_freq_allow1: TcxGridDBColumn;
    col_freq_forbidden1: TcxGridDBColumn;
    col_ID: TcxGridDBColumn;

    procedure cxGrid1Exit(Sender: TObject);
    procedure FormCreate(Sender: TObject);
//    procedure DoAction(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
   // procedure qry_ResourceNewRecord(DataSet: TDataSet);
  private
  //  FWorksheet: TWorksheet;

//    FLinkFreqPlanID : integer;

//    procedure Import (aFileName: string);

  public
    procedure SetReadOnly(Value: Boolean);
    procedure View(aLinkFreqPlan_ID: integer);

  end;


//===========================================================
implementation {$R *.DFM}
//===========================================================

const
  CAPTION_BAND       = '��������, [GHz]';
  CAPTION_FREQ_ALLOW  = '����������� ������ ������, [MHz]';
  CAPTION_FREQ_FORBID = '����������� ������ ������, [MHz]';



procedure Tframe_LinkFreqPlan_Resource.cxGrid1Exit(Sender: TObject);
begin
  db_PostDataset(qry_Resource);
end;

//-------------------------------------------------------------
procedure Tframe_LinkFreqPlan_Resource.FormCreate(Sender: TObject);
//-------------------------------------------------------------
begin
  inherited;

//  FWorksheet:=TWorksheet.Create;
  cxGrid1.Align:=alClient;

  col_band1.Caption        := CAPTION_BAND;
  col_freq_allow1.Caption  := CAPTION_FREQ_ALLOW;
  col_freq_forbidden1.Caption := CAPTION_FREQ_FORBID;


end;

//-------------------------------------------------------------
procedure Tframe_LinkFreqPlan_Resource.FormDestroy(Sender: TObject);
//-------------------------------------------------------------
begin

end;

//------------------------------------------------------
procedure Tframe_LinkFreqPlan_Resource.View(aLinkFreqPlan_ID: integer);
//------------------------------------------------------
const
  SQL_SELECT_LINKFREQPLAN_RESOURCE =
     'SELECT * FROM '+ TBL_LINKFREQPLAN_RESOURCE +
     ' WHERE (LinkFreqPlan_id = :LinkFreqPlan_id)';// order by band';

{  RANGE_ARRAY : array[0..12] of integer =
   (6, 7, 8, 11, 13, 15, 18, 23, 26, 28, 32, 38, 52);
}

var i: Integer;
begin
//  FLinkFreqPlanID:=aLinkFreqPlan_ID;

 // dmOnega_DB_data.LinkFreqPlan_Init(aLinkFreqPlan_ID);

  dmOnega_DB_data.OpenQuery(qry_Resource,
        SQL_SELECT_LINKFREQPLAN_RESOURCE,
        [FLD_LINKFREQPLAN_ID, aLinkFreqPlan_ID]);


(*  db_OpenQuery(qry_Resource, SQL_SELECT_LINKFREQPLAN_RESOURCE,
                            [db_par(FLD_LINKFREQPLAN_ID, aLinkFreqPlan_ID)]);

  i :=qry_Resource.RecordCount;

  if qry_Resource.IsEmpty then
    for i:=0 to dmLibrary.Bands.Count-1 do
//    for i := 0 to High(RANGE_ARRAY) do
      db_AddRecord(qry_Resource, [db_par(FLD_LINKFREQPLAN_ID, aLinkFreqPlan_ID),
                                  db_par(FLD_BAND, dmLibrary.Bands[i])]);
*)

end;



procedure Tframe_LinkFreqPlan_Resource.SetReadOnly(Value: Boolean);
begin
  cxGrid1DBTableView1.OptionsData.Editing := not Value;

//  FReadOnly := Value;

  qry_Resource.LockType := IIF(Value, ltReadOnly, ltOptimistic);

 // SetFormActionsEnabled(Self, not Value);

//  ADOStoredProc1.LockType := IIF(dmUser_Security.ProjectIsReadOnly, ltReadOnly, ltBatchOptimistic);
//  SetFormActionsEnabled(Self, not dmUser_Security.ProjectIsReadOnly);

end;

//------------------------------------------------------------------------------
end.


//
//
////------------------------------------------------------------------------------
//procedure Tframe_LinkFreqPlan_Resource.qry_ResourceNewRecord(DataSet: TDataSet);
////------------------------------------------------------------------------------
//begin
////  DataSet[FLD_LINKFREQPLAN_ID]:= FLinkFreqPlanID;
//end;



//-------------------------------------------------------------------
//procedure Tframe_LinkFreqPlan_Resource.Import (aFileName: string);
//-------------------------------------------------------------------
    //-----------------------------------------------------
//    procedure DoInitWorksheetColumns();
    //-----------------------------------------------------
//    var i: integer; arrColumns: TColumnArray;
//    begin
//      SetLength(arrColumns, High(EXCEL_COLUMNS)+1);
//      for i:=0 to High(arrColumns) do begin
//        arrColumns[i].Name     :=EXCEL_COLUMNS[i].Name;
//        arrColumns[i].FieldType:=EXCEL_COLUMNS[i].FieldType;
//      end;
//      FWorksheet.SetupColumns (arrColumns);
//    end;
//
//var i: integer;
//    arrImportRange : TDoubleArray;
//    dRangeXSL : double;
//begin
//  if not FileExists(aFileName) then
//  begin
//    ErrDlg_FileNotExists(aFileName);
//    Exit;
//  end;
//
//  DoInitWorksheetColumns();
//
//  FWorksheet.HeaderRows:=1;
//
//  with FWorksheet do
//  if LoadFromFile (aFileName) then
//    for i := 0 to RowCount-1 do
//    begin
//      AddToDoubleArray(Cells[i,CAPTION_BAND], arrImportRange);
//
//      dRangeXSL:= AsFloat(Cells[i,CAPTION_BAND]);
//
//      if qry_Resource.Locate(FLD_BAND_NAME, AsString(dRangeXSL), []) then
//        db_UpdateRecord(qry_Resource,
//          [db_par('freq_allow',     Cells[i,CAPTION_FREQ_ALLOW]),
//           db_par('freq_forbidden', Cells[i,CAPTION_FREQ_FORBID]) ])
//      else
//        db_AddRecord(qry_Resource,
//          [db_par(FLD_LINKFREQPLAN_ID, FLinkFreqPlanID),
//           db_par(FLD_BAND_NAME,        Cells[i,CAPTION_BAND]),
//           db_par(FLD_RANGE,            Cells[i,CAPTION_BAND]),
//           db_par('freq_allow',         Cells[i,CAPTION_FREQ_ALLOW]),
//           db_par('freq_forbidden',     Cells[i,CAPTION_FREQ_FORBID]) ])
//    end;
//
//
//{  qry_Resource.First;
//  with qry_Resource do while not Eof do
//  begin
//    if FindInDoubleArray (FieldByName(FLD_BAND_NAME).AsString, arrImportRange) < 0 then
//    if FindInDoubleArray (FieldByName(FLD_RANGE).AsInteger, arrImportRange) < 0 then
//      gl_DB.DeleteRecordByID(TBL_LINK_FREQPLAN_RESOURCE, qry_Resource[FLD_ID]);
//
//    Next;
//  end;
//}
//
//end;
//
//-------------------------------------------------------------
//procedure Tframe_LinkFreqPlan_Resource.DoAction(Sender: TObject);
//-------------------------------------------------------------
//begin
  //-----------------------
//  if Sender=act_Export then
//  begin
//    SaveDialog.Filter:= FILTER_EXCEL;
//    SaveDialog.DefaultExt:= 'xls';
//    if SaveDialog.Execute then
//      dxDBGrid.SaveToXLS(SaveDialog.FileName, True);
//  end else
//
  //-----------------------
//  if Sender=act_Import then
//  begin
//    OpenDialog.Filter:= FILTER_EXCEL;
//    OpenDialog.DefaultExt:= 'xls';
//    if OpenDialog.Execute then
//      Import(OpenDialog.FileName);
//  end;
//
//  View(FLinkFreqPlanID);
//end;

