unit d_LinkEnd_inspector;

interface

uses
  Classes, Controls, Forms,
  rxPlacemnt,

  u_func,

  fr_LinkEnd_inspector, StdCtrls, ExtCtrls
  ;


type
  Tdlg_LinkEnd_inspector = class(TForm)
    FormPlacement1: TFormPlacement;
    pn_Buttons: TPanel;
    Bevel1: TBevel;
    Panel1: TPanel;
    btn_Ok: TButton;
    btn_Cancel: TButton;
    procedure FormCreate(Sender: TObject);
  private
    Fframe_LinkEnd_inspector: Tframe_LinkEnd_inspector;

  public
    class function ExecDlg1(aID: integer; aObjName: string): boolean;

  end;


implementation

{$R *.dfm}


//--------------------------------------------------------------------
class function Tdlg_LinkEnd_inspector.ExecDlg1(aID: integer; aObjName: string): boolean;
//--------------------------------------------------------------------
begin
  with Tdlg_LinkEnd_inspector.Create(Application) do
  try
    Fframe_LinkEnd_inspector.View (aID, aObjName);

    Result:=(ShowModal=mrOK);

    if Result then
      Fframe_LinkEnd_inspector.Post;

(*
    FormPlacement1.Active:=True;
    FormPlacement1.RestoreFormPlacement;

    View (aID, aObjName);
    //Ffrm_DBInspector.Options.AutoCommit:=False;
    pn_Buttons.Visible:=True;

    Result:=(ShowModal=mrOK);

*)

  finally
    Free;
  end;

end;



procedure Tdlg_LinkEnd_inspector.FormCreate(Sender: TObject);
begin
//  Caption :='���';
  CreateChildForm(Tframe_LinkEnd_inspector, Fframe_LinkEnd_inspector, Self);
end;


end.
