object dlg_LinkEnd_Get_Band: Tdlg_LinkEnd_Get_Band
  Left = 943
  Top = 225
  Width = 685
  Height = 676
  Caption = 'dlg_LinkEnd_Get_Band'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Scaled = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 602
    Width = 669
    Height = 35
    Align = alBottom
    BevelOuter = bvNone
    Constraints.MaxHeight = 35
    Constraints.MinHeight = 35
    TabOrder = 0
    DesignSize = (
      669
      35)
    object btn_Ok: TButton
      Left = 506
      Top = 6
      Width = 75
      Height = 25
      Anchors = [akTop, akRight]
      Caption = 'Ok'
      Default = True
      ModalResult = 1
      TabOrder = 0
    end
    object Button2: TButton
      Left = 588
      Top = 6
      Width = 75
      Height = 25
      Anchors = [akTop, akRight]
      Cancel = True
      Caption = #1047#1072#1082#1088#1099#1090#1100
      ModalResult = 2
      TabOrder = 1
    end
    object Edit1: TEdit
      Left = 24
      Top = 8
      Width = 249
      Height = 21
      TabOrder = 2
      Text = 'Channel_width=50'
      Visible = False
    end
    object Button1: TButton
      Left = 280
      Top = 8
      Width = 75
      Height = 25
      Caption = 'Button1'
      TabOrder = 3
      Visible = False
      OnClick = Button1Click
    end
    object Button3: TButton
      Left = 368
      Top = 8
      Width = 75
      Height = 25
      Caption = 'Button3'
      TabOrder = 4
      Visible = False
      OnClick = Button3Click
    end
  end
  object PageControl1: TPageControl
    Left = 0
    Top = 246
    Width = 669
    Height = 356
    ActivePage = TabSheet1
    Align = alBottom
    TabOrder = 1
    object TabSheet1: TTabSheet
      Caption = #1042#1099#1073#1086#1088' '#1080#1079' '#1089#1087#1088#1072#1074#1086#1095#1085#1080#1082#1072
      object Splitter1: TSplitter
        Left = 0
        Top = 160
        Width = 661
        Height = 5
        Cursor = crVSplit
        Align = alTop
      end
      object cxGrid2: TcxGrid
        Left = 0
        Top = 184
        Width = 661
        Height = 144
        Align = alBottom
        TabOrder = 0
        LookAndFeel.Kind = lfFlat
        object cxGrid2DBBandedTableView_Band_Channels: TcxGridDBBandedTableView
          NavigatorButtons.ConfirmDelete = False
          DataController.DataSource = ds_Band_Channels
          DataController.Filter.MaxValueListCount = 1000
          DataController.KeyFieldNames = 'id'
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          Filtering.ColumnPopup.MaxDropDownItemCount = 12
          OptionsBehavior.ImmediateEditor = False
          OptionsBehavior.ColumnHeaderHints = False
          OptionsCustomize.ColumnHiding = True
          OptionsCustomize.ColumnsQuickCustomization = True
          OptionsCustomize.GroupBySorting = True
          OptionsCustomize.GroupRowSizing = True
          OptionsCustomize.BandHiding = True
          OptionsCustomize.BandsQuickCustomization = True
          OptionsCustomize.NestedBands = False
          OptionsData.Deleting = False
          OptionsData.Inserting = False
          OptionsSelection.CellSelect = False
          OptionsSelection.HideFocusRectOnExit = False
          OptionsSelection.InvertSelect = False
          OptionsView.CellAutoHeight = True
          OptionsView.GroupByBox = False
          OptionsView.HeaderAutoHeight = True
          OptionsView.Indicator = True
          Preview.AutoHeight = False
          Preview.MaxLineCount = 2
          Styles.OnGetContentStyle = cxGrid2DBBandedTableView_Band_ChannelsStylesGetContentStyle
          Bands = <
            item
              Width = 134
            end
            item
              Caption = 'Low'
              Width = 172
            end
            item
              Caption = 'High'
              Width = 173
            end
            item
              Width = 121
            end>
          object col__band_name: TcxGridDBBandedColumn
            DataBinding.FieldName = 'HIGH'
            PropertiesClassName = 'TcxTextEditProperties'
            Properties.Alignment.Horz = taLeftJustify
            Properties.MaxLength = 0
            Properties.ReadOnly = True
            Visible = False
            Options.Filtering = False
            Width = 107
            Position.BandIndex = 0
            Position.ColIndex = 0
            Position.RowIndex = 0
          end
          object col__channel_low: TcxGridDBBandedColumn
            DataBinding.FieldName = 'LOW'
            PropertiesClassName = 'TcxTextEditProperties'
            Properties.Alignment.Horz = taLeftJustify
            Properties.MaxLength = 0
            Properties.ReadOnly = True
            Visible = False
            Options.Editing = False
            Options.Filtering = False
            Width = 76
            Position.BandIndex = 0
            Position.ColIndex = 1
            Position.RowIndex = 0
          end
          object col__TX_freq_low: TcxGridDBBandedColumn
            DataBinding.FieldName = 'TX_Freq_LOW'
            PropertiesClassName = 'TcxCurrencyEditProperties'
            Properties.Alignment.Horz = taLeftJustify
            Properties.AssignedValues.MaxValue = True
            Properties.AssignedValues.MinValue = True
            Properties.DecimalPlaces = 0
            Properties.DisplayFormat = ',0.00'
            Properties.Nullable = False
            Properties.ReadOnly = True
            Options.Editing = False
            Options.Filtering = False
            Width = 78
            Position.BandIndex = 1
            Position.ColIndex = 0
            Position.RowIndex = 0
          end
          object col__RX_freq_low: TcxGridDBBandedColumn
            DataBinding.FieldName = 'TX_Freq_HIGH'
            PropertiesClassName = 'TcxCurrencyEditProperties'
            Properties.Alignment.Horz = taLeftJustify
            Properties.AssignedValues.MaxValue = True
            Properties.AssignedValues.MinValue = True
            Properties.DisplayFormat = ',0.00'
            Properties.Nullable = False
            Properties.ReadOnly = True
            Options.Editing = False
            Options.Filtering = False
            Width = 80
            Position.BandIndex = 2
            Position.ColIndex = 0
            Position.RowIndex = 0
          end
          object col__tx_freq_high: TcxGridDBBandedColumn
            DataBinding.FieldName = 'RX_Freq_LOW'
            PropertiesClassName = 'TcxCurrencyEditProperties'
            Properties.Alignment.Horz = taLeftJustify
            Properties.AssignedValues.MaxValue = True
            Properties.AssignedValues.MinValue = True
            Properties.DisplayFormat = ',0.00'
            Properties.Nullable = False
            Properties.ReadOnly = True
            Options.Filtering = False
            Width = 79
            Position.BandIndex = 1
            Position.ColIndex = 1
            Position.RowIndex = 0
          end
          object col__rx_freq_high: TcxGridDBBandedColumn
            DataBinding.FieldName = 'RX_Freq_HIGH'
            PropertiesClassName = 'TcxCurrencyEditProperties'
            Properties.Alignment.Horz = taLeftJustify
            Properties.AssignedValues.MaxValue = True
            Properties.AssignedValues.MinValue = True
            Properties.DisplayFormat = ',0.00'
            Properties.Nullable = False
            Properties.ReadOnly = True
            Options.Filtering = False
            Width = 81
            Position.BandIndex = 2
            Position.ColIndex = 1
            Position.RowIndex = 0
          end
          object col_Channel: TcxGridDBBandedColumn
            DataBinding.FieldName = 'Channel'
            Options.Filtering = False
            Width = 85
            Position.BandIndex = 0
            Position.ColIndex = 3
            Position.RowIndex = 0
          end
          object col_name: TcxGridDBBandedColumn
            DataBinding.FieldName = 'name'
            Visible = False
            Options.Filtering = False
            Width = 85
            Position.BandIndex = 0
            Position.ColIndex = 2
            Position.RowIndex = 0
          end
          object col_Checked: TcxGridDBBandedColumn
            Caption = #1047#1072#1087#1088#1077#1090' MO'
            DataBinding.FieldName = 'checked'
            PropertiesClassName = 'TcxCheckBoxProperties'
            Properties.NullStyle = nssUnchecked
            Options.Filtering = False
            Position.BandIndex = 3
            Position.ColIndex = 0
            Position.RowIndex = 0
          end
          object col_bandwidth: TcxGridDBBandedColumn
            DataBinding.FieldName = 'bandwidth'
            Options.Filtering = False
            Position.BandIndex = 0
            Position.ColIndex = 4
            Position.RowIndex = 0
          end
        end
        object cxGrid2Level1: TcxGridLevel
          GridView = cxGrid2DBBandedTableView_Band_Channels
        end
      end
      object cxGrid1: TcxGrid
        Left = 0
        Top = 39
        Width = 661
        Height = 121
        Align = alTop
        TabOrder = 1
        LookAndFeel.Kind = lfFlat
        object cxGrid1DBTableView1_Bands: TcxGridDBTableView
          PopupMenu = PopupMenu1
          NavigatorButtons.ConfirmDelete = False
          DataController.DataSource = ds_Bands
          DataController.Filter.MaxValueListCount = 1000
          DataController.KeyFieldNames = 'id'
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          Filtering.ColumnPopup.MaxDropDownItemCount = 12
          OptionsBehavior.ImmediateEditor = False
          OptionsCustomize.ColumnsQuickCustomization = True
          OptionsData.Editing = False
          OptionsData.Inserting = False
          OptionsSelection.CellSelect = False
          OptionsSelection.HideFocusRectOnExit = False
          OptionsSelection.InvertSelect = False
          OptionsView.GroupByBox = False
          OptionsView.GroupFooters = gfVisibleWhenExpanded
          OptionsView.HeaderAutoHeight = True
          OptionsView.Indicator = True
          Preview.AutoHeight = False
          Preview.MaxLineCount = 2
          object col__id: TcxGridDBColumn
            Tag = -1
            DataBinding.FieldName = 'id'
            PropertiesClassName = 'TcxTextEditProperties'
            Properties.Alignment.Horz = taLeftJustify
            Properties.MaxLength = 0
            Properties.ReadOnly = False
            Visible = False
            Options.Filtering = False
            Width = 34
          end
          object col__Low: TcxGridDBColumn
            DataBinding.FieldName = 'Low'
            Options.Filtering = False
            Width = 45
          end
          object col__High: TcxGridDBColumn
            DataBinding.FieldName = 'High'
            Options.Filtering = False
            Width = 47
          end
          object col__TxRx_Shift: TcxGridDBColumn
            DataBinding.FieldName = 'TxRx_Shift'
            Options.Filtering = False
            Width = 63
          end
          object col__freq_min_low: TcxGridDBColumn
            DataBinding.FieldName = 'freq_min_low'
            Options.Filtering = False
            Width = 75
          end
          object col__freq_max_low: TcxGridDBColumn
            DataBinding.FieldName = 'freq_max_low'
            HeaderAlignmentHorz = taCenter
            Options.Filtering = False
            Width = 84
          end
          object col__freq_min_high: TcxGridDBColumn
            DataBinding.FieldName = 'freq_min_high'
            Options.Filtering = False
            Width = 72
          end
          object col__freq_max_high: TcxGridDBColumn
            DataBinding.FieldName = 'freq_max_high'
            Options.Filtering = False
            Width = 75
          end
          object col__Channel_width: TcxGridDBColumn
            DataBinding.FieldName = 'Channel_width'
            Options.Filtering = False
            Width = 77
          end
          object col_bandwidth_: TcxGridDBColumn
            DataBinding.FieldName = 'bandwidth'
            Options.Editing = False
            Options.Filtering = False
          end
          object col__Channel_count: TcxGridDBColumn
            DataBinding.FieldName = 'Channel_count'
            Options.Filtering = False
            Width = 80
          end
        end
        object cxGrid1Level1: TcxGridLevel
          GridView = cxGrid1DBTableView1_Bands
        end
      end
      object pn_RadioButtons: TPanel
        Left = 0
        Top = 0
        Width = 661
        Height = 39
        Align = alTop
        BevelOuter = bvNone
        BorderWidth = 2
        TabOrder = 2
        object RadioGroup_Auto: TRadioGroup
          Left = 2
          Top = 2
          Width = 113
          Height = 35
          Align = alLeft
          Columns = 2
          Items.Strings = (
            'Low'
            'High')
          TabOrder = 0
          OnClick = RadioGroup_AutoClick
        end
        object CheckBox1: TCheckBox
          Left = 128
          Top = 8
          Width = 145
          Height = 17
          Action = act_Filter_DEL
          TabOrder = 1
        end
      end
    end
    object TabSheet2: TTabSheet
      Caption = #1056#1091#1095#1085#1086#1081' '#1074#1074#1086#1076
      ImageIndex = 1
      object Panel2: TPanel
        Left = 0
        Top = 0
        Width = 661
        Height = 39
        Align = alTop
        BevelOuter = bvNone
        BorderWidth = 2
        TabOrder = 0
        object RadioGroup_hand: TRadioGroup
          Left = 2
          Top = 2
          Width = 113
          Height = 35
          Align = alLeft
          Columns = 2
          Items.Strings = (
            'Low'
            'High')
          TabOrder = 0
          OnClick = RadioGroup_handClick
        end
      end
      object ed_Rx: TLabeledEdit
        Left = 164
        Top = 112
        Width = 150
        Height = 21
        EditLabel.Width = 133
        EditLabel.Height = 13
        EditLabel.Caption = 'Rx, '#1063#1072#1089#1090#1086#1090#1072' '#1087#1088#1080#1077#1084#1072' [MHz]'
        TabOrder = 1
        OnChange = ed_RxChange
      end
      object ed_Tx: TLabeledEdit
        Left = 8
        Top = 112
        Width = 150
        Height = 21
        EditLabel.Width = 141
        EditLabel.Height = 13
        EditLabel.Caption = 'Tx, '#1063#1072#1089#1090#1086#1090#1072' '#1087#1077#1088#1077#1076#1072#1095#1080' [MHz]'
        TabOrder = 2
        OnChange = ed_TxChange
      end
      object ed_Sub_band: TLabeledEdit
        Left = 8
        Top = 64
        Width = 121
        Height = 21
        EditLabel.Width = 68
        EditLabel.Height = 13
        EditLabel.Caption = #1055#1086#1076#1076#1080#1072#1087#1072#1079#1086#1085
        TabOrder = 3
      end
      object GroupBox1: TGroupBox
        Left = 8
        Top = 152
        Width = 185
        Height = 105
        Caption = #1056#1072#1079#1085#1086#1089' '#1095#1072#1089#1090#1086#1090' '#1055#1056#1044'/'#1055#1056#1052' [MHz]'
        TabOrder = 4
        object RxDBLookupList_txrx_shift: TRxDBLookupList
          Left = 2
          Top = 15
          Width = 181
          Height = 82
          Align = alClient
          Ctl3D = True
          LookupField = 'txrx_shift'
          LookupSource = ds_txrx_shift
          ParentCtl3D = False
          TabOrder = 0
          OnClick = RxDBLookupList_txrx_shiftClick
        end
      end
    end
  end
  object qry_Bands: TADOQuery
    Connection = dmMain.ADOConnection1
    CursorType = ctStatic
    AfterScroll = qry_BandsAfterScroll
    Parameters = <>
    SQL.Strings = (
      'select * from LinkEndType_Band')
    Left = 40
    Top = 76
  end
  object ds_Band_Channels: TDataSource
    DataSet = sp_Band_Channels
    Left = 132
    Top = 140
  end
  object ds_Bands: TDataSource
    DataSet = qry_Bands
    Left = 40
    Top = 132
  end
  object FormStorage1: TFormStorage
    IniFileName = 'Software\Onega\'
    Options = [fpPosition]
    StoredProps.Strings = (
      'cxGrid1.Height'
      'PageControl1.ActivePage')
    StoredValues = <>
    Left = 156
    Top = 12
  end
  object ActionList1: TActionList
    OnUpdate = ActionList1Update
    Left = 344
    Top = 8
    object act_Filter_DEL: TAction
      Caption = #1059#1073#1088#1072#1090#1100' '#1092#1080#1083#1100#1090#1088
      OnExecute = act_Filter_DELExecute
    end
  end
  object sp_Band_Channels: TADOStoredProc
    Filtered = True
    Parameters = <>
    Left = 132
    Top = 84
  end
  object q_ch_spacing: TADOQuery
    Connection = dmMain.ADOConnection1
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'SELECT distinct ch_spacing FROM LINKENDTYPE_mode ')
    Left = 520
    Top = 92
  end
  object ds_ch_spacing: TDataSource
    DataSet = q_ch_spacing
    Left = 520
    Top = 148
  end
  object q_txrx_shift: TADOQuery
    Connection = dmMain.ADOConnection1
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'SELECT distinct ch_spacing FROM LINKENDTYPE_mode ')
    Left = 400
    Top = 92
  end
  object ds_txrx_shift: TDataSource
    DataSet = q_txrx_shift
    Left = 400
    Top = 140
  end
  object PopupMenu1: TPopupMenu
    Left = 256
    Top = 96
    object N1: TMenuItem
      Action = act_Filter_DEL
    end
  end
  object cxStyleRepository1: TcxStyleRepository
    Left = 40
    Top = 16
    PixelsPerInch = 96
    object cxStyle_red: TcxStyle
      AssignedValues = [svColor]
      Color = clInfoBk
    end
  end
end
