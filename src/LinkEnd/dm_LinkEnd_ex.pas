unit dm_LinkEnd_ex;

interface

uses
  SysUtils, Classes, Forms,

  dm_Main,

  u_types,

  u_db,

  u_const_db, DB, ADODB

  ;


type
  TdmLinkEnd_ex = class(TDataModule)
    qry_Temp: TADOQuery;
    procedure DataModuleCreate(Sender: TObject);
  private
  public
    function GetBand_Params(aLinkEnd_ID: integer; var aTxRx_Shift, aChannelWidth,
        aFreqMinLow_MHz: double; var aChannelCount: integer; aObjName: string):
        boolean;
        
    function GetMaxAntennaHeight(aID: integer): Double;

  //  class procedure Init;

  end;

function dmLinkEnd_ex: TdmLinkEnd_ex;


implementation

uses dm_Onega_db_data;

{$R *.dfm}

var
  FdmLinkEnd_ex: TdmLinkEnd_ex;

// ---------------------------------------------------------------
function dmLinkEnd_ex: TdmLinkEnd_ex;
// ---------------------------------------------------------------
begin
 if not Assigned(FdmLinkEnd_ex) then
     FdmLinkEnd_ex := TdmLinkEnd_ex.Create(Application);

 Result := FdmLinkEnd_ex;
end;


//---------------------------------------------------------
function TdmLinkEnd_ex.GetMaxAntennaHeight(aID: integer): Double;
//---------------------------------------------------------
//function TdmLinkFreqPlan_Neighbors_calc.Calc_Diagram(aBLPoint1, aBLPoint2: TBLPoint;
//    aLinkEnd1,aLinkEnd2: TnbLinkFreqPlan_Linkend): Double;


const
  SQL = 'SELECT HEIGHT FROM '+ tbl_LinkEnd_ANTENNA +
        ' WHERE LinkEnd_id = :LinkEnd_id '+
        ' ORDER BY HEIGHT DESC';
begin
  dmOnega_DB_data.OpenQuery(qry_Temp, SQL, [FLD_LinkEnd_ID, aID]);

  if qry_Temp.IsEmpty then
    Result:= 0
  else
    Result:= qry_Temp.FieldByName(FLD_HEIGHT).AsFloat;
end;



procedure TdmLinkEnd_ex.DataModuleCreate(Sender: TObject);
begin
  db_SetComponentADOConnection(Self, dmMain.ADOConnection1);
end;

//--------------------------------------------------------------------
function TdmLinkEnd_ex.GetBand_Params(aLinkEnd_ID: integer; var aTxRx_Shift,
    aChannelWidth, aFreqMinLow_MHz: double; var aChannelCount: integer; aObjName:
    string): boolean;
//--------------------------------------------------------------------
// function TdmLinkFreqPlan_calc.GetFreq_Resource(aLinkEndID: integer; var
//    aTxRx_Shift, aChannelWidth, aFreqMin: double; var aChannelsAllowed:
//    string): Boolean;

//procedure TdmLinkFreqPlan_calc.Load_XMLToDB (aFileName: string);
//procedure TdmLinkFreqPlan_Tools.Save_Fixed(aLinkFreqPlan_ID: integer);
//procedure TdmLinkFreqPlan_LinkEnd_View.UpdateFreqTxRx(aDataset: TDataset);

const
  SQL_BAND = 'SELECT * FROM '+ TBL_LINKENDTYPE_BAND +' WHERE id=:id';

var
    sTableName: string;

  iLinkEndType_Band_ID: Integer;
begin
  Assert(aObjName <> '', 'No ObjName');

  if not ((aObjName = OBJ_LINKEND) or
          (aObjName = OBJ_PMP_SECTOR) or
          (aObjName = OBJ_PMP_TERMINAL))
  then
    Raise Exception.Create('Unknown ObjName');

  if (aObjName = OBJ_LINKEND)      then sTableName:=TBL_LINKEND;
  if (aObjName = OBJ_PMP_SECTOR)   then sTableName:=TBL_LINKEND; //TBL_PMP_SECTOR;
  if (aObjName = OBJ_PMP_TERMINAL) then sTableName:=TBL_LINKEND;// TBL_Pmp_Terminal;


//  iLinkEndTypeID:= gl_DB.GetIntFieldValueByID(sTableName, FLD_LINKENDTYPE_ID, aLinkEnd_ID);


  iLinkEndType_Band_ID:= gl_DB.GetIntFieldValueByID(sTableName, FLD_LINKENDTYPE_BAND_ID, aLinkEnd_ID);

//  sSubBand     := gl_DB.GetStringFieldValueByID(sTableName, FLD_SubBand, aLinkEnd_ID);
//  iBand_ID      := dmLinkEndType.GetBandID_by_Name (sSubBand, iLinkEndTypeID);
//  iBand_ID:= gl_DB.GetFieldValueByID(sTableName, FLD_BAND_ID, aLinkEnd_ID);

  dmOnega_db_data.OpenQuery(qry_Temp, SQL_BAND, [FLD_ID, iLinkEndType_Band_ID]);
//  db_O penQuery(qry_Temp, SQL_BAND, [db_par(FLD_ID, iBand_ID)]);

  with qry_Temp do
    if not EOF then
    begin
      aChannelWidth := FieldByName(FLD_CHANNEL_WIDTH).AsFloat;
      aTxRx_Shift   := FieldByName(FLD_TXRX_SHIFT).AsFloat;
      aFreqMinLow_MHz  := FieldByName(FLD_FREQ_MIN_LOW).AsFloat;
      aChannelCount := FieldByName(FLD_CHANNEL_COUNT).AsInteger;

      Result:=True;
    end else
      Result:=False;
end;

end.




(*
class procedure TdmLinkEnd_ex.Init;
begin
  if not Assigned(dmLinkEnd_ex) then
    dmLinkEnd_ex := TdmLinkEnd_ex.Create(Application);

  
end;
*)

