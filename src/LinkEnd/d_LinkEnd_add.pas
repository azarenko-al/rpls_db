unit d_LinkEnd_add;

interface
{$I ver.Inc}

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
  ActnList,
  cxButtonEdit, cxGraphics, cxVGrid, cxDropDownEdit,
  Registry,


  dm_Onega_DB_data,

  d_Wizard_Add_with_params,

  d_PmpTerminal_Plug,

//  u_LinkEnd_Add,



  dm_Act_LinkEndType,

  dm_Main,

 // u_dlg,


  dm_MapEngine_store,

  I_Shell,

  u_types,

  u_link_const_str,
  u_const_str,
  u_const_db,

  u_classes,
  u_img,
  u_func,


  u_cx_VGrid_export,
  u_cx_VGrid,
  u_Geo,


  dm_Library,
  dm_LinkEndType,

  dm_Property,

  dm_LinkEnd,

  dm_PmpTerminal,
  dm_Pmp_Sector,
  dm_Pmp_Site,

  cxStyles, cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxEdit,
  cxCheckBox, cxTextEdit, cxInplaceContainer, cxPropertiesStore,
  rxPlacemnt, ComCtrls, StdCtrls, ExtCtrls, dxSkinsCore,
  dxSkinsDefaultPainters;

type
  Tdlg_LinkEnd_add_rec = record
//    NewName: string;//='';

 //   FolderID: integer;
    BLPoint: TBLPoint;
    PropertyID: integer;

    ChannelType : string;


//    PmpSiteID: integer;
    PMPSectorID: integer;

    SectorIsSender: boolean;// = false): integer;

    //future using
  //  Dataset: TDataSet;
   // IsCreateOnMap : boolean;

  end;



  Tdlg_LinkEnd_add = class(Tdlg_Wizard_add_with_params)
    cxVerticalGrid1: TcxVerticalGrid;
    row_Property: TcxEditorRow;
    row_property_delimiter: TcxCategoryRow;
    row_AssociateWithPmpSite: TcxEditorRow;
    row_PmpSite1: TcxEditorRow;
    row_PmpSector1: TcxEditorRow;
    row_Pmp_Delimiter: TcxCategoryRow;
    row_Cell_Layer: TcxEditorRow;
    row_Calc_Model: TcxEditorRow;
    row_CalcRadius_km: TcxEditorRow;
    row_Calc_Delimiter: TcxCategoryRow;
    row_Equipment_Type: TcxEditorRow;
    row_Equip_Typed: TcxEditorRow;
    row_LinkEndType: TcxEditorRow;
    row_Mode: TcxEditorRow;
    row_Equip_not_Typed: TcxEditorRow;
    row_Freq_GHz: TcxEditorRow;
    row_Power_dBm: TcxEditorRow;
    row_Loss: TcxEditorRow;
    row_Raznos_MHz: TcxEditorRow;
    row_signature_width: TcxEditorRow;
    row_signature_height: TcxEditorRow;
    row_Sens_ber_3: TcxEditorRow;
    row_Sens_ber_6: TcxEditorRow;
    row_Kng: TcxEditorRow;
    row_Band: TcxEditorRow;
    cxVerticalGrid1CategoryRow5: TcxCategoryRow;
    cxVerticalGrid1EditorRow23: TcxEditorRow;
    row_Antenna1_Type: TcxEditorRow;
    row_Antenna1_Height: TcxEditorRow;
    row_Antenna1_Azimuth: TcxEditorRow;
    cxVerticalGrid1EditorRow29: TcxEditorRow;
    cxVerticalGrid1EditorRow30: TcxEditorRow;
    cxVerticalGrid1EditorRow31: TcxEditorRow;
    cxStyleRepository1: TcxStyleRepository;
    cxStyle1: TcxStyle;
    row_Property_type: TcxEditorRow;
    row_Property_Address: TcxEditorRow;
    row_Property_pos: TcxEditorRow;
    row_property_exists: TcxEditorRow;
    row_Pmp_sector: TcxCategoryRow;
    row_ChannelType: TcxEditorRow;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);

    procedure act_OkExecute(Sender: TObject);

//    procedure row_LinkendTypeButtonClick(Sender: TObject; AbsoluteIndex: Integer);
    procedure ActionList1Update(Action: TBasicAction; var Handled: Boolean);
    //procedure Button1Click(Sender: TObject);
    procedure cxVerticalGrid1DrawValue(Sender: TObject; ACanvas: TcxCanvas;
        APainter: TcxvgPainter; AValueInfo: TcxRowValueInfo; var Done: Boolean);

    procedure row_Property1EditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure row_Equipment_TypeEditPropertiesCloseUp(Sender: TObject);
    procedure cxVerticalGrid1StylesGetContentStyle(Sender: TObject;
      AEditProp: TcxCustomEditorRowProperties; AFocused: Boolean;
      ARecordIndex: Integer; var AStyle: TcxStyle);
//    procedure row_Template_siteEditPropertiesButtonClick(Sender: TObject;
 //     AButtonIndex: Integer);
  private
    FIDList: TIDList;

    FBLPoint: TBLPoint;


    FID: integer;
    FObjectName: string;

    FRec: TdmLinkEndAddRec;
   // FRec_antenna: TdmAntennaAddRec;

//    function AddProperty: integer;
    procedure DoOnEquipment_TypeChange();

    procedure Append();
    procedure Select_PmpSector(aEditorRow: TcxEditorRow);
    procedure UpdateAntennaMaxHeight;

    procedure SetObjectName(aObjectName : string);


  public
    class function ExecDlg_LinkEnd (aRec: Tdlg_LinkEnd_add_rec): integer;
    class function ExecDlg_PmpTerminal (aRec: Tdlg_LinkEnd_add_rec): integer;
    class function ExecDlg_PmpSector (aPmpSiteID: integer): integer;
  end;


//====================================================================
// implementation
//====================================================================

implementation
  {$R *.DFM}



//--------------------------------------------------------------------
class function Tdlg_LinkEnd_add.ExecDlg_LinkEnd (aRec: Tdlg_LinkEnd_add_rec): integer;
//--------------------------------------------------------------------
begin
{
  // ---------------------------------------------------------------
  if dmUser_Security.ProjectIsReadOnly then
  begin
    ShowMessage('���������� �������� ������. ������ ������ ��� ������.');
    Result:=0;
    Exit;
  end;
  // ---------------------------------------------------------------
}


  with Tdlg_LinkEnd_add.Create(Application) do
  begin
    FBLPoint:=aRec.BLPoint;

    if (aRec.PropertyID = 0) then
      aRec.PropertyID:=dmProperty.GetNearestID(aRec.BLPoint);

    FObjectName:=OBJ_LINKEND;
    SetActionName(STR_DLG_ADD_LINKEND);

//    ed_Name_.Text :=dmOnega_DB_data.SP_Object_GetNewName_new(OBJ_LINKEND, dmMain.ProjectID, aRec.PropertyID);

//    Integer = 0; aProperty_ID: Integer = 0): string;



 //   if aRec.NewName='' then
    ed_Name_.Text :=dmLinkEnd.GetNewName (aRec.PropertyID);

    if aRec.ChannelType<>''  then
      row_ChannelType.Properties.Value:=aRec.ChannelType;

    FRec.Property_ID  :=aRec.PropertyID;

    UpdateAntennaMaxHeight();

  //  FRec.FolderID    :=aRec.FolderID;


    row_Property.Properties.Value:=dmProperty.GetNameByID (FRec.Property_ID);

//    row_Property1.Text:=dmProperty.GetNameByID (FRec.Property_ID);
  //  row_Folder.Text  :=dmFolder.GetPath       (FRec.FolderID);

    SetObjectName (OBJ_LINKEND);


    row_AssociateWithPmpSite.Visible := False;
   // row_PmpSite.Visible := False;
   // row_PmpSector.Visible := False;
    row_Pmp_Delimiter.Visible := False;

    row_Cell_Layer.Visible := False;
    row_Calc_Model.Visible := False;
    row_CalcRadius_km.Visible := False;
    row_Calc_Delimiter.Visible := False;
    row_Antenna1_Azimuth.Visible := False;
//------------------------------------------------------

    if (ShowModal=mrOk) then
      Result:=FID
    else
      Result:=0;

    Free;
  end;
end;


//--------------------------------------------------------------------
class function Tdlg_LinkEnd_add.ExecDlg_PmpTerminal  (aRec: Tdlg_LinkEnd_add_rec): integer;
//--------------------------------------------------------------------
begin
{
  // ---------------------------------------------------------------
  if dmUser_Security.ProjectIsReadOnly then
  begin
    ShowMessage('���������� �������� ������. ������ ������ ��� ������.');
    Result:=0;
    Exit;
  end;
  // ---------------------------------------------------------------
}

  with Tdlg_LinkEnd_add.Create(Application) do
  begin
    FBLPoint:=aRec.BLPoint;


    row_PmpSite1.Properties.Value:='';
    row_PmpSector1.Properties.Value:='';


    if (aRec.PropertyID = 0) and (aRec.BLPoint.B <> 0) then
      aRec.PropertyID:=dmProperty.GetNearestID(aRec.BLPoint);

    FObjectName:=OBJ_PMP_TERMINAL;
    SetActionName(STR_DLG_ADD_PMPTERMINAL);

    ed_Name_.Text := dmPmpTerminal.GetNewName(aRec.PropertyID);

  //  row_PmpSite.ReadOnly := true;
//    row_PmpSite.OnButtonClick := nil;
//    row_PmpSite.HideEditCursor := true;

    if aRec.SectorIsSender then
    begin
      row_AssociateWithPmpSite.Properties.Value:=False;
    //  row_AssociateWithPmpSite_.ReadOnly:=true;
    end;

    // ---------------------------------------------------------------
    if (aRec.PropertyID > 0) then
    // ---------------------------------------------------------------
    begin
      FRec.Property_ID   :=aRec.PropertyID;
      row_Property.Properties.Value :=dmProperty.GetNameByID (FRec.Property_ID);

      FRec.PmpSector.PmpSite_ID:=dmPmpSite.GetNearestID (aRec.PropertyID);

//      row_PmpSite.Properties.Value:=dmPmpSite.GetNameByID  (FRec.PmpSector.PmpSite_ID);

//      FRec.PmpTerminal.PmpSector_ID:=gl_DB.GetIntFieldValue (TBL_PMP_SECTOR, FLD_ID,
//                           [db_Par(FLD_PMP_SITE_ID, Frec.PmpTerminal.PmpSite_ID)]);
//
//      row_PmpSector.Properties.Value:=dmPmp_Sector.GetNameByID  (FRec.Pmp.PmpSector_ID);

    end;

    // ---------------------------------------------------------------
    if (aRec.PMPSectorID > 0) then
    // ---------------------------------------------------------------
    begin
(*      FRec.Pmp.PmpSector_ID  :=aRec.PMPSectorID;
      row_PmpSector.Properties.Value:=dmPmp_Sector.GetNameByID(FRec.Pmp.PmpSector_ID);

      FRec.Pmp.PmpSite_ID    :=dmPmp_Sector.GetSiteID (aRec.PMPSectorID);
      row_PmpSite.Properties.Value  :=dmPmpSite.GetNameByID (FRec.Pmp.PmpSite_ID);
*)
    end;

  //  FRec.FolderID       :=aRec.FolderID;
  //  row_Folder.Text     :=dmFolder.GetPath (FRec.FolderID);

//------------------------------------------------------
    SetObjectName (OBJ_PMP_TERMINAL);



    //PMP SITE, SECTOR
    row_Antenna1_Azimuth.Visible := False;
    row_Cell_Layer.Visible := False;
    row_Calc_Model.Visible := False;
    row_CalcRadius_km.Visible := False;
    row_Calc_Delimiter.Visible := False;

    //LINKEND
//    row_IsRepeater.Visible := False;


    if (ShowModal=mrOk) then
      Result:=FID
    else
      Result:=0;

    Free;
  end;
end;


//--------------------------------------------------------------------
class function Tdlg_LinkEnd_add.ExecDlg_PmpSector(aPmpSiteID: integer): integer;
//--------------------------------------------------------------------
begin
{
  // ---------------------------------------------------------------
  if dmUser_Security.ProjectIsReadOnly then
  begin
    ShowMessage('���������� �������� ������. ������ ������ ��� ������.');
    Result:=0;
    Exit;
  end;
  // ---------------------------------------------------------------
}

  with Tdlg_LinkEnd_add.Create(Application) do
  try


    FRec.PmpSector.PmpSite_ID:=aPmpSiteID;

//    if (FRec.PmpSector.PmpSite_ID > 0) then
//      row_PmpSite.Properties.Value:=dmPmpSite.GetNameByID(FRec.PmpSector.PmpSite_ID);

    FObjectName:=OBJ_PMP_SECTOR;
    SetActionName(STR_DLG_ADD_PMP_SECTOR);

   // FRec.FolderID   :=aFolderID;
 //   row_Folder.Text :=dmFolder.GetPath (FRec.FolderID);


//    ed_Name_.Text:=dmPMP_Sector.GetNewName_For_Cell1(FRec.PmpSector.PmpSite_ID);
    ed_Name_.Text:=dmPMP_Sector.GetNewName(FRec.PmpSector.PmpSite_ID);


    //------------------------------------------------------
    SetObjectName (OBJ_PMP_SECTOR);


    //PMP SECTOR
    row_Property.Visible := False;

    row_property_delimiter.Visible := False;

    //PMP SITE
    row_AssociateWithPmpSite.Visible := False;
//    row_PmpSector.Visible := False;
    row_Pmp_Delimiter.Visible := False;


   // row_IsRepeater.Visible := False;
    //---------------------------------

    if (ShowModal=mrOk) then
      Result:=FID
    else
      Result:=0;

  finally
    Free;
  end;
end;

//--------------------------------------------------------------------
procedure Tdlg_LinkEnd_add.UpdateAntennaMaxHeight;
//--------------------------------------------------------------------
var
  eH: Double;
begin
  eH:=dmOnega_DB_data.Property_GetMaxHeight(FRec.Property_ID);

  if eH>0 then
    row_Antenna1_Height.Properties.Value:=eH;
end;



//--------------------------------------------------------------------
procedure Tdlg_LinkEnd_add.FormCreate(Sender: TObject);
//--------------------------------------------------------------------
begin
  inherited;

//  Assert(Assigned(IMapEngine));


//  ShowMessage('debug');


 ///////////// {$ELSE}
   /////////  cxVerticalGrid1.Visible := False;
 /////////// {$ENDIF}



  row_Property.Properties.Caption := STR_PROPERTY;
//  row_Folder.Caption   := STR_FOLDER;

  row_AssociateWithPmpSite.Properties.Caption := '����������� �������� � �������';
    row_PmpSite1.Properties.Caption       := STR_PMP_SITE;
    row_PmpSector1.Properties.Caption     := STR_PMP_SECTOR;
   // row_Pmp_Delimiter.Properties.Caption := '';

//  row_PmpSite.Properties.Caption:=  STR_PMP_SITE;
//  row_PmpSector.Properties.Caption:=STR_PMP_SECTOR;

  row_Cell_Layer.Properties.Caption:=STR_CELL_LAYER;
  row_Calc_Model.Properties.Caption:=STR_CALC_MODEL;
  row_CalcRadius_km.Properties.Caption    :=STR_CALC_RADIUS_km;

  row_Freq_GHz.Properties.Caption         := STR_NOT_TYPED_1_FREQ_GHz;
  row_Power_dBm.Properties.Caption        := STR_NOT_TYPED_2_POWER_dBm;
  row_Loss.Properties.Caption             := STR_NOT_TYPED_3_LOSS_dB;
  row_Raznos_MHz.Properties.Caption       := STR_NOT_TYPED_4_RAZNOS_MHz;
  row_signature_width.Properties.Caption  := STR_NOT_TYPED_5_SIGNATURE_WIDTH_MHz;
  row_signature_height.Properties.Caption := STR_NOT_TYPED_6_SIGNATURE_HEIGHT_dB;
  row_Sens_ber_3.Properties.Caption        := STR_NOT_TYPED_7_SENS_3_dBm;
  row_Sens_ber_6.Properties.Caption        := STR_NOT_TYPED_8_SENS_6_dBm;
  row_Kng.Properties.Caption              := STR_NOT_TYPED_9_KNG;

  row_ChannelType.Properties.Caption       := STR_Channel_Type;



(*
  STR_NOT_TYPED_1_FREQ_GHz         = '1 ������� (��������) [GHz]';
  STR_NOT_TYPED_2_POWER_dBm        = '2 �������� �������� ��� [dBm]';
  STR_NOT_TYPED_3_LOSS_dB          = '3 ������ ��� (�� ������) [dB]';
  STR_NOT_TYPED_4_KRATNOSTBYSPACE  = '4 ��������� ���������� �� ������������';
  STR_NOT_TYPED_5_KRATNOSTBYFREQ   = '5 ��������� ���������� �� �������';
  STR_NOT_TYPED_6_RAZNOS_MHz       = '6 Min ������ ������� [MHz]';
 // STR_NOT_TYPED_EQUALISER_PROFIT_dB = '7 �������, �������� ������������ [dB]';
  STR_NOT_TYPED_7_SIGNATURE_WIDTH_MHz = '7 ������ ��������� [MHz]';
  STR_NOT_TYPED_8_SIGNATURE_HEIGHT_dB = '8 ������ ��������� [dB]';
  STR_NOT_TYPED_9_SENS_3_dBm       = '9 ���������������� ��� BER 10^-3 [dBm]';
  STR_NOT_TYPED_10_SENS_6_dBm      = '10 ���������������� ��� BER 10^-6 [dBm]';
  STR_NOT_TYPED_11_KNG             = '11 ���  ������������ [%]';
*)


  row_Property_pos.Properties.Caption    :='����������';
  row_Property_address.Properties.Caption:='�����';



  //------------------------------------------------------
  row_Property.Properties.Caption := STR_PROPERTY;
//  row_Folder.Properties.Caption   := STR_FOLDER;


  row_AssociateWithPmpSite.Properties.Caption := '����������� �������� � �������';
    row_PmpSite1.Properties.Caption       := STR_PMP_SITE;
    row_PmpSector1.Properties.Caption     := STR_PMP_SECTOR;
   // row_Pmp_Delimiter.Properties.Caption := '';

//  row_PmpSite.Properties.Caption :=  STR_PMP_SITE;
//  row_PmpSector.Properties.Caption :=STR_PMP_SECTOR;

  row_Cell_Layer.Properties.Caption    :=STR_CELL_LAYER;
  row_Calc_Model.Properties.Caption    :=STR_CALC_MODEL;
  row_CalcRadius_km.Properties.Caption :=STR_CALC_RADIUS_km;

 // row_Equipment_Type.Properties.Caption:='��� ���';

  row_Equipment_Type.Properties.Caption:='��� ���';

  ////////////////////
  ////////////////////
  ////////////////////
  ////////////////////
  row_Property_pos.Visible :=False;
  row_Property_Address.Visible :=False;
  row_property_exists.Visible :=False;


(*  with TcxComboBoxProperties(
           row_Equipment_Type.Properties.EditProperties) do
  begin
    Items.Clear;
    Items.Add(STR_TYPED);
    Items.Add(STR_NOT_TYPED);

  end;
*)
  cx_SetRowItemValues (row_Equipment_Type, [STR_TYPED, STR_NOT_TYPED]);

//  cx_s


(*
  row_Equipment_Type.Items.Add('�������');
  row_Equipment_Type.Items.Add('���������');
*)

  row_Freq_GHz.Properties.Caption         := STR_NOT_TYPED_1_FREQ_GHz;
  row_Power_dBm.Properties.Caption        := STR_NOT_TYPED_2_POWER_dBm;
  row_Loss.Properties.Caption             := STR_NOT_TYPED_3_LOSS_dB;
  row_Raznos_MHz.Properties.Caption       := STR_NOT_TYPED_4_RAZNOS_MHz;
  row_signature_width.Properties.Caption  := STR_NOT_TYPED_5_SIGNATURE_WIDTH_MHz;
  row_signature_height.Properties.Caption := STR_NOT_TYPED_6_SIGNATURE_HEIGHT_dB;
  row_Sens_ber_3.Properties.Caption       := STR_NOT_TYPED_7_SENS_3_dBm;
  row_Sens_ber_6.Properties.Caption       := STR_NOT_TYPED_8_SENS_6_dBm;
  row_Kng.Properties.Caption              := STR_NOT_TYPED_9_KNG;


//  row_AssociateWithPmpSite.OnToggleClick := row_ChekTempToggleClick;

//  row_Not_typed.Node.Collapse(true);

  lb_Name.Caption:=STR_NAME;

//  row_Property.Caption:= STR_PROPERTY;


  with TRegIniFile.Create(FRegPath) do
  begin
    //////////
    FRec.Antenna1.AntennaTypeID:= ReadInteger('', FLD_ANTENNATYPE_ID, 0);


    //////////
    FRec.LinkEndType_ID        := ReadInteger('', FLD_LINKENDTYPE_ID, 0);
    FRec.PmpSector.CellLayer_ID      := ReadInteger('', FLD_CELL_LAYER_ID, 0);
    FRec.PmpSector.CalcModel_ID      := ReadInteger('', FLD_CALC_MODEL_ID, 0);

    FRec.LinkEndType_Mode_ID   := ReadInteger('', FLD_LinkEndType_Mode_ID, 0);

//    FRec.PmpTerminal.PmpSite_ID   := ReadInteger('', FLD_PmpSiteID,   0);
//    FRec.PmpTerminal.PmpSector_ID := ReadInteger('', 'PmpSector_ID', 0);


{
    PmpSiteID: integer;
    PMPSectorID: integer;
    SectorIsSender: boolean;// = false): integer;
}


   // if Assigned(row_AssociateWithPmpSite) then
///////    row_AssociateWithPmpSite.Properties.Value:= ReadBool ('', row_AssociateWithPmpSite.Name, true);

    Free;
  end;

  cx_VerticalGrid_LoadFromReg(cxVerticalGrid1, FRegPath);



  //----------------
  row_Antenna1_Type.Properties.Value:= gl_DB.GetNameByID(TBL_ANTENNATYPE, FRec.Antenna1.AntennaTypeID);


  //----------------
  row_Property.Properties.Value  := gl_DB.GetNameByID(TBL_Property, FRec.Property_ID);


  row_LinkEndType.Properties.Value  := gl_DB.GetNameByID(TBL_LINKENDTYPE, FRec.LinkEndType_ID);
  row_Cell_Layer.Properties.Value   := gl_DB.GetNameByID(TBL_CELLLAYER,   FRec.PmpSector.CellLayer_ID);
  row_Calc_Model.Properties.Value   := gl_DB.GetNameByID(TBL_CALCMODEL,   FRec.PmpSector.CalcModel_ID);


//  row_PmpSite.Properties.Value   := gl_DB.GetNameByID(TBL_PMP_SITE,   FRec.PmpSector.PmpSite_ID);

//  row_PmpSector.Properties.Value := gl_DB.GetNameByID(TBL_PMP_SECTOR,   FRec.PmpTerminal.PmpSector_ID);




  DoOnEquipment_TypeChange();

 // Update_row (FLinkEndTypeID);

  Height:=385;
  SetDefaultWidth ();

  act_ok.Enabled:=False;

  PostMessage(btn_Ok.Handle, WM_SETFOCUS,0,0);
//  btn_Ok.SetFocus;

  FIDList:=TIDList.Create;

  cx_InitVerticalGrid(cxVerticalGrid1);

  //dmLIbrary.Bands
//  row_Band.Items.Assign(dmLIbrary.Bands);

//  row_Band_.Properties.EditPropertiesClass:=TcxComboBoxProperties;

 // TcxComboBoxProperties(row_Band_.Properties).

  TcxComboBoxProperties(row_Band.Properties.EditProperties).Items.Assign (dmLIbrary.Bands);

end;


//--------------------------------------------------------------------
procedure Tdlg_LinkEnd_add.FormDestroy(Sender: TObject);
//--------------------------------------------------------------------
begin
  cx_VerticalGrid_SaveToReg(cxVerticalGrid1, FRegPath);


  with TRegIniFile.Create(FRegPath) do
  begin
    //--------------
    WriteInteger('', FLD_Property_ID,        FRec.Property_ID);

    WriteInteger('', FLD_ANTENNATYPE_ID,        FRec.Antenna1.AntennaTypeID);

    //--------------

    WriteInteger('', FLD_LINKENDTYPE_ID,        FRec.LinkEndType_ID);
    WriteInteger('', FLD_CELL_LAYER_ID,         FRec.PmpSector.CellLayer_ID);
    WriteInteger('', FLD_CALC_MODEL_ID,         FRec.PmpSector.CalcModel_ID);

    WriteInteger('', FLD_LinkEndType_Mode_ID,   FRec.LinkEndType_Mode_ID);


  //  WriteInteger('', FLD_PmpSiteID,   FRec.PmpSector.PmpSite_ID );

  //  WriteInteger('', 'PMPSectorID', FRec.PmpTerminal.PmpSector_ID);



{    WriteString ('',  row_equipment_type.Name,   row_equipment_type.Properties.Value);

    WriteString ('',  row_Power_dBm.Name,        AsString(row_Power_dBm.Properties.Value));
    WriteString ('',  row_Freq_GHz.Name,         AsString(row_Freq_GHz.Properties.Value));
    WriteString ('',  row_Sens_ber_3.Name,       AsString(row_Sens_ber_3.Properties.Value));
    WriteString ('',  row_Sens_ber_6.Name,       AsString(row_Sens_ber_6.Properties.Value));
    WriteString ('',  row_Loss.Name,             AsString(row_Loss.Properties.Value));
    WriteString ('',  row_Raznos_MHz.Name,       AsString(row_Raznos_MHz.Properties.Value));
    WriteString ('',  row_signature_height.Name, AsString(row_signature_height.Properties.Value));
    WriteString ('',  row_signature_width.Name,  AsString(row_signature_width.Properties.Value));
    WriteString ('',  row_Antenna1_Height.Name,  AsString(row_Antenna1_Height.Properties.Value));
    WriteString ('',  row_Kng.Name,              AsString(row_Kng.Properties.Value));
    WriteString ('',  row_Band.Name,             AsString(row_Band.Properties.Value));
//    WriteString ('', row_AssociateWithPmpSite.Name, AsString(row_AssociateWithPmpSite.Properties.Value));
    WriteString ('', row_Mode.Name,             AsString(row_Mode.Properties.Value));
    WriteString ('', row_CalcRadius_km.Name,    AsString(row_CalcRadius_km.Properties.Value));
}


    Free;
  end;

  inherited;
end;


//----------------------------------------------------------------
procedure Tdlg_LinkEnd_add.act_OkExecute(Sender: TObject);
//----------------------------------------------------------------
begin
  inherited;

  Append();
end;

//-------------------------------------------------------------------
procedure Tdlg_LinkEnd_add.DoOnEquipment_TypeChange();
//-------------------------------------------------------------------
var
  iInd: integer;
begin
  iInd:=cx_GetRowItemIndex(row_Equipment_Type);

  row_Equip_Typed.Visible    :=iInd=0;
  row_Equip_not_Typed.Visible:=iInd<>0;
end;



//----------------------------------------------------------------
procedure Tdlg_LinkEnd_add.ActionList1Update(Action: TBasicAction; var Handled: Boolean);
//----------------------------------------------------------------
var
  bMainConditions, bDependentConditions: boolean;
  b: boolean;
begin
  //------------------------------------------------------
  if FObjectName = OBJ_LINKEND then
  //------------------------------------------------------
    bMainConditions:= (row_Property.Properties.Value <> '');

  //------------------------------------------------------
  if FObjectName = OBJ_PMP_TERMINAL then
  //------------------------------------------------------
  begin
    if row_AssociateWithPmpSite.Properties.Value=true then
      bMainConditions:= (row_Property.Properties.Value <> '') and
                        (row_PmpSite1.Properties.Value <> '') and
                        (row_PmpSector1.Properties.Value <> '')
    else
      bMainConditions:= (row_Property.Properties.Value <> '');
  end;

  //------------------------------------------------------
  if FObjectName = OBJ_PMP_SECTOR then
  //------------------------------------------------------
    bMainConditions:= (row_PmpSite1.Properties.Value <> '') and
                     /// (row_Calc_Model.Text <> '') and
                      (row_Cell_Layer.Properties.Value <> '');


//  bMainConditions:= (row_Property.Text <> '') and (row_Folder.Text <> '');
(*
  STR_TYPED         = '�������';
  STR_NOT_TYPED     = '���������';
*)

  b:=Eq(AsString(row_equipment_type.Properties.Value), STR_TYPED);

  if b then
    bDependentConditions:= (row_LinkendType.Properties.Value   <> '') and
                           (row_Antenna1_Type.Properties.Value <> '')
  else
    bDependentConditions:=true;

  bMainConditions:=bMainConditions and
                  (AsFloat(row_Antenna1_Height.Properties.Value)>0);

  act_ok.Enabled:=bMainConditions and bDependentConditions;
end;


//-------------------------------------------------------------------
procedure Tdlg_LinkEnd_add.Append;
//-------------------------------------------------------------------
var
  iLink, iPropertyID: Integer;

  blPropertyPos, blTerminalPos: TBLPoint;
  eAzimuth: Double;

//  rLink: TdmLinkAddRec;
//  oLinkEndAdd: TLinkEndAdd1;
  iInd: integer;
  iLink_ID: Integer;
  k: Integer;
begin
///  oLinkEndAdd := TLinkEndAdd1.Create();
 // oLinkEndAdd.NewName :=ed_Name_.Text;

 // oLinkEndAdd.


//  oPropertyAdd.Height    := AsFloat(row_Height.Properties.Value);
 // oPropertyAdd.Azimuth   := row_Azimuth.Properties.Value;
//  FAntennaAdd.PropertyID:= FPropertyID;

//  oPropertyAdd.AntennaType_ID :=FRec.AntennaTypeID;

////////////  FID:=oLinkEndAdd.Add(dmMain.ADOConnection);



  FRec.NewName  :=ed_Name_.Text;

  iInd:=cx_GetRowItemIndex(row_equipment_type);

  FRec.EquipmentType:=iInd;
//  FRec.EquipmentType:=IIF(row_equipment_type.Text=row_equipment_type.Items[0], 0,1);

  FRec.Channel_Type  := AsString(row_ChannelType.Properties.Value);

  case FRec.EquipmentType of
    // �������
    0:  begin

        // FRec.Mode := AsInteger(row_Mode.Text);
        // FRec.BAND:= dmLinkEndType.GetBand(FRec.LinkEndType_ID);

     //    oLinkEndAdd.BAND  :=

      //   oLinkEndAdd.LinkEndType_ID:=FRec.LinkEndType_ID;
      //   oLinkEndAdd.LinkEndType_Mode_ID:=FRec.LinkEndType_Mode_ID;

//         oLinkEndAdd.Mode:=FRec.Mode;
       end;

    // ���������
    1: begin
         FRec.LinkEndType_ID :=0;

(*         // ---------------------------------------------------------------
         oLinkEndAdd.BAND:=AsString(row_Band.Properties.Value);

         oLinkEndAdd.TxFreq_MHz         := AsFloat(row_Freq_GHz.Properties.Value)*1000;

         oLinkEndAdd.Power_dBm          := AsFloat(row_Power_dBm.Properties.Value);

         oLinkEndAdd.Threshold_BER_3    := AsFloat(row_Sens_ber_3.Properties.Value);
         oLinkEndAdd.Threshold_BER_6    := AsFloat(row_Sens_ber_6.Properties.Value);
         oLinkEndAdd.Loss_dB            := AsFloat(row_Loss.Properties.Value);
     //    oLinkEndAdd.equaliser_profit   := 0;///AsFloat(row_equaliser_pro
         oLinkEndAdd.Freq_Spacing_MHz   := AsFloat(row_Raznos_MHz.Properties.Value);
         oLinkEndAdd.Signature_height_dB:= AsFloat(row_signature_height.Properties.Value);
         oLinkEndAdd.Signature_width_MHz:= AsFloat(row_signature_width.Properties.Value);
         oLinkEndAdd.Kng                := AsFloat(row_Kng.Properties.Value);
*)
         // ---------------------------------------------------------------

//         FRec.LinkEndType_ID:=0;

         FRec.BAND:=AsString(row_Band.Properties.Value);

         FRec.TxFreq_MHz        := AsFloat(row_Freq_GHz.Properties.Value)*1000;

        // dPower_dBm             := AsFloat(row_Power_dBm.Text);

         FRec.Power_dBm         := AsFloat(row_Power_dBm.Properties.Value);
      //   FRec.Power_Max_dBm     := FRec.Power_dBm;

         FRec.Channel_Type  := AsString(row_ChannelType.Properties.Value);

         FRec.Threshold_BER_3   := AsFloat(row_Sens_ber_3.Properties.Value);
         FRec.Threshold_BER_6   := AsFloat(row_Sens_ber_6.Properties.Value);
         FRec.Loss_dB           := AsFloat(row_Loss.Properties.Value);
   //      FRec.equaliser_profit  := 0;///AsFloat(row_equaliser_profit.Text);
         FRec.Freq_Spacing_MHz  := AsFloat(row_Raznos_MHz.Properties.Value);

     ////////////    FRec.Kratnost_by_Space := Flist1.IndexOf(row_kratnostBySpace.Text);
     ///////////    FRec.Kratnost_by_Freq  := Flist2.IndexOf(row_kratnostByFreq.Text);
         FRec.Signature_height_dB := AsFloat(row_signature_height.Properties.Value);
         FRec.Signature_width_MHz := AsFloat(row_signature_width.Properties.Value);
         FRec.Kng                 := AsFloat(row_Kng.Properties.Value);


        // oLinkEndAdd.TxFreq:=FRec.TxFreq;
      //   oLinkEndAdd. Mode:=FRec.Mode;

//         oLinkEndAdd.Threshold_BER_3

       end;
  else
    raise Exception.Create('');
  end;


  // -------------------------------------------------------------------
  if Eq(FObjectName, OBJ_LINKEND) then
  // -------------------------------------------------------------------
  begin
  //  oLinkEndAdd.ObjName:=OBJ_LINKEND;

 //   oLinkEndAdd.IsRepeater:=AsBoolean(row_IsRepeater.Properties.Value);

 //   FRec.IsRepeater:=AsBoolean(row_IsRepeater.Properties.Value);
  end else

  // -------------------------------------------------------------------
  if Eq(FObjectName, OBJ_PMP_TERMINAL) then
  // -------------------------------------------------------------------
  begin
    FillChar (FRec.PmpSector, SizeOf (FRec.PmpSector), 0);


 //   oLinkEndAdd.ObjName:=OBJ_PMP_TERMINAL;

//
//    blPropertyPos        := dmPROPERTY.GetPos(FRec.Property_ID);
//    blTerminalPos        := dmPmpSite.GetPropertyPos(FRec.PmpSector.PmpSite_ID);
//
//    eAzimuth:= geo_Azimuth(blTerminalPos, blPropertyPos);
//
//    FRec.Antenna1_.Azimuth:= eAzimuth;
//    FRec_antenna.Azimuth := eAzimuth;

    if row_AssociateWithPmpSite.Properties.Value = false then
    begin
   //   oLinkEndAdd.PmpTerminal.PmpSector_ID := FRec.PmpTerminal.PmpSector_ID;

   // ..  FRec.Pmp.PmpSite_ID:=0;
      FRec.PmpTerminal.PmpSector_ID:=0;
    end;

  end else

  // -------------------------------------------------------------------
  if FObjectName = OBJ_PMP_SECTOR then
  // -------------------------------------------------------------------
  begin

(*    oLinkEndAdd.ObjName:=OBJ_PMP_SECTOR;
    oLinkEndAdd.PmpSector.CalcModel_ID:=FRec.PmpSector.CalcModel_ID;
    oLinkEndAdd.PmpSector.CellLayer_ID:=FRec.PmpSector.CellLayer_ID;
    oLinkEndAdd.PmpSector.CalcRadius_km:=FRec.PmpSector.CalcRadius_km;
*)

    Frec.PmpSector.Color:= img_GetRandomColor();
    FRec.PmpSector.CalcRadius_km:=AsFloat(row_CalcRadius_km.Properties.Value);

 //   FRec_antenna.Azimuth := AsFloat(row_Antenna1_Azimuth.Properties.Value);

 // - !!!!
    FRec.Antenna1.Azimuth := -AsFloat(row_Antenna1_Azimuth.Properties.Value);

  end;


  FRec.Antenna1.Height := AsFloat(row_Antenna1_Height.Properties.Value);
 

//  FRec.ObjectName:=FObjectName;

//  assert (FRec.Property_ID > 0);

  FID:=dmLinkEnd.Add1 (FRec, FObjectName);

  assert (FID > 0);

 // -------------------------------------------------------------------
  if (FID>0) and Eq(FObjectName, OBJ_LINKEND) then begin
 // -------------------------------------------------------------------
     dmMapEngine_store.Feature_Add(OBJ_LINKEND, FID);


(*    if not dmMapEngine_store.Is_Object_Map_Exists(OBJ_LINKEND) then
       dmMapEngine_store.Open_Object_Map(OBJ_LINKEND);

*)
    g_ShellEvents.Shell_UpdateNodeChildren_ByObjName (OBJ_PROPERTY, FRec.Property_ID);
  //  g_Shell.Shell_UpdateNodeChildren_ByObjName (OBJ_PROPERTY, FRec.Property_ID);

  end else


 // -------------------------------------------------------------------
  if(FID>0) and Eq(FObjectName , OBJ_PMP_SECTOR) then begin
 // -------------------------------------------------------------------
  //  FIDList.Clear;
  //  dmPMP_Sector.GetAntennaIDList (FID, FIDList);

   // if Assigned(IMapEngine) then

// ..  k:=FRec.Property_ID;

//    g_ShellEvents.Shell_UpdateNodeChildren_ByObjName (OBJ_PROPERTY, FRec.Property_ID);


 //   dmMapEngine.Sync (otPmpSectorAnt);

  end else


 // -------------------------------------------------------------------
  if (FID>0) and Eq(FObjectName , OBJ_PMP_TERMINAL) then
 // -------------------------------------------------------------------
  begin
////    dmMapEngine.Sync (otCellAnt);
 //   Assert(FRec.PmpSectorID>0);
  //  Assert(FRec.PmpSiteID>0);
   //  if Assigned(IMapEngine) then
    //  begin

      dmMapEngine_store.Feature_Add(OBJ_Pmp_Terminal, FID);

      k:=FRec.Property_ID;


      g_ShellEvents.Shell_UpdateNodeChildren_ByObjName (OBJ_PROPERTY, FRec.Property_ID);



      //CREATE FUNCTION dbo.fn_LinkEnd_Get_Link_ID

      dmOnega_DB_data.OpenQuery(dmOnega_DB_data.ADOQuery1, Format('select dbo.fn_LinkEnd_Get_Link_ID (%d)', [FID] ));

      iLink_ID:=dmOnega_DB_data.ADOQuery1.Fields[0].AsInteger;
      if iLink_ID>0 then
        dmMapEngine_store.Feature_Add(OBJ_LINK, iLink_ID);

//
//      if FRec.PmpTerminal.PmpSector_ID>0 then
//      begin
//        iLink:=dmPmpTerminal.SwitchSector(FID, FRec.PmpTerminal.PmpSector_ID);
//
//        if iLink>0 then
//        begin
//
//
//            dmMapEngine_store.Feature_Add(OBJ_LINK, FID);
//
//
//        end;
//       //   begin
//          //  if Assigned(IMapEngine) then
//        //  end;
//      //  end;
//      end;

//
//    if (FRec.Pmp.PmpSite_ID>0) then
//      iPropertyID:= dmPmpSite.GetPropertyID(FRec.Pmp.PmpSite_ID);
//
//    if (FRec.Pmp.PmpSite_ID>0) and (iPropertyID > 0) and (iPropertyID <> FRec.Property_ID) then
//    begin
//      FillChar (rLink, SizeOf(rLink), 0);
//
//      rLink.Name:= row_PmpSite.Text+ ' <-> ' +ed_Name_.Text;
//
//      rLink.ClutterModelID:=1;
//      rLink.PmpSiteID    :=FRec.Pmp.PmpSite_ID;
//      rLink.PmpSectorID  :=FRec.Pmp.PmpSector_ID;
//      rLink.PmpTerminalID:=FID;
//
//
//      iLink:=dmLink.Add(rLink);
//      if iLink>0 then
//   //   begin
//      //  if Assigned(IMapEngine) then
//          dmMapEngine1.CreateObject (otLink, iLink);
//    //  end;
//    end;


  end;


////  FreeAndNil(oLinkEndAdd);


 // if assigned(IMapAct) then



{ TODO : !!!!!!!!!!!!!!!!!!!! }

{  if Assigned(IMapAct) then
    dmAct_Map.MapRefresh;


   if Assigned(IShell) then
  begin
    sGUID:=dmProperty.GetGUIDByID(FRec.PropertyID);

    dmAct_Explorer.Shell_UpdateNodeChildren(sGUID);
  end;
}


//  if FObjectName = OBJ_PMPTERMINAL then
 //   dmMapEngine.Sync (otPmpTerminalAnt);
end;


// ---------------------------------------------------------------
procedure Tdlg_LinkEnd_add.Select_PmpSector(aEditorRow: TcxEditorRow);
// ---------------------------------------------------------------
var
//  oSectorIDList: TIDLIst;

  rec: Tdlg_PmpTerminal_Plug_rec;

begin



//  oSectorIDList:=TIDLIst.Create;

//  rec.PmpSite_ID  :=Frec.Pmp.PmpSite_ID;
  rec.PmpSector_ID:=Frec.PmpTerminal.PmpSector_ID;


  if Tdlg_PmpTerminal_Plug.ExecDlg(0, dmMain.ProjectID, rec) then
  begin
    Frec.PmpTerminal.PmpSector_ID:=rec.PmpSector_ID;
 //   Frec.Pmp.PmpSite_ID  :=rec.PmpSite_ID;

 //   cx_SetButtonEditText (row_PmpSector, rec.PmpSector_name);
  //  cx_SetButtonEditText (row_PmpSite, rec.PmpSite_name);

//    row_PmpSector.e

   // cxVerticalGrid1.


    row_PmpSector1.Properties.Value:=rec.PmpSector_name;
    row_PmpSite1.Properties.Value  :=rec.PmpSite_name;

    row_AssociateWithPmpSite.Properties.Value  := True;

//    aSender.PostEditValue;

    cxVerticalGrid1.CancelEdit;

//    aSender.Post

//    aEditorRow.PostEditValue;

//    if  then

  end;

end;




//--------------------------------------------------------------
procedure Tdlg_LinkEnd_add.cxVerticalGrid1DrawValue(Sender: TObject; ACanvas:
     TcxCanvas; APainter: TcxvgPainter; AValueInfo: TcxRowValueInfo; var Done: Boolean);
//--------------------------------------------------------------
var
//  s: string;
  oRow: TcxEditorRow;
  s: string;
  b: boolean;
begin
 // AValueInfo.
  s:=Sender.className;


  if not (AValueInfo.Row is TcxEditorRow) then
    Exit;

  oRow:=TcxEditorRow(AValueInfo.Row);
  s:=oRow.Name;

  cx_ColorRow(oRow, [row_Antenna1_Type,
                     row_Antenna1_Height,
                     row_Property,
                     row_Cell_Layer,
                     row_PmpSite1], ACanvas,  true, clSilver);

  b:=Eq(AsString(row_equipment_Type.Properties.Value), STR_TYPED);

  if b then
    cx_ColorRow(oRow, [row_LinkEndType],  ACanvas,  true, clSilver)
  else
    cx_ColorRow(oRow, [row_Power_dBm,
                       row_Freq_GHz,
                       row_Sens_ber_3,
                       row_Sens_ber_6,
                       row_signature_height,
                       row_signature_width
                       ],  ACanvas,  true, clSilver);

  if (FObjectName = OBJ_PMP_TERMINAL) then
    if (row_AssociateWithPmpSite.Properties.Value=true)
  then
    cx_ColorRow(oRow, [row_PmpSector1],  ACanvas,  true, clSilver);

end;




procedure Tdlg_LinkEnd_add.row_Property1EditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
var
 // iMode: Integer;
  oSectorIDList: TIDLIst;
  iLinkEndType_Mode_ID: integer;

  rec: TdmLinkEndTypeModeInfoRec;

begin
//  cx_SetButtonEditText(Sender as TcxButtonEdit, 'dsfsdf');
 // Exit;


 // (Sender as TcxButtonEdit).Text:='aaaaaaaa';
//  (Sender as TcxButtonEdit).PostEditValue;

//  cxVerticalGrid1.HideEdit;

////////
 // Exit;

  Assert(Sender is TcxButtonEdit);


 // ShowMessage(Sender.ClassName);    // cxButtonEdit

(*
  if (cxVerticalGrid1.FocusedRow = row_Mode_) or
     (cxVerticalGrid1.FocusedRow = row_LinkendType_)
  then
    Assert(Assigned(ILinkEndType));
*)

  //-------------------------------
  if cxVerticalGrid1.FocusedRow =row_Property then  // begin
  //-------------------------------
  begin
    if Dlg_SelectObject_cxButtonEdit (Sender as TcxButtonEdit, otProperty, FRec.Property_ID) then
      UpdateAntennaMaxHeight();
  end
  else

  //-------------------------------
  if cxVerticalGrid1.FocusedRow =row_Mode then begin
  //-------------------------------
    if dmAct_LinkEndType.Dlg_GetMode (FRec.LinkEndType_ID, FRec.LinkEndType_Mode_ID, rec) then
    begin
      FRec.LinkEndType_mode_ID := rec.ID;

      cx_SetButtonEditText(Sender, IntToStr(rec.Mode));
    end;
  end else

  //-------------------------------
  if cxVerticalGrid1.FocusedRow =row_LinkendType then
  //-------------------------------
    case AButtonIndex of
      0: begin
           if Dlg_SelectObject_cxButtonEdit(Sender as TcxButtonEdit, otLinkEndType, FRec.LinkEndType_ID) then
             if dmAct_LinkEndType.Dlg_GetMode (FRec.LinkEndType_ID, 0, rec) then  //iMode, iLinkEndType_Mode_ID,
             begin
               FRec.LinkEndType_mode_ID := rec.ID;

               row_Mode.Properties.Value:=IntToStr(rec.Mode);
             end;
         end;
      1: begin
           FRec.LinkEndType_ID:=0;
           FRec.LinkEndType_mode_ID:=0;
           row_LinkendType.Properties.Value:='';
         end;
    end
  else

  //-------------------------------
  if cxVerticalGrid1.FocusedRow =row_Antenna1_Type then
  //-------------------------------
    case AButtonIndex of
      0: Dlg_SelectObject_cxButtonEdit(Sender as TcxButtonEdit,
            otAntennaType, FRec.Antenna1.AntennaTypeID);
      1:  begin
            FRec.Antenna1.AntennaTypeID:=0;
            row_Antenna1_Type.Properties.Value:='';
          end;
    end
  else

  //-------------------------------
  if cxVerticalGrid1.FocusedRow =row_Cell_Layer then
  //-------------------------------
    case AButtonIndex of
      0: Dlg_SelectObject_cxButtonEdit(Sender as TcxButtonEdit, otCellLayer, FRec.PmpSector.CellLayer_ID);
      1:  begin
            FRec.PmpSector.CellLayer_ID:=0;
            row_Cell_Layer.Properties.Value:='';
          end;
    end
  else

  //-------------------------------
  if cxVerticalGrid1.FocusedRow =row_Calc_Model then
  //-------------------------------
    case AButtonIndex of
      0: Dlg_SelectObject_cxButtonEdit(Sender as TcxButtonEdit, otCalcModel, FRec.PmpSector.CalcModel_ID);
      1: begin
           FRec.PmpSector.CalcModel_ID:=0;
           row_Calc_Model.Properties.Value:='';
         end;
    end
  else

  //-------------------------------
  if cxVerticalGrid1.FocusedRow =row_PmpSite1 then begin
  //-------------------------------
    Select_PmpSector(row_PmpSite1);   

//    Dlg_SelectObject_cxButtonEdit(Sender as TcxButtonEdit, otPmpSite, FRec.Pmp.PmpSite_ID);
  end else

  //-------------------------------
  if cxVerticalGrid1.FocusedRow =row_PmpSector1 then begin
  //-------------------------------
    Select_PmpSector(row_PmpSector1)

(*    oSectorIDList:=TIDLIst.Create;

//    if Tdlg_expl_Object_Move_Enhanced.ExecDlg(otPmpSite, otPmpSector, oSectorIDList, true) then
    if dmAct_Explorer.Dlg_Object_Move_Enhanced(otPmpSite, otPmpSector, oSectorIDList, true) then
    begin
      Frec.Pmp.PmpSector_ID  :=oSectorIDList[0].ID;
      row_PmpSector.Properties.Value:=dmPmp_Sector.GetNameByID(Frec.Pmp.PmpSector_ID);
      Frec.Pmp.PmpSite_ID:=dmPmp_Sector.GetSiteID(Frec.Pmp.PmpSector_ID);
      row_PmpSite.Properties.Value  :=dmPmpSite.GetNameByID(Frec.Pmp.PmpSite_ID);
    end;


    oSectorIDList.Free;
*)
  end;// else

end;



procedure Tdlg_LinkEnd_add.row_Equipment_TypeEditPropertiesCloseUp(
  Sender: TObject);
begin
  DoOnEquipment_TypeChange();
end;

procedure Tdlg_LinkEnd_add.cxVerticalGrid1StylesGetContentStyle(
  Sender: TObject; AEditProp: TcxCustomEditorRowProperties;
  AFocused: Boolean; ARecordIndex: Integer; var AStyle: TcxStyle);

var
//  s: string;
  oRow: TcxEditorRow;
  s: string;
  b: boolean;
begin


  Exit;


 // AValueInfo.

 // s:=Sender.className;


  if not (AEditProp.Row is TcxEditorRow) then
    Exit;

  if (AEditProp.Row=row_Antenna1_Type) or
     (AEditProp.Row=row_Antenna1_Height) or
     (AEditProp.Row=row_Property) or
     (AEditProp.Row=row_Cell_Layer) or
     (AEditProp.Row=row_PmpSite1) 
  then begin
    AStyle := cxStyle1;
    Exit;
  end;


  b:=Eq(AsString(row_equipment_Type.Properties.Value), STR_TYPED);

  if b then
  begin
    if (AEditProp.Row=row_LinkEndType) then
      AStyle := cxStyle1;

  end
  //    cx_ColorRow(oRow, [],  ACanvas,  true, clSilver)
  else
  begin
    if (AEditProp.Row=row_Power_dBm) or
        (AEditProp.Row=row_Freq_GHz) or
        (AEditProp.Row=row_Sens_ber_3) or
        (AEditProp.Row=row_Sens_ber_6) or
        (AEditProp.Row=row_signature_height) or
        (AEditProp.Row=row_signature_width)
    then
      AStyle := cxStyle1;

  end;

(*    cx_ColorRow(oRow, [row_Power_dBm,
                       row_Freq_GHz,
                       row_Sens_ber_3,
                       row_Sens_ber_6,
                       row_signature_height,
                       row_signature_width],  ACanvas,  true, clSilver);
*)

  if (FObjectName = OBJ_PMP_TERMINAL) then
    if (row_AssociateWithPmpSite.Properties.Value=true)
  then
    if (AEditProp.Row=row_PmpSector1) then
      AStyle := cxStyle1;

  //  cx_ColorRow(oRow, [row_PmpSector],  ACanvas,  true, clSilver);



end;


//---------------------------------------------------------------------------
procedure Tdlg_LinkEnd_add.SetObjectName(aObjectName : string);
//---------------------------------------------------------------------------
begin
  row_Pmp_sector.Visible:=Eq(aObjectName, OBJ_PMP_SECTOR);


  row_AssociateWithPmpSite.Visible := Eq(aObjectName, OBJ_PMP_TERMINAL);


end;


end.



{

// ---------------------------------------------------------------
procedure Tdlg_LinkEnd_add.row_Template_siteEditPropertiesButtonClick( Sender: TObject; AButtonIndex: Integer);
// ---------------------------------------------------------------
begin
  if AButtonIndex=0 then
    if cxVerticalGrid1.FocusedRow=row_Template_link then
      Dlg_SelectObject_cxButtonEdit (Sender as TcxButtonEdit, otTemplate_Link, Frec.Template_link_id);

//    Dlg_Apply_template;

  if AButtonIndex=1 then
  begin
    Frec.Template_link_id:=0;
    row_Template_link.Properties.Value:='';

  // cx_

    row_Template_link.VerticalGrid.HideEdit;

//    cx_SetButtonEditText(row_Template_site, '');

  end;



end;

