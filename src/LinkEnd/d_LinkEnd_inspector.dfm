object dlg_LinkEnd_inspector: Tdlg_LinkEnd_inspector
  Left = 441
  Top = 219
  Width = 635
  Height = 538
  Caption = 'dlg_LinkEnd_inspector'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Scaled = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object pn_Buttons: TPanel
    Left = 0
    Top = 476
    Width = 627
    Height = 35
    Align = alBottom
    BevelOuter = bvNone
    Constraints.MaxHeight = 35
    Constraints.MinHeight = 35
    TabOrder = 0
    object Bevel1: TBevel
      Left = 0
      Top = 0
      Width = 627
      Height = 2
      Align = alTop
      Shape = bsTopLine
    end
    object Panel1: TPanel
      Left = 453
      Top = 2
      Width = 174
      Height = 33
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 0
      object btn_Ok: TButton
        Left = 13
        Top = 5
        Width = 75
        Height = 23
        Caption = 'Ok'
        Default = True
        ModalResult = 1
        TabOrder = 0
      end
      object btn_Cancel: TButton
        Left = 93
        Top = 5
        Width = 75
        Height = 23
        Cancel = True
        Caption = #1054#1090#1084#1077#1085#1072
        ModalResult = 2
        TabOrder = 1
      end
    end
  end
  object FormPlacement1: TFormPlacement
    IniFileName = 'Software\Onega\'
    Left = 320
    Top = 80
  end
end
