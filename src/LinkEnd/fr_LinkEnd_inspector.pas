unit fr_LinkEnd_inspector;

interface

uses
  u_classes,

  d_PmpTerminal_Plug,
  u_func_msg,

//  dm_User_Security,

  d_LinkEndType_Mode,
  d_LinkEnd_Get_Band,

  dm_LinkEnd_tools,

 // I_Act_Explorer, //
 dm_Act_Explorer,


  fr_DBInspector_Container,
  dm_Onega_DB_data,

  dm_Main,


  I_Shell,
  I_LinkEndType,

 // fr_Object_inspector,

  u_Inspector_common,

  u_const_db,
  u_link_const,

  dm_Library,

  dm_LinkEnd,
  dm_Link,
  dm_LinkEndType,
//  dm_LinkEnd_View,
  dm_Combiner,


  u_func,
  u_radio,
  u_db,
  u_dlg,

  u_types,

//  fr_DBInspector_Container_new,

  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, Variants,
  ActnList, rxPlacemnt, StdCtrls, ExtCtrls, Math, ToolWin, ComCtrls, Db, ADODB,
  cxStyles, ImgList, Menus, Grids, DBGrids, Mask, DBCtrls, cxVGrid, cxDBVGrid, cxControls,

  cxInplaceContainer;

type
  Tframe_LinkEnd_inspector = class(Tframe_DBInspector_Container)
    qry_Temp11: TADOQuery;
//    procedure cb_PassiveClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    FBitRate_Mbps: double;

    FObjName: string;
    FTableName: string;
    FIsPostEnabled: boolean;
    FObjID: integer;

//    FOldEquipType: integer;

    procedure ShowEquipmentType;
    procedure UpdateFreqSpacing;

    procedure UpdateLinkEndTypeInfo (aLinkEndTypeID: integer );
    procedure UpdateKNG ();
    procedure UpdateRestorePeriod ();
    procedure UpdatePassiveElement (aNewValue: Variant);
    procedure Dlg_GetBand;

    //1 ���������: �������, ��������� ������������
    procedure ChangeEquipmentType(aNewValue: Variant);

    procedure Clear_Band ();
    procedure Refresh_Freq_Color;

    procedure Select_PmpSector();


  protected

    procedure DoOnPost(Sender: TObject); override;
    procedure DoOnFieldChanged (Sender: TObject; aFieldName: string; aNewValue: Variant); override;

    procedure DoOnButtonFieldClick (Sender: TObject; aFieldName: string; aNewValue: Variant); override;

   
  public
    Params: record
      Next_LinkEnd_AutoSave: boolean;
    end;

    function Dlg_UpdateMode: Boolean;

    procedure View(aID: integer; aObjName: string);

  end;


//==================================================================
//==================================================================
implementation
{$R *.dfm}


const
  STR_ERROR_NEGATIVE_VALUE =
          '������������ ��������! '+ #13#10 +
          '%s �� ����� ����� ������������� ��������!';

  ALIAS_equipment_standard     = 'equipment_standard';
  ALIAS_equipment_non_standard = 'equipment_non_standard';

  DEF_EQUIPMENT_STANDARD     = 0;
  DEF_EQUIPMENT_NON_STANDARD = 1;

  ALIAS_ITU_R       ='ITU-R';
  ALIAS_GOST_53363  ='GOST_53363';
  ALIAS_NIIR_1998   ='NIIR_1998';


  FLD_XPIC_use_2_stvola_diff_pol = 'XPIC_use_2_stvola_diff_pol';

//-------------------------------------------------------------------
procedure Tframe_LinkEnd_inspector.FormCreate(Sender: TObject);
//-------------------------------------------------------------------
begin
  inherited;

  db_SetComponentADOConnection(Self, dmMain.ADOConnection);


  {$IFDEF de
  pn_test.Visible := True;
  {$ELSE}
  pn_test.Visible := False;
  {$ENDIF}


//  pn_test.Visible := False;
end;


//-------------------------------------------------------------------
procedure Tframe_LinkEnd_inspector.Refresh_Freq_Color;
//-------------------------------------------------------------------
var
  eRX_FREQ_MHz: Double;
  eTX_FREQ_MHz: Double;
Begin
  eTX_FREQ_MHz := GetFloatFieldValue (FLD_TX_FREQ_MHz);
  eRX_FREQ_MHz := GetFloatFieldValue (FLD_RX_FREQ_MHz);

  if Abs(eTX_FREQ_MHz-eRX_FREQ_MHz)<0.001 then
  begin
    SetColor_Yellow(FLD_TX_FREQ_MHz);
    SetColor_Yellow(FLD_RX_FREQ_MHz);
  end else
  begin
    SetColor_Blank(FLD_TX_FREQ_MHz);
    SetColor_Blank(FLD_RX_FREQ_MHz);
  end

end;

//-------------------------------------------------------------------
procedure Tframe_LinkEnd_inspector.View(aID: integer; aObjName: string);
//-------------------------------------------------------------------

{const
  ALIAS_PMP_TERMINAL = 'PMP_TERMINAL';
  ALIAS_PMP_SECTOR   = 'PMP_SECTOR';
  ALIAS_link_calc_params = 'link_calc_params';

}
var
  bLinkend: Boolean;
//  bIsRepeater: Boolean;
  iGeoRegion_ID: Integer;
  iLink_calc_method_id: Integer;

begin
  Post;

  PrepareViewForObject (aObjName);

  inherited View(aID);


  FBitRate_Mbps := GetFloatFieldValue (FLD_BitRate_Mbps);

  Refresh_Freq_Color();

//  SetColor_Yellow(FLD_TX_FREQ_MHz);
//  SetColor_Yellow(FLD_RX_FREQ_MHz);


//       SetFieldValue (FLD_BitRate_Mbps,        rec.BitRate_Mbps_);


//  if aObjName2<>'' then
 //   aObjName:=aObjName2;


  FObjName:=aObjName;
  FObjID:=ID;

  ShowEquipmentType ();

  UpdateFreqSpacing ();


 // CollapseRowByFieldName (FLD_SUBBAND);
  ExpandRowByFieldName(FLD_POWER_dBm, False);
  ExpandRowByFieldName(FLD_PASSIVE_ELEMENT_ID, False);

(*  CollapseRowByFieldName (FLD_POWER_dBm);
  CollapseRowByFieldName (FLD_PASSIVE_ELEMENT_ID);
*)

 /// CollapseRowByFieldName (FLD_BEELINE);

//  bIsRepeater := GetBooleanFieldValue(FLD_Is_Repeater);

(*  if bIsRepeater then
  begin
    SetFieldReadOnly(FLD_THRESHOLD_BER_3, false );
    SetFieldReadOnly(FLD_THRESHOLD_BER_6, false );
  end;
*)
 // cb_Passive.Checked:= bIsRepeater;

 // StatusBar1.SimpleText:=IIF(bIsRepeater, 'bIsRepeater', '');


  FIsPostEnabled:=false;


//  FOldEquipType:=GetIntFieldValue(FLD_EQUIPMENT_TYPE);


////////  SetMinMaxValues (FLD_freq_channel_count, 1,10);
///////////  SetMinMaxValues (FLD_power_dBm,  0,100);
//////////  SetMinMaxValues (FLD_power_loss, 0,30);

  //iGeoRegion_ID := dmLinkend.GetIntFieldValue(aID, FLD_GeoRegion_ID);


  {bLinkend:=Eq(aObjName, OBJ_LINKEND);
  SetRowVisibleByAlias(ALIAS_link_calc_params, bLinkend);

  if bLinkend then
  begin
    iLink_calc_method_id := GetIntFieldValue('Link_calc_method_id');

    SetRowVisibleByAlias(ALIAS_ITU_R,      iLink_calc_method_id = 0);
    SetRowVisibleByAlias(ALIAS_GOST_53363, iLink_calc_method_id = 1);
    SetRowVisibleByAlias(ALIAS_NIIR_1998,  iLink_calc_method_id = 2);

  end;


  SetRowVisibleByAlias(ALIAS_PMP_TERMINAL, Eq(aObjName, OBJ_PMP_TERMINAL));
  SetRowVisibleByAlias(ALIAS_PMP_SECTOR,   Eq(aObjName, OBJ_PMP_SECTOR));

}



//  link_calc_params



(*
//  iGeoRegion_ID := GetDataset(). GetIntFieldValue(FLD_GeoRegion_ID);
  iGeoRegion_ID := GetDataset().FieldByName(FLD_GeoRegion_ID).AsInteger;// GetIntFieldValue(FLD_GeoRegion_ID);

  SetReadOnly(dmUser_Security.ProjectIsReadOnly, 'GeoRegion:'+IntToStr(iGeoRegion_ID));
*)

 // Ffrm_DBInspector.
 // SetReadOnly(dmUser_Security.ProjectIsReadOnly);
 // uses.Enabled := True;

//  ShowMessage(IntToStr(iGeoRegion_ID));


end;


//-------------------------------------------------------------------
procedure Tframe_LinkEnd_inspector.UpdateFreqSpacing;
//-------------------------------------------------------------------
var
  iChannelSpacing: Integer;
  dFreqSpacing_MHz: double;
  eChannel_width_MHz: Double;
begin
  iChannelSpacing    :=GetIntFieldValue(FLD_CHANNEL_SPACING);
  eChannel_width_MHz :=GetFloatFieldValue(FLD_channel_width_MHz);
  dFreqSpacing_MHz   :=iChannelSpacing*eChannel_width_MHz;

  if (GetFloatFieldValue(FLD_FREQ_SPACING) <> dFreqSpacing_MHz) then
      SetFieldValue(FLD_FREQ_SPACING, dFreqSpacing_MHz);
end;


//-------------------------------------------------------------------
function Tframe_LinkEnd_inspector.Dlg_UpdateMode: Boolean;
//-------------------------------------------------------------------
var
  iID: Integer;
  rec: TdmLinkEndTypeModeInfoRec;

  iLinkEndType_Mode_ID: integer;
  iLinkEndTypeID: Integer;

begin
  iLinkEndType_Mode_ID:=GetIntFieldValue (FLD_LinkEndType_Mode_ID);
  iLinkEndTypeID      :=GetIntFieldValue (FLD_LINKENDTYPE_ID);

 // Assert(iLinkEndTypeID>0);

  iID:= Tdlg_LinkEndType_Mode.Dlg_GetMode1 (iLinkEndTypeID, iLinkEndType_Mode_ID, rec);

  if iID>0 then
  begin
     SetFieldValue (FLD_MODE,                rec.Mode);
     SetFieldValue (FLD_LinkEndType_Mode_ID, rec.ID);

     SetFieldValue (FLD_BitRate_Mbps,        rec.BitRate_Mbps_);
     SetFieldValue (FLD_MODULATION_TYPE,     rec.MODULATION_TYPE);
     SetFieldValue (FLD_SIGNATURE_HEIGHT,    rec.SIGNATURE_HEIGHT);
     SetFieldValue (FLD_SIGNATURE_WIDTH,     rec.SIGNATURE_WIDTH);
     SetFieldValue (FLD_THRESHOLD_BER_3,     rec.THRESHOLD_BER_3);
     SetFieldValue (FLD_THRESHOLD_BER_6,     rec.THRESHOLD_BER_6);

//     SetFieldValue (FLD_SUBBAND, null);
//     SetFieldValue (FLD_CHAnnel_number,     null);


     SetFieldValue (FLD_channel_width_MHz,   rec.bandwidth_MHz);
                                       

     SetFieldValue (FLD_POWER_dBm,           rec.Power_max_dBm);
     SetFieldValue (FLD_POWER_MAX,           rec.Power_max_dBm);
     SetFieldValue (FLD_POWER_LOSS,          0);

     SetFieldValue (FLD_MODULATION_COUNT,      rec.MODULATION_level_COUNT);
     SetFieldValue (FLD_GOST_53363_modulation, rec.GOST_Modulation);

     HideEdit;

     FIsPostEnabled := True;

     UpdateFreqSpacing;

     Result := True;

  end else
     Result := False;
end;


//-------------------------------------------------------------------
procedure Tframe_LinkEnd_inspector.ShowEquipmentType;
//-------------------------------------------------------------------
var iValue: integer;
  b: boolean;
  bEQUIPMENT_STANDARD: Boolean;
begin
  iValue:=GetIntFieldValue(FLD_EQUIPMENT_TYPE);

  b:=(iValue=DEF_EQUIPMENT_STANDARD);
(*
  SetRowVisibleByFieldName (ALIAS_equipment_standard, b);
  SetRowVisibleByFieldName (ALIAS_equipment_non_standard, not b);


  if iValue=DEF_EQUIPMENT_STANDARD then
  begin
    ExpandRowByFieldName   (ALIAS_equipment_standard    );
    CollapseRowByFieldName (ALIAS_equipment_non_standard);
  end else begin
    CollapseRowByFieldName (ALIAS_equipment_standard    );
    ExpandRowByFieldName   (ALIAS_equipment_non_standard);
  end;
*)


  SetRowVisibleByAlias (ALIAS_equipment_standard, b);
  SetRowVisibleByAlias (ALIAS_equipment_non_standard, not b);


  bEQUIPMENT_STANDARD:=iValue=DEF_EQUIPMENT_STANDARD;

  ExpandRowByAlias (ALIAS_equipment_standard, bEQUIPMENT_STANDARD);
  ExpandRowByAlias (ALIAS_equipment_non_standard, not bEQUIPMENT_STANDARD);


{  if iValue=DEF_EQUIPMENT_STANDARD then
  begin
    ExpandRowByAlias (ALIAS_equipment_standard, True);
    ExpandRowByAlias (ALIAS_equipment_non_standard, False);
  end else begin
    ExpandRowByAlias (ALIAS_equipment_standard, False);
    ExpandRowByAlias (ALIAS_equipment_non_standard, True);
  end;
}


end;


//-------------------------------------------------------------------
procedure Tframe_LinkEnd_inspector.UpdateLinkEndTypeInfo (aLinkEndTypeID: integer );
//-------------------------------------------------------------------
var
  rec: TdmLinkEndTypeInfoRec;
  iValue: integer;
  sValue: string;
  eFreq_MHZ: Double;
begin
  if //(aLinkEndTypeID>0) and
      dmLinkEndType.GetInfoRec (aLinkEndTypeID, rec) then
  begin
    SetFieldValue (FLD_POWER_dBm,  rec.Power_Max);
    SetFieldValue (FLD_POWER_MAX,  rec.Power_Max);
    SetFieldValue (FLD_POWER_LOSS, 0);
    SetFieldValue (FLD_KNG,        0);

    if rec.failure_period<0 then
      rec.failure_period:=0;

    SetFieldValue (FLD_FAILURE_PERIOD,  rec.failure_period);
    SetFieldValue (FLD_RESTORE_PERIOD,  0);

    SetFieldValue (FLD_BAND,     rec.BAND);
//    SetFieldValue (FLD_RANGE,     rec.RANGE);

//    if GetIntFieldValue(FLD_CHANNEL_NUMBER)) = 0 then
//    begin

    SetFieldValue (FLD_CHANNEL_NUMBER, null);

 //   SetFieldValue (FLD_RX_FREQ,   0);
  ////////////////  SetFieldValue (FLD_TX_FREQ,   rec.RANGE*1000);

//    eFreq_MHZ:=dmLibrary.GetBandAveFreq_MHZ(rec.BAND);
    eFreq_MHZ:=rec.Freq_Ave_MHz;

    SetFieldValue (FLD_RX_FREQ_MHz, eFreq_MHZ);
    SetFieldValue (FLD_TX_FREQ_MHz, eFreq_MHZ);

    SetFieldValue (FLD_LINKENDTYPE_BAND_ID, null);


    ///
//    SetFieldValue (FLD_MODULATION_COUNT,      rec.MODULATION_level_COUNT);
 //   SetFieldValue (FLD_GOST_53363_modulation, rec.GOST_Modulation);


  end
;

//  UpdateBand;
end;


//-------------------------------------------------------------------
procedure Tframe_LinkEnd_inspector.UpdateKNG ();
//-------------------------------------------------------------------
var
  iRedundancy: integer;
  dRestorePeriod, dFailurePeriod, dKNG: double;
begin
  dRestorePeriod:= GetFloatFieldValue (FLD_RESTORE_PERIOD);
  dFailurePeriod:= GetFloatFieldValue (FLD_FAILURE_PERIOD);
  iRedundancy   := GetIntFieldValue(FLD_REDUNDANCY);

  if dRestorePeriod<0 then
  begin
    ErrorDlg(Format (STR_ERROR_NEGATIVE_VALUE, ['����� ��������������']));
    dRestorePeriod:= 0;
    SetFieldValue (FLD_RESTORE_PERIOD, dRestorePeriod);
  end;

  if (dRestorePeriod=0) and (dFailurePeriod=0) then
    SetFieldValue (FLD_KNG, 0)
  else begin
    dKNG:= TruncFloat(dRestorePeriod/(dRestorePeriod + dFailurePeriod)*100, 5);
    if iRedundancy = 1 then
      dKNG:= TruncFloat(dKNG*dKNG, 5);

    SetFieldValue (FLD_KNG, dKNG);
  end;

end;

//-------------------------------------------------------------------
procedure Tframe_LinkEnd_inspector.UpdateRestorePeriod ();
//-------------------------------------------------------------------
var
  iRedundancy: integer;
  dKNG, dFailurePeriod, dRestorePeriod: double;
begin
  dKNG          := GetFloatFieldValue (FLD_KNG);
  dFailurePeriod:= GetFloatFieldValue (FLD_FAILURE_PERIOD);
  iRedundancy   := GetIntFieldValue   (FLD_REDUNDANCY);

  if dKNG<0 then
  begin
    ErrorDlg(Format (STR_ERROR_NEGATIVE_VALUE, ['Kng']));
    dKNG:=0;
      SetFieldValue (FLD_KNG, dKNG)
  end;

  if dKNG>=100 then
  begin
    dKNG:=100;
    SetFieldValue (FLD_KNG, dKNG);
    SetFieldValue (FLD_RESTORE_PERIOD, 100000000);

    Exit;
  end;

  if (dKNG=0) and (dFailurePeriod=0) then
      SetFieldValue (FLD_RESTORE_PERIOD, 0)

  else begin
    if iRedundancy = 1 then
      dKNG:= Power(dKNG, 0.5);

    dRestorePeriod:= TruncFloat(dFailurePeriod*dKNG/(100-dKNG), 5);
    SetFieldValue (FLD_RESTORE_PERIOD, dRestorePeriod);
  end;
end;

//-------------------------------------------------------------------
procedure Tframe_LinkEnd_inspector.UpdatePassiveElement (aNewValue: Variant);
//-------------------------------------------------------------------
var
  dPassiveElementLoss: double;
//  dPassiveElementLoss, dPassiveElementLoss1, dLoss: double;
begin
//  if AsInteger(aNewValue) > 0 then
//  begin
  dPassiveElementLoss:= dmCombiner.GetLoss(aNewValue);

  SetFieldValue (FLD_PASSIVE_ELEMENT_LOSS, dPassiveElementLoss);

end;


//-------------------------------------------------------------------
procedure Tframe_LinkEnd_inspector.Clear_Band ();
//-------------------------------------------------------------------
begin
  SetFieldValue(FLD_LINKENDTYPE_BAND_ID, null);
//  SetFieldValue(FLD_LINKENDTYPE_BAND_ID+'_str', null);
  SetFieldValue(FLD_SUBBAND, null);

//  SetFieldValue (FLD_CHANNEL_TYPE, null);

  SetFieldValue (FLD_CHANNEL_NUMBER, null);


end;




//-------------------------------------------------------------------  
procedure Tframe_LinkEnd_inspector.ChangeEquipmentType(aNewValue: Variant);
//-------------------------------------------------------------------
var
  i, iLinkEndType_ID: integer;
  sName: Widestring;
 // iEquipType: integer;
begin
  //iEquipType:= GetIntFieldValue(FLD_EQUIPMENT_TYPE);



(*  if FOldEquipType = aNewValue then
  begin
    FIsPostEnabled:= false;
    exit;
  end;
*)


  case aNewValue of
    //---------------------------------------------------
    DEF_EQUIPMENT_STANDARD:
    //---------------------------------------------------
    begin
       iLinkEndType_ID := GetIntFieldValue(FLD_LINKENDTYPE_ID);

     //  iLinkEndType_ID:=0;

       if dmAct_Explorer.Dlg_Select_Object (otLinkEndType, iLinkEndType_ID, sName) then
       begin
         SetFieldValue(FLD_EQUIPMENT_TYPE, DEF_EQUIPMENT_STANDARD);
      //  ShowEquipmentType();

         SetFieldValue(FLD_LINKENDTYPE_ID, iLinkEndType_ID);
         SetFieldValue(FLD_LINKENDTYPE_ID+'_str', sName, False);

         UpdateLinkEndTypeInfo (iLinkEndType_ID);

         Dlg_UpdateMode();


         Clear_Band();

//         SetFieldValue(FLD_LINKENDTYPE_BAND_ID, null);
  //       SetFieldValue(FLD_LINKENDTYPE_BAND_ID+'_str', null);


   //      if Dlg_UpdateMode() then
     //      UpdateLinkEndTypeInfo (iLinkEndType_ID);

      end else
      begin
        SetFieldValue(FLD_EQUIPMENT_TYPE, DEF_EQUIPMENT_NON_STANDARD);
//        SetFieldValue(FLD_EQUIPMENT_TYPE+'_str', '���������', False)
      end;

      FIsPostEnabled:=true;
    end;

    //---------------------------------------------------
    DEF_EQUIPMENT_NON_STANDARD:
    //---------------------------------------------------
    begin
       if ConfirmDlg('�� ������������� ������ ������� ������������ �� ���������?'+CRLF+
                     ' (������� ��� ������������� �������� ����� ����������!)') then
       begin
        SetFieldValue(FLD_EQUIPMENT_TYPE, DEF_EQUIPMENT_NON_STANDARD);
      //  SetFieldValue(FLD_EQUIPMENT_TYPE+'_str', '���������', False);

        SetFieldValue(FLD_LINKENDTYPE_ID, null); //0);
      //  ShowEquipmentType();
      end else
      begin
        SetFieldValue(FLD_EQUIPMENT_TYPE, DEF_EQUIPMENT_STANDARD);
//       / SetFieldValue(FLD_EQUIPMENT_TYPE+'_str', '�������', False)

       //  SetFieldValue(FLD_LINKENDTYPE_ID, iLinkEndType_ID);
      //   SetFieldValue(FLD_LINKENDTYPE_ID+'_str', sName);   

      end;

      FIsPostEnabled:=true;
    end;
  end;

//  FOldEquipType:= GetIntFieldValue(FLD_EQUIPMENT_TYPE);

  ShowEquipmentType();

/////  CollapseRowByFieldName (FLD_SUBBAND);

end;


//-------------------------------------------------------------------
procedure Tframe_LinkEnd_inspector.DoOnPost(Sender: TObject);
//-------------------------------------------------------------------
//
//    // -------------------------------------------------------------------
//    procedure DoAddPMP;
//    // -------------------------------------------------------------------
//    begin
//
//  (*      db_OpenTable(qry_Temp1, VIEW_PMP_SECTOR_ANTENNAS,
//                    [db_Par(FLD_PMP_SECTOR_ID, FObjID)  ]);
//
//        while not qry_Temp1.EOF do
//        begin
//      /////    dmMapEngine1.ReCreateObject (otPmpSectorAnt, qry_Temp[FLD_ID]);
//          qry_Temp.Next;
//        end;
//*)
//    end;    //


begin
 // if (FIsPostEnabled) and (FObjName = OBJ_PMP_SECTOR) then
  //  DoAddPMP();

  if FObjName <> OBJ_LINKEND then
    Exit;

  if (FIsPostEnabled) and Eq(FObjName, OBJ_LINKEND) then
  begin
    if (Params.Next_LinkEnd_AutoSave) or
       (ConfirmDlg('����������� ��������� ������������ ��� ��������� ���?'))
    then
      dmOnega_DB_data.LinkEnd_UpdateNext(ID);
  end;

  //  DoUpdateAnotherLinkEnd();

end;

//-------------------------------------------------------------------
procedure Tframe_LinkEnd_inspector.DoOnFieldChanged (Sender: TObject; aFieldName: string;
                                                aNewValue: Variant);
//-------------------------------------------------------------------
var
  bXPIC: Boolean;
  dPowerLoss,dPowerMax,dPower,dAllowedPowerLoss: double;

  iValue: integer;
  dPassiveElementLoss, dLoss: double;
  eBitRate_Mbps: Double;

  sProtectionType: string;
  iLinkEndTypeID: integer;
  iLinkEndType_Mode_ID: Integer;
  k,iID: integer;
begin
  if aFieldName='' then
    Exit;

  FIsPostEnabled:=True;


  // -------------------------------------------------------------------
  if Eq(aFieldName, FLD_kratnost_By_Space)  //or
    // Eq(aFieldName, FLD_kratnost_By_Freq)
  // -------------------------------------------------------------------

  then begin
    k:=GetIntFieldValue(FLD_kratnost_By_Space);

    bXPIC:=k>1;

    SetFieldValue (FLD_XPIC_use_2_stvola_diff_pol, IIF(bXPIC, 1, 0 ));

    iLinkEndType_Mode_ID:=GetIntFieldValue (FLD_LinkEndType_Mode_ID);

    if iLinkEndType_Mode_ID>0 then
    begin
      dmOnega_DB_data.OpenQuery(dmOnega_DB_data.ADOQuery1,
                  Format('select BitRate_Mbps from LinkEndType_Mode where id=%d', [iLinkEndType_Mode_ID]));

  //    eBitRate_Mbps := GetFloatFieldValue (FLD_BitRate_Mbps);
      eBitRate_Mbps := dmOnega_DB_data.ADOQuery1.fieldByName(FLD_BitRate_Mbps).AsFloat;

      SetFieldValue (FLD_BitRate_Mbps, IIF(bXPIC, 2*eBitRate_Mbps, eBitRate_Mbps ));

    end;



//    SetFieldValue (FLD_XPIC_use_2_stvola_diff_pol, IIF((k=0) or (k=1), 0, 1 ));


//    SetFieldValue (FLD_XPIC_use_2_stvola_diff_pol, IIF((k=0) or (k=1), 0, 1 ));

//  FBitRate_Mbps := GetFloatFieldValue (FLD_BitRate_Mbps);


//    k:=GetIntFieldValue(FLD_kratnost_By_Space);



    //���������� ��������������
  //  sProtectionType:='';

   (* if GetIntFieldValue(FLD_redundancy) = 1 then
    begin
      //��������� ���������� �� ������������
      if (GetIntFieldValue(FLD_kratnost_By_Space)=0 ) and
         (GetIntFieldValue(FLD_kratnost_By_Freq)=0 )
      then sProtectionType:='HSBY' else

      if (GetIntFieldValue(FLD_kratnost_By_Space)=0 ) and
         (GetIntFieldValue(FLD_kratnost_By_Freq)>0 )
      then sProtectionType:='FD' else

      if (GetIntFieldValue(FLD_kratnost_By_Space)=1 )
//         ( FieldBYName(kratnostByFreq).AsInteger>0 )
      then sProtectionType:='HSBY SD' else

      if (GetIntFieldValue(FLD_kratnost_By_Space)=1 ) and
         (GetIntFieldValue(FLD_kratnost_By_Freq)>0 )
      then sProtectionType:='FD SD';
    end;*)

////////    SetFieldValue (FLD_PROTECTION_TYPE, sProtectionType);
  end else



  //------------------------------
  if Eq(aFieldName, FLD_CALC_RADIUS_KM) then
  begin
    FIsPostEnabled:=True;
  end else

  //------------------------------
  if Eq(aFieldName, FLD_EQUIPMENT_TYPE) then
  begin
   // iValue:= GetIntFieldValue(FLD_EQUIPMENT_TYPE);

    ChangeEquipmentType(aNewValue);
  end else

  //------------------------------
  if Eq(aFieldName, FLD_CHANNEL_SPACING) then
    UpdateFreqSpacing() else

  //------------------------------
  if Eq(aFieldName, FLD_PASSIVE_ELEMENT_ID) then
  begin
    iID :=AsInteger(aNewValue);
    UpdatePassiveElement(iID);
  end else


  //------------------------------
  if Eq(aFieldName, FLD_REDUNDANCY) then
    UpdateKNG ()
  else

  //------------------------------
  if (Eq(aFieldName, FLD_KNG)) or
     (Eq(aFieldName, FLD_REDUNDANCY)) then
    UpdateRestorePeriod ()
  else

  //------------------------------
  if Eq(aFieldName, FLD_RESTORE_PERIOD) then
  begin
    if aNewValue <> '' then
      UpdateKNG ()
    else
      SetFieldValue (FLD_KNG, NULL);
  end
     else

  //------------------------------
  if Eq(aFieldName, FLD_POWER_LOSS) then
  //------------------------------
  begin
      if aNewValue = '' then
        aNewValue := 0;

{      if (aNewValue < 0) then
    begin
        SetFieldValue (FLD_POWER_LOSS, 0);;
      exit;
    end;  }

    iLinkEndTypeID := GetIntFieldValue (FLD_LINKENDTYPE_ID);
    dAllowedPowerLoss:=
      gl_DB.GetIntFieldValueByID(TBL_LINKENDTYPE, FLD_POWER_LOSS, iLinkEndTypeID);


    if (aNewValue > dAllowedPowerLoss) then
    begin
      SetFieldValue (FLD_POWER_LOSS, 0);;
      exit;
    end;

    dPowerMax:=GetFloatFieldValue (FLD_POWER_MAX);
    dPowerLoss:=aNewValue;

    SetFieldValue (FLD_POWER_dBm, dPowerMax - dPowerLoss);

  end else

  //------------------------------
  if Eq(aFieldName, FLD_POWER_dBm) then
  //------------------------------
  begin
    if aNewValue = '' then
      aNewValue := 0;

    aNewValue := GetFloatFieldValue (FLD_POWER_MAX) - aNewValue;

    SetFieldValue (FLD_POWER_LOSS, aNewValue);
  end else

  //------------------------------
  if Eq(aFieldName, FLD_LINKENDTYPE_ID) then
  //------------------------------
  begin
    if aNewValue = 0 then
    begin
      SetFieldValue(FLD_EQUIPMENT_TYPE, DEF_EQUIPMENT_NON_STANDARD);

//      SetFieldValue (FLD_LINKENDTYPE_BAND_ID, null);
    end
    else
      SetFieldValue(FLD_EQUIPMENT_TYPE, DEF_EQUIPMENT_STANDARD);

    if aNewValue>0 then
    begin
      UpdateLinkEndTypeInfo(aNewValue);
      Dlg_UpdateMode();
    end;


    Clear_Band();

(*
    ShowEquipmentType(); //equipment_standard, non equipment_standard

    //clear fields
    UpdateLinkEndTypeInfo(0);
    //set new LinkEndType
    UpdateLinkEndTypeInfo(aNewValue);
    CollapseRowByFieldName (FLD_SUBBAND);

    if aNewValue>0 then
      Dlg_UpdateMode();
      *)

  end;

end;

//-------------------------------------------------------------------
procedure Tframe_LinkEnd_inspector.DoOnButtonFieldClick (Sender: TObject; aFieldName: string;
                                                aNewValue: Variant);
//-------------------------------------------------------------------
begin

  if Eq(aFieldName, FLD_MODE) then
    Dlg_UpdateMode
  else


  //------------------------------
  if Eq(aFieldName, FLD_CHANNEL_NUMBER) or
     Eq(aFieldName, FLD_TX_FREQ_MHz) or
     Eq(aFieldName, FLD_RX_FREQ_MHz) or
     Eq(aFieldName, FLD_CHANNEL_TYPE) or
     Eq(aFieldName, FLD_SUBBAND)
  then begin
    Dlg_GetBand;
  end;


  if Eq(aFieldName, FLD_CONNECTED_PMP_SITE_ID) or
     Eq(aFieldName, FLD_CONNECTED_PMP_SECTOR_ID)
  then
    Select_PmpSector;
//    Dlg_UpdateMode




end;

//-------------------------------------------------------------------
procedure Tframe_LinkEnd_inspector.Dlg_GetBand;
//-------------------------------------------------------------------
var
  rec_band: TLinkEnd_Band_rec;

  eFreqSpacing_MHz: Double;
  iChannelSpacing: Double;

begin
  FillChar(rec_band, SizeOf(rec_band), 0);

  rec_band.LinkEndType_BAND_ID := GetIntFieldValue (FLD_LINKENDTYPE_BAND_ID);

  rec_band.LinkEndType_ID := GetIntFieldValue (FLD_LINKENDTYPE_ID);
  rec_band.Channel_Type   := GetStrFieldValue (FLD_CHANNEL_TYPE);
  rec_band.Channel_number := GetIntFieldValue (FLD_CHANNEL_NUMBER);

  rec_band.TxFreq_MHz  := GetIntFieldValue (FLD_TX_FREQ_MHz);
  rec_band.RxFreq_MHz  := GetIntFieldValue (FLD_RX_FREQ_MHz);

  rec_band.Channel_width_MHz := GetFloatFieldValue (FLD_channel_width_MHz);
  rec_band.SubBand           := GetStrFieldValue (FLD_SubBand);

  //            SetFieldValue(FLD_FREQ_SPACING, eFreqSpacing_MHz);


//            SetFieldValue(FLD_TX_FREQ_MHz,        rec_band.TxFreq_MHz );
 //           SetFieldValue(FLD_RX_FREQ_MHz,        rec_band.RxFreq_MHz);



  if Tdlg_LinkEnd_Get_Band.ExecDlg(rec_band) then
  begin
    case rec_band.InputType of
      itAuto:
          begin
            SetFieldValue(FLD_LINKENDTYPE_BAND_ID, rec_band.LinkEndType_BAND_ID);

            SetFieldValue(FLD_CHANNEL_TYPE,       rec_band.Channel_Type);
            SetFieldValue(FLD_CHANNEL_NUMBER,     rec_band.Channel_number);
            SetFieldValue(FLD_TX_FREQ_MHz,        rec_band.TxFreq_MHz );
            SetFieldValue(FLD_RX_FREQ_MHz,        rec_band.RxFreq_MHz);
            SetFieldValue(FLD_SUBBAND,            rec_band.LinkEndType_BAND_Name);

            iChannelSpacing:=GetIntFieldValue(FLD_CHANNEL_SPACING);

            eFreqSpacing_MHz:=iChannelSpacing * rec_band.channel_width_MHz;

          //  if (GetFloatFieldValue(FLD_FREQ_SPACING) <> dFreqSpacing) then
            SetFieldValue(FLD_FREQ_SPACING, eFreqSpacing_MHz);

            //������ ������ �������
//            SetFieldValue(FLD_channel_width_MHz,  999);
            SetFieldValue(FLD_channel_width_MHz,  rec_band.Channel_width_MHz);

          end;

        itHand:
          begin

            SetFieldValue(FLD_CHANNEL_TYPE,       rec_band.Channel_Type);

            SetFieldValue(FLD_TX_FREQ_MHz,        rec_band.TxFreq_MHz );
            SetFieldValue(FLD_RX_FREQ_MHz,        rec_band.RxFreq_MHz);

            SetFieldValue(FLD_SubBand,          rec_band.SubBand);

            SetFieldValue(FLD_channel_width_MHz,  rec_band.Channel_width_MHz);

            SetFieldValue(FLD_CHANNEL_NUMBER,  null);

//            SetFieldValue(FLD_CHANNEL_NUMBER,  rec_band.Channel_number);

          //    rec_band.Channel_number := GetIntFieldValue (FLD_CHANNEL_NUMBER);

//            SetFieldValue(FLD_FREQ_SPACING, eFreqSpacing_MHz);


          end;

    end;

    FIsPostEnabled := True;

    Refresh_Freq_Color;
//    Exit;
  end;

end;



// ---------------------------------------------------------------
procedure Tframe_LinkEnd_inspector.Select_PmpSector();
// ---------------------------------------------------------------
var
  oSectorIDList: TIDLIst;

  rec: Tdlg_PmpTerminal_Plug_rec;

begin

{
    SetFieldValue(FLD_CHANNEL_TYPE,       rec_band.Channel_Type);
    SetFieldValue(FLD_CHANNEL_NUMBER,     rec_band.Channel_number);
    SetFieldValue(FLD_TX_FREQ_MHz,        rec_band.TxFreq_MHz );
}

//  oSectorIDList:=TIDLIst.Create;

//  rec.PmpSite_ID  :=GetIntFieldValue (FLD_Pmp_Site_ID);
  rec.PmpSector_ID:=GetIntFieldValue (FLD_Pmp_Sector_ID);


//  rec_band.LinkEndType_BAND_ID := GetIntFieldValue (FLD_LINKENDTYPE_BAND_ID);


  if Tdlg_PmpTerminal_Plug.ExecDlg(0, dmMain.ProjectID, rec) then
  begin
   // Frec.PmpTerminal.PmpSector_ID:=rec.PmpSector_ID;

 //   Frec.Pmp.PmpSite_ID  :=rec.PmpSite_ID;

 //   cx_SetButtonEditText (row_PmpSector, rec.PmpSector_name);
  //  cx_SetButtonEditText (row_PmpSite, rec.PmpSite_name);

//    row_PmpSector.e

   // cxVerticalGrid1.


//    row_PmpSector.Properties.Value:=rec.PmpSector_name;
//    row_PmpSite.Properties.Value  :=rec.PmpSite_name;

//    row_AssociateWithPmpSite.Properties.Value  := True;

//    aSender.PostEditValue;

  //  cxVerticalGrid1.CancelEdit;

//    aSender.Post

//    aEditorRow.PostEditValue;

//    if  then

  end;



end;




end.

