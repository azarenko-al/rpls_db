unit fr_LinkEnd_antennas;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
  cxTLData, cxPropertiesStore, cxGridLevel, cxClasses, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid, ADODB,
  cxVGrid, cxDBVGrid, DB, Menus, ActnList, cxControls,

  ComCtrls, cxInplaceContainer, ExtCtrls, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, dxSkinsDefaultPainters,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage,

   ToolWin,

  u_cx,

  dm_Main,

 // dm_User_Security,


 // I_Act_Explorer, //dm_Act_Explorer,
 // I_Act_LinkEnd,  //dm_Act_LinkEnd,
 // I_Act_Antenna, //dm_Act_Antenna,

  u_storage,

  dm_Onega_DB_data,

 // f_Custom,
  //I_core,

  u_vars,

  I_Shell,

 // I_Antenna,
//  I_MapEngine,
//  dm_MapEngine,




  u_Func_arrays,
  u_func,
  u_Geo,
  u_dlg,
  u_db,
 // u_cx,
  u_Log,

  u_func_msg,
  u_const_msg,

  u_const,
  u_const_str,
  u_const_db,

  u_types,

//  dm_Antenna,
//  dm_AntType,
//  dm_LinkEnd,
//  dm_LinkEnd_View,


  cxEdit, cxDBData, cxButtonEdit, cxCurrencyEdit

  ;

type
  Tframe_LinkEnd_antennas = class(TForm)
    ActionList1: TActionList;
    act_Antenna_Edit: TAction;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    act_Antenna_New: TAction;
    N2: TMenuItem;
    actAntennaNew1: TMenuItem;
    ds_qry_Antennas: TDataSource;
    act_Antenna_Del: TAction;
    N3: TMenuItem;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    col___name: TcxGridDBColumn;
    col___AntType_Name: TcxGridDBColumn;
    col___azimuth: TcxGridDBColumn;
    col___height: TcxGridDBColumn;
    col___Diameter: TcxGridDBColumn;
    col___antenna_loss: TcxGridDBColumn;
    col___Gain: TcxGridDBColumn;
    col___Polarization_STR: TcxGridDBColumn;
    col___vert_width: TcxGridDBColumn;
    col___horz_width: TcxGridDBColumn;
    col___Pos: TcxGridDBColumn;
    cxGrid1Level1: TcxGridLevel;
    ADOStoredProc1: TADOStoredProc;
    col_ID: TcxGridDBColumn;
    ADOConnection1: TADOConnection;
    col_Mast_index: TcxGridDBColumn;
    cxStyleRepository2: TcxStyleRepository;
    cxStyle_Edit: TcxStyle;
    cxStyle_ReadOnly: TcxStyle;
    col_polarization_ratio: TcxGridDBColumn;
                                     

    procedure FormCreate(Sender: TObject);
    procedure act_Antenna_EditExecute(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure ActionList1Update(Action: TBasicAction; var Handled: Boolean);
    procedure ADOStoredProc1AfterPost(DataSet: TDataSet);
    procedure col___AntType_NamePropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure cxGrid1Exit(Sender: TObject);
    procedure cxDBVerticalGrid1antennaType_nameEditPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
  private
    FID: integer;
    FObjName: string;

    FDataSet: TDataSet;

    FReadOnly : Boolean;

    procedure Dlg_Select_AntType;


//    procedure GetMessage(aMsg: TEventType; aParams: TEventParamList; var aHandled: Boolean);

  public
    procedure SetReadOnly(aValue: boolean);
//    procedure SetReadOnly(Value: Boolean);
// TODO: OpenAntennas
//  procedure OpenAntennas;
    procedure View(aID: integer = 0; aObjName: string = '');
  end;

//====================================================================
//====================================================================
implementation
 {$R *.dfm}

uses dm_act_Antenna,
     dm_Act_Explorer;




//
//  FReadOnly := dmUser_Security.Project_is_ReadOnly;
//
//
//  ADOStoredProc1.LockType := IIF(FReadOnly, ltReadOnly, ltBatchOptimistic);
//  SetFormActionsEnabled(Self, not FReadOnly);
//
//
//  cxGrid1DBTableView1.OptionsData.Editing := not FReadOnly;
//



//--------------------------------------------------------------------
procedure Tframe_LinkEnd_antennas.FormCreate(Sender: TObject);
//--------------------------------------------------------------------
begin
  inherited;

  Assert(not ADOConnection1.Connected);


//  FRegPath :=FRegPath + '1';


  //Assert(Assigned(IAntenna));
 // Assert(Assigned(ILinkEnd));
//  Assert(Assigned(IShell));

/////////////  FDataSet:=mem_Data1;
  FDataSet:=ADOStoredProc1;//  qry_Antennas;

//  Assert(Assigned(IMapEngine));

 ///////////
 ////////// cxDBTreeList1.Align:=alClient;
//  FUpdated:=false;

 // act_Setup_Dlg.Caption:= STR_SETUP_TABLE;
  act_Antenna_Edit.Caption:='��������';
  act_Antenna_New.Caption:='��������';

  //GSPages.Align:=alClient;
  cxGrid1.Align:=alClient;

(*
  col_ant_Azimuth.Caption   :=STR_AZIMUTH;
  col_ant_Diameter.Caption  :=STR_DIAMETER;
  col_ant_Gain.Caption      :=STR_GAIN; // '�� [dB]';
  col_ant_Height.Caption    :=STR_HEIGHT;
  col_AntType_Name.Caption  :=STR_TYPE;
  col_vert_width.Caption    :='������ ��� (V)';
  col_horz_width.Caption    :='������ ��� (H)';
  col_Loss.Caption          :=STR_LOSS_dB;
  col_Name.Caption          :=STR_NAME;
  col_Polar.Caption         :=STR_Polarization;
  col_Pos.Caption           :=STR_COORDS;
  col_LOCATION_TYPE.Caption :=STR_ANTENNA_Location_type;
*)


  // -------------------------------------------------------------------
//  cxGrid1DBTableView1.RestoreFromRegistry(FRegPath + cxGrid1DBTableView1.Name);

  g_Storage.RestoreFromRegistry(cxGrid1DBTableView1, className);

{
  col__Azimuth.Caption.Text      :=STR_AZIMUTH;
  col__Diameter.Caption.Text     :=STR_DIAMETER;
  col__Gain.Caption.Text         :=STR_GAIN; // '�� [dB]';
  col__Height.Caption.Text       :=STR_HEIGHT;
  col__AntType_Name.Caption.Text :=STR_TYPE;
  col__horz_width.Caption.Text   :='������ ��� (H)';
  col__antenna_loss.Caption.Text :=STR_LOSS_dB;
  col__Name.Caption.Text         :=STR_NAME;
  col__Polarization.Caption.Text :=STR_Polarization;
  col__Pos.Caption.Text          :=STR_COORDS;
  col__vert_width.Caption.Text   :='������ ��� (V)';
  col__LOCATION_TYPE.Caption.Text:=STR_ANTENNA_Location_type;
}
(*
  //------------------------------------------------------
  col___Name.Caption         :=STR_NAME;
  col___Azimuth.Caption      :=STR_AZIMUTH;
  col___Diameter.Caption     :=STR_DIAMETER;
  col___Gain.Caption         :=STR_GAIN; // '�� [dB]';
  col___Height.Caption       :=STR_HEIGHT;
  col___AntType_Name.Caption :=STR_TYPE;
  col___vert_width.Caption   :='������ ��� (V)';
  col___horz_width.Caption   :='������ ��� (H)';
  col___antenna_loss.Caption :=STR_LOSS_dB;
  col___Polarization.Caption :=STR_Polarization;
 // col___Pos.Caption.Text          :=STR_COORDS;
 // col___LOCATION_TYPE.Caption.Text:=STR_ANTENNA_Location_type;
*)

  // -------------------------------------------------------------------


 // col_AntType_Name.FieldName:=FLD_ANTENNATYPE_NAME;
  col___AntType_Name.DataBinding.FieldName:=FLD_ANTENNATYPE_NAME;

//  col_LOCATION_TYPE.FieldName:=FLD_LOCATION_TYPE;

///  col__LOCATION_TYPE.DataBinding.FieldName:=FLD_LOCATION_TYPE;





 //zzzzzzzzzzz dmLinkEnd_View.InitDB_Antennas (mem_Data1);

//  dxDBTree.LoadFromRegistry (FRegPath+ dxDBTree.name);


//  col___Pos.Visible := False;

//  dx_CheckColumnSizes_DBTreeList (dxDBTree);


  cx_SetColumnCaptions(cxGrid1DBTableView1,
    [
      FLD_Name,          STR_NAME,
      FLD_Azimuth,       STR_AZIMUTH,
      FLD_Diameter,      STR_DIAMETER,
      FLD_TILT,          STR_TILT, // '�� [dB]',
      FLD_Gain,          STR_GAIN, // '�� [dB]',
      FLD_Height,        STR_HEIGHT,
      FLD_ANTENNATYPE_NAME, STR_TYPE,
      FLD_vert_width,    '������ ��� [V]',
      FLD_horz_width,    '������ ��� [H]',
      FLD_loss,        STR_LOSS,
    //  SArr(FLD_antenna_loss,  STR_LOSS),
      FLD_Polarization_STR,  STR_Polarization,
      FLD_Mast_index,   STR_Mast_index,
      FLD_BAND,         STR_BAND,

      FLD_polarization_ratio , STR_polarization_ratio // '�������� ��������� H/V [dB]'
    ]);

end;


procedure Tframe_LinkEnd_antennas.FormDestroy(Sender: TObject);
begin
//  dxDBTree.SaveToRegistry (FRegPath+ dxDBTree.name);
//  cxGrid1DBTableView1.StoreToRegistry(FRegPath + cxGrid1DBTableView1.Name);

  g_Storage.StoreToRegistry(cxGrid1DBTableView1, className);


  inherited;
end;



// ---------------------------------------------------------------
procedure Tframe_LinkEnd_antennas.SetReadOnly(aValue: boolean);
// ---------------------------------------------------------------
begin
  { check : !!! }
  cx_SetGridReadOnly (cxGrid1DBTableView1, aValue);


  if aValue then
    cxGrid1DBTableView1.Styles.Background:=cxStyle_ReadOnly
  else
    cxGrid1DBTableView1.Styles.Background:=cxStyle_Edit



{  cxGrid1DBBandedTableView1.OptionsData.Editing:=aValue;
  cxGrid1DBBandedTableView1.OptionsSelection.CellSelect:=not aValue;
}
//  ToolBar2.Visible:=False;


end;


// ---------------------------------------------------------------
procedure Tframe_LinkEnd_antennas.View(aID: integer = 0; aObjName: string = '');
// ---------------------------------------------------------------
(*
  //-------------------------------------------------------------------
  function DoGetObjectNameByOwner(aObjectName: string): string;
  //-------------------------------------------------------------------
  begin
    if  Eq(aObjectName,OBJ_LINKEND)      then Result := OBJ_LINKEND_Antenna  else
    if  Eq(aObjectName,OBJ_PMP_SECTOR)   then Result := OBJ_PMP_SECTOR_ANT   else
    if  Eq(aObjectName,OBJ_PMP_TERMINAL) then Result := OBJ_PMP_TERMINAL_ANT else

  //  if  Eq(aObjectName,OBJ_CELL)         then Result := OBJ_CELL_ANT        else
  //  if  sObjName=OBJ_CELL_3G      then Result := OBJ_CELL_3G     else

    raise
      Exception.Create('function TdmAntenna.GetObjectNameByOwner(aObjectName: string): string;');

  end;
*)

var
  i: Integer;
begin
  if aID>0 then
  begin
    FID:=aID;
    FObjName:=aObjName;
  end;

(*
  FReadOnly := dmUser_Security.ProjectIsReadOnly;

  ADOStoredProc1.LockType := IIF(FReadOnly, ltReadOnly, ltBatchOptimistic);
  SetFormActionsEnabled(Self, not FReadOnly);

  cxGrid1DBTableView1.OptionsData.Editing := not FReadOnly;

*)
  //dmOnega_DB_data.Pmp_Site_Select_Item(ADOStoredProc1, aID);


(*  FUpdated:=false;
  dmLinkEnd_View.Open_Antennas (aID, mem_Data1, aObjName);
  FUpdated:=true;
*)
//  dx_CheckColumnSizes_DBTreeList (dxDBTree);

 // dxDBTree.FullExpand;

//  FAntObjName:=dmAntenna.GetObjectNameByOwner(aObjName);
//  FAntObjName:=DoGetObjectNameByOwner(FObjName);

 // OpenAntennas();


  i:=dmOnega_DB_data.OpenStoredProc(ADOStoredProc1, 'sp_Linkend_Antenna_Select',
      [
       FLD_ObjName, FObjName,
       FLD_ID,      FID
       ]);

   // db_View(ADOStoredProc1);
end;


//--------------------------------------------------------------------
procedure Tframe_LinkEnd_antennas.act_Antenna_EditExecute(Sender: TObject);
//--------------------------------------------------------------------
var
  iID: integer;
  k: Integer;


begin
//  db_ViewDataSet();


//  act_Setup_Dlg.Caption:= STR_SETUP_TABLE;
(*
  //---------------------------------
  if Sender = act_Setup_Dlg then begin
  //---------------------------------
    dx_Dlg_Customize_DBTreeList (dxDBTree);
  end else
*)


(*  //---------------------------------
  if Sender = act_Setup_Dlg then begin
  //---------------------------------
    dx_Dlg_Customize_DBTreeList (dxDBTree);
  end else
*)

  //---------------------------------
  if Sender=act_Antenna_Del then begin
  //---------------------------------
    if FDataSet.RecordCount>1 then
      if ConfirmDlg('������� �������') then
      begin
        iID :=FDataSet.FieldByName(FLD_ID).AsInteger;

        k:=dmOnega_DB_data.Linkend_Antenna_Del(iID);

  //      dmAntenna.Del(iID);

        View ();
//        qry_Antennas.Delete;
      end;
(*

   // if FDataSet[FLD_TYPE] = OBJ_ANTENNA then
      if dmAct_Antenna.Dlg_Edit (FDataSet[FLD_ID])  then
      begin
//        dmMapEngine1.ReCreateObject(otLinkEndAnt, mem_Data[FLD_ID]);

        View (FID, FObjName);
      end;*)

  end else


  //---------------------------------
  if Sender=act_Antenna_Edit then begin
  //---------------------------------
   // if FDataSet[FLD_TYPE] = OBJ_ANTENNA then
      if dmAct_Antenna.Dlg_Edit (FDataSet[FLD_ID])  then
      begin
//        dmMapEngine1.ReCreateObject(otLinkEndAnt, mem_Data[FLD_ID]);

        View ();
      end;
  end else

  //---------------------------------
  if Sender=act_Antenna_New then begin
  //---------------------------------
    iID:=0;

    if Eq(FObjName,OBJ_LINK_REPEATER) then
      iID:= dmAct_Antenna.Dlg_Add (OBJ_LINK_REPEATER, FID)

    else

//    else
{
    if Eq(FObjName,OBJ_PMP_SECTOR) then
      iID:= dmAct_Antenna.Dlg_AddPmpAntenna (FID) else

    if Eq(FObjName,OBJ_PMP_TERMINAL) then
      iID:= dmAct_Antenna.Dlg_AddPMPTerminalAntenna(FID) else
 }
    if Eq(FObjName,OBJ_LINKEND) then
      iID:= dmAct_Antenna.Dlg_Add_LinkEnd_Antenna (FID);


    if iID>0 then
      View ();
  end;

end;

//--------------------------------------------------------------------
procedure Tframe_LinkEnd_antennas.ActionList1Update(Action: TBasicAction; var Handled: Boolean);
//--------------------------------------------------------------------
var  b: boolean;
begin
//  act_Edit.Enabled:=(not mem_Data.IsEmpty);

//  b:=Assigned(dxDBTree.FocusedNode);
//..  act_Edit.Enabled := b;
//..  act_Antenna_Move.Enabled := b and (mem_Data.FieldValues[FLD_TYPE]=OBJ_ANTENNA);

  if FReadOnly then
    Exit;

  if not FDataSet.Active then
    Exit;
                   

  b :=FDataSet.RecordCount>0;

  act_Antenna_Del.Enabled :=  (not FReadOnly) and b;
  act_Antenna_Edit.Enabled := (not FReadOnly) and b;


{
  act_Antenna_New.Enabled :=  (mem_Data[FLD_TYPE]<>OBJ_ANTENNA);
  act_Antenna_Edit.Enabled := (mem_Data[FLD_TYPE]= OBJ_ANTENNA);
}
end;


procedure Tframe_LinkEnd_antennas.ADOStoredProc1AfterPost(DataSet: TDataSet);
var
  k: Integer;
begin
  k:=dmOnega_DB_data.ExecStoredProc_('sp_Linkend_Antenna_Update',
      [
       FLD_ID,     DataSet[FLD_ID],

       FLD_NAME,   DataSet[FLD_NAME],
       FLD_HEIGHT, DataSet[FLD_HEIGHT],

       FLD_LOSS, DataSet[FLD_LOSS],
       FLD_AZIMUTH, DataSet[FLD_AZIMUTH],
       FLD_TILT,    DataSet[FLD_TILT]

      ]);

   k:=0;
end;


procedure Tframe_LinkEnd_antennas.col___AntType_NamePropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  Dlg_Select_AntType();

end;

procedure Tframe_LinkEnd_antennas.cxGrid1Exit(Sender: TObject);
begin
  db_PostDataset(FDataSet);
end;

procedure Tframe_LinkEnd_antennas.cxDBVerticalGrid1antennaType_nameEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  Dlg_Select_AntType();
end;


//-------------------------------------------------------------------
procedure Tframe_LinkEnd_antennas.Dlg_Select_AntType;
//-------------------------------------------------------------------
var
  iID: integer;
  s,sObjName: string;
//  iRange1,iRange2: Integer;
  iAntTypeID: Integer;
  iRes: Integer;
  sName: Widestring;
begin
  iID       := FDataSet[FLD_ID];
  iAntTypeID:= FDataSet.FieldBYName(FLD_ANTENNATYPE_ID).AsInteger ;

//  s:=FObjName;

  if dmAct_Explorer.Dlg_Select_Object (otAntennaType, iAntTypeID, sName) then
  begin
    iRes := dmOnega_DB_data.Linkend_Antenna_Update_AntennaType(FDataSet[FLD_ID], iAntTypeID);

    if Eq(FObjName, OBJ_Linkend) then
    begin
       dmOnega_DB_data.Linkend_restore_XPIC_params(FDataSet[FLD_ID]);
              
      g_EventManager.PostEvent_(et_RefreshData,
        ['objname', OBJ_Linkend,
         'id',      FID
         ]);

    end;

  ////////  if IAct_LinkEnd.CheckRanges(FID, iAntTypeID) then
 ///////     dmOnega_DB_data.Object_UPDATE_AntType(OBJ_LINKEND_ANTENNA, FDataSet[FLD_ID], iAntTypeID);

//    if Assigned(IMapEngine) then
   //   dmMapEngine.ReCreateObject(ObjNameToType(FAntObjName), iID);

    View();
  end;
end;


end.




{


// ---------------------------------------------------------------
procedure Tframe_LinkEnd_antennas.SetReadOnly(Value: Boolean);
// ---------------------------------------------------------------
begin
  FReadOnly := Value;

  cxGrid1DBTableView1.OptionsData.Editing := not FReadOnly;

//  ADOStoredProc1.Close;

//  ADOStoredProc1.LockType := IIF(FReadOnly, ltReadOnly, ltBatchOptimistic);
  SetFormActionsEnabled(Self, not FReadOnly);

 // cxGrid1DBTableView1.OptionsData.Editing := not FReadOnly;


end;

}
