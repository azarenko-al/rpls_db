unit dm_LinkEnd_tools;

interface

uses
  Windows, Messages, DB, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Math, Variants,

  dm_Onega_DB_data,

  d_Object_dependence,

//
//  dm_Main,
//
////  dm_Custom,
//  dm_Antenna,

  dm_LinkEnd,

//  dm_LinkEndType,
//  dm_LinkLine,

//  u_rel_Profile,

  u_classes,
  u_Geo,
  u_db,
  u_dlg,
  u_func,
  u_radio,
  u_func_msg,

  u_types,

  u_const_db,
  u_link_const,
  u_const_msg,

  ADODB, dxmdaset;

type
  TdmLinkEnd_tools = class(TDataModule)
    qry_LinkEnds: TADOQuery;
    qry_Antennas: TADOQuery;
    qry_Items: TADOQuery;
    ADOStoredProc1: TADOStoredProc;

  private

  public

//    procedure GetSubItemList (aID: integer; aItemList: TIDList);

    function Del(aID: integer; aIDList: TIDList): boolean;

  end;

function dmLinkEnd_tools: TdmLinkEnd_tools;


//==================================================================
implementation  {$R *.DFM}
//==================================================================
//uses dm_Link;

var
  FdmLinkEnd_tools: TdmLinkEnd_tools;


// ---------------------------------------------------------------
function dmLinkEnd_tools: TdmLinkEnd_tools;
// ---------------------------------------------------------------
begin
  if not Assigned(FdmLinkEnd_tools) then
      FdmLinkEnd_tools := TdmLinkEnd_tools.Create(Application);

  Result := FdmLinkEnd_tools;
end;


//--------------------------------------------------------------------
function TdmLinkEnd_tools.Del(aID: integer; aIDList: TIDList): boolean;
//--------------------------------------------------------------------
var
  b: Boolean;
 // iLinkID, iLinkLineID: Integer;
  oSList: TStringList;
  s: string;


begin

(*  if Sender = act_Object_dependence then
    Tdlg_Object_dependence.ExecDlg(OBJ_LINK, FFocusedName, FFocusedID) else

*)

  Result:=False;

  aIDList.AddObjName(aID, OBJ_LINKEND);

//  dmOnega_DB_data.LinkEnd_GetSubItems(ADOStoredProc1, aID);
  dmOnega_DB_data.LinkEnd_Dependency(ADOStoredProc1, aID);


  oSList:=TStringList.Create;


(*  db_Open Query (qry_Items, 'exec  '+sp_LinkEnd_GetSubItems +'  :id',
               [db_Par(FLD_ID, aID)]);
*)

   with ADOStoredProc1 do
     while not EOF do
   begin
     s:=FieldByName(FLD_NAME).AsString;

     oSList.Add(s);

     aIDList.AddObjName(FieldValues[FLD_ID], FieldValues[FLD_OBJNAME]);
     Next;
   end;

   if oSList.Count<=1 then
     b:=true
   else
     b:= ConfirmDlg('������ � ��� ����� ������: '+ CRLF +
        oSList.Text + CRLF +
        '���������� ?');

   FreeAndNil(oSList);


   if b then
     Result:=dmLinkEnd.Del (aID);

end;



end.




  //db_ViewDataSet(qry_Items);

//
//
//  iLinkID := dmLinkEnd.GetLinkID(aID);
//
//  //���� ���� ��������, �� �� ���� ������ �������� �����
//  if iLinkID>0 then
//    iLinkLineID  :=gl_DB.GetIntFieldValue(TBL_LinkLineLinkXREF,FLD_LINKLINE_ID,
//          [db_Par(FLD_LINK_ID,iLinkID)]);
//
/////  CanDeleteLinkEnd:=False;
//
//  //���� ���� �������� � �����
//  if (iLinkID>0) and (iLinkLineID>0) then
//  begin
//	//�� ������ ������ �� �� ��������
//
//	if ConfirmDlg (Format('������ � ��� ����� ����� �� �������� %s, � ����� �� ����� %s,'+
//				   		 ' ���������� ���� ��������. ������������� ��������?',
//				    		[dmLink.GetNameByID(iLinkID),
//                 dmLinkLine.GetNameByID(iLinkLineID)])) then
//	begin
//  //  aIDList.AddObjName(iLinkID, OBJ_LINK);
//
//	//  sLinkGUID:= dmLink.GetGUIDByID(iLinkID);
//
//	  if dmLink.Del (iLinkID) then
//	  begin
//     ////////// g_ShellEvents.Shell_PostDelNode(sLinkGUID);
//
//   //   PostEvent (WE_EXPLORER_DELETE_NODE_BY_GUID,  [app_Par(PAR_GUID, sLinkGUID) ]);
//
//
//
//     // sh_PostDelNode (sLinkGUID);
//    /////////////  dmMapEngine1.DelObject (otLink, iLinkID);
//	  end;
//
//	////////////  sLinkLineGUID:= dmLinkLine.GetGUIDByID(iLinkLineID);
//
//	  if dmLinkLine.Del (iLinkLineID) then
//	  begin
//     //////////  g_ShellEvents.Shell_PostDelNode (sLinkLineGUID);
//
//      //sh_PostDelNode (sLinkLineGUID);
//  	 //	dmMapEngine1.DelObject (otLinkLine, iLinkLineID);
//	  end;
////	  CanDeleteLinkEnd:=True;
//	end else
//	  exit;
//
//  end else
//
//  //------------------------------------------------------
//
//	//���� ���� ��������
//	if (iLinkID>0) then
/////	begin
//	  //�� ������ ������ �� � ��������
//	  if ConfirmDlg(Format('������ � ��� ����� ����� �� �������� %s. ������������� ��������?',
//							[dmLink.GetNameByID(iLinkID)])) then
//	  begin
//   //   aIDList.AddObjName(iLinkID, OBJ_LINK);
//
//		//  sLinkGUID:= dmLink.GetGUIDByID (iLinkID);
//
//      if dmLink.Del (iLinkID) then
//      begin
////////////        g_ShellEvents.Shell_PostDelNode (sLinkGUID);
//
//
//
//    //	  sh_PostDelNode (sLinkGUID);���
//
//      //////////////  dmMapEngine1.DelObject (otLink, iLinkID);
//      end;
//
//  //	  CanDeleteLinkEnd:=True;
//	  end else
//  		exit;
//
//
//  Result:=dmLinkEnd.Del (aID);
//  end;
