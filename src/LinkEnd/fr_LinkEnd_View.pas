unit fr_LinkEnd_view;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
   cxControls, cxSplitter, Menus,  ImgList,  ActnList,
   ExtCtrls, ComCtrls, StdCtrls, ToolWin, cxPropertiesStore,

  cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, dxSkinsCore, dxSkinsDefaultPainters,


  fr_View_base,

dm_User_Security,

  u_func,
  u_reg,
  u_db,

  u_func_msg,

  u_const_db,  
  u_types,

 // dm_Folder,
 // dm_LINKEND,

 // fr_LinkEndType_Mode,

  fr_LinkEnd_inspector,
  fr_LinkEnd_antennas, dxBar, cxBarEditItem, cxClasses

  ;

type
  Tframe_LinkEnd_View = class(Tframe_View_Base, IMessageHandler)
    pn_Inspector: TPanel;
    cxSplitter1: TcxSplitter;
    pn_Antennas: TPanel;
    act_Get_Mode: TAction;
    ToolBar1: TToolBar;
    cb_Next_LinkEnd_AutoSave: TCheckBox;
    ToolButton1: TToolButton;
    procedure FormCreate(Sender: TObject);
  //  procedure FormDestroy(Sender: TObject);
    procedure act_Get_ModeExecute(Sender: TObject);
    procedure cb_Next_LinkEnd_AutoSaveClick(Sender: TObject);
  private
   // FID: integer;

    Fframe_Inspector: Tframe_LinkEnd_inspector;
    Fframe_antennas: Tframe_LinkEnd_antennas;

    procedure GetMessage(aMsg: TEventType; aParams: TEventParamList; var aHandled:
        boolean);


  public
    procedure View(aID: integer; aGUID: string); override;

  end;


//====================================================================
//====================================================================
implementation
{$R *.dfm}


//--------------------------------------------------------------------
procedure Tframe_LinkEnd_View.FormCreate(Sender: TObject);
//--------------------------------------------------------------------
begin
  inherited;

  ObjectName:=OBJ_LINKEND;

  TableName:=TBL_LINKEND;


  pn_Inspector.Align:=alClient;


  CreateChildForm(Tframe_LinkEnd_inspector, Fframe_Inspector, pn_Inspector);
  CreateChildForm(Tframe_LinkEnd_antennas,  Fframe_antennas,  pn_Antennas);


{  Fframe_Inspector:=Tframe_LinkEnd_inspector.CreateChildForm(pn_Inspector);

  Fframe_antennas:=Tframe_LinkEnd_antennas.CreateChildForm(pn_Antennas);
}


  AddComponentProp(pn_Antennas, PROP_HEIGHT);
  AddComponentProp(cb_Next_LinkEnd_AutoSave, PROP_CHECKED);
  cxPropertiesStore.RestoreFrom;

  cb_Next_LinkEnd_AutoSaveClick (nil);

end;


//--------------------------------------------------------------------
procedure Tframe_LinkEnd_View.View(aID: integer; aGUID: string);
//--------------------------------------------------------------------
var
//  bAllow: Boolean;
  bReadOnly: Boolean;
//  iGeoRegion_ID: Integer;
begin

   inherited ;


 // FID:= aID;
  if not RecordExists (aID) then
    Exit;


  Fframe_Inspector.View (aID, OBJ_LINKEND);


  bReadOnly:=dmUser_Security.Object_Edit_ReadOnly();

  Fframe_Inspector.SetReadOnly(bReadOnly);


  // -------------------------
//  FGeoRegion_ID := Fframe_Inspector.GetDataset().FieldByName(FLD_GeoRegion_ID).AsInteger;// GetIntFieldValue(FLD_GeoRegion_ID);
//  bReadOnly:=dmUser_Security.GeoRegion_is_ReadOnly(FGeoRegion_ID);

//  Fframe_Inspector.SetReadOnly(bReadOnly, IntToStr(FGeoRegion_ID));
//  Fframe_antennas.SetReadOnly(bReadOnly);
  // -------------------------

  Fframe_antennas.View (aID, OBJ_LINKEND);
  Fframe_antennas.SetReadOnly(bReadOnly);
end;

// -------------------------------------------------------------------
procedure Tframe_LinkEnd_View.act_Get_ModeExecute(Sender: TObject);
// -------------------------------------------------------------------
begin
  //-------------------------------------------------------------------
  if Sender = act_Get_Mode then begin
  //-------------------------------------------------------------------
    Fframe_Inspector.Dlg_UpdateMode;

  end else
    raise Exception.Create('');
end;


procedure Tframe_LinkEnd_View.cb_Next_LinkEnd_AutoSaveClick(Sender: TObject);
begin
  Fframe_Inspector.Params.Next_LinkEnd_AutoSave:=cb_Next_LinkEnd_AutoSave.Checked;
end;


// ---------------------------------------------------------------
procedure Tframe_LinkEnd_View.GetMessage(aMsg: TEventType; aParams:
    TEventParamList; var aHandled: boolean);
// ---------------------------------------------------------------
begin
  Assert(ID>0);

  case aMsg of
    et_RefreshData: begin
                      Assert (aParams.objname <> '');

                      if Eq (aParams.objname , OBJ_LinkEnd ) then
                        if aParams.id = ID then
                          Fframe_Inspector.View (ID, OBJ_LINKEND);
                   end;
  end;
end;



end.


  {


//--------------------------------------------------------------------
procedure Tframe_LinkEnd_View.FormDestroy(Sender: TObject);
//--------------------------------------------------------------------
begin
//  Fframe_Inspector.Free;
//  Fframe_LinkEnd_antennas.Free;

  inherited;
end;
