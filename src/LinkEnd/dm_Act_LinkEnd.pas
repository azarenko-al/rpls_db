unit dm_Act_LinkEnd;

interface
{$I ver.inc}

uses
  Classes, Menus, Forms, ActnList, 


  d_LinkEnd_Get_Band,

  u_shell_var,

//  dm_User_Security,

  i_Audit,
  

//  I_Act_Explorer, // dm_Act_Explorer
 // I_act_Antenna,  // dm_act_Antenna
//  I_Act_LinkEnd,  // dm_act_LinkEnd

  d_LinkEnd_inspector,

  fr_LinkEnd_View,

  dm_Onega_DB_data,

  dm_LinkEnd_tools,

  dm_act_Base,

  d_GroupAssign,
  d_Object_dependence,

  I_Object,



  dm_Act_LinkEndType,
  dm_LinkEndType,
  dm_Folder,

  dm_LinkEnd,

  u_const_db,
  u_const_str,


  u_Types,

  u_classes,
  u_func,


  u_db,
  u_dlg,
  u_Geo,

  f_Browser,

  d_LinkEnd_add
  ;

type
  TdmAct_LinkEnd = class(TdmAct_Base)//, IAct_LinkEnd_X)
    act_Antenna_Add: TAction;
    act_MoveToFolder: TAction;
    act_Change_LinkEndType: TAction;
    act_ShowLinkInfo: TAction;
    act_Object_dependence: TAction;
    ActionList2: TActionList;
    act_Copy: TAction;
    Action1: TAction;
    act_Audit: TAction;
    act_Change_AntType: TAction;

   // procedure DataModuleDestroy(Sender: TObject);
    procedure DataModuleCreate(Sender: TObject);
  private
    FBLPoint: TBLPoint;
    FProperty: integer;

//    procedure Dlg_Apply_template(aIDList: TIDList);
  //  procedure Test_Clear(aIDList: TIDList);
// TODO: CheckRanges11111
//  function CheckRanges11111(aLinkEndID, aAntTypeID: Integer): Boolean;
 //   procedure Copy;
    procedure Dlg_Change_AntType(aIDList: TIDList);
//    procedure DoAfterItemDel(aID: integer);

    procedure ShowLinkInfo(aID: Integer);

  protected
    procedure GetPopupMenu (aPopupMenu: TPopupMenu; aMenuType: TMenuType); override;
    function  GetViewForm (aOwnerForm: TForm): TForm; override;
    procedure DoAction (Sender: TObject);

    function ItemDel(aID: integer; aName: string = ''): boolean; override;
    function ItemAdd (aFolderID: integer): integer; override;

    procedure ShowOnMap (aID: integer);

    function ViewInProjectTree(aID: integer): TStrArray; override;
  public
    procedure Add (aFolderID,aProperty: integer); //overload;
    procedure AddByProperty (aProperty: integer);

    procedure AddByPos(aBLPoint: TBLPoint);

    procedure Dlg_Change_LinkEndType(Sender: TObject; aIDList: TIDList);
    function Dlg_Edit(aID: integer): boolean;// override;

    class procedure Init;
  end;

var
  dmAct_LinkEnd: TdmAct_LinkEnd;


//====================================================================
// implementation
//====================================================================
implementation
 {$R *.dfm}

uses
  dm_Act_Explorer,
  dm_act_Antenna, dm_Main;





//--------------------------------------------------------------------
class procedure TdmAct_LinkEnd.Init;
//--------------------------------------------------------------------
begin
  Assert(not Assigned(dmAct_LinkEnd));

  Application.CreateForm(TdmAct_LinkEnd, dmAct_LinkEnd);
//  dmAct_LinkEnd.GetInterface(IAct_LinkEnd_X, IAct_LinkEnd);

//  Assert(Assigned(IAct_LinkEnd));
end;

//
//procedure TdmAct_LinkEnd.DataModuleDestroy(Sender: TObject);
//begin
////  IAct_LinkEnd := nil;
////  dmAct_LinkEnd:= nil;
//
//  inherited;
//end;


//--------------------------------------------------------------------
procedure TdmAct_LinkEnd.DataModuleCreate(Sender: TObject);
//--------------------------------------------------------------------
begin
  inherited;

  ObjectName:=OBJ_LINKEND;

//  act_Undelete_RRS.Visible:= false;

  act_Antenna_Add.Caption:='������� �������';
  act_Change_LinkEndType.Caption:='������� ������������';
  act_Change_AntType.Caption:='������� ������ �������';
  act_Object_dependence.Caption:=S_OBJECT_DEPENDENCE;

  act_Copy.Caption:='����������';

  act_Add.Caption:='������� �� �������';


//  act_Apply_template.Caption:='������� �� �������';


//  act_Apply_template.Caption:='������';



  act_Audit.Caption:=STR_Audit;

//  act_Audit.Enabled:=False;



 // OnAfterItemDel:=DoAfterItemDel;

  SetActionsExecuteProc ([
//           act_Apply_template,

           act_Antenna_Add,

           act_Object_ShowOnMap,

        //   act_MoveToFolder,
           act_Change_LinkEndType,
           act_ShowLinkInfo,

           act_Object_dependence,

        //   act_Copy,
     //      act_Linkend_Clear,

           act_GroupAssign  ,
           act_Change_AntType,

           act_Audit

         //  act_Copy

         ], DoAction);

 SetCheckedActionsArr([
//        act_Apply_template,

        act_Add,
        act_Del_,
        act_Del_List,

        act_Antenna_Add,
        act_Copy,
        act_GroupAssign,

     //   act_Linkend_Clear,

        act_Change_LinkEndType,
        act_Change_AntType
     ]);


 // act_Add.Enabled := not dmUser_Security.ProjectIsReadOnly;

end;


//--------------------------------------------------------------------
procedure TdmAct_LinkEnd.ShowOnMap (aID: integer);
//--------------------------------------------------------------------
var
  bl: TBLPoint;
begin
  bl:=dmLinkEnd.GetPos (aID, OBJ_LINKEND);
  //Gefunction TdmObject_base.GetPos(aID: integer; aObjName: string): TBLPoint;aID


  ShowPointOnMap (bl, aID) ;//dmLinkEnd.GetPropertyPos (aID), aID);

end;

//--------------------------------------------------------------------
function TdmAct_LinkEnd.Dlg_Edit(aID: integer): boolean;
//--------------------------------------------------------------------
begin
  Result:=Tdlg_LinkEnd_inspector.ExecDlg1 (aID, OBJ_LINKEND);
//  Result:=Tframe_LinkEnd_inspector.ExecDlg1 (aID, OBJ_LINKEND);
end;


//--------------------------------------------------------------------
function TdmAct_LinkEnd.GetViewForm (aOwnerForm: TForm): TForm;                               
//--------------------------------------------------------------------
begin
  Result:=Tframe_LinkEnd_View.Create(aOwnerForm);
end;

//--------------------------------------------------------------------
procedure TdmAct_LinkEnd.AddByProperty (aProperty: integer);
//--------------------------------------------------------------------
begin
  FBLPoint:=NULL_BLPOINT;
  FProperty:=aProperty;
  inherited Add (0);
end;

//--------------------------------------------------------------------
procedure TdmAct_LinkEnd.Add (aFolderID,aProperty: integer);
//--------------------------------------------------------------------
begin
  FBLPoint:=NULL_BLPOINT;
  FProperty:=aProperty;
  inherited Add (aFolderID);
end;


//--------------------------------------------------------------------
function TdmAct_LinkEnd.ViewInProjectTree(aID: integer): TStrArray;
//--------------------------------------------------------------------
var sFolderGUID: string;
  iFolderID: Integer;

//var sGUID: string;
begin
  if Assigned(IMainProjectExplorer) then
  begin
    IMainProjectExplorer.ExpandByGUID(GUID_PROJECT);
    IMainProjectExplorer.ExpandByGUID(GUID_LINK_GROUP);
    IMainProjectExplorer.ExpandByGUID(GUID_LinkEnd);

   // sGUID:=dmFolder.GetGUIDByID (dmLinkEnd.GetFolderID(aID));


    iFolderID := dmLinkEnd.GetFolderID(aID);

    if iFolderID=0 then
      sFolderGUID:=g_Obj.ItemByName[ObjectName].RootFolderGUID
    else
      sFolderGUID:=dmFolder.GetGUIDByID(iFolderID);


    Result := StrArray([GUID_PROJECT,
                        GUID_LINK_GROUP,
                        GUID_LinkEnd,
                        sFolderGUID]);


    IMainProjectExplorer.ExpandByGUID(sFolderGUID);
//    IMainProjectExplorer.FocuseNodeByGUID (aGUID);

    IMainProjectExplorer.FocuseNodeByObjName(ObjectName, aID);

  end;
end;


//--------------------------------------------------------------------
procedure TdmAct_LinkEnd.AddByPos(aBLPoint: TBLPoint);
//--------------------------------------------------------------------
begin
  FBLPoint:=aBLPoint;
  FProperty:=0;
  inherited Add (0);

end;


//--------------------------------------------------------------------
procedure TdmAct_LinkEnd.ShowLinkInfo(aID: Integer);
//--------------------------------------------------------------------
var
  iLinkID: integer;
begin
  iLinkID:=dmLinkEnd.GetLinkID(aID);

  if iLinkID <> 0 then
    Tfrm_Browser.ViewObjectByID(OBJ_LINK, iLinkID)
  else
    MsgDlg('��� �������� � ���������');
end;



//--------------------------------------------------------------------
procedure TdmAct_LinkEnd.Dlg_Change_AntType(aIDList: TIDList);
//--------------------------------------------------------------------
var
  i: Integer;
  iAntennaTypeID: Integer;
  sName: WideString;
begin
  if dmAct_Explorer.Dlg_Select_Object(otAntennaType, iAntennaTypeID, sName) then
    for i:=0 to FSelectedIDList.Count-1 do
      dmOnega_DB_data.LinkEnd_update_antenna_type
          (FSelectedIDList[i].ID, iAntennaTypeID);
end;

{
//--------------------------------------------------------------------
procedure TdmAct_LinkEnd.Test_Clear(aIDList: TIDList);
//--------------------------------------------------------------------
var
  i: Integer;
  iAntennaTypeID: Integer;
  sName: WideString;
begin
//  if dmAct_Explorer.Dlg_Select_Object(otAntennaType, iAntennaTypeID, sName) then
    for i:=0 to FSelectedIDList.Count-1 do
      dmOnega_DB_data.ExecStoredProc_('sp_LinkEnd_Clear' ,
          [
            FLD_ID, FSelectedIDList[i].ID
          ]);

end;
}

//--------------------------------------------------------------------
function TdmAct_LinkEnd.ItemAdd (aFolderID: integer): integer;
//--------------------------------------------------------------------
var
  rec: Tdlg_LinkEnd_add_rec;
  sGUID: string;
begin
   FillChar(rec, SizeOf(rec), 0);

 //  rec.FolderID  :=aFolderID;
   rec.BLPoint   :=FBLPoint;
   rec.PropertyID:=FProperty;


  Result:=Tdlg_LinkEnd_add.ExecDlg_LinkEnd(rec);//aFolderID, FBLPoint, FProperty);
  if Result>0 then
  begin
 //    sGUID := dmProperty.GetGUIDByID(FProperty);
//     g_ShellEvents.Shell_UpdateNodeChildren_ByGUID (sGUID);

 //   Assert(Assigned(IMapAct));


//    dmMapEngine.CreateObject (otLINKEND, Result);
 //   dmMapEngine.Sync (otLinkEndAnt);
   // if assigned(IMapAct) then
    //  dmAct_Map.MapRefresh;
//    PostSyncDB_AddObject (Result);
  end;
end;

//--------------------------------------------------------------------
function TdmAct_LinkEnd.ItemDel(aID: integer; aName: string = ''): boolean;
//--------------------------------------------------------------------
begin
  Result:=dmLinkEnd_tools.Del (aID, FDeletedIDList);
end;


//--------------------------------------------------------------------
// actions
//--------------------------------------------------------------------
procedure TdmAct_LinkEnd.DoAction (Sender: TObject);
var
  iID: integer;
  S,sNewName: string;
  iAntennaCount: Integer;
begin


  if Sender=act_Audit then
     Init_IAuditX.ExecDlg( dmMain.GetConnectionString, TBL_LinkEnd, FFocusedID) else

  //   Tdlg_Audit.ExecDlg (TBL_LinkEnd, FFocusedID) else


(*  if Sender=act_MoveToFolder then
    raise Exception.Create('')

   // dmAct_Folder.MoveToFolderDlg (ObjectName, FSelectedPIDLs)
  else
*)
//  if Sender=act_Copy then
//    Copy() else

  if Sender=act_GroupAssign then
    Tdlg_GroupAssign.ExecDlg (OBJ_LinkEnd, FSelectedIDList) else


//  if Sender=act_Linkend_Clear then
  //  Test_Clear(FSelectedIDList) else
         

  if Sender = act_Object_dependence then
    Tdlg_Object_dependence.ExecDlg(OBJ_LINKEND, FFocusedName, FFocusedID) else

  if Sender=act_Object_ShowOnMap then
    ShowOnMap(FFocusedID) else

  if Sender=act_GroupAssign then
    Tdlg_GroupAssign.ExecDlg (OBJ_Linkend, FSelectedIDList) else

  if Sender=act_Change_AntType then
    Dlg_Change_AntType (FSelectedIDList) else

//  if Sender=act_Apply_template then
//    Dlg_Apply_template (FSelectedIDList) else


    //Dlg_Apply_template

  //------------------------------------------------------
  if Sender=act_Antenna_Add then
  //------------------------------------------------------
  begin
    dmAct_Antenna.Dlg_Add_LinkEnd_Antenna (FFocusedID);
  end else

  if Sender=act_Change_LinkEndType then
    Dlg_Change_LinkEndType(Sender, FSelectedIDList) else

  if Sender=act_ShowLinkInfo then
  	ShowLinkInfo(FFocusedID) else

end;


//--------------------------------------------------------------------
procedure TdmAct_LinkEnd.GetPopupMenu;
//--------------------------------------------------------------------
var
//  bReadOnly: Boolean;
  iAntennaCount: integer;
begin
 // CheckActionsEnable();


  case aMenuType of
    mtFolder: begin
                AddFolderMenu_Create (aPopupMenu);
                AddMenuItem (aPopupMenu, nil);
                AddFolderPopupMenu (aPopupMenu);

             ///   AddMenuItem (aPopupMenu, act_Undelete);
               // AddMenuItem (aPopupMenu, act_Undelete_RRS);
              end;
    mtItem:   begin

{                iAntennaCount:=dmAntenna.GetOwnerAntennaCount (FFocusedID, OBJ_LINKEND);
                act_Antenna_Add.Enabled:= not (iAntennaCount > 1);
}


           //     AddMenuItem (aPopupMenu, act_Apply_template);

                AddMenuItem (aPopupMenu, act_Antenna_Add);
                AddMenuItem (aPopupMenu, nil);
                AddMenuItem (aPopupMenu, act_Object_ShowOnMap);
                AddMenuItem (aPopupMenu, act_ShowLinkInfo);
                AddMenuItem (aPopupMenu, act_Object_dependence);

                AddMenuItem (aPopupMenu, nil);

                AddMenuItem (aPopupMenu, act_GroupAssign);


              //  AddMenuItem (aPopupMenu, act_MoveToFolder);
        ///        AddMenuItem (aPopupMenu, act_Del_);

                AddFolderMenu_Tools (aPopupMenu);


     //     //      AddMenuItem (aPopupMenu, act_Change_LinkEndType);
           //     AddMenuItem (aPopupMenu, nil);
                AddMenuItem (aPopupMenu, act_Change_LinkEndType);
                AddMenuItem (aPopupMenu, nil);
                AddMenuItem (aPopupMenu, act_Del_);
            //    AddMenuItem (aPopupMenu, act_Copy);

                AddMenuItem (aPopupMenu, act_Change_AntType);


                AddMenuItem (aPopupMenu, act_Audit);


      //////////	AddMenuItem (aPopupMenu, act_Undelete_Ant);
              end;
    mtList:   begin
//                AddMenuItem (aPopupMenu, act_Apply_template);


                AddMenuItem (aPopupMenu, act_Del_list);
                AddFolderMenu_Tools (aPopupMenu);
                AddMenuItem (aPopupMenu, nil);
                AddMenuItem (aPopupMenu, act_Change_LinkEndType);

                AddMenuItem (aPopupMenu, act_GroupAssign);

                AddMenuItem (aPopupMenu, act_Change_AntType);

              //  AddMenuItem (aPopupMenu, act_Linkend_Clear);



              end;

  end;
end;



//--------------------------------------------------------------------
procedure TdmAct_LinkEnd.Dlg_Change_LinkEndType(Sender: TObject; aIDList:
    TIDList);
//--------------------------------------------------------------------
var
  b: Boolean;
  i: Integer;
  iMode,iLinkEndTypeID: integer;
  sName: Widestring;
  mode_rec: TdmLinkEndTypeModeInfoRec;
  iLinkEndType_Mode_ID: integer;
  k: Integer;

   rec_band: TLinkEnd_Band_rec;
begin
  FillChar(rec_band, SizeOf(rec_band), 0);


  //Assert(Assigned(IShell));
//  Assert(Assigned(ILinkEndType));

  if dmAct_Explorer.Dlg_Select_Object(otLinkEndType, iLinkEndTypeID, sName) then
    if dmAct_LinkEndType.Dlg_GetMode (iLinkEndTypeID, 0, mode_rec) then
    begin

      rec_band.LinkEndType_ID := iLinkEndTypeID;

      b:= Tdlg_LinkEnd_Get_Band.ExecDlg(rec_band);


      for i := 0 to aIDList.Count-1 do
        if b then
        begin
          k := dmOnega_DB_data.ExecStoredProc_('sp_Object_update_LinkEndType',
               [FLD_ObjName,             OBJ_LinkEnd,

                FLD_ID,                  aIDList[i].ID,
                FLD_LinkEndType_ID,      iLinkEndTypeID,
                FLD_LinkEndType_Mode_ID, mode_rec.ID,

                FLD_LinkEndType_band_ID, rec_band.LinkEndType_band_ID,
                FLD_CHANNEL_NUMBER,      rec_band.Channel_number
                ]);

       //  Assert(k>0);

        end
         else
         begin
            k:=dmOnega_DB_data.Object_update_LinkEndType
              (OBJ_LinkEnd, aIDList[i].ID, iLinkEndTypeID, mode_rec.ID);

         //   Assert(k>0);
         end;

{
        k:=dmOnega_DB_data.Object_update_LinkEndType
          (OBJ_LinkEnd,
           aIDList[i].ID, iLinkEndTypeID, mode_rec.ID);
}

{
  Result := ExecStoredProc('sp_Object_update_LinkEndType',
         [db_Par(FLD_ObjName,         aObjName),

          db_Par(FLD_ID,              aID),
          db_Par(FLD_LinkEndType_ID,  aLinkEndTypeID),
          db_Par(FLD_LinkEndType_Mode_ID, aLinkEndType_Mode_ID)  ]);

}


    end;

end;



end.

