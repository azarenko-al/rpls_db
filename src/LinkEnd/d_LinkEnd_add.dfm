inherited dlg_LinkEnd_add: Tdlg_LinkEnd_add
  Left = 1244
  Top = 115
  Width = 506
  Height = 679
  ActiveControl = btn_Ok
  Caption = 'dlg_LinkEnd_add'
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  inherited pn_Buttons: TPanel
    Top = 605
    Width = 490
    inherited Bevel1: TBevel
      Width = 490
    end
    inherited Panel3: TPanel
      Left = 304
      Width = 186
      inherited btn_Ok: TButton
        Left = 7
      end
    end
  end
  inherited pn_Top_: TPanel
    Width = 490
    inherited Bevel2: TBevel
      Width = 490
    end
    inherited pn_Header: TPanel
      Width = 490
    end
  end
  inherited Panel1: TPanel
    Width = 490
    inherited ed_Name_: TEdit
      Width = 487
    end
  end
  inherited Panel2: TPanel
    Width = 490
    Height = 476
    inherited PageControl1: TPageControl
      Width = 480
      Height = 428
      inherited TabSheet1: TTabSheet
        object cxVerticalGrid1: TcxVerticalGrid
          Left = 0
          Top = 0
          Width = 472
          Height = 397
          BorderStyle = cxcbsNone
          Align = alClient
          LookAndFeel.Kind = lfUltraFlat
          LookAndFeel.NativeStyle = False
          OptionsView.CellTextMaxLineCount = 3
          OptionsView.ShowEditButtons = ecsbAlways
          OptionsView.AutoScaleBands = False
          OptionsView.PaintStyle = psDelphi
          OptionsView.GridLineColor = clBtnShadow
          OptionsView.RowHeaderMinWidth = 30
          OptionsView.RowHeaderWidth = 225
          OptionsView.ValueWidth = 40
          Styles.OnGetContentStyle = cxVerticalGrid1StylesGetContentStyle
          TabOrder = 0
          OnDrawValue = cxVerticalGrid1DrawValue
          Version = 1
          object row_Property_type: TcxEditorRow
            Properties.Caption = 'row_Property_type'
            Properties.DataBinding.ValueType = 'String'
            Properties.Value = Null
            Visible = False
            ID = 0
            ParentID = -1
            Index = 0
            Version = 1
          end
          object row_Property: TcxEditorRow
            Properties.Caption = 'Property'
            Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
            Properties.EditProperties.Alignment.Horz = taLeftJustify
            Properties.EditProperties.AutoSelect = False
            Properties.EditProperties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            Properties.EditProperties.ReadOnly = True
            Properties.EditProperties.OnButtonClick = row_Property1EditPropertiesButtonClick
            Properties.DataBinding.ValueType = 'String'
            Properties.Options.Filtering = False
            Properties.Options.ShowEditButtons = eisbAlways
            Properties.Value = Null
            ID = 1
            ParentID = -1
            Index = 1
            Version = 1
          end
          object row_Property_pos: TcxEditorRow
            Properties.Caption = 'row_Property_pos'
            Properties.DataBinding.ValueType = 'String'
            Properties.Value = Null
            ID = 2
            ParentID = 1
            Index = 0
            Version = 1
          end
          object row_property_exists: TcxEditorRow
            Properties.Caption = 'row_property_exists'
            Properties.DataBinding.ValueType = 'String'
            Properties.Value = Null
            ID = 3
            ParentID = 1
            Index = 1
            Version = 1
          end
          object row_Property_Address: TcxEditorRow
            Properties.Caption = 'row_Property_Address'
            Properties.DataBinding.ValueType = 'String'
            Properties.Value = Null
            ID = 4
            ParentID = 1
            Index = 2
            Version = 1
          end
          object row_property_delimiter: TcxCategoryRow
            Expanded = False
            ID = 5
            ParentID = -1
            Index = 2
            Version = 1
          end
          object row_AssociateWithPmpSite: TcxEditorRow
            Properties.Caption = 'row_AssociateWithPmpSite'
            Properties.EditPropertiesClassName = 'TcxCheckBoxProperties'
            Properties.EditProperties.Alignment = taLeftJustify
            Properties.EditProperties.NullStyle = nssUnchecked
            Properties.EditProperties.ReadOnly = False
            Properties.EditProperties.UseAlignmentWhenInplace = True
            Properties.EditProperties.ValueChecked = 'True'
            Properties.EditProperties.ValueGrayed = ''
            Properties.EditProperties.ValueUnchecked = 'False'
            Properties.DataBinding.ValueType = 'Boolean'
            Properties.Value = Null
            ID = 6
            ParentID = -1
            Index = 3
            Version = 1
          end
          object row_PmpSite1: TcxEditorRow
            Expanded = False
            Properties.Caption = 'row_PmpSite'
            Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
            Properties.EditProperties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            Properties.EditProperties.ReadOnly = True
            Properties.EditProperties.OnButtonClick = row_Property1EditPropertiesButtonClick
            Properties.DataBinding.ValueType = 'String'
            Properties.Value = Null
            ID = 7
            ParentID = 6
            Index = 0
            Version = 1
          end
          object row_PmpSector1: TcxEditorRow
            Expanded = False
            Properties.Caption = 'row_PmpSector'
            Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
            Properties.EditProperties.Alignment.Horz = taLeftJustify
            Properties.EditProperties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            Properties.EditProperties.ReadOnly = True
            Properties.EditProperties.OnButtonClick = row_Property1EditPropertiesButtonClick
            Properties.DataBinding.ValueType = 'String'
            Properties.Value = Null
            ID = 8
            ParentID = 6
            Index = 1
            Version = 1
          end
          object row_Pmp_Delimiter: TcxCategoryRow
            Expanded = False
            ID = 9
            ParentID = -1
            Index = 4
            Version = 1
          end
          object row_Pmp_sector: TcxCategoryRow
            Expanded = False
            Properties.Caption = 'PmP '#1089#1077#1082#1090#1086#1088
            ID = 10
            ParentID = -1
            Index = 5
            Version = 1
          end
          object row_Cell_Layer: TcxEditorRow
            Expanded = False
            Properties.Caption = 'row_Cell_Layer'
            Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
            Properties.EditProperties.Alignment.Horz = taLeftJustify
            Properties.EditProperties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            Properties.EditProperties.ReadOnly = True
            Properties.EditProperties.OnButtonClick = row_Property1EditPropertiesButtonClick
            Properties.DataBinding.ValueType = 'String'
            Properties.Value = Null
            ID = 11
            ParentID = 10
            Index = 0
            Version = 1
          end
          object row_Calc_Model: TcxEditorRow
            Expanded = False
            Properties.Caption = 'row_Calc_Model'
            Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
            Properties.EditProperties.Alignment.Horz = taLeftJustify
            Properties.EditProperties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            Properties.EditProperties.ReadOnly = True
            Properties.EditProperties.OnButtonClick = row_Property1EditPropertiesButtonClick
            Properties.DataBinding.ValueType = 'String'
            Properties.Value = Null
            ID = 12
            ParentID = 10
            Index = 1
            Version = 1
          end
          object row_CalcRadius_km: TcxEditorRow
            Expanded = False
            Properties.Caption = 'CalcRadius_km'
            Properties.EditPropertiesClassName = 'TcxTextEditProperties'
            Properties.EditProperties.Alignment.Horz = taLeftJustify
            Properties.EditProperties.Alignment.Vert = taVCenter
            Properties.EditProperties.MaxLength = 0
            Properties.EditProperties.ReadOnly = False
            Properties.DataBinding.ValueType = 'String'
            Properties.Value = Null
            ID = 13
            ParentID = 10
            Index = 2
            Version = 1
          end
          object row_Calc_Delimiter: TcxCategoryRow
            Expanded = False
            ID = 14
            ParentID = -1
            Index = 6
            Version = 1
          end
          object row_Equipment_Type: TcxEditorRow
            Expanded = False
            Properties.Caption = #1042#1080#1076' '#1056#1056#1057
            Properties.EditPropertiesClassName = 'TcxComboBoxProperties'
            Properties.EditProperties.Alignment.Horz = taLeftJustify
            Properties.EditProperties.Alignment.Vert = taVCenter
            Properties.EditProperties.DropDownListStyle = lsFixedList
            Properties.EditProperties.DropDownRows = 7
            Properties.EditProperties.ImmediatePost = True
            Properties.EditProperties.Items.Strings = (
              #1058#1080#1087#1086#1074#1086#1081
              #1053#1077#1090#1080#1087#1086#1074#1086#1081)
            Properties.EditProperties.MaxLength = 0
            Properties.EditProperties.ReadOnly = False
            Properties.EditProperties.Revertable = True
            Properties.EditProperties.OnCloseUp = row_Equipment_TypeEditPropertiesCloseUp
            Properties.DataBinding.ValueType = 'String'
            Properties.Value = Null
            ID = 15
            ParentID = -1
            Index = 7
            Version = 1
          end
          object row_Equip_Typed: TcxEditorRow
            Properties.Caption = #1054#1073#1086#1088#1091#1076#1086#1074#1072#1085#1080#1077' '#1090#1080#1087#1086#1074#1086#1077
            Properties.EditPropertiesClassName = 'TcxTextEditProperties'
            Properties.EditProperties.Alignment.Horz = taLeftJustify
            Properties.EditProperties.Alignment.Vert = taVCenter
            Properties.EditProperties.MaxLength = 0
            Properties.EditProperties.ReadOnly = True
            Properties.DataBinding.ValueType = 'String'
            Properties.Options.Editing = False
            Properties.Value = Null
            ID = 16
            ParentID = -1
            Index = 8
            Version = 1
          end
          object row_LinkEndType: TcxEditorRow
            Expanded = False
            Properties.Caption = #1054#1073#1086#1088#1091#1076#1086#1074#1072#1085#1080#1077
            Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
            Properties.EditProperties.Alignment.Horz = taLeftJustify
            Properties.EditProperties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            Properties.EditProperties.ReadOnly = True
            Properties.EditProperties.OnButtonClick = row_Property1EditPropertiesButtonClick
            Properties.DataBinding.ValueType = 'String'
            Properties.Value = Null
            ID = 17
            ParentID = 16
            Index = 0
            Version = 1
          end
          object row_Mode: TcxEditorRow
            Expanded = False
            Properties.Caption = #1088#1077#1078#1080#1084
            Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
            Properties.EditProperties.Alignment.Horz = taLeftJustify
            Properties.EditProperties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            Properties.EditProperties.ReadOnly = True
            Properties.EditProperties.OnButtonClick = row_Property1EditPropertiesButtonClick
            Properties.DataBinding.ValueType = 'String'
            Properties.Value = Null
            ID = 18
            ParentID = 16
            Index = 1
            Version = 1
          end
          object row_ChannelType: TcxEditorRow
            Properties.Caption = 'ChannelType'
            Properties.EditPropertiesClassName = 'TcxComboBoxProperties'
            Properties.EditProperties.DropDownListStyle = lsFixedList
            Properties.EditProperties.Items.Strings = (
              'low'
              'high')
            Properties.DataBinding.ValueType = 'String'
            Properties.Value = 'low'
            ID = 19
            ParentID = 16
            Index = 2
            Version = 1
          end
          object row_Equip_not_Typed: TcxEditorRow
            Expanded = False
            Properties.Caption = #1054#1073#1086#1088#1091#1076#1086#1074#1072#1085#1080#1077' '#1085#1077' '#1090#1080#1087#1086#1074#1086#1077
            Properties.EditPropertiesClassName = 'TcxTextEditProperties'
            Properties.EditProperties.Alignment.Horz = taLeftJustify
            Properties.EditProperties.Alignment.Vert = taVCenter
            Properties.EditProperties.MaxLength = 0
            Properties.EditProperties.ReadOnly = True
            Properties.DataBinding.ValueType = 'String'
            Properties.Options.Editing = False
            Properties.Value = Null
            ID = 20
            ParentID = -1
            Index = 9
            Version = 1
          end
          object row_Freq_GHz: TcxEditorRow
            Expanded = False
            Properties.Caption = 'row_Freq GHz'
            Properties.EditPropertiesClassName = 'TcxTextEditProperties'
            Properties.EditProperties.Alignment.Horz = taLeftJustify
            Properties.EditProperties.Alignment.Vert = taVCenter
            Properties.EditProperties.MaxLength = 0
            Properties.EditProperties.ReadOnly = False
            Properties.DataBinding.ValueType = 'String'
            Properties.Value = Null
            ID = 21
            ParentID = 20
            Index = 0
            Version = 1
          end
          object row_Power_dBm: TcxEditorRow
            Expanded = False
            Properties.Caption = 'row_Power'
            Properties.EditPropertiesClassName = 'TcxTextEditProperties'
            Properties.EditProperties.Alignment.Horz = taLeftJustify
            Properties.EditProperties.Alignment.Vert = taVCenter
            Properties.EditProperties.MaxLength = 0
            Properties.EditProperties.ReadOnly = False
            Properties.DataBinding.ValueType = 'String'
            Properties.Value = Null
            ID = 22
            ParentID = 20
            Index = 1
            Version = 1
          end
          object row_Loss: TcxEditorRow
            Expanded = False
            Properties.Caption = 'row_Loss'
            Properties.EditPropertiesClassName = 'TcxTextEditProperties'
            Properties.EditProperties.Alignment.Horz = taLeftJustify
            Properties.EditProperties.Alignment.Vert = taVCenter
            Properties.EditProperties.MaxLength = 0
            Properties.EditProperties.ReadOnly = False
            Properties.DataBinding.ValueType = 'String'
            Properties.Value = Null
            ID = 23
            ParentID = 20
            Index = 2
            Version = 1
          end
          object row_Raznos_MHz: TcxEditorRow
            Expanded = False
            Properties.Caption = 'row_Raznos_MHz'
            Properties.EditPropertiesClassName = 'TcxTextEditProperties'
            Properties.EditProperties.Alignment.Horz = taLeftJustify
            Properties.EditProperties.Alignment.Vert = taVCenter
            Properties.EditProperties.MaxLength = 0
            Properties.EditProperties.ReadOnly = False
            Properties.DataBinding.ValueType = 'String'
            Properties.Value = Null
            ID = 24
            ParentID = 20
            Index = 3
            Version = 1
          end
          object row_signature_width: TcxEditorRow
            Expanded = False
            Properties.Caption = 'row_signature_width'
            Properties.EditPropertiesClassName = 'TcxTextEditProperties'
            Properties.EditProperties.Alignment.Horz = taLeftJustify
            Properties.EditProperties.Alignment.Vert = taVCenter
            Properties.EditProperties.MaxLength = 0
            Properties.EditProperties.ReadOnly = False
            Properties.DataBinding.ValueType = 'String'
            Properties.Value = Null
            ID = 25
            ParentID = 20
            Index = 4
            Version = 1
          end
          object row_signature_height: TcxEditorRow
            Expanded = False
            Properties.Caption = 'row_signature_height'
            Properties.EditPropertiesClassName = 'TcxTextEditProperties'
            Properties.EditProperties.Alignment.Horz = taLeftJustify
            Properties.EditProperties.Alignment.Vert = taVCenter
            Properties.EditProperties.MaxLength = 0
            Properties.EditProperties.ReadOnly = False
            Properties.DataBinding.ValueType = 'String'
            Properties.Value = Null
            ID = 26
            ParentID = 20
            Index = 5
            Version = 1
          end
          object row_Sens_ber_3: TcxEditorRow
            Expanded = False
            Properties.Caption = 'row_Sens_ber_3'
            Properties.EditPropertiesClassName = 'TcxTextEditProperties'
            Properties.EditProperties.Alignment.Horz = taLeftJustify
            Properties.EditProperties.Alignment.Vert = taVCenter
            Properties.EditProperties.MaxLength = 0
            Properties.EditProperties.ReadOnly = False
            Properties.DataBinding.ValueType = 'String'
            Properties.Value = Null
            ID = 27
            ParentID = 20
            Index = 6
            Version = 1
          end
          object row_Sens_ber_6: TcxEditorRow
            Expanded = False
            Properties.Caption = 'row_Sens_ber_6'
            Properties.EditPropertiesClassName = 'TcxTextEditProperties'
            Properties.EditProperties.Alignment.Horz = taLeftJustify
            Properties.EditProperties.Alignment.Vert = taVCenter
            Properties.EditProperties.MaxLength = 0
            Properties.EditProperties.ReadOnly = False
            Properties.DataBinding.ValueType = 'String'
            Properties.Value = Null
            ID = 28
            ParentID = 20
            Index = 7
            Version = 1
          end
          object row_Kng: TcxEditorRow
            Expanded = False
            Properties.Caption = 'row_Kng'
            Properties.EditPropertiesClassName = 'TcxTextEditProperties'
            Properties.EditProperties.Alignment.Horz = taLeftJustify
            Properties.EditProperties.Alignment.Vert = taVCenter
            Properties.EditProperties.MaxLength = 0
            Properties.EditProperties.ReadOnly = False
            Properties.DataBinding.ValueType = 'String'
            Properties.Value = Null
            ID = 29
            ParentID = 20
            Index = 8
            Version = 1
          end
          object row_Band: TcxEditorRow
            Properties.Caption = #1044#1080#1072#1087#1072#1079#1086#1085
            Properties.EditPropertiesClassName = 'TcxComboBoxProperties'
            Properties.EditProperties.Items.Strings = (
              '1'
              '2')
            Properties.DataBinding.ValueType = 'String'
            Properties.Value = Null
            ID = 30
            ParentID = 20
            Index = 9
            Version = 1
          end
          object cxVerticalGrid1CategoryRow5: TcxCategoryRow
            Expanded = False
            ID = 31
            ParentID = -1
            Index = 10
            Version = 1
          end
          object cxVerticalGrid1EditorRow23: TcxEditorRow
            Expanded = False
            Properties.Caption = #1040#1085#1090#1077#1085#1085#1072' '#1086#1089#1085#1086#1074#1085#1072#1103
            Properties.EditPropertiesClassName = 'TcxTextEditProperties'
            Properties.EditProperties.Alignment.Horz = taLeftJustify
            Properties.EditProperties.Alignment.Vert = taVCenter
            Properties.EditProperties.MaxLength = 0
            Properties.EditProperties.ReadOnly = False
            Properties.DataBinding.ValueType = 'String'
            Properties.Options.Editing = False
            Properties.Value = Null
            ID = 32
            ParentID = -1
            Index = 11
            Version = 1
          end
          object row_Antenna1_Type: TcxEditorRow
            Expanded = False
            Properties.Caption = #1058#1080#1087
            Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
            Properties.EditProperties.Alignment.Horz = taLeftJustify
            Properties.EditProperties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            Properties.EditProperties.ReadOnly = True
            Properties.EditProperties.OnButtonClick = row_Property1EditPropertiesButtonClick
            Properties.DataBinding.ValueType = 'String'
            Properties.Value = Null
            ID = 33
            ParentID = 32
            Index = 0
            Version = 1
          end
          object row_Antenna1_Height: TcxEditorRow
            Expanded = False
            Properties.Caption = #1042#1099#1089#1086#1090#1072
            Properties.EditPropertiesClassName = 'TcxTextEditProperties'
            Properties.EditProperties.Alignment.Horz = taLeftJustify
            Properties.EditProperties.Alignment.Vert = taVCenter
            Properties.EditProperties.MaxLength = 0
            Properties.EditProperties.ReadOnly = False
            Properties.DataBinding.ValueType = 'String'
            Properties.Value = '0'
            ID = 34
            ParentID = 32
            Index = 1
            Version = 1
          end
          object row_Antenna1_Azimuth: TcxEditorRow
            Expanded = False
            Properties.Caption = #1040#1079#1080#1084#1091#1090
            Properties.EditPropertiesClassName = 'TcxTextEditProperties'
            Properties.EditProperties.Alignment.Horz = taLeftJustify
            Properties.EditProperties.Alignment.Vert = taVCenter
            Properties.EditProperties.MaxLength = 0
            Properties.EditProperties.ReadOnly = False
            Properties.DataBinding.ValueType = 'String'
            Properties.Value = '0'
            ID = 35
            ParentID = 32
            Index = 2
            Version = 1
          end
          object cxVerticalGrid1EditorRow29: TcxEditorRow
            Properties.Caption = 'Max '#1082#1088#1072#1090#1085#1086#1089#1090#1100' '#1088#1072#1079#1085#1077#1089#1077#1085#1080#1103' '#1087#1086' '#1095#1072#1089#1090#1086#1090#1077
            Properties.EditPropertiesClassName = 'TcxTextEditProperties'
            Properties.EditProperties.Alignment.Horz = taLeftJustify
            Properties.EditProperties.Alignment.Vert = taVCenter
            Properties.EditProperties.MaxLength = 0
            Properties.EditProperties.ReadOnly = False
            Properties.DataBinding.ValueType = 'String'
            Properties.Value = Null
            Visible = False
            ID = 36
            ParentID = -1
            Index = 12
            Version = 1
          end
          object cxVerticalGrid1EditorRow30: TcxEditorRow
            Properties.Caption = 'Max '#1082#1088#1072#1090#1085#1086#1089#1090#1100' '#1088#1072#1079#1085#1077#1089#1077#1085#1080#1103' '#1087#1086' '#1087#1088#1086#1089#1090#1088#1072#1085#1089#1090#1074#1091
            Properties.EditPropertiesClassName = 'TcxTextEditProperties'
            Properties.EditProperties.Alignment.Horz = taLeftJustify
            Properties.EditProperties.Alignment.Vert = taVCenter
            Properties.EditProperties.MaxLength = 0
            Properties.EditProperties.ReadOnly = False
            Properties.DataBinding.ValueType = 'String'
            Properties.Value = Null
            Visible = False
            ID = 37
            ParentID = -1
            Index = 13
            Version = 1
          end
          object cxVerticalGrid1EditorRow31: TcxEditorRow
            Properties.Caption = #1047#1072#1097#1080#1090#1072
            Properties.EditPropertiesClassName = 'TcxTextEditProperties'
            Properties.EditProperties.Alignment.Horz = taLeftJustify
            Properties.EditProperties.Alignment.Vert = taVCenter
            Properties.EditProperties.MaxLength = 0
            Properties.EditProperties.ReadOnly = False
            Properties.DataBinding.ValueType = 'String'
            Properties.Value = Null
            Visible = False
            ID = 38
            ParentID = -1
            Index = 14
            Version = 1
          end
        end
      end
    end
  end
  inherited ActionList1: TActionList
    OnUpdate = ActionList1Update
    Left = 316
  end
  inherited FormStorage1: TFormStorage
    Left = 344
  end
  inherited cxPropertiesStore: TcxPropertiesStore
    Left = 414
  end
  inherited cxLookAndFeelController1: TcxLookAndFeelController
    Left = 208
    Top = 56
  end
  object cxStyleRepository1: TcxStyleRepository
    Left = 208
    Top = 8
    PixelsPerInch = 96
    object cxStyle1: TcxStyle
      AssignedValues = [svColor]
      Color = clBtnShadow
    end
  end
end
