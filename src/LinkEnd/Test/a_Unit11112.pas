unit a_Unit11112;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, rxPlacemnt, ComCtrls, ExtCtrls, ToolWin,

  dm_Main,
  dm_Main,

  I_Options,

  fr_LinkEndType_View,


  d_Property_add,

  d_PMP_site_add,
  d_MSC_add,


  d_ClutterModel_add,

  d_Linkend_add,

 dm_Act_Antenna,

 dm_Property,



  dm_Act_Explorer,
  u_func, 
//  


  f_Log,

  u_types,

  fr_Browser,

  fr_Explorer_Container,
  dm_Explorer,


  dm_Act_Map_Engine,

  dm_Act_Project,

  //u_func_reg_lib,
  u_reg,
  u_func_msg,

  u_const,
  u_const_db,
  u_const_msg,
  u_Geo,

  u_db,

  dm_act_Property,
  dm_Act_LinkEnd,
  dm_Act_LinkEndType,

  fr_LinkEnd_view

  ;


type
  TForm16 = class(TForm)
    FormStorage1: TFormStorage;
    Panel1: TPanel;
    pc_Main: TPageControl;
    TabSheet1: TTabSheet;
    pn_Browser: TPanel;
    Splitter1: TSplitter;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    id: integer;

    Fframe_Browser : Tframe_Browser;
    Fframe_Explorer : Tframe_Explorer_Container;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form16: TForm16;

implementation

//uses dm_DB_upgrade;


{$R *.DFM}


procedure TForm16.FormCreate(Sender: TObject);
var
  iProperty: integer;
  iPROJECT: integer;
  iLinkEnd: integer;


  rec: Tdlg_LinkEnd_add_rec;
  i: integer;
begin
 // gl_Reg:=TRegStore_Create(REGISTRY_ROOT);

 // Tfrm_Log.CreateForm;

  TdmMain.Init;
  dmMain.OpenDlg;

//  IShellFactory_Init;// (dmMain);

  TdmAct_Explorer.Init;

  
{
//------------------------------------------------------
 i:=gl_DB.GetMaxID1(TBL_LinkEndType);

 with Tframe_LinkEndType_View.Create(Self) do
  begin
    View(i,'');
//    View(53912,'');

    WindowState:=wsMaximized;
    ShowModal;

  end;
//------------------------------------------------------

}


  TdmAct_LinkEnd.Init;


  IOptions_Init;


  dmAct_Map_Engine.DebugMode:=True;


  TdmAct_LinkEndType.Init;

  TdmAct_Antenna.Init;
 
{

  CreateChildForm(Tframe_Explorer_Container, Fframe_Explorer, TabSheet1);

//  Fframe_Explorer := Tframe_Explorer_Container.CreateChildForm ( TabSheet1);
  Fframe_Explorer.SetViewObjects([otLinkEnd,otLinkEndType,otLink]);
//  Fframe_Explorer.SetViewObjects([otLinkEnd,otProperty,otLinkEndType]);
  Fframe_Explorer.RegPath:='LinkEnd';
  Fframe_Explorer.Load;

  CreateChildForm(Tframe_Browser, Fframe_Browser, pn_Browser);

 // Fframe_Browser := Tframe_Browser.CreateChildForm( pn_Browser);
  Fframe_Explorer.OnNodeFocused:=Fframe_Browser.ViewObjectByPIDL;
}
 // dmAct_Map_Engine.Enabled:=False;

  TdmAct_Project.Init;

 // dmAct_Project.LoadLastProject;


  iProperty:=gl_DB.GetMaxID1(TBL_property);
//

  iPROJECT:=dmProperty.GetIntFieldValue(iProperty, FLD_PROJECT_ID);



  dmMain.ProjectID:= iPROJECT;

{
   TdmDB_upgrade.Init;
   dmDB_upgrade.Exec;
}

/////
/////


// Tdlg_ClutterModel_add.ExecDlg(0);
{
  Tdlg_Property_add.ExecDlg(0, NULL_BLPOINT);

  Tdlg_PMP_site_add.ExecDlg(0,0, NULL_BLPOINT);

  Tdlg_MSC_add.ExecDlg(0, NULL_BLPOINT);
}

////    fc



  FillChar(rec, SizeOf(rec), 0);


  rec.BLPoint:=NULL_BLPOINT;
  rec.PropertyID:=iProperty;




  Tdlg_LinkEnd_add.ExecDlg_LinkEnd (rec);


  iLinkEnd:=gl_DB.GetMaxID1(TBL_LinkEnd);


 with Tframe_LinkEnd_View.Create(Self) do
  begin
    View(iLinkEnd,'');
//    View(53912,'');

    WindowState:=wsMaximized;
    ShowModal;

  end;




end;


procedure TForm16.FormDestroy(Sender: TObject);
begin
  //Fframe_Browser.Free;
//  Fframe_Explorer.Free;

  inherited;
end;

procedure TForm16.FormClose(Sender: TObject; var Action: TCloseAction);
begin
//  FreeAndNil( Fframe_Browser);
//  FreeAndNil( Fframe_Explorer);


 // ShowMessage('procedure TForm16.FormClose(Sender: TObject; var Action: TCloseAction);');
end;


end.

