object Form16: TForm16
  Left = 781
  Top = 309
  Width = 552
  Height = 472
  Caption = 'Form16'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 544
    Height = 246
    Align = alTop
    Anchors = [akLeft, akTop, akRight, akBottom]
    BevelOuter = bvNone
    BorderWidth = 5
    TabOrder = 0
    object Splitter1: TSplitter
      Left = 185
      Top = 5
      Height = 236
    end
    object pc_Main: TPageControl
      Left = 5
      Top = 5
      Width = 180
      Height = 236
      ActivePage = TabSheet1
      Align = alLeft
      TabOrder = 0
      object TabSheet1: TTabSheet
        Caption = 'TabSheet1'
      end
    end
    object pn_Browser: TPanel
      Left = 188
      Top = 5
      Width = 351
      Height = 236
      Align = alClient
      Caption = 'pn_Browser'
      TabOrder = 1
    end
  end
  object FormStorage1: TFormStorage
    IniFileName = 'Software\Onega\'
    StoredProps.Strings = (
      'TabSheet1.Width')
    StoredValues = <>
    Left = 60
    Top = 88
  end
end
