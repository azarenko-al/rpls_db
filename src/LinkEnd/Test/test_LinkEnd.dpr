program test_LinkEnd;

uses
  a_Unit11112 in 'a_Unit11112.pas' {Form16},
  d_LinkEnd_add in '..\d_LinkEnd_add.pas' {dlg_LinkEnd_add},
  d_LinkEndType_add in '..\..\LinkEndType\d_LinkEndType_add.pas' {dlg_LinkEndType_add},
  d_LinkEndType_Band in '..\..\LinkEndType\d_LinkEndType_Band.pas' {dlg_LinkEndType_Band},
  d_LinkEndType_Mode in '..\..\LinkEndType\d_LinkEndType_Mode.pas' {dlg_LinkEndType_Mode},
  dm_act_Antenna in '..\..\Antenna\dm_act_Antenna.pas' {dmAct_Antenna: TDataModule},
  dm_act_AntType in '..\..\AntennaType\dm_Act_AntType.pas' {dmAct_AntType: TDataModule},
  dm_act_Folder in '..\..\Folder\dm_act_Folder.pas' {dmAct_Folder: TDataModule},
  dm_Act_LinkEnd in '..\dm_Act_LinkEnd.pas' {dmAct_LinkEnd: TDataModule},
  dm_act_LinkEndType in '..\..\LinkEndType\dm_act_LinkEndType.pas' {dmAct_LinkEndType: TDataModule},
  dm_act_Property in '..\..\Property\dm_act_Property.pas' {dmAct_Property: TDataModule},
  dm_Antenna_tools in '..\..\Antenna\dm_Antenna_tools.pas' {dmAntenna_tools: TDataModule},
  dm_Custom in '..\..\.common\dm_Custom.pas' {dmCustom: TDataModule},
  dm_Link_calc_SESR in '..\..\Link\dm_Link_calc_SESR.pas' {dmLink_calc_SESR: TDataModule},
  dm_LinkEnd in '..\dm_LinkEnd.pas' {dmLinkEnd: TDataModule},
  dm_LinkEnd_tools in '..\dm_LinkEnd_tools.pas' {dmLinkEnd_tools: TDataModule},
  dm_LinkEnd_View in '..\dm_LinkEnd_View.pas' {dmLinkEnd_View: TDataModule},
  dm_LinkEndType in '..\..\LinkEndType\dm_LinkEndType.pas' {dmLinkEndType: TDataModule},
  dm_LinkType in '..\..\LinkType\dm_LinkType.pas' {dmLinkType: TDataModule},
  dm_Object_base in '..\..\Common\dm_Object_base.pas' {dmObject_base: TDataModule},
  f_Custom in '..\..\.common\f_Custom.pas' {frm_Custom},
  Forms,
  fr_AntType_view in '..\..\AntennaType\fr_AntType_view.pas' {frame_AntType_View},
  fr_LinkEnd_antennas in '..\fr_LinkEnd_antennas.pas' {frame_LinkEnd_antennas},
  fr_LinkEnd_inspector in '..\fr_LinkEnd_inspector.pas' {frame_LinkEnd_inspector},
  fr_LinkEnd_view in '..\fr_LinkEnd_view.pas' {frame_LinkEnd_View},
  fr_LinkEndType_view in '..\..\LinkEndType\fr_LinkEndType_View.pas' {frame_LinkEndType_View},
  fr_Object_inspector in '..\..\Common\fr_Object_inspector.pas' {frame_Object_inspector},
  fr_View_base in '..\..\Explorer\fr_View_base.pas' {frame_View_Base},
  I_Main in '..\..\DataModules\I_Main.pas',
  dm_Explorer in '..\..\Explorer\dm_Explorer.pas' {dmExplorer: TDataModule},
  dm_ViewEngine in '..\..\Explorer\dm_ViewEngine.pas' {dmViewEngine: TDataModule},
  fr_DBInspector in '..\..\Explorer\fr_DBInspector.pas' {frame_DBInspector},
  fr_DBInspector_Container in '..\..\Explorer\fr_DBInspector_Container.pas' {frame_DBInspector_Container};

{$R *.RES}

begin
  Application.Initialize;
  Application.CreateForm(TForm16, Form16);
  Application.Run;
end.
