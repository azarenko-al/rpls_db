inherited frame_LinkEnd_View: Tframe_LinkEnd_View
  Left = 1118
  Top = 359
  Width = 409
  Height = 526
  Caption = 'frame_LinkEnd_View'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited Panel1: TPanel
    Width = 401
    TabOrder = 1
  end
  inherited pn_Caption: TPanel
    Width = 401
    TabOrder = 0
  end
  inherited pn_Main: TPanel
    Width = 401
    Height = 246
    object pn_Inspector: TPanel
      Left = -1
      Top = 0
      Width = 399
      Height = 105
      BevelOuter = bvLowered
      Caption = 'pn_Inspector'
      TabOrder = 0
      object ToolBar1: TToolBar
        Left = 1
        Top = 81
        Width = 397
        Height = 23
        Align = alBottom
        ButtonHeight = 17
        Caption = 'ToolBar1'
        EdgeBorders = [ebTop, ebBottom]
        TabOrder = 0
        object ToolButton1: TToolButton
          Left = 0
          Top = 2
          Width = 8
          Caption = 'ToolButton1'
          Style = tbsSeparator
        end
        object cb_Next_LinkEnd_AutoSave: TCheckBox
          Left = 8
          Top = 2
          Width = 369
          Height = 17
          Caption = #1072#1074#1090#1086#1084#1072#1090#1080#1095#1077#1089#1082#1080' '#1084#1077#1085#1103#1090#1100' '#1087#1072#1088#1072#1084#1077#1090#1088#1099' '#1074#1089#1090#1088#1077#1095#1085#1086#1081' '#1056#1056#1057
          Checked = True
          State = cbChecked
          TabOrder = 0
          OnClick = cb_Next_LinkEnd_AutoSaveClick
        end
      end
    end
    object cxSplitter1: TcxSplitter
      Left = 1
      Top = 166
      Width = 399
      Height = 8
      HotZoneClassName = 'TcxSimpleStyle'
      AlignSplitter = salBottom
      Control = pn_Antennas
    end
    object pn_Antennas: TPanel
      Left = 1
      Top = 174
      Width = 399
      Height = 71
      Align = alBottom
      BevelOuter = bvLowered
      Caption = 'pn_Antennas'
      TabOrder = 2
    end
  end
  inherited MainActionList: TActionList
    Left = 12
    Top = 3
    object act_Get_Mode: TAction
      Caption = #1047#1072#1076#1072#1090#1100' '#1088#1077#1078#1080#1084
      OnExecute = act_Get_ModeExecute
    end
  end
  inherited ImageList1: TImageList
    Left = 40
    Top = 3
  end
  inherited PopupMenu1: TPopupMenu
    Left = 104
    Top = 3
  end
  inherited cxLookAndFeelController1: TcxLookAndFeelController
    Left = 64
    Top = 376
  end
  inherited dxBarManager1: TdxBarManager
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    Left = 64
    Top = 432
    DockControlHeights = (
      0
      0
      25
      0)
  end
end
