object frame_LinkEnd_antennas: Tframe_LinkEnd_antennas
  Left = 970
  Top = 446
  Width = 1073
  Height = 384
  Caption = 'frame_LinkEnd_antennas'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = True
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object cxGrid1: TcxGrid
    Left = 0
    Top = 0
    Width = 1065
    Height = 152
    Align = alTop
    TabOrder = 0
    OnExit = cxGrid1Exit
    LookAndFeel.Kind = lfFlat
    object cxGrid1DBTableView1: TcxGridDBTableView
      PopupMenu = PopupMenu1
      NavigatorButtons.ConfirmDelete = False
      DataController.DataSource = ds_qry_Antennas
      DataController.KeyFieldNames = 'id'
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsCustomize.ColumnFiltering = False
      OptionsCustomize.ColumnGrouping = False
      OptionsCustomize.ColumnsQuickCustomization = True
      OptionsData.Deleting = False
      OptionsData.Inserting = False
      OptionsView.GroupByBox = False
      OptionsView.HeaderAutoHeight = True
      OptionsView.Indicator = True
      object col_ID: TcxGridDBColumn
        DataBinding.FieldName = 'ID'
        Visible = False
      end
      object col___name: TcxGridDBColumn
        DataBinding.FieldName = 'name'
        Width = 167
      end
      object col___AntType_Name: TcxGridDBColumn
        DataBinding.FieldName = 'ANTENNATYPE_NAME'
        PropertiesClassName = 'TcxButtonEditProperties'
        Properties.AutoSelect = False
        Properties.Buttons = <
          item
            Default = True
            Kind = bkEllipsis
          end>
        Properties.ReadOnly = True
        Properties.OnButtonClick = col___AntType_NamePropertiesButtonClick
        Width = 121
      end
      object col___azimuth: TcxGridDBColumn
        DataBinding.FieldName = 'azimuth'
        Width = 49
      end
      object col___height: TcxGridDBColumn
        DataBinding.FieldName = 'height'
      end
      object col___Diameter: TcxGridDBColumn
        DataBinding.FieldName = 'Diameter'
        Options.Editing = False
        Width = 55
      end
      object col___Gain: TcxGridDBColumn
        DataBinding.FieldName = 'Gain'
        Options.Editing = False
        Width = 63
      end
      object col___Polarization_STR: TcxGridDBColumn
        DataBinding.FieldName = 'Polarization_STR'
        Options.Editing = False
        Width = 82
      end
      object col___vert_width: TcxGridDBColumn
        DataBinding.FieldName = 'vert_width'
        Options.Editing = False
      end
      object col___horz_width: TcxGridDBColumn
        DataBinding.FieldName = 'horz_width'
        Options.Editing = False
      end
      object col___Pos: TcxGridDBColumn
        DataBinding.FieldName = 'pos'
        Visible = False
        Options.Editing = False
        Width = 72
      end
      object col_Mast_index: TcxGridDBColumn
        DataBinding.FieldName = 'Mast_index'
        Visible = False
      end
      object col_polarization_ratio: TcxGridDBColumn
        DataBinding.FieldName = 'polarization_ratio'
        Width = 111
      end
      object col___antenna_loss: TcxGridDBColumn
        DataBinding.FieldName = 'loss'
        Width = 81
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1DBTableView1
    end
  end
  object ActionList1: TActionList
    OnUpdate = ActionList1Update
    Left = 108
    Top = 204
    object act_Antenna_Edit: TAction
      Caption = #1048#1079#1084#1077#1085#1080#1090#1100
      OnExecute = act_Antenna_EditExecute
    end
    object act_Antenna_New: TAction
      Caption = 'act_Antenna_New'
      OnExecute = act_Antenna_EditExecute
    end
    object act_Antenna_Del: TAction
      Caption = #1059#1076#1072#1083#1080#1090#1100
      OnExecute = act_Antenna_EditExecute
    end
  end
  object PopupMenu1: TPopupMenu
    Left = 40
    Top = 204
    object actAntennaNew1: TMenuItem
      Action = act_Antenna_New
    end
    object N1: TMenuItem
      Action = act_Antenna_Edit
    end
    object N2: TMenuItem
      Caption = '-'
      Visible = False
    end
    object N3: TMenuItem
      Action = act_Antenna_Del
    end
  end
  object ds_qry_Antennas: TDataSource
    DataSet = ADOStoredProc1
    Left = 205
    Top = 252
  end
  object ADOStoredProc1: TADOStoredProc
    Connection = ADOConnection1
    CursorType = ctStatic
    LockType = ltBatchOptimistic
    AfterPost = ADOStoredProc1AfterPost
    ProcedureName = 'sp_Linkend_Antenna_Select'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 2
      end
      item
        Name = '@OBJNAME'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = 'linkend'
      end
      item
        Name = '@ID'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 2353
      end>
    Prepared = True
    Left = 203
    Top = 200
  end
  object ADOConnection1: TADOConnection
    CommandTimeout = 60
    ConnectionString = 
      'Provider=MSDASQL.1;Password=sa;Persist Security Info=True;User I' +
      'D=sa;Data Source=onega_link'
    ConnectionTimeout = 30
    LoginPrompt = False
    Provider = 'MSDASQL.1'
    Left = 376
    Top = 200
  end
  object cxStyleRepository2: TcxStyleRepository
    Left = 376
    Top = 264
    PixelsPerInch = 96
    object cxStyle_Edit: TcxStyle
    end
    object cxStyle_ReadOnly: TcxStyle
      AssignedValues = [svColor]
      Color = clInactiveBorder
    end
  end
end
