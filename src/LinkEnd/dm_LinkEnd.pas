unit dm_LinkEnd;

interface

uses
  SysUtils, Classes, DB, ADODB, Forms, Math,Variants,

  dm_Onega_DB_data,

  u_Antenna_add,

  dm_Main,

  u_types,
  u_func,
  u_classes,
  u_db,
  u_Log,
  //u_img,
  u_radio,
  u_GEO,

  u_link_const,
  u_const_db,

  dm_Object_base,

  dm_Antenna

  ;

const
  //������������: 0-�������, 1-���������
  DEF_TYPE_EQUIPMENT     = 0;
  DEF_NON_TYPE_EQUIPMENT = 1;


type
  //-------------------------------------------------------------------
  TdmLinkEndAddRec = record
  //-------------------------------------------------------------------
    BAND              : string; //old - range

    ID                : integer;
    NewName           : string;
    Property_ID       : integer;

//    Template_LINK_ID  : integer;


    //---------------------------
    // ������� �������������
    //---------------------------
    LinkEndType_ID      : integer;
    LinkEndType_Mode_ID : integer;   
    LinkEndType_Band_ID : integer;
    //BAND_NAME1        : string; //old - range
     SubBand            : string; //old - range


    //---------------------------
    //������������: 0-�������, 1-���������
    EquipmentType      : integer;

    PassiveElement_ID  : integer;

    Redundancy         : integer;  //���������� ��������������
    Loss_dB            : double;


    KNG                : double;
    Failure_period     : double;
    RESTORE_PERIOD     : double;

    //---------------------------
    // custom
    //---------------------------

    Power_dBm          : double;

    Bitrate_Mbps      : double;

//    RxFreq_MHz1         : double;
    TxFreq_MHz         : double;

    Threshold_BER_3    : double;
    Threshold_BER_6    : double;

    Kratnost_by_Space  : integer;
    Kratnost_by_Freq   : integer;

//    Space_diversity : string;  //1+0, 1+1 - ���������������� ������

    Signature_width_MHz    : double;
    Signature_height_dB    : double;

  //    STR_NOT_TYPED_SIGNATURE_WIDTH_MHz = '������ ��������� [MHz]';
 // STR_NOT_TYPED_SIGNATURE_HEIGHT_dB = '������ ��������� [dB]';



    Modulation_type    : string;

//    EQUALISER_PROFIT   : double;

    CHANNEL_NUMBER     : integer;
    //CHANNEL_NUMBER_TYPE: integer;
    CHANNEL_TYPE       : string;

    CHANNEL_SPACING    : double;

    freq_channel_count: word;

    Freq_Spacing_MHz  : double;

    // antenna -----------------
    Antenna1 : TdmAntennaAddRec;

    IsUseTemplate: boolean;

    // PMP ------------------------------

    PmpSector: record
      PmpSite_ID    :  integer;

      CalcModel_ID: integer;
      CellLayer_ID: integer;
      CalcRadius_km : Double;

      Color : integer;
    end;

    PmpTerminal: record
      PmpSector_ID  :  integer;
    end;


    Comments : string;

  end;


  // -------------------------------------------------------------------
  TdmLinkEnd = class(TdmObject_base)
    qry_Temp: TADOQuery;
    procedure DataModuleCreate(Sender: TObject);

  private
//    function GetIntFieldValue__(aID: integer; aObjName, aFieldName: string):  integer;
//    function GetDoubleFieldValue__(aID: integer; aObjName, aFieldName: string):  Double;
  //  function GetStringFieldValue__(aID: integer; aObjName, aFieldName: string):  string;


    function AddAntenna(aOwnerID: integer; aRec: TdmLinkEndAddRec; aObjectName:
        string): Integer;
    function GetBand(aID: integer): string;
// TODO: GetLinkEndTypeID1111111
//  function GetLinkEndTypeID1111111(aID: integer): integer;
  public
    function Add1(aRec: TdmLinkEndAddRec; aObjectName: string; aIDList: TIDList =
        nil): integer;

    function Del (aID: integer): boolean; override;

    function Add_Linkend(aRec: TdmLinkEndAddRec; aObjName: string; aIDList: TIDList = nil): Integer;

    function GetInfoRec (aID: integer; var aRec: TdmLinkEndAddRec): boolean;
    function GetLinkID(aID: integer): integer;
    function GetNewName(aPropertyID: integer): string;

 //   function GetMaxAntennaHeight(aID: integer): Double;
  end;



function dmLinkEnd: TdmLinkEnd;

//===================================================================
implementation {$R *.dfm}
//===================================================================

var
 FdmLinkEnd: TdmLinkEnd;

// ---------------------------------------------------------------
function dmLinkEnd: TdmLinkEnd;
// ---------------------------------------------------------------
begin
 if not Assigned(FdmLinkEnd) then
     FdmLinkEnd := TdmLinkEnd.Create(Application);

 Result := FdmLinkEnd;
end;


//-------------------------------------------------------------------
procedure TdmLinkEnd.DataModuleCreate(Sender: TObject);
//-------------------------------------------------------------------
begin
  inherited;

  TableName:=TBL_LINKEND;
  ObjectName:=OBJ_LINKEND;
end;


//-------------------------------------------------------------------
function TdmLinkEnd.GetInfoRec (aID: integer; var aRec: TdmLinkEndAddRec): boolean;
//-------------------------------------------------------------------
begin
  FillChar (aRec, SizeOf(aRec), 0);

  db_OpenTableByID (qry_Temp, TBL_LinkEnd, aID);
  Result:=not qry_Temp.IsEmpty;

  with qry_Temp, aRec do
  begin
     ID                   := FieldByName(FLD_ID).AsInteger;

     BAND                 := FieldByName(FLD_BAND).AsString;
     SubBAND             := FieldByName(FLD_SubBAND).AsString;

     Power_dBm            := FieldByName(FLD_POWER_dBm).AsFloat;
 //    Power_Max_dBm        := FieldByName(FLD_POWER_MAX).AsFloat;

     KNG                  := FieldByName(FLD_KNG).AsFloat;
  //   POWER_LOSS_dBm       := FieldByName(FLD_POWER_LOSS).AsFloat;
     Loss_dB              := FieldByName(FLD_LOSS).AsFloat;

     FAILURE_PERIOD       := FieldByName(FLD_FAILURE_PERIOD).AsFloat;
     RESTORE_PERIOD       := FieldByName(FLD_RESTORE_PERIOD).AsFloat;

     freq_channel_count   := FieldByName(FLD_freq_channel_count).AsInteger;

//    KRATNOST_BY_FREQ  : integer;
  //  KRATNOST_BY_SPACE : integer;


     KRATNOST_BY_FREQ     :=FieldByName(FLD_KRATNOST_BY_FREQ).AsInteger;
     KRATNOST_BY_SPACE    :=FieldByName(FLD_KRATNOST_BY_SPACE).AsInteger;
  //   EQUALISER_PROFIT     :=FieldByName(FLD_EQUALISER_PROFIT).AsFloat;

     LinkEndType_ID        :=FieldByName(FLD_LINKENDType_ID).AsInteger;
     EquipmentType        :=FieldByName(FLD_EQUIPMENT_TYPE).AsInteger;

//     MODE                 :=FieldByName(FLD_MODE).AsInteger;
     LinkEndType_Mode_ID  :=FieldByName(FLD_LinkEndType_Mode_ID).AsInteger;


     Bitrate_Mbps      :=FieldByName(FLD_Bitrate_Mbps).AsFloat;

   //  SPEED                :=FieldByName(FLD_SPEED).AsInteger;
    // SPEED_E1             :=FieldByName(FLD_SPEED_E1).AsString;

     CHANNEL_NUMBER       := FieldByName(FLD_CHANNEL_NUMBER).AsInteger;

     CHANNEL_TYPE  := FieldByName(FLD_CHANNEL_TYPE).Asstring;

//     CHANNEL_NUMBER_TYPE  := FieldByName(FLD_CHANNEL_NUMBER_TYPE).AsInteger;
     CHANNEL_SPACING      := FieldByName(FLD_CHANNEL_SPACING).AsFloat;
     FREQ_SPACING_MHz     := FieldByName(FLD_FREQ_SPACING).AsFloat;

     TxFreq_MHz               :=FieldByName(FLD_TX_FREQ_MHz).AsFloat;
//     RxFreq_MHz1               :=FieldByName(FLD_RX_FREQ_MHz).AsFloat;

  //   IsRepeater          :=FieldByName(FLD_IS_REPEATER).AsBoolean;

     //-----------------------------------------------------
     MODULATION_TYPE      :=FieldByName(FLD_MODULATION_TYPE).AsString;
     Signature_height_dB  :=FieldByName(FLD_SIGNATURE_HEIGHT).AsFloat;
     Signature_width_MHz  :=FieldByName(FLD_SIGNATURE_WIDTH).AsFloat;
     THRESHOLD_BER_3      :=FieldByName(FLD_THRESHOLD_BER_3).AsFloat;
     THRESHOLD_BER_6      :=FieldByName(FLD_THRESHOLD_BER_6).AsFloat;


  end;
end;



//-------------------------------------------------------------------
function TdmLinkEnd.GetLinkID(aID: integer): integer;
//-------------------------------------------------------------------
begin
  dmOnega_DB_data.OpenQuery (qry_Temp,
               'SELECT id FROM '+ TBL_link+ ' WHERE (LinkEnd1_id=:id) or (LinkEnd2_id=:id)',
               [FLD_ID, aID]);

  if qry_Temp.IsEmpty then
    Result:=0
  else
    Result:=qry_Temp.Fields[0].Value;
end;


//-------------------------------------------------------------------
function TdmLinkEnd.Del (aID: integer): boolean;
//-------------------------------------------------------------------
begin
//  Result:=gl_DB.ExecSP (sp_LinkEnd_del, [db_Par(FLD_LinkEnd_id, aID )  ]);
  Result := dmOnega_DB_data.LinkEnd_del(aID)>0;
//  Result:=True;
end;

//--------------------------------------------------------------------
function TdmLinkEnd.Add1(aRec: TdmLinkEndAddRec; aObjectName: string; aIDList:
    TIDList = nil): integer;
//--------------------------------------------------------------------
begin
  Assert (aObjectName<>'');

  Result:=Add_Linkend(aRec, aObjectName, aIDList);


end;



//--------------------------------------------------------------------
function TdmLinkEnd.AddAntenna(aOwnerID: integer; aRec: TdmLinkEndAddRec;
    aObjectName: string): Integer;
//--------------------------------------------------------------------
begin

  Assert(aOwnerID>0, 'Value <=0');
{
  case ObjNameToType (aObjectName) of
    otLinkEnd:     aRec.Antenna1_.Parent.LinkEnd_ID :=aOwnerID;
    otPMPSector:   aRec.Antenna1_.Parent.PMPSector_ID :=aOwnerID;
    otPmpTerminal: aRec.Antenna1_.Parent.PmpTerminal_ID :=aOwnerID;
  else
    raise Exception.Create('case ObjNameToType (aObjectName) of');
  end;
}

  aRec.Antenna1.Parent.LinkEnd_ID :=aOwnerID;

  Result:=dmAntenna.Add (aRec.Antenna1);

end;



//--------------------------------------------------------------------
function TdmLinkEnd.Add_Linkend(aRec: TdmLinkEndAddRec; aObjName: string;
    aIDList: TIDList = nil): Integer;
//--------------------------------------------------------------------

begin
//  if Eq(aObjName, OBJ_PMP_TERMINAL) or Eq(aObjName, OBJ_LINKEND) then
//    assert (aRec.Property_ID > 0);


  if Eq(aObjName, OBJ_PMP_SECTOR) then
    assert (aRec.PmpSector.PmpSite_ID > 0)
  else
    assert (aRec.Property_ID > 0);


//  Result:=dmOnega_DB_data.OpenStoredProc(ADOStoredProc1,
 //              'sp_LinkEnd_Add_Pmp_Terminal',

  Result:=dmOnega_DB_data.ExecStoredProc_('sp_LinkEnd_Add',
//  Result:=dmOnega_DB_data.OpenStoredProc('sp_LinkEnd_Add',

          [
          FLD_OBJNAME,          aObjName, //OBJ_LinkEnd,

          FLD_Channel_Type,     aRec.CHANNEL_TYPE,

//          FLD_Template_link_ID, aRec.Template_link_ID,

          FLD_NAME,             aRec.NewName,

          FLD_PROPERTY_ID,      aRec.Property_ID,
//          FLD_PROPERTY_ID,      IIF_NULL (aRec.Property_ID),


//               db_Par(FLD_PMP_SECTOR_ID,    aRec.PmpTerminal.PmpSector_ID,

           FLD_PASSIVE_ELEMENT_ID,  IIF_NULL(aRec.PassiveElement_ID),
//               db_Par(FLD_PASSIVE_ELEMENT_LOSS, dPassiveComponent_Loss,


           FLD_LINKENDTYPE_ID,       aRec.LinkEndType_ID,
           FLD_LinkEndType_MODE_ID,  aRec.LinkEndType_MODE_ID,


           FLD_POWER_dBm,           aRec.Power_dBm,
           FLD_TX_FREQ_MHz,         aRec.TxFreq_MHz,
           FLD_FREQ_SPACING_MHz,    aRec.Freq_Spacing_MHz,

           FLD_SIGNATURE_HEIGHT_dB, aRec.Signature_height_dB,
           FLD_SIGNATURE_WIDTH_MHz, aRec.Signature_width_MHz,
           FLD_THRESHOLD_BER_3,     aRec.THRESHOLD_BER_3,
           FLD_THRESHOLD_BER_6,     aRec.THRESHOLD_BER_6,
           FLD_KNG,                 aRec.Kng,
           FLD_Bitrate_Mbps,        aRec.Bitrate_Mbps,

           FLD_BAND,                IIF_NULL(aRec.BAND) ,

         //  db_Par(FLD_IS_REPEATER,        IIF(aRec.IsRepeater, True, null),

           FLD_COMMENTS,           IIF_NULL(aRec.Comments),

           //----------------------
           //PmpSector
           //----------------------
           FLD_PMP_SITE_ID,      IIF_NULL(aRec.PmpSector.PmpSite_ID),

           FLD_CELL_LAYER_ID,  IIF_NULL(aRec.PmpSector.CellLayer_ID) ,
           FLD_CALC_MODEL_ID,  IIF_NULL(aRec.PmpSector.CalcModel_ID) ,
           FLD_CALC_RADIUS_KM, IIF_NULL(aRec.PmpSector.CalcRadius_km),

           //----------------------
           //PmpTerminal
           //----------------------
           FLD_CONNECTED_PMP_SECTOR_ID,    IIF_NULL(aRec.PmpTerminal.PmpSector_ID)



           // -------------------------

           ]);

 // assert (Result > 0);


    if Result>0 then
    begin

      if Assigned(aIDList) then
        aIDList.AddObjName(Result, aObjName);

      if not aRec.IsUseTemplate then
        AddAntenna (Result, aRec, aObjName);

    end;

end;


//---------------------------------------------------------
function TdmLinkEnd.GetBand(aID: integer): string;
//---------------------------------------------------------
begin
  Result:=GetStringFieldValue(aID, FLD_BAND);
end;

// ---------------------------------------------------------------
function TdmLinkEnd.GetNewName(aPropertyID: integer): string;
// ---------------------------------------------------------------
var
  rec: TObject_GetNewName_Params;
begin
  FillChar(rec,SizeOf(rec),0);
  rec.ObjName := OBJ_LINKEND;
  rec.Project_ID := dmMain.ProjectID;
  rec.Property_ID := aPropertyID;

  Result := dmOnega_DB_data.Object_GetNewName_new(rec, OBJ_LINKEND, dmMain.ProjectID, aPropertyID);

end;


begin

end.



{

    function Add_PMP_terminal(aRec: TdmLinkEndAddRec; aIDList: TIDList = nil):
        Integer;
    function Add_PMP_sector(aRec: TdmLinkEndAddRec; aIDList: TIDList = nil):
        Integer;

//--------------------------------------------------------------------
function TdmLinkEnd.Add_PMP_terminal(aRec: TdmLinkEndAddRec; aIDList: TIDList =
    nil): Integer;
//--------------------------------------------------------------------

begin
//  Result:=dmOnega_DB_data.OpenStoredProc(ADOStoredProc1,
 //              'sp_LinkEnd_Add_Pmp_Terminal',

  assert (aRec.Property_ID <> 0);

  Result:=dmOnega_DB_data.ExecStoredProc_('sp_LinkEnd_Add_Pmp_Terminal',
//  Result:=dmOnega_DB_data.ExecStoredProc('sp_Pmp_Terminal_add',

              [
               FLD_OBJNAME,     OBJ_LinkEnd,


          //     FLD_Channel_Type,     aRec.CHANNEL_TYPE,

               FLD_NAME,             aRec.NewName,
               FLD_PROPERTY_ID,      IIF_NULL(aRec.Property_ID),

               FLD_PMP_SECTOR_ID,    IIF_NULL(aRec.PmpTerminal.PmpSector_ID),


               FLD_LINKENDTYPE_ID,       aRec.LinkEndType_ID,
               FLD_LinkEndType_MODE_ID,  aRec.LinkEndType_MODE_ID,

               // -------------------------
               FLD_POWER_dBm,           aRec.Power_dBm,
               FLD_TX_FREQ_MHz,         aRec.TxFreq_MHz,
               FLD_FREQ_SPACING_MHz,        aRec.Freq_Spacing_MHz,

               FLD_SIGNATURE_HEIGHT_dB,    aRec.Signature_height_dB,
               FLD_SIGNATURE_WIDTH_MHz,     aRec.Signature_width_MHz,
               FLD_THRESHOLD_BER_3,     aRec.THRESHOLD_BER_3,
               FLD_THRESHOLD_BER_6,     aRec.THRESHOLD_BER_6,
               FLD_KNG,                 aRec.Kng,
               FLD_Bitrate_Mbps,        aRec.Bitrate_Mbps,

               FLD_BAND,                IIF_NULL(aRec.BAND)

               // -------------------------

               ]);


    if Result>0 then
    begin

      if Assigned(aIDList) then
        aIDList.AddObjName(Result, OBJ_PMP_TERMINAL);

      if not aRec.IsUseTemplate then
        AddAntenna (Result, aRec, OBJ_PMP_TERMINAL);

    end;


end;




//--------------------------------------------------------------------
function TdmLinkEnd.Add_PMP_sector(aRec: TdmLinkEndAddRec; aIDList: TIDList =
    nil): Integer;
//--------------------------------------------------------------------

begin
//  Result:=dmOnega_DB_data.OpenStoredProc(ADOStoredProc1,
 //              'sp_LinkEnd_Add_Pmp_Terminal',

  Result:=dmOnega_DB_data.ExecStoredProc_ ('sp_LinkEnd_Add_Pmp_sector',
//  Result:=dmOnega_DB_data.ExecStoredProc('sp_Pmp_sector_Add',

        [

        
         FLD_OBJNAME,     OBJ_LinkEnd,

         FLD_NAME,             aRec.NewName,
     //    db_Par(FLD_PROPERTY_ID,      aRec.Property_ID),

      //   db_Par(FLD_PMP_SECTOR_ID,    aRec.PmpTerminal.PmpSector_ID),

         FLD_PMP_SITE_ID,   IIF_NULL(aRec.PmpSector.PmpSite_ID),

//             db_Par(FLD_CALC_RADIUS,   aRec.CalcRadius,

         FLD_CELL_LAYER_ID,  IIF_NULL(aRec.PmpSector.CellLayer_ID) ,
         FLD_CALC_MODEL_ID,  IIF_NULL(aRec.PmpSector.CalcModel_ID) ,
         FLD_CALC_RADIUS_KM, IIF_NULL(aRec.PmpSector.CalcRadius_km),



         FLD_LINKENDTYPE_ID,      aRec.LinkEndType_ID,
         FLD_LinkEndType_MODE_ID, aRec.LinkEndType_MODE_ID,

         // -------------------------
         FLD_TX_FREQ_MHz,         aRec.TxFreq_MHz,
         FLD_POWER_dBm,           aRec.Power_dBm,
         FLD_FREQ_SPACING_MHz,    aRec.Freq_Spacing_MHz,

         FLD_SIGNATURE_HEIGHT_dB, aRec.Signature_height_dB,
         FLD_SIGNATURE_WIDTH_MHz, aRec.Signature_width_MHz,
         FLD_THRESHOLD_BER_3,     aRec.THRESHOLD_BER_3,
         FLD_THRESHOLD_BER_6,     aRec.THRESHOLD_BER_6,
         FLD_KNG,                 aRec.Kng,
         FLD_Bitrate_Mbps,        aRec.Bitrate_Mbps,

         FLD_BAND,                IIF_NULL(aRec.BAND)

         // -------------------------

         ]);


    if Result>0 then
    begin

      if Assigned(aIDList) then
        aIDList.AddObjName(Result, OBJ_PMP_sector);

      if not aRec.IsUseTemplate then
        AddAntenna (Result, aRec, OBJ_PMP_sector);

    end;


end;


}

{
//-------------------------------------------------------------------
function TdmLinkEnd.GetIntFieldValue__(aID: integer; aObjName, aFieldName:
    string): integer;
//-------------------------------------------------------------------
var sTableName: string;
begin
  sTableName:= g_Obj.ItemByName[aObjName].TableName;
  Result:=gl_DB.GetIntFieldValue (sTableName, aFieldName, [db_Par(FLD_ID, aID) ]);
end;


//-------------------------------------------------------------------
function TdmLinkEnd.GetDoubleFieldValue__(aID: integer; aObjName, aFieldName:
    string): Double;
//-------------------------------------------------------------------
var sTableName: string;
begin
  sTableName:= g_Obj.ItemByName[aObjName].TableName;
  Result:=gl_DB.GetDoubleFieldValue (sTableName, aFieldName, [db_Par(FLD_ID, aID) ]);
end;

//-------------------------------------------------------------------
function TdmLinkEnd.GetStringFieldValue__(aID: integer; aObjName, aFieldName:
    string): string;
//-------------------------------------------------------------------
var sTableName: string;
begin
  sTableName:= g_Obj.ItemByName[aObjName].TableName;
  Result:=gl_DB.GetStringFieldValue (sTableName, aFieldName, [db_Par(FLD_ID, aID) ]);
end;

}



  {
//  Result:=Add_Linkend(aRec, aIDList);

  case ObjNameToType(aObjectName) of
    otLinkEnd :     Result:=Add_Linkend(aRec, OBJ_LINKEND, aIDList);
    otPmpSector :   Result:=Add_Linkend(aRec, OBJ_Pmp_Sector, aIDList);//  Add_PMP_Sector(aRec, aIDList);

    otPmpTerminal:  Result:=Add_Linkend(aRec, OBJ_Pmp_Terminal, aIDList); //Add_PMP_terminal(aRec, aIDList);
  end;
  }
