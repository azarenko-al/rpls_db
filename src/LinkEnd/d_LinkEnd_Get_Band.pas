unit d_LinkEnd_Get_Band;

interface

uses

  dm_Onega_DB_data,

  u_LinkEndType_str,
  u_LinkEndType_const,

  dm_Main,

  u_cx,

  u_Func_arrays,
  u_func,
  u_db,

  u_const_str,
  u_const_db,

  SysUtils, Variants, Classes, Controls, Forms,
  cxGridBandedTableView, cxGridDBBandedTableView, ExtCtrls,
  cxGridTableView, cxClasses, cxControls, cxGridCustomView,
  cxGridLevel, cxGridCustomTableView, cxGridDBTableView,
  ActnList, cxGrid, StdCtrls, rxPlacemnt, DB, ADODB, ComCtrls,

  ToolWin, DBCtrls, cxContainer, cxListBox, cxDBEdit, RxLookup, cxGraphics,
  cxLookAndFeels, cxLookAndFeelPainters, cxStyles, dxSkinsCore,
  dxSkinsDefaultPainters, dxSkinscxPCPainter, cxCustomData, cxFilter,
  cxData, cxDataStorage, cxEdit, cxDBData, cxTextEdit, cxCurrencyEdit,
  Menus, cxCheckBox;

type
  TLinkEnd_Band_rec = record
    InputType : (itAuto, itHand);


    LinkEndType_ID: Integer;
    LinkEndType_band_ID: Integer;


    LinkEndType_band_name : string;

    Channel_Type   : string;  //low,high
    Channel_number : Integer; //�����

    TxFreq_MHz: Double;
    RxFreq_MHz: Double;

  //  ChannelSpacing : double;
    Channel_Width_MHz : double;

    SubBand: string;
//    channel_number : string;

  end;



  Tdlg_LinkEnd_Get_Band = class(TForm)
    qry_Bands: TADOQuery;
    ds_Band_Channels: TDataSource;
    ds_Bands: TDataSource;
    FormStorage1: TFormStorage;
    Panel1: TPanel;
    btn_Ok: TButton;
    Button2: TButton;
    ActionList1: TActionList;
    sp_Band_Channels: TADOStoredProc;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    cxGrid2: TcxGrid;
    cxGrid2DBBandedTableView_Band_Channels: TcxGridDBBandedTableView;
    col__band_name: TcxGridDBBandedColumn;
    col__channel_low: TcxGridDBBandedColumn;
    col__TX_freq_low: TcxGridDBBandedColumn;
    col__RX_freq_low: TcxGridDBBandedColumn;
    col__tx_freq_high: TcxGridDBBandedColumn;
    col__rx_freq_high: TcxGridDBBandedColumn;
    col_Channel: TcxGridDBBandedColumn;
    col_name: TcxGridDBBandedColumn;
    cxGrid2Level1: TcxGridLevel;
    Splitter1: TSplitter;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1_Bands: TcxGridDBTableView;
    col__id: TcxGridDBColumn;
    col__Low: TcxGridDBColumn;
    col__High: TcxGridDBColumn;
    col__TxRx_Shift: TcxGridDBColumn;
    col__freq_min_low: TcxGridDBColumn;
    col__freq_max_low: TcxGridDBColumn;
    col__freq_min_high: TcxGridDBColumn;
    col__freq_max_high: TcxGridDBColumn;
    col__Channel_width: TcxGridDBColumn;
    col__Channel_count: TcxGridDBColumn;
    cxGrid1Level1: TcxGridLevel;
    pn_RadioButtons: TPanel;
    RadioGroup_Auto: TRadioGroup;
    Panel2: TPanel;
    RadioGroup_hand: TRadioGroup;
    ed_Rx: TLabeledEdit;
    ed_Tx: TLabeledEdit;
    ed_Sub_band: TLabeledEdit;
    q_ch_spacing: TADOQuery;
    ds_ch_spacing: TDataSource;
    GroupBox1: TGroupBox;
    RxDBLookupList_txrx_shift: TRxDBLookupList;
    q_txrx_shift: TADOQuery;
    ds_txrx_shift: TDataSource;
    Edit1: TEdit;
    Button1: TButton;
    Button3: TButton;
    act_Filter_DEL: TAction;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    col_Checked: TcxGridDBBandedColumn;
    col_bandwidth: TcxGridDBBandedColumn;
    cxStyleRepository1: TcxStyleRepository;
    cxStyle_red: TcxStyle;
    col_bandwidth_: TcxGridDBColumn;
    CheckBox1: TCheckBox;
    procedure ActionList1Update(Action: TBasicAction; var Handled: Boolean);
    procedure act_Filter_DELExecute(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure ed_RxChange(Sender: TObject);
    procedure ed_TxChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure qry_BandsAfterScroll(DataSet: TDataSet);
    procedure RadioGroup_AutoClick(Sender: TObject);
    procedure RadioGroup_handClick(Sender: TObject);
    procedure RxDBLookupList_txrx_shiftClick(Sender: TObject);
    procedure cxGrid2DBBandedTableView_Band_ChannelsStylesGetContentStyle(
      Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
      AItem: TcxCustomGridTableItem; out AStyle: TcxStyle);
  private
    FRegPath: string;
    FCanUpdate : Boolean;

    FParamsRec: TLinkEnd_Band_rec;

    procedure GetValues(var aRec: TLinkEnd_Band_rec);
//    procedure OpenDB_Band(aADOQuery: TADOQuery; aID: integer);
    procedure UpdateLowHigh(aIndex : Integer);

    function View(var aRec: TLinkEnd_Band_rec): Boolean;
  public
    class function ExecDlg(var aRec: TLinkEnd_Band_rec): boolean;

  end;


implementation
{$R *.dfm}


{
const
  CAPTION_NAME   = '������������';

  CAPTION_High = '������������ ������������ High';
  CAPTION_Low  = '������������ ������������ Low';

  CAPTION_CHANNEL_WIDTH     = '������ ������� ������ [MHz]'; //'������ ���������� ������ [MHz]';
  CAPTION_DUPLEX_FREQ_DELTA = '���������� ������ ������ ���/��� [MHz]';

//- ��� ����� ������ [MHz]  = ������ ������� ������ [MHz]
//- ������ ������ ���/��� [MHz] = ���������� ������ ������ ���/��� [MHz]



  CAPTION_FREQ_MIN          = 'Min ������� ������� %s [MHz]';
  CAPTION_FREQ_MAX          = 'Max ������� ������� %s [MHz]';

  STR_RX_FREQ_LOW          = 'Rx low [MHz]';
  STR_TX_FREQ_LOW          = 'Tx low [MHz]';

  STR_RX_FREQ_HIGH         = 'Rx high [MHz]';
  STR_TX_FREQ_HIGH         = 'Tx high [MHz]';

  CAPTION_CH_COUNT          = '���-�� ������';

  CAPTION_CHANNEL            = '�����';

  CAPTION_CH_LOW            = '����� Low';
  CAPTION_CH_HIGH           = '����� High';
  CAPTION_LOW_TX_FREQ       = '������� ��� [MHz]';
  CAPTION_LOW_RX_FREQ       = '������� ��� [MHz]';

  CAPTION_HIGH_TX_FREQ       = '������� ��� [MHz]';
  CAPTION_HIGH_RX_FREQ       = '������� ��� [MHz]';


  CAPTION_SPEED            = '�������� �������� [Mbit/s]';
  CAPTION_CH_SPACING       = '������ ������� [MHz]';
  CAPTION_MODULATION_TYPE  = '��� ���������';
  CAPTION_MODE             = '�����';
  CAPTION_BANDWIDTH        = '������ ������ [MHz]';

}

//--------------------------------------------------------------------
class function Tdlg_LinkEnd_Get_Band.ExecDlg(var aRec: TLinkEnd_Band_rec): boolean;
//--------------------------------------------------------------------
begin
  with Tdlg_LinkEnd_Get_Band.Create(Application) do
  begin
    View(aRec);

    Result:=ShowModal=mrOK;

    if Result then
      GetValues (aRec);

    Free;
  end;
end;


// ---------------------------------------------------------------
procedure Tdlg_LinkEnd_Get_Band.FormCreate(Sender: TObject);
// ---------------------------------------------------------------
begin
  cxGrid2.Align:=alClient;
  PageControl1.Align:=alClient;
  PageControl1.ActivePageIndex:=0;

  FRegPath :=FRegPath+ '232';

  Caption :='����� ������������.';

//  cxGrid1DBTableView1.restoreFromRegistry (FRegPath+ cxGrid1DBTableView1.Name);
//  cxGrid2DBBandedTableView1.restoreFromRegistry (FRegPath+ cxGrid2DBBandedTableView1.Name);


 cx_SetColumnCaptions(cxGrid1DBTableView1_Bands,
     [
       FLD_freq_min_low,  Format(CAPTION_FREQ_MIN, ['Low']),
       FLD_freq_max_low,  Format(CAPTION_FREQ_MAX, ['Low']),
       FLD_freq_min_high, Format(CAPTION_FREQ_MIN, ['High']),
       FLD_freq_max_high, Format(CAPTION_FREQ_MAX, ['High']),

       FLD_Channel_width, CAPTION_CHANNEL_WIDTH,
       FLD_Channel_count, CAPTION_CH_COUNT,

       FLD_TxRx_Shift,    CAPTION_DUPLEX_FREQ_DELTA,
       FLD_BANDWIDTH,     CAPTION_Bandwidth_signal

     ] );



 cx_SetColumnCaptions(cxGrid2DBBandedTableView_Band_Channels,
     [
       FLD_BANDWIDTH,     CAPTION_Bandwidth_signal,

       FLD_Channel  ,     CAPTION_CHANNEL,
       FLD_RX_Freq_LOW  , STR_RX_FREQ_LOW,
       FLD_TX_Freq_LOW  , STR_TX_FREQ_LOW,
       FLD_RX_Freq_HIGH , STR_RX_FREQ_HIGH,
       FLD_TX_Freq_HIGH , STR_TX_FREQ_HIGH

     //  SArr(FLD_TxRx_Shift, CAPTION_DUPLEX_FREQ_DELTA)

     ]);


//....- ��� ����� ������ [MHz]  = ������ ������� ������ [MHz]
//..- ������ ������ ���/��� [MHz] = ���������� ������ ������ ���/��� [MHz]


//  qry_Bands.Connection:=dmMain.ADOConnection;


end;

// ---------------------------------------------------------------
procedure Tdlg_LinkEnd_Get_Band.UpdateLowHigh(aIndex : Integer);
// ---------------------------------------------------------------
var
  b : Boolean;
begin
  b := aIndex=0;

  col__Low.Visible  := b;
  col__High.Visible := not b;

  col__freq_min_low.Visible  := b;
  col__freq_max_low.Visible  := b;

  col__freq_min_high.Visible  := not b;
  col__freq_max_high.Visible  := not b;

  cxGrid2DBBandedTableView_Band_Channels.Bands[1].Visible := b;
  cxGrid2DBBandedTableView_Band_Channels.Bands[2].Visible := not b;

end;


// ---------------------------------------------------------------
procedure Tdlg_LinkEnd_Get_Band.qry_BandsAfterScroll(DataSet: TDataSet);
// ---------------------------------------------------------------
var
  iID: Integer;
begin
  //  dmOnega_DB_data.LinkEndType_Band_Channel_Select(sp_Band_Channels, FID);

  if sp_Band_Channels.Active then
  begin
    iID := DataSet.FieldByName(FLD_ID).AsInteger;

    sp_Band_Channels.Filtered := True;
    sp_Band_Channels.Filter :=  FLD_LinkEndType_band_id + '=' + IntToStr(iID);

    btn_Ok.enabled :=  (sp_Band_Channels.RecordCount>0);

  //  g_Log.Msg('Tdlg_LinkEnd_Get_Band: qry_BandsAfterScroll');
  end;


end;



//-------------------------------------------------------------
function Tdlg_LinkEnd_Get_Band.View(var aRec: TLinkEnd_Band_rec): boolean;
//-------------------------------------------------------------
begin
  FParamsRec:=aRec;

  qry_Bands.AfterScroll :=nil;

//  dmOnega_DB_data.OpenQuery
  dmOnega_DB_data.OpenQuery (q_ch_spacing,
               'SELECT distinct ch_spacing FROM ' + TBL_LINKENDTYPE_mode +
               ' WHERE LinkEndType_id=:LinkEndType_id ',
               [FLD_LINKENDType_ID, aRec.LinkEndType_ID]);

  dmOnega_DB_data.OpenQuery (q_txrx_shift,
               'SELECT distinct txrx_shift FROM ' + TBL_LINKENDTYPE_band +
               ' WHERE LinkEndType_id=:LinkEndType_id ',
               [FLD_LINKENDType_ID, aRec.LinkEndType_ID]);

  if not q_txrx_shift.IsEmpty then
    RxDBLookupList_txrx_shift.Value:=q_txrx_shift['txrx_shift'];


 // db_View (q_ch_spacing);

//  db_OpenQuery (q_ch_spacing,
//               'SELECT distinct ch_spacing FROM ' + TBL_LINKENDTYPE_mode );

//  db_View (q_ch_spacing);


{
  db_Op enQuery (qry_Bands,
               'SELECT * FROM ' + TBL_LINKENDTYPE_BAND +
               ' WHERE LinkEndType_id=:LinkEndType_id ORDER BY low',
               [db_par(FLD_LINKENDType_ID, aRec.LinkEndType_ID)]);
}

  dmOnega_DB_data.OpenQuery (qry_Bands,
               'SELECT * FROM ' + TBL_LINKENDTYPE_BAND +
               ' WHERE LinkEndType_id=:LinkEndType_id and bandwidth=:bandwidth  ORDER BY low',
               [FLD_LINKENDType_ID, aRec.LinkEndType_ID,
                FLD_bandwidth, aRec.Channel_Width_MHz
               ]);

  qry_Bands.AfterScroll :=qry_BandsAfterScroll;


//  qry_Bands.Filtered:=true;
//  qry_Bands.Filter:='bandwidth=' + FloatToStr(aRec.Channel_Width_MHz);
//  qry_Bands.Filter:='(Channel_width='+ FloatToStr(aRec.Channel_Width_MHz) +') or (bandwidth=' + FloatToStr(aRec.Channel_Width_MHz) + ')';

//  qry_Bands.Filter:='Channel_width=' + FloatToStr(aRec.Channel_Width_MHz);


//  OpenDB_Band (qry_Bands, aRec.LinkEndType_ID);

  dmOnega_DB_data.LinkEndType_Band_Channel_Select(sp_Band_Channels, aRec.LinkEndType_ID);
 // qry_Bands.First;


{
  db_SetFieldCaptions(sp_Band_Channels,
   [

   SArr(FLD_Channel  ,     CAPTION_CHANNEL),
   SArr(FLD_RX_Freq_LOW  , STR_RX_FREQ_LOW),
   SArr(FLD_TX_Freq_LOW  , STR_TX_FREQ_LOW),
   SArr(FLD_RX_Freq_HIGH , STR_RX_FREQ_HIGH),
   SArr(FLD_TX_Freq_HIGH , STR_TX_FREQ_HIGH)

 //  SArr(FLD_TxRx_Shift, CAPTION_DUPLEX_FREQ_DELTA)

   ]);
  }


  qry_Bands.Locate(FLD_ID, aRec.LinkEndType_band_ID, []);

  sp_Band_Channels.Filter :=  FLD_LinkEndType_band_id + '=' + IntToStr(qry_Bands.FieldByName(FLD_ID).AsInteger);
  sp_Band_Channels.Filtered := True;


 // qry_Band_Channels.Filtered := True;
  if sp_Band_Channels.RecordCount>0 then
    sp_Band_Channels.Locate(FLD_CHANNEL, aRec.Channel_number, []);


  if Eq(aRec.Channel_Type,'low') then
    RadioGroup_Auto.ItemIndex :=0
  else
    RadioGroup_Auto.ItemIndex :=1;

  //----------------------------------
  if Eq(aRec.Channel_Type,'low') then
    RadioGroup_Hand.ItemIndex :=0
  else
    RadioGroup_Hand.ItemIndex :=1;

  ed_Rx.Text:=FloatToStr(aRec.RxFreq_MHz);
  ed_Tx.Text:=FloatToStr(aRec.TxFreq_MHz);

  ed_Sub_band.Text:=aRec.SubBand;

//  RxDBLookupList1.Value:=FloatToStr(aRec.Channel_Width_MHz);

//  ed_channel_number.text := IntToStr( aRec.channel_number );

//            aRec.Channel_Width_MHz:=AsFloat(RxDBLookupList1.Value);


//  rec_band.TxFreq_MHz  := GetIntFieldValue (FLD_TX_FREQ_MHz);
//  rec_band.RxFreq_MHz  := GetIntFieldValue (FLD_RX_FREQ_MHz);

  FCanUpdate:=True;

  btn_Ok.enabled :=  (sp_Band_Channels.RecordCount>0);


end;


//--------------------------------------------------------------------
procedure Tdlg_LinkEnd_Get_Band.GetValues(var aRec: TLinkEnd_Band_rec);
//--------------------------------------------------------------------
begin
  case PageControl1.ActivePageIndex of
    //----------
    //auto
    //----------
    0: begin
          aRec.InputType:=itAuto;

          case RadioGroup_Auto.ItemIndex of
            0: begin
                 aRec.Channel_Type := 'low';
                 aRec.TxFreq_MHz := sp_Band_Channels.FieldByName(FLD_TX_Freq_LOW).AsFloat;
                 aRec.RxFreq_MHz := sp_Band_Channels.FieldByName(FLD_RX_Freq_LOW).AsFloat;
                 aRec.LinkEndType_band_name := qry_Bands.FieldByName(FLD_low).AsString;

               end;

            1: begin
                 aRec.Channel_Type := 'high';
                 aRec.TxFreq_MHz := sp_Band_Channels.FieldByName(FLD_TX_Freq_HIGH).AsFloat;
                 aRec.RxFreq_MHz := sp_Band_Channels.FieldByName(FLD_RX_Freq_HIGH).AsFloat;
                 aRec.LinkEndType_band_name := qry_Bands.FieldByName(FLD_high).AsString;

               end;
          end;

          aRec.Channel_number       := sp_Band_Channels.FieldByName(FLD_Channel).AsInteger;

          aRec.LinkEndType_BAND_ID  := qry_Bands.FieldByName(FLD_ID).AsInteger;
          aRec.Channel_Width_MHz    := qry_Bands.FieldByName(FLD_CHANNEL_WIDTH).AsFloat;
       end;

    //----------
    //auto
    //----------
    1: begin
          aRec.InputType:=itHand;

          case RadioGroup_hand.ItemIndex of
            0: begin
                 aRec.Channel_Type := 'low';

                 aRec.TxFreq_MHz := AsFloat(ed_Tx.Text) ;
                 aRec.RxFreq_MHz := AsFloat(ed_Rx.Text) ;
              //   aRec.LinkEndType_band_name := qry_Bands.FieldByName(FLD_low).AsString;

               end;

            1: begin
                 aRec.Channel_Type := 'high';

                 aRec.TxFreq_MHz := AsFloat(ed_Tx.Text) ;
                 aRec.RxFreq_MHz := AsFloat(ed_Rx.Text) ;

              //   aRec.TxFreq_MHz := sp_Band_Channels.FieldByName(FLD_TX_Freq_HIGH).AsFloat;
              //   aRec.RxFreq_MHz := sp_Band_Channels.FieldByName(FLD_RX_Freq_HIGH).AsFloat;
              //   aRec.LinkEndType_band_name := qry_Bands.FieldByName(FLD_high).AsString;

               end;
          end;

          aRec.SubBand:=Trim(ed_Sub_band.Text);
       //   aRec.Channel_Width_MHz:=AsFloat(RxDBLookupList1.Value);

//          aRec.channel_number:=AsInteger(ed_channel_number.Text);
          

      //    aRec.Channel_number       := sp_Band_Channels.FieldByName(FLD_Channel).AsInteger;

        //  aRec.LinkEndType_BAND_ID  := qry_Bands.FieldByName(FLD_ID).AsInteger;
        //  aRec.Channel_Width_MHz    := qry_Bands.FieldByName(FLD_CHANNEL_WIDTH).AsFloat;
       end;


  end;

//  aRec.ChannelSpacing := qry_Band_Channels.FieldByName(FLD_CHANNEL_SPACING).AsFloat;


end;


procedure Tdlg_LinkEnd_Get_Band.RadioGroup_AutoClick(Sender: TObject);
begin
  UpdateLowHigh (RadioGroup_Auto.ItemIndex);
end;


procedure Tdlg_LinkEnd_Get_Band.ActionList1Update(Action: TBasicAction; var Handled:
    Boolean);
begin
//  btn_Ok.enabled :=
 //   cxGrid2DBBandedTableView_Band_Channels.DataController.RecordCount > 0;

//  btn_Ok.enabled := //(qry_Bands.RecordCount>0) and
//                    (sp_Band_Channels.RecordCount>0);

end;

//----------------------------------------------------------------
procedure Tdlg_LinkEnd_Get_Band.act_Filter_DELExecute(Sender: TObject);
//----------------------------------------------------------------
begin
  act_Filter_DEL.Checked:=not act_Filter_DEL.Checked;

  if act_Filter_DEL.Checked then
  db_OpenQuery_ (qry_Bands,
               'SELECT * FROM ' + TBL_LINKENDTYPE_BAND +
               ' WHERE LinkEndType_id=:LinkEndType_id ORDER BY low',
//               ' WHERE LinkEndType_id=:LinkEndType_id and bandwidth=:bandwidth  ORDER BY low',
               [FLD_LINKENDType_ID, FParamsRec.LinkEndType_ID
  //             FLD_bandwidth, aRec.Channel_Width_MHz
               ])

  else
    dmOnega_DB_data.OpenQuery (qry_Bands,
                 'SELECT * FROM ' + TBL_LINKENDTYPE_BAND +
                 ' WHERE LinkEndType_id=:LinkEndType_id and bandwidth=:bandwidth  ORDER BY low',
                 [FLD_LINKENDType_ID, FParamsRec.LinkEndType_ID,
                  FLD_bandwidth,      FParamsRec.Channel_Width_MHz
                 ]);



//  qry_Bands.Filtered:=False;
//  qry_Bands.First;
end;

procedure Tdlg_LinkEnd_Get_Band.Button1Click(Sender: TObject);
begin
  qry_Bands.Filter:=Edit1.Text;
//  qry_Bands.Filtered:=true;

end;

procedure Tdlg_LinkEnd_Get_Band.Button3Click(Sender: TObject);
begin
  qry_Bands.Filtered:=false;
end;

// ---------------------------------------------------------------
procedure Tdlg_LinkEnd_Get_Band.ed_TxChange(Sender: TObject);
// ---------------------------------------------------------------
var
  e: Double;
  eShift_MHz: Double;
begin
  if not FCanUpdate then
    Exit;

  FCanUpdate:=False;

  eShift_MHz:=AsFloat(RxDBLookupList_txrx_shift.Value);
  e:=AsFloat(ed_Tx.Text) + eShift_MHz * IIF(RadioGroup_hand.ItemIndex=0,+1,-1);

  ed_Rx.Text:=FloatToStr(e);

  FCanUpdate:=True;

end;

// ---------------------------------------------------------------
procedure Tdlg_LinkEnd_Get_Band.ed_RxChange(Sender: TObject);
// ---------------------------------------------------------------
var
  e: Double;
  eShift_MHz: Double;
begin
  if not FCanUpdate then
    Exit;

  FCanUpdate:=False;

  eShift_MHz:=AsFloat(RxDBLookupList_txrx_shift.Value);
  e:=AsFloat(ed_Rx.Text) + eShift_MHz * IIF(RadioGroup_hand.ItemIndex=0,-1,+1);
  ed_tx.Text:=FloatToStr(e);

  FCanUpdate:=True;
end;

procedure Tdlg_LinkEnd_Get_Band.RadioGroup_handClick(Sender: TObject);
begin
  if not FCanUpdate then
    Exit;

  ed_TxChange(nil);
end;


procedure Tdlg_LinkEnd_Get_Band.RxDBLookupList_txrx_shiftClick(Sender: TObject);
begin
  if not FCanUpdate then
    Exit;

  ed_TxChange(nil);
end;


procedure Tdlg_LinkEnd_Get_Band.cxGrid2DBBandedTableView_Band_ChannelsStylesGetContentStyle(
  Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
  AItem: TcxCustomGridTableItem; out AStyle: TcxStyle);
begin
  if aRecord.Values[col_Checked.Index]=True then
    AStyle:=cxStyle_red;
end;


end.

