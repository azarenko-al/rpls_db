unit u_LinkEnd_Add;

interface

uses Variants,

  dm_Onega_DB_data,

  u_const_db,

  u_func,
  u_db
  ;


type
  //-------------------------------------------------------------------
  TLinkEndAdd = class
  //-------------------------------------------------------------------

    ObjName : string;

    NewName            : string;
    Property_ID        : integer;

    //-------------------------
    // LinkEndType
    //-------------------------

    LinkEndType_ID      : integer;
    LinkEndType_Mode_ID : integer;

    //-------------------------
    // custom
    //-------------------------
    BAND                : string; //old - range

    TxFreq_MHz          : double;
    Power_dBm           : Double;

    Threshold_BER_3     : double;
    Threshold_BER_6     : double;

    Loss             : double;

//    EQUALISER_PROFIT    : double;

    Freq_Spacing_MHz    : double;

    Signature_width_MHz : double; //'������ ��������� [MHz]';
    Signature_height_dB : double; //'������ ��������� [dB]';

    KNG                 : double;

    Bitrate_Mbps        : Integer;

  //    STR_NOT_TYPED_SIGNATURE_WIDTH_MHz = '������ ��������� [MHz]';
 // STR_NOT_TYPED_SIGNATURE_HEIGHT_dB = '������ ��������� [dB]';

    // ------------------------------

    Comments : string;

  public
    function Add: integer;
  end;



implementation

// ---------------------------------------------------------------
function TLinkEndAdd.Add: integer;
// ---------------------------------------------------------------
begin
  Assert(NewName<>'');
//  Assert(Project_ID>0,  ' ProjectID <=0');
  Assert(Property_ID>0, ' Property_ID <=0');

//  Assert(Band<>'', ' Band <>''''');

  Assert(Assigned(dmOnega_DB_data), 'Value not assigned');

  Result:=dmOnega_DB_data.OpenStoredProc( dmOnega_DB_data.ADOStoredProc1,
      SP_LinkEnd_ADD,

//  Result:=db_ExecStoredProc(aADOConnection, SP_LinkEnd_ADD,

      [
     //  db_Par(FLD_OBJNAME,        ObjName),


       FLD_NAME,           NewName,
       FLD_PROPERTY_ID,    Property_ID,
     //  db_Par(FLD_FOLDER_ID,    IIF_NULL(aRec.FolderID)),

//                                 db_Par(FLD_Next_LinkEnd_ID, IIF_NULL(aRec.Next_LinkEnd_ID)),

//                                 db_Par(FLD_Space_diversity, IIF(aRec.Kra, True, null)),


       FLD_LinkEndType_ID,      IIF_NULL(LinkEndType_ID),
       FLD_LinkEndType_MODE_ID, IIF_NULL(LinkEndType_MODE_ID),

       FLD_COMMENTS,        IIF_NULL(Comments),


       //-------------------------
       // custom
       //-------------------------
        FLD_Band,                 IIF_NULL(Band),

        FLD_Power_dBm,            IIF_NULL(Power_dBm),
        FLD_Tx_Freq_MHz,          IIF_NULL(TxFreq_MHz),

        FLD_Threshold_BER_3,      IIF_NULL(Threshold_BER_3),
        FLD_Threshold_BER_6,      IIF_NULL(Threshold_BER_6),

        FLD_Loss,              IIF_NULL(Loss),

//        db_Par(FLD_EQUALISER_PROFIT,     IIF_NULL(EQUALISER_PROFIT)),

        FLD_Freq_Spacing_MHz,     IIF_NULL(Freq_Spacing_MHz),

        FLD_Signature_width_MHz,  IIF_NULL(Signature_width_MHz),
        FLD_Signature_height_dB,  IIF_NULL(Signature_height_dB),

        FLD_KNG,                  IIF_NULL(KNG),
        FLD_Bitrate_Mbps,         IIF_NULL(Bitrate_Mbps)

        ]);

  Assert(result>0, 'SP_LinkEnd_ADD.result <=0');

end;




end.
