unit u_msg;

interface
uses
   Classes,Windows,SysUtils,Forms,

   u_classes,
   u_func,
   u_geo
   ;


type

  TEventParamRec = record
    Name:  string;  //ParamName
    Value: Variant;
    Pointer: TObject;
    BLPoint: TBLPoint;
  end;

  TEventParam = class (TCollectionItem)
  public
    Name: string;
    Value: Variant;
    Pointer: TObject;
    BLPoint: TBLPoint;
  end;

  TEventParamList = class (TCollection)
  private
    function GetItem (Index: integer): TEventParam;
  public

    IDList: TIDList;
    LParam,WParam: integer;

    constructor Create;

    procedure AddItem (aRec: TEventParamRec);
{
    function ExtractBLPoint: TBLPoint;
    function ExtractBLVector (): TBLVector;
    function ExtractBLRect (): TBLRect;
}
    function FindItem (aName: string): TEventParam;
    function ValueByName (aName: string): Variant;
    function IntByName (aName: string): integer;
    function PointerByName (aName: string): Pointer;

    property Items[Index: integer]: TEventParam read GetItem; default;
  end;



  procedure PostWMsg (Msg: integer; WParam: integer=0; LParam: integer=0); overload;  

{
  procedure PostMsgPointer (Msg: integer; WParam,LParam: Pointer); overload;
}
//  procedure PostMsgDouble  (Msg: integer; WParam: double;  LParam: double);  overload;

///////  procedure PostEvent (aMsg: integer; aBLPoints: array of TBLPoint); overload;


{  procedure PostBLVector (aMsg: integer; aBLVector: TBLVector);
  procedure PostBLRect   (aMsg: integer; aBLRect: TBLRect);
  procedure PostBLPoint (aMsg: integer; aBLPoint: TBLPoint);
}

  procedure PostEvent (aMsg: integer; aParams: array of TEventParamRec); overload;
  procedure PostEvent(aMsg: integer; aParamName: string; aParamValue: string); overload;
  procedure PostEvent (aMsg: integer; aParamName: string; aParamValue: Pointer); overload;
  procedure PostEvent (aMsg: integer); overload;

  procedure PostEvent (aMsg: integer; WParam: integer; LParam: integer=0); overload;


  function app_Par (aName: string; aValue: Variant): TEventParamRec; overload;
  function app_Par (aName: string; aValue: Pointer): TEventParamRec; overload;


{  function RegisterProc(aProc: TOnActionExecuteEvent; aName: string): integer;
  procedure UnRegisterProc (aIndex: integer);
}

const
  BLANK_PARAM: TEventParamRec =
   (Name:'');


//  PAR_PROFILE = 'profile';
{
  PAR_LAT  = 'lat';
  PAR_LON  = 'lon';
  PAR_LAT1 = 'lat1';
  PAR_LON1 = 'lon1';
  PAR_LAT2 = 'lat2';
  PAR_LON2 = 'lon2';
}
  PAR_STEP = 'step';
//  PAR_BLVector = 'BLVector';

 { PAR_BLRECT = 'BLRECT';
  PAR_BLPOINT_ = 'BLPOINT';
}

  PAR_DESKTOP_ID      = 'DESKTOP_ID';
  PAR_FOLDER_ID       = 'FOLDER_ID';
  PAR_CLUTTERMODEL_ID = 'CLUTTERMODEL_ID';


  PAR_OWNER       = 'OWNER';
  PAR_MODULE_NAME = 'MODULE_NAME';
  PAR_OBJECT_NAME = 'ObjectName';
  PAR_Query       = 'Query';
  PAR_SENDER      = 'Sender';
  PAR_FORM        = 'Form';
  PAR_ID          = 'ID';
  PAR_GUID        = 'GUID';
  PAR_PIDL_LIST   = 'PIDL_List';
  PAR_name        = 'name';
  PAR_XML         = 'XML';
  PAR_SQL         = 'SQL';
  PAR_FILENAME    = 'filename';
  PAR_MENU_ITEM   = 'MENU_ITEM';
  PAR_MemDataList = 'MemDataList';
  PAR_POPUP_MENU  = 'POPUP_MENU';
  PAR_type        = 'type';
  PAR_IS_FOLDER   = 'IS_FOLDER';
//  PAR_RADIUS      = 'RADIUS';

//  PAR_BLPOINT_ARR = 'BLPOINT_ARR';

//  PAR_BLPOINT_LIST = 'BLPOINT_LIST';

{  PAR_BLPOINT1_    = 'BLPOINT1';
  PAR_BLPOINT2_    = 'BLPOINT2';
}
  PAR_ObjectType  = 'ObjectType';
 // PAR_ListID      = 'ListID';
//  PAR_dxDBGrid    = 'dxDBGrid';
//  PAR_PIDL        = 'PIDL';

  PAR_COLOR       = 'COLOR';

  PAR_ACTION       = 'ACTION';
//  PAR_ADD_NEIGHBOR = 'ADD_NEIGHBOR';
 // PAR_SELECT_CELL  = 'SELECT_CELL';

  PAR_WIDTH       = 'WIDTH';
  PAR_TEXT        = 'TEXT';
  PAR_STYLE       = 'STYLE';


type
  // �������� ����������, ������� ������ ������������� �����, �����������
  // � ���������� �� ���������� ������� ������
  IMessageHandler = interface
    ['{DB6CC915-A762-4C09-B12A-053D3D2E765C}']
    procedure GetMessage (aMsg: integer;
                          aParams: TEventParamList; var aHandled: Boolean);
  end;

type
  TOnActionExecuteEvent = procedure (aMsg: integer;
          aParams: TEventParamList; var aHandled: Boolean) of object;


  TEventManager = class(TStringList)
  private
    FProcArr: array [0..1000] of record
                 Proc: TOnActionExecuteEvent;
                 Name: string;
              end;
    FCount: integer;

  public
    function  RegisterProc  (aProc: TOnActionExecuteEvent; aName: string): integer;
    procedure UnRegisterProc(aIndex: integer; aProc: TOnActionExecuteEvent);

    procedure PostEvent (aMsg: integer;
                         aParams: array of TEventParamRec;
                         aWParam: integer=0;
                         aLParam: integer=0);
  end;


var
  g_EventManager: TEventManager;




//===================================================================
implementation
//===================================================================


  function DoubleAsInteger (Value: double): integer; forward;


function app_Par (aName: string; aValue: Variant): TEventParamRec;
begin
  Result.Name:=aName;
  Result.Value:=aValue;
end;

function app_Par (aName: string; aValue: Pointer): TEventParamRec;
begin
  Result.Name:=aName;
  Result.Pointer:=aValue;
end;

procedure PostEvent (aMsg: integer); overload;
begin
  PostEvent (aMsg, [BLANK_PARAM]);
end;

procedure PostEvent (aMsg: integer; aParamName: string; aParamValue: string);
begin
  PostEvent (aMsg, [app_Par(aParamName,aParamValue)]);
end;

procedure PostEvent (aMsg: integer; aParamName: string; aParamValue: Pointer);
begin
  PostEvent (aMsg, [app_Par(aParamName,aParamValue)]);
end;


//-------------------------------------------------------
procedure PostEvent (aMsg: integer; aParams: array of TEventParamRec);
//-------------------------------------------------------
begin
  g_EventManager.PostEvent (aMsg, aParams);
end;


procedure PostEvent (aMsg: integer; WParam: integer;
                                    LParam: integer=0);
begin
  g_EventManager.PostEvent (aMsg, [BLANK_PARAM], WParam, LParam);
end;


procedure PostMsgPointer (Msg: integer; WParam,LParam: Pointer); overload;
begin
  PostMessage (Application.Handle, Msg, Integer(WParam), Integer(LParam));
end;


procedure PostWMsg (Msg: integer; WParam: integer=0; LParam: integer=0);
begin
  PostMessage (Application.Handle, Msg, WParam, LParam);
end;


procedure PostMsgDouble (Msg: integer; WParam: double; LParam: double);
begin
  PostMessage (Application.Handle, Msg, DoubleAsInteger(WParam), DoubleAsInteger(LParam));
end;


function DoubleAsInteger (Value: double): integer;
var
  rec: record case v:byte of  0:(Sing:single); 1:(Int:integer); end;
begin
  rec.Sing:=Value;  Result:=rec.Int;
end;

//==================================================================
// TEventParamList
//==================================================================

function TEventParamList.FindItem(aName: string): TEventParam;
var i:integer;
begin
  for i:=0 to Count-1 do
    if Eq(Items[i].Name, aName) then begin Result:=Items[i]; Exit; end;

  Result:=nil;
  raise Exception.Create('Result:=Null; param:'+aName);
end;

function TEventParamList.IntByName(aName: string): integer;
begin
  Result:=AsInteger(ValueByName(aName));
end;


function TEventParamList.ValueByName (aName: string): Variant;
var obj: TEventParam;
begin
  obj:=FindItem(aName);
  if Assigned(obj) then Result:=obj.Value
                   else
                     raise Exception.Create('Result:=Null; param:'+aName);
//                   Result:=Null;
end;

function TEventParamList.PointerByName (aName: string): Pointer;
var obj: TEventParam;
begin
  obj:=FindItem(aName);
  if Assigned(obj) then Result:=obj.Pointer
                   else Result:=nil;
end;


procedure TEventParamList.AddItem (aRec: TEventParamRec);
var obj: TEventParam;
begin
  obj:=TEventParam.Create (Self);
  obj.Name:=aRec.Name;
  obj.Value:=aRec.Value;
  obj.Pointer:=aRec.Pointer;

end;

function TEventParamList.GetItem (Index: integer): TEventParam;
begin
  Result:=TEventParam (inherited Items[Index])
end;


constructor TEventParamList.Create;
begin
  inherited Create (TEventParam);

  IDList:=TIDList.Create;
end;


//-------------------------------------------------------------------
procedure TEventManager.PostEvent (aMsg: integer;
                                   aParams: array of TEventParamRec;
                                   aWParam: integer=0;
                                   aLParam: integer=0);
//-------------------------------------------------------------------
var i: integer;
    pProc,pProc1: TOnActionExecuteEvent;
    p: Pointer;
    oEventParams: TEventParamList;
    bHandled: Boolean;

    Routine: TMethod;

    intf: IMessageHandler;
begin
 //Assert(App<>nil);

  oEventParams:=TEventParamList.Create;
  oEventParams.WParam:=aWParam;
  oEventParams.LParam:=aLParam;
  bHandled:=False;

  for i:=0 to High(aParams) do
    oEventParams.AddItem (aParams[i]);



//  try
  // ������� �� ���� ���� ������, ������� ��������� � ���������� �� ����������
//  if Assigned(App) then
    for i:=Application.ComponentCount-1 downto 0 do
      if Application.Components[i].GetInterface(IMessageHandler, intf) then
      begin
        intf.GetMessage (aMsg, oEventParams, bHandled);
        if bHandled then Break;
      end;
//  except end;




  if not bHandled then
    try
      for i:=0 to FCount-1 do
      begin

     {   Routine.Data := Pointer(Objects[i]) ;
        Routine.Code:=Objects[i].MethodAddress('');
}

      //  p:=Pointer(Objects[i]);
      //  pProc1:=TOnActionExecuteEvent(Routine);

        pProc:=FProcArr[i].Proc;

        if Assigned(pProc) then
          try
            pProc (aMsg, oEventParams, bHandled);

             if bHandled then Break;
          except end;
      end;

    except end;

  oEventParams.Free;
end;



function TEventManager.RegisterProc(aProc: TOnActionExecuteEvent; aName: string): integer;
var
  routine: TMethod;
begin

{  aProc.MethodAddress()

  AddObject(aName, TObject(@aProc));

}
  Result:=FCount;
  FProcArr[FCount].Proc:=aProc;
  FProcArr[FCount].Name:=aName;
  FCount:=FCount+1;
end;



procedure TEventManager.UnRegisterProc(aIndex: integer; aProc:
    TOnActionExecuteEvent);
var
  i: integer;
begin
{  i:=IndexOfObject(TObject(@FProcArr[aIndex].Proc));
  if i>=0 then
    Delete(i);
}

 // IndexOfObject()

  FProcArr[aIndex].Proc:=nil;
  FProcArr[aIndex].Name:='';
end;          


initialization
  g_EventManager:=TEventManager.Create;

finalization
  FreeAndNil(g_EventManager);
end.

