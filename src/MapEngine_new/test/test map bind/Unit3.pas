unit Unit3;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, OleCtrls, MapXLib_TLB, StdCtrls, DB, ADODB, Grids, DBGrids,

  u_func,

  u_mapx
  ;

type
  TForm3 = class(TForm)
    Map1: TMap;
    ADOConnection1: TADOConnection;
    qry_Temp: TADOQuery;
    Button1: TButton;
    DataSource1: TDataSource;
    DBGrid1: TDBGrid;
    Button2: TButton;
    Button3: TButton;
    Button4: TButton;
    Button5: TButton;
    Button6: TButton;
    Button7: TButton;
    Button8: TButton;
    Button9: TButton;
    procedure FormCreate(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure Button6Click(Sender: TObject);
//    procedure Button7Click(Sender: TObject);
//    procedure Button9Click(Sender: TObject);
  private
      FMap: TMap;
    procedure OpenData;
  public
    function CreateLayerFromDb(aDataSet: TCustomAdoDataSet; aFileName: string):
        CMapxLayer;
    { Public declarations }
  end;

var
  Form3: TForm3;

implementation

{$R *.dfm}

const
  DEF_FILENAME = 'd:\1111_prop.Tab';


procedure TForm3.FormCreate(Sender: TObject);
begin
  qry_Temp.Close;
  qry_Temp.SQL.Text := ' select top 2000 id, lat,lon, name, address, (name+char(10)+address) as caption from property';
  qry_Temp.Open;

end;

procedure TForm3.Button1Click(Sender: TObject);
var
  s: string;
  vLayer: CMapXLayer;
begin
 // vLayer:= mapx_GetLayerByFileName(Map1, 'd:\1111_prop.Tab');

//  vLayer:=Map1.Layers. Item('Temp_bind_layer');


  Map1.Layers.RemoveAll;

  vLayer:=Map1.Layers.Add(DEF_FILENAME, EmptyParam);
  s:=vLayer.Name;

  vLayer.Name := 'Temp_bind_layer';

end;

procedure TForm3.Button2Click(Sender: TObject);
begin
  Map1.Layers.RemoveAll;


  qry_Temp.Open;


  FMap:=TMap.Create(Self);

 //   SaveTime;

  CreateLayerFromDb(qry_Temp, DEF_FILENAME);

 // ShowTime;


  FreeAndNil(FMap);

end;



procedure TForm3.Button3Click(Sender: TObject);
const
  FLD_ID = 'ID';
 // FLD_NAME ='NAME';
//  FLD_GEONAME ='GEONAME';

   FLD_caption = 'caption';

var
  I: Integer;

  vBindLayerObject: CMapXBindLayer;
  vDataSet: CMapxDataset;
  VLayer: CMapXLayer;
//  vFld: CMapXField;

  vFields: CMapXFields;
  //vLayerInfoObject: CMapXLayerInfo;
  iIndex: Integer;

//  vFields: CMapXFields;
  s: string;

begin

  Map1.Layers.RemoveAll;
  vLayer:=Map1.Layers.Add(DEF_FILENAME, EmptyParam);

  OpenData;


  vFields := CoFields.Create;
  vFields.Add(FLD_ID, FLD_ID, miAggregationIndividual, miTypeInteger);
  vFields.Add('caption', 'caption', miAggregationIndividual, miTypeString);


  vDataSet := Map1.DataSets.Add(miDatasetADO, qry_Temp.Recordset, EmptyParam,
        FLD_ID,
        EmptyParam,
        vLayer.Name,
        vFields,
        EmptyParam);

  VLayer:=Map1.Layers.Item(1);


  VLayer.LabelProperties.Dataset   := vDataSet;
  VLayer.LabelProperties.DataField := vDataSet.Fields.Item('caption');

  VLayer.LabelProperties.Visible := True;
  VLayer.AutoLabel := True;

//  VLayer.LabelProperties.DataField := ds.Fields.Item('address');
//  VLayer.LabelProperties.DataField := ds.Fields.Item('name');


end;


procedure TForm3.Button4Click(Sender: TObject);
begin
  map1.PropertyPage;
end;


procedure TForm3.Button5Click(Sender: TObject);
var
  VLayer: CMapXLayer;

begin
  VLayer:=Map1.Layers.Item(1);
  //VLayer.LabelProperties.Visible := True;

  VLayer.AutoLabel := True;

 // VLayer.LabelProperties.DataField  := 'geoname';
// map1.Refresh;
//  VLayer.Refresh;

end;


procedure TForm3.Button6Click(Sender: TObject);
begin
  OpenData;
end;




//------------------------------------------------------------------------------
function TForm3.CreateLayerFromDb(aDataSet: TCustomAdoDataSet; aFileName:
    string): CMapxLayer;
//------------------------------------------------------------------------------
const
  FLD_ID  = 'ID';

var
 // sTempFileName: string;
  vBindLayerObject: CMapXBindLayer;
  vDataSet: CMapxDataset;
  VLayer: CMapXLayer;
  vFld: CMapXField;

  vFlds: CMapXFields;
  vLayerInfoObject: CMapXLayerInfo;
  iIndex, i: Integer;

 // BLRect: TBLRect;

begin
 // sTempFileName:= 'C:\___1.tab';

 // sTempFileName:=aFileName;

  DeleteFile(aFileName);


  vBindLayerObject := CoBindLayer.Create;
  vBindLayerObject.LayerName := 'Temp_bind_layer';
  vBindLayerObject.RefColumn1 := 'lon';
  vBindLayerObject.RefColumn2 := 'Lat';
  vBindLayerObject.LayerType := miBindLayerTypeXY;
  vBindLayerObject.FileSpec  := aFileName;


  SaveTime;

  vDataSet := FMap.DataSets.Add(miDatasetADO, aDataSet.Recordset, EmptyParam,
                     FLD_ID,
                     EmptyParam,
                     vBindLayerObject,
                     EmptyParam,
                     EmptyParam);

  FMap.DataSets.RemoveAll;


  ShowTime;

end;

procedure TForm3.OpenData;
begin
  qry_Temp.Close;
  qry_Temp.SQL.Text := ' select top 2 id, lat,lon, name, address, '+
                      '(name+char(10)+address) as caption '+
                      ' from property';
  qry_Temp.Open;
end;

end.




