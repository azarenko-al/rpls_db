object dmMapEngine_new: TdmMapEngine_new
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  OnDestroy = DataModuleDestroy
  Left = 1470
  Top = 919
  Height = 200
  Width = 237
  object qry_Poly: TADOQuery
    LockType = ltBatchOptimistic
    Parameters = <>
    SQL.Strings = (
      
        'select top 500 id, lat1,lon1,    lat2,lon2,   checksum(lat1,lon1' +
        ',  lat2, lon2) as checksum'
      'from view_Link'
      'order by checksum'
      '')
    Left = 144
    Top = 8
  end
  object qry_Temp: TADOQuery
    LockType = ltBatchOptimistic
    Parameters = <>
    SQL.Strings = (
      
        'select top 500 id, lat1,lon1,    lat2,lon2,   checksum(lat1,lon1' +
        ',  lat2, lon2) as checksum'
      'from view_Link'
      'order by checksum'
      '')
    Left = 144
    Top = 80
  end
  object ADOStoredProc1: TADOStoredProc
    LockType = ltBatchOptimistic
    Parameters = <>
    Left = 40
    Top = 8
  end
end
