unit u_MapEngine_classes;

interface

uses
  Classes, IniFiles, DB, SysUtils,

  u_geo;

type

  //-------------------------------------------------------------------
  TMapFeature = class(TCollectionItem)
  //-------------------------------------------------------------------
  public
    ID: Integer;
    Lat : double;
    Lon : double;

    ObjType : (otPoint,otLine,otRegion);

    BLPoint : TBLPoint;
    BLVector: TBLVector;
    Azimuth : double;

    Checksum : Integer;

    Index : Integer;  // for links
  public
    procedure LoadFromDataset(aDataset: TDataset);
  end;


  //-------------------------------------------------------------------
  TMapFeatureList = class(TCollection)
  //-------------------------------------------------------------------
  private
    function GetItems(Index: Integer): TMapFeature;
  public
    constructor Create;
    function AddItem: TMapFeature;
    function FindByID(aID: Integer): TMapFeature;

    procedure LoadFromDataset(aDataset: TDataset);

    procedure LoadFromIni(aFileName: string);
    procedure SaveToIni(aFileName: string);

    property Items[Index: Integer]: TMapFeature read GetItems; default;
  end;


implementation


//-------------------------------------------------------------------
procedure TMapFeature.LoadFromDataset(aDataset: TDataset);
//-------------------------------------------------------------------
begin
  with aDataset do
  begin
    ID  :=FieldByName('id').AsInteger;
    Lat :=FieldByName('Lat').AsFloat;
    Lon :=FieldByName('Lon').AsFloat;


//    if Assigned(FindByID) then

    Azimuth :=FieldByName('Azimuth').AsFloat;


    Checksum :=FieldByName('Checksum').AsInteger;
  end;
end;

constructor TMapFeatureList.Create;
begin
  inherited Create(TMapFeature);
end;

function TMapFeatureList.AddItem: TMapFeature;
begin
  Result := TMapFeature (inherited Add);
end;

function TMapFeatureList.FindByID(aID: Integer): TMapFeature;
var I: Integer;
begin
 Result := nil;

  for I := 0 to Count - 1 do
    if Items[i].ID= aID then
    begin
      Result := Items[i];
      Break;
    end;
end;

function TMapFeatureList.GetItems(Index: Integer): TMapFeature;
begin
  Result := TMapFeature(inherited Items[Index]);
end;

// ---------------------------------------------------------------
procedure TMapFeatureList.LoadFromDataset(aDataset: TDataset);
// ---------------------------------------------------------------
var
  obj: TMapFeature;
//  iChecksum : Integer;

begin
  Clear;
  aDataset.First;

//  iChecksum :=0;

  with aDataset do
    while not EOF do
  begin
   // obj:=
  //  AddItem.LoadFromDataset(aDataset);

    obj:=AddItem();

(*
    obj.ID  :=FieldByName('id').AsInteger;
    obj.Lat :=FieldByName('Lat').AsFloat;
*)
//    obj.Lon :=FieldByName('Lon').AsFloat;

    obj.blVector:=db_ExtractBLVector (aDataset);

(*    obj.Lon :=FieldByName('Lon').AsFloat;
    obj.Lon :=FieldByName('Lon').AsFloat;
*)

//     Azimuth :=FieldByName('Azimuth').AsFloat;

    obj.Checksum :=FieldByName('Checksum').AsInteger;


    aDataset.Next;
  end;
end;

// ---------------------------------------------------------------
procedure TMapFeatureList.LoadFromIni(aFileName: string);
// ---------------------------------------------------------------
var
  I: Integer;
  iCount: Integer;
  oIni: TMemIniFile;

  obj: TMapFeature;
  sSection: string;
begin
  oIni:=TMemIniFile.Create (aFileName);

  iCount:=oIni.ReadInteger('main', 'count',  0);

  for I := 0 to Count - 1 do
  begin
    obj:=AddItem();

    sSection:= Format('item_%d', [i]);

    obj.ID := oIni.ReadInteger(sSection, 'ID', 0);
    obj.Checksum := oIni.ReadInteger(sSection, 'Checksum', 0);
    obj.lat := oIni.ReadFloat(sSection, 'lat', 0);
    obj.lon := oIni.ReadFloat(sSection, 'lon', 0);

  end;


  oIni.Free;
end;

// ---------------------------------------------------------------
procedure TMapFeatureList.SaveToIni(aFileName: string);
// ---------------------------------------------------------------
var
  I: Integer;

  obj: TMapFeature;
  s: string;
  sSection: string;

  oStrList: TStringList;
begin
  oStrList:=TStringList.Create;

  oStrList.Add('[main]');
  oStrList.Add(Format('count=%d', [Count]));
  oStrList.Add('');


  for I := 0 to Count - 1 do
  begin
    obj:=Items[i];

    oStrList.Add(Format('[%d]', [obj.ID]));
    oStrList.Add(Format('Checksum=%d', [obj.Checksum]));
    oStrList.Add('');

  end;

  oStrList.SaveToFile(aFileName);

  oStrList.Free;

end;


end.

  (*Exit;

  oIni:=TMemIniFile.Create (aFileName);

  oIni.WriteInteger('main', 'count',  Count);

  for I := 0 to Count - 1 do
   // with obj do
  begin
    obj:=Items[i];

  //  sSection:= Format('item_%d', [i]);
    sSection:= Format('%d', [obj.ID]);

  //  oIni.WriteInteger(sSection, 'ID', obj.ID);
    oIni.WriteInteger(sSection, 'Checksum', obj.Checksum);

//s:=IntToStr(Checksum);
  //  oIni.WriteString(sSection, 'Checksum', IntToStr(Checksum));

  //  oIni.WriteFloat(sSection, 'lat', lat);
//    oIni.WriteFloat(sSection, 'lon', lon);

  end;

  oIni.UpdateFile;
  oIni.Free;
  *)


