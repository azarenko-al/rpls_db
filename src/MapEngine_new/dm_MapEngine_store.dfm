object dmMapEngine_store: TdmMapEngine_store
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Left = 1493
  Top = 524
  Height = 282
  Width = 364
  object ADOStoredProc11: TADOStoredProc
    LockType = ltBatchOptimistic
    Parameters = <>
    Left = 56
    Top = 32
  end
  object ADOStoredProc_Object_Maps: TADOStoredProc
    ConnectionString = 
      'Provider=MSDASQL.1;Password=sa;Persist Security Info=True;User I' +
      'D=sa;Data Source=onega_link'
    LockType = ltBatchOptimistic
    ProcedureName = 'sp_MapDesktop_select_ObjectMaps'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@ID'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CHECKED'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end>
    Left = 88
    Top = 152
  end
end
