unit dm_MapEngine_store;

interface
{$I Mapx.inc}

uses
  Classes, Forms, DB, ADODB, Variants, Dialogs,  sysUtils,  StrUtils,
  MapXLib_TLB,

  u_func_msg,

 // Options_TLB,

   u_MapDesktop_classes,
   dm_Map_Desktop,


   i_Map_engine,

//  I_Options_,

//  X_Options,

  u_mapX,

  dm_Main,

  dm_MapEngine_new,

  u_const_db,
  

  u_files,
  
  u_log  ;

//  AppEvnts, IdBaseComponent, IdComponent, IdUDPBase, IdUDPClient, IdSNMP


//type
//  TdmMapEngine_store_flags = set of (flAutoBindData);

type
  TdmMapEngine_store = class(TDataModule, IMessageHandler)
    ADOStoredProc11: TADOStoredProc;
    ADOStoredProc_Object_Maps: TADOStoredProc;
 //   procedure DataModuleDestroy(Sender: TObject);
//    procedure ApplicationEvents1Message(var Msg: tagMSG; var Handled: Boolean);
    procedure DataModuleCreate(Sender: TObject);
  private
    FFileDir: string;

  private

    procedure Bind_points(aVLayer: CMapXLayer; aObjName: string);
    function Bind_data(aVLayer: CMapXLayer; aADODataset: TCustomADODataSet;
        aObjName: string): Boolean;
    procedure Delete_ADOStoredProc(aObjName: string);
    function GetFileName(aObjName: string): string;
    function Create_ADOStoredProc(aObjName: string): TADOStoredProc;
    function ExtractObjName(aLayerName: string): string;
    function Find_ADOStoredProc(aObjName: string): TADOStoredProc;
    function GetLayerBy_ObjectName(aObjName: string): CMapXLayer;

    procedure Log(aMsg: string);
    procedure Open_Map_Desktop;
//    procedure RefreshObjectThemas(aVLayer: CMapXLayer; aObjName: string);
    procedure SetMap(const aMap: TMap);

     procedure GetMessage(aMsg: TEventType; aParams: TEventParamList ; var aHandled:   boolean);


// TODO: Update_Link_Status
//  procedure Update_Link_Status(aID, aIntValue: Integer; aStrValue: string);
  public
    FMap: TMap;

    function IsMapAssigned: Boolean;

    class procedure Init;

    procedure ClearFiles;
    procedure CloseAllData;

    procedure Delete_Object_Map(aObjName: string);

    function Open_Object_Map(aObjName: string): CMapXLayer;

    procedure Feature_Add(aObjName: string; aID: Integer);
    function Feature_Del(aObjName: string; aID: Integer): Integer;

    procedure Update_Data(aObjName: string; aCMapXLayer: CMapXLayer=nil);

    procedure Dlg_ShowInfo;
// TODO: Data_Update11111111111
//  procedure Data_Update11111111111(aObjName: string);

    procedure Feature_Update(aObjName: string; aID: Integer);

    function Is_Object_Map_Exists(aObjName: string): Boolean;

    procedure RefreshObjectThemas_All;

    procedure Reload_Object_Layers;

    procedure Reload_Object(aObjName: string);


    property Map: TMap read FMap write SetMap;

  end;


var
  dmMapEngine_store: TdmMapEngine_store;

implementation

uses dm_Onega_db_data;


{$R *.dfm}

const
//  DEF_PREFIX = '___';
  DEF_PREFIX = '~';

  OBJ_link = 'link';

    

class procedure TdmMapEngine_store.Init;
begin
  if not Assigned(dmMapEngine_store) then
    Application.CreateForm(TdmMapEngine_store, dmMapEngine_store);
//    dmMapEngine_store:= TdmMapEngine_store.Create(Application);

end;


procedure TdmMapEngine_store.DataModuleCreate(Sender: TObject);
begin
  TdmMapEngine_new.Init;


//    IdSNMP1.

end;



// ---------------------------------------------------------------
procedure TdmMapEngine_store.Bind_points(aVLayer: CMapXLayer; aObjName: string);
// ---------------------------------------------------------------
const
  FLD_caption = 'caption';
  FLD_ID = 'ID';

var
  I: Integer;

 // vBindLayerObject: CMapXBindLayer;
  vDataSet: CMapxDataset;

 // oField: TField;
 ///
 // vField: CMapXField;
 // vFields: CMapXFields;
  iIndex: Integer;
  k: Integer;
//  oADOStoredProc: TADOStoredProc;

  s: string;
  sFieldName: string;
  v: Variant;

begin
(*  // -------------------------
  oADOStoredProc:=Find_ADOStoredProc(aObjName);

  oADOStoredProc.Close;
  oADOStoredProc.Open;

*)
  assert(assigned(Map));
  Assert(Assigned(aVLayer), 'Value not assigned');

  i := aVLayer.Datasets.Count;

  aVLayer.Datasets.RemoveAll;

  s:=aVLayer.Name;

  k:=aVLayer.Datasets.Count;


//  if (vLayer.Datasets.Count = 0) then

  try
    vDataSet := Map.Datasets.Add (miDataSetLayer, aVLayer,
                    aObjName+'-Points', //EmptyParam, // 'layer',

                  //  'Points',
                 //   EmptyParam,
                    //aVLayer.Name,
                    EmptyParam, EmptyParam, EmptyParam, EmptyParam, EmptyParam) ;

    i:=vDataSet.RowCount;

  except
   // on E: Exception do: ;
  end;

//xx  k:=aVLayer.Datasets.Count;
  s:=vDataSet.Name;

end;



function TdmMapEngine_store.IsMapAssigned: Boolean;
begin
  Result := Assigned(FMap);
end;



// ---------------------------------------------------------------
function TdmMapEngine_store.Bind_data(aVLayer: CMapXLayer; aADODataset: TCustomADODataSet; aObjName: string): Boolean;
// ---------------------------------------------------------------
const
  FLD_caption = 'caption';
  FLD_ID = 'ID';

var
  I: Integer;
  iID: Integer;
  vBindLayerObject: CMapXBindLayer;
  vDataSet: CMapxDataset;
  oField: TField;
  vField: CMapXField;
  vFields: CMapXFields;
  iIndex: Integer;
  k: Integer;
  s: string;
  sDatasetName: string;
 // sDatasetName: string;
  sFieldName: string;
  sTempFileName: string;
  v: Variant;

  vTempLayer: CMapXLayer;


  vLayerInfoObject: CMapXLayerInfo;


begin
  Result := False;



  assert(assigned(Map));
  Assert(Assigned(aVLayer), 'Value not assigned');

  i := aVLayer.Datasets.Count;


  s:=aVLayer.Name;
  vFields := CoFields.Create;


  sDatasetName := aObjName+'-Data';

//  sDatasetName := 'Data';

  vDataSet := mapx_GetDatasetByName(aVLayer, sDatasetName);

  if Assigned(vDataSet) then
    aVLayer.Datasets.Remove(sDatasetName);


{
  vDataSet := mapx_GetDatasetByName(aVLayer, sDatasetName);

  if Assigned(vDataSet) then
    aVLayer.Datasets.Remove(sDatasetName);
 }


  k:=Map.DataSets.Count;

{
  if aVLayer.Datasets.Count>0 then
    aVLayer.Datasets.RemoveAll;
}

  k:=Map.DataSets.Count;


//  db_view(aADODataset);

//  if aADODataset.RecordCount=0 then
//    Exit;


  for I := 0 to aADODataset.Fields.Count-1  do
  begin
    oField := aADODataset.Fields[i];
    s:=oField.FieldName;

    sFieldName :=oField.FieldName;


    case oField.DataType of
      ftAutoInc,
      ftInteger : vField:=vFields.Add(sFieldName, sFieldName, miAggregationIndividual, miTypeInteger);

      ftFloat   : vField:=vFields.Add(sFieldName, sFieldName, miAggregationIndividual, miTypeFloat);


      ftBoolean   : vField:=vFields.Add(sFieldName, sFieldName, miAggregationIndividual, miTypeLogical);
      ftDateTime   : vField:=vFields.Add(sFieldName, sFieldName, miAggregationIndividual, miTypeDate);


     // ftGUID,
      ftMemo,
      ftWideString,
      ftGuid,
      ftString  : vField:=vFields.Add(sFieldName, sFieldName, miAggregationIndividual, miTypeString);
    else
      raise Exception.Create(' TdmMapEngine_store.Bind_data - case oField.DataType of '  +  IntToStr(Integer(oField.DataType)) );
    end;

{
 TFieldType = (ftUnknown, ftString, ftSmallint, ftInteger, ftWord,
    ftBoolean, ftFloat, ftCurrency, ftBCD, ftDate, ftTime, ftDateTime,
    ftBytes, ftVarBytes, ftAutoInc, ftBlob, ftMemo, ftGraphic, ftFmtMemo,
    ftParadoxOle, ftDBaseOle, ftTypedBinary, ftCursor, ftFixedChar, ftWideString,
    ftLargeint, ftADT, ftArray, ftReference, ftDataSet, ftOraBlob, ftOraClob,
    ftVariant, ftInterface, ftIDispatch, ftGuid, ftTimeStamp, ftFMTBcd);
}



  end;

  Assert(vFields.Count>0,
    'function TdmMapEngine_store.Bind_data(aVLayer: CMapXLayer; aADODataset: TCustomADODataSet; aObjName: string): Boolean; '+
    ' for I := 0 to aADODataset.Fields.Count-1  do');



  k:=Map.DataSets.Count;


 // Map.DataSets.aObjName+'-Points';

 // Assert(Map.DataSets.Count>0, 'Map.DataSets.Count>0');
//  Assert(Map.DataSets.Item(1).RowCount>0, 'Map.DataSets.Item(1).RowCount>0');

  try
    vDataSet := Map.DataSets.Add(miDatasetADO, aADODataset.Recordset,
          aObjName+'-Data',//EmptyParam,
        // 'Data',
        //  EmptyParam,
          FLD_ID,
          EmptyParam,
          aVLayer.Name,
          vFields,
          EmptyParam //Dynamic	 Variant: A Boolean value that controls whether the data binding is dynamic. Optional; if omitted, defaults to false, meaning that the binding is static (i.e., MapX will copy needed data when the database is opened). If you specify True, MapX accesses data in a live manner, only data needed (e.g. when labeling). If you specify True but the Dataset does not support dynamic columns, an exception will be thrown.
          );


    i:=vDataSet.RowCount;


   // k:=aVLayer.Datasets.Count;
   // s:=vDataSet.Name;

  except
     on E: Exception do
     begin
       ShowMessage(E.Message);
       Exit;
     end;
  end;

  //-----------------------------
   //aObjName


//   Map.La


    {

ShowMessage(aObjName);



//////////////////////
  sTempFileName:= Format('d:\~%s.tab', [aObjName]);

  k:=Map.Layers.Count;

  vLayerInfoObject := CoLayerInfo.Create;
  vLayerInfoObject.Type_ := miLayerInfoTypeNewTable;
  vLayerInfoObject.AddParameter('FileSpec', sTempFileName);
  vLayerInfoObject.AddParameter('Fields', vDataSet.Fields);
  vLayerInfoObject.AddParameter('Features', aVLayer.AllFeatures);
  vLayerInfoObject.AddParameter('AutoCreateDataset', 1);
  vLayerInfoObject.AddParameter('OverwriteFile', 1);

  vTempLayer:=Map.Layers.Add(vLayerInfoObject, EmptyParam);

  k:=Map.Layers.Count;

  Map.Layers.Remove(vTempLayer);

  k:=Map.Layers.Count;
    }

//////////////////////


  //-----------------------------

  avLayer.LabelProperties.Dataset   := vDataSet;


  v := vDataSet.Fields.Item[FLD_caption];

  avLayer.LabelProperties.DataField := v; // vDataSet.Fields.Item(FLD_caption);



//  aVLayer.LabelProperties.Visible := True;
//  aVLayer.AutoLabel := True;


  // -------------------------

///////////  RefreshObjectThemas(aVLayer, aObjName);

 // r=t
end;


procedure TdmMapEngine_store.ClearFiles;
begin
  DirClear(FFileDir);
end;

// ---------------------------------------------------------------
function TdmMapEngine_store.GetFileName(aObjName: string): string;
// ---------------------------------------------------------------
begin
//  FFileDir := 's:\1\';

  FFileDir:=dmMain.Dirs.ProjectMapDir;

  ForceDirectories(FFileDir);

  Result := ChangeFileExt(FFileDir + DEF_PREFIX+ aObjName, '.tab');
end;

// ---------------------------------------------------------------
procedure TdmMapEngine_store.Delete_ADOStoredProc(aObjName: string);
// ---------------------------------------------------------------
var
  oADOStoredProc: TADOStoredProc;
begin
  oADOStoredProc:=Find_ADOStoredProc(aObjName);

  if Assigned(oADOStoredProc) then
    oADOStoredProc.Free;

end;



// ---------------------------------------------------------------
function TdmMapEngine_store.Create_ADOStoredProc(aObjName: string):
    TADOStoredProc;
// ---------------------------------------------------------------
var
  oComponent: TComponent;
  oADOStoredProc: TADOStoredProc;
  sName: string;
begin
  sName := 'proc_'+ aObjName;

  oComponent:=FindComponent(sName);
  if Assigned(oComponent) then
    oADOStoredProc := TADOStoredProc(oComponent)
//    oComponent.Free;
  else
  begin
    oADOStoredProc:=TADOStoredProc.Create(Self);
    oADOStoredProc.Connection  := dmMain.ADOConnection1;
    oADOStoredProc.Name :=sName;
    oADOStoredProc.LockType := ltBatchOptimistic;
    oADOStoredProc.Tag  :=1;

  end;
//  i:=ComponentCount;

 // InsertComponent(oADOStoredProc);

  Result := oADOStoredProc;
end;


// ---------------------------------------------------------------
procedure TdmMapEngine_store.CloseAllData;
// ---------------------------------------------------------------
var
  I: Integer;
begin
  for I := ComponentCount - 1 downto 0  do
  begin
    if Components[i].Tag=1 then
      Components[i].Free;
  end;
end;


// ---------------------------------------------------------------
function TdmMapEngine_store.Find_ADOStoredProc(aObjName: string):
    TADOStoredProc;
// ---------------------------------------------------------------
var
  oComponent: TComponent;
  sName: string;
begin
  sName := 'proc_'+ aObjName;

  oComponent:=FindComponent(sName);
  if Assigned(oComponent) then
    result := TADOStoredProc(oComponent)
  else
    Result := nil;
end;


// ---------------------------------------------------------------
function TdmMapEngine_store.GetLayerBy_ObjectName(aObjName: string): CMapXLayer;
// ---------------------------------------------------------------
var
  iInd: Integer;
begin
  assert(assigned(Map));

  iInd := mapx_GetLayerIndexByName_from_1(Map, DEF_PREFIX + aObjName);
  if iInd>0 then
    Result := mapx_GetLayerByIndex_from_1(Map, iInd)

  else
    Result := nil;
end;

// ---------------------------------------------------------------
function TdmMapEngine_store.Is_Object_Map_Exists(aObjName: string): Boolean;
// ---------------------------------------------------------------
var
  iInd: Integer;
  sFileName: string;

  vLayer: CMapXLayer;
begin
  Result := False;
//  Assert(Assigned(Map), 'Value not assigned');

  if not Assigned(FMap) then
    Exit;


  sFileName := GetFileName (aObjName);

  iInd := mapx_GetLayerIndexByFileName_from_1(FMap, sFileName);

  if iInd>1 then
  begin
    vLayer := mapx_GetLayerByIndex_from_1(FMap, iInd);
    vLayer.Visible := True;


  end;


  Result := iInd>=1;
end;

// ---------------------------------------------------------------
procedure TdmMapEngine_store.Reload_Object_Layers;
// ---------------------------------------------------------------
var
  I: Integer;
  oSList: TStringList;
  s: string;

  vLayer: CMapXLayer;
begin
  if not Assigned(FMap) then
    Exit;


  oSList:=TStringList.Create;

   for I := 1 to Map.Layers.Count do
   begin
     vLayer := mapx_GetLayerByIndex_from_1(Map, i);

     s:= ExtractObjName(vLayer.Name);
     if s<>'' then
       oslist.Add(s)
   end;

  for I := 0 to oSList.Count - 1 do
    Open_Object_Map(oSList[i]);


  FreeAndNil(oSList);


  RefreshObjectThemas_All;

end;



// ---------------------------------------------------------------
function TdmMapEngine_store.Open_Object_Map(aObjName: string): CMapXLayer;
// ---------------------------------------------------------------
var
  b: Boolean;
  bAutoLabel: Boolean;
  I: Integer;
  iInd: Integer;
  iInd_new: Integer;
  s: string;
  vLayer: CMapXLayer;
  oADOStoredProc: TADOStoredProc;
  sFileName: string;
  iProjectID : Integer;


    FMapDesktopItem: TMapDesktopItem;

      oMapItem: TMapItem;


//  oSList: TStringList;

begin
  Assert(aObjName<>'');

  if not Assigned(FMap) then
    Exit;


  assert(assigned(FMap));

  sFileName := GetFileName (aObjName);

  iProjectID :=dmMain.ProjectID;

  bAutoLabel := False;



 // mapx_Show_Layers_Dlg(Map);

  ///////////////////////////////////////////

  iInd := mapx_GetLayerIndexByFileName_from_1(Map, sFileName);
  if iInd>0 then
  begin
    vLayer := mapx_GetLayerByIndex_from_1(Map, iInd);

    i:=vLayer.Datasets.Count;

    s :=vLayer.Name;

    bAutoLabel :=vLayer.AutoLabel;

    Log('TdmMapEngine_store.Open_Object_Map: '+  sFileName + '  dataset: '+ IntToStr(i));

    Map.Layers.Remove(iInd);

    i:=Map.Layers.Count;
  end;

  dmMapEngine_new.Make_Object_Map(iProjectID, aObjName, sFileName);

  if not FileExists(sFileName) then
  begin
    Log('FileExists(sFileName): '+ sFileName);

    Exit;
  end;


//  vLayer.



  i:=Map.Layers.Count;

  if iInd>0 then
    vLayer:= FMap.Layers.Add(sFileName, iInd)
  else
//    vLayer:= FMap.Layers.Add(sFileName, Map.Layers.Count+1);
    vLayer:= FMap.Layers.Add(sFileName, EmptyParam);



//  vLayer:= FMap.Layers.Add(sFileName, Map.Layers.Count);
//mapx_Show_Layers_Dlg(Map);


//  vLayer:= FMap.Layers.Add(sFileName, EmptyParam);

  iInd_new := Map.Layers.Count;
//  mapx_GetLayerIndexByFileName_from_1(Map, sFileName);


//mapx_Show_Layers_Dlg(FMap);

 // vLayer:= FMap.Layers.Add(sFileName, FMap.Layers.Count);
  s :=vLayer.Name;
  vLayer.Name := DEF_PREFIX+ aObjName;

  i:=Map.Layers.Count;

//
//  if (iInd>0) and (iInd_new <> iInd) then
//  begin
//    mapx_Show_Layers_Dlg(Map);
//
//    g_Log.SysMsg(Format('Open_Object_Map - FMap.Layers.Move(%d, %d);', [iInd_new, iInd]));
//
//    b:=Map.Layers.Move(iInd_new, iInd);
//
//
//
////mapx_Show_Layers_Dlg(FMap);
//
//  end;


//    mapx_Show_Layers_Dlg(Map);


  Result := vLayer;

 // vLayer.AutoLabel := True;

 // ------------------------
 // data
 // ------------------------

  oADOStoredProc:=Create_ADOStoredProc(aObjName);

  dmMapEngine_new.Data_Open_StoredProc_data(oADOStoredProc, iProjectID, aObjName);


//  db_View(oADOStoredProc);


// try
  Bind_points(vLayer, aObjName ); //oADOStoredProc,
  Bind_data(VLayer, oADOStoredProc, aObjName);
 // Exit;

  Assert(Assigned (vLayer));




 // vLayer.AutoLabel:= bAutoLabel;

// -- if bAutoLabel then
  begin


    FMapDesktopItem:=dmMap_Desktop.MapDesktopItem;

    if FMapDesktopItem.MapList.Count=0 then
    begin
      dmMap_Desktop.Open;
      dmMap_Desktop.Prepare_ObjectMaps;
    end;

    oMapItem:=FMapDesktopItem.MapList.FindByObjectName(aObjName);
    if Assigned(oMapItem) then
    begin
      oMapItem.LabelProperties.SaveToMapinfoLayer(vLayer);
      vLayer.AutoLabel:=oMapItem.AutoLabel;

      //vLayer.LabelProperties.DataField;

    //  if vLayer.AutoLabel then
    //    Assert (vLayer.LabelProperties.DataField <> null);

    end;

 end;





   i:=Map.Layers.Count;

 // except

//  end;

 // db_View(oADOStoredProc);


 // RefreshObjectThemas (vLayer, aObjName);

end;


// ---------------------------------------------------------------
procedure TdmMapEngine_store.Delete_Object_Map(aObjName: string);
// ---------------------------------------------------------------
var
  I: Integer;
  iInd: Integer;
  sFileName: string;
  oADOStoredProc: TADOStoredProc;

 // s: string;
 // vLayer: CMapXLayer;
 // oComponent: TComponent;

  //sName: string;


 // iProjectID : Integer;

begin
//  FFileDir := 's:\1\';
  assert(assigned(Map));



  sFileName := GetFileName (aObjName);

//  iProjectID :=dmMain.ProjectID;

  iInd := mapx_GetLayerIndexByFileName_from_1(Map, sFileName);
  if iInd>=1 then
    Map.Layers.Remove(iInd);


 // ------------------------
 // data
 // ------------------------
  Delete_ADOStoredProc(aObjName);
end;

procedure TdmMapEngine_store.Log(aMsg: string);
begin
  g_log.Msg(aMsg);
end;



procedure TdmMapEngine_store.Dlg_ShowInfo;
var
  I: Integer;
  s: string;
begin
  s := '';

  for I := 0 to ComponentCount - 1 do
    s:=s+ Components[i].Name + '-';

  ShowMessage(s);

end;

// ---------------------------------------------------------------
procedure TdmMapEngine_store.Feature_Update(aObjName: string; aID: Integer);
// ---------------------------------------------------------------
var
  vCMapXLayer: CMapXLayer;
begin
 // ShowMessage('procedure TdmMapEngine_store.Feature_Update(aObjName: string; aID: Integer);');


  if not Assigned(FMap) then
    Exit;

  vCMapXLayer := GetLayerBy_ObjectName(aObjName);
  if not Assigned(vCMapXLayer) then
    Exit;
  

  dmMapEngine_new.Feature_Del1(FMap, vCMapXLayer, aObjName, aID);
  dmMapEngine_new.Feature_Add (FMap, vCMapXLayer, aObjName, aID);

  Update_Data (aObjName);
end;


// ---------------------------------------------------------------
procedure TdmMapEngine_store.Feature_Add(aObjName: string; aID: Integer);
// ---------------------------------------------------------------
var
  vCMapXLayer: CMapXLayer;
  oADOStoredProc: TADOStoredProc;
begin
  if not Assigned(Map) then
    Exit;

 // Assert(aID>0, 'Value <=0');

  vCMapXLayer := GetLayerBy_ObjectName(aObjName);
  if not Assigned(vCMapXLayer) then
    Exit;


 // db_View(oADOStoredProc);

  dmMapEngine_new.Feature_Add(Map, vCMapXLayer, aObjName, aID);

  Update_Data(aObjName);

end;



// ---------------------------------------------------------------
procedure TdmMapEngine_store.Update_Data(aObjName: string; aCMapXLayer:
    CMapXLayer=nil);
// ---------------------------------------------------------------
var
  vCMapXLayer: CMapXLayer;
  oADOStoredProc: TADOStoredProc;
begin
//  if not Assigned(aCMapXLayer) then


  if not Assigned(FMap) then
    Exit;

  vCMapXLayer := GetLayerBy_ObjectName(aObjName);
  if not Assigned(vCMapXLayer) then
    Exit;

  oADOStoredProc:=Find_ADOStoredProc(aObjName);
  if not Assigned(oADOStoredProc) then
    Exit;


  oADOStoredProc.Close;
  oADOStoredProc.Open;


 // db_View (oADOStoredProc);

  Bind_data(vCMapXLayer, oAdOStoredProc, aObjName);

//  FMap.Refresh;


  RefreshObjectThemas_All;

end;


// ---------------------------------------------------------------
function TdmMapEngine_store.Feature_Del(aObjName: string; aID: Integer):
    Integer;
// ---------------------------------------------------------------
var
  vCMapXLayer: CMapXLayer;
begin
  result:=-1;

  if not Assigned(FMap) then
    Exit;

  vCMapXLayer := GetLayerBy_ObjectName(aObjName);
  if not Assigned(vCMapXLayer) then
    Exit;

  result:=dmMapEngine_new.Feature_Del1(FMap, vCMapXLayer, aObjName, aID);

end;


procedure TdmMapEngine_store.SetMap(const aMap: TMap);
begin
  FMap := aMap;

  if FMap=nil then
    CloseAllData;
end;



function TdmMapEngine_store.ExtractObjName(aLayerName: string): string;
begin
  if LeftStr(aLayerName, Length(DEF_PREFIX))=DEF_PREFIX then
    result := Copy(aLayerName, Length(DEF_PREFIX)+1, 100)
  else
    result :='';
end;

// ---------------------------------------------------------------
procedure TdmMapEngine_store.Open_Map_Desktop;
// ---------------------------------------------------------------
var
  iID: Integer;
  k: Integer;
begin
  k := dmOnega_DB_data.MapDesktop_Select(ADOStoredProc11, dmMain.ProjectID);

  iID := ADOStoredProc11.FieldByName(FLD_ID).AsInteger;

  k:=dmOnega_DB_data.MapDesktop_Select_ObjectMaps (ADOStoredProc_Object_Maps, iID);

  // TODO -cMM: TdmMapEngine_store.Open_Map_Desktop default body inserted
end;

// ---------------------------------------------------------------
procedure TdmMapEngine_store.RefreshObjectThemas_All;
// ---------------------------------------------------------------
var
  iID: Integer;
  k: Integer;
  sObjName: string;
  sStyle: string;

begin
  k := dmOnega_DB_data.MapDesktop_Select(ADOStoredProc11, dmMain.ProjectID);
  iID := ADOStoredProc11.FieldByName(FLD_ID).AsInteger;

  k:=dmOnega_DB_data.MapDesktop_Select_ObjectMaps (ADOStoredProc_Object_Maps, iID);


  with ADOStoredProc_Object_Maps do
    while not Eof do
    begin
      sObjName:=FieldByName ('MapObject_name').AsString;
      sStyle  :=FieldByName (FLD_Style).AsString;

     // sStyle  := ''; //FieldByName (FLD_Style).AsString;


      Init_IMapEngineX.Apply_Theme( Map.DefaultInterface, sObjName, sStyle );

      NExt;
    end;


   FMap.refresh;

//  IOptions.Apply_Theme(FMap.DefaultInterface);
          
end;

procedure TdmMapEngine_store.Reload_Object(aObjName: string);
begin
  if Is_Object_Map_Exists(aObjName) then
    Open_Object_Map(aObjName);

end;

procedure TdmMapEngine_store.GetMessage(aMsg: TEventType; aParams:  TEventParamList ; var aHandled: boolean);
begin
  if not Assigned(FMap) then
    Exit;

  case aMsg of
    WE_MAP_REFRESH_OBJECT_Themas:
         RefreshObjectThemas_All ();

  end;

end;




end.

  {
procedure TdmMapEngine_store.DataModuleDestroy(Sender: TObject);
begin
 ////// FreeAndNil(dmMapEngine_new);
end;



procedure TdmMapEngine_store.ApplicationEvents1Message(var Msg: tagMSG; var Handled: Boolean);
begin
  if not Assigned(FMap) then
    Exit;


  case Msg.message of
    WE_MAP_REFRESH_OBJECT_Themas:
         RefreshObjectThemas_All ();

  end;
end;

