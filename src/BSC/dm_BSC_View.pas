unit dm_BSC_View;

interface

uses
  SysUtils, Classes, Forms, DB,

  dm_Main,
  
  
  
  u_db,

  dm_Custom,

  u_Types,
  u_const_db, ADODB
;


type
  TdmBSC_View = class(TdmCustom)
    qry_Temp: TADOQuery;
  private
    { Private declarations }
  public
    procedure InitDB (aDataset: TDataset);
    procedure OpenDB  (aID: integer; aDataset: TDataset);
//    procedure OpenDBLac (aID: integer; aDataset: TDataset);

  end;


function dmBSC_View: TdmBSC_View;

//==============================================================
implementation {$R *.DFM}
//==============================================================


var
  FdmBSC_View: TdmBSC_View;


// ---------------------------------------------------------------
function dmBSC_View: TdmBSC_View;
// ---------------------------------------------------------------
begin
  if not Assigned(FdmBSC_View) then
      FdmBSC_View := TdmBSC_View.Create(Application);

  Result := FdmBSC_View;
end;


//-------------------------------------------------------------------
procedure TdmBSC_View.InitDB (aDataset: TDataset);
//-------------------------------------------------------------------
begin
  db_CreateField (aDataset, FLD_ID, ftInteger);
  db_CreateField (aDataset, FLD_CODE, ftInteger);
  db_CreateField (aDataset, FLD_COLOR, ftInteger);
  db_CreateField (aDataset, FLD_NAME, ftString);
  db_CreateField (aDataset, FLD_COMMENT, ftString);
  db_CreateField (aDataset, FLD_TYPE, ftString);
end;


//-------------------------------------------------------------------
//procedure TdmBSC_View.OpenDBLac (aID: integer; aDataset: TDataset);
//-------------------------------------------------------------------
//begin
//  aDataset.Close;
//  aDataset.Open;
//
//  db_OpenTable (qry_Temp, TBL_LAC, [db_Par(FLD_BSC_ID, aID)] );
//
//  with qry_Temp do
//  begin
//    First;
//    while not EOF do
//    begin
//      db_AddRecord (aDataset,
//         [db_Par(FLD_ID,    FieldValues[FLD_ID]),
//          db_Par(FLD_CODE,  FieldValues[FLD_CODE]),
//          db_Par(FLD_COLOR, FieldValues[FLD_COLOR])
//         ]);
//
//      Next;
//end;
//  end;
//
//end;


//-------------------------------------------------------------------
procedure TdmBSC_View.OpenDB (aID: integer; aDataset: TDataset);
//-------------------------------------------------------------------
const
  SQL_SELECT_TEMPLATE= 'SELECT id,name FROM %s WHERE bsc_id=:bsc_id';

  //-------------------------------------------------------------------
  procedure DoAddRecords (aTableName,aObjectName: string);
  //-------------------------------------------------------------------
  var sSQL: string;
  begin
    sSQL:=Format(SQL_SELECT_TEMPLATE, [aTableName]);

    db_OpenQuery (gl_DB.Query, sSQL, [db_Par(FLD_BSC_ID, aID)] );

    with gl_DB.Query do
      while not EOF do
    begin
      db_AddRecord (aDataset,
         [db_Par(FLD_ID,   FieldValues[FLD_ID]),
          db_Par(FLD_NAME, FieldValues[FLD_NAME]),
          db_Par(FLD_TYPE, aObjectName)
         ]);

      Next;
    end;
  end;


begin
  aDataset.Close;
  aDataset.Open;

  DoAddRecords (TBL_SITE,    OBJ_SITE);
 // DoAddRecords (TBL_LINKEND, OBJ_LINKEND);
  //DoAddRecords (TBL_MSC, OBJ_MSC);
 // DoAddRecords (TBL_BSC, OBJ_BSC);

end;

begin
  
end.
