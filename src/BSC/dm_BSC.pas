unit dm_BSC;

interface

uses
  SysUtils, Classes, DB, ADODB, Forms,Variants, 

  dm_Onega_DB_data,

  u_func,
  u_db,
  u_const_db,

  u_types,

  //dm_Folder,
  dm_Main,
  dm_Object_base;


type
  TdmBSCAddRec = record
    NewName: string;
//    FolderID: integer;
    PropertyID: integer;
    MscID: integer;
  end;


  TdmBSC = class(TdmObject_base)
    qry_BSC: TADOQuery;
    ds_BSC: TDataSource;
    ADOConnection1_temp: TADOConnection;
    procedure DataModuleCreate(Sender: TObject);
  private
  public
    function Add (aRec: TdmBSCAddRec): integer;
  	function Del (aID: integer): boolean; override;


  end;

function dmBSC: TdmBSC;


//================================================================
implementation    {$R *.DFM}
//================================================================
var
  FdmBSC: TdmBSC;

// ---------------------------------------------------------------
function dmBSC: TdmBSC;
// ---------------------------------------------------------------
begin
  if not Assigned(FdmBSC) then
    FdmBSC := TdmBSC.Create(Application);

  Result := FdmBSC;
end;


procedure TdmBSC.DataModuleCreate(Sender: TObject);
begin
  inherited;
  TableName:=TBL_BSC;
  ObjectName:=OBJ_BSC;
//  DisplayName:='BSC';
end;


function TdmBSC.Del (aID: integer): boolean;
begin
  Result:=gl_DB.DeleteRecordByID(TBL_BSC, aID);
//  Result:=gl_DB.ExecSP (sp_BSC_del, [db_Par(FLD_ID, aID)]);
end;


//---------------------------------------------------------
function TdmBSC.Add (aRec: TdmBSCAddRec): integer;
//---------------------------------------------------------
begin
  Result := dmOnega_DB_data.ExecStoredProc_ ('sp_BSC_Add',

 // with aRec do
   // Result:=gl_DB.AddRecordID (TBL_MSC,

   // gl_DB.AddRecord (TBL_MSC,
     [
      //db_Par (FLD_PROJECT_ID,  dmMain.ProjectID),
       FLD_NAME,        aRec.NewName,
    //  db_Par (FLD_FOLDER_ID,   IIF(FolderID>0,FolderID,NULL)),
       FLD_PROPERTY_ID, aRec.PropertyID,

       FLD_MSC_ID,      aRec.MscID
     ]);



(*  with aRec do
  Result:=gl_DB.AddRecordID (TableName,

   // gl_DB.AddRecord (TBL_BSC,
     [
      db_Par (FLD_PROJECT_ID,  dmMain.ProjectID),

    //  db_Par (FLD_FOLDER_ID,   IIF_NULL(FolderID )),

      db_Par (FLD_NAME,        aRec.Name),
      db_Par (FLD_PROPERTY_ID, aRec.PropertyID),
      db_Par (FLD_MSC_ID,      IIF_NULL(MscID))
     ]);
 //  ;*)

end;



begin

end.


