inherited frame_BSC_View: Tframe_BSC_View
  Left = 355
  Top = 222
  Width = 623
  Height = 468
  Caption = 'frame_BSC_View'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited Panel1: TPanel
    Width = 615
    TabOrder = 1
  end
  inherited pn_Caption: TPanel
    Width = 615
    TabOrder = 0
  end
  inherited pn_Main: TPanel
    Width = 615
    Height = 311
    object pn_Inspector: TPanel
      Left = 1
      Top = 1
      Width = 597
      Height = 92
      Align = alTop
      BevelOuter = bvLowered
      Caption = 'pn_Inspector'
      TabOrder = 0
    end
    object cxSplitter1: TcxSplitter
      Left = 1
      Top = 160
      Width = 597
      Height = 8
      HotZoneClassName = 'TcxSimpleStyle'
      AlignSplitter = salBottom
      Control = pn_LAC
    end
    object pn_LAC: TPanel
      Left = 1
      Top = 168
      Width = 597
      Height = 142
      Align = alBottom
      BevelOuter = bvNone
      Caption = 'pn_LAC'
      TabOrder = 2
    end
  end
  inherited dxBarManager1: TdxBarManager
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    DockControlHeights = (
      0
      0
      25
      0)
  end
  object ActionList1: TActionList
    Left = 256
    Top = 8
    object act_Line_add1: TAction
      Caption = 'Line_add1'
    end
    object act_Line_add2: TAction
      Caption = 'Line_add2'
    end
  end
end
