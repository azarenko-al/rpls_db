unit a_Unit;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,

  f_Log,

  fr_BSC_view,
  fr_Explorer,
  dm_Explorer,

  dm_Act_View_Eng,
  dm_Act_Map,
  dm_Act_Map_Eng,
  I_Shell,
  dm_Act_BSC,
  dm_Main,

  u_const_msg,
  u_const_DB,
  u_func_app,
  u_dlg, StdCtrls, ComCtrls,
  u_db,
  u_types
  ;

type
  Tfrm_test_BSC = class(TForm)
    pc_Main: TPageControl;
    TabSheet1: TTabSheet;
    bt_Add: TButton;
    bt_View: TButton;
    procedure FormCreate(Sender: TObject);
    procedure bt_AddClick(Sender: TObject);
    procedure bt_ViewClick(Sender: TObject);
  private
    { Private declarations }
  public
    Fframe_BSC : Tframe_Explorer;
    { Public declarations }
  end;

var
  frm_test_BSC: Tfrm_test_BSC;
  id: integer;

implementation
{$R *.DFM}


procedure Tfrm_test_BSC.FormCreate(Sender: TObject);
begin
  dmMain.Test;

  Tfrm_Log.CreateForm;

  Fframe_BSC := Tframe_Explorer.CreateChildForm( TabSheet1);
  Fframe_BSC.Style := esObject;
  Fframe_BSC.SetViewObjects([otBSC]);
  Fframe_BSC.Load;

  PostEvent (WE_PROJECT_CHANGED);


  PostMsg (WM_SHOW_FORM_MAP);

end;

procedure Tfrm_test_BSC.bt_AddClick(Sender: TObject);
begin
//PostMsg (WM_BSC_ADD, 0,0);
end;


procedure Tfrm_test_BSC.bt_ViewClick(Sender: TObject);
begin
id:=gl_DB.GetMaxID (TBL_BSC);
Tframe_BSC_View.CreateForm (id);
end;

end.
