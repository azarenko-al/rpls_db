program test_BSC;

uses
  Forms,
  a_Unit in 'a_Unit.pas' {frm_test_BSC},
  dm_BSC in '..\dm_BSC.pas' {dmBSC: TDataModule},
  fr_BSC_view in '..\fr_BSC_view.pas' {frame_BSC_View},
  dm_BSC_View in '..\dm_BSC_View.pas' {dmBSC_View: TDataModule},
  dm_Main in '..\..\DataModules\dm_Main.pas' {dmMain: TDataModule},
  dm_Main_res in '..\..\DataModules\dm_Main_res.pas' {dmMain_res: TDataModule},
  f_Map in '..\..\Map\f_Map.pas' {frm_Map},
  dm_Main_local in '..\..\DataModules\dm_Main_local.pas' {dmMain_local: TDataModule},
  dm_act_BSC in '..\dm_act_BSC.pas' {dmAct_BSC: TDataModule},
  fr_View_base in '..\..\Explorer\fr_View_base.pas' {frame_View_Base},
  d_BSC_add in '..\d_BSC_add.pas' {dlg_BSC_add};

{$R *.RES}


begin
  Application.Initialize;
  Application.CreateForm(TdmMain, dmMain);
  Application.CreateForm(TdmMain_res, dmMain_res);
  Application.CreateForm(TdmMain_local, dmMain_local);
  Application.CreateForm(Tfrm_test_BSC, frm_test_BSC);
  Application.CreateForm(Tdlg_Wizard_add_with_params, dlg_Wizard_add_with_params);
  Application.Run;
end.
