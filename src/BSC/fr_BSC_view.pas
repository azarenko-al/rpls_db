unit fr_BSC_view;

{ TODO : items }

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
  Dialogs, ActnList, Menus, Grids, DBGrids,
 
  DBCtrls, ToolWin, ImgList,  Db,   ComCtrls, ExtCtrls,
  RXSplit,   cxControls, cxSplitter, dxmdaset, ADODB,

  dm_Main,

  dm_User_Security,

  fr_DBInspector_Container,

  u_const_db,

  u_reg,

  u_func,
  u_types,


  fr_View_base, cxPropertiesStore, dxBar, cxBarEditItem, cxClasses,
  cxLookAndFeels

  ;

type
  Tframe_BSC_View = class(Tframe_View_Base)
    ActionList1: TActionList;
    act_Line_add1: TAction;
    act_Line_add2: TAction;
    pn_Inspector: TPanel;
    cxSplitter1: TcxSplitter;
    pn_LAC: TPanel;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    Ffrm_Inspector: Tframe_DBInspector_Container;


  public
    procedure View (aID: integer; aGUID: string); override;
  end;


//==================================================================
//==================================================================
implementation

{$R *.DFM}

//--------------------------------------------------------------------
procedure Tframe_BSC_View.FormCreate(Sender: TObject);
//--------------------------------------------------------------------
begin
  inherited;

  ObjectName:=OBJ_BSC;

  pn_Inspector.Align:= alClient;


  CreateChildForm(Tframe_DBInspector_Container, Ffrm_Inspector, pn_Inspector);
  Ffrm_Inspector.PrepareViewForObject( OBJ_BSC);


  AddComponentProp(pn_Lac, PROP_HEIGHT);
  cxPropertiesStore.RestoreFrom;


end;

//--------------------------------------------------------------------
procedure Tframe_BSC_View.FormDestroy(Sender: TObject);
//--------------------------------------------------------------------
begin


  inherited;
end;

//--------------------------------------------------------------------
procedure Tframe_BSC_View.View (aID: integer; aGUID: string);
//--------------------------------------------------------------------
//ar
//  bReadOnly: Boolean;
//  iGeoRegion_ID: Integer;
begin
 inherited;

  Ffrm_Inspector.View(aID);

  // -------------------------
//  iGeoRegion_ID := Ffrm_inspector.GetDataset().FieldByName(FLD_GeoRegion_ID).AsInteger;// GetIntFieldValue(FLD_GeoRegion_ID);
//  bReadOnly:=dmUser_Security.GeoRegion_is_ReadOnly(iGeoRegion_ID);

//  Ffrm_inspector.SetReadOnly(bReadOnly, IntToStr(iGeoRegion_ID));
 // -------------------------


end;

end.