unit d_BSC_add;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  rxPlacemnt, ActnList,    
  StdCtrls, ExtCtrls, cxButtonEdit,

  u_cx_vgrid,

  dm_MapEngine_store,

  d_Wizard_Add_with_params,

  dm_Main,

  I_Shell,
 // I_MapEngine,
 // dm_MapEngine,

 // I_MapAct,
 // dm_Act_Map,


  u_types,
  u_const_str,
  u_const_DB,

  u_dlg,
  u_db,
  u_Geo,

  dm_BSC,
  dm_Property,

   cxPropertiesStore, cxVGrid, cxControls, cxInplaceContainer,
  ComCtrls


  ;

type
  Tdlg_BSC_add = class(Tdlg_Wizard_add_with_params)
    cxVerticalGrid1: TcxVerticalGrid;
    row_Property: TcxEditorRow;
    cxVerticalGrid1CategoryRow1: TcxCategoryRow;
    row_MSC: TcxEditorRow;
    procedure FormCreate(Sender: TObject);
    procedure act_OkExecute(Sender: TObject);
  //  procedure row_Property1ButtonClick(Sender: TObject;
    //  AbsoluteIndex: Integer);
    procedure ActionList1Update(Action: TBasicAction;
      var Handled: Boolean);
    procedure cxVerticalGrid1EditorRow1EditPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
  private
    FID: integer;

    FRec: TdmBSCAddRec;

    procedure Add;

  public
     class function ExecDlg (//aFolderID: integer;
                             aPropertyID: integer;
                             aBLPoint: TBLPoint): integer;
  end;


//====================================================================
// implementation
//====================================================================
implementation {$R *.dfm}


//-------------------------------------------------------------------
class function Tdlg_BSC_add.ExecDlg(//aFolderID: integer;
                                    aPropertyID: integer;
                                    aBLPoint: TBLPoint): integer;
//-------------------------------------------------------------------
begin                            
  with Tdlg_BSC_add.Create(Application) do
  try
    if (aPropertyID = 0)  then
      aPropertyID:=dmProperty.GetNearestID(aBLPoint);

    FRec.PropertyID:=aPropertyID;
//    FRec.FolderID:=aFolderID;

    ed_Name_.Text      :=dmBSC.GetNewName();
    row_Property.Properties.Value :=dmProperty.GetNameByID (aPropertyID);

    if (ShowModal=mrOk) then
    begin
      Result:=FID;

      g_ShellEvents.Shell_UpdateNodeChildren_ByObjName (OBJ_PROPERTY, FRec.PropertyID);
    //  g_ShellEvents.Shell_UpdateNodeChildren_ByObjName (OBJ_BSC, FID);

      dmMapEngine_store.Feature_Add(OBJ_BSC, Result);


   //   dmMapEngine.CreateObject (otBSC, Result);
    //  dmAct_Map.MapRefresh;

    end
    else
      Result:=0;

  finally
    Free;
  end;
end;

//-------------------------------------------------------------------
procedure Tdlg_BSC_add.FormCreate(Sender: TObject);
//-------------------------------------------------------------------
begin
  inherited;
  SetActionName (STR_DLG_ADD_BSC);

  row_Property.Properties.Caption:=STR_PROPERTY;

  SetDefaultSize();

  cx_InitVerticalGrid(cxverticalGrid1);

end;


procedure Tdlg_BSC_add.act_OkExecute(Sender: TObject);
begin
  Add();
end;

//-------------------------------------------------------------------
procedure Tdlg_BSC_add.Add;
//-------------------------------------------------------------------
begin
  FRec.NewName:=ed_Name_.Text;

  FID:=dmBSC.Add (FRec);
end;


procedure Tdlg_BSC_add.ActionList1Update(Action: TBasicAction;  var Handled: Boolean);
begin
  act_Ok.Enabled:= (FRec.PropertyID > 0);
                  // and (FRec.MscID  > 0);
end;


procedure Tdlg_BSC_add.cxVerticalGrid1EditorRow1EditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  if cxVerticalGrid1.FocusedRow =row_Property then
    Dlg_SelectObject_cxButtonEdit (Sender as TcxButtonEdit, otProperty, FRec.PropertyID);

  if cxVerticalGrid1.FocusedRow =row_MSC then
    Dlg_SelectObject_cxButtonEdit (Sender as TcxButtonEdit,otMSC, FRec.MscID);


end;

end.




//-------------------------------------------------------------------
procedure Tdlg_BSC_add.row_Property1ButtonClick(Sender: TObject; AbsoluteIndex: Integer);
//-------------------------------------------------------------------
begin
(*  if Sender=row_Property then begin
    Dlg_SelectObject_dx(row_Property, otProperty, FRec.PropertyID);
  end else
//    Dlg_SelectObject (row_Property, otProperty, FRec.PropertyID) else

  if Sender=row_MSC then begin
    Dlg_SelectObject_dx(row_MSC, otMSC, FRec.MscID);
  end;*)
//    Dlg_SelectObject (row_MSC, otMSC, FRec.MscID);
end;