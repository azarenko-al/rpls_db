unit dm_act_BSC;

interface
{$I ver.inc}


uses
  Classes, Forms,  ActnList, Menus, 

//  I_Act_BSC,

  I_Object,

  u_func,
  u_geo,

  u_types,

  fr_BSC_view,
  d_BSC_add,

  dm_act_base,

  dm_BSC
  
  ;

type
  TdmAct_BSC = class(TdmAct_Base) //, IAct_BSC_X)
   // procedure DataModuleDestroy(Sender: TObject);
    procedure DataModuleCreate(Sender: TObject);
  //  procedure ApplicationEvents1Message(var Msg: tagMSG; var Handled: Boolean);
  private
    FBLPoint: TBLPoint;

    procedure Check;
    procedure ShowOnMap (aID: integer);

  protected
    procedure DoAction (Sender: TObject);
    procedure GetPopupMenu (aPopupMenu: TPopupMenu; aMenuType: TMenuType); override;
    function GetViewForm (aID: integer; aOwnerForm: TForm): TForm;

    function ItemDel(aID: integer; aName: string): boolean; override;
    function ItemAdd (aFolderID: integer): integer; override;

  //  procedure DoAfterItemDel(aID: integer);
  //  procedure DoOnMapRefresh (Sender: TObject);

  public
    procedure AddByPos(aBLPoint: TBLPoint);
    procedure Add (aFolderID: integer); override;
    procedure AddByProperty(aProperty: integer);

    class procedure Init;
  end;

var
  dmAct_BSC: TdmAct_BSC;

//==================================================================
//==================================================================
implementation


 {$R *.dfm}

const
  MSG_DEL_LINK_LINE='������� ?';


//--------------------------------------------------------------------
class procedure TdmAct_BSC.Init;
//--------------------------------------------------------------------
begin
  if not Assigned(dmAct_BSC) then
  begin
    dmAct_BSC:=TdmAct_BSC.Create(Application);

 //   dmAct_BSC.GetInterface(IAct_BSC_X, IAct_BSC);
   // Assert(Assigned(IAct_BSC));

  end;
end;

//
//procedure TdmAct_BSC.DataModuleDestroy(Sender: TObject);
//begin
////  IAct_BSC := nil;
////  dmAct_BSC:= nil;
//
//  inherited;
//end;


//-------------------------------------------------------------------
procedure TdmAct_BSC.DataModuleCreate(Sender: TObject);
//-------------------------------------------------------------------
var b: Boolean;
begin
  inherited;

  ObjectName:=OBJ_BSC;

  act_Add.Caption := '������� BSC';

  //OnAfterItemDel:=DoAfterItemDel;
 // OnMapRefresh:=DoOnMapRefresh;

  SetActionsExecuteProc ([
     act_Object_ShowOnMap
   ], DoAction);           

  SetCheckedActionsArr([
      act_Add
     ]);

end;


procedure TdmAct_BSC.Check;
begin

(*
  SetActionsEnabled
    ([
        act_Add
     ],
     not dmUser_Security.ProjectIsReadOnly);
*)
end;



//--------------------------------------------------------------------
procedure TdmAct_BSC.AddByProperty(aProperty: integer);
//--------------------------------------------------------------------
begin
  Tdlg_BSC_add.ExecDlg (aProperty, NULL_POINT);

(*  FBLPoint:=NULL_BLPOINT;
  FProperty:=aProperty;
  inherited Add (0);*)
end;



//--------------------------------------------------------------------
function TdmAct_BSC.GetViewForm (aID: integer; aOwnerForm: TForm): TForm;
//--------------------------------------------------------------------
begin
  Result:=Tframe_BSC_View.Create(aOwnerForm);
  Tframe_BSC_View(Result).ID:=aID;
end;

//--------------------------------------------------------------------
function TdmAct_BSC.ItemAdd (aFolderID: integer): integer;
//--------------------------------------------------------------------
begin
  Result:=Tdlg_BSC_add.ExecDlg ( 0, FBLPoint);
end;



//--------------------------------------------------------------------
function TdmAct_BSC.ItemDel(aID: integer; aName: string): boolean;
//--------------------------------------------------------------------
begin
  Result:=dmBSC.Del (aID);
end;


//--------------------------------------------------------------------
procedure TdmAct_BSC.Add (aFolderID: integer);
//--------------------------------------------------------------------
begin
  FBLPoint:=NULL_POINT;
  inherited Add(aFolderID);
end;


//--------------------------------------------------------------------
procedure TdmAct_BSC.AddByPos(aBLPoint: TBLPoint);
//--------------------------------------------------------------------
begin
  FBLPoint:=aBLPoint;
  inherited Add(0);
end;

//--------------------------------------------------------------------
// actions
//--------------------------------------------------------------------
procedure TdmAct_BSC.DoAction (Sender: TObject);
begin
  if Sender=act_Object_ShowOnMap then
    ShowOnMap (FFocusedID) else  ;

end;

//--------------------------------------------------------------------
procedure TdmAct_BSC.ShowOnMap (aID: integer);
//--------------------------------------------------------------------
begin
  ShowPointOnMap (dmBSC.GetPropertyPos(aID), aID);
end;

//--------------------------------------------------------------------
procedure TdmAct_BSC.GetPopupMenu;
//--------------------------------------------------------------------
begin
  //Check();

  case aMenuType of
    mtFolder: begin
                AddFolderMenu_Create (aPopupMenu);
                //AddMenuItem (aPopupMenu, act_Add);
                AddFolderPopupMenu (aPopupMenu);
              end;

    mtItem:  begin
               AddMenuItem (aPopupMenu, act_Del_);
                AddMenuItem (aPopupMenu, act_Object_ShowOnMap);
                AddMenuItem (aPopupMenu, nil);

                AddFolderMenu_Tools (aPopupMenu);

             end;
    mtList:  begin
               AddMenuItem (aPopupMenu, act_Del_list);
               AddFolderMenu_Tools (aPopupMenu);

             end;
  end;
end;
 
end.


