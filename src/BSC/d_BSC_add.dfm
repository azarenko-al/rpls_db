inherited dlg_BSC_add: Tdlg_BSC_add
  Left = 723
  Top = 358
  Caption = 'dlg_BSC_add'
  PixelsPerInch = 96
  TextHeight = 13
  inherited Panel2: TPanel
    inherited PageControl1: TPageControl
      inherited TabSheet1: TTabSheet
        object cxVerticalGrid1: TcxVerticalGrid
          Left = 0
          Top = 0
          Width = 536
          Height = 98
          Align = alClient
          LookAndFeel.Kind = lfStandard
          OptionsView.CellTextMaxLineCount = 3
          OptionsView.AutoScaleBands = False
          OptionsView.PaintStyle = psDelphi
          OptionsView.GridLineColor = clBtnShadow
          OptionsView.RowHeaderMinWidth = 30
          OptionsView.RowHeaderWidth = 253
          OptionsView.ValueWidth = 40
          TabOrder = 0
          object row_Property: TcxEditorRow
            Expanded = False
            Properties.Caption = #1055#1083#1086#1097#1072#1076#1082#1072
            Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
            Properties.EditProperties.Alignment.Horz = taLeftJustify
            Properties.EditProperties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            Properties.EditProperties.ReadOnly = True
            Properties.EditProperties.OnButtonClick = cxVerticalGrid1EditorRow1EditPropertiesButtonClick
            Properties.DataBinding.ValueType = 'String'
            Properties.Value = Null
          end
          object cxVerticalGrid1CategoryRow1: TcxCategoryRow
            Expanded = False
          end
          object row_MSC: TcxEditorRow
            Expanded = False
            Properties.Caption = 'MSC'
            Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
            Properties.EditProperties.Alignment.Horz = taLeftJustify
            Properties.EditProperties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            Properties.EditProperties.ReadOnly = True
            Properties.EditProperties.OnButtonClick = cxVerticalGrid1EditorRow1EditPropertiesButtonClick
            Properties.DataBinding.ValueType = 'String'
            Properties.Value = Null
          end
        end
      end
    end
  end
  inherited ActionList1: TActionList
    OnUpdate = ActionList1Update
  end
end
