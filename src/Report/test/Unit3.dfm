object Form2: TForm2
  Left = 316
  Top = 99
  Width = 1022
  Height = 647
  Caption = 'Form2'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object DBGrid1: TDBGrid
    Left = 0
    Top = 516
    Width = 1014
    Height = 97
    Align = alBottom
    DataSource = DataSource1
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Visible = False
  end
  object dxDBGrid1: TdxDBGrid
    Left = 16
    Top = 112
    Width = 521
    Height = 225
    Bands = <
      item
      end
      item
      end>
    DefaultLayout = False
    HeaderPanelRowCount = 2
    SummaryGroups = <>
    SummarySeparator = ', '
    TabOrder = 1
    DataSource = DataSource1
    Filter.Criteria = {00000000}
    object dxDBGrid1id: TdxDBGridMaskColumn
      BandIndex = 1
      RowIndex = 0
      FieldName = 'id'
    end
    object dxDBGrid1name: TdxDBGridMaskColumn
      Width = 116
      BandIndex = 0
      RowIndex = 0
      FieldName = 'name'
    end
    object dxDBGrid1project_id: TdxDBGridMaskColumn
      BandIndex = 1
      RowIndex = 0
      FieldName = 'project_id'
    end
    object dxDBGrid1clutter_model_id: TdxDBGridMaskColumn
      BandIndex = 1
      RowIndex = 0
      FieldName = 'clutter_model_id'
    end
    object dxDBGrid1folder_id: TdxDBGridMaskColumn
      BandIndex = 1
      RowIndex = 0
      FieldName = 'folder_id'
    end
    object dxDBGrid1linkend1_id: TdxDBGridMaskColumn
      BandIndex = 1
      RowIndex = 0
      FieldName = 'linkend1_id'
    end
    object dxDBGrid1objname: TdxDBGridMaskColumn
      BandIndex = 0
      RowIndex = 1
      FieldName = 'objname'
      StoredRowIndex = 1
    end
    object dxDBGrid1linkend2_id: TdxDBGridMaskColumn
      BandIndex = 1
      RowIndex = 0
      FieldName = 'linkend2_id'
    end
    object dxDBGrid1pmp_sector_id: TdxDBGridMaskColumn
      BandIndex = 1
      RowIndex = 0
      FieldName = 'pmp_sector_id'
    end
    object dxDBGrid1pmp_terminal_id: TdxDBGridMaskColumn
      BandIndex = 1
      RowIndex = 0
      FieldName = 'pmp_terminal_id'
    end
    object dxDBGrid1Property1_id: TdxDBGridMaskColumn
      BandIndex = 1
      RowIndex = 0
      FieldName = 'Property1_id'
    end
    object dxDBGrid1Property2_id: TdxDBGridMaskColumn
      BandIndex = 1
      RowIndex = 0
      FieldName = 'Property2_id'
    end
    object dxDBGrid1length: TdxDBGridMaskColumn
      BandIndex = 1
      RowIndex = 0
      FieldName = 'length'
    end
    object dxDBGrid1length_km: TdxDBGridMaskColumn
      BandIndex = 1
      RowIndex = 0
      FieldName = 'length_km'
    end
    object dxDBGrid1calc_method: TdxDBGridMaskColumn
      BandIndex = 1
      RowIndex = 0
      FieldName = 'calc_method'
    end
    object dxDBGrid1comments: TdxDBGridMaskColumn
      BandIndex = 1
      RowIndex = 0
      FieldName = 'comments'
    end
    object dxDBGrid1refraction: TdxDBGridMaskColumn
      BandIndex = 1
      RowIndex = 0
      FieldName = 'refraction'
    end
    object dxDBGrid1NFrenel: TdxDBGridMaskColumn
      BandIndex = 1
      RowIndex = 0
      FieldName = 'NFrenel'
    end
    object dxDBGrid1profile_step: TdxDBGridMaskColumn
      BandIndex = 1
      RowIndex = 0
      FieldName = 'profile_step'
    end
    object dxDBGrid1is_profile_reversed: TdxDBGridCheckColumn
      Width = 100
      BandIndex = 1
      RowIndex = 0
      FieldName = 'is_profile_reversed'
      ValueChecked = 'True'
      ValueUnchecked = 'False'
    end
    object dxDBGrid1_: TdxDBGridMaskColumn
      BandIndex = 1
      RowIndex = 0
      FieldName = '_'
    end
    object dxDBGrid1LOS_status: TdxDBGridMaskColumn
      BandIndex = 1
      RowIndex = 0
      FieldName = 'LOS_status'
    end
    object dxDBGrid1status: TdxDBGridMaskColumn
      BandIndex = 1
      RowIndex = 0
      FieldName = 'status'
    end
    object dxDBGrid1rx_level_dBm: TdxDBGridMaskColumn
      BandIndex = 1
      RowIndex = 0
      FieldName = 'rx_level_dBm'
    end
    object dxDBGrid1SESR: TdxDBGridMaskColumn
      BandIndex = 1
      RowIndex = 0
      FieldName = 'SESR'
    end
    object dxDBGrid1Kng: TdxDBGridMaskColumn
      BandIndex = 1
      RowIndex = 0
      FieldName = 'Kng'
    end
    object dxDBGrid1kng_year: TdxDBGridMaskColumn
      BandIndex = 1
      RowIndex = 0
      FieldName = 'kng_year'
    end
    object dxDBGrid1fade_margin_dB: TdxDBGridMaskColumn
      BandIndex = 1
      RowIndex = 0
      FieldName = 'fade_margin_dB'
    end
    object dxDBGrid1rain_intensity: TdxDBGridMaskColumn
      BandIndex = 1
      RowIndex = 0
      FieldName = 'rain_intensity'
    end
    object dxDBGrid1rain_IsCalcLength: TdxDBGridCheckColumn
      Width = 100
      BandIndex = 1
      RowIndex = 0
      FieldName = 'rain_IsCalcLength'
      ValueChecked = 'True'
      ValueUnchecked = 'False'
    end
    object dxDBGrid1Rain_Length_km: TdxDBGridMaskColumn
      BandIndex = 1
      RowIndex = 0
      FieldName = 'Rain_Length_km'
    end
    object dxDBGrid1rain_signal_quality: TdxDBGridMaskColumn
      BandIndex = 1
      RowIndex = 0
      FieldName = 'rain_signal_quality'
    end
    object dxDBGrid1rain_Weakening_vert: TdxDBGridMaskColumn
      BandIndex = 1
      RowIndex = 0
      FieldName = 'rain_Weakening_vert'
    end
    object dxDBGrid1rain_Weakening_horz: TdxDBGridMaskColumn
      BandIndex = 1
      RowIndex = 0
      FieldName = 'rain_Weakening_horz'
    end
    object dxDBGrid1rain_Allowable_Intense_vert: TdxDBGridMaskColumn
      BandIndex = 1
      RowIndex = 0
      FieldName = 'rain_Allowable_Intense_vert'
    end
    object dxDBGrid1rain_Allowable_Intense_horz: TdxDBGridMaskColumn
      BandIndex = 1
      RowIndex = 0
      FieldName = 'rain_Allowable_Intense_horz'
    end
    object dxDBGrid1rain_signal_depression_dB: TdxDBGridMaskColumn
      BandIndex = 1
      RowIndex = 0
      FieldName = 'rain_signal_depression_dB'
    end
    object dxDBGrid1tilt: TdxDBGridMaskColumn
      BandIndex = 1
      RowIndex = 0
      FieldName = 'tilt'
    end
    object dxDBGrid1_________: TdxDBGridMaskColumn
      BandIndex = 1
      RowIndex = 0
      FieldName = '_________'
    end
    object dxDBGrid1GST_type: TdxDBGridMaskColumn
      BandIndex = 1
      RowIndex = 0
      FieldName = 'GST_type'
    end
    object dxDBGrid1GST_SESR: TdxDBGridMaskColumn
      BandIndex = 1
      RowIndex = 0
      FieldName = 'GST_SESR'
    end
    object dxDBGrid1GST_KNG: TdxDBGridMaskColumn
      BandIndex = 1
      RowIndex = 0
      FieldName = 'GST_KNG'
    end
    object dxDBGrid1GST_length: TdxDBGridMaskColumn
      BandIndex = 1
      RowIndex = 0
      FieldName = 'GST_length'
    end
    object dxDBGrid1GST_equivalent_length_km: TdxDBGridMaskColumn
      BandIndex = 1
      RowIndex = 0
      FieldName = 'GST_equivalent_length_km'
    end
    object dxDBGrid1KNG_dop: TdxDBGridMaskColumn
      BandIndex = 1
      RowIndex = 0
      FieldName = 'KNG_dop'
    end
    object dxDBGrid1SESR_norm: TdxDBGridMaskColumn
      BandIndex = 1
      RowIndex = 0
      FieldName = 'SESR_norm'
    end
    object dxDBGrid1KNG_norm: TdxDBGridMaskColumn
      BandIndex = 1
      RowIndex = 0
      FieldName = 'KNG_norm'
    end
    object dxDBGrid1ESR_norm: TdxDBGridMaskColumn
      BandIndex = 1
      RowIndex = 0
      FieldName = 'ESR_norm'
    end
    object dxDBGrid1BBER_norm: TdxDBGridMaskColumn
      BandIndex = 1
      RowIndex = 0
      FieldName = 'BBER_norm'
    end
    object dxDBGrid1linkType_id: TdxDBGridMaskColumn
      BandIndex = 1
      RowIndex = 0
      FieldName = 'linkType_id'
    end
    object dxDBGrid1RRV_name: TdxDBGridMaskColumn
      BandIndex = 1
      RowIndex = 0
      FieldName = 'RRV_name'
    end
    object dxDBGrid1Column52: TdxDBGridMaskColumn
      BandIndex = 1
      RowIndex = 0
      FieldName = '--- '#1059#1089#1083#1086#1074#1080#1103' '#1088#1072#1089#1087#1088#1086#1089#1090#1088#1072#1085#1077#1085#1080#1103' '#1088#1072#1076#1080#1086#1074#1086#1083#1085' ('#1056#1056#1042') ----'
    end
    object dxDBGrid1air_temperature: TdxDBGridMaskColumn
      BandIndex = 1
      RowIndex = 0
      FieldName = 'air_temperature'
    end
    object dxDBGrid1atmosphere_pressure: TdxDBGridMaskColumn
      BandIndex = 1
      RowIndex = 0
      FieldName = 'atmosphere_pressure'
    end
    object dxDBGrid1gradient_diel: TdxDBGridMaskColumn
      BandIndex = 1
      RowIndex = 0
      FieldName = 'gradient_diel'
    end
    object dxDBGrid1gradient: TdxDBGridMaskColumn
      BandIndex = 1
      RowIndex = 0
      FieldName = 'gradient'
    end
    object dxDBGrid1terrain_type: TdxDBGridMaskColumn
      BandIndex = 1
      RowIndex = 0
      FieldName = 'terrain_type'
    end
    object dxDBGrid1underlying_terrain_type: TdxDBGridMaskColumn
      BandIndex = 1
      RowIndex = 0
      FieldName = 'underlying_terrain_type'
    end
    object dxDBGrid1Q_factor: TdxDBGridMaskColumn
      BandIndex = 1
      RowIndex = 0
      FieldName = 'Q_factor'
    end
    object dxDBGrid1climate_factor: TdxDBGridMaskColumn
      BandIndex = 1
      RowIndex = 0
      FieldName = 'climate_factor'
    end
    object dxDBGrid1factor_B: TdxDBGridMaskColumn
      BandIndex = 1
      RowIndex = 0
      FieldName = 'factor_B'
    end
    object dxDBGrid1factor_C: TdxDBGridMaskColumn
      BandIndex = 1
      RowIndex = 0
      FieldName = 'factor_C'
    end
    object dxDBGrid1factor_D: TdxDBGridMaskColumn
      BandIndex = 1
      RowIndex = 0
      FieldName = 'factor_D'
    end
    object dxDBGrid1rain_rate: TdxDBGridMaskColumn
      BandIndex = 1
      RowIndex = 0
      FieldName = 'rain_rate'
    end
    object dxDBGrid1steam_wet: TdxDBGridMaskColumn
      BandIndex = 1
      RowIndex = 0
      FieldName = 'steam_wet'
    end
    object dxDBGrid1BER_required: TdxDBGridMaskColumn
      BandIndex = 1
      RowIndex = 0
      FieldName = 'BER_required'
    end
    object dxDBGrid1ESR_required: TdxDBGridMaskColumn
      BandIndex = 1
      RowIndex = 0
      FieldName = 'ESR_required'
    end
    object dxDBGrid1BBER_required: TdxDBGridMaskColumn
      BandIndex = 1
      RowIndex = 0
      FieldName = 'BBER_required'
    end
    object dxDBGrid1space_limit: TdxDBGridMaskColumn
      BandIndex = 1
      RowIndex = 0
      FieldName = 'space_limit'
    end
    object dxDBGrid1space_limit_probability: TdxDBGridMaskColumn
      BandIndex = 1
      RowIndex = 0
      FieldName = 'space_limit_probability'
    end
    object dxDBGrid1margin_height: TdxDBGridMaskColumn
      BandIndex = 1
      RowIndex = 0
      FieldName = 'margin_height'
    end
    object dxDBGrid1margin_distance: TdxDBGridMaskColumn
      BandIndex = 1
      RowIndex = 0
      FieldName = 'margin_distance'
    end
    object dxDBGrid1sesr_subrefraction: TdxDBGridMaskColumn
      BandIndex = 1
      RowIndex = 0
      FieldName = 'sesr_subrefraction'
    end
    object dxDBGrid1sesr_rain: TdxDBGridMaskColumn
      BandIndex = 1
      RowIndex = 0
      FieldName = 'sesr_rain'
    end
    object dxDBGrid1sesr_interference: TdxDBGridMaskColumn
      BandIndex = 1
      RowIndex = 0
      FieldName = 'sesr_interference'
    end
    object dxDBGrid1___________: TdxDBGridMaskColumn
      BandIndex = 1
      RowIndex = 0
      FieldName = '___________'
    end
    object dxDBGrid1protection_type: TdxDBGridMaskColumn
      BandIndex = 1
      RowIndex = 0
      FieldName = 'protection_type'
    end
    object dxDBGrid1redundancy: TdxDBGridMaskColumn
      BandIndex = 1
      RowIndex = 0
      FieldName = 'redundancy'
    end
    object dxDBGrid1______________: TdxDBGridMaskColumn
      BandIndex = 1
      RowIndex = 0
      FieldName = '______________'
    end
    object dxDBGrid1status_id: TdxDBGridMaskColumn
      BandIndex = 1
      RowIndex = 0
      FieldName = 'status_id'
    end
    object dxDBGrid1_______: TdxDBGridCheckColumn
      Width = 100
      BandIndex = 1
      RowIndex = 0
      FieldName = '_______'
      ValueChecked = 'True'
      ValueUnchecked = 'False'
    end
    object dxDBGrid1profile_source: TdxDBGridMaskColumn
      BandIndex = 1
      RowIndex = 0
      FieldName = 'profile_source'
    end
    object dxDBGrid1profile_XML: TdxDBGridMemoColumn
      BandIndex = 1
      RowIndex = 0
      FieldName = 'profile_XML'
    end
    object dxDBGrid1profile_is_custom: TdxDBGridCheckColumn
      Width = 100
      BandIndex = 1
      RowIndex = 0
      FieldName = 'profile_is_custom'
      ValueChecked = 'True'
      ValueUnchecked = 'False'
    end
    object dxDBGrid1calc_results_xml: TdxDBGridMemoColumn
      BandIndex = 1
      RowIndex = 0
      FieldName = 'calc_results_xml'
    end
    object dxDBGrid1Name_SDB: TdxDBGridMaskColumn
      BandIndex = 1
      RowIndex = 0
      FieldName = 'Name_SDB'
    end
    object dxDBGrid1Name_TN: TdxDBGridMaskColumn
      BandIndex = 1
      RowIndex = 0
      FieldName = 'Name_TN'
    end
    object dxDBGrid1repeater_linkend_id: TdxDBGridMaskColumn
      BandIndex = 1
      RowIndex = 0
      FieldName = 'repeater_linkend_id'
    end
    object dxDBGrid1repeater_property_id: TdxDBGridMaskColumn
      BandIndex = 1
      RowIndex = 0
      FieldName = 'repeater_property_id'
    end
    object dxDBGrid1guid: TdxDBGridColumn
      BandIndex = 1
      RowIndex = 0
      FieldName = 'guid'
    end
    object dxDBGrid1date_created: TdxDBGridDateColumn
      BandIndex = 1
      RowIndex = 0
      FieldName = 'date_created'
    end
    object dxDBGrid1date_modify: TdxDBGridDateColumn
      BandIndex = 1
      RowIndex = 0
      FieldName = 'date_modify'
    end
    object dxDBGrid1user_created: TdxDBGridMaskColumn
      BandIndex = 1
      RowIndex = 0
      FieldName = 'user_created'
    end
    object dxDBGrid1user_modify: TdxDBGridMaskColumn
      BandIndex = 1
      RowIndex = 0
      FieldName = 'user_modify'
    end
    object dxDBGrid1precision_of_condition_V_diffration_is_equal_to_V_min_dB: TdxDBGridMaskColumn
      BandIndex = 1
      RowIndex = 0
      FieldName = 'precision_of_condition_V_diffration_is_equal_to_V_min_dB'
    end
    object dxDBGrid1azimuth1: TdxDBGridMaskColumn
      BandIndex = 1
      RowIndex = 0
      FieldName = 'azimuth1'
    end
    object dxDBGrid1azimuth2: TdxDBGridMaskColumn
      BandIndex = 1
      RowIndex = 0
      FieldName = 'azimuth2'
    end
    object dxDBGrid1is_UsePassiveElements: TdxDBGridCheckColumn
      Width = 100
      BandIndex = 1
      RowIndex = 0
      FieldName = 'is_UsePassiveElements'
      ValueChecked = 'True'
      ValueUnchecked = 'False'
    end
    object dxDBGrid1Is_CalcWithAdditionalRain: TdxDBGridCheckColumn
      Width = 100
      BandIndex = 1
      RowIndex = 0
      FieldName = 'Is_CalcWithAdditionalRain'
      ValueChecked = 'True'
      ValueUnchecked = 'False'
    end
    object dxDBGrid1NIIR1998_rain_region_number: TdxDBGridMaskColumn
      BandIndex = 1
      RowIndex = 0
      FieldName = 'NIIR1998_rain_region_number'
    end
    object dxDBGrid1NIIR1998_Qd_region_number: TdxDBGridMaskColumn
      BandIndex = 1
      RowIndex = 0
      FieldName = 'NIIR1998_Qd_region_number'
    end
    object dxDBGrid1NIIR1998_gradient: TdxDBGridMaskColumn
      BandIndex = 1
      RowIndex = 0
      FieldName = 'NIIR1998_gradient'
    end
    object dxDBGrid1NIIR1998_numidity: TdxDBGridMaskColumn
      BandIndex = 1
      RowIndex = 0
      FieldName = 'NIIR1998_numidity'
    end
    object dxDBGrid1Property1name: TdxDBGridMaskColumn
      BandIndex = 1
      RowIndex = 0
      FieldName = 'Property1.name'
    end
    object dxDBGrid1Property2name: TdxDBGridMaskColumn
      BandIndex = 1
      RowIndex = 0
      FieldName = 'Property2.name'
    end
    object dxDBGrid1Property1address: TdxDBGridMaskColumn
      BandIndex = 1
      RowIndex = 0
      FieldName = 'Property1.address'
    end
    object dxDBGrid1Property2address: TdxDBGridMaskColumn
      BandIndex = 1
      RowIndex = 0
      FieldName = 'Property2.address'
    end
    object dxDBGrid1Property2lat: TdxDBGridMaskColumn
      BandIndex = 1
      RowIndex = 0
      FieldName = 'Property2.lat'
    end
    object dxDBGrid1Property2lon: TdxDBGridMaskColumn
      BandIndex = 1
      RowIndex = 0
      FieldName = 'Property2.lon'
    end
    object dxDBGrid1Property1lat: TdxDBGridMaskColumn
      BandIndex = 1
      RowIndex = 0
      FieldName = 'Property1.lat'
    end
    object dxDBGrid1Property1lon: TdxDBGridMaskColumn
      BandIndex = 1
      RowIndex = 0
      FieldName = 'Property1.lon'
    end
  end
  object cxGrid1: TcxGrid
    Left = 16
    Top = 288
    Width = 929
    Height = 200
    TabOrder = 2
    LookAndFeel.Kind = lfStandard
    object cxGrid1DBBandedTableView1: TcxGridDBBandedTableView
      DataController.DataSource = DataSource1
      DataController.Filter.MaxValueListCount = 1000
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      NavigatorButtons.ConfirmDelete = False
      Filtering.MaxDropDownCount = 12
      OptionsSelection.HideFocusRectOnExit = False
      OptionsSelection.InvertSelect = False
      OptionsView.GroupByBox = False
      OptionsView.GroupFooters = gfVisibleWhenExpanded
      OptionsView.BandHeaderLineCount = 2
      OptionsView.BandHeaders = False
      Preview.AutoHeight = False
      Preview.MaxLineCount = 1
      Bands = <
        item
        end
        item
        end
        item
        end>
      object cxGrid1DBBandedTableView1Property1name: TcxGridDBBandedColumn
        DataBinding.FieldName = 'Property1.name'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 100
        Properties.ReadOnly = False
        Options.Filtering = False
        Width = 186
        Position.BandIndex = 0
        Position.ColIndex = 0
        Position.RowIndex = 1
      end
      object cxGrid1DBBandedTableView1Property2name: TcxGridDBBandedColumn
        DataBinding.FieldName = 'Property2.name'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 100
        Properties.ReadOnly = False
        Options.Filtering = False
        Width = 186
        Position.BandIndex = 0
        Position.ColIndex = 0
        Position.RowIndex = 0
      end
      object cxGrid1DBBandedTableView1Property1address: TcxGridDBBandedColumn
        DataBinding.FieldName = 'Property1.address'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 250
        Properties.ReadOnly = False
        Options.Filtering = False
        Width = 387
        Position.BandIndex = 0
        Position.ColIndex = 1
        Position.RowIndex = 0
      end
      object cxGrid1DBBandedTableView1Property2address: TcxGridDBBandedColumn
        DataBinding.FieldName = 'Property2.address'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 250
        Properties.ReadOnly = False
        Options.Filtering = False
        Width = 387
        Position.BandIndex = 0
        Position.ColIndex = 1
        Position.RowIndex = 1
      end
      object cxGrid1DBBandedTableView1id: TcxGridDBBandedColumn
        DataBinding.FieldName = 'id'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = False
        Options.Filtering = False
        Position.BandIndex = 2
        Position.ColIndex = 0
        Position.LineCount = 2
        Position.RowIndex = 0
      end
      object cxGrid1DBBandedTableView1name: TcxGridDBBandedColumn
        DataBinding.FieldName = 'name'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 100
        Properties.ReadOnly = False
        Options.Filtering = False
        Width = 139
        Position.BandIndex = 1
        Position.ColIndex = 0
        Position.RowIndex = 0
      end
      object cxGrid1DBBandedTableView1project_id: TcxGridDBBandedColumn
        DataBinding.FieldName = 'project_id'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = False
        Options.Filtering = False
        Position.BandIndex = 2
        Position.ColIndex = 2
        Position.RowIndex = 0
      end
      object cxGrid1DBBandedTableView1clutter_model_id: TcxGridDBBandedColumn
        DataBinding.FieldName = 'clutter_model_id'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = False
        Options.Filtering = False
        Width = 83
        Position.BandIndex = 2
        Position.ColIndex = 3
        Position.RowIndex = 0
      end
      object cxGrid1DBBandedTableView1folder_id: TcxGridDBBandedColumn
        DataBinding.FieldName = 'folder_id'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = False
        Options.Filtering = False
        Position.BandIndex = 2
        Position.ColIndex = 4
        Position.RowIndex = 0
      end
      object cxGrid1DBBandedTableView1objname: TcxGridDBBandedColumn
        DataBinding.FieldName = 'objname'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 20
        Properties.ReadOnly = False
        Options.Filtering = False
        Position.BandIndex = 1
        Position.ColIndex = 0
        Position.RowIndex = 1
      end
      object cxGrid1DBBandedTableView1linkend1_id: TcxGridDBBandedColumn
        DataBinding.FieldName = 'linkend1_id'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = False
        Options.Filtering = False
        Position.BandIndex = 2
        Position.ColIndex = 5
        Position.RowIndex = 0
      end
      object cxGrid1DBBandedTableView1linkend2_id: TcxGridDBBandedColumn
        DataBinding.FieldName = 'linkend2_id'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = False
        Options.Filtering = False
        Position.BandIndex = 2
        Position.ColIndex = 6
        Position.RowIndex = 0
      end
      object cxGrid1DBBandedTableView1pmp_sector_id: TcxGridDBBandedColumn
        DataBinding.FieldName = 'pmp_sector_id'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = False
        Options.Filtering = False
        Width = 75
        Position.BandIndex = 2
        Position.ColIndex = 7
        Position.RowIndex = 0
      end
      object cxGrid1DBBandedTableView1pmp_terminal_id: TcxGridDBBandedColumn
        DataBinding.FieldName = 'pmp_terminal_id'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = False
        Options.Filtering = False
        Width = 82
        Position.BandIndex = 2
        Position.ColIndex = 8
        Position.RowIndex = 0
      end
      object cxGrid1DBBandedTableView1Property1_id: TcxGridDBBandedColumn
        DataBinding.FieldName = 'Property1_id'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = False
        Options.Filtering = False
        Width = 65
        Position.BandIndex = 2
        Position.ColIndex = 9
        Position.RowIndex = 0
      end
      object cxGrid1DBBandedTableView1Property2_id: TcxGridDBBandedColumn
        DataBinding.FieldName = 'Property2_id'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = False
        Options.Filtering = False
        Width = 65
        Position.BandIndex = 2
        Position.ColIndex = 10
        Position.RowIndex = 0
      end
      object cxGrid1DBBandedTableView1length: TcxGridDBBandedColumn
        DataBinding.FieldName = 'length'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = False
        Options.Filtering = False
        Position.BandIndex = 2
        Position.ColIndex = 11
        Position.RowIndex = 0
      end
      object cxGrid1DBBandedTableView1length_km: TcxGridDBBandedColumn
        DataBinding.FieldName = 'length_km'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = False
        Options.Filtering = False
        Position.BandIndex = 2
        Position.ColIndex = 12
        Position.RowIndex = 0
      end
      object cxGrid1DBBandedTableView1calc_method: TcxGridDBBandedColumn
        DataBinding.FieldName = 'calc_method'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = False
        Options.Filtering = False
        Width = 67
        Position.BandIndex = 2
        Position.ColIndex = 13
        Position.RowIndex = 0
      end
      object cxGrid1DBBandedTableView1comments: TcxGridDBBandedColumn
        DataBinding.FieldName = 'comments'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 250
        Properties.ReadOnly = False
        Options.Filtering = False
        Position.BandIndex = 2
        Position.ColIndex = 14
        Position.RowIndex = 0
      end
      object cxGrid1DBBandedTableView1refraction: TcxGridDBBandedColumn
        DataBinding.FieldName = 'refraction'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = False
        Options.Filtering = False
        Position.BandIndex = 2
        Position.ColIndex = 15
        Position.RowIndex = 0
      end
      object cxGrid1DBBandedTableView1NFrenel: TcxGridDBBandedColumn
        DataBinding.FieldName = 'NFrenel'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = False
        Options.Filtering = False
        Position.BandIndex = 2
        Position.ColIndex = 16
        Position.RowIndex = 0
      end
      object cxGrid1DBBandedTableView1profile_step: TcxGridDBBandedColumn
        DataBinding.FieldName = 'profile_step'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = False
        Options.Filtering = False
        Position.BandIndex = 2
        Position.ColIndex = 17
        Position.RowIndex = 0
      end
      object cxGrid1DBBandedTableView1is_profile_reversed: TcxGridDBBandedColumn
        DataBinding.FieldName = 'is_profile_reversed'
        PropertiesClassName = 'TcxCheckBoxProperties'
        Properties.Alignment = taLeftJustify
        Properties.NullStyle = nssUnchecked
        Properties.ReadOnly = False
        Properties.ValueChecked = 'True'
        Properties.ValueGrayed = ''
        Properties.ValueUnchecked = 'False'
        MinWidth = 16
        Options.Filtering = False
        Width = 100
        Position.BandIndex = 2
        Position.ColIndex = 18
        Position.RowIndex = 0
      end
      object cxGrid1DBBandedTableView1_: TcxGridDBBandedColumn
        DataBinding.FieldName = '_'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 1
        Properties.ReadOnly = False
        Options.Filtering = False
        Position.BandIndex = 2
        Position.ColIndex = 19
        Position.RowIndex = 0
      end
      object cxGrid1DBBandedTableView1LOS_status: TcxGridDBBandedColumn
        DataBinding.FieldName = 'LOS_status'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = False
        Options.Filtering = False
        Position.BandIndex = 2
        Position.ColIndex = 20
        Position.RowIndex = 0
      end
      object cxGrid1DBBandedTableView1status: TcxGridDBBandedColumn
        DataBinding.FieldName = 'status'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = False
        Options.Filtering = False
        Position.BandIndex = 2
        Position.ColIndex = 21
        Position.RowIndex = 0
      end
      object cxGrid1DBBandedTableView1rx_level_dBm: TcxGridDBBandedColumn
        DataBinding.FieldName = 'rx_level_dBm'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = False
        Options.Filtering = False
        Width = 69
        Position.BandIndex = 2
        Position.ColIndex = 22
        Position.RowIndex = 0
      end
      object cxGrid1DBBandedTableView1SESR: TcxGridDBBandedColumn
        DataBinding.FieldName = 'SESR'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = False
        Options.Filtering = False
        Position.BandIndex = 2
        Position.ColIndex = 23
        Position.RowIndex = 0
      end
      object cxGrid1DBBandedTableView1Kng: TcxGridDBBandedColumn
        DataBinding.FieldName = 'Kng'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = False
        Options.Filtering = False
        Position.BandIndex = 2
        Position.ColIndex = 24
        Position.RowIndex = 0
      end
      object cxGrid1DBBandedTableView1kng_year: TcxGridDBBandedColumn
        DataBinding.FieldName = 'kng_year'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = False
        Options.Filtering = False
        Position.BandIndex = 2
        Position.ColIndex = 25
        Position.RowIndex = 0
      end
      object cxGrid1DBBandedTableView1fade_margin_dB: TcxGridDBBandedColumn
        DataBinding.FieldName = 'fade_margin_dB'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = False
        Options.Filtering = False
        Width = 83
        Position.BandIndex = 2
        Position.ColIndex = 26
        Position.RowIndex = 0
      end
      object cxGrid1DBBandedTableView1rain_intensity: TcxGridDBBandedColumn
        DataBinding.FieldName = 'rain_intensity'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = False
        Options.Filtering = False
        Width = 67
        Position.BandIndex = 2
        Position.ColIndex = 27
        Position.RowIndex = 0
      end
      object cxGrid1DBBandedTableView1rain_IsCalcLength: TcxGridDBBandedColumn
        DataBinding.FieldName = 'rain_IsCalcLength'
        PropertiesClassName = 'TcxCheckBoxProperties'
        Properties.Alignment = taLeftJustify
        Properties.NullStyle = nssUnchecked
        Properties.ReadOnly = False
        Properties.ValueChecked = 'True'
        Properties.ValueGrayed = ''
        Properties.ValueUnchecked = 'False'
        MinWidth = 16
        Options.Filtering = False
        Width = 100
        Position.BandIndex = 2
        Position.ColIndex = 28
        Position.RowIndex = 0
      end
      object cxGrid1DBBandedTableView1Rain_Length_km: TcxGridDBBandedColumn
        DataBinding.FieldName = 'Rain_Length_km'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = False
        Options.Filtering = False
        Width = 87
        Position.BandIndex = 2
        Position.ColIndex = 29
        Position.RowIndex = 0
      end
      object cxGrid1DBBandedTableView1rain_signal_quality: TcxGridDBBandedColumn
        DataBinding.FieldName = 'rain_signal_quality'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 53
        Properties.ReadOnly = False
        Options.Filtering = False
        Position.BandIndex = 2
        Position.ColIndex = 30
        Position.RowIndex = 0
      end
      object cxGrid1DBBandedTableView1rain_Weakening_vert: TcxGridDBBandedColumn
        DataBinding.FieldName = 'rain_Weakening_vert'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = False
        Options.Filtering = False
        Width = 108
        Position.BandIndex = 2
        Position.ColIndex = 31
        Position.RowIndex = 0
      end
      object cxGrid1DBBandedTableView1rain_Weakening_horz: TcxGridDBBandedColumn
        DataBinding.FieldName = 'rain_Weakening_horz'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = False
        Options.Filtering = False
        Width = 110
        Position.BandIndex = 2
        Position.ColIndex = 32
        Position.RowIndex = 0
      end
      object cxGrid1DBBandedTableView1rain_Allowable_Intense_vert: TcxGridDBBandedColumn
        DataBinding.FieldName = 'rain_Allowable_Intense_vert'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = False
        Options.Filtering = False
        Width = 139
        Position.BandIndex = 2
        Position.ColIndex = 33
        Position.RowIndex = 0
      end
      object cxGrid1DBBandedTableView1rain_Allowable_Intense_horz: TcxGridDBBandedColumn
        DataBinding.FieldName = 'rain_Allowable_Intense_horz'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = False
        Options.Filtering = False
        Width = 141
        Position.BandIndex = 2
        Position.ColIndex = 34
        Position.RowIndex = 0
      end
      object cxGrid1DBBandedTableView1rain_signal_depression_dB: TcxGridDBBandedColumn
        DataBinding.FieldName = 'rain_signal_depression_dB'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = False
        Options.Filtering = False
        Width = 132
        Position.BandIndex = 2
        Position.ColIndex = 35
        Position.RowIndex = 0
      end
      object cxGrid1DBBandedTableView1tilt: TcxGridDBBandedColumn
        DataBinding.FieldName = 'tilt'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = False
        Options.Filtering = False
        Position.BandIndex = 2
        Position.ColIndex = 36
        Position.RowIndex = 0
      end
      object cxGrid1DBBandedTableView1_________: TcxGridDBBandedColumn
        DataBinding.FieldName = '_________'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 1
        Properties.ReadOnly = False
        Options.Filtering = False
        Width = 60
        Position.BandIndex = 2
        Position.ColIndex = 37
        Position.RowIndex = 0
      end
      object cxGrid1DBBandedTableView1GST_type: TcxGridDBBandedColumn
        DataBinding.FieldName = 'GST_type'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = False
        Options.Filtering = False
        Position.BandIndex = 2
        Position.ColIndex = 38
        Position.RowIndex = 0
      end
      object cxGrid1DBBandedTableView1GST_SESR: TcxGridDBBandedColumn
        DataBinding.FieldName = 'GST_SESR'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = False
        Options.Filtering = False
        Position.BandIndex = 2
        Position.ColIndex = 39
        Position.RowIndex = 0
      end
      object cxGrid1DBBandedTableView1GST_KNG: TcxGridDBBandedColumn
        DataBinding.FieldName = 'GST_KNG'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = False
        Options.Filtering = False
        Position.BandIndex = 2
        Position.ColIndex = 40
        Position.RowIndex = 0
      end
      object cxGrid1DBBandedTableView1GST_length: TcxGridDBBandedColumn
        DataBinding.FieldName = 'GST_length'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = False
        Options.Filtering = False
        Position.BandIndex = 2
        Position.ColIndex = 41
        Position.RowIndex = 0
      end
      object cxGrid1DBBandedTableView1GST_equivalent_length_km: TcxGridDBBandedColumn
        DataBinding.FieldName = 'GST_equivalent_length_km'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = False
        Options.Filtering = False
        Width = 138
        Position.BandIndex = 2
        Position.ColIndex = 42
        Position.RowIndex = 0
      end
      object cxGrid1DBBandedTableView1KNG_dop: TcxGridDBBandedColumn
        DataBinding.FieldName = 'KNG_dop'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = False
        Options.Filtering = False
        Position.BandIndex = 2
        Position.ColIndex = 43
        Position.RowIndex = 0
      end
      object cxGrid1DBBandedTableView1SESR_norm: TcxGridDBBandedColumn
        DataBinding.FieldName = 'SESR_norm'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = False
        Options.Filtering = False
        Position.BandIndex = 2
        Position.ColIndex = 44
        Position.RowIndex = 0
      end
      object cxGrid1DBBandedTableView1KNG_norm: TcxGridDBBandedColumn
        DataBinding.FieldName = 'KNG_norm'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = False
        Options.Filtering = False
        Position.BandIndex = 2
        Position.ColIndex = 45
        Position.RowIndex = 0
      end
      object cxGrid1DBBandedTableView1ESR_norm: TcxGridDBBandedColumn
        DataBinding.FieldName = 'ESR_norm'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = False
        Options.Filtering = False
        Position.BandIndex = 2
        Position.ColIndex = 46
        Position.RowIndex = 0
      end
      object cxGrid1DBBandedTableView1BBER_norm: TcxGridDBBandedColumn
        DataBinding.FieldName = 'BBER_norm'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = False
        Options.Filtering = False
        Position.BandIndex = 2
        Position.ColIndex = 47
        Position.RowIndex = 0
      end
      object cxGrid1DBBandedTableView1linkType_id: TcxGridDBBandedColumn
        DataBinding.FieldName = 'linkType_id'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = False
        Options.Filtering = False
        Position.BandIndex = 2
        Position.ColIndex = 48
        Position.RowIndex = 0
      end
      object cxGrid1DBBandedTableView1RRV_name: TcxGridDBBandedColumn
        DataBinding.FieldName = 'RRV_name'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 50
        Properties.ReadOnly = False
        Options.Filtering = False
        Position.BandIndex = 2
        Position.ColIndex = 49
        Position.RowIndex = 0
      end
      object cxGrid1DBBandedTableView1DBBandedColumn: TcxGridDBBandedColumn
        DataBinding.FieldName = '--- '#1059#1089#1083#1086#1074#1080#1103' '#1088#1072#1089#1087#1088#1086#1089#1090#1088#1072#1085#1077#1085#1080#1103' '#1088#1072#1076#1080#1086#1074#1086#1083#1085' ('#1056#1056#1042') ----'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 1
        Properties.ReadOnly = False
        Options.Filtering = False
        Width = 256
        Position.BandIndex = 2
        Position.ColIndex = 50
        Position.RowIndex = 0
      end
      object cxGrid1DBBandedTableView1air_temperature: TcxGridDBBandedColumn
        DataBinding.FieldName = 'air_temperature'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = False
        Options.Filtering = False
        Width = 79
        Position.BandIndex = 2
        Position.ColIndex = 51
        Position.RowIndex = 0
      end
      object cxGrid1DBBandedTableView1atmosphere_pressure: TcxGridDBBandedColumn
        DataBinding.FieldName = 'atmosphere_pressure'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = False
        Options.Filtering = False
        Width = 107
        Position.BandIndex = 2
        Position.ColIndex = 52
        Position.RowIndex = 0
      end
      object cxGrid1DBBandedTableView1gradient_diel: TcxGridDBBandedColumn
        DataBinding.FieldName = 'gradient_diel'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = False
        Options.Filtering = False
        Width = 66
        Position.BandIndex = 2
        Position.ColIndex = 53
        Position.RowIndex = 0
      end
      object cxGrid1DBBandedTableView1gradient: TcxGridDBBandedColumn
        DataBinding.FieldName = 'gradient'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = False
        Options.Filtering = False
        Position.BandIndex = 2
        Position.ColIndex = 54
        Position.RowIndex = 0
      end
      object cxGrid1DBBandedTableView1terrain_type: TcxGridDBBandedColumn
        DataBinding.FieldName = 'terrain_type'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = False
        Options.Filtering = False
        Position.BandIndex = 2
        Position.ColIndex = 55
        Position.RowIndex = 0
      end
      object cxGrid1DBBandedTableView1underlying_terrain_type: TcxGridDBBandedColumn
        DataBinding.FieldName = 'underlying_terrain_type'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = False
        Options.Filtering = False
        Width = 115
        Position.BandIndex = 2
        Position.ColIndex = 56
        Position.RowIndex = 0
      end
      object cxGrid1DBBandedTableView1Q_factor: TcxGridDBBandedColumn
        DataBinding.FieldName = 'Q_factor'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = False
        Options.Filtering = False
        Position.BandIndex = 2
        Position.ColIndex = 57
        Position.RowIndex = 0
      end
      object cxGrid1DBBandedTableView1climate_factor: TcxGridDBBandedColumn
        DataBinding.FieldName = 'climate_factor'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = False
        Options.Filtering = False
        Width = 72
        Position.BandIndex = 2
        Position.ColIndex = 58
        Position.RowIndex = 0
      end
      object cxGrid1DBBandedTableView1factor_B: TcxGridDBBandedColumn
        DataBinding.FieldName = 'factor_B'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = False
        Options.Filtering = False
        Position.BandIndex = 2
        Position.ColIndex = 59
        Position.RowIndex = 0
      end
      object cxGrid1DBBandedTableView1factor_C: TcxGridDBBandedColumn
        DataBinding.FieldName = 'factor_C'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = False
        Options.Filtering = False
        Position.BandIndex = 2
        Position.ColIndex = 60
        Position.RowIndex = 0
      end
      object cxGrid1DBBandedTableView1factor_D: TcxGridDBBandedColumn
        DataBinding.FieldName = 'factor_D'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = False
        Options.Filtering = False
        Position.BandIndex = 2
        Position.ColIndex = 61
        Position.RowIndex = 0
      end
      object cxGrid1DBBandedTableView1rain_rate: TcxGridDBBandedColumn
        DataBinding.FieldName = 'rain_rate'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = False
        Options.Filtering = False
        Position.BandIndex = 2
        Position.ColIndex = 62
        Position.RowIndex = 0
      end
      object cxGrid1DBBandedTableView1steam_wet: TcxGridDBBandedColumn
        DataBinding.FieldName = 'steam_wet'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = False
        Options.Filtering = False
        Position.BandIndex = 2
        Position.ColIndex = 63
        Position.RowIndex = 0
      end
      object cxGrid1DBBandedTableView1BER_required: TcxGridDBBandedColumn
        DataBinding.FieldName = 'BER_required'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = False
        Options.Filtering = False
        Width = 72
        Position.BandIndex = 2
        Position.ColIndex = 64
        Position.RowIndex = 0
      end
      object cxGrid1DBBandedTableView1ESR_required: TcxGridDBBandedColumn
        DataBinding.FieldName = 'ESR_required'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = False
        Options.Filtering = False
        Width = 72
        Position.BandIndex = 2
        Position.ColIndex = 65
        Position.RowIndex = 0
      end
      object cxGrid1DBBandedTableView1BBER_required: TcxGridDBBandedColumn
        DataBinding.FieldName = 'BBER_required'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = False
        Options.Filtering = False
        Width = 79
        Position.BandIndex = 2
        Position.ColIndex = 66
        Position.RowIndex = 0
      end
      object cxGrid1DBBandedTableView1space_limit: TcxGridDBBandedColumn
        DataBinding.FieldName = 'space_limit'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = False
        Options.Filtering = False
        Position.BandIndex = 2
        Position.ColIndex = 67
        Position.RowIndex = 0
      end
      object cxGrid1DBBandedTableView1space_limit_probability: TcxGridDBBandedColumn
        DataBinding.FieldName = 'space_limit_probability'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = False
        Options.Filtering = False
        Width = 111
        Position.BandIndex = 2
        Position.ColIndex = 68
        Position.RowIndex = 0
      end
      object cxGrid1DBBandedTableView1margin_height: TcxGridDBBandedColumn
        DataBinding.FieldName = 'margin_height'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = False
        Options.Filtering = False
        Width = 72
        Position.BandIndex = 2
        Position.ColIndex = 69
        Position.RowIndex = 0
      end
      object cxGrid1DBBandedTableView1margin_distance: TcxGridDBBandedColumn
        DataBinding.FieldName = 'margin_distance'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = False
        Options.Filtering = False
        Width = 83
        Position.BandIndex = 2
        Position.ColIndex = 70
        Position.RowIndex = 0
      end
      object cxGrid1DBBandedTableView1sesr_subrefraction: TcxGridDBBandedColumn
        DataBinding.FieldName = 'sesr_subrefraction'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = False
        Options.Filtering = False
        Width = 92
        Position.BandIndex = 2
        Position.ColIndex = 71
        Position.RowIndex = 0
      end
      object cxGrid1DBBandedTableView1sesr_rain: TcxGridDBBandedColumn
        DataBinding.FieldName = 'sesr_rain'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = False
        Options.Filtering = False
        Position.BandIndex = 2
        Position.ColIndex = 72
        Position.RowIndex = 0
      end
      object cxGrid1DBBandedTableView1sesr_interference: TcxGridDBBandedColumn
        DataBinding.FieldName = 'sesr_interference'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = False
        Options.Filtering = False
        Width = 87
        Position.BandIndex = 2
        Position.ColIndex = 73
        Position.RowIndex = 0
      end
      object cxGrid1DBBandedTableView1___________: TcxGridDBBandedColumn
        DataBinding.FieldName = '___________'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 1
        Properties.ReadOnly = False
        Options.Filtering = False
        Width = 72
        Position.BandIndex = 2
        Position.ColIndex = 74
        Position.RowIndex = 0
      end
      object cxGrid1DBBandedTableView1protection_type: TcxGridDBBandedColumn
        DataBinding.FieldName = 'protection_type'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 10
        Properties.ReadOnly = False
        Options.Filtering = False
        Width = 79
        Position.BandIndex = 2
        Position.ColIndex = 75
        Position.RowIndex = 0
      end
      object cxGrid1DBBandedTableView1redundancy: TcxGridDBBandedColumn
        DataBinding.FieldName = 'redundancy'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 50
        Properties.ReadOnly = False
        Options.Filtering = False
        Position.BandIndex = 2
        Position.ColIndex = 76
        Position.RowIndex = 0
      end
      object cxGrid1DBBandedTableView1______________: TcxGridDBBandedColumn
        DataBinding.FieldName = '______________'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 1
        Properties.ReadOnly = False
        Options.Filtering = False
        Width = 90
        Position.BandIndex = 2
        Position.ColIndex = 77
        Position.RowIndex = 0
      end
      object cxGrid1DBBandedTableView1status_id: TcxGridDBBandedColumn
        DataBinding.FieldName = 'status_id'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = False
        Options.Filtering = False
        Position.BandIndex = 2
        Position.ColIndex = 78
        Position.RowIndex = 0
      end
      object cxGrid1DBBandedTableView1_______: TcxGridDBBandedColumn
        DataBinding.FieldName = '_______'
        PropertiesClassName = 'TcxCheckBoxProperties'
        Properties.Alignment = taLeftJustify
        Properties.NullStyle = nssUnchecked
        Properties.ReadOnly = False
        Properties.ValueChecked = 'True'
        Properties.ValueGrayed = ''
        Properties.ValueUnchecked = 'False'
        MinWidth = 16
        Options.Filtering = False
        Width = 100
        Position.BandIndex = 2
        Position.ColIndex = 79
        Position.RowIndex = 0
      end
      object cxGrid1DBBandedTableView1profile_source: TcxGridDBBandedColumn
        DataBinding.FieldName = 'profile_source'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = False
        Options.Filtering = False
        Width = 72
        Position.BandIndex = 2
        Position.ColIndex = 80
        Position.RowIndex = 0
      end
      object cxGrid1DBBandedTableView1profile_XML: TcxGridDBBandedColumn
        DataBinding.FieldName = 'profile_XML'
        PropertiesClassName = 'TcxMemoProperties'
        Properties.Alignment = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = False
        Options.Filtering = False
        Position.BandIndex = 2
        Position.ColIndex = 81
        Position.RowIndex = 0
      end
      object cxGrid1DBBandedTableView1profile_is_custom: TcxGridDBBandedColumn
        DataBinding.FieldName = 'profile_is_custom'
        PropertiesClassName = 'TcxCheckBoxProperties'
        Properties.Alignment = taLeftJustify
        Properties.NullStyle = nssUnchecked
        Properties.ReadOnly = False
        Properties.ValueChecked = 'True'
        Properties.ValueGrayed = ''
        Properties.ValueUnchecked = 'False'
        MinWidth = 16
        Options.Filtering = False
        Width = 100
        Position.BandIndex = 2
        Position.ColIndex = 82
        Position.RowIndex = 0
      end
      object cxGrid1DBBandedTableView1calc_results_xml: TcxGridDBBandedColumn
        DataBinding.FieldName = 'calc_results_xml'
        PropertiesClassName = 'TcxMemoProperties'
        Properties.Alignment = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = False
        Options.Filtering = False
        Width = 83
        Position.BandIndex = 2
        Position.ColIndex = 83
        Position.RowIndex = 0
      end
      object cxGrid1DBBandedTableView1Name_SDB: TcxGridDBBandedColumn
        DataBinding.FieldName = 'Name_SDB'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 20
        Properties.ReadOnly = False
        Options.Filtering = False
        Position.BandIndex = 2
        Position.ColIndex = 84
        Position.RowIndex = 0
      end
      object cxGrid1DBBandedTableView1Name_TN: TcxGridDBBandedColumn
        DataBinding.FieldName = 'Name_TN'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 20
        Properties.ReadOnly = False
        Options.Filtering = False
        Position.BandIndex = 2
        Position.ColIndex = 85
        Position.RowIndex = 0
      end
      object cxGrid1DBBandedTableView1repeater_linkend_id: TcxGridDBBandedColumn
        DataBinding.FieldName = 'repeater_linkend_id'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = False
        Options.Filtering = False
        Width = 99
        Position.BandIndex = 2
        Position.ColIndex = 86
        Position.RowIndex = 0
      end
      object cxGrid1DBBandedTableView1repeater_property_id: TcxGridDBBandedColumn
        DataBinding.FieldName = 'repeater_property_id'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = False
        Options.Filtering = False
        Width = 103
        Position.BandIndex = 2
        Position.ColIndex = 87
        Position.RowIndex = 0
      end
      object cxGrid1DBBandedTableView1guid: TcxGridDBBandedColumn
        DataBinding.FieldName = 'guid'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = False
        Options.Filtering = False
        Position.BandIndex = 2
        Position.ColIndex = 88
        Position.RowIndex = 0
      end
      object cxGrid1DBBandedTableView1date_created: TcxGridDBBandedColumn
        DataBinding.FieldName = 'date_created'
        PropertiesClassName = 'TcxDateEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.DateButtons = [btnClear, btnToday]
        Properties.DateOnError = deToday
        Properties.InputKind = ikRegExpr
        Options.Filtering = False
        Position.BandIndex = 2
        Position.ColIndex = 89
        Position.RowIndex = 0
      end
      object cxGrid1DBBandedTableView1date_modify: TcxGridDBBandedColumn
        DataBinding.FieldName = 'date_modify'
        PropertiesClassName = 'TcxDateEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.DateButtons = [btnClear, btnToday]
        Properties.DateOnError = deToday
        Properties.InputKind = ikRegExpr
        Options.Filtering = False
        Position.BandIndex = 2
        Position.ColIndex = 90
        Position.RowIndex = 0
      end
      object cxGrid1DBBandedTableView1user_created: TcxGridDBBandedColumn
        DataBinding.FieldName = 'user_created'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 30
        Properties.ReadOnly = False
        Options.Filtering = False
        Position.BandIndex = 2
        Position.ColIndex = 91
        Position.RowIndex = 0
      end
      object cxGrid1DBBandedTableView1user_modify: TcxGridDBBandedColumn
        DataBinding.FieldName = 'user_modify'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 30
        Properties.ReadOnly = False
        Options.Filtering = False
        Position.BandIndex = 2
        Position.ColIndex = 92
        Position.RowIndex = 0
      end
      object cxGrid1DBBandedTableView1precision_of_condition_V_diffration_is_equal_to_V_min_dB: TcxGridDBBandedColumn
        DataBinding.FieldName = 'precision_of_condition_V_diffration_is_equal_to_V_min_dB'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = False
        Options.Filtering = False
        Width = 285
        Position.BandIndex = 2
        Position.ColIndex = 93
        Position.RowIndex = 0
      end
      object cxGrid1DBBandedTableView1azimuth1: TcxGridDBBandedColumn
        DataBinding.FieldName = 'azimuth1'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = False
        Options.Filtering = False
        Position.BandIndex = 2
        Position.ColIndex = 94
        Position.RowIndex = 0
      end
      object cxGrid1DBBandedTableView1azimuth2: TcxGridDBBandedColumn
        DataBinding.FieldName = 'azimuth2'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = False
        Options.Filtering = False
        Position.BandIndex = 2
        Position.ColIndex = 95
        Position.RowIndex = 0
      end
      object cxGrid1DBBandedTableView1is_UsePassiveElements: TcxGridDBBandedColumn
        DataBinding.FieldName = 'is_UsePassiveElements'
        PropertiesClassName = 'TcxCheckBoxProperties'
        Properties.Alignment = taLeftJustify
        Properties.NullStyle = nssUnchecked
        Properties.ReadOnly = False
        Properties.ValueChecked = 'True'
        Properties.ValueGrayed = ''
        Properties.ValueUnchecked = 'False'
        MinWidth = 16
        Options.Filtering = False
        Width = 100
        Position.BandIndex = 2
        Position.ColIndex = 96
        Position.RowIndex = 0
      end
      object cxGrid1DBBandedTableView1Is_CalcWithAdditionalRain: TcxGridDBBandedColumn
        DataBinding.FieldName = 'Is_CalcWithAdditionalRain'
        PropertiesClassName = 'TcxCheckBoxProperties'
        Properties.Alignment = taLeftJustify
        Properties.NullStyle = nssUnchecked
        Properties.ReadOnly = False
        Properties.ValueChecked = 'True'
        Properties.ValueGrayed = ''
        Properties.ValueUnchecked = 'False'
        MinWidth = 16
        Options.Filtering = False
        Width = 100
        Position.BandIndex = 2
        Position.ColIndex = 97
        Position.RowIndex = 0
      end
      object cxGrid1DBBandedTableView1NIIR1998_rain_region_number: TcxGridDBBandedColumn
        DataBinding.FieldName = 'NIIR1998_rain_region_number'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = False
        Options.Filtering = False
        Width = 151
        Position.BandIndex = 2
        Position.ColIndex = 98
        Position.RowIndex = 0
      end
      object cxGrid1DBBandedTableView1NIIR1998_Qd_region_number: TcxGridDBBandedColumn
        DataBinding.FieldName = 'NIIR1998_Qd_region_number'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = False
        Options.Filtering = False
        Width = 148
        Position.BandIndex = 2
        Position.ColIndex = 99
        Position.RowIndex = 0
      end
      object cxGrid1DBBandedTableView1NIIR1998_gradient: TcxGridDBBandedColumn
        DataBinding.FieldName = 'NIIR1998_gradient'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = False
        Options.Filtering = False
        Width = 96
        Position.BandIndex = 2
        Position.ColIndex = 100
        Position.RowIndex = 0
      end
      object cxGrid1DBBandedTableView1NIIR1998_numidity: TcxGridDBBandedColumn
        DataBinding.FieldName = 'NIIR1998_numidity'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = False
        Options.Filtering = False
        Width = 96
        Position.BandIndex = 2
        Position.ColIndex = 101
        Position.RowIndex = 0
      end
      object cxGrid1DBBandedTableView1Property2lat: TcxGridDBBandedColumn
        DataBinding.FieldName = 'Property2.lat'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = False
        Options.Filtering = False
        Width = 314
        Position.BandIndex = 2
        Position.ColIndex = 1
        Position.RowIndex = 0
      end
      object cxGrid1DBBandedTableView1Property2lon: TcxGridDBBandedColumn
        DataBinding.FieldName = 'Property2.lon'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = False
        Options.Filtering = False
        Width = 68
        Position.BandIndex = 2
        Position.ColIndex = 102
        Position.RowIndex = 0
      end
      object cxGrid1DBBandedTableView1Property1lat: TcxGridDBBandedColumn
        DataBinding.FieldName = 'Property1.lat'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = False
        Options.Filtering = False
        Width = 65
        Position.BandIndex = 2
        Position.ColIndex = 103
        Position.RowIndex = 0
      end
      object cxGrid1DBBandedTableView1Property1lon: TcxGridDBBandedColumn
        DataBinding.FieldName = 'Property1.lon'
        PropertiesClassName = 'TcxMaskEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = False
        Options.Filtering = False
        Width = 68
        Position.BandIndex = 2
        Position.ColIndex = 104
        Position.RowIndex = 0
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1DBBandedTableView1
    end
  end
  object cxDBVerticalGrid1: TcxDBVerticalGrid
    Left = 600
    Top = 16
    Width = 353
    Height = 200
    Styles.OnGetContentStyle = cxDBVerticalGrid1StylesGetContentStyle
    TabOrder = 3
    DataController.DataSource = DataSource1
    object cxDBVerticalGrid1id: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'id'
    end
    object cxDBVerticalGrid1name: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'name'
    end
    object cxDBVerticalGrid1project_id: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'project_id'
    end
    object cxDBVerticalGrid1clutter_model_id: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'clutter_model_id'
    end
    object cxDBVerticalGrid1folder_id: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'folder_id'
    end
    object cxDBVerticalGrid1objname: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'objname'
    end
    object cxDBVerticalGrid1linkend1_id: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'linkend1_id'
    end
    object cxDBVerticalGrid1linkend2_id: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'linkend2_id'
    end
    object cxDBVerticalGrid1pmp_sector_id: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'pmp_sector_id'
    end
    object cxDBVerticalGrid1pmp_terminal_id: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'pmp_terminal_id'
    end
    object cxDBVerticalGrid1Property1_id: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'Property1_id'
    end
    object cxDBVerticalGrid1Property2_id: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'Property2_id'
    end
    object cxDBVerticalGrid1length: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'length'
    end
    object cxDBVerticalGrid1length_km: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'length_km'
    end
    object cxDBVerticalGrid1calc_method: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'calc_method'
    end
    object cxDBVerticalGrid1comments: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'comments'
    end
    object cxDBVerticalGrid1refraction: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'refraction'
    end
    object cxDBVerticalGrid1NFrenel: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'NFrenel'
    end
    object cxDBVerticalGrid1profile_step: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'profile_step'
    end
    object cxDBVerticalGrid1is_profile_reversed: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'is_profile_reversed'
    end
    object cxDBVerticalGrid1_: TcxDBEditorRow
      Properties.DataBinding.FieldName = '_'
    end
    object cxDBVerticalGrid1LOS_status: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'LOS_status'
    end
    object cxDBVerticalGrid1status: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'status'
    end
    object cxDBVerticalGrid1rx_level_dBm: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'rx_level_dBm'
    end
    object cxDBVerticalGrid1SESR: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'SESR'
    end
    object cxDBVerticalGrid1Kng: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'Kng'
    end
    object cxDBVerticalGrid1kng_year: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'kng_year'
    end
    object cxDBVerticalGrid1fade_margin_dB: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'fade_margin_dB'
    end
    object cxDBVerticalGrid1rain_intensity: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'rain_intensity'
    end
    object cxDBVerticalGrid1rain_IsCalcLength: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'rain_IsCalcLength'
    end
    object cxDBVerticalGrid1Rain_Length_km: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'Rain_Length_km'
    end
    object cxDBVerticalGrid1rain_signal_quality: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'rain_signal_quality'
    end
    object cxDBVerticalGrid1rain_Weakening_vert: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'rain_Weakening_vert'
    end
    object cxDBVerticalGrid1rain_Weakening_horz: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'rain_Weakening_horz'
    end
    object cxDBVerticalGrid1rain_Allowable_Intense_vert: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'rain_Allowable_Intense_vert'
    end
    object cxDBVerticalGrid1rain_Allowable_Intense_horz: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'rain_Allowable_Intense_horz'
    end
    object cxDBVerticalGrid1rain_signal_depression_dB: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'rain_signal_depression_dB'
    end
    object cxDBVerticalGrid1tilt: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'tilt'
    end
    object cxDBVerticalGrid1_________: TcxDBEditorRow
      Properties.DataBinding.FieldName = '_________'
    end
    object cxDBVerticalGrid1GST_type: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'GST_type'
    end
    object cxDBVerticalGrid1GST_SESR: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'GST_SESR'
    end
    object cxDBVerticalGrid1GST_KNG: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'GST_KNG'
    end
    object cxDBVerticalGrid1GST_length: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'GST_length'
    end
    object cxDBVerticalGrid1GST_equivalent_length_km: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'GST_equivalent_length_km'
    end
    object cxDBVerticalGrid1KNG_dop: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'KNG_dop'
    end
    object cxDBVerticalGrid1SESR_norm: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'SESR_norm'
    end
    object cxDBVerticalGrid1KNG_norm: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'KNG_norm'
    end
    object cxDBVerticalGrid1ESR_norm: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'ESR_norm'
    end
    object cxDBVerticalGrid1BBER_norm: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'BBER_norm'
    end
    object cxDBVerticalGrid1linkType_id: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'linkType_id'
    end
    object cxDBVerticalGrid1RRV_name: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'RRV_name'
    end
    object cxDBVerticalGrid1DBEditorRow: TcxDBEditorRow
      Properties.DataBinding.FieldName = '--- '#1059#1089#1083#1086#1074#1080#1103' '#1088#1072#1089#1087#1088#1086#1089#1090#1088#1072#1085#1077#1085#1080#1103' '#1088#1072#1076#1080#1086#1074#1086#1083#1085' ('#1056#1056#1042') ----'
    end
    object cxDBVerticalGrid1air_temperature: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'air_temperature'
    end
    object cxDBVerticalGrid1atmosphere_pressure: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'atmosphere_pressure'
    end
    object cxDBVerticalGrid1gradient_diel: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'gradient_diel'
    end
    object cxDBVerticalGrid1gradient: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'gradient'
    end
    object cxDBVerticalGrid1terrain_type: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'terrain_type'
    end
    object cxDBVerticalGrid1underlying_terrain_type: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'underlying_terrain_type'
    end
    object cxDBVerticalGrid1Q_factor: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'Q_factor'
    end
    object cxDBVerticalGrid1climate_factor: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'climate_factor'
    end
    object cxDBVerticalGrid1factor_B: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'factor_B'
    end
    object cxDBVerticalGrid1factor_C: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'factor_C'
    end
    object cxDBVerticalGrid1factor_D: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'factor_D'
    end
    object cxDBVerticalGrid1rain_rate: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'rain_rate'
    end
    object cxDBVerticalGrid1steam_wet: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'steam_wet'
    end
    object cxDBVerticalGrid1BER_required: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'BER_required'
    end
    object cxDBVerticalGrid1ESR_required: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'ESR_required'
    end
    object cxDBVerticalGrid1BBER_required: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'BBER_required'
    end
    object cxDBVerticalGrid1space_limit: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'space_limit'
    end
    object cxDBVerticalGrid1space_limit_probability: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'space_limit_probability'
    end
    object cxDBVerticalGrid1margin_height: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'margin_height'
    end
    object cxDBVerticalGrid1margin_distance: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'margin_distance'
    end
    object cxDBVerticalGrid1sesr_subrefraction: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'sesr_subrefraction'
    end
    object cxDBVerticalGrid1sesr_rain: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'sesr_rain'
    end
    object cxDBVerticalGrid1sesr_interference: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'sesr_interference'
    end
    object cxDBVerticalGrid1___________: TcxDBEditorRow
      Properties.DataBinding.FieldName = '___________'
    end
    object cxDBVerticalGrid1protection_type: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'protection_type'
    end
    object cxDBVerticalGrid1redundancy: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'redundancy'
    end
    object cxDBVerticalGrid1______________: TcxDBEditorRow
      Properties.DataBinding.FieldName = '______________'
    end
    object cxDBVerticalGrid1status_id: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'status_id'
    end
    object cxDBVerticalGrid1_______: TcxDBEditorRow
      Properties.DataBinding.FieldName = '_______'
    end
    object cxDBVerticalGrid1profile_source: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'profile_source'
    end
    object cxDBVerticalGrid1profile_XML: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'profile_XML'
    end
    object cxDBVerticalGrid1profile_is_custom: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'profile_is_custom'
    end
    object cxDBVerticalGrid1calc_results_xml: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'calc_results_xml'
    end
    object cxDBVerticalGrid1Name_SDB: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'Name_SDB'
    end
    object cxDBVerticalGrid1Name_TN: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'Name_TN'
    end
    object cxDBVerticalGrid1repeater_linkend_id: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'repeater_linkend_id'
    end
    object cxDBVerticalGrid1repeater_property_id: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'repeater_property_id'
    end
    object cxDBVerticalGrid1guid: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'guid'
    end
    object cxDBVerticalGrid1date_created: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'date_created'
    end
    object cxDBVerticalGrid1date_modify: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'date_modify'
    end
    object cxDBVerticalGrid1user_created: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'user_created'
    end
    object cxDBVerticalGrid1user_modify: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'user_modify'
    end
    object cxDBVerticalGrid1precision_of_condition_V_diffration_is_equal_to_V_min_dB: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'precision_of_condition_V_diffration_is_equal_to_V_min_dB'
    end
    object cxDBVerticalGrid1azimuth1: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'azimuth1'
    end
    object cxDBVerticalGrid1azimuth2: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'azimuth2'
    end
    object cxDBVerticalGrid1is_UsePassiveElements: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'is_UsePassiveElements'
    end
    object cxDBVerticalGrid1Is_CalcWithAdditionalRain: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'Is_CalcWithAdditionalRain'
    end
    object cxDBVerticalGrid1NIIR1998_rain_region_number: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'NIIR1998_rain_region_number'
    end
    object cxDBVerticalGrid1NIIR1998_Qd_region_number: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'NIIR1998_Qd_region_number'
    end
    object cxDBVerticalGrid1NIIR1998_gradient: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'NIIR1998_gradient'
    end
    object cxDBVerticalGrid1NIIR1998_numidity: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'NIIR1998_numidity'
    end
    object cxDBVerticalGrid1NIIR1998_gradient_deviation: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'NIIR1998_gradient_deviation'
    end
    object cxDBVerticalGrid1NIIR1998_water_area_percent: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'NIIR1998_water_area_percent'
    end
    object cxDBVerticalGrid1link_repeater_id: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'link_repeater_id'
    end
    object cxDBVerticalGrid1link_repeater_profile_XML_1: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'link_repeater_profile_XML_1'
    end
    object cxDBVerticalGrid1link_repeater_profile_XML_2: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'link_repeater_profile_XML_2'
    end
    object cxDBVerticalGrid1repeater_length_km_1: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'repeater_length_km_1'
    end
    object cxDBVerticalGrid1repeater_length_km_2: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'repeater_length_km_2'
    end
    object cxDBVerticalGrid1Property1name: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'Property1.name'
    end
    object cxDBVerticalGrid1Property2name: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'Property2.name'
    end
    object cxDBVerticalGrid1Property1address: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'Property1.address'
    end
    object cxDBVerticalGrid1Property2address: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'Property2.address'
    end
    object cxDBVerticalGrid1Property2lat: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'Property2.lat'
    end
    object cxDBVerticalGrid1Property2lon: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'Property2.lon'
    end
    object cxDBVerticalGrid1Property1lat: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'Property1.lat'
    end
    object cxDBVerticalGrid1Property1lon: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'Property1.lon'
    end
  end
  object DataSource1: TDataSource
    DataSet = ADOQuery1
    Left = 56
    Top = 64
  end
  object ADOQuery1: TADOQuery
    Active = True
    Connection = ADOConnection1
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select * From __view_link_report')
    Left = 160
    Top = 16
  end
  object ADOConnection1: TADOConnection
    CommandTimeout = 60
    Connected = True
    ConnectionString = 
      'Provider=MSDASQL.1;Persist Security Info=False;User ID=sa;Data S' +
      'ource=onega_1'
    ConnectionTimeout = 30
    LoginPrompt = False
    Left = 352
    Top = 16
  end
  object cxStyleRepository1: TcxStyleRepository
    Left = 488
    Top = 24
    object cxStyle1: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
    end
  end
end
