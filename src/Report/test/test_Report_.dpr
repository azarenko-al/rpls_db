program test_Report_;

uses
  a_Unit1 in 'a_Unit1.pas' {Form7},
  d_Report_Add in '..\d_Report_Add.pas',
  d_Report_Select in '..\d_Report_Select.pas' {dlg_Report_Select},
  d_Wizard_Add_with_params in '..\..\.common\d_Wizard_Add_with_params.pas' {dlg_Wizard_add_with_params},
  dm_Act_Report in '..\dm_Act_Report.pas' {dmAct_Report1: TDataModule},
  dm_Document in '..\..\Documents\dm_Document.pas' {dmDocument: TDataModule},
  Forms,
  fr_Report_Item in '..\fr_Report_Item.pas' {frame_Report_Item},
  fr_Report_View in '..\fr_Report_View.pas' {frame_Report_View},
  u_const in '..\..\u_const.pas';

{$R *.RES}



begin
  Application.Initialize;
  Application.CreateForm(TForm7, Form7);
  Application.Run;
end.
