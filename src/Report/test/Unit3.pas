unit Unit3;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ADODB, Grids, DBGrids, DB,     dxDBCtrl,   
      cxGridCustomTableView, cxGridTableView,
  cxGridBandedTableView, cxGridDBBandedTableView, cxControls,
  cxGridCustomView, cxClasses, cxGridLevel, cxGrid, cxInplaceContainer,
  cxVGrid, cxStyles, cxDBVGrid;

type
  TForm2 = class(TForm)
    DataSource1: TDataSource;
    DBGrid1: TDBGrid;
    ADOQuery1: TADOQuery;
    dxDBGrid1: TdxDBGrid;
    ADOConnection1: TADOConnection;
    dxDBGrid1id: TdxDBGridMaskColumn;
    dxDBGrid1name: TdxDBGridMaskColumn;
    dxDBGrid1project_id: TdxDBGridMaskColumn;
    dxDBGrid1clutter_model_id: TdxDBGridMaskColumn;
    dxDBGrid1folder_id: TdxDBGridMaskColumn;
    dxDBGrid1objname: TdxDBGridMaskColumn;
    dxDBGrid1linkend1_id: TdxDBGridMaskColumn;
    dxDBGrid1linkend2_id: TdxDBGridMaskColumn;
    dxDBGrid1pmp_sector_id: TdxDBGridMaskColumn;
    dxDBGrid1pmp_terminal_id: TdxDBGridMaskColumn;
    dxDBGrid1Property1_id: TdxDBGridMaskColumn;
    dxDBGrid1Property2_id: TdxDBGridMaskColumn;
    dxDBGrid1length: TdxDBGridMaskColumn;
    dxDBGrid1length_km: TdxDBGridMaskColumn;
    dxDBGrid1calc_method: TdxDBGridMaskColumn;
    dxDBGrid1comments: TdxDBGridMaskColumn;
    dxDBGrid1refraction: TdxDBGridMaskColumn;
    dxDBGrid1NFrenel: TdxDBGridMaskColumn;
    dxDBGrid1profile_step: TdxDBGridMaskColumn;
    dxDBGrid1is_profile_reversed: TdxDBGridCheckColumn;
    dxDBGrid1_: TdxDBGridMaskColumn;
    dxDBGrid1LOS_status: TdxDBGridMaskColumn;
    dxDBGrid1status: TdxDBGridMaskColumn;
    dxDBGrid1rx_level_dBm: TdxDBGridMaskColumn;
    dxDBGrid1SESR: TdxDBGridMaskColumn;
    dxDBGrid1Kng: TdxDBGridMaskColumn;
    dxDBGrid1kng_year: TdxDBGridMaskColumn;
    dxDBGrid1fade_margin_dB: TdxDBGridMaskColumn;
    dxDBGrid1rain_intensity: TdxDBGridMaskColumn;
    dxDBGrid1rain_IsCalcLength: TdxDBGridCheckColumn;
    dxDBGrid1Rain_Length_km: TdxDBGridMaskColumn;
    dxDBGrid1rain_signal_quality: TdxDBGridMaskColumn;
    dxDBGrid1rain_Weakening_vert: TdxDBGridMaskColumn;
    dxDBGrid1rain_Weakening_horz: TdxDBGridMaskColumn;
    dxDBGrid1rain_Allowable_Intense_vert: TdxDBGridMaskColumn;
    dxDBGrid1rain_Allowable_Intense_horz: TdxDBGridMaskColumn;
    dxDBGrid1rain_signal_depression_dB: TdxDBGridMaskColumn;
    dxDBGrid1tilt: TdxDBGridMaskColumn;
    dxDBGrid1_________: TdxDBGridMaskColumn;
    dxDBGrid1GST_type: TdxDBGridMaskColumn;
    dxDBGrid1GST_SESR: TdxDBGridMaskColumn;
    dxDBGrid1GST_KNG: TdxDBGridMaskColumn;
    dxDBGrid1GST_length: TdxDBGridMaskColumn;
    dxDBGrid1GST_equivalent_length_km: TdxDBGridMaskColumn;
    dxDBGrid1KNG_dop: TdxDBGridMaskColumn;
    dxDBGrid1SESR_norm: TdxDBGridMaskColumn;
    dxDBGrid1KNG_norm: TdxDBGridMaskColumn;
    dxDBGrid1ESR_norm: TdxDBGridMaskColumn;
    dxDBGrid1BBER_norm: TdxDBGridMaskColumn;
    dxDBGrid1linkType_id: TdxDBGridMaskColumn;
    dxDBGrid1RRV_name: TdxDBGridMaskColumn;
    dxDBGrid1Column52: TdxDBGridMaskColumn;
    dxDBGrid1air_temperature: TdxDBGridMaskColumn;
    dxDBGrid1atmosphere_pressure: TdxDBGridMaskColumn;
    dxDBGrid1gradient_diel: TdxDBGridMaskColumn;
    dxDBGrid1gradient: TdxDBGridMaskColumn;
    dxDBGrid1terrain_type: TdxDBGridMaskColumn;
    dxDBGrid1underlying_terrain_type: TdxDBGridMaskColumn;
    dxDBGrid1Q_factor: TdxDBGridMaskColumn;
    dxDBGrid1climate_factor: TdxDBGridMaskColumn;
    dxDBGrid1factor_B: TdxDBGridMaskColumn;
    dxDBGrid1factor_C: TdxDBGridMaskColumn;
    dxDBGrid1factor_D: TdxDBGridMaskColumn;
    dxDBGrid1rain_rate: TdxDBGridMaskColumn;
    dxDBGrid1steam_wet: TdxDBGridMaskColumn;
    dxDBGrid1BER_required: TdxDBGridMaskColumn;
    dxDBGrid1ESR_required: TdxDBGridMaskColumn;
    dxDBGrid1BBER_required: TdxDBGridMaskColumn;
    dxDBGrid1space_limit: TdxDBGridMaskColumn;
    dxDBGrid1space_limit_probability: TdxDBGridMaskColumn;
    dxDBGrid1margin_height: TdxDBGridMaskColumn;
    dxDBGrid1margin_distance: TdxDBGridMaskColumn;
    dxDBGrid1sesr_subrefraction: TdxDBGridMaskColumn;
    dxDBGrid1sesr_rain: TdxDBGridMaskColumn;
    dxDBGrid1sesr_interference: TdxDBGridMaskColumn;
    dxDBGrid1___________: TdxDBGridMaskColumn;
    dxDBGrid1protection_type: TdxDBGridMaskColumn;
    dxDBGrid1redundancy: TdxDBGridMaskColumn;
    dxDBGrid1______________: TdxDBGridMaskColumn;
    dxDBGrid1status_id: TdxDBGridMaskColumn;
    dxDBGrid1_______: TdxDBGridCheckColumn;
    dxDBGrid1profile_source: TdxDBGridMaskColumn;
    dxDBGrid1profile_XML: TdxDBGridMemoColumn;
    dxDBGrid1profile_is_custom: TdxDBGridCheckColumn;
    dxDBGrid1calc_results_xml: TdxDBGridMemoColumn;
    dxDBGrid1Name_SDB: TdxDBGridMaskColumn;
    dxDBGrid1Name_TN: TdxDBGridMaskColumn;
    dxDBGrid1repeater_linkend_id: TdxDBGridMaskColumn;
    dxDBGrid1repeater_property_id: TdxDBGridMaskColumn;
    dxDBGrid1guid: TdxDBGridColumn;
    dxDBGrid1date_created: TdxDBGridDateColumn;
    dxDBGrid1date_modify: TdxDBGridDateColumn;
    dxDBGrid1user_created: TdxDBGridMaskColumn;
    dxDBGrid1user_modify: TdxDBGridMaskColumn;
    dxDBGrid1precision_of_condition_V_diffration_is_equal_to_V_min_dB: TdxDBGridMaskColumn;
    dxDBGrid1azimuth1: TdxDBGridMaskColumn;
    dxDBGrid1azimuth2: TdxDBGridMaskColumn;
    dxDBGrid1is_UsePassiveElements: TdxDBGridCheckColumn;
    dxDBGrid1Is_CalcWithAdditionalRain: TdxDBGridCheckColumn;
    dxDBGrid1NIIR1998_rain_region_number: TdxDBGridMaskColumn;
    dxDBGrid1NIIR1998_Qd_region_number: TdxDBGridMaskColumn;
    dxDBGrid1NIIR1998_gradient: TdxDBGridMaskColumn;
    dxDBGrid1NIIR1998_numidity: TdxDBGridMaskColumn;
    dxDBGrid1Property1name: TdxDBGridMaskColumn;
    dxDBGrid1Property2name: TdxDBGridMaskColumn;
    dxDBGrid1Property1address: TdxDBGridMaskColumn;
    dxDBGrid1Property2address: TdxDBGridMaskColumn;
    dxDBGrid1Property2lat: TdxDBGridMaskColumn;
    dxDBGrid1Property2lon: TdxDBGridMaskColumn;
    dxDBGrid1Property1lat: TdxDBGridMaskColumn;
    dxDBGrid1Property1lon: TdxDBGridMaskColumn;
    cxGrid1: TcxGrid;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1DBBandedTableView1: TcxGridDBBandedTableView;
    cxGrid1DBBandedTableView1id: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1name: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1project_id: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1clutter_model_id: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1folder_id: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1objname: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1linkend1_id: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1linkend2_id: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1pmp_sector_id: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1pmp_terminal_id: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1Property1_id: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1Property2_id: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1length: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1length_km: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1calc_method: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1comments: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1refraction: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1NFrenel: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1profile_step: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1is_profile_reversed: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1_: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1LOS_status: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1status: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1rx_level_dBm: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1SESR: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1Kng: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1kng_year: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1fade_margin_dB: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1rain_intensity: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1rain_IsCalcLength: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1Rain_Length_km: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1rain_signal_quality: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1rain_Weakening_vert: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1rain_Weakening_horz: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1rain_Allowable_Intense_vert: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1rain_Allowable_Intense_horz: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1rain_signal_depression_dB: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1tilt: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1_________: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1GST_type: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1GST_SESR: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1GST_KNG: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1GST_length: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1GST_equivalent_length_km: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1KNG_dop: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1SESR_norm: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1KNG_norm: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1ESR_norm: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1BBER_norm: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1linkType_id: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1RRV_name: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1DBBandedColumn: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1air_temperature: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1atmosphere_pressure: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1gradient_diel: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1gradient: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1terrain_type: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1underlying_terrain_type: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1Q_factor: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1climate_factor: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1factor_B: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1factor_C: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1factor_D: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1rain_rate: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1steam_wet: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1BER_required: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1ESR_required: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1BBER_required: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1space_limit: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1space_limit_probability: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1margin_height: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1margin_distance: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1sesr_subrefraction: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1sesr_rain: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1sesr_interference: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1___________: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1protection_type: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1redundancy: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1______________: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1status_id: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1_______: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1profile_source: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1profile_XML: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1profile_is_custom: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1calc_results_xml: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1Name_SDB: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1Name_TN: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1repeater_linkend_id: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1repeater_property_id: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1guid: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1date_created: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1date_modify: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1user_created: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1user_modify: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1precision_of_condition_V_diffration_is_equal_to_V_min_dB: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1azimuth1: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1azimuth2: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1is_UsePassiveElements: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1Is_CalcWithAdditionalRain: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1NIIR1998_rain_region_number: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1NIIR1998_Qd_region_number: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1NIIR1998_gradient: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1NIIR1998_numidity: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1Property1name: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1Property2name: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1Property1address: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1Property2address: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1Property2lat: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1Property2lon: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1Property1lat: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1Property1lon: TcxGridDBBandedColumn;
    cxDBVerticalGrid1: TcxDBVerticalGrid;
    cxDBVerticalGrid1id: TcxDBEditorRow;
    cxDBVerticalGrid1name: TcxDBEditorRow;
    cxDBVerticalGrid1project_id: TcxDBEditorRow;
    cxDBVerticalGrid1clutter_model_id: TcxDBEditorRow;
    cxDBVerticalGrid1folder_id: TcxDBEditorRow;
    cxDBVerticalGrid1objname: TcxDBEditorRow;
    cxDBVerticalGrid1linkend1_id: TcxDBEditorRow;
    cxDBVerticalGrid1linkend2_id: TcxDBEditorRow;
    cxDBVerticalGrid1pmp_sector_id: TcxDBEditorRow;
    cxDBVerticalGrid1pmp_terminal_id: TcxDBEditorRow;
    cxDBVerticalGrid1Property1_id: TcxDBEditorRow;
    cxDBVerticalGrid1Property2_id: TcxDBEditorRow;
    cxDBVerticalGrid1length: TcxDBEditorRow;
    cxDBVerticalGrid1length_km: TcxDBEditorRow;
    cxDBVerticalGrid1calc_method: TcxDBEditorRow;
    cxDBVerticalGrid1comments: TcxDBEditorRow;
    cxDBVerticalGrid1refraction: TcxDBEditorRow;
    cxDBVerticalGrid1NFrenel: TcxDBEditorRow;
    cxDBVerticalGrid1profile_step: TcxDBEditorRow;
    cxDBVerticalGrid1is_profile_reversed: TcxDBEditorRow;
    cxDBVerticalGrid1_: TcxDBEditorRow;
    cxDBVerticalGrid1LOS_status: TcxDBEditorRow;
    cxDBVerticalGrid1status: TcxDBEditorRow;
    cxDBVerticalGrid1rx_level_dBm: TcxDBEditorRow;
    cxDBVerticalGrid1SESR: TcxDBEditorRow;
    cxDBVerticalGrid1Kng: TcxDBEditorRow;
    cxDBVerticalGrid1kng_year: TcxDBEditorRow;
    cxDBVerticalGrid1fade_margin_dB: TcxDBEditorRow;
    cxDBVerticalGrid1rain_intensity: TcxDBEditorRow;
    cxDBVerticalGrid1rain_IsCalcLength: TcxDBEditorRow;
    cxDBVerticalGrid1Rain_Length_km: TcxDBEditorRow;
    cxDBVerticalGrid1rain_signal_quality: TcxDBEditorRow;
    cxDBVerticalGrid1rain_Weakening_vert: TcxDBEditorRow;
    cxDBVerticalGrid1rain_Weakening_horz: TcxDBEditorRow;
    cxDBVerticalGrid1rain_Allowable_Intense_vert: TcxDBEditorRow;
    cxDBVerticalGrid1rain_Allowable_Intense_horz: TcxDBEditorRow;
    cxDBVerticalGrid1rain_signal_depression_dB: TcxDBEditorRow;
    cxDBVerticalGrid1tilt: TcxDBEditorRow;
    cxDBVerticalGrid1_________: TcxDBEditorRow;
    cxDBVerticalGrid1GST_type: TcxDBEditorRow;
    cxDBVerticalGrid1GST_SESR: TcxDBEditorRow;
    cxDBVerticalGrid1GST_KNG: TcxDBEditorRow;
    cxDBVerticalGrid1GST_length: TcxDBEditorRow;
    cxDBVerticalGrid1GST_equivalent_length_km: TcxDBEditorRow;
    cxDBVerticalGrid1KNG_dop: TcxDBEditorRow;
    cxDBVerticalGrid1SESR_norm: TcxDBEditorRow;
    cxDBVerticalGrid1KNG_norm: TcxDBEditorRow;
    cxDBVerticalGrid1ESR_norm: TcxDBEditorRow;
    cxDBVerticalGrid1BBER_norm: TcxDBEditorRow;
    cxDBVerticalGrid1linkType_id: TcxDBEditorRow;
    cxDBVerticalGrid1RRV_name: TcxDBEditorRow;
    cxDBVerticalGrid1DBEditorRow: TcxDBEditorRow;
    cxDBVerticalGrid1air_temperature: TcxDBEditorRow;
    cxDBVerticalGrid1atmosphere_pressure: TcxDBEditorRow;
    cxDBVerticalGrid1gradient_diel: TcxDBEditorRow;
    cxDBVerticalGrid1gradient: TcxDBEditorRow;
    cxDBVerticalGrid1terrain_type: TcxDBEditorRow;
    cxDBVerticalGrid1underlying_terrain_type: TcxDBEditorRow;
    cxDBVerticalGrid1Q_factor: TcxDBEditorRow;
    cxDBVerticalGrid1climate_factor: TcxDBEditorRow;
    cxDBVerticalGrid1factor_B: TcxDBEditorRow;
    cxDBVerticalGrid1factor_C: TcxDBEditorRow;
    cxDBVerticalGrid1factor_D: TcxDBEditorRow;
    cxDBVerticalGrid1rain_rate: TcxDBEditorRow;
    cxDBVerticalGrid1steam_wet: TcxDBEditorRow;
    cxDBVerticalGrid1BER_required: TcxDBEditorRow;
    cxDBVerticalGrid1ESR_required: TcxDBEditorRow;
    cxDBVerticalGrid1BBER_required: TcxDBEditorRow;
    cxDBVerticalGrid1space_limit: TcxDBEditorRow;
    cxDBVerticalGrid1space_limit_probability: TcxDBEditorRow;
    cxDBVerticalGrid1margin_height: TcxDBEditorRow;
    cxDBVerticalGrid1margin_distance: TcxDBEditorRow;
    cxDBVerticalGrid1sesr_subrefraction: TcxDBEditorRow;
    cxDBVerticalGrid1sesr_rain: TcxDBEditorRow;
    cxDBVerticalGrid1sesr_interference: TcxDBEditorRow;
    cxDBVerticalGrid1___________: TcxDBEditorRow;
    cxDBVerticalGrid1protection_type: TcxDBEditorRow;
    cxDBVerticalGrid1redundancy: TcxDBEditorRow;
    cxDBVerticalGrid1______________: TcxDBEditorRow;
    cxDBVerticalGrid1status_id: TcxDBEditorRow;
    cxDBVerticalGrid1_______: TcxDBEditorRow;
    cxDBVerticalGrid1profile_source: TcxDBEditorRow;
    cxDBVerticalGrid1profile_XML: TcxDBEditorRow;
    cxDBVerticalGrid1profile_is_custom: TcxDBEditorRow;
    cxDBVerticalGrid1calc_results_xml: TcxDBEditorRow;
    cxDBVerticalGrid1Name_SDB: TcxDBEditorRow;
    cxDBVerticalGrid1Name_TN: TcxDBEditorRow;
    cxDBVerticalGrid1repeater_linkend_id: TcxDBEditorRow;
    cxDBVerticalGrid1repeater_property_id: TcxDBEditorRow;
    cxDBVerticalGrid1guid: TcxDBEditorRow;
    cxDBVerticalGrid1date_created: TcxDBEditorRow;
    cxDBVerticalGrid1date_modify: TcxDBEditorRow;
    cxDBVerticalGrid1user_created: TcxDBEditorRow;
    cxDBVerticalGrid1user_modify: TcxDBEditorRow;
    cxDBVerticalGrid1precision_of_condition_V_diffration_is_equal_to_V_min_dB: TcxDBEditorRow;
    cxDBVerticalGrid1azimuth1: TcxDBEditorRow;
    cxDBVerticalGrid1azimuth2: TcxDBEditorRow;
    cxDBVerticalGrid1is_UsePassiveElements: TcxDBEditorRow;
    cxDBVerticalGrid1Is_CalcWithAdditionalRain: TcxDBEditorRow;
    cxDBVerticalGrid1NIIR1998_rain_region_number: TcxDBEditorRow;
    cxDBVerticalGrid1NIIR1998_Qd_region_number: TcxDBEditorRow;
    cxDBVerticalGrid1NIIR1998_gradient: TcxDBEditorRow;
    cxDBVerticalGrid1NIIR1998_numidity: TcxDBEditorRow;
    cxDBVerticalGrid1NIIR1998_gradient_deviation: TcxDBEditorRow;
    cxDBVerticalGrid1NIIR1998_water_area_percent: TcxDBEditorRow;
    cxDBVerticalGrid1link_repeater_id: TcxDBEditorRow;
    cxDBVerticalGrid1link_repeater_profile_XML_1: TcxDBEditorRow;
    cxDBVerticalGrid1link_repeater_profile_XML_2: TcxDBEditorRow;
    cxDBVerticalGrid1repeater_length_km_1: TcxDBEditorRow;
    cxDBVerticalGrid1repeater_length_km_2: TcxDBEditorRow;
    cxDBVerticalGrid1Property1name: TcxDBEditorRow;
    cxDBVerticalGrid1Property2name: TcxDBEditorRow;
    cxDBVerticalGrid1Property1address: TcxDBEditorRow;
    cxDBVerticalGrid1Property2address: TcxDBEditorRow;
    cxDBVerticalGrid1Property2lat: TcxDBEditorRow;
    cxDBVerticalGrid1Property2lon: TcxDBEditorRow;
    cxDBVerticalGrid1Property1lat: TcxDBEditorRow;
    cxDBVerticalGrid1Property1lon: TcxDBEditorRow;
    cxStyleRepository1: TcxStyleRepository;
    cxStyle1: TcxStyle;
    procedure cxDBVerticalGrid1StylesGetContentStyle(Sender: TObject;
      AEditProp: TcxCustomEditorRowProperties; AFocused: Boolean;
      ARecordIndex: Integer; var AStyle: TcxStyle);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form2: TForm2;

implementation

{$R *.dfm}

procedure TForm2.cxDBVerticalGrid1StylesGetContentStyle(Sender: TObject;
  AEditProp: TcxCustomEditorRowProperties; AFocused: Boolean;
  ARecordIndex: Integer; var AStyle: TcxStyle);

begin
   if TcxDBEditorRow(AEditProp.Row).Properties.DataBinding.FieldName = 'name' then

//   if TcxEditorRow(ARow).Properties. Caption = 'HP' then
    AStyle := cxstyle1;


end;


end.

(*

procedure TForm1.cxDBVerticalGridStylesGetCategoryStyle(
  Sender: TObject; ARow: TcxCustomRow; var AStyle: TcxStyle);
begin
  if TcxCategoryRow(ARow).Properties.Caption = 'Car' then
    AStyle := ACategoryStyle;
end;
procedure TForm1.cxDBVerticalGridStylesGetContentStyle(
  Sender: TObject; AEditProp: TcxCustomEditorRowProperties;
  AFocused: Boolean; ARecordIndex: Integer; var AStyle: TcxStyle);
begin
  if not (ARecordIndex = -1) then
  begin
    if (AEditProp.Caption = 'HP') and (AEditProp.Values[ARecordIndex] > 300) then
      AStyle := AContentStyle;
  end;
end;
procedure TForm1.cxDBVerticalGridStylesGetHeaderStyle(
  Sender: TObject; ARow: TcxCustomRow; var AStyle: TcxStyle);
begin
  if TcxEditorRow(ARow).Properties.Caption = 'HP' then
    AStyle := AHeaderStyle;
end;

*)