inherited dlg_Report_Add: Tdlg_Report_Add
  Left = 484
  Top = 248
  Caption = 'dlg_Report_Add'
  PixelsPerInch = 96
  TextHeight = 13
  inherited Panel2: TPanel
    Height = 210
    inherited PageControl1: TPageControl
      Height = 200
      inherited TabSheet1: TTabSheet
        object cxVerticalGrid1: TcxVerticalGrid
          Left = 0
          Top = 0
          Width = 542
          Height = 169
          Align = alClient
          LookAndFeel.Kind = lfStandard
          OptionsView.CellTextMaxLineCount = 3
          OptionsView.AutoScaleBands = False
          OptionsView.PaintStyle = psDelphi
          OptionsView.GridLineColor = clBtnShadow
          OptionsView.RowHeaderMinWidth = 30
          OptionsView.RowHeaderWidth = 140
          OptionsView.ValueWidth = 60
          TabOrder = 0
          Version = 1
          object row_FileName: TcxEditorRow
            Expanded = False
            Properties.Caption = #1048#1084#1103' '#1092#1072#1081#1083#1072' c '#1086#1090#1095#1077#1090#1086#1084
            Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
            Properties.EditProperties.Alignment.Horz = taLeftJustify
            Properties.EditProperties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            Properties.EditProperties.OnButtonClick = row_FileNameEditPropertiesButtonClick
            Properties.DataBinding.ValueType = 'String'
            Properties.Value = Null
            ID = 0
            ParentID = -1
            Index = 0
            Version = 1
          end
          object row_category: TcxEditorRow
            Expanded = False
            Properties.Caption = #1050#1072#1090#1077#1075#1086#1088#1080#1103
            Properties.EditPropertiesClassName = 'TcxComboBoxProperties'
            Properties.EditProperties.Alignment.Horz = taLeftJustify
            Properties.EditProperties.Alignment.Vert = taVCenter
            Properties.EditProperties.DropDownListStyle = lsFixedList
            Properties.EditProperties.DropDownRows = 7
            Properties.EditProperties.Items.Strings = (
              'link'
              'link_sdb'
              'link_extended'
              'linkline'
              'linknet'
              'link_freq_plan')
            Properties.EditProperties.MaxLength = 0
            Properties.EditProperties.ReadOnly = False
            Properties.EditProperties.Revertable = True
            Properties.DataBinding.ValueType = 'String'
            Properties.Value = Null
            ID = 1
            ParentID = -1
            Index = 1
            Version = 1
          end
        end
      end
    end
  end
  inherited ActionList1: TActionList
    OnUpdate = ActionList1Update
  end
end
