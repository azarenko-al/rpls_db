unit dm_Act_Report;

interface

uses
  Classes, Menus, Forms, Dialogs, ActnList,

   u_const_str,
  dm_User_Security,

   i_Audit,
  u_DataExport_run,

  I_Object,

//  I_Act_Report,

  fr_Report_View,

  u_types,

  u_const_db,

  dm_Report,
  d_Report_Add,
 // d_Report_Select,

  
  u_const_filters,

  u_func,

  dm_act_Base;

type
  TdmAct_Report = class(TdmAct_Base) //, IAct_Report_X)
    SaveDialog1: TSaveDialog;
    ActionList2_report: TActionList;
    act_Load: TAction;
    act_SaveAs: TAction;
    OpenDialog1: TOpenDialog;
    act_Export_MDB: TAction;
    act_Audit: TAction;
 //   procedure DataModuleDestroy(Sender: TObject);
    procedure DataModuleCreate(Sender: TObject);
  private
    function CheckActionsEnable: Boolean;
  protected
    function  GetViewForm  (aOwnerForm: TForm): TForm; override;
    procedure GetPopupMenu (aPopupMenu: TPopupMenu; aMenuType: TMenuType); override;

    function ItemDel(aID: integer; aName: string = ''): boolean; override;
    function ItemAdd (aFolderID: integer): integer; override;

    procedure DoAction (Sender: TObject);

  public
//    function Dlg_Select(aCategory: WideString; aReportParams:
  //      TIni_Link_Report_Param): boolean;

    function Dlg_Add  (aFolderID: integer =0): Integer;

    class procedure Init;
  end;


var
  dmAct_Report: TdmAct_Report;

//====================================================================
// implementation
//====================================================================
implementation

uses dm_Main;  {$R *.DFM}


//--------------------------------------------------------------------
class procedure TdmAct_Report.Init;
//--------------------------------------------------------------------
begin
  Assert(not Assigned(dmAct_Report));

  dmAct_Report:=TdmAct_Report.Create(Application);

end;      



//-------------------------------------------------------------------
procedure TdmAct_Report.DataModuleCreate(Sender: TObject);
//-------------------------------------------------------------------
begin
  inherited;
  ObjectName:=OBJ_REPORT;


  act_Export_MDB.Caption:=DEF_STR_Export_MDB;
  act_Audit.Caption:=STR_Audit;


  SetActionsExecuteProc
     ([act_SaveAs,
       act_Load,
       act_Export_MDB,

       act_Audit

      ],
      DoAction);

 {

 SetActionsEnabled1([
       act_SaveAs,
    //   act_Load,
       act_Export_MDB
       ],

   dmUser_Security.Is_Lib_Edit_allow );

  }


  OpenDialog1.Filter:=FILTER_REPORTS_FR3;//'MS Word|*.doc;XSL|*.xsl;';
  SaveDialog1.Filter:=FILTER_REPORTS_FR3;//'*.*|*.*';  //'XSL|*.xsl;*.*|*.*';


//  act_Report_SaveAs.Caption:=STR_SAVEAS;
end;


function TdmAct_Report.Dlg_Add(aFolderID: integer =0): integer;
begin
  Result := Tdlg_Report_Add.ExecDlg(aFolderID);
end;

{
function TdmAct_Report.Dlg_Select(aCategory: WideString; aReportParams:
    TIni_Link_Report_Param): boolean;
begin
  Result := Tdlg_Report_Select.ExecDlg(aCategory, aReportParams);
end;
 }

//--------------------------------------------------------------------
function TdmAct_Report.ItemAdd (aFolderID: integer): integer;
//--------------------------------------------------------------------
begin
  Result:=Dlg_Add (aFolderID);
end;

//--------------------------------------------------------------------
function TdmAct_Report.ItemDel(aID: integer; aName: string = ''): boolean;
//--------------------------------------------------------------------
begin
  Result:=dmReport.Del(aID);
end;

//--------------------------------------------------------------------
function TdmAct_Report.GetViewForm (aOwnerForm: TForm): TForm;
//--------------------------------------------------------------------
begin
  Result:=Tframe_Report_View.Create(aOwnerForm);
end;


//--------------------------------------------------------------------
procedure TdmAct_Report.DoAction (Sender: TObject);
//--------------------------------------------------------------------
var sFileName: string;
begin
  if Sender=act_Audit then
    Init_IAuditX.ExecDlg( dmMain.GetConnectionString, TBL_Report, FFocusedID) else


  if Sender=act_Export_MDB then
    TDataExport_run.ExportToRplsDb_selected (TBL_Report, FSelectedIDList);

  //------------------------------------
  if Sender=act_Load then begin
  //------------------------------------
  //  sFileName:= dmReport.GetStringFieldValue(FFocusedID, FLD_FILENAME);

  //  if sFileName = '' then
   //   sFileName:= 'report.xsl';

    OpenDialog1.FileName:= sFileName; //qry_Report.FieldByName(FLD_FILENAME).AsString; // col_FileName.Field.Text;
  //  OpenDialog1.Filter:=FILTER_REPORTS_FR3;//'MS Word|*.doc;XSL|*.xsl;';


    if OpenDialog1.Execute then
      dmReport.LoadFromFile (FFocusedID, OpenDialog1.FileName);
  end else

  //------------------------------------------------------
  if Sender=act_SaveAs then
  //------------------------------------------------------
  begin
//    sFileName:=dmReport.GetStringFieldValue(FFocusedID, FLD_FILENAME);
    sFileName:=dmReport.GetFileName (FFocusedID);

  //  if sFileName = '' then
   //   sFileName:= 'report.xsl';

    SaveDialog1.FileName:=sFileName;//ChangeFileExt(sFileName, '.xsl');
//    SaveDialog1.Filter:=FILTER_REPORTS_FR3;//'*.*|*.*';  //'XSL|*.xsl;*.*|*.*';

    if SaveDialog1.Execute then
      dmReport.SaveToFile (FFocusedID, SaveDialog1.FileName);
  end;
end;



// ---------------------------------------------------------------
function TdmAct_Report.CheckActionsEnable: Boolean;
// ---------------------------------------------------------------
begin
  Result:= dmUser_Security.Modify_lib in [mtAllow, mtAllowMy];

  SetActionsEnabled1( [

   act_Load,
//                           act_Import_MDB,

//  act_Copy,

  act_Del_,
  act_Del_list

      ],

   Result );

   //  TModify_lib = (mtAllow=1, mtAllowMy=2, mtForbidden=0 );


end;



//--------------------------------------------------------------------
procedure TdmAct_Report.GetPopupMenu;
//--------------------------------------------------------------------
var
  bEnable: Boolean;
begin
  bEnable:=CheckActionsEnable();


  case aMenuType of
    mtFolder: begin
                AddFolderMenu_Create (aPopupMenu, bEnable);
                AddFolderPopupMenu  (aPopupMenu, bEnable);
              end;

    mtItem:  begin
                AddMenuItem (aPopupMenu, act_SaveAs);
                AddMenuItem (aPopupMenu, act_Load);
                AddMenuItem (aPopupMenu, nil);
                AddMenuItem (aPopupMenu, act_Del_);
                AddMenuItem (aPopupMenu, nil);

               // if bEnable then
                 AddFolderMenu_Tools (aPopupMenu);

                AddMenuItem (aPopupMenu, act_Export_MDB);
                AddMenuItem (aPopupMenu, act_Audit);
             end;

    mtList: begin
              AddMenuItem (aPopupMenu, act_Del_list);
              AddMenuItem (aPopupMenu, nil);

              AddFolderMenu_Tools (aPopupMenu);
              AddMenuItem (aPopupMenu, act_Export_MDB);
            end;
  end;

end;

 
end.




