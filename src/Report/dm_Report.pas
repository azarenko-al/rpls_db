unit dm_Report;

interface

uses
  Windows, Messages, Db, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, ADODB, Variants,

  dm_Onega_DB_data,

  dm_Main,

  dm_Object_base,

  u_types,
  u_const_db,

  u_func,
  u_db_blob,
  u_db,
  u_files
  ;

type
  //---------------------------------------
  TdmReportAddRec = record
  //---------------------------------------
    Name: string;
    FolderID: integer;
 
    FileName: string;
    Category: string;
  end;



  //--------------------------------------------------------------------
  TdmReport = class(TdmObject_base)
  //--------------------------------------------------------------------
    procedure DataModuleCreate(Sender: TObject);
  private
  public
    function Add (aRec: TdmReportAddRec): integer; overload;

    function Copy(aID: Integer; aName: string): Integer;
    function GetFileName(aID: integer): string;

    function  SaveToFile   (aID: integer; aFileName: string): boolean;
    procedure LoadFromFile (aID: integer; aFileName: string);

  end;


function dmReport: TdmReport;

//================================================================
//implementation
//================================================================
implementation  {$R *.dfm}

var
  FdmReport: TdmReport;

// ---------------------------------------------------------------
function dmReport: TdmReport;
// ---------------------------------------------------------------
begin
  if not Assigned(FdmReport) then
    Application.CreateForm(TdmReport, FdmReport);
//    FdmReport := TdmReport1.Create(Application);

  Result := FdmReport;
end;



procedure TdmReport.DataModuleCreate(Sender: TObject);
begin

  inherited;
  TableName:=TBL_REPORT;
  ObjectName:=OBJ_REPORT;

end;


//--------------------------------------------------------------------
function TdmReport.Add (aRec: TdmReportAddRec): integer;
//--------------------------------------------------------------------
begin
  Result:=gl_DB.AddRecordID (TableName,
                  [db_Par(FLD_NAME,  aRec.Name),

                   db_Par(FLD_FOLDER_ID, IIF_NULL(aRec.FolderID)),

                   db_Par(FLD_FILENAME, ExtractFileName(aRec.FileName)),
                   db_Par(FLD_CATEGORY, aRec.Category)

                   ] );


  Assert(FileExists(aRec.FileName));

  if FileExists(aRec.FileName) then
    LoadFromFile (Result, aRec.FileName);

  //  SaveToFile (Result, aRec.FileName+'_');


end;


//--------------------------------------------------------------------
function TdmReport.Copy(aID: Integer; aName: string): Integer;
//--------------------------------------------------------------------
begin
  Result:=dmOnega_DB_data.ExecStoredProc('sp_Report',
                  [db_Par(FLD_COMMAND,  'copy'),

                   db_Par(FLD_ID, aID),
                   db_Par(FLD_NAME, aNAME)
                  ] );  
end;



//----------------------------------------------------------------------------
procedure TdmReport.LoadFromFile (aID: integer; aFileName: string);
//----------------------------------------------------------------------------
var
  rec: TdbBlobRec;
begin
  rec.TableName:=TableName;
  rec.ID:=aID;
  rec.FieldName:=FLD_CONTENT_BIN;
  rec.FileName:=aFileName;

  db_Blob_LoadFromFile (dmMain.ADOConnection, rec, false);

  Update_ (aID, [db_Par(FLD_FILENAME, ExtractFileName(aFileName))] );

end;

//----------------------------------------------------------------------------
function TdmReport.SaveToFile(aID: integer; aFileName: string): boolean;
//----------------------------------------------------------------------------
var
  rec: TdbBlobRec;
begin
  rec.TableName:=TableName;
  rec.ID:=aID;
  rec.FieldName:=FLD_CONTENT_BIN;
  rec.FileName:=aFileName;


  Result:=db_Blob_SaveToFile (dmMain.ADOConnection, rec, false);
end;

//----------------------------------------------------------------------------
function TdmReport.GetFileName(aID: integer): string;
//----------------------------------------------------------------------------
begin
  Result:=gl_DB.GetStringFieldValue (TableName, FLD_FILENAME, [db_par(FLD_ID, aID)]);
end;



begin 

end.




