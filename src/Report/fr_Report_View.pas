unit fr_Report_View;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
  cxControls, cxSplitter, Menus, AppEvnts, ImgList, ActnList,   ExtCtrls,

  dm_User_Security,

  fr_DBInspector_Container,

  u_types,
  u_const,

  u_func,
  u_dlg,
  u_reg,
  u_db,

  fr_View_base, cxPropertiesStore, dxBar, cxBarEditItem, cxClasses,
  cxLookAndFeels

  ;

type
  Tframe_Report_View = class(Tframe_View_Base)
    pn_Inspector: TPanel;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    Ffrm_DBInspector: Tframe_DBInspector_Container;
  //  Fframe_Report_Item: Tframe_Report_Item;

  public
    procedure View (aID: integer; aGUID: string); override;
  end;


//==================================================================
//
//==================================================================
implementation {$R *.dfm}

//--------------------------------------------------------------------
procedure Tframe_Report_View.FormCreate(Sender: TObject);
//--------------------------------------------------------------------
begin
  inherited;

//  pn_Main.Align:=alClient;
  pn_Inspector.Align:=alClient;

  ObjectName:=OBJ_REPORT;

  CreateChildForm(Tframe_DBInspector_Container, Ffrm_DBInspector, pn_Inspector);

 // Ffrm_DBInspector:=Tframe_DBInspector_Container.CreateChildForm ( pn_Inspector);
  Ffrm_DBInspector.PrepareViewForObject (OBJ_REPORT);
{
  Fframe_Report_Item:=Tframe_Report_Item.CreateChildForm ( pn_Bottom);

  AddControl(pn_Inspector, [PROP_HEIGHT]);
}
end;

//--------------------------------------------------------------------
procedure Tframe_Report_View.FormDestroy(Sender: TObject);
//--------------------------------------------------------------------
begin
//  Ffrm_DBInspector.Free;
 // Fframe_Report_Item.Free;

  inherited;
end;



procedure Tframe_Report_View.View (aID: integer; aGUID: string);
var
  bReadOnly: Boolean;
begin
  inherited;
  
  Ffrm_DBInspector.View (aID);
 // Fframe_Report_Item.View (aID);


  bReadOnly:=dmUser_Security.Lib_Edit_ReadOnly();
                                       

  Ffrm_DBInspector.SetReadOnly(bReadOnly);


end;


end.

  {


//--------------------------------------------------------------------
procedure Tframe_AntType_View.View (aID: integer; aGUID: string);
//--------------------------------------------------------------------
begin
  inherited;

  if not RecordExists (aID) then
    Exit;


  Ffrm_DBInspector.View (aID);
  Fframe_AntType_masks.View (aID);


  Ffrm_DBInspector.SetReadOnly(dmUser_Security.Is_Lib_Edit_allow);
  Fframe_AntType_masks.SetReadOnly(dmUser_Security.Is_Lib_Edit_allow);


   {
 SetActionsEnabled1(
       act_SaveAs,
    //   act_Load,
       act_Export_MDB

   dmUser_Security.Is_Lib_Edit_allow );
   }

end;


