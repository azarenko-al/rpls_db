unit d_Report_Add;

interface

uses
  Windows, cxPropertiesStore, cxVGrid, cxControls, cxInplaceContainer,
  Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,  
  rxPlacemnt, ActnList,     StdCtrls, ExtCtrls, 
  cxButtonEdit,

  u_cx_vgrid,

  d_Wizard_add_with_Inspector,

//  I_Report,

  //common
//  u_common_const,
  u_const_filters,

  u_cx,
    
  u_db,
  u_reg,
  u_files,
  u_func,

  u_const,
  u_const_db,
  u_const_str,

  dm_Report, ComCtrls, cxLookAndFeels
  ;


type
  Tdlg_Report_Add = class(Tdlg_Wizard_add_with_Inspector)
    cxVerticalGrid1: TcxVerticalGrid;
    row_FileName: TcxEditorRow;
    row_category: TcxEditorRow;
    procedure FormCreate(Sender: TObject);
    procedure act_OkExecute(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure row_FileName1ButtonClick(Sender: TObject;
      AbsoluteIndex: Integer);
    procedure ActionList1Update(Action: TBasicAction;
      var Handled: Boolean);
    procedure row_FileNameEditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
  private
    FID: integer;
 //   FFolderID: integer;

    FRec: TdmReportAddRec;

    procedure Append;

  public
    class function ExecDlg (aFolderID: integer =0): integer;

  end;
                                               

//====================================================================
implementation {$R *.DFM}
//====================================================================

//--------------------------------------------------------------------
class function Tdlg_Report_Add.ExecDlg;
//--------------------------------------------------------------------
begin
   with Tdlg_Report_add.Create(Application) do
  begin
    FRec.FolderID:=aFolderID;
    ed_Name_.Text:=dmReport.GetNewName();

    if ShowModal=mrOk then
      Result:=FID
    else
      Result:=0;

    Free;
  end;
end;
        

//--------------------------------------------------------------------
procedure Tdlg_Report_Add.act_OkExecute(Sender: TObject);
//--------------------------------------------------------------------
begin
  inherited;
  Append;
end;


//--------------------------------------------------------------------
procedure Tdlg_Report_Add.Append;
//--------------------------------------------------------------------
begin
 // FillChar(rec, SizeOf(rec), 0);

  FRec.Name:=ed_Name_.Text;
 // rec.FolderID:=FFolderID;
  FRec.FileName:=row_FileName.Properties.Value;
  FRec.Category:=row_Category.Properties.Value;

  FID:=dmReport.Add (FRec);
end;


//--------------------------------------------------------------------
procedure Tdlg_Report_Add.FormCreate(Sender: TObject);
//--------------------------------------------------------------------
begin
  inherited;
  Caption:=DLG_CAPTION_OBJECT_CREATE;

//  lb_File.Caption:=STR_IMPORT_FILENAME;

  SetActionName (STR_DLG_ADD_REPORT);

  SetDefaultSize();

  cx_InitVerticalGrid (cxVerticalGrid1);

end;


procedure Tdlg_Report_Add.FormDestroy(Sender: TObject);
begin
  //gl_Reg.SaveAndClearGroup(Self);
  inherited;
end;


procedure Tdlg_Report_Add.row_FileName1ButtonClick(Sender: TObject; AbsoluteIndex: Integer);
begin
 // if dx_BrowseFile (Sender, FILTER_REPORTS_DOC_XSL) then
   // ed_Name_.Text:=ExtractFileNameNoExt(row_FileName.Text);
end;


procedure Tdlg_Report_Add.ActionList1Update(Action: TBasicAction;  var Handled: Boolean);
var
  sFileName: string;
begin
  inherited;

  sFileName:=AsString(row_FileName.Properties.Value);

  act_Ok.Enabled:=
    (sFileName <> '') and
    (FileExists(sFileName)) and
    (row_category.Properties.Value<>'');
end;


procedure Tdlg_Report_Add.row_FileNameEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
var
  s: string;
begin
  s:=Sender.ClassName ;

 // cx_BrowseFile(Sender);
  if cx_BrowseFile (Sender as TcxButtonEdit, FILTER_REPORTS_FR3) then
    ed_Name_.Text:=ExtractFileNameNoExt((Sender as TcxButtonEdit).Text);

end;

end.