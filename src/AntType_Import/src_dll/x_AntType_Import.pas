unit x_AntType_Import;

interface

uses  Sysutils, Dialogs, IniFiles, Windows, Messages,

  u_dll_with_dmMain,


  dm_MDB,

  dm_AntType_Import,
  d_AntType_Import,


//  dm_Link_ACAD,

  dm_Main,         
(*
  d_Link_Optimize_freq_diversity,
  d_Link_antennas_optimize,

  u_ini_Link_Optimize_params,
*)
//  u_Ini_Link_Graph_Params,


 I_AntType_Import;

type
  TAntType_Import_X = class(TDllCustomX, IAntType_Import_X)
  public
    procedure Execute(aFileName: WideString); stdcall;

  end;

  function DllGetInterface(const IID: TGUID; var Obj): HResult; stdcall;

exports
  DllGetInterface;

  
implementation


const
  WM_REFRESH_DATA = WM_USER + 600;



// ---------------------------------------------------------------
procedure TAntType_Import_X.Execute(aFileName: WideString);
// ---------------------------------------------------------------
const
  TEMP_INI_FILE = 'link_ACAD.ini';

var
  b: Boolean;
  iLinkID: Integer;
  oIniFile: TIniFile;

 // oParams: TIni_Link_Optimize_Params;


begin
  Assert(Assigned(dmMain), 'Assigned(dmMain) not assigned');


  if not FileExists(aFileName ) then
  begin
    ShowMessage(aFileName);
    Exit;
  end;


  oIniFile := TIniFile.Create(aFileName);

  with oIniFile do
  begin
  //  iProjectID :=ReadInteger ('main', 'ProjectID', 0);
    iLinkID    :=ReadInteger ('main', 'link_ID',    0);
  end;


  FreeAndNil(oIniFile);

  // -------------------------
  
  TdmMDB_ant.Init;


  TdmAntType_Import.Init;
  dmAntType_Import.Params.ADOConnection:=dmMain.ADOConnection;

  //dmAntType.AddTest;

//  Tdlg_AntennaType_import.ExecDlg (iFolderID);
  Tdlg_AntennaType_import.ExecDlg (0);



//
//
//  oParams:= TIni_Link_Optimize_Params.Create;
//  oParams.LoadFromFile(aFileName);
//
//
//  dmMain.ProjectID:=oParams.ProjectID;
//
//
//
//  if oParams.Mode=LowerCase('Optimize_freq_diversity') then
//    b:=Tdlg_Link_Optimize_freq_diversity.ExecDlg(oParams.LinkID, oParams)
//  else
//
//  if oParams.Mode=LowerCase('Optimize_antennas') then
//    b:=Tdlg_Link_antennas_optimize.ExecDlg (oParams.LinkID, oParams)
//  else
//    ShowMessage('Mode=?');
//
//
//  if b then
//  begin
//  //  ShowMessage('WM_REFRESH_DATA: ' + IntToStr(iHandle));
//
//    if oParams.Handle>0 then
//      SendMessage (oParams.Handle, WM_REFRESH_DATA, 0,0);
//
//  end;
//
//
//  FreeAndNil(oParams);
//
//
//

(*

  TdmMain.Init;
  if not dmMain.OpenDB_reg then
    Exit;
*)
(*

  Tfrm_Main_link_graph.ExecDlg (oParams.LinkID, oParams);


  FreeAndNil(oParams);


  *)

end;


// ---------------------------------------------------------------
function DllGetInterface(const IID: TGUID; var Obj): HResult;
// ---------------------------------------------------------------
begin
  Result:=S_FALSE; Integer(Obj):=0;

  if IsEqualGUID(IID, IAntType_Import_X) then
  begin
    with TAntType_Import_X.Create() do
      if GetInterface(IID,OBJ) then
        Result:=S_OK
  end else
    raise Exception.Create('if IsEqualGUID(IID, ILink_graphX)');


end;



begin
 // RegisterClass(TdmMain_bpl);

end.



    {


//--------------------------------------------------------------
procedure TdmMain_link_ACAD.DataModuleCreate(Sender: TObject);
//--------------------------------------------------------------
const
  DEF_SOFTWARE_NAME= 'RPLS_DB_LINK';
  DEF_TEMP_FILENAME = 'link_acad.ini';

var
  oIniFile: TIniFile;

var
  iLinkID: Integer;
  iProjectID: Integer;

  sIniFileName: string;


begin
 // Assert(Assigned(dmMain), 'Assigned(dmMain) not assigned');



  sIniFileName:=ParamStr(1);
  if sIniFileName='' then
//    sIniFileName:=TEMP_INI_FILE;
    sIniFileName:=g_ApplicationDataDir+ DEF_TEMP_FILENAME;

   // sIniFileName:=GetCommonApplicationDataDir(DEF_SOFTWARE_NAME)+
      //            DEF_TEMP_FILENAME;


  if not FileExists(sIniFileName) then
  begin
    ShowMessage(sIniFileName);
    Exit;
  end;

 // g_Log:=TLog.Create();
//    IncludeTrailingBackslash(GetTempFileDir())+'_rpls_log.txt');


  oIniFile := TIniFile.Create(sIniFileName);

  with oIniFile do
  begin
  //  iProjectID :=ReadInteger ('main', 'ProjectID', 0);
    iLinkID    :=ReadInteger ('main', 'Link_ID',    0);
  end;


  FreeAndNil(oIniFile);



 // if not oLinkCalcParams.Validate then
   // Exit;

(*
  if (iLinkID=0) or
     (iProjectID=0)
  then begin
    ShowMessage('(iLinkID=0) or (iProjectID=0) ');
    Exit;
  end;*)


  TdmMain.Init;
  if not dmMain.OpenDB_reg then
    Exit;


////////////
 // TdmRel_Engine.Init;

 // dmMain.ProjectID:=iProjectID;



 // ShowMessage('export to ACAD');
end;
