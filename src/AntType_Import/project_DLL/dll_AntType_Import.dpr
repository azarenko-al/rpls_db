library dll_AntType_Import;

{ Important note about DLL memory management: ShareMem must be the
  first unit in your library's USES clause AND your project's (select
  Project-View Source) USES clause if your DLL exports any procedures or
  functions that pass strings as parameters or function results. This
  applies to all strings passed to and from your DLL--even those that
  are nested in records and classes. ShareMem is the interface unit to
  the BORLNDMM.DLL shared memory manager, which must be deployed along
  with your DLL. To avoid using BORLNDMM.DLL, pass string information
  using PChar or ShortString parameters. }

uses
  Classes,
  d_AntType_import in '..\src\d_AntType_import.pas' {dlg_AntennaType_import},
  dm_AntType_Import in '..\src\dm_AntType_Import.pas' {dmAntType_Import: TDataModule},
  dm_Main_app in '..\src\dm_Main_app.pas' {dmMain_app: TDataModule},
  dm_MDB in '..\src\dm_MDB.pas' {dmMDB_ant: TDataModule},
  f_AntType_Preview in '..\src\f_AntType_Preview.pas' {frm_AntType_Preview},
  I_AntType_Import in '..\src shared\I_AntType_Import.pas',
  SysUtils,
  u_ant_ADF in '..\src\u_ant_ADF.pas',
  u_ant_DAT in '..\src\u_ant_DAT.pas',
  u_ant_MSI in '..\src\u_ant_MSI.pas',
  u_antenna_custom in '..\src\u_antenna_custom.pas',
  x_AntType_Import in '..\src_dll\x_AntType_Import.pas';

{$R *.res}

begin
end.
