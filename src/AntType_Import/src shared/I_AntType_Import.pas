unit i_AntType_Import;

interface

uses ADOInt,
  u_dll;

type

  IAntType_Import_X = interface(IInterface)
  ['{7638AE4F-8682-4EC8-A6FD-05DE58481EDB}']

    procedure InitADO(aConnection: _Connection); stdcall;
//    procedure InitAppHandle(aHandle: integer); stdcall;

    procedure Execute (aFileName: WideString);  stdcall;
  end;

var
  IAntType_Import: IAntType_Import_X;

  function Load_IAntType_Import: Boolean;
  procedure UnLoad_IAntType_Import;

implementation

const
//  DEF_BPL_FILENAME = 'bpl_LOS.bpl';
  DEF_FILENAME = 'dll_AntType_Import.dll';

var
  LHandle : Integer;


function Load_IAntType_Import: Boolean;
begin
  Result:=GetInterface_DLL(DEF_FILENAME, LHandle, IAntType_Import_X, IAntType_Import)=0;
end;


procedure UnLoad_IAntType_Import;
begin
  IAntType_Import := nil;
  UnloadPackage_dll(LHandle);
end;


end.



