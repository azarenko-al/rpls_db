unit dm_AntType_Import;

interface

uses
  SysUtils, Classes, Forms, ADODB, Variants, ADOInt, 

  dm_Main,
  dm_MDB,

  u_antenna_custom,

  dm_AntType_add,

  d_Progress,

  u_const_db,

  u_files,
  u_db,

  u_ant_MSI,
  u_ant_ADF, DB

  ;

type
  TdmAntType_Import = class(TDataModule)
    ADOConnection1: TADOConnection;
    qry_AntTypes: TADOQuery;
    procedure DataModuleCreate(Sender: TObject);
  private
    FMsiFile: TMsiFile;
    FAdfFile: TAdfFile;

    procedure LoadFromMSI(aFileName: string);
    procedure LoadFromADF(aFileName: string);

    function ExtractModelName (aFileName: string): string;

    procedure AddAntenna(aFileName: string);
    procedure AddToMDB(aRec: TdmAntTypeAddRec);

    function FindByName(aName : string; aFolderID : Integer): Boolean;

// TODO: SetConnectionObject
//// TODO: AddToMDB
////  procedure AddToMDB(aRec: TdmAntTypeAddRec);
//
//  procedure SetConnectionObject(aConnectionObject: _Connection);

  public
    Params: record
      FolderID : integer;
      FileNames: TStringList;

     // ADOConnection1: TADOConnection;
    end;

    procedure ExecuteDlg;
    procedure ExecuteProc;
//    procedure ExecuteProc; override;

//    procedure InitConnection(aConnectionObject: _Connection);

    class procedure Init;

  end;

var
  dmAntType_Import: TdmAntType_Import;


//==================================================================
//==================================================================
implementation{$R *.DFM}

class procedure TdmAntType_Import.Init;
begin
  if not Assigned(dmAntType_Import) then
    dmAntType_Import:=TdmAntType_Import.Create (Application);
end;


procedure TdmAntType_Import.DataModuleCreate(Sender: TObject);
begin
  inherited;
  Params.FileNames:=TStringList.Create;
  FMsiFile:= TMsiFile.Create;
  FAdfFile:= TAdfFile.Create;

  db_SetComponentADOConnection(Self, dmMain.ADOConnection);

end;


//--------------------------------------------------------------------
function TdmAntType_Import.ExtractModelName (aFileName: string): string;
//--------------------------------------------------------------------
begin
  FMsiFile.LoadFromFile (aFileName);
  Result:=FMsiFile.ModelName;
end;



//--------------------------------------------------------------------
procedure TdmAntType_Import.LoadFromMSI(aFileName: string);
//--------------------------------------------------------------------
var iAntennaType_id,i,j: integer;
//, iTiltType
    sName: string;
     sHorz_mask: string;
     sVert_mask: string;
     rec: TdmAntTypeAddRec;
begin
  sName:=ExtractFileNameNoExt(aFileName);

//      sName:=Trim(oMsiFile.Params.Model);

 /// if not FindByName(sName,aFolderID) then

(*  if FindByName(sName,aFolderID) then
    Exit;
*)
(*
  if gl_DB.GetIntFieldValue (TBL_ANTENNATYPE, FLD_ID,
                        [db_Par (FLD_NAME, sName)//,
                     //    db_Par (FLD_FOLDER_ID, aFolderID)
                         ]) > 0
  then Exit;
*)

  if aFileName<>'' then
    FMsiFile.LoadFromFile (aFileName)
  else
    Exit;

  //электрический, механический
 {
  if FMsiFile.TiltType=ttElectrical then
    iTiltType:=0
  else
    iTiltType:=1;

  iTiltType:= IIF(FMsiFile.Diagram.Vert[0].Loss_db=0, 1, 0);

 }


  sVert_mask := FMsiFile.Diagram.Vert.GetDBStr;
  sHorz_mask := FMsiFile.Diagram.Horz.GetDBStr;

  // ---------------------------------------------------------------

  FillChar (rec, SizeOf(rec), 0);


  rec.FileName := aFileName;


 // rec.FileName := aFileName;
  rec.Folder_ID :=Params.FolderID;
  rec.NewNAME   :=sName;
  rec.Gain_dB   :=FMsiFile.Gain_dB1;
  rec.Diameter  :=FMsiFile.Diameter;
  rec.Freq_MHz  :=FMsiFile.Freq_MHz;
 // rec.TiltType  :=iTiltType;
  rec.VERT_WIDTH :=FMsiFile.Diagram.GetVertWidth();
  rec.HORZ_WIDTH :=FMsiFile.Diagram.GetHorzWidth();
//  rec.POLARIZATION      :=0; //horz
//  rec.POLARIZATION_RATIO:= FAdfFile.Get_Polarization_Ratio;
  rec.COMMENT   :=FMsiFile.Comment;

  rec.vert_mask := sVert_mask;
  rec.horz_mask := sHorz_mask;

  AddToMDB(rec);


  if FindByName(sName,Params.FolderID) then
    Exit;

  dmAntType_add.Add(rec);


end;

//--------------------------------------------------------------------
procedure TdmAntType_Import.AddAntenna(aFileName: string);
//--------------------------------------------------------------------
var
  sExt: string;
begin

  sExt:=LowerCase(ExtractFileExt(aFileName));

  if sExt='.msi' then
    LoadFromMSI (aFileName) else

  if (sExt='.adf') or (sExt='.txt') then
    LoadFromADF (aFileName);

end;

procedure TdmAntType_Import.ExecuteDlg;
begin
  Progress_ExecDlg_proc(ExecuteProc);
end;

//--------------------------------------------------------------------
procedure TdmAntType_Import.ExecuteProc;
//--------------------------------------------------------------------
var i: integer;
begin
  dmOn  OpenQuery(qry_AntTypes,
    Format('SELECT id, folder_id, name FROM '+ TBL_AntennaType +
           ' WHERE folder_id=%d', [Params.FolderID]));

  TdmMDB_ant.Init;
  dmMDB_ant.Open;


 // db_SetComponentADOConnection(Self, Params.ADOConnection);


  for i:=0 to Params.FileNames.Count-1 do
  //  if not Terminated then
  begin
  //  DoProgress(i+1, Params.FileNames.Count);

 //   gl_DB.ADOConnection.BeginTrans;

    AddAntenna (Params.FileNames[i]);


{    if Terminated then
      gl_DB.ADOConnection.RollbackTrans
    else
}

//    gl_DB.ADOConnection.CommitTrans;

  end;

 // if Tfrm_AntType_Preview.ExecDlg then
 // ;

end;


// ---------------------------------------------------------------
procedure TdmAntType_Import.AddToMDB(aRec: TdmAntTypeAddRec);
// ---------------------------------------------------------------

 // bChecked: Boolean;

//  rec: TdmAntTypeAddRec;

begin
//bChecked:= FindByName(aRec.NewNAME,0);

  db_AddRecord (dmMDB_ant.tbl_AntennaType,
    [
   //   db_Par(FLD_CHECKED,    bChecked),

      db_Par(FLD_NAME,       aRec.NewNAME),

      db_Par(FLD_Freq_MHz,   aRec.Freq_MHz),
      db_Par(FLD_Gain_dB,    aRec.Gain_dB),

      db_Par(FLD_horz_mask,  aRec.horz_mask),
      db_Par(FLD_vert_mask,  aRec.vert_mask),

      db_Par(FLD_vert_width, aRec.VERT_WIDTH),
      db_Par(FLD_horz_width, aRec.HORZ_WIDTH),

      db_Par(FLD_Diameter, aRec.Diameter),

//      db_Par(FLD_Tilt_Type, aRec.TiltType),

      db_Par(FLD_Polarization_str, aRec.Polarization_str),

      db_Par(FLD_FILENAME, aRec.FileName)

//      db_Par(FLD_FileName, aRec.FileName)

   //   db_Par(FLD_Comment,  oProp.Comment)
    ]);

end;



function TdmAntType_Import.FindByName(aName : string; aFolderID : Integer):
    Boolean;
var v: Variant;
begin
  if aFolderID>0 then
    v := qry_AntTypes.Lookup(FLD_NAME+';'+FLD_FOLDER_ID, VarArrayOf([aName,aFolderID]), FLD_ID)
  else
    v := qry_AntTypes.Lookup(FLD_NAME+';'+FLD_FOLDER_ID, VarArrayOf([aName,null]), FLD_ID);
//    v := qry_AntTypes.Lookup(FLD_NAME, aName, FLD_ID);

  Result := not VarIsNull(v);
end;



//--------------------------------------------------------------------
procedure TdmAntType_Import.LoadFromADF(aFileName: string);
//--------------------------------------------------------------------
var i, j, id: integer;
  sName: string;

  sHorz_mask: string;
  sVert_mask: string;

  rec: TdmAntTypeAddRec;

begin
  if (aFileName='') or (not FAdfFile.LoadFromFile (aFileName)) then
    Exit;

//  if qry_AntTypes.Locate() then


  sName :=FAdfFile.ModelName+'(H)';

//  if not FindByName(sName,aFolderID) then

//  if gl_DB.GetIntFieldValue (TBL_ANTENNATYPE, FLD_ID,
  //                      [db_Par (FLD_NAME,      sName),
    //                     db_Par (FLD_FOLDER_ID, aFolderID)]) = 0 then
  // add horz polarization
  begin
    // ---------------------------------------------------------------
   // sVert_mask := '';
//    sHorz_mask := '';

    sVert_mask := FAdfFile.H.Vert.GetDBStr;
    sHorz_mask := FAdfFile.H.Horz.GetDBStr;


(*    for j:=0 to FAdfFile.H.Vert.Count-1 do
//    for j:=0 to High(FAdfFile.H.Vert) do
      sVert_mask := sVert_mask + Format('%1.1f=%1.2f;', [FAdfFile.H.Vert[j].Angle, FAdfFile.H.Vert[j].Loss]);

    for j:=0 to FAdfFile.H.Horz.Count-1 do
//    for j:=0 to High(FAdfFile.H.Horz) do
      sHorz_mask := sHorz_mask + Format('%1.1f=%1.2f;', [FAdfFile.H.Horz[j].Angle, FAdfFile.H.Horz[j].Loss]);
*)

    // ---------------------------------------------------------------

    FillChar (rec, SizeOf(rec),0 );

 //   rec.FileName := aFileName;

    rec.Folder_ID         :=Params.FolderID;
    rec.NewNAME           :=sName;
    rec.Diameter          :=FAdfFile.Diameter;
    rec.Gain_dB           :=FAdfFile.Gain_dB1;
    rec.Freq_MHz          :=FAdfFile.Freq_MHz;
//    rec.FrontToBackRatio  :=FAdfFile.FrontToBackRatio;
    rec.VERT_WIDTH         :=FAdfFile.DNAVertWidth;// H.DNAVertWidth;
    rec.HORZ_WIDTH         :=FAdfFile.DNAHorzWidth;//  H.DNAHorzWidth;
  //zzzzzzzzz  rec.POLARIZATION      :=0; //horz
 //   rec.POLARIZATION_RATIO:=FAdfFile.H.Get_Polarization_Ratio;
    rec.COMMENT           :=FAdfFile.Comment;

    rec.Polarization_str :='H';

    rec.FileName := aFileName;

//      TiltType

    rec.vert_mask := sVert_mask;
    rec.horz_mask := sHorz_mask;

    if not FindByName(sName,Params.FolderID) then
      dmAntType_add.Add(rec);

    AddToMDB(rec);

  end;

  sName :=FAdfFile.ModelName+'(V)';

  // add vert polarization
//  if not FindByName(sName,aFolderID) then

//  if gl_DB.GetIntFieldValue (TBL_ANTENNATYPE, FLD_ID,
  //                      [db_Par (FLD_NAME,      sName),
    //                     db_Par (FLD_FOLDER_ID, aFolderID)]) = 0 then
  begin
    // ---------------------------------------------------------------
//    sVert_mask := '';
 //   sHorz_mask := '';

    sVert_mask := FAdfFile.V.Vert.GetDBStr;
    sHorz_mask := FAdfFile.V.Horz.GetDBStr;

(*
    for j:=0 to FAdfFile.V.Vert.Count-1 do
//    for j:=0 to High(FAdfFile.V.Vert) do
      sVert_mask := sVert_mask + Format('%1.1f=%1.2f;', [FAdfFile.V.Vert[j].Angle, FAdfFile.V.Vert[j].Loss]);

    for j:=0 to FAdfFile.V.Horz.Count-1 do
//    for j:=0 to High(FAdfFile.V.Horz) do
      sHorz_mask := sHorz_mask + Format('%1.1f=%1.2f;', [FAdfFile.V.Horz[j].Angle, FAdfFile.V.Horz[j].Loss]);

*)
   // ---------------------------------------------------------------


{    id:=gl_DB.AddRecordID (TBL_ANTENNA_TYPE,
                    [db_Par (FLD_FOLDER_ID,  IIF(aFolderID>0, aFolderID, NULL) ),
                     db_Par (FLD_NAME,       FAdfFile.ModelName+'(V)')]);
   // id:=gl_DB.GetIdent();
}

      FillChar (rec, SizeOf(rec),0 );

      rec.FileName := aFileName;


  //    rec.FileName := aFileName;

      rec.Folder_ID         :=Params.FolderID;
      rec.NewNAME           :=sName;
      rec.Diameter          :=FAdfFile.Diameter;
      rec.Gain_dB           :=FAdfFile.Gain_dB1;
      rec.Freq_MHz          :=FAdfFile.Freq_MHz;
   //   rec.FrontToBackRatio  :=FAdfFile.FrontToBackRatio;

(*
      rec.VERT_WIDTH         :=FAdfFile.V.DNAVertWidth;
      rec.HORZ_WIDTH         :=FAdfFile.V.DNAHorzWidth;
*)
      rec.VERT_WIDTH         :=FAdfFile.DNAVertWidth;// H.DNAVertWidth;
      rec.HORZ_WIDTH         :=FAdfFile.DNAHorzWidth;//  H.DNAHorzWidth;

      rec.Polarization_str :='V';

   //zzzzz   rec.POLARIZATION      :=1; //horz
   //   rec.POLARIZATION_RATIO:=FAdfFile.v.Get_Polarization_Ratio;
      rec.COMMENT           :=FAdfFile.Comment;

      rec.vert_mask := sVert_mask;
      rec.horz_mask := sHorz_mask;

      if not FindByName(sName,Params.FolderID) then
        dmAntType_add.Add(rec);

      AddToMDB(rec);
  end;

end;


begin

end.




// TODO: AddToMDB
//// ---------------------------------------------------------------
//procedure TdmAntType_Import.AddToMDB(aRec: TdmAntTypeAddRec);
//// ---------------------------------------------------------------
//var
//bChecked: Boolean;
//
//rec: TdmAntTypeAddRec;
//
//begin
//bChecked:= FindByName(aRec.NewNAME,0);
//(*
//db_AddRecord (dmMDB_ant.tbl_AntennaType,
//    [
//      db_Par(FLD_CHECKED,    bChecked),
//
//      db_Par(FLD_NAME,       aRec.NewNAME),
//
//      db_Par(FLD_Freq_MHz,   aRec.Freq_MHz),
//      db_Par(FLD_Gain_dB,    aRec.Gain_dB),
//
//      db_Par(FLD_horz_mask,  aRec.horz_mask),
//      db_Par(FLD_vert_mask,  aRec.vert_mask),
//
//      db_Par(FLD_vert_width, aRec.VERT_WIDTH),
//      db_Par(FLD_horz_width, aRec.HORZ_WIDTH),
//
//      db_Par(FLD_FileName, aRec.FileName)
//
//   //   db_Par(FLD_Comment,  oProp.Comment)
//    ]);
//*)
//
//(*
//
// FillChar (rec, SizeOf(rec),0 );
//
//// rec.Folder_ID         :=aFolderID;
//rec.NewNAME           :=aRec.NewNAME;
//rec.Gain_dB           :=FAdfFile.Gain_dB;
//rec.Freq_MHz          :=FAdfFile.Freq_MHz;
//rec.FrontToBackRatio  :=FAdfFile.FrontToBackRatio;
//rec.VERT_WIDTH         :=FAdfFile.H.DNAVertWidth;
//rec.HORZ_WIDTH         :=FAdfFile.H.DNAHorzWidth;
////zzzzzzzzz  rec.POLARIZATION      :=0; //horz
//rec.POLARIZATION_RATIO:=FAdfFile.H.Get_Polarization_Ratio;
//rec.COMMENT           :=FAdfFile.Comment;
//
//rec.vert_mask := aRec.Vert_mask;
//rec.horz_mask := aRec.Horz_mask;
//
//dmAntType_add.Add(rec);
//*)
//end;


// TODO: SetConnectionObject
//procedure TdmAntType_Import.SetConnectionObject(aConnectionObject: _Connection);
//begin
//ADOConnection1.ConnectionObject:=aConnectionObject;
//end;

