unit u_ant_MSI;

interface
uses Classes,SysUtils,

    u_antenna_custom,

    u_Func_arrays,
    u_func
    ;

type

  TMsiFile = class(TCustomAntennaFile)
  public
    Diagram: TAntennaDiagram;

    constructor Create;
    destructor Destroy; override;

    function LoadFromFile(aFileName: string): Boolean; override;
    function SaveToFile(aFileName: string): Boolean;
  end;


implementation


constructor TMsiFile.Create;
begin
  inherited Create;
  Diagram := TAntennaDiagram.Create();
end;

destructor TMsiFile.Destroy;
begin
  FreeAndNil(Diagram);
  inherited Destroy;
end;

//---------------------------------------------------------
// Load from MSI file
//---------------------------------------------------------

function TMsiFile.LoadFromFile(aFileName: string): Boolean;
var
  oStrList: TStringList;
  strArr: TStrArray;
  str, s1, s2, s3: string;
  angle, value: Double;
  i, total: Integer;
//  k, m: Integer;
  eAngle: Double;
  eLoss: Double;
  s: string;
  state: (HEADER_PART,HORIZONTAL_PART,VERTICAL_PART);

begin
  Result:=false;

  Diagram.Clear;

  if not FileExists(aFileName) then
    Exit;


  oStrList:=TStringList.Create;
  Result:=false;

  try
    oStrList.LoadFromFile (AFileName);
  except
  end;

  state:=HEADER_PART;

    // �� ��������� ��������� ��� ������� ������ ������ �����
  if oStrList.Count>0 then
    Modelname:=oStrList[0];


 // k:=0; m:=0;

  i := 0;

//  while i<oStrList.Count do
  for i:=0 to oStrList.Count-1 do
  begin
      // �������� ���������� �� �������
    s:=ReplaceStr (oStrList[i], #9, ' ');

    strArr:=StringToStrArray (s,' ');
    total:=High(strArr)+1;
    s1:=''; s2:=''; s3:='';

    if total>=1 then s1:=strArr[0];
    if total>=2 then s2:=strArr[1];
    if total>=3 then s3:=strArr[2];

    if Eq(s1,'NAME')       then Modelname:=s2 + s3     else
    if Eq(s1,'FREQUENCY')  then Freq_MHz:=AsFloat(s2) else
    if Eq(s1,'Gain')   then begin

                                 Gain_dB1:=AsFloat(s2);
                                // Gain_dBi:=AsFloat(s2);
                                   //���� ������� ��������� dBd -> ��������� 3.5
                                 if Pos('dbd',LowerCase(s))>0 then
//                                   Gain_dBi:=Gain_dBi + 2.15;
                                   Gain_dB1:=Gain_dB1 + 2.15;
                           end else

    if Eq(s1,'TILT')       then
    begin
      Tilt_str:=s2;

    //  if eq(s2, 'Electrical') then TiltType :=ttElectrical
        //   TiltElectrical:=s2
    end
                           else
    if Eq(s1,'COMMENT')    then Comment:=s2+s3 else

    if Eq(s1,'HORIZONTAL') then state:=HORIZONTAL_PART  else
    if Eq(s1,'VERTICAL')   then state:=VERTICAL_PART
    else

    if state in [HORIZONTAL_PART,VERTICAL_PART] then
    begin
    //  iCount:=AsInteger(s2);

      eAngle:=AsFloat(s1);
      eLoss :=AsFloat(s2);

    //  if (eAngle>=0) and (eAngle<360) then
      case state of
        HORIZONTAL_PART: Diagram.Horz.AddItem(eAngle, eLoss);
        VERTICAL_PART:   Diagram.Vert.AddItem(eAngle, eLoss);
      end;
    end;
  end;

  //  DefineDNAWidth(); // ������ ��� � ������������ ������ ��� �������

  Result:=true; // Everything's OK

  oStrList.Free;

end;

// ---------------------------------------------------------------
function TMsiFile.SaveToFile(aFileName: string): Boolean;
// ---------------------------------------------------------------
var
  oStrList: TStringList;
  i: Integer;
 // s: string;
begin
  oStrList:=TStringList.Create;
  Result:=false;

  with oStrList do
  begin
    Add ( Format('NAME %s',      [Modelname]));
    Add ( Format('FREQUENCY %1.0f', [Freq_MHz]));
//    Add ( Format('Gain_dBi %1.0f',      [Gain_dBi]));
//    Add ( Format('Gain_dBi %1.0f',      [Gain_dB]));
    Add ( Format('Gain %1.0f dB',      [Gain_dB1]));

//    if TiltType=ttElectrical then
//      s:='ELECTRICAL'
//    else
//      s:='';

    Add ( Format('TILT %s',      [Tilt_str]));
    Add ( Format('COMMENT %s',   [Comment]));

    Add ( Format('HORIZONTAL %d',   [Diagram.Horz.Count]));
//    Add ( Format('HORIZONTAL %d',   [Length(Diagram.Horz)]));
    for i:=0 to Diagram.Horz.Count-1 do
//    for i:=0 to High(Diagram.Horz) do
      Add(Format('%1.1f %f', [Diagram.Horz[i].Angle, Diagram.Horz[i].Loss_db]));

    Add ( Format('VERTICAL %d',   [Diagram.Vert.Count]));
//    Add ( Format('VERTICAL %d',   [Length(Diagram.Vert)]));
    for i:=0 to Diagram.Vert.Count-1 do
//    for i:=0 to High(Diagram.Vert) do
      Add(Format('%1.1f %f', [Diagram.Vert[i].Angle, Diagram.Vert[i].Loss_db]));
  end;

  try
    oStrList.SaveToFile (aFileName);
    Result:=true; // Everything's OK
  finally
    FreeAndNil(oStrList);
                
  end;
end;


//---------------------------------------------------------




{ TAntImport }

{

var
  obj: TMsiFile;
  i: integer;
begin

  obj:=TMsiFile.Create;

  obj.LoadFromMSIFile ('u:\727727X2.MSI');
  obj.SaveToMSIFile ('u:\727727X2_.MSI');

  i:=obj.GetVertWidth;
  i:=obj.GetHorzWidth;

  i:=0;
 }



// W:\RPLS_DB\src\AntType_Import\data
const

  DEF='D:\.ant\7143.24.33.000.msi';
  DEF_='D:\.ant\7143.24.33.00__.msi';


(*var
  obj: TMsiFile;
  i: integer;
begin

  obj:=TMsiFile.Create;
  obj.LoadFromFile(DEF);

  obj.SaveToFile(DEF_);

*)
end.

