unit u_ant_pathLoss;

interface
uses Classes,SysUtils,ADODB,

  u_antenna_custom,

  u_func,

  u_files
  , Masks;

type
  //--------------------------------------------------------------------
  TPathLossFile = class(TCustomAntennaFile)
  //--------------------------------------------------------------------
  private
    DNAHorzWidth,
    DNAVertWidth   : double; // ������ ���,���� ��������� ��������������

//    function Find_Angle_InMask(aAngle: double; aMask: TAntennaMask): boolean;
  public
    H: TAntennaDiagram;
    V: TAntennaDiagram;


    constructor Create;
    destructor Destroy; override;
    
    function LoadFromFile(aFileName: string): boolean;

  end;



//==========================================================
implementation

constructor TPathLossFile.Create;
begin
  inherited Create;
  H := TAntennaDiagram.Create();
  V := TAntennaDiagram.Create();
end;


destructor TPathLossFile.Destroy;
begin
  FreeAndNil(V);
  FreeAndNil(H);
  inherited Destroy;
end;

//---------------------------------------------------------
// Load from ADF file
//---------------------------------------------------------
function TPathLossFile.LoadFromFile(aFileName: string): boolean;
//---------------------------------------------------------
var strList: TStringList;
    strArr: TStrArray;
    str,s1,s2,str3, sFileName: string;
    dAngle,eLoss: double;
    iAngle,i,total: integer;
    k,m,n,p: integer;

    state: (HEADER_PART,HORIZONTAL_PART_H,VERTICAL_PART_H,
                        HORIZONTAL_PART_V,VERTICAL_PART_V);
  iLen: integer;
  s: string;
//    DiagramHorz_H, DiagramVert_H,
//    DiagramHorz_V, DiagramVert_V: TMask;
begin
  H.Clear;
  V.Clear;


  Result:=false;

  if not FileExists(aFileName) then Exit;

  strList:=TStringList.Create;
  Result:=false;

  try
    strList.LoadFromFile (AFileName);
  except
  end;

  state:=HEADER_PART;

//  k:=0; m:=0; n:=0; p:=0;

  if strList.Count<10 then
    Exit;


  i := 0;
  while i<strList.Count do
  begin
    s:= strList[i];

    strArr:=StringToStrArray (s,' ');

    iLen:=High(strArr)+1;
    s1:='';
    s2:='';

    if iLen>=1 then s1:=strArr[0];
    if iLen>=2 then s2:=strArr[1];


    case i of
      0: ; //RFS
      1: ModelName :=s1; //SB4-127A
      2: ; //4 ; Compactline Single Polarisation
      3: ; //NONE
      4: ; //NONE
      5: ; //20020701
    {  6: ; // FRT 020705
      6: ; // 12700 - 13250 MHZ
      6: ; // 41.5 dBi
      6: ; // 1.4 Deg
}


    end;



  end;    // while

{HH 107
-180.0  -67.00
 -95.0  -67.00
 -80.0  -60.00
 -66.5  -48.25
 -47.5  -42.90
 -30.0  -42.00
 -20.5  -41.00
}


  strList.Free;
end;


end.

