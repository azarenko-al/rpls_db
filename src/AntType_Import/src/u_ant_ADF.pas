unit u_ant_ADF;

interface
uses Classes,SysUtils,

  u_antenna_custom,

  u_Func_arrays,
  u_func;

type
  //--------------------------------------------------------------------
  TAdfFile = class(TCustomAntennaFile)
  //--------------------------------------------------------------------
  private
  public
    H: TAntennaDiagram;
    V: TAntennaDiagram;

    DNAHorzWidth,
    DNAVertWidth   : double; // ������ ���,���� ��������� ��������������


    constructor Create;
    destructor Destroy; override;
    
    function LoadFromFile(aFileName: string): boolean; override;

  end;



//==========================================================
implementation


constructor TAdfFile.Create;
begin
  inherited Create;
  H := TAntennaDiagram.Create();
  V := TAntennaDiagram.Create();
end;


destructor TAdfFile.Destroy;
begin
  FreeAndNil(V);
  FreeAndNil(H);
  inherited Destroy;
end;

//---------------------------------------------------------
// Load from ADF file
//---------------------------------------------------------
function TAdfFile.LoadFromFile(aFileName: string): boolean;
//---------------------------------------------------------
var strList: TStringList;
  bIsDigit: Boolean;

  rHeader: record
     DESCR1: string;
     DESCR2: string;
     MODNUM: string;

     HGHFRQ: Double;
     LOWFRQ: Double;
  end;
         

  strArr: TStrArray;
  str,s1,s2,s3, sFileName: string;
  eAngle,eLoss: double;
  iAngle,i,total: integer;

  state: (st_HEADER_PART,
          st_POLARI_H_H,
          st_POLARI_H_V,
          st_POLARI_V_H,
          st_POLARI_V_V
           );
begin
  H.Clear;
  V.Clear;

  Result:=false;

  if not FileExists(aFileName) then
    Exit;

  strList:=TStringList.Create;
  Result:=false;

  try
    strList.LoadFromFile (AFileName);
  except
  end;

  state:=st_HEADER_PART;

  AFileName := UpperCase(AFileName);

  if Pos('LP1-', aFileName)>0 then Diameter := 0.3*1;
  if Pos('LP2-', aFileName)>0 then Diameter := 0.3*2;
  if Pos('LP3-', aFileName)>0 then Diameter := 0.3*3;
  if Pos('LP4-', aFileName)>0 then Diameter := 0.3*4;
  if Pos('LP6-', aFileName)>0 then Diameter := 0.3*6;

  if Pos('LPX1-', aFileName)>0 then Diameter := 0.3*1;
  if Pos('LPX2-', aFileName)>0 then Diameter := 0.3*2;
  if Pos('LPX3-', aFileName)>0 then Diameter := 0.3*3;
  if Pos('LPX4-', aFileName)>0 then Diameter := 0.3*4;
  if Pos('LPX6-', aFileName)>0 then Diameter := 0.3*6;




 // k:=0; m:=0; n:=0; p:=0;

  for i:=0 to strList.Count-1 do
  begin
    // �������� ���������� �� �������
    strList[i]:=ReplaceStr (strList[i], #9, ' ');

    bIsDigit := Copy(strList[i], 1, 1)[1] in ['0'..'9','-'];


//    strList[i]:=ReplaceStr (strList[i], ',', ' ');

//    strArr:=StringToStrArray (strList[i],' ');
    strArr:=StringToStrArray (strList[i], ',');

    total:=High(strArr)+1;
    s1:=''; s2:=''; s3:='';
    if total>=1 then s1:=strArr[0];
    if total>=2 then s2:=strArr[1];
    if total>=3 then s3:=strArr[2];

//DESCR1:,7W GHz0.8M Single Polarization ULTRA HIGH PERFORMANCE Deep Reflector Antenna
//DESCR2:,ETSI EN 302 217  Range 1 Class 3


//MODNUM:,SLC0878DS6
    if Eq(s1,'MODNUM:') then rHeader.MODNUM := s2 else
    if Eq(s1,'DESCR1:') then rHeader.DESCR1 := s2 else
    if Eq(s1,'DESCR2:') then rHeader.DESCR2 := s2 else
    if Eq(s1,'LOWFRQ:') then rHeader.LOWFRQ := AsFloat(s2) else //LOWFRQ:,7100
    if Eq(s1,'HGHFRQ:') then rHeader.HGHFRQ := AsFloat(s2) else //HGHFRQ:,8500

    ;


    if Eq(s1,'MODNUM:') then ModelName := s2 + s3     else
    if Eq(s1,'LOWFRQ:') then Freq_MHz := AsFloat(s2) else
    if Eq(s1,'HGHFRQ:') then Freq_MHz := (AsFloat(s2)+Freq_MHz)/2 else
    if Eq(s1,'MDGAIN:') then Gain_dB1 := AsFloat(s2) else
//    if Eq(s1,'MDGAIN:')     then Gain_dBi := AsFloat(s2) else

//ELTILT:,0       //��� �������, 0-�������������, 1-������������

(*    if Eq(s1,'ELTILT:')     then
    begin
      // TiltElectrical  :=s2
    end
    else
*)

    if Eq(s1,'FRTOBA:')     then FrontToBackRatio:= AsFloat(s2) else

    if Eq(s1,'AZWIDT:')     then DNAHorzWidth := AsFloat(s2) else
    if Eq(s1,'ELWIDT:')     then DNAVertWidth := AsFloat(s2) else

  //  if Eq(s1,'ATVSWR:')     then Diameter := AsFloat(s2) else
 //   ATVSWR:,1.3

    if (Eq(s1,'POLARI:')) then
    begin
      if (Eq(s2,'H/H')) then state:=st_POLARI_H_H else
      if (Eq(s2,'H/V')) then state:=st_POLARI_H_V else
      if (Eq(s2,'V/H')) then state:=st_POLARI_V_H else
      if (Eq(s2,'V/V')) then state:=st_POLARI_V_V;
    end;
    

 (*   if (Eq(s1,'POLARI:')) and (Eq(s2,'H/H')) then state:=st_POLARI_H_H   else
    if (Eq(s1,'POLARI:')) and (Eq(s2,'H/V')) then state:=st_POLARI_H_V   else
    if (Eq(s1,'POLARI:')) and (Eq(s2,'V/H')) then state:=st_POLARI_V_H   else
    if (Eq(s1,'POLARI:')) and (Eq(s2,'V/V')) then state:=st_POLARI_V_V   else
*)

    if Eq(s1,'PATCUT:')     then Continue  else
    if Eq(s1,'NUPOIN:')     then Continue  else
    if Eq(s1,'ENDFIL:')     then Break  else

    if (state <> st_HEADER_PART) and (bIsDigit) then

//      in [POLARI_H_H,POLARI_H_V,
 //                POLARI_V_H,POLARI_V_V] then
    begin
      eAngle:=AsFloat(s1);

(*
      if (eAngle<0) and (eAngle>=-180) then
        eAngle:= 360 + eAngle;
*)
      eLoss :=Abs(AsFloat(s2));

    (*  if (eAngle>=0) and (eAngle<360) then
      begin
*)
      case state of
        st_POLARI_H_H : H.Horz.AddItem(eAngle, eLoss);
        st_POLARI_H_V : H.Vert.AddItem(eAngle, eLoss);

        st_POLARI_V_V : V.Vert.AddItem(eAngle, eLoss);
        st_POLARI_V_H : V.Horz.AddItem(eAngle, eLoss);
      end;
     // end;
    end;
  end;

  Result:=true; // Everything's OK

  strList.Free;
end;

end.

