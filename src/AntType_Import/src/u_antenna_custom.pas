unit u_antenna_custom;

interface

uses Classes,SysUtils;
 // u_antenna_mask;

type


  TAntennaMaskItem = class(TCollectionItem)
  public
    Angle: double;
    Loss_db: double;
  end;


  TAntennaMask = class(TCollection)
  private
    function Find_Angle(aAngle: double): boolean;
    function GetItems(Index: Integer): TAntennaMaskItem;
  public
    constructor Create;
    procedure AddItem(aAngle, aLoss: double);

    function GetLoss(aAngle: integer): Double;
    function IsOMNI: Boolean;
    function GetWidth: Integer;

    function GetDBStr: string;

    property Items[Index: Integer]: TAntennaMaskItem read GetItems; default;
  end;


  //--------------------------------------------------------------------
  TCustomAntennaFile = class(TObject)
  //--------------------------------------------------------------------
  public
    Diameter         : Double;

    Freq_MHz         : Double;

    ModelName        : string;
    Gain_dB1          : Double;

    Comment          : string;
    FrontToBackRatio : Double;
    Producer         : string;
//    TiltElectrical   : string;

    Tilt_str  : string;

//    TiltType: (ttMechanical,ttElectrical);

    function LoadFromFile(aFileName: string): Boolean; virtual; abstract;
  end;


  //--------------------------------------------------------------------
  TAntennaDiagram = class(TObject)
  //--------------------------------------------------------------------
  public
    DNAHorzWidth: Integer;
    DNAVertWidth: Integer;

    Horz: TAntennaMask;
    Vert: TAntennaMask;

    constructor Create;
    destructor Destroy; override;

    procedure Clear;

    function GetHorzLoss(aAngle: integer): Double;
    function GetHorzWidth: Integer;

    function GetVertLoss(aAngle: integer): Double;
    function GetVertWidth: Integer;

    function Get_Polarization_Ratio: Double;
    function IsOMNI: Boolean;
  end;



implementation



constructor TAntennaDiagram.Create;
begin
  inherited Create;
  Horz := TAntennaMask.Create();
  Vert := TAntennaMask.Create();
end;

destructor TAntennaDiagram.Destroy;
begin
  FreeAndNil(Vert);
  FreeAndNil(Horz);
  inherited Destroy;
end;

procedure TAntennaDiagram.Clear;
begin
  Vert.Clear;
  Horz.Clear;
end;

function TAntennaDiagram.GetHorzLoss(aAngle: integer): Double;
begin
  Result:=Horz.GetLoss(aAngle);
end;

function TAntennaDiagram.GetHorzWidth: Integer;
begin
  Result:=Horz.GetWidth;
end;

function TAntennaDiagram.GetVertLoss(aAngle: integer): Double;
begin
  Result:=Vert.GetLoss(aAngle);
end;

function TAntennaDiagram.GetVertWidth: Integer;
begin
  Result:=Vert.GetWidth;
end;

function TAntennaDiagram.IsOMNI: Boolean;
begin
  Result:= Horz.IsOMNI;
end;

// ---------------------------------------------------------------
function TAntennaMask.GetWidth: Integer;
// ---------------------------------------------------------------
var
  i, ind, iNulAngleInd: Integer;
  iMinIndex: Integer;
  iMinValue: Double;
  isOMNI: Boolean;
begin
  Result:=0;

  //def OMNI
  isOMNI:=True;
  for i:=0 to Count-1 do
    if (Items[i].Loss_db > 3) then begin isOMNI:=False; Break; end;

  if isOMNI then begin
    Result:=360;
    Exit;
  end;

  iMinValue:=999;
  iMinIndex:=999;

  for i:=0 to Count-1 do
    if Items[i].Loss_db < iMinValue then
    begin
      iMinValue:=Items[i].Loss_db;
      iMinIndex:=i;

//      iNulAngleInd:=i; Break;
    end;

  iNulAngleInd:=iMinIndex;

//  iNulAngleInd:=-1;
//  for i:=0 to Count-1 do
//    if Items[i].Loss_db=0 then begin iNulAngleInd:=i; Break; end;

  if iNulAngleInd<0 then
   Exit;

  for i:=0 to Count-1 do
  begin
    ind:=(iNulAngleInd + i) mod Count;
    if (Items[ind].Loss_db < 3) then Inc(Result) else Break;
  end;

  for i:=0 to Count-1 do
  begin
    ind:=((iNulAngleInd - i) + Count) mod Count;
    if (Items[ind].Loss_db < 3) then Inc(Result) else Break;
  end;
end;


function TAntennaMask.IsOMNI: Boolean;
var
  i: Integer;
begin
  Result:= True;

  for i:=0 to Count-1 do
    if Items[i].Loss_db>3 then begin Result:= False; Break; end;
end;

// ---------------------------------------------------------------
function TAntennaMask.GetLoss(aAngle: integer): Double;
// ---------------------------------------------------------------
var
  i: Integer;
begin
  Result:=0;

  for i:=0 to Count-1 do
    if (Items[i].Angle=aAngle) then begin Result:=Items[i].Loss_db; Exit; end;

  for i:=0 to Count-1-1 do
    if ((Items[i].Angle>aAngle) and (Items[i+1].Angle<aAngle)) then
      begin
        Result:=Trunc(Items[i].Angle); Exit;
      end;
end;

// ---------------------------------------------------------------
function TAntennaDiagram.Get_Polarization_Ratio: Double;
// ---------------------------------------------------------------
var
  i: Integer;
  dLoss0: Double;
begin
  Result:= 0;
  dLoss0 := 0;

  for i:=0 to Horz.Count-1 do
    if Horz[i].Angle = 0 then
      begin
        dLoss0:= Horz[i].Loss_db;
        Break;
      end;

  for i:=0 to Vert.Count-1 do
    if Vert[i].Angle = 0 then
      begin
        Result:= Abs(Vert[i].Loss_db - dLoss0);
        Break;
      end;
end;


constructor TAntennaMask.Create;
begin
  inherited Create(TAntennaMaskItem);
end;

// ---------------------------------------------------------------
procedure TAntennaMask.AddItem(aAngle, aLoss: double);
// ---------------------------------------------------------------
begin             
  if (aAngle<0) then
    aAngle:= 360 + aAngle;


  if Find_Angle(aAngle) then
    exit;
                       
  with TAntennaMaskItem(Add) do
  begin
    Angle:=aAngle;
    Loss_db:=aLoss;
  end;
end;

function TAntennaMask.GetItems(Index: Integer): TAntennaMaskItem;
begin
  Result := TAntennaMaskItem(inherited Items[Index]);
end;

//--------------------------------------------------------------------------
function TAntennaMask.Find_Angle(aAngle: double): boolean;
//--------------------------------------------------------------------------
var  I: Integer;
begin
  Result:= False;
  for i:=0 to Count-1 do
    if Items[i].Angle = aAngle then
    begin
      Result:= True;
      Exit;
    end;
end;


function TAntennaMask.GetDBStr: string;
var
  i: Integer;
begin
  Result := '';

  for i:=0 to Count-1 do
    Result := Result + Format('%1.1f=%1.2f;', [Items[i].Angle, Items[i].Loss_db]);

  i:=Length(Result);
  i:=0;
  // TODO -cMM: TAntennaMask.GetDBStr default body inserted
end;

end.

