unit dm_Main_app;

interface

uses
  Windows, Classes,

  dm_Main,

  dm_MDB,

  dm_AntType_Import,
  d_AntType_Import,

  u_func,

  u_const_msg
  ;

type
  TdmMain_app = class(TDataModule)
    procedure DataModuleCreate(Sender: TObject);

  end;


var
  dmMain_app: TdmMain_app;

//===================================================================
implementation {$R *.DFM}
//===================================================================

procedure PostMsg;
var
  h: hwnd;
begin
    h:=FindWindow (PChar('Tfrm_Main_MDI'), nil);
    if h>0 then
      SendMessage (h, WM_EXTERNAL_PROJECT_IMPORT_DONE,0,0);
//      SendMessage (h, WM_EXTERNAL_IMPORT_DONE,0,0);

end;



//--------------------------------------------------------------
procedure TdmMain_app.DataModuleCreate(Sender: TObject);
//--------------------------------------------------------------
var
  iFolderID: integer;
begin
  iFolderID:=AsInteger(ParamStr(1));

  TdmMain.Init;
  if not dmMain.OpenDB_reg then
    Exit;

  TdmMDB_ant.Init;


  TdmAntType_Import.Init;
//  dmAntType_Import.Params.ADOConnection1:=dmMain.ADOConnection;

  //dmAntType.AddTest;

  Tdlg_AntennaType_import.ExecDlg (iFolderID);

end;





end.
