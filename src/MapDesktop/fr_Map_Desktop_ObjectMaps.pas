unit fr_Map_Desktop_ObjectMaps;

interface
{$I ver.inc}

uses
   Dialogs, Classes, cxGridCustomView, Controls, Forms, cxGridDBTableView, cxGrid,
  ImgList, cxGridTableView, cxClasses, cxControls, cxGridCustomTableView,  sysutils,
  ActnList, Menus, Db, ADODB, Variants,  cxGridLevel,
  cxCheckBox, Windows,

  u_const_msg,

  dm_Main,


  i_Map_engine,
  Map_Engine_TLB,

//  u_MapDesktop_var,

  dm_Map_Desktop,

  dm_MapEngine_store,
  dm_MapEngine_new,

  u_Mapx_Style ,

//  d_Map_Attributes,

  u_mapx,

  u_Storage,

  dm_Onega_DB_data,

  u_MapDesktop_classes,

 // I_Filter,

//  u_map_vars,

//  fr_Map_Desktop_Maps,

//  fr_CalcMaps,


  u_cx,

  u_db,

//  i_MapX_tools,

//  d_Map_Label_Setup,

  u_const,
  u_const_db,

  u_const_str,


  MapXLib_TLB,

  StdCtrls, DBCtrls, ToolWin, ComCtrls, Grids,
  DBGrids, RxMemDS;


type
  Tframe_Map_Desktop_ObjectMaps = class(TForm)
    act_Allow_Zoom: TAction;
    act_Bottom: TAction;
    act_Check: TAction;
    act_Check_All: TAction;
    act_Disallow_Zoom: TAction;
    act_Down: TAction;
    act_MapAdd: TAction;
    act_MapDel: TAction;
    act_SetMaxZoom: TAction;
    act_SetMinZoom: TAction;
    act_Top: TAction;
    act_UnCheck: TAction;
    act_Uncheck_All: TAction;
    act_Up: TAction;
    ActionList2: TActionList;
    menu_MapObjects: TPopupMenu;
    MenuItem1: TMenuItem;
    MenuItem10: TMenuItem;
    MenuItem11: TMenuItem;
    MenuItem2: TMenuItem;
    MenuItem3: TMenuItem;
    ImageList2: TImageList;
    cxGrid1: TcxGrid;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1DBTableView1: TcxGridDBTableView;
    col_Checked: TcxGridDBColumn;
    col_Caption: TcxGridDBColumn;
    co_AUTOLABEL: TcxGridDBColumn;
    col_ID: TcxGridDBColumn;
    ActionList1_new: TActionList;
    act_Dlg_Label_Style: TAction;
    ds_ObjectMaps: TDataSource;
    ADOStoredProc_Object_Maps: TADOStoredProc;
    act_Refresh: TAction;
    act_Attributes: TAction;
    N1: TMenuItem;
    actAttr1: TMenuItem;
    act_Object_SetDefault: TAction;
    actObjectSetDefault1: TMenuItem;
    ToolBar1: TToolBar;
    cb_Check_All: TDBCheckBox;
    col_Attr_: TcxGridDBColumn;
    col_Style_: TcxGridDBColumn;
    col_label_style: TcxGridDBColumn;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    DBMemo1: TDBMemo;
    procedure ActionList1_newUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure act_AttributesExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
//    procedure _Actions(Sender: TObject);
//    procedure ActionList1Update(Action: TBasicAction;  var Handled: Boolean);
    procedure FormDestroy(Sender: TObject);
    procedure act_CheckExecute(Sender: TObject);
    procedure Button1Click(Sender: TObject);
 //   procedure act_Dlg_Label_StyleExecute(Sender: TObject);
//    procedure Button1Click(Sender: TObject);
//    procedure act_Object_SetDefaultExecute(Sender: TObject);
//    procedure col_FilterButtonClick(Sender: TObject; AbsoluteIndex: Integer);


 //   procedure dx_MapsExit(Sender: TObject);
//    procedure col_CheckedToggleClick(Sender: TObject; const Text: String; State: TdxCheckBoxState);
//    procedure dx_MapsHotTrackNode(Sender: TObject; AHotTrackInfo: TdxTreeListHotTrackInfo; var ACursor: TCursor);
//    procedure dx_Objects11ColumnClick(Sender: TObject; Column: TdxDBTreeListColumn);
//    procedure ds_MapObjectsDataChange(Sender: TObject; Field: TField);
    procedure col_StylePropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure col__CheckedPropertiesChange(Sender: TObject);
    procedure co__aUTOLABELPropertiesChange(Sender: TObject);
//    procedure cb_Check_AllClick(Sender: TObject);
    procedure Move_Actions(Sender: TObject);
    procedure qry_ObjectMaps11AfterPost(DataSet: TDataSet);
  //  procedure ToolButton1Click(Sender: TObject);
    procedure col_AttrPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure col_Style_PropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure cxGrid1DBTableView1Column1PropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);

  private
    FID : Integer;

    FDataset: TDataset;

    FMap: TMap;

    FOnMapChecked: TNotifyEvent;

    procedure Dlg_Attributes;
    procedure Dlg_Label_style_new;
//    procedure Dlg_Label_Style111;
//    procedure Dlg_Label_Style_new1;
    procedure Dlg_Style;

    procedure DoMapChecked;
    procedure UpdateData(aAutoLabel: Boolean);
//    procedure UpdateFilteredMapObjects;
  public
  //  IsAutoUpdate : Boolean;

    procedure View(aID: Integer; aMap: TMap);

    property OnMapChecked: TNotifyEvent read FOnMapChecked write FOnMapChecked;

  end;


//==================================================================

implementation  {$R *.dfm}

const
  FLD_MapObject_Name = 'MapObject_Name';
  FLD_LabelStyle     = 'Label_Style';



procedure Tframe_Map_Desktop_ObjectMaps.ActionList1_newUpdate(Action:
    TBasicAction; var Handled: Boolean);
var
  b: Boolean;
begin
  b:=FDataset.RecordCount>0;


  act_Check.Enabled    := b;
  act_UnCheck.Enabled  := b;

  act_MapDel.Enabled  := b;

  act_Up.Enabled  := b;


  

end;

// ---------------------------------------------------------------
procedure Tframe_Map_Desktop_ObjectMaps.act_AttributesExecute(Sender: TObject);
// ---------------------------------------------------------------
var
  s: string;
  sObjName: string;
begin
// db_View(FDataset);

  sObjName:= FDataset.FieldByName(FLD_NAME).AsString;

  s:='';

 // Tdlg_Map_Attributes.ExecDlg(sObjName, s);

end;


//-------------------------------------------------------------------
procedure Tframe_Map_Desktop_ObjectMaps.Move_Actions(Sender: TObject);
//-------------------------------------------------------------------
var
  k: Integer;
  iID: integer;
  iRes: Integer;
begin

{
  if (Sender=act_Down) then
    k :=-1 else
 //   oMapItem.Index := oMapItem.Index +1 else

  if (Sender=act_Up) then
    k :=1 else

  if (Sender=act_Bottom) then
    k :=-999 else

  if (Sender=act_Top) then
    k :=999;

   iID:= FDataset.FieldByName(FLD_ID).AsInteger;

   iRes:=dmOnega_DB_data.MapDesktop_Move_Maps(iID, k);

   View(FID, FMap);
 }
 
end;


//--------------------------------------------------------------------
procedure Tframe_Map_Desktop_ObjectMaps.FormCreate(Sender: TObject);
//--------------------------------------------------------------------
var
  I: Integer;

  oItem: TmiLabel;

begin
  inherited;

 // Assert(Assigned(IFilter));

  Caption:= '��������� ����';

  cb_Check_All.DataSource := dmMap_Desktop.ds_Desktops;
  cb_Check_All.Caption:='�������� ����';

//  dx_Objects.Align:= alClient;
  cxGrid1.Align:= alClient;

 // Panel1.Align:=alClient;

//  dx_Objects.DataSource := dmMap_Desktop.
//  FDataset:=qry_ObjectMaps;
  FDataset:=ADOStoredProc_Object_Maps;

 // ds_MapObjects.DataSet:= dmMap_Desktop.mem_MapObjects;

  act_Object_SetDefault.Caption  := '���������� ����������';


  act_SetMaxZoom.Caption  := '��������� ������������ �������';
  act_SetMinZoom.Caption  := '��������� ����������� �������';

  act_Check.Caption       := '��������';
  act_UnCheck.Caption     := '����� �������';

  act_Check_All.Caption   := '�������� ���';
  act_UnCheck_All.Caption := '����� ������� �� ����';

  col_Caption.Caption     := STR_NAME;
 // col_FileName.Caption    := STR_FILENAME;
  col_Checked.Caption     := STR_CHECKED;



  col_Checked.Visible := True;
  col_Caption.Visible := True;
  co_AUTOLABEL.Visible := True;
//  col_Style.Visible := True;


//  col_map_index.Caption   := '������';
(*
//  col__map_index.Caption  := '������';
  col__FileName.Caption   := STR_FILENAME;

  col_Obj_name.FieldName  := FLD_OBJECTNAME;
*)


//  FRegPath:=REGISTRY_COMMON_FORMS + ClassName + '\1\';

 (* dx_CheckRegistry (dx_Objects, FRegPath + dx_Objects.Name);
  dx_Objects.LoadFromRegistry (FRegPath + dx_Objects.Name);
*)

//  col_map_index.Visible:= False;

 // dx_Objects.KeyField :=FLD_OBJECTNAME;


  col_ID.Visible:=False;
 // col_map_index.Visible:=False;

 
//  dmMap_Desktop.Desktop_SetDefaultMapObjectsChecked;


//  dmMap_Desktop.OnAfterScroll:=  DoOnAfterScroll;
 // dmMap_Desktop.OnBeforeScroll:= DoOnBeforeScroll;

(*  if dmMain.DEBUG then
    ShowMessage('Tdlg_Map_Desktops dmMap_Desktop.OpenDesktops');*)


(*  if dmMain.DEBUG then
    ShowMessage('Tdlg_Map_Desktops dmMap_Desktop.Desktop_SetDefaultMapObjectsChecked');*)
{
  with gl_Reg do
  begin
    BeginGroup (Self, 'Forms\'+ ClassName);

    AddControl (GSPages1,            [PROP_ACTIVE_PAGE_INDEX]);
    AddControl (pn_Top,              [PROP_HEIGHT]);
    AddControl (cb_AutoRefreshCalcs, [PROP_CHECKED]);
  end;}

 // TBItem6.ImageIndex:= -1;

  ////////IsChanged := false;
(*  FIsRebuildMapObjects:= false;
  FIsRebuildMaps:=       false;
  FIsRebuildCalcs:=      false;*)

 // dx_CheckRegistry (dx_Maps,    REGISTRY_FORMS  + ClassName + '\'+ dx_Maps.Name);



//  col_map_index.Sorted:=csUp;


///////////  dmMap_Desktop.mem_MapObjects.SortedField:=FLD_map_index;
//  dmMap_Desktop.mem_MapObjects.SortOptions:=


{
  if not dmMain.DEBUG then
  begin
    dx_Maps.LoadFromRegistry    (REGISTRY_FORMS  + ClassName + '\'+ dx_Maps.Name);
    dx_Objects.LoadFromRegistry (REGISTRY_FORMS  + ClassName + '\'+ dx_Objects.Name);
  end;}
{
  col_ID.Visible:=        dmMain.DEBUG;
  col_Index.Visible:=     dmMain.DEBUG;
}

//  col_map_index.Visible:= dmMain.DEBUG;


  g_Storage.RestoreFromRegistry(cxGrid1DBTableView1, className+'1');

  cxGrid_RemoveUnusedColumns_DBTableView(cxGrid1DBTableView1);

//  col_Attr.Visible := False;

{
  db_CreateField(RxMemoryData1,
     [
       db_Field(FLD_NAME,      ftString),
       db_Field(FLD_CAPTION,   ftString),
       db_Field(FLD_OBJNAME,   ftString),

       db_Field(FLD_CHECKED,   ftBoolean),
       db_Field(FLD_AUTOLABEL, ftBoolean)
     ]);


   RxMemoryData1.Open;


   for I := 0 to g_MapDesktop_Labels.Count - 1 do    // Iterate
   begin
     oItem:=g_MapDesktop_Labels[i];


     db_AddRecord_ (RxMemoryData1,

       [
         FLD_NAME,     oItem.OBJNAME,
         FLD_CAPTION,  oItem.OBJNAME,
         FLD_OBJNAME,  oItem.OBJNAME,

         FLD_CHECKED,  oItem.Checked,
         FLD_AUTOLABEL,oItem.AUTOLABEL
       ]);

   end;    // for



   RxMemoryData1.First;

}

 // cb_Check_All.OnClick := cb_Check_AllClick;

end;

//--------------------------------------------------------------------
procedure Tframe_Map_Desktop_ObjectMaps.FormDestroy(Sender: TObject);
//--------------------------------------------------------------------
begin
  db_PostDataset(ADOStoredProc_Object_Maps);

  FDataset:=nil;


  g_Storage.StoreToRegistry(cxGrid1DBTableView1, className+'1');

//  dx_Objects.SaveToRegistry (FRegPath + dx_Objects.Name);

//  if not dmMain.DEBUG then begin
  //  dx_Maps.SaveToRegistry    (REGISTRY_FORMS + ClassName + '\'+ dx_Maps.Name);
 // end;

(*  dmMap_Desktop.OnAfterScroll:=  nil;
  dmMap_Desktop.OnBeforeScroll:= nil;
*)

  inherited;
end;


procedure Tframe_Map_Desktop_ObjectMaps.qry_ObjectMaps11AfterPost(DataSet: TDataSet);
var
  k: Integer;
//  oMiLabel: TmiLabel;
  sObjName: string;
begin
  sObjName := FDataset.FieldByName(FLD_MapObject_Name).AsString;

{
  oMiLabel:=g_MapDesktop_Labels.GetItemByName(sObjName);

  Assert(Assigned(oMiLabel));

  oMiLabel.Checked:=DataSet[FLD_CHECKED];
  oMiLabel.AUTOLABEL:=DataSet[FLD_AutoLabel];

}


//  dmOnega_DB_data.ExecStoredProc (sp_MapDesktop_Map_Update,
  k:=dmOnega_DB_data.ExecStoredProc_ (sp_Map_XREF_Update,
        [FLD_ID,      DataSet[FLD_ID] ,
         FLD_CHECKED, DataSet[FLD_CHECKED],

       //  db_Par (FLD_priority, DataSet[FLD_priority]),
       //  db_Par (FLD_ZOOM_MIN, DataSet[FLD_ZOOM_MIN]),
       //  db_Par (FLD_ZOOM_MAX, DataSet[FLD_ZOOM_MAX]),


         FLD_STYLE, DataSet[FLD_STYLE],

         FLD_AutoLabel, DataSet[FLD_AutoLabel],
      //   db_Par (FLD_ALLOW_ZOOM, DataSet[FLD_ALLOW_ZOOM]),

         FLD_label_style, DataSet[FLD_label_style]
        ]);
end;


//------------------------------------------------------------------
procedure Tframe_Map_Desktop_ObjectMaps.act_CheckExecute(Sender: TObject);
//------------------------------------------------------------------
var
  bChecked, bAllowZoom: boolean;
  sZoom: string;
  dZoom: double;
  i, iID: integer;

var
  sObjName: string;
  sLayerName, sFileName, sRegPath: string;

begin
  // ---------------------------------------------
  if Sender=act_Object_SetDefault then
  // ---------------------------------------------
  begin

    i:=dmOnega_DB_data.ExecStoredProc ('sp_MapDesktop_ObjectMap_set_default',
        [db_Par (FLD_ID,  FDataSet[FLD_ID] )     ]);

  end else

  // -------------------------------------------------------------------
  if (Sender = act_Check) or (Sender = act_UnCheck) then
  // -------------------------------------------------------------------
  begin
    bChecked:=(Sender = act_Check);

    cx_UpdateSelectedItems_Table(cxGrid1DBTableView1,
                              //   FLD_ID,
                                 FLD_CHECKED, bChecked);

//     dx_SetValuesForSelectedItems (dx_Objects,  FLD_CHECKED, bChecked);
//     IsChanged:= true;
     DoMapChecked();
  end else

  // -------------------------------------------------------------------
  if (Sender = act_Check_All) or (Sender = act_UnCheck_All) then
  // -------------------------------------------------------------------
  begin
    bChecked:=(Sender = act_Check_All);

    cxGrid1DBTableView1.DataController.SelectAll;

    cx_UpdateSelectedItems_Table(cxGrid1DBTableView1,
                            //     FLD_ID,
                                 FLD_CHECKED, bChecked);

(*    dx_Objects.SelectAll;
    dx_SetValuesForSelectedItems (dx_Objects, FLD_CHECKED,  bChecked);
    dx_Objects.ClearSelection;
*)
//    IsChanged:= true;

    DoMapChecked();

 end else
   raise Exception.Create('');

end;


procedure Tframe_Map_Desktop_ObjectMaps.Button1Click(Sender: TObject);
var
  oLabel: TmiLabel;
  sOBJNAME: string;
  sStyle: string;
begin
 { FDataset.First;

  db_View(FDataset);


  g_MapDesktop_Labels.Clear;


  while not FDataset.EOF do
  begin
    sOBJNAME:= FDataset.FieldByName(FLD_NAME).AsString;

    oLabel:=g_MapDesktop_Labels.AddItem(sOBJNAME);
    oLabel.Checked  :=FDataset.FieldByName(FLD_CHECKED).AsBoolean;
    oLabel.AUTOLABEL:=FDataset.FieldByName(FLD_AUTOLABEL).AsBoolean;

    sStyle:= FDataset.FieldByName(FLD_label_style).AsString;

    oLabel.LabelProperties.LoadFromText(sStyle);


    FDataset.Next;
  end;

  g_MapDesktop_Labels.SaveToIniFile('d:\111.ini');
}

end;


//------------------------------------------------------------------
procedure Tframe_Map_Desktop_ObjectMaps.DoMapChecked;
//------------------------------------------------------------------
begin
//  db_PostDataset(FDataSet);

  if Assigned(FOnMapChecked) then
    FOnMapChecked(Self);

end;



procedure Tframe_Map_Desktop_ObjectMaps.View(aID: Integer; aMap: TMap);
var
  k: Integer;
begin
  FID:=aID;

  FMap:=aMap;

  k:=dmOnega_DB_data.MapDesktop_Select_ObjectMaps (ADOStoredProc_Object_Maps, aID);

//  dmOnega_DB_data.sp_MapDesktop_Select_ObjectMaps (qry_ObjectMaps, aID);
end;


procedure Tframe_Map_Desktop_ObjectMaps.col_StylePropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
 // if cxGrid1DBTableView1.Controller.FocusedColumn=col__Style then
//   Dlg_Label_Style_new1;

end;



// ---------------------------------------------------------------
procedure Tframe_Map_Desktop_ObjectMaps.Dlg_Style;
// ---------------------------------------------------------------
var ///oMapItem: TMapItem;
  sStyle_XML: Widestring;
  b: Boolean;
  bAUTOLABEL: Boolean;
//  oMiLabel: TmiLabel;
  sTabFileName: string;

  sObjName: string;

const
  FLD_style = 'style';


begin
  sObjName := FDataset.FieldByName(FLD_MapObject_Name).AsString;



  sStyle_XML:= FDataset.FieldByName(FLD_style).AsString;

 // sStyle_XML:= '';

 // sTabFileName:= FDataset.FieldByName(FLD_FILENAME).AsString;

//  Assert(Assigned(oMapItem), 'Value not assigned');

 // Tdlg_Map_Label_Setup_ExecDlg_Obj('', oMapItem.Style);

  if Init_IMapEngineX.Dlg_Style( dmMain.GetConnectionString, sObjName, sStyle_XML ) = S_OK then

//  if Tdlg_Map_Label_Setup.ExecDlg_Obj_new(sStyle) then //aTabFileName,        //,  aMap
//  if MapX_Label_Setup_Dlg( sStyle) then
//  if Tdlg_Map_Label_Setup.ExecDlg_Obj_new(s, sTabFileName) then
  begin
    db_SetEditState(FDataset);
    FDataset[FLD_style] := sStyle_XML;
    FDataset.POst;


    Init_IMapEngineX.Apply_Theme(FMap.DefaultInterface, sObjName, sStyle_XML );

//    FMap

 //   db_UpdateRecord(FDataset, FLD_label_style, s);

//    bAUTOLABEL:= FDataset.FieldByName(FLD_Checked).AsBoolean;
  //  bAUTOLABEL:= FDataset.FieldByName(FLD_AUTOLABEL).AsBoolean;

   // UpdateData(bAUTOLABEL);

  end;
end;



procedure Tframe_Map_Desktop_ObjectMaps.col__CheckedPropertiesChange( Sender: TObject);
var
  bChecked: Boolean;
  sObjName: string;

  vLayer: CMapXLayer;

begin


 // db_PostDataset(adoStoredProc_Object_Maps);
  db_PostDataset(FDataset);


  bChecked:=(Sender as TcxCheckBox).Checked;

//  ShowMessage('col__CheckedPropertiesChange');
  sObjName := FDataset.FieldByName(FLD_MapObject_Name).AsString;

//  if not IsAutoUpdate then
//    Exit;





  if not bChecked then
  begin
    vLayer :=mapx_GetLayerByName(FMap, '~'+sObjName);
    if Assigned(vLayer) then
    try
      FMap.Layers.Remove(vLayer);
    except
    end;
//     mapx_rem


//    if Assigned(vLayer) then
//      vLayer.Visible :=False;

  end else
  begin
    dmMapEngine_store.Open_Object_Map(sObjName);
    dmMapEngine_store.RefreshObjectThemas_All;
  end

  
 (* if Assigned(vLayer) then
  begin
    vLayer.Visible :=bChecked
  end else
    dmMapEngine_store.Open_Object_Map(sObjName);

*)

//  DoMapChecked();
end;


procedure Tframe_Map_Desktop_ObjectMaps.co__aUTOLABELPropertiesChange(
  Sender: TObject);
var
  b: Boolean;
begin
  b:=(Sender as TcxCheckBox).Checked;
  UpdateData(b);
end;



// ---------------------------------------------------------------
procedure Tframe_Map_Desktop_ObjectMaps.UpdateData(aAutoLabel: Boolean);
// ---------------------------------------------------------------

var
  bChecked: Boolean;
  sObjName: string;
  vLayer: CMapXLayer;

  oLabelProperties: TmiLabelProperties;
  s: string;
  sLabelStyle: string;

 // oMiLabel: TmiLabel;

begin
 // db_PostDataset(adoStoredProc_Object_Maps);
  db_PostDataset(FDataset);

 // db_View(FDataset);
  sObjName    := FDataset.FieldByName(FLD_MapObject_Name).AsString;
  sLabelStyle := FDataset.FieldByName(FLD_LabelStyle).AsString;


  vLayer :=mapx_GetLayerByName(FMap, '~'+sObjName);

  if Assigned(vLayer) then
  begin

  //  bChecked:= FDataset.FieldByName(FLD_Checked).AsBoolean;

    vLayer.AutoLabel := aAutoLabel;

    if aAutoLabel and (sLabelStyle<>'') then
    begin
     // oMiLabel:=g_MapDesktop_Labels.GetItemByName(sObjName);

   //   Assert(Assigned(oMiLabel));

{
   for I := 0 to g_MapDesktop_Labels.Count - 1 do    // Iterate
   begin
     oItem:=g_MapDesktop_Labels[i];
}

    //  oMiLabel.LabelProperties.SaveToMapinfoLayer(vLayer);




      oLabelProperties:=TmiLabelProperties.Create;

      oLabelProperties.AsText := sLabelStyle;
      oLabelProperties.SaveToMapinfoLayer(vLayer);

      FreeAndNil(oLabelProperties);


    end;
  end;



(*    vLayer := MapAdd (rec)
  else
    MapUpdate (rec, rec.DisplayIndex, vLayer);
*)


(*  ShowMessage('co__aUTOLABELPropertiesChange');

  DoMapChecked();

*)

end;

// ---------------------------------------------------------------
procedure Tframe_Map_Desktop_ObjectMaps.col_AttrPropertiesButtonClick( Sender: TObject; AButtonIndex: Integer);
// ---------------------------------------------------------------
//var
//  sObjName: string;
//  v: IMapEngineX;

begin
  Dlg_Attributes();

{
  sObjName := FDataset.FieldByName(FLD_MapObject_Name).AsString;

  Init_IMapEngineX.Dlg_Attributes( dmMain.GetConnectionString, sObjName );
}

//  v:=Init_IMapEngineX;
//  v.Dlg_Attributes( dmMain.GetConnectionString, sObjName );

end;


// ---------------------------------------------------------------
procedure Tframe_Map_Desktop_ObjectMaps.Dlg_Attributes;
// ---------------------------------------------------------------
var
  bAUTOLABEL: Boolean;
  sObjName: string;
//  v: IMapEngineX;

begin
  sObjName := FDataset.FieldByName(FLD_MapObject_Name).AsString;

  if S_OK = Init_IMapEngineX.Dlg_Attributes( dmMain.GetConnectionString, sObjName ) then
  begin
    bAUTOLABEL:= FDataset.FieldByName(FLD_AUTOLABEL).AsBoolean;

    UpdateData(bAUTOLABEL);
  end;
end;


// ---------------------------------------------------------------
procedure Tframe_Map_Desktop_ObjectMaps.col_Style_PropertiesButtonClick( Sender: TObject; AButtonIndex: Integer);
// ---------------------------------------------------------------
begin
  Dlg_Style

end;



procedure Tframe_Map_Desktop_ObjectMaps.cxGrid1DBTableView1Column1PropertiesButtonClick(  Sender: TObject; AButtonIndex: Integer);
begin
  Dlg_Label_style_new;
end;


// ---------------------------------------------------------------
procedure Tframe_Map_Desktop_ObjectMaps.Dlg_Label_style_new;
// ---------------------------------------------------------------
var
  bAUTOLABEL: Boolean;
  s: Widestring;
begin
  s:= FDataset.FieldByName(FLD_label_style).AsString;

  if Init_IMapEngineX.Dlg_Label_style(s) = s_Ok then
  begin
    db_SetEditState(FDataset);
    FDataset[FLD_label_style] := s;

    bAUTOLABEL:= FDataset.FieldByName(FLD_AUTOLABEL).AsBoolean;

    UpdateData(bAUTOLABEL);

  end;
end;




end.




{



// ---------------------------------------------------------------
procedure Tframe_Map_Desktop_ObjectMaps.Dlg_Label_Style_new1;
// ---------------------------------------------------------------
var ///oMapItem: TMapItem;
  slabel_Style: string;
  b: Boolean;
  bAUTOLABEL: Boolean;
//  oMiLabel: TmiLabel;
  sTabFileName: string;

  sObjName: string;
begin
  sObjName := FDataset.FieldByName(FLD_MapObject_Name).AsString;




  slabel_Style:= FDataset.FieldByName(FLD_label_style).AsString;

 // sTabFileName:= FDataset.FieldByName(FLD_FILENAME).AsString;

//  Assert(Assigned(oMapItem), 'Value not assigned');

 // Tdlg_Map_Label_Setup_ExecDlg_Obj('', oMapItem.Style);

  if Tdlg_Map_Label_Setup.ExecDlg_Obj_new(slabel_Style) then //aTabFileName,        //,  aMap
//  if MapX_Label_Setup_Dlg( sStyle) then
//  if Tdlg_Map_Label_Setup.ExecDlg_Obj_new(s, sTabFileName) then
  begin
    db_SetEditState(FDataset);
    FDataset[FLD_label_style] := slabel_Style;

 //   db_UpdateRecord(FDataset, FLD_label_style, s);

//    bAUTOLABEL:= FDataset.FieldByName(FLD_Checked).AsBoolean;
    bAUTOLABEL:= FDataset.FieldByName(FLD_AUTOLABEL).AsBoolean;

    UpdateData(bAUTOLABEL);

  end;
end;



