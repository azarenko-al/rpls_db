object dmMap_Desktop: TdmMap_Desktop
  OldCreateOrder = True
  OnCreate = DataModuleCreate
  OnDestroy = DataModuleDestroy
  Left = 1449
  Top = 700
  Height = 306
  Width = 471
  object ds_Desktops: TDataSource
    DataSet = ADOStoredProc_MapDesktops
    Left = 60
    Top = 72
  end
  object ADOStoredProc_Maps: TADOStoredProc
    LockType = ltBatchOptimistic
    Parameters = <>
    Left = 248
    Top = 24
  end
  object ADOStoredProc_Object_Maps: TADOStoredProc
    LockType = ltBatchOptimistic
    Parameters = <>
    Left = 248
    Top = 104
  end
  object ADOStoredProc_CalcMaps: TADOStoredProc
    LockType = ltBatchOptimistic
    Parameters = <>
    Left = 248
    Top = 160
  end
  object ADOStoredProc_MapDesktops: TADOStoredProc
    LockType = ltBatchOptimistic
    AfterPost = ADOStoredProc_MapDesktopsAfterPost
    Parameters = <>
    Left = 64
    Top = 8
  end
end
