unit d_Map_Desktops;

interface

uses
windows,
  Classes, Controls, Forms, ActnList, StdCtrls, Menus, ComCtrls,  SySutils,
  Db, ADODB, cxSplitter,  Variants  , Dialogs, Messages, cxPropertiesStore,

  MapXLib_TLB,


  u_log,

  dm_Onega_DB_data,

  u_db,

  d_Wizard,

  dm_Main,
//  I_MapView,

  fr_Map_Desktop_Maps,
  fr_Map_Desktop_ObjectMaps,
  fr_CalcMaps,
//  fr_MapsOrder,

 // d_Map_Label_Setup,

  dm_Map_Desktop,

  u_func,

  u_func_msg,

  u_reg,

  u_const_db,
  u_const_msg,

  u_const_str,


  DBCtrls, ImgList, cxControls,   rxPlacemnt  , ExtCtrls, AppEvnts,
  dxBar, cxClasses, cxBarEditItem, cxLookAndFeels;


type
  Tdlg_Map_Desktops = class(Tdlg_Wizard, IMessageHandler)
    act_Add: TAction;
    act_Bottom: TAction;
    act_Del: TAction;
    act_Down: TAction;
    act_Refresh: TAction;
    act_Rename: TAction;
    act_SetDefault: TAction;
    act_Top: TAction;
    act_Up: TAction;
    ActionList2: TActionList;
    N1: TMenuItem;
    ImageList2: TImageList;
    menu_MapDesktop: TPopupMenu;
    N2: TMenuItem;
    N3: TMenuItem;
    N4: TMenuItem;
    act_Create_Default: TAction;
    act_ReloadFromDir: TAction;
    dxBarManager1: TdxBarManager;
    PageControl1: TPageControl;
    TabSheet_GeoMaps: TTabSheet;
    TabSheet_ObjectMaps: TTabSheet;
    TabSheet_CalcMaps: TTabSheet;
    dxBarManager1Bar1: TdxBar;
    dxBarButton1: TdxBarButton;
    cxBarEditItem1: TcxBarEditItem;
    cxBarEditItem2: TcxBarEditItem;
    dxBarButton2: TdxBarButton;
    dxBarButton3: TdxBarButton;
    dxBarButton4: TdxBarButton;
    dxBarButton5: TdxBarButton;
    StatusBar1: TStatusBar;
    procedure FormCreate(Sender: TObject);
    procedure _Actions(Sender: TObject);
    procedure ActionList1Update(Action: TBasicAction;  var Handled: Boolean);
    procedure act_UpUpdate(Sender: TObject);
//    procedure ApplicationEvents1Message(var Msg: tagMSG; var Handled: Boolean);
    procedure FormDestroy(Sender: TObject);
//    procedure cb_AutoRefreshCalcsClick(Sender: TObject);
//    procedure DBLookupComboBox1CloseUp(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure PageControl1Change(Sender: TObject);

  private
//    FIsAllowScroll: Boolean;

 //   FMapDesktop_ID : Integer;

    FMap: TMap;

 //   FIsDebug : Boolean;


    FDataSet : TDataSet;

    Ffrm_CalcMaps:  Tframe_CalcMaps;
//    Ffrm_MapsOrder: Tframe_MapsOrder;

    Fframe_Map_Desktop_Maps: Tframe_Map_Desktop_Maps;
    Fframe_Map_Desktop_ObjectMaps: Tframe_Map_Desktop_ObjectMaps;

//    procedure DoOnAfterScroll(DataSet: TDataSet);
  //  procedure DoOnBeforeScroll(DataSet: TDataSet);
    procedure DoOnMapChecked(Sender: TObject);
//    procedure SetMapDesktop(aID: integer);

    procedure GetMessage(aMsg: TEventType; aParams: TEventParamList ; var aHandled:
        boolean);


  public
//    IMapView: IMapViewX;

    procedure View(aMap: TMap);

    class procedure CreateSingleForm(aMap: TMap);
    //:
     //   Tdlg_Map_Desktops;
  end;



//==================================================================
implementation{$R *.dfm}
//==================================================================

//--------------------------------------------------------------------
class procedure Tdlg_Map_Desktops.CreateSingleForm(aMap: TMap);
//--------------------------------------------------------------------
var
  oForm: Tdlg_Map_Desktops;
  s: string;
begin
 // Exit;


  s:=Tdlg_Map_Desktops.ClassName;

  oForm:= Tdlg_Map_Desktops (FindForm (Tdlg_Map_Desktops.ClassName));

  if not Assigned(oForm) then
    Application.CreateForm(Tdlg_Map_Desktops, oForm);

//    oForm:=Tdlg_Map_Desktops.Create(Application);

(*  if not Assigned(oForm) then
    oForm:=Tdlg_Map_Desktops.Create(Application);
*)

 // Assert(dmMap_Desktop.qry_Desktops.Active, 'dmMap_Desktop.qry_Desktops.Active');


//  if not dmMap_Desktop.qry_Desktops.Locate (FLD_ID, aMapDesktopID, []) then
  //  dmMap_Desktop.qry_Desktops.First;

  oForm.View ( aMap);

//  (oForm).View (aMapDesktopID);

  oForm.Show;

 // Result := oForm;
end;

//--------------------------------------------------------------------
procedure Tdlg_Map_Desktops.FormCreate(Sender: TObject);
//--------------------------------------------------------------------
begin
  inherited;

 /////////// FIsDebug :=True;
                              
  Caption:= '��������� ����';

  PageControl1.Align:=alClient;

  TdmMap_Desktop.Init;

//  dx_Desktops.DataSource:= dmMap_Desktop.ds_Desktops;

{
  DBLookupComboBox1.ListSource:= dmMap_Desktop.ds_Desktops;
  DBLookupComboBox1.ListField := FLD_NAME;
  DBLookupComboBox1.KeyField  := FLD_ID;
 }

  act_Add.Caption       := STR_ACT_ADD;
  act_Del.Caption       := STR_ACT_DEL;
  act_Rename.Caption    := STR_RENAME;

 // col_DeskTop_Name.Caption:= STR_NAME;


  CreateChildForm(Tframe_Map_Desktop_Maps,  Fframe_Map_Desktop_Maps, TabSheet_GeoMaps);
  Fframe_Map_Desktop_Maps.OnMapChecked:=DoOnMapChecked;


  CreateChildForm(Tframe_Map_Desktop_ObjectMaps,  Fframe_Map_Desktop_ObjectMaps, TabSheet_ObjectMaps);
  Fframe_Map_Desktop_ObjectMaps.OnMapChecked:=DoOnMapChecked;

  CreateChildForm(Tframe_CalcMaps,  Ffrm_CalcMaps, TabSheet_CalcMaps);
  Ffrm_CalcMaps.OnMapChecked:= DoOnMapChecked;


  dmMap_Desktop.OnMapChecked:= DoOnMapChecked;


//  CreateChildForm(Tframe_MapsOrder,  Ffrm_MapsOrder, TabSheet_Order);
 // Ffrm_MapsOrder.OnMapChecked:=DoOnMapChecked;

  FDataSet := dmMap_Desktop.ADOStoredProc_MapDesktops;


  if not FDataSet.Active then
    dmMap_Desktop.ChangeProject(dmMain.ProjectID);


  Assert(FDataSet.Active);

//  FDataSet.AfterScroll:=  DoOnAfterScroll;


 // AddComponentProp(pn_Top, PROP_HEIGHT);
//  AddComponentProp(dx_Desktops, 'Height');
 // AddComponentProp(cb_AutoRefreshCalcs, PROP_CHECKED);

  AddComponentProp(PageControl1, 'ActivePageIndex');

  RestoreFrom();

 // cb_AutoRefreshCalcsClick(nil);


//  TBItem6.ImageIndex:= -1;

  act_ReloadFromDir.Visible := False;


  PageControl1Change(nil);

end;

//--------------------------------------------------------------------
procedure Tdlg_Map_Desktops.FormDestroy(Sender: TObject);
//--------------------------------------------------------------------
begin
 ///// dmMap_Desktop.ADOStoredProc_MapDesktops.AfterScroll:=  nil;

  FDataSet := nil;
//  DBLookupComboBox1.ListSource := nil;
  dmMap_Desktop.OnMapChecked   := nil;

  FreeAndNil(Fframe_Map_Desktop_Maps);
  FreeAndNil(Fframe_Map_Desktop_ObjectMaps);
  FreeAndNil(Ffrm_CalcMaps);
                                 
//  dmMap_Desktop.qry_Desktops.BeforeScroll:= nil;

  inherited;


 // ShowMessage('procedure Tdlg_Map_Desktops.FormDestroy(Sender: TObject);');
end;


//-------------------------------------------------------------------
procedure Tdlg_Map_Desktops._Actions(Sender: TObject);
//-------------------------------------------------------------------
{var
  sName: string;
  bDirectionIsUp: boolean;
}

//  bIsRebuildAll: boolean;
 // iID: integer;
begin
  if Sender=act_Up     then Fframe_Map_Desktop_Maps.act_Up.Execute;
  if Sender=act_Top    then Fframe_Map_Desktop_Maps.act_Top.Execute;
  if Sender=act_Bottom then Fframe_Map_Desktop_Maps.act_Bottom.Execute;
  if Sender=act_Down   then Fframe_Map_Desktop_Maps.act_Down.Execute;


  //-----------------------------------------------
  if Sender=act_Refresh then
  //-----------------------------------------------
  begin
    db_PostDataset(FDataSet);

 //   iID := FDataSet.FieldByName(FLD_ID).AsInteger;

    dmMap_Desktop.MapDesktopItem_Init2();//iID);
   // dmMap_Desktop.Save;

    g_EventManager.postEvent_ (et_MAP_REFRESH_FROM_DESKTOP, []);

    {
    PostEvent (WE_MAP_REFRESH_FROM_DESKTOP,
                [
                 //app_Par(PAR_ID,  iID)
                 ]);
    }

  end else

  {
  //-----------------------------------------------
  if Sender=act_Add then
  //-----------------------------------------------
  begin
    if dmMap_Desktop.Add_Dlg then
       DoOnAfterScroll(FDataSet);

  end else

  //-----------------------------------------------
  if Sender=act_Del then
  begin
/////    Assert(dmMap_Desktop.qry_Desktops.Active);

    dmMap_Desktop.DeleteDlg;

  ////////  if ConfirmDlg (STR_ASK_DEL) then
  ////    dmMap_Desktop.qry_Desktops.Delete;
  end else
  }

(*  //-----------------------------------------------
  if Sender=act_SetDefault  then
  begin
   // dmMap_Desktop.SaveAsTemplate;
  end else

  //-----------------------------------------------
  if Sender=act_Create_Default then
  begin
   // dmMap_Desktop.CreateDefaultDesktop_;
  //  dmMap_Desktop.OpenDesktops;
  end else
 

  //-----------------------------------------------
  if Sender=act_Rename then
  begin
    dmMap_Desktop.RenameDlg;

  end
       *)
end;

//--------------------------------------------------------------------
procedure Tdlg_Map_Desktops.ActionList1Update(Action: TBasicAction; var Handled: Boolean);
//--------------------------------------------------------------------
var
  bMoveEnabled: boolean;
begin

  //(not db_IsEditState (dmMap_Desktop.mem_Desktops)) and

{ if  dmMap_Desktop.mem_Desktops.IsEmpty then
  begin
    act_Ok.Enabled    :=(not db_IsEditState (dmMap_Desktop.mem_Desktops)) and
                        (not db_IsEditState (dmMap_Desktop.mem_Maps)) and
                        (not db_IsEditState (dmMap_Desktop.mem_MapObjects));
    GSPages1.Enabled := False;
  end  else
    GSPages1.Enabled := True;  }


  Assert(dmMap_Desktop.ADOStoredProc_MapDesktops.Active);

  with dmMap_Desktop.ADOStoredProc_MapDesktops do
  begin
    act_Ok.Enabled      := RecordCount > 0;
    act_Refresh.Enabled := RecordCount > 0;
    act_Del.Enabled     := RecordCount > 1;
    act_Rename.Enabled  := RecordCount > 1;
  end;

end;

procedure Tdlg_Map_Desktops.act_UpUpdate(Sender: TObject);
var
  b: Boolean;
begin
  b:=PageControl1.ActivePage = TabSheet_GeoMaps;

  act_Up.Enabled    :=b and Fframe_Map_Desktop_Maps.act_Up.Enabled;
  act_Top.Enabled   :=b and Fframe_Map_Desktop_Maps.act_Top.Enabled;
  act_Bottom.Enabled:=b and Fframe_Map_Desktop_Maps.act_Bottom.Enabled;
  act_Down.Enabled  :=b and Fframe_Map_Desktop_Maps.act_Down.Enabled;

end;


//------------------------------------------------------------------
procedure Tdlg_Map_Desktops.DoOnMapChecked(Sender: TObject);
//------------------------------------------------------------------
begin
//  if cb_AutoRefreshCalcs.Checked then
   act_Refresh.Execute;
   
end;

procedure Tdlg_Map_Desktops.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
   Action :=  caFree;
end;


procedure Tdlg_Map_Desktops.PageControl1Change(Sender: TObject);
begin
//  if FIsDebug then
 //   Exit;


   case PageControl1.ActivePageIndex of
      0: begin
           {
           btn_Top.Action   :=Fframe_Map_Desktop_Maps.act_Top;
           btn_Down.Action  :=Fframe_Map_Desktop_Maps.act_Down;
           btn_Up.Action    :=Fframe_Map_Desktop_Maps.act_Up;
           btn_Bottom.Action:=Fframe_Map_Desktop_Maps.act_Bottom;
           }
         end;

(*       1: begin
           btn_Top.Action   :=Fframe_Map_Desktop_ObjectMaps.act_Top;
           btn_Down.Action  :=Fframe_Map_Desktop_ObjectMaps.act_Down;
           btn_Up.Action    :=Fframe_Map_Desktop_ObjectMaps.act_Up;
           btn_Bottom.Action:=Fframe_Map_Desktop_ObjectMaps.act_Bottom;
         end;

       3: begin
           btn_Top.Action   :=act_Top;
           btn_Down.Action  :=act_Down;
           btn_Up.Action    :=act_Up;
           btn_Bottom.Action:=act_Bottom;

         end;
*)
      else
         begin
         {
           btn_Top.Action   :=nil;
           btn_Down.Action  :=nil;
           btn_Up.Action    :=nil;
           btn_Bottom.Action:=nil;
          }
          
         end;
  end;

  {
   btn_Top.Enabled   :=btn_Top.Action<>nil;
   btn_Down.Enabled  :=btn_Down.Action<>nil;
   btn_Up.Enabled    :=btn_Up.Action<>nil;
   btn_Bottom.Enabled:=btn_Bottom.Action<>nil;
   }

end;

// ---------------------------------------------------------------
procedure Tdlg_Map_Desktops.View(aMap: TMap);
// ---------------------------------------------------------------
var
  iID: Integer;
begin
 // if FIsDebug then
  //  Exit;

  if Assigned(aMap) then
    FMap := aMap;

//  FDataSet.Locate (FLD_ID, aMapDesktopID, []) ;

//  DBLookupComboBox1.KeyValue := FDataSet[FLD_ID];

    iID:= FDataSet[FLD_ID];


//g_log.Msg ('Tdlg_Map_Desktops.SetMapDesktop(aID: Integer): '+ IntToStr(aID));

//  if Assigned(Fframe_Map_Desktop_Maps) then
    Fframe_Map_Desktop_Maps.View (iID, FMap);

//  if Assigned(Fframe_Map_Desktop_ObjectMaps) then
    Fframe_Map_Desktop_ObjectMaps.View (iID, FMap);

  //if Assigned(Ffrm_CalcMaps) then
    Ffrm_CalcMaps.View (FMap, iID);



//  FIsAllowScroll := True;

//  DoOnAfterScroll(FDataSet);
end;



procedure Tdlg_Map_Desktops.GetMessage(aMsg: TEventType; aParams:
    TEventParamList ; var aHandled: boolean);
begin
  case aMsg of
    et_PROJECT_CHANGED: begin
                          dmMap_Desktop.ChangeProject(dmMain.ProjectID);
                          View (nil);
                        end;


    et_MAP_CLOSED,
    et_PROJECT_CLEAR:  Close;
  end;

//      dmMap_Desktop.ChangeProject(dmMain.ProjectID);

end;



end.



{

//------------------------------------------------------------------
procedure Tdlg_Map_Desktops.SetMapDesktop(aID: integer);
//------------------------------------------------------------------
begin
//  if FIsDebug then
 //   Exit;

//    aID: integer);




//  if (aID=0) or (aID=FMapDesktop_ID) then  //��� ���������� ������ �����������
 //   Exit;

//  FMapDesktop_ID := aID;

  g_log.Msg ('Tdlg_Map_Desktops.SetMapDesktop(aID: Integer): '+ IntToStr(aID));

//  if Assigned(Fframe_Map_Desktop_Maps) then
    Fframe_Map_Desktop_Maps.View (aID, FMap);

//  if Assigned(Fframe_Map_Desktop_ObjectMaps) then
    Fframe_Map_Desktop_ObjectMaps.View (aID, FMap);

  //if Assigned(Ffrm_CalcMaps) then
    Ffrm_CalcMaps.View (FMap, aID);

end;


