unit d_Transparent;

interface

uses
  SysUtils, Classes, Controls, Forms,
  StdCtrls, ComCtrls;

type
  Tdlg_Transparent = class(TForm)
    TrackBar1: TTrackBar;
    Button1: TButton;
    Button2: TButton;
    Label1: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure TrackBar1Change(Sender: TObject);
  private
    procedure SetValue(aValue : integer);
    { Private declarations }
  public
    class function ExecDlg(var aTransparency: Integer): Boolean;
  end;


implementation

{$R *.dfm}



//--------------------------------------------------------------------
class function Tdlg_Transparent.ExecDlg(var aTransparency: Integer): Boolean;
//--------------------------------------------------------------------
begin
  with Tdlg_Transparent.Create(Application.MainForm) do
  try
    TrackBar1.Position:= aTransparency;

    SetValue (TrackBar1.Position);


    Result:= ShowModal=mrOk ;

    if Result then
      aTransparency:= TrackBar1.Position;

  finally
    Free;
  end;
end;


procedure Tdlg_Transparent.FormCreate(Sender: TObject);
begin
  Caption:='������������';

  TrackBar1.Max:=100;

end;

procedure Tdlg_Transparent.TrackBar1Change(Sender: TObject);
begin
  SetValue (TrackBar1.Position);
end;


procedure Tdlg_Transparent.SetValue(aValue : integer);
begin
  Label1.Caption:= Format('��������: %d', [aValue]) + ' %';

end;


end.
