object frm_test_Map: Tfrm_test_Map
  Left = 411
  Top = 176
  Width = 427
  Height = 470
  Caption = 'frm_test_Map'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object pc_Main: TPageControl
    Left = 0
    Top = 49
    Width = 220
    Height = 394
    ActivePage = ts_Region
    Align = alLeft
    TabOrder = 0
    object ts_Region: TTabSheet
      Caption = 'ts_Region'
      ImageIndex = 1
    end
  end
  object ToolBar1: TToolBar
    Left = 0
    Top = 0
    Width = 419
    Height = 49
    Caption = 'ToolBar1'
    TabOrder = 1
  end
  object Button1: TButton
    Left = 260
    Top = 100
    Width = 129
    Height = 45
    Caption = 'CalcRegion'
    TabOrder = 2
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 260
    Top = 172
    Width = 129
    Height = 45
    Caption = 'MapDesktop'
    TabOrder = 3
    OnClick = Button2Click
  end
  object btn_MapReb: TButton
    Left = 256
    Top = 260
    Width = 117
    Height = 69
    Caption = 'btn_MapReb'
    TabOrder = 4
    OnClick = btn_MapRebClick
  end
  object Button3: TButton
    Left = 260
    Top = 352
    Width = 113
    Height = 61
    Caption = 'Button3'
    TabOrder = 5
    OnClick = Button3Click
  end
  object Button4: TButton
    Left = 268
    Top = 64
    Width = 75
    Height = 25
    Caption = 'Button4'
    TabOrder = 6
    OnClick = Button4Click
  end
  object FormPlacement1: TFormPlacement
    IniFileName = 'Software\Onega\'
    Left = 156
    Top = 4
  end
end
