program test_MapDeskTop;



uses
  a_Unit in 'a_Unit.pas' {frm_test_Map},
  d_Map_Desktops in '..\d_Map_Desktops.pas' {dlg_Map_Desktops},
  dm_CalcMap_View in '..\dm_CalcMap_View.pas' {dmCalcMap_View111: TDataModule},
  dm_Map_Desktop in '..\dm_Map_Desktop.pas' {dmMap_Desktop: TDataModule},
  f_Map in '..\..\Map\f_Map.pas' {frm_Map},
  Forms,
  fr_CalcMaps in '..\fr_CalcMaps.pas' {frame_Map_Desktop_CalcMaps},
  fr_Map_Desktop_Maps in '..\fr_Map_Desktop_Maps.pas' {frame_Map_Desktop_Maps},
  fr_Map_Desktop_ObjectMaps in '..\fr_Map_Desktop_ObjectMaps.pas' {frame_Map_Desktop_ObjectMaps},
  u_const_msg in '..\..\u_const_msg.pas',
  u_MapDesktop_classes in '..\u_MapDesktop_classes.pas';

{$R *.res}


begin
  Application.Initialize;
  Application.CreateForm(Tfrm_test_Map, frm_test_Map);
  Application.Run;
end.
