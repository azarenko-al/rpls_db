object Form6: TForm6
  Left = 606
  Top = 193
  Width = 546
  Height = 526
  Caption = 'Form6'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object cxVerticalGrid1: TcxVerticalGrid
    Left = 0
    Top = 0
    Width = 538
    Height = 241
    BorderStyle = cxcbsNone
    Align = alTop
    LookAndFeel.Kind = lfOffice11
    OptionsView.CellTextMaxLineCount = 3
    OptionsView.AutoScaleBands = False
    OptionsView.PaintStyle = psDelphi
    OptionsView.GridLineColor = clBtnShadow
    OptionsView.RowHeaderMinWidth = 30
    OptionsView.RowHeaderWidth = 205
    OptionsView.ValueWidth = 40
    TabOrder = 0
    object cxVerticalGrid1CategoryRow5: TcxCategoryRow
    end
    object cxVerticalGrid1CategoryRow6: TcxCategoryRow
      Properties.Caption = #1050#1072#1088#1090#1072
      object row_Map_type: TcxEditorRow
        Properties.Caption = #1058#1080#1087' '#1082#1072#1088#1090#1099
        Properties.ImageIndex = 1
        Properties.EditPropertiesClassName = 'TcxRadioGroupProperties'
        Properties.EditProperties.Columns = 4
        Properties.EditProperties.DefaultValue = 0
        Properties.EditProperties.Items = <
          item
            Caption = #1056#1072#1089#1090#1088
            Value = '0'
          end
          item
            Caption = #1042#1077#1082#1090#1086#1088
            Value = '1'
          end
          item
            Caption = 'KML'
            Value = '2'
          end
          item
            Caption = 'KMZ'
            Value = '3'
          end>
        Properties.DataBinding.ValueType = 'Integer'
        Properties.Value = 0
      end
      object row_Google_FileName: TcxEditorRow
        Properties.Caption = 'Google_FileName'
        Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
        Properties.EditProperties.Buttons = <
          item
            Default = True
            Kind = bkEllipsis
          end>
        Properties.DataBinding.ValueType = 'String'
        Properties.Value = 's:\_g1111.kmz'
      end
      object row_MapRasterTransparence: TcxEditorRow
        Properties.Caption = 'MapRasterTransparence 0..255'
        Properties.EditPropertiesClassName = 'TcxSpinEditProperties'
        Properties.DataBinding.ValueType = 'String'
        Properties.Value = '100'
      end
    end
  end
  object pn_Buttons: TPanel
    Left = 0
    Top = 457
    Width = 538
    Height = 35
    Align = alBottom
    BevelOuter = bvNone
    Constraints.MaxHeight = 35
    Constraints.MinHeight = 35
    TabOrder = 1
    object Bevel1: TBevel
      Left = 0
      Top = 0
      Width = 538
      Height = 2
      Align = alTop
      Shape = bsTopLine
    end
    object Panel3: TPanel
      Left = 364
      Top = 2
      Width = 174
      Height = 33
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 0
      object btn_Ok: TButton
        Left = 3
        Top = 5
        Width = 75
        Height = 23
        Action = act_Ok
        Default = True
        ModalResult = 1
        TabOrder = 0
      end
      object btn_Cancel: TButton
        Left = 87
        Top = 5
        Width = 75
        Height = 23
        Action = act_Cancel
        Cancel = True
        ModalResult = 2
        TabOrder = 1
      end
    end
  end
  object ActionList2: TActionList
    Left = 32
    Top = 284
    object FileSaveAs1: TFileSaveAs
      Category = 'File'
      Caption = 'Save &As...'
      Hint = 'Save As|Saves the active file with a new name'
      ImageIndex = 30
    end
  end
  object ActionList1: TActionList
    Left = 32
    Top = 356
    object act_Ok: TAction
      Category = 'Main'
      Caption = 'Ok'
    end
    object act_Cancel: TAction
      Category = 'Main'
      Caption = #1047#1072#1082#1088#1099#1090#1100
    end
    object act_Calc_EMP: TAction
      Caption = #1056#1072#1089#1095#1077#1090' '#1069#1052#1055
    end
    object act_Join: TAction
      Caption = #1054#1073#1098#1077#1076#1080#1085#1077#1085#1080#1077' '#1088#1072#1089#1095#1077#1090#1086#1074
    end
    object act_MakeMaps: TAction
      Caption = #1057#1092#1086#1088#1084#1080#1088#1086#1074#1072#1090#1100' '#1082#1072#1088#1090#1099
    end
  end
end
