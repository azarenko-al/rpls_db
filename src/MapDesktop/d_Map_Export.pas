unit d_Map_Export;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, ActnList, StdActns, cxVGrid, cxControls,
  ImgList, cxDropDownEdit, cxImageComboBox, cxContainer,
  cxInplaceContainer, cxEdit, cxTextEdit,

  u_func,
  u_files,

 // u_common_const,
  u_const_filters,

  u_Storage,

  //u_files,

  u_const,

  u_cx_VGrid,
  u_cx_VGrid_export,

  u_dlg,

  u_vars,

  u_run,

  u_Params_Bin_to_Map,
  u_Params_Bin_To_Map_classes,


  cxMaskEdit, cxSpinEdit, rxPlacemnt

  ;

type
  Tdlg_Map_Export = class(TForm)
    ActionList2: TActionList;
    FileSaveAs1: TFileSaveAs;
    pn_Buttons: TPanel;
    Bevel1: TBevel;
    Panel3: TPanel;
    btn_Ok: TButton;
    btn_Cancel: TButton;
    gb_Options_Site: TGroupBox;
    lb_TranspPercent: TLabel;
    lb_VectorTranspStyle: TLabel;
    ed_TransparentPercent: TcxSpinEdit;
    Panel10: TPanel;
    img_VectorStyle: TImage;
    cxImageComboBox1: TcxImageComboBox;
    img_MIF_RegionStyle: TImageList;
    Panel1: TPanel;
    cxVerticalGrid1: TcxVerticalGrid;
    row_FileName: TcxEditorRow;
    cxVerticalGrid1CategoryRow5: TcxCategoryRow;
    row_Map_type: TcxEditorRow;
    row_Transparence: TcxEditorRow;
    row_Misc: TcxCategoryRow;
    row_Matrix_FileName: TcxEditorRow;
    row_ColorXmlFileName: TcxEditorRow;
    FormStorage1: TFormStorage;
    procedure FormDestroy(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure row_FileNameEditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure row_Map_typeEditPropertiesChange(Sender: TObject);
  private
    FMatrix_FileName: string;
    FColorXmlFileName: string;

    FRegPath : string;

    procedure Run;

  public
    class procedure ExecDlg(aMatrix_FileName, aColorXmlFileName: string);

  end;



implementation   
{$R *.dfm}


//--------------------------------------------------------------------
class procedure Tdlg_Map_Export.ExecDlg(aMatrix_FileName, aColorXmlFileName:
    string);
//--------------------------------------------------------------------
begin
  with Tdlg_Map_Export.Create(Application.MainForm) do
  try
    FMatrix_FileName :=aMatrix_FileName;
    FColorXmlFileName:=aColorXmlFileName;


    row_Matrix_FileName.Properties.Value :=aMatrix_FileName;
    row_ColorXmlFileName.Properties.Value:=aColorXmlFileName;
                                         

    if ShowModal=mrOk then
      Run;
  finally
    Free;
  end;
end;


// ---------------------------------------------------------------
procedure Tdlg_Map_Export.FormCreate(Sender: TObject);
// ---------------------------------------------------------------
begin
  Caption := '������� �����';

  cx_InitVerticalGrid (cxVerticalGrid1);

  row_Misc.Collapse(True);

//  FileSaveAs1.Dialog.Filter:=FILTER_TAB_KML_KMZ;


  row_Matrix_FileName.Properties.Caption := '��������� �������';
  row_ColorXmlFileName.Properties.Caption := '�������� ���������';


  dlg_SetDefaultSize (Self);


  FRegPath := g_Storage.GetPathByClass(ClassName);

  cx_VerticalGrid_LoadFromReg(cxVerticalGrid1, FRegPath);


  row_Misc.Visible := False;

 // SetDefaultWidth;
end;


procedure Tdlg_Map_Export.FormDestroy(Sender: TObject);
begin
  cx_VerticalGrid_SaveToReg(cxVerticalGrid1, FRegPath);

end;


// ---------------------------------------------------------------
procedure Tdlg_Map_Export.Run;
// ---------------------------------------------------------------
const
  TEMP_FILENAME = 'bin_to_mif.xml';

var
  b: Boolean;
  iVal: Integer;
  oMatrixFile: u_Params_Bin_To_Map_classes.TBinMatrixFile;
  oParams: u_Params_Bin_To_Map.TParams_Bin_to_Map1;
  sXmlFile: string;

begin
  sXmlFile := g_ApplicationDataDir + TEMP_FILENAME;

  // -------------------------


  oParams:=TParams_Bin_to_Map1.Create;


  iVal:= AsInteger (row_Transparence.Properties.Value);


//  oParams.Transparence_0_255 := Round((255 / 100) * (255-iVal));
  oParams.Transparence := iVal;



  case row_Map_type.Properties.Value of
    0 : oParams.MapExportType :=mtRaster;
    1 : oParams.MapExportType :=mtVector;
    2 : oParams.MapExportType :=mtKML;
    3 : oParams.MapExportType :=mtKMZ;
    4 : oParams.MapExportType :=mtMif;
    5 : oParams.MapExportType :=mtGeoTiff;

  ///  5 : oParams.MapExportType :=mtJSON;
  else
    raise Exception.Create('');
  end;


//  b:=dmColorSchema_export.ExportItemToXMLNode_new(Params.ColorSchema_LOS_ID, oParams.ColorSchemaList);


  oMatrixFile:=oParams.BinMatrixArrayList.AddItem;
  
  oMatrixFile.MatrixFileName :=FMatrix_FileName;
  oMatrixFile.MapFileName    :=row_FileName.Properties.Value;

  oMatrixFile.ColorSchema_FileName :=FColorXmlFileName;
 // oMatrixFile.ColorSchema_ID := 1;

  oParams.SaveToXMLFile(sXmlFile);

  FreeAndNil(oParams);

  // -------------------------

  RunApp (EXE_RPLS_BIN_TO_MIF, DoubleQuotedStr(sXmlFile));
                 

 // SetDefaultWidth;
end;


// ---------------------------------------------------------------
procedure Tdlg_Map_Export.row_FileNameEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
// ---------------------------------------------------------------
var
  iFilterIndex: Integer;
  iOld: Integer;
begin

  FileSaveAs1.Dialog.Filter:=FILTER_TAB_KML_KMZ;


  FileSaveAs1.Dialog.FileName := AsString( row_FileName.Properties.Value);

 // FileSaveAs1.Dialog.DefaultExt := FILTERS[AsInteger( row_Map_type.Properties.Value)];



//  iFilterIndex

  case row_Map_type.Properties.Value of
    0,
    1: iFilterIndex:=1;

    2: iFilterIndex:=2;
    3: iFilterIndex:=3;
  end;

  iOld:=FileSaveAs1.Dialog.FilterIndex;

  FileSaveAs1.Dialog.FilterIndex := iFilterIndex;


  if FileSaveAs1.Execute then
    cx_SetButtonEditText (Sender, FileSaveAs1.Dialog.FileName);

//    row_FileName.Properties.Value:=FileSaveAs1.Dialog.FileName;

end;

// ---------------------------------------------------------------
procedure Tdlg_Map_Export.row_Map_typeEditPropertiesChange(
  Sender: TObject);
// ---------------------------------------------------------------
var
  s: string;
 // iVal: Integer;
  sFileName: string;
begin
  sFileName:= AsString( row_FileName.Properties.Value);

//  iVal:=row_Map_type.Properties.Value;

  case row_Map_type.Properties.Value of
    0,
    1: s:= ChangeFileExt(sFileName, '.tab');

    2: s:= ChangeFileExt(sFileName, '.kml');
    3: s:= ChangeFileExt(sFileName, '.kmz');

    4: s:= ChangeFileExt(sFileName, '.mif');
    5: s:= ChangeFileExt(sFileName, '.tif');
  end;                              

  row_FileName.Properties.Value:= s;

end;

end.




