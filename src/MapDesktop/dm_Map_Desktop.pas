unit dm_Map_Desktop;

interface
{$I ver.inc}

uses
  SysUtils, Classes, DB, ADODB, Forms, Variants, dxmdaset, Dialogs,

  dm_Onega_DB_data,

  u_MapDesktop_classes,

  dm_Main,

  u_const_db,

  u_const_str,

  u_func,
  u_db,
  u_log
  ;


type

  TdmMap_Desktop = class(TDataModule)
    ds_Desktops: TDataSource;
    ADOStoredProc_Maps: TADOStoredProc;
    ADOStoredProc_Object_Maps: TADOStoredProc;
    ADOStoredProc_CalcMaps: TADOStoredProc;
    ADOStoredProc_MapDesktops: TADOStoredProc;
    procedure ADOStoredProc_MapDesktopsAfterPost(DataSet: TDataSet);
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
//    procedure ds_DesktopsDataChange(Sender: TObject; Field: TField);
  private
    FDataset : TDataset;
    FOnMapChecked: TNotifyEvent;

//    FMapDesktop_ID : Integer;

    procedure DoMapChecked;
//    function GetDefaultDesktopID1111111(var aName: string): integer;
 //   function GetNameByID_new111111(var aID: integer; var aName: string): Boolean;
    procedure Log(aMsg: string);

  protected
  public
    MapDesktopItem: TMapDesktopItem;

    MapDesktop_ID : Integer;


    procedure Prepare_CalcMaps;
    procedure Prepare_ObjectMaps;

    procedure ChangeProject(aProjectID: integer);

 //   function Add(aName: string): boolean;
 //   function Add_Dlg: boolean;

  //  procedure DeleteDlg;

    procedure MapDesktopItem_Init2;

    procedure CloseDB;

    procedure Prepare_GeoMaps;

    property OnMapChecked: TNotifyEvent read FOnMapChecked write FOnMapChecked;

 //   procedure RenameDlg;

    class procedure Init;

    procedure Open;

  end;


var
  dmMap_Desktop: TdmMap_Desktop;


//==================================================================
implementation  {$R *.dfm}
//==================================================================

const
  MAP_DEFAULT_DESKTOP_NAME = 'default';


// ---------------------------------------------------------------
class procedure TdmMap_Desktop.Init;
// ---------------------------------------------------------------
begin
  if not Assigned(dmMap_Desktop) then
    dmMap_Desktop := TdmMap_Desktop.Create(Application);
end;


//--------------------------------------------------------------------
procedure TdmMap_Desktop.DataModuleCreate(Sender: TObject);
//--------------------------------------------------------------------
begin
  inherited;

  db_SetComponentADOConnection(Self, dmMain.AdoConnection);

  MapDesktopItem := TMapDesktopItem.Create(nil);

  FDataset := ADOStoredProc_MapDesktops;
end;


procedure TdmMap_Desktop.DataModuleDestroy(Sender: TObject);
begin
  FreeAndNil(MapDesktopItem);

  FDataset:=nil;

  inherited;


  dmMap_Desktop:=nil;


 // ShowMessage('procedure TdmMap_Desktop.DataModuleDestroy(Sender: TObject);');
end;



procedure TdmMap_Desktop.ADOStoredProc_MapDesktopsAfterPost(DataSet: TDataSet);
var
  k: Integer;
begin
//   dmOnega_DB_data.sp_MapDesktop_update(iID, 'rename', sName);

  k:= dmOnega_DB_data.ExecStoredProc_('sp_MapDesktop_update',

        [
         FLD_ID,               DataSet[FLD_ID],
         FLD_show_calc_maps,   DataSet[FLD_Show_calc_maps],
         FLD_show_geo_maps,    DataSet[FLD_Show_geo_maps],
         FLD_show_object_maps, DataSet[FLD_Show_object_maps],
         FLD_is_calc_map_at_bottom, DataSet[FLD_is_calc_map_at_bottom]

//    MapDesktopItem.is_calc_map_at_bottom  := FieldByName().AsBoolean;


        ]);
end;



// ---------------------------------------------------------------
procedure TdmMap_Desktop.ChangeProject(aProjectID: integer);
// ---------------------------------------------------------------

begin
//  FProjectID :=aProjectID;

  if aProjectID=0 then
    Exit;

  Open;


 (* if qry_Desktops.RecordCount=0 then
  begin
    Add(MAP_DEFAULT_DESKTOP_NAME);

    Open;
  end;*)

end;

procedure TdmMap_Desktop.CloseDB;
begin
  FDataSet.Close;
end;


procedure TdmMap_Desktop.Log(aMsg: string);
begin
  g_Log.SysMsg(aMsg);
end;

//----------------------------------------------------------------------
procedure TdmMap_Desktop.Open;
//----------------------------------------------------------------------
var
  k: Integer;
begin
  if dmMain.ProjectID=0 then
    Exit;

  k := dmOnega_DB_data.MapDesktop_Select(ADOStoredProc_MapDesktops, dmMain.ProjectID);


  MapDesktop_ID := FDataset.FieldByName(FLD_ID).AsInteger;

end;


// ---------------------------------------------------------------
procedure TdmMap_Desktop.MapDesktopItem_Init2;
// ---------------------------------------------------------------
var
//  iMapDesktop_ID: Integer;
  k: Integer;
begin
 // Assert(dmMain.ProjectID>0, 'Value <=0');


  Assert(MapDesktop_ID>0, 'Value <=0');


//  Assert(ADOQuery1.RecordCount>0, 'TBL_MAP_DESKTOPS <=0');

  with ADOStoredProc_MapDesktops do
  begin
    MapDesktopItem.IsShowGeoMap   := FieldByName(FLD_show_geo_maps).AsBoolean;
    MapDesktopItem.IsShowObjectMap:= FieldByName(FLD_show_object_maps).AsBoolean;
    MapDesktopItem.IsShowCalcMap:= FieldByName(FLD_show_calc_maps).AsBoolean;

    MapDesktopItem.is_calc_map_at_bottom  := FieldByName(FLD_is_calc_map_at_bottom).AsBoolean;


    //iMapDesktop_ID := FieldByName(FLD_ID).AsInteger;

  end;

  MapDesktopItem.MapList.Clear;



  if MapDesktopItem.IsShowObjectMap then
    Prepare_ObjectMaps ();


  if MapDesktopItem.is_calc_map_at_bottom then
  begin
    if MapDesktopItem.IsShowGeoMap then
      Prepare_GeoMaps ();

    if MapDesktopItem.IsShowCalcMap then
      Prepare_CalcMaps ();
  end else
  begin
    if MapDesktopItem.IsShowCalcMap then
      Prepare_CalcMaps ();

    if MapDesktopItem.IsShowGeoMap then
      Prepare_GeoMaps ();
  end;


end;

//--------------------------------------------------------------------
procedure TdmMap_Desktop.Prepare_GeoMaps;
//--------------------------------------------------------------------
var
  i: Integer;
  oMapItem: TMapItem;
  s: string;
  sFileName: string;
begin
  Assert(MapDesktop_ID>0, 'Value <=0');


  i:=dmOnega_DB_data.MapDesktop_Select_GeoMaps (ADOStoredProc_Maps, MapDesktop_ID, True);

 // MapDesktopItem.GeoMapList.Clear;


  with ADOStoredProc_Maps do
    while not EOF do
  begin
//    sFileName := FieldByName(FLD_FILENAME).AsString;
    sFileName := FieldByName(FLD_NAME).AsString;

 //   if not FileExists(oMapItem.FileName) then
    if not FileExists(sFileName) then
    begin
      Next;
      Continue;
    end;



//    oMapItem :=MapDesktopItem.GeoMapList.AddItem;
    oMapItem :=MapDesktopItem.MapList.AddItem;

    oMapItem.MapKind := mkGeoMap;

    oMapItem.map_ID     := FieldByName(FLD_MAP_ID).AsInteger;

//   oMapItem.FileName:=FieldByName(FLD_NAME).AsString;
   oMapItem.FileName:=sFileName; //FieldByName(FLD_FILENAME).AsString;

//    oMapItem.FileName:=FieldByName(FLD_NAME).AsString;

    oMapItem.ZoomMin:=   FieldByName(FLD_ZOOM_MIN).AsInteger;
    oMapItem.ZoomMax:=   FieldByName(FLD_ZOOM_MAX).AsInteger;

    oMapItem.IsZoomAllowed:= FieldByName(FLD_ALLOW_ZOOM).AsBoolean;
    oMapItem.AutoLabel    := FieldByName(FLD_AutoLabel).AsBoolean;

    if oMapItem.AutoLabel then
    begin
      s:=FieldByName(FLD_label_style).AsString;
//      oMapItem.LabelProperties.LoadFromText(s);
      oMapItem.LabelProperties.AsText := s;
    end;

    oMapItem.RunTime.IS_FILE_EXISTS := FileExists (oMapItem.FileName);

    Next;
  end;
end;


//--------------------------------------------------------------------
procedure TdmMap_Desktop.Prepare_CalcMaps;
//--------------------------------------------------------------------
var
  oMapItem: TMapItem;
  i: integer;
  sFileName: string;
begin
  Assert(MapDesktop_ID>0, 'Value <=0');

//  dmOnega_DB_data.sp_MapDesktop_Select_CalcMaps (qry_CalcMaps, aMapDesktop_ID, True);
  i := dmOnega_DB_data.MapDesktop_Select_CalcMaps (ADOStoredProc_CalcMaps, MapDesktop_ID, True);

 // i := ADOStoredProc_CalcMaps.RecordCount;

 ///// db_View(ADOStoredProc_CalcMaps);

 // MapDesktopItem.CalcMapList.Clear;

  with ADOStoredProc_CalcMaps do
    while not EOF do
  begin
    sFileName:=FieldByName(FLD_FILENAME).AsString;

    if not FileExists(sFileName) then
    begin
      Next;
      Continue;
    end;



    if sFileName<>'' then
    begin
  //    oMapItem :=MapDesktopItem.CalcMapList.AddItem;
      oMapItem :=MapDesktopItem.MapList.AddItem;


      oMapItem.MapKind := mkCalcMap;
      oMapItem.FileName:=FieldByName(FLD_FILENAME).AsString;
//    oMapItem.FileName:=ME).AsString;
    end;
//FieldByName(FLD_NAME).AsString;

    Next;
  end;
end;

//--------------------------------------------------------------------
procedure TdmMap_Desktop.Prepare_ObjectMaps;
//--------------------------------------------------------------------
const
  DEF_PREFIX = '~';
var
  i: Integer;
  oMapItem: TMapItem;
  s: string;
begin
  

  Assert(MapDesktop_ID>0, 'Value <=0');


  i := dmOnega_DB_data.MapDesktop_Select_ObjectMaps (ADOStoredProc_Object_Maps, MapDesktop_ID, True);


  //   db_View(ADOStoredProc_Object_Maps);


//  dmOnega_DB_data.sp_MapDesktop_Select_ObjectMaps (qry_Object_Maps, aMapDesktop_ID, True);

 // MapDesktopItem.ObjectMapList.Clear;


  with ADOStoredProc_Object_Maps do
    while not EOF do
  begin
//    oMapItem :=MapDesktopItem.ObjectMapList.AddItem;
    oMapItem :=MapDesktopItem.MapList.AddItem;


    oMapItem.MapKind := mkObjectMap;


 //   oMapItem.FileName := sFileName;
  //  oMapItem.MapKind  := mkGeoMap;
  //  oMapItem.ID      := FieldByName(FLD_ID).AsInteger;

//  FFileDir:=dmMain.Dirs.ProjectMapDir;

//  ForceDirectories(FFileDir);

 // Result := ChangeFileExt(FFileDir + DEF_PREFIX+ aObjName, '.tab');

    oMapItem.ObjectName   :=FieldByName(FLD_NAME).AsString;

    oMapItem.FileName     := dmMain.Dirs.ProjectMapDir +
                             DEF_PREFIX+ oMapItem.ObjectName +'.tab';

    oMapItem.Caption      :=FieldByName(FLD_Caption).AsString;

{
    oMapItem.IsZoomAllowed:= FieldByName(FLD_ALLOW_ZOOM).AsBoolean;
    oMapItem.ZoomMin      := FieldByName(FLD_ZOOM_MIN).AsInteger;
    oMapItem.ZoomMax      := FieldByName(FLD_ZOOM_MAX).AsInteger;
}

    oMapItem.AutoLabel    := FieldByName(FLD_AutoLabel).AsBoolean;
    oMapItem.IsSelectable := True;

    s:=FieldByName(FLD_label_style).AsString;

    oMapItem.LabelProperties.AsText := s;




(*
    if oMapItem.AutoLabel then
    begin
      s:=FieldByName(FLD_label_style).AsString;
      oMapItem.LabelProperties.AsText := s;
    end;
*)
    Next;
  end;

end;


//------------------------------------------------------------------
procedure TdmMap_Desktop.DoMapChecked;
//------------------------------------------------------------------
begin
//  db_PostDataset(FDataSet);

  if Assigned(FOnMapChecked) then
    FOnMapChecked(Self);
end;



begin

end.



