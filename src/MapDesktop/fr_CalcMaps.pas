unit fr_CalcMaps;

interface

uses
  SysUtils, Classes, Controls, Forms, ImgList,   Dialogs, Messages,
  cxInplaceContainer, cxTL, cxControls, StdCtrls, ComCtrls,
  DB, ADODB, cxTLData, Grids, DBGrids, DBCtrls,  Windows,
  Menus, ActnList, cxDBTL,  ToolWin, Variants,

  u_func_msg,

  MapXLib_TLB,

  d_Transparent,

  d_Map_Export,

  u_classes,

  dm_Map_Desktop,

  dm_CalcMap,

  dm_Onega_DB_data,

 ///// d_export_CalcMap_to_kml,

  u_mapx,

  u_db,
  u_dlg,
  u_func,

  u_const_db,
  u_const_msg,

  AppEvnts, cxStyles;




type
  Tframe_CalcMaps = class(TForm, IMessageHandler)
    CheckStateImages: TImageList;
    FolderImageList1: TImageList;
    PopupMenu1: TPopupMenu;
    ActionList1: TActionList;
    act_Del: TAction;
    N1: TMenuItem;
    act_ShowInExplorer: TAction;
    actShowInExplorer1: TMenuItem;
    ds_CalcMaps: TDataSource;
    cxDBTreeList1: TcxDBTreeList;
    col_ID: TcxDBTreeListColumn;
    col_name: TcxDBTreeListColumn;
    col_filename: TcxDBTreeListColumn;
    col_checked: TcxDBTreeListColumn;
    col_is_folder_: TcxDBTreeListColumn;
    ADOStoredProc_CalcMaps: TADOStoredProc;
    act_Refresh: TAction;
    N2: TMenuItem;
    N3: TMenuItem;
    col_parent_id: TcxDBTreeListColumn;
    StatusBar_File: TStatusBar;
    ADOConnection1111111: TADOConnection;
    ToolBar1: TToolBar;
    DBCheckBox2: TDBCheckBox;
    DBCheckBox1: TDBCheckBox;
    act_Show_in_Map: TAction;
    actShowinMap1: TMenuItem;
    col_checked_image_index: TcxDBTreeListColumn;
    DBGrid1: TDBGrid;
    cxDBTreeList1cxDBTreeListColumn1: TcxDBTreeListColumn;
    act_Transparent: TAction;
    N4: TMenuItem;
    act_Export: TAction;
    actTransparent1: TMenuItem;
    col_folder_image_index: TcxDBTreeListColumn;
    actExport2: TMenuItem;
    col_Date_craeted: TcxDBTreeListColumn;
    col_Matrix_filename: TcxDBTreeListColumn;
    cxStyleRepository1: TcxStyleRepository;
    cxStyle_StrikeOut: TcxStyle;
    procedure FormDestroy(Sender: TObject);
    procedure FormCreate(Sender: TObject);

//   procedure FormDestroy(Sender: TObject);
    procedure DoOnExecute(Sender: TObject);
    procedure ActionList1Update(Action: TBasicAction; var Handled: Boolean);
//    procedure act_RefreshExecute(Sender: TObject);
//    procedure ApplicationEvents1Message(var Msg: tagMSG; var Handled: Boolean);
//    procedure CheckBox1Click(Sender: TObject);
//    procedure dx_CalcMapsClick(Sender: TObject);
    procedure qry_CalcMapsAfterScroll(DataSet: TDataSet);
    procedure col_checked_PropertiesChange(Sender: TObject);
    procedure cxDBTreeList1MouseDown(Sender: TObject; Button: TMouseButton; Shift:
        TShiftState; X, Y: Integer);
    procedure cxDBTreeList1MouseUp(Sender: TObject; Button: TMouseButton; Shift:
        TShiftState; X, Y: Integer);
//    procedure dddddddddddddd1Click(Sender: TObject);
    procedure qry_CalcMapsAfterPost(DataSet: TDataSet);
    procedure cxDBTreeList1StylesGetContentStyle(Sender: TcxCustomTreeList;
      AColumn: TcxTreeListColumn; ANode: TcxTreeListNode;
      var AStyle: TcxStyle);
  private
    FChangeChecked : Boolean;
  private
    FList: TStringList;


    FDataSet: TDataSet;

    FMap: TMap;

    FMapDesktopID: Integer;

    FOnMapChecked: TNotifyEvent;

    procedure DoGoogleExport;
    procedure DoMapChecked;

    procedure GetMessage(aMsg: TEventType; aParams: TEventParamList ; var aHandled:
        boolean);
  protected

  public
   // IsAutoUpdate : Boolean;

    procedure View(aMap: TMap; aMapDesktopID: Integer);

    property OnMapChecked: TNotifyEvent read FOnMapChecked write FOnMapChecked;
  end;


//================================================
implementation {$R *.DFM}

const
  FLD_checked_image_index = 'checked_image_index';

  DEF_STATE_UNCHECKED = 0;
  DEF_STATE_CHECKED   = 1;


//-------------------------------------------------------------------
procedure Tframe_CalcMaps.FormCreate(Sender: TObject);
//-------------------------------------------------------------------
begin
  inherited;

  FList:=TStringList.Create;


  if ADOConnection1111111.Connected then
    ShowMessage('procedure Tframe_CalcMaps.FormCreate(Sender: TObject);-ADOConnection1.Connected');

  act_Show_in_Map.Caption:='�������� �� �����';

  act_Transparent.Caption:='������������';

  act_Export.Caption:= '�������';
//  act_Transp.Caption:= '������������';



  DBCheckBox1.DataSource := dmMap_Desktop.ds_Desktops;
  DBCheckBox1.Caption:='�������� ����';

  DBCheckBox2.DataSource := dmMap_Desktop.ds_Desktops;
  DBCheckBox2.Caption:='����������� ��� ����� �������';


//  col_matrix_filename.Visible := False;
 // col_matrix_filename.Visible := True;



 // FRegPath:=REGISTRY_COMMON_FORMS+ClassName+'\';


  act_ShowInExplorer.Caption:= '�������� � ����������';

  cxDBTreeList1.Align:=alClient;
  cxDBTreeList1.Images := nil;

//  cxDBTreeList1.StoreToRegistry();



//  cxDBTreeList1.DataController.StateIndexFieldName := '';
  cxDBTreeList1.DataController.StateIndexField := FLD_checked_image_index;
  cxDBTreeList1.StateImages := CheckStateImages;

  col_Checked.Caption.Text:='���';

 // FDataSet :=qry_CalcMaps;
  FDataSet :=ADOStoredProc_CalcMaps;
end;


procedure Tframe_CalcMaps.FormDestroy(Sender: TObject);
begin
  FDataSet := nil;

  FreeAndNil(FList);

  inherited;
end;



procedure Tframe_CalcMaps.qry_CalcMapsAfterPost(DataSet: TDataSet);
var
  bChecked: Boolean;
  k: Integer;
 // v: Variant;
begin
 // v:=DataSet[FLD_ID];
  bChecked:=DataSet.FieldByName(FLD_CHECKED).AsBoolean;

  k:=dmOnega_DB_data.Map_XREF_Update(DataSet[FLD_ID], bChecked);


end;



//-------------------------------------------------------------------
procedure Tframe_CalcMaps.View(aMap: TMap; aMapDesktopID: Integer);
//-------------------------------------------------------------------
var
  k: Integer;
begin
  Assert(Assigned(aMap), 'Value not assigned');
  // -------------------------

  FMapDesktopID:=aMapDesktopID;
  k:=dmOnega_DB_data.MapDesktop_Select_CalcMaps (ADOStoredProc_CalcMaps, aMapDesktopID);

  FMap:=aMap;


  cxDBTreeList1.FullExpand;

 // db_View(ADOStoredProc_CalcMaps);

end;


//-------------------------------------------------------------------
procedure Tframe_CalcMaps.DoOnExecute(Sender: TObject);
//-------------------------------------------------------------------
var
  i: Integer;
  sFile: string;
  iID: Integer;
  iIndex: Integer;
  iValue: Integer;
  k: Integer;
  sFileName: string;
  sValue: string;
  vLayer: CMapXLayer;

begin
  //---------------------------------------
  if Sender=act_Transparent then begin
  //---------------------------------------
    sFileName := FDataset.FieldByName(FLD_filename).AsString;


//    if not MapFileExists(sFileName) then
    if not FileExists(sFileName) then
    begin
      ErrDlg('���������� ����� �� ����������');
      exit;
    end;


    iValue := mapx_GetTransparence_0_100(sFileName);
    if iValue>=0 then
    begin
      sValue := IntToStr(iValue);


      if Tdlg_Transparent.ExecDlg (iValue) then
      begin
        mapx_SetTransparence_0_100 (sFileName, iValue);

        mapx_ReOpenMapByFileName (Fmap, sFileName);
      end;

{

//      if InputQuery('������������', '�������� 0..255', sValue) then
      if InputQuery('������������', '�������� 0-100 [%]', sValue) then
      begin
        mapx_SetTransparence_0_100 (sFileName, AsInteger(sValue));

        mapx_ReOpenMapByFileName (Fmap, sFileName);



      end;
}

    end;




 //   if Assigned(vLayer) then
//      vLayer.Style.
//      vLayer.Visible :=False;


//    function InputQuery(const ACaption, APrompt: string; var Value: string): Boolean;

//    Query


  //  TDlg_MapTransp.ExecDlg (sFileName,
    //  mem_CalcMaps.FieldByName(FLD_NAME).AsString);

  end else


  //---------------------------------------
  if Sender=act_Export then
  //---------------------------------------
  begin
    DoGoogleExport;
  end else


  // -------------------------
  if Sender=act_Show_in_Map then
  // -------------------------
  begin
    sFileName := FDataset.FieldByName(FLD_filename).AsString;

    vLayer :=mapx_GetLayerByFileName(FMap, sFileName);
    if Assigned(vLayer) then
      mapx_SetBoundsFromLayer(FMap, vLayer);

  end else

  // -------------------------
  if (Sender = act_Refresh) then
  // -------------------------
    View(FMap,FMapDesktopID) else

  // ---------------------------------------------------------------
  if Sender = act_Del then
  // ---------------------------------------------------------------
  begin
    if ConfirmDlg('������� ?') then
    begin
     // db_ViewDataSet(FDataSet);

      iID := FDataSet[FLD_CALCMAP_ID];

      sFileName := FDataSet.FieldByName(FLD_FILENAME).AsString;

      k:=dmOnega_DB_data.CalcMap_Del(iID);

      cxDBTreeList1.FocusedNode.Delete;

//      cxDBTreeList1.sub


      if sFileName<>'' then
        mapx_RemoveMapFile (Fmap, sFileName);

// iID:=SelectedRows[i].Values[col_Map_id.Index];


     // dmOnega_DB_data.CalcMap_Del(FDataSet[FLD_ID]);
 ///////     View (FmapDesktopID);
    end;
  end else

  // ---------------------------------------------------------------
  if Sender = act_ShowInExplorer then
  // ---------------------------------------------------------------
  begin
    sFile:=FDataSet.FieldByName(FLD_FILENAME).AsString;
    if (sFile <> '') then //and   //   (mem_CalcMaps.FieldByName(FLD_EXIST).AsBoolean)
      ShellExec('explorer.exe', ExtractFileDir(sFile))
  end  else
    raise Exception.Create('');

end;

//-------------------------------------------------------------------
procedure Tframe_CalcMaps.ActionList1Update(Action: TBasicAction; var Handled: Boolean);
//-------------------------------------------------------------------
var
  b: Boolean;
begin
  b:=FDataSet.RecordCount > 0;

  act_Del.Enabled:=b;
  act_Show_in_Map.Enabled:=b;

//  act_ShowInExplorer.Enabled:=  (mem_CalcMaps.FieldByName(FLD_FILENAME).AsString <> '') and
//                                (mem_CalcMaps.FieldByName(FLD_EXIST).AsBoolean);

  act_ShowInExplorer.Enabled:=  (FDataSet.FieldByName(FLD_FILENAME).AsString <> '');

end;

// ---------------------------------------------------------------
procedure Tframe_CalcMaps.DoMapChecked;
// ---------------------------------------------------------------
var
  bChecked: Boolean;
  sFileName: string;

 // vLayer: CMapXLayer;
begin
 // db_PostDataset(FDataSet);

  db_PostDataset(FDataSet);


  sFileName := FDataset.FieldByName(FLD_filename).AsString;
  bChecked  := FDataset.FieldByName(FLD_Checked).AsBoolean;

//  if bChecked then


//   mapx_ad

 // if aMap.Layers.Count=0 then



  //vLayer:=mapx_GetLayerByFileName(Fmap, sFileName);


  if bChecked then
    mapx_AddMap (FMap, sFileName)
  else
    mapx_RemoveMapFile (FMap, sFileName);


   

 {
  Assert(FileExists(sMapFileName));

  vLayer:=mapx_GetLayerByFileName(Fmap, sMapFileName);


  if vLayer=nil then
    //vLayer:=
    FMap.Layers.Add (sMapFileName, EmptyParam);


           FMap.Layers.Remove (i);


  }

 // if IsAutoUpdate then
  //  Ffrm_CalcMaps

  exit;

  if Assigned(FOnMapChecked) then
    FOnMapChecked(Self);

end;


procedure Tframe_CalcMaps.qry_CalcMapsAfterScroll(DataSet: TDataSet);
begin
//  StatusBar1.SimpleText:= Format('Key: %s', [DataSet.FieldByName(FLD_KEY_NAME).AsString]);
  StatusBar_File.SimpleText:= Format('����: %s', [DataSet.FieldByName(FLD_FILENAME).AsString]);
end;

procedure Tframe_CalcMaps.col_checked_PropertiesChange(Sender: TObject);
begin
  DoMapChecked();
end;

procedure Tframe_CalcMaps.cxDBTreeList1MouseDown(Sender: TObject; Button:
    TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  FChangeChecked:=cxDBTreeList1.HitTest.HitAtStateImage;
end;

// ---------------------------------------------------------------
procedure Tframe_CalcMaps.cxDBTreeList1MouseUp(Sender: TObject; Button:
    TMouseButton; Shift: TShiftState; X, Y: Integer);
// ---------------------------------------------------------------
var
  i: Integer;
  bChanged, bCdmaMap: boolean;
  oNode: TcxTreeListNode;// DBTreeListNode;
//  arrMaps: TStrArray;
  sFileName: string;
  iImageIndex : Integer;

begin
  if not FChangeChecked then
    Exit;

  FChangeChecked:=false;

  oNode:=cxDBTreeList1.HitTest.HitNode;

//  DEF_STATE_CHECKED   = 0;
//  DEF_STATE_UNCHECKED = 1;


  if oNode.StateIndex=DEF_STATE_CHECKED then
    iImageIndex:=DEF_STATE_UNCHECKED
//    oNode.StateIndex:=DEF_STATE_UNCHECKED
  else
    iImageIndex:=DEF_STATE_CHECKED;

  oNode.StateIndex:=iImageIndex;

  db_UpdateRecord (ADOStoredProc_CalcMaps, FLD_checked_image_index, iImageIndex);
  db_UpdateRecord (ADOStoredProc_CalcMaps, FLD_CHECKED,             iImageIndex = DEF_STATE_CHECKED);

  DoMapChecked();

(*
  if oNode.StateIndex=1 then
    oNode.StateIndex:=0
  else
    oNode.StateIndex:=1;

*)
//  iNewSta

//  db_UpdateRecord (ADOStoredProc_CalcMaps, FLD_checked_image_index, 1);
//  db_UpdateRecord (ADOStoredProc_CalcMaps, FLD_CHECKED, True);




//  bChanged:= ChangeChecked(tree_Local.GetHitInfo(Point(X,Y)).Node);
////  bChanged:= dmCalcMap.ChangeChecked(FLocRegPath, arrMaps);
//  if not bChanged then exit;
//  Changed:= true;
//
////  sFileName:= dmCalcMap.mem_CalcMaps.FieldByName(FLD_FILENAME).AsString;
////  bCdmaMap:= Pos('CDMA', sFileName)>0;
//
//  if Assigned(OnMapSelected) and (Changed) and (bChanged) then
//     FOnMapSelected;


end;

// ---------------------------------------------------------------
procedure Tframe_CalcMaps.DoGoogleExport;
// ---------------------------------------------------------------
var
  i, iID: Integer;
  oIDList: TIDList;
  sColorXMLFile: string;
  sMapFileName: string;
  sMatrix_FileName: string;
begin
  sMatrix_FileName := FDataset.FieldByName(FLD_Matrix_filename).AsString;
  sMapFileName     := FDataset.FieldByName(FLD_filename).AsString;

  sColorXMLFile:= ChangeFileExt(sMapFileName, '.colors.xml');


  if not FileExists(sMatrix_FileName)  then
  begin
    ShowMessage('���� � ������ Matrix_FileName: ' + sMatrix_FileName);
    Exit;
  end;

  if not FileExists(sColorXMLFile)  then
  begin
    ShowMessage('���� � ������: ' + sColorXMLFile);
    Exit;
  end;


  Tdlg_Map_Export.ExecDlg (sMatrix_FileName, sColorXMLFile);

(*
  if FileExists(sMatrix_FileName) and (FileExists(sColorXMLFile)) then
    Tdlg_Map_Export.ExecDlg (sMatrix_FileName, sColorXMLFile)
  else
    ShowMessage('������� ������ ������ �� ��������������.');
*)

(*
  if not FileExists(sMatrix_FileName)  then
      ShowMessage('������� ������ ������ �� ��������������.');

    Tdlg_Map_Export.ExecDlg (sMatrix_FileName, sColorXMLFile)
  else
*)


(*  oIDList:= TIDList.Create;

  for i := 0 to cxDBTreeList1.SelectionCount - 1 do
  begin
    iID:= AsInteger(cxDBTreeList1.Selections[i].Values[col_ID.ItemIndex]);

    oIDList.AddID(iID).Name:=
      AsString(cxDBTreeList1.Selections[i].Values[col_FileName.ItemIndex]);
  end;

 ///// Tdlg_export_CalcMap_to_kml.ExecDlg(oIDList);

  oIDList.Free;
*)


(*      if not MapForGoogle(mem_CalcMaps.FieldByName(FLD_FILENAME).AsString) then
    MsgDlg('������� �������� ������ ��� ��������� ���� ������ � ���������� ����� (��������, ������, ������ ����, Handover, CI, CA, CIA)')
  else
    Tdlg_export_CalcMap_to_kml.ExecDlg(mem_CalcMaps.FieldByName(FLD_ID).AsInteger,
            mem_CalcMaps.FieldByName(FLD_FILENAME).AsString);
*)
end;


procedure Tframe_CalcMaps.GetMessage(aMsg: TEventType; aParams: TEventParamList
    ; var aHandled: boolean);
begin

  case aMsg of
    et_CalcMap_REFRESH:
        View(FMap, FMapDesktopID)
//
//   // WE_PROJECT_UPDATE_MAP_LIST: View(FID);
  end;

end;




procedure Tframe_CalcMaps.cxDBTreeList1StylesGetContentStyle(
  Sender: TcxCustomTreeList; AColumn: TcxTreeListColumn;
  ANode: TcxTreeListNode; var AStyle: TcxStyle);
var
  sFileName: string;
begin
   sFileName:= VarToStr (ANode.Values[col_FileName.ItemIndex] );
   if (sFileName<>'') and (not FileExists(sFileName)) then
      AStyle:=cxStyle_StrikeOut;

end;

end.


  {


procedure Tframe_Explorer.cxTreeList1StylesGetContentStyle(
  Sender: TcxCustomTreeList; AColumn: TcxTreeListColumn;
  ANode: TcxTreeListNode; var AStyle: TcxStyle);
var
  sFileName: string;
begin
   if  LowerCase( VarToStr (ANode.Values[col_OBJECT_NAME.ItemIndex])) = 'rel_matrix' then
   begin
     sFileName:= ANode.Values[col_Name_.ItemIndex];
     if (sFileName<>DN_FOLDER_REL_MATRIX) and (not FileExists(sFileName)) then
        AStyle:=cxStyle_StrikeOut;
   end;


         //DN_FOLDER_REL_MATRIX
//  AStyle:=cxStyle_StrikeOut;
end;

end.

procedure Tframe_CalcMaps.act_RefreshExecute(Sender: TObject);
begin

//  if (Sender = act_Refresh) then
 //   View(FMapDesktopID, FMap)

end;




FMapList: TStringList;



  vLayer: CMapXLayer;

     vLayer:=mapx_GetLayerByFileName(Fmap, oMapItem.FileName);

          if vLayer<>nil then
            vLayer.Visible := True;
           // ShowMessage('if vLayer<>nil then');


          if vLayer=nil then
          //�������� � ����� ������

            try
            //  iEllipsoid1:=FMap.DisplayCoordSys.Datum.Ellipsoid;
           //   iEllipsoid2:=FMap.NumericCoordSys.Datum.Ellipsoid;
       //       if not FileExists(oMapItem.FileName) then
        //        Continue;
              Assert(FileExists(oMapItem.FileName));


              vLayer:= FMap.Layers.Add (oMapItem.FileName, FMap.Layers.Count+1);

           //   if oMapItem.map_ID>0 then
           //      FMapList.Values[ IntToStr( oMapItem.map_ID) ]:=oMapItem.FileName;


            except
              on E: Exception do
                Continue;
            end;


            
//---------------------------------------------------------------------------
procedure Tframe_CalcMaps.ApplicationEvents1Message(var Msg: tagMSG; var Handled: Boolean);
//---------------------------------------------------------------------------
begin
{
  case Msg.message of
     WM_REFRESH_CALC_MAP_FILES: View(FMap, FMapDesktopID)
//
//   // WE_PROJECT_UPDATE_MAP_LIST: View(FID);
  end;
}

  case Msg.message of
     WM_USER + Integer(WM__CalcMap_REFRESH):
        View(FMap, FMapDesktopID)
//
//   // WE_PROJECT_UPDATE_MAP_LIST: View(FID);
  end;

end;



{


procedure Tframe_Explorer.cxTreeList1StylesGetContentStyle(
  Sender: TcxCustomTreeList; AColumn: TcxTreeListColumn;
  ANode: TcxTreeListNode; var AStyle: TcxStyle);
var
  sFileName: string;
begin
   if  LowerCase( VarToStr (ANode.Values[col_OBJECT_NAME.ItemIndex])) = 'rel_matrix' then
   begin
     sFileName:= ANode.Values[col_Name_.ItemIndex];
     if (sFileName<>DN_FOLDER_REL_MATRIX) and (not FileExists(sFileName)) then
        AStyle:=cxStyle_StrikeOut;
   end;


         //DN_FOLDER_REL_MATRIX
//  AStyle:=cxStyle_StrikeOut;
end;

end.
