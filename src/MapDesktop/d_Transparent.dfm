object dlg_Transparent: Tdlg_Transparent
  Left = 1229
  Top = 599
  BorderStyle = bsDialog
  Caption = 'dlg_Transparent'
  ClientHeight = 112
  ClientWidth = 285
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 48
    Width = 27
    Height = 13
    Caption = 'Value'
  end
  object TrackBar1: TTrackBar
    Left = 0
    Top = 0
    Width = 285
    Height = 41
    Align = alTop
    BorderWidth = 8
    Max = 100
    TabOrder = 0
    TickStyle = tsManual
    OnChange = TrackBar1Change
  end
  object Button1: TButton
    Left = 64
    Top = 80
    Width = 75
    Height = 25
    Caption = 'Ok'
    Default = True
    ModalResult = 1
    TabOrder = 1
  end
  object Button2: TButton
    Left = 152
    Top = 80
    Width = 75
    Height = 25
    Cancel = True
    Caption = #1054#1090#1084#1077#1085#1072
    ModalResult = 2
    TabOrder = 2
  end
end
