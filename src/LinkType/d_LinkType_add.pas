unit d_LinkType_add;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  rxPlacemnt, ActnList, StdCtrls, ExtCtrls, cxPropertiesStore,

  d_Wizard_Add,

  u_const_str,

  dm_LinkType
  ;

type
  Tdlg_LinkType_add = class(Tdlg_Wizard_Add)
    procedure act_OkExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    FID,FFolderID: integer;

  public
    class function ExecDlg (aFolderID: integer =0): integer;
  end;


implementation

{$R *.DFM}

//--------------------------------------------------------------------
class function Tdlg_LinkType_add.ExecDlg (aFolderID: integer =0): integer;
//--------------------------------------------------------------------
begin
  with Tdlg_LinkType_add.Create(Application) do
  begin
    FFolderID:=aFolderID;
    ed_Name_.Text:=dmLinkType.GetNewName();

    ShowModal;
    Result:=FID;

    Free;
  end;
end;


procedure Tdlg_LinkType_add.act_OkExecute(Sender: TObject);
begin
  inherited;
  FID:=dmLinkType.Add (ed_Name_.Text);//, FFolderID);
end;


procedure Tdlg_LinkType_add.FormCreate(Sender: TObject);
begin
  inherited;
  SetActionName (STR_DLG_ADD_LINKTYPE);

end;


end.
