unit dm_LinkType;

interface

uses
  SysUtils, Classes, Graphics, Controls, Forms, Db, ADODB, Variants,

  dm_Main,

  u_types,

  u_const_str,
  u_const_db,

  u_db,
  u_func,

  dm_Object_base
  ;

type
  TdmLinkType = class(TdmObject_base)
  procedure DataModuleCreate(Sender: TObject);
  private
    { Private declarations }
  public
    function Add(aName: string): integer;

  end;


function dmLinkType: TdmLinkType;


//==================================================================
implementation {$R *.DFM}
//==================================================================

var
  FdmLinkType: TdmLinkType;


procedure TdmLinkType.DataModuleCreate(Sender: TObject);
begin
  inherited;

  TableName:=TBL_LINKTYPE;
  DisplayName:=STR_LINKTYPE;
  ObjectName:=OBJ_LINK_TYPE;
end;


// ---------------------------------------------------------------
function dmLinkType: TdmLinkType;
// ---------------------------------------------------------------
begin
  if not Assigned(FdmLinkType) then
    FdmLinkType := TdmLinkType.Create(Application);

  Result := FdmLinkType;
end;


//--------------------------------------------------------------------
function TdmLinkType.Add(aName: string): integer;
//--------------------------------------------------------------------
begin
  Result:=gl_DB.AddRecordID (TableName,
                  [db_Par(FLD_NAME, aName)
                   //db_Par(FLD_FOLDER_ID, IIF_NULL(aFolderID))
                  ]);
end;


begin

end.


(*
//--------------------------------------------------------------------
function TdmLinkType.Add (aName: string; aFolderID: integer): integer;
//--------------------------------------------------------------------
begin
  Result:=gl_DB.AddRecordID (TableName,
                  [db_Par(FLD_NAME, aName),
                   db_Par(FLD_FOLDER_ID, IIF_NULL(aFolderID))
                  ]);
end;
       *)
