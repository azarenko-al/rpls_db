unit dm_act_LinkType;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls,
  Dialogs,  Menus, ActnList, Forms,

  dm_User_Security,

  dm_Onega_DB_data,

 // d_Audit,

  I_Shell,
  u_Shell_new,  

  u_DataExport_run,

  I_Object,
  
  dm_act_Base,

  u_func,
  u_const,
  u_const_msg,
  u_const_db,

  u_const_str,

  u_types,
  u_func_msg,
  u_dlg,

  dm_LinkType,
  d_LinkType_add;

type
  TdmAct_LinkType = class(TdmAct_Base)
    ActionList2: TActionList;
    act_Export_MDB: TAction;
    act_Audit1: TAction;
    act_Copy: TAction;
    act_Validate: TAction;
    procedure DataModuleCreate(Sender: TObject);
  private
    function CheckActionsEnable: Boolean;
//    function CheckActionsEnable: Boolean;
    procedure Copy;
    procedure DoAction (Sender: TObject);
  protected
    procedure GetPopupMenu (aPopupMenu: TPopupMenu; aMenuType: TMenuType); override;

    function ItemDel(aID: integer; aName: string = ''): boolean; override;
    function ItemAdd (aFolderID: integer): integer; override;

  public
//    procedure Add       (aFolderID: integer);
//    procedure Del       (aID: integer);

    class procedure Init;
  end;

var
  dmAct_LinkType: TdmAct_LinkType;

//==================================================================
implementation {$R *.dfm}
//==================================================================

//--------------------------------------------------------------------
class procedure TdmAct_LinkType.Init;
//--------------------------------------------------------------------
begin
  if not Assigned(dmAct_LinkType) then
    dmAct_LinkType:=TdmAct_LinkType.Create(Application);
end;

//--------------------------------------------------------------------
procedure TdmAct_LinkType.DataModuleCreate(Sender: TObject);
//--------------------------------------------------------------------
begin
  inherited;

  ObjectName:=OBJ_LINK_TYPE;

  act_Export_MDB.Caption:=DEF_STR_Export_MDB;

  act_Copy.Caption  := STR_ACT_COPY;
  act_Validate.Caption  := '������������ ������';


{
  act_Audit.Caption:=STR_Audit;
  act_Audit.Enabled:=False;
}

  SetActionsExecuteProc ([
                          act_Copy,
                          act_Export_MDB,
                          act_Validate
                          
                       //   act_Audit

                          ], DoAction);


//  act_Add.Caption:=STR_ADD;
//  act_Del.Caption:=STR_DEl;

//  dlg_SetAllActionsExecuteProc ([act_Add,act_Del], DoAction);


end;

//--------------------------------------------------------------------
function TdmAct_LinkType.ItemDel(aID: integer; aName: string = ''): boolean;
//--------------------------------------------------------------------
begin
  Result:=dmLinkType.Del (aID);
end;

//--------------------------------------------------------------------
function TdmAct_LinkType.ItemAdd (aFolderID: integer): integer;
//--------------------------------------------------------------------
begin
  Result:=Tdlg_LinkType_add.ExecDlg (aFolderID);
end;


// ---------------------------------------------------------------
function TdmAct_LinkType.CheckActionsEnable: Boolean;
// ---------------------------------------------------------------
begin
  Result:= dmUser_Security.Modify_lib in [mtAllow, mtAllowMy];

  SetActionsEnabled1( [
          act_Del_,
          act_Del_list,
          act_Copy,

          act_Validate

      ],

   Result );


  //  TModify_lib = (mtAllow=1, mtAllowMy=2, mtForbidden=0 );


end;



//--------------------------------------------------------------------
procedure TdmAct_LinkType.GetPopupMenu;
//-------------------------------------------------------------------
var
  bEnable: Boolean;
begin
  bEnable:=CheckActionsEnable();


  case aMenuType of
    mtFolder:  begin
                 AddFolderMenu_Create (aPopupMenu, bEnable);
                 AddFolderPopupMenu (aPopupMenu, bEnable);

                 AddMenuItem (aPopupMenu, act_Validate);

                 AddMenuItem (aPopupMenu, act_Export_MDB);

               end;
    mtItem: begin
               AddMenuItem (aPopupMenu, act_Del_);
                AddMenuItem (aPopupMenu, act_Copy);

           //   AddMenuItem (aPopupMenu, nil);
           //   AddFolderMenu_Tools (aPopupMenu);
              AddMenuItem (aPopupMenu, act_Export_MDB);
            //   AddMenuItem (aPopupMenu, act_Audit);

            end;
    mtList: begin
               AddMenuItem (aPopupMenu, act_Del_list);
               AddMenuItem (aPopupMenu, act_Export_MDB);
            end;
  end;
end;



// ---------------------------------------------------------------
procedure TdmAct_LinkType.Copy;
// ---------------------------------------------------------------
var
  iID: integer;
  sNewName: string;
begin

  sNewName:=FFocusedName +' (�����)';

  if InputQuery ('����� ������','������� ����� ��������', sNewName) then
  begin
    iID:=dmOnega_DB_data.LinkType_Copy(FFocusedID, sNewName);
//      dmAntType.Copy (FFocusedID, sNewName);

//      sGUIDg_Obj.ItemByName[OBJ_ANTENNA_TYPE].RootFolderGUID;

    g_Shell.UpdateNodeChildren_ByGUID(GUID_Link_Type);

  end;
end;


//--------------------------------------------------------------------
// actions
//--------------------------------------------------------------------
procedure TdmAct_LinkType.DoAction (Sender: TObject);
var
  k: Integer;
begin

{  if Sender=act_Audit then
     dlg_Audit (TBL_LinkType, FFocusedID) else

}

  if Sender=act_Copy then
    Copy() else


  if Sender=act_Validate then
  begin
    k:=dmOnega_DB_data.ExecStoredProc('sp_LinkType_validate',[]);
    k:=dmOnega_DB_data.ExecStoredProc('sp_Link_validate',[]);
    ShowMessage('���������.');
  end else


  if Sender=act_Export_MDB then
//    TDataExport_run.ExportToRplsDbGuides1 (OBJ_ANTENNA_TYPE);
    TDataExport_run.ExportToRplsDb_selected (TBL_LinkType, FSelectedIDList);



//  if Sender=act_Add       then Add (FFocusedID) else
//  if Sender=act_Del       then Del (FFocusedID) else
end;


begin

end.