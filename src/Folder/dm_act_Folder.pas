unit dm_act_Folder;


(*    if InputQuery('�������', '������� �������', sZoom) then
    begin
      dZoom:= AsFloat(sZoom);
*)

interface

uses
  Classes, menus, ActnList, Forms,  SysUtils,

  u_Shell_new,

  u_shell_var,

  I_Shell,

  d_Folder_Select,
  d_Folder_add,

  dm_Folder,

  u_Explorer,
  

  u_func,
//  u_func_msg,
  u_dlg,


  u_types,

  u_const_str;

  //, IFolderX

type
  TdmAct_Folder = class(TDataModule)
    ActionList1: TActionList;
    act_Folder_Add: TAction;
    act_Folder_Del: TAction;
    act_Folder_Rename: TAction;
    act_Add11: TAction;
    act_Del_11: TAction;
    act_Folder_MoveTo: TAction;
    act_Del_list: TAction;
//    procedure DataModuleDestroy(Sender: TObject);
    procedure DataModuleCreate(Sender: TObject);
//    procedure DataModuleCreate(Sender: TObject);
  private
//    FFocusedID: integer;

      procedure DoExecAction (Sender: TObject);
  private

    function Dlg_Edit(aID: integer): boolean; virtual; abstract;

  public
    procedure AddFolderPopupMenu(aPopupMenu: TPopupMenu; aSelectedPIDLs:
        TrpPIDL_List);

    procedure AddFolderMenu_Tools(aPopupMenu: TPopupMenu; aSelectedPIDLs:    TrpPIDL_List);


    function Dlg_FolderAdd(aObjectName: string; aFolderID: integer; aGUID: string): integer;
    procedure Dlg_FolderDel(aID: integer; aGUID: string);

// TODO: FolderDelByObject
//  procedure FolderDelByObject (aID: integer; aGUID: string; aObjType: TrpObjectType);

    procedure Dlg_MoveTo(aPIDLs: TrpPIDL_List);
    //FSelectedPIDLs:  TrpPIDL_List;

    procedure Dlg_FolderRename(aID: integer; aGUID: string);

    function Dlg_Folder_Select(aObjectName: string): integer; overload;


    class procedure Init;

    procedure SetAllActionsEnabled(Value: Boolean);
  end;

var
  dmAct_Folder: TdmAct_Folder;

//-------------------------------------------------------------------
// implementation
//-------------------------------------------------------------------
implementation {$R *.dfm}



//--------------------------------------------------------------------
class procedure TdmAct_Folder.Init;
//--------------------------------------------------------------------
begin
  if not Assigned(dmAct_Folder) then
    dmAct_Folder:=TdmAct_Folder.Create(Application);
end;


//-------------------------------------------------------------------
procedure TdmAct_Folder.DataModuleCreate(Sender: TObject);
//-------------------------------------------------------------------
begin

  act_Folder_Add.Caption:=STR_FOLDER_ADD_;
  act_Folder_Del.Caption:=STR_FOLDER_DEL_;
  act_Folder_Rename.Caption:=STR_FOLDER_RENAME;
  act_Folder_MoveTo.Caption:=STR_FOLDER_MOVE_TO;

  SetActionsExecuteProc
               ([act_Folder_Add,
                 act_Folder_Del,
                 act_Folder_Rename,
                 act_Folder_MoveTo
                 ],
                DoExecAction);

end;


procedure TdmAct_Folder.SetAllActionsEnabled(Value: Boolean);
begin
  SetActionsEnabled1
     ([act_Folder_Add,
       act_Folder_Del,
       act_Folder_Rename,
       act_Folder_MoveTo
       ], Value);

end;



//--------------------------------------------------------------------
procedure TdmAct_Folder.DoExecAction (Sender: TObject);
//--------------------------------------------------------------------
var
  sObjName: string;
  oSelectedPIDLs:  TrpPIDL_List;
  oPIDL: TrpPIDL;
  iFocusedID: integer;
  sFocusedGUID: string;
begin
 // if Sender=act_Add     then Add (iFocusedID) else
 // if Sender=act_Del_    then Del (iFocusedID, sFocusedGUID) else
 // if Sender=act_Del_list then Del_List () else

   if not Assigned(Sender) then
     Exit;

   oSelectedPIDLs:=TrpPIDL_List(TBasicAction(Sender).Tag);

   oPIDL:=oSelectedPIDLs[0];
   sObjName:=oPIDL.ObjectName;

   iFocusedID:=oPIDL.ID;
   sFocusedGUID:=oPIDL.GUID;


  if Sender=act_Folder_Add    then Dlg_FolderAdd (sObjName,iFocusedID, sFocusedGUID) else
  if Sender=act_Folder_Del    then Dlg_FolderDel (iFocusedID, sFocusedGUID) else
  if Sender=act_Folder_Rename then Dlg_FolderRename (iFocusedID, sFocusedGUID) else
  if Sender=act_Folder_MoveTo then Dlg_MoveTo (oSelectedPIDLs) else

//  if Sender=act_GroupAssign   then Dlg_GroupAssignValues() else

end;


//--------------------------------------------------------------------
procedure TdmAct_Folder.AddFolderMenu_Tools(aPopupMenu: TPopupMenu;
    aSelectedPIDLs: TrpPIDL_List);
//--------------------------------------------------------------------
//var oMenuItem: TMenuItem;
begin
  AddMenuDelimiter (aPopupMenu);
  dlg_AddMenuItem (aPopupMenu, act_Folder_MoveTo);

  act_Folder_MoveTo.Enabled:= act_Folder_MoveTo.Enabled and (aSelectedPIDLs.Count>0);
//  act_Folder_MoveTo.Enabled:=(FFocusedID>0);
  act_Folder_MoveTo.Tag:=integer(aSelectedPIDLs);


 // SetFormActionsEnabled(Self);


end;

//--------------------------------------------------------------------
procedure TdmAct_Folder.AddFolderPopupMenu(aPopupMenu: TPopupMenu;
    aSelectedPIDLs: TrpPIDL_List);
//--------------------------------------------------------------------

var
  sObjName: string;
  oPIDL: TrpPIDL;
  iFocusedID: integer;
  sFocusedGUID: string;
begin
 // SetFormActionsEnabled(Self, aEnabled);



 // if not Assigned(Sender) then
 //   Exit;

  // oSelectedPIDLs:=TrpPIDL_List(TBasicAction(Sender).Tag);

   oPIDL:=aSelectedPIDLs[0];
  // sObjName:=oPIDL.ObjectName;

   iFocusedID:=oPIDL.ID;
  // sFocusedGUID:=oPIDL.GUID;



  dlg_AddMenuItem (aPopupMenu, nil);
{
  oMenuItem:=dlg_AddMenuItem (aPopupMenu, nil, '�����');
    dlg_AddMenuItem (oMenuItem, act_Folder_Add);
    dlg_AddMenuItem (oMenuItem, act_Folder_Rename);
    dlg_AddMenuItem (oMenuItem, act_Folder_MoveTo);
 }
 // dlg_AddMenuItem (aPopupMenu, act_Folder_Add);
  dlg_AddMenuItem (aPopupMenu, act_Folder_Add);
  dlg_AddMenuItem (aPopupMenu, act_Folder_Rename);
  dlg_AddMenuItem (aPopupMenu, act_Folder_MoveTo);
  dlg_AddMenuItem (aPopupMenu, nil);
  dlg_AddMenuItem (aPopupMenu, act_Folder_Del);

{
  act_Folder_Del.Enabled   := act_Folder_Del.Enabled    and (iFocusedID>0);
  act_Folder_Rename.Enabled:= act_Folder_Rename.Enabled and (iFocusedID>0);
  act_Folder_MoveTo.Enabled:= act_Folder_MoveTo.Enabled and (iFocusedID>0);
}

  act_Folder_Del.Enabled   := (iFocusedID>0);
  act_Folder_Rename.Enabled:= (iFocusedID>0);
  act_Folder_MoveTo.Enabled:= (iFocusedID>0);


  act_Folder_Add.Tag   :=Integer(aSelectedPIDLs);
  act_Folder_Del.Tag   :=Integer(aSelectedPIDLs);
  act_Folder_Rename.Tag:=integer(aSelectedPIDLs);
  act_Folder_MoveTo.Tag:=integer(aSelectedPIDLs);


//  dlg_AddMenuItem (oMenuItem, dmAct_Folder.act_Folder_Add);

 // act_Add.Tag                    :=integer(FSelectedPIDLs);
 // dmAct_Folder.act_Folder_Add.Tag:=integer(FSelectedPIDLs);

end;




function TdmAct_Folder.Dlg_Folder_Select(aObjectName: string): integer;
var
  sName: string ;
begin
  Result := Tdlg_Folder_Select.ExecDlg (aObjectName,sName);
end;



//--------------------------------------------------------------------
function TdmAct_Folder.Dlg_FolderAdd(aObjectName: string; aFolderID: integer; aGUID: string): integer;
//--------------------------------------------------------------------
var sNewGUID: string;
  sParentGUID: string;
begin
  Result:=Tdlg_Folder_add.ExecDlg (aObjectName, aFolderID);

  if Result>0 then
  begin
    if aFolderID=0 then
      sParentGUID:=g_Obj.ItemByName[aObjectName].RootFolderGUID
    else
      sParentGUID:=dmFolder.GetGUIDByID(aFolderID);



   // sParentGUID:=dmFolder.GetGUIDByID(aFolderID);

    sNewGUID:=dmFolder.GetGUIDByID(Result);

    g_Shell.UpdateNodeChildren_ByGUID (sParentGUID);

//  g_Shell.UpdateNodeChildren_ByGUID (sFolderGUID);
    g_Shell.EXPAND_BY_GUID(sParentGUID);

//    g_Shell.Focu seNodeByObjName  ByObjName (ObjectName, FID);

   // g_Shell.Shell_UpdateNodeChildren_ByGUID (aGUID);

   // g_Exu_explorer_list

    if Assigned(IMainProjectExplorer) then
      IMainProjectExplorer.FocuseNodeByGUID(sNewGUID);


 //   PostUpdateNodeChildren (aGUID);
   // PostFocusNode (sNewGUID);
  end;

end;


//--------------------------------------------------------------------
procedure TdmAct_Folder.Dlg_FolderDel(aID: integer; aGUID: string);
//--------------------------------------------------------------------
begin
  if ConfirmDlg (STR_ASK_FOLDER_DEL) then
    if dmFolder.Del (aID) then
    begin
      g_ShellEvents.Shell_PostDelNode(aGUID);
      g_Shell.PostDelNode(aGUID);

    end;

end;


//--------------------------------------------------------------------
procedure TdmAct_Folder.Dlg_MoveTo(aPIDLs: TrpPIDL_List);
//--------------------------------------------------------------------
var
  i,iFolderID: integer;
  k: Integer;
  sName,sFolderGUID,sObjName: string;

begin
  if aPIDLs.Count=0 then
    Exit;

  sObjName:= aPIDLs[0].ObjectName;

  iFolderID:= Tdlg_Folder_Select.ExecDlg (sObjName,sName);
  if (iFolderID < 0) then
    Exit;

 // if aPIDLs[0].IsFolder then
 // begin
{
    k:=dmFolder.MoveToFolder (aPIDLs.Items[0].ID, iFolderID);

    if k<=0 then
      ErrorDlg('������ ��� ����������� ����� '+ aPIDLs.Items[0].name);
}

 //   Exit;

    {
    if dmFolder.FolderIsChildOf(iFolderID, aPIDLs[0].ID) then
    begin
      MsgDlg('���������� ����������� �����');
      Exit;
    end;
     }
 /// end else
{
  if (iFolderID = aPIDLs[0].ID) then
  begin
    MsgDlg('������ ����������� ����� ���� � ����');
    Exit;
  end;
 }

    with aPIDLs do
      for i:=0 to Count-1 do
        if Eq(Items[i].ObjectName, sObjName) then
        begin
          if Items[i].IsFolder then
          begin
            k:=dmFolder.MoveToFolder (aPIDLs.Items[i].ID, iFolderID);

            if k<=0 then
            begin
            //sName
               ErrorDlg(Format('������ ��� ����������� �����: %s -> %s   (%d)->(%d)', [aPIDLs.Items[i].name, sName, aPIDLs.Items[i].ID, iFolderID ]));
//               ErrorDlg(Format('������ ��� ����������� �����: %s -> %s', [aPIDLs.Items[i].name, sName]));
               exit;
            end;


          ///////  dmFolder.MoveToFolder (Items[i].ID, iFolderID);

    //        g_ShellEvents.Shell_PostDelNode(Items[i].GUID);
        ////    PostEvent (WE_EXPLORER_DELETE_NODE_BY_GUID,  [app_Par(PAR_GUID, Items[i].GUID) ]);
  //          SH_PostDelNode (Items[i].GUID);
          end else begin
            dmFolder.MoveObjectToFolder (Items[i].ObjectName, Items[i].ID, iFolderID);
  //          SH_PostDelNode (Items[i].GUID);

  //          g_ShellEvents.Shell_PostDelNode(Items[i].GUID);
           // PostEvent (WE_EXPLORER_DELETE_NODE_BY_GUID,  [app_Par(PAR_GUID, Items[i].GUID) ]);
  //
          end;

          g_ShellEvents.Shell_PostDelNode(Items[i].GUID);
          g_Shell.PostDelNode(Items[i].GUID);

        end;

  sFolderGUID:= dmFolder.GetGUIDByID (iFolderID);
  if sFolderGUID = '' then
    if g_Obj.IsObjectNameExist(sObjName) then
      sFolderGUID:= g_Obj.ItemByName[sObjName].RootFolderGUID;

  g_Shell.UpdateNodeChildren_ByGUID (sFolderGUID);
 // g_Shell.Shell_UpdateNodeChildren_ByGUID(sFolderGUID);


//  dmAct_Explorer.Shell_UpdateNodeChildren (sFolderGUID);
end;

//--------------------------------------------------------------------
procedure TdmAct_Folder.Dlg_FolderRename(aID: integer; aGUID: string);
//--------------------------------------------------------------------
var sNewName: string;
begin

//      if InputQuery('������������', '�������� [%]', sValue) then
//      begin
//        mapx_SetTransparence_0_100 (sFileName, AsInteger(sValue));
//
//        mapx_ReOpenMapByFileName (Fmap, sFileName);



  if Tdlg_Folder_Add.ExecRename (aID, sNewName) then
  begin
    g_ShellEvents.Shell_RENAME_ITEM(aGUID, sNewName);  //  dmAct_Explorer.
    g_Shell.RENAME_ITEM_(aGUID, sNewName);  //  dmAct_Explorer.

  end;

//    PostEvent (WE_EXPLORER_RENAME_ITEM,
  //        [app_Par(PAR_GUID, aGUID),
    //       app_Par(PAR_NAME, sNewName) ]);

end;




end.
//
//
//
//      if InputQuery('������������', '�������� [%]', sValue) then
//      begin
//        mapx_SetTransparence_0_100 (sFileName, AsInteger(sValue));
//
//        mapx_ReOpenMapByFileName (Fmap, sFileName);

