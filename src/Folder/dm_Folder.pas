unit dm_Folder;

interface

uses
  Windows, SysUtils, Messages,  Classes, Graphics, Controls, Forms, Dialogs, Db, ADODB, Variants,

  dm_Onega_DB_data,

  dm_Object_base,
  dm_Main,

  u_types,

  u_const_db,

  u_log,

  u_func_arrays,
  u_func,
  u_dlg,
  u_db
  ;

type

  TdmFolderAddRec = record
    ObjectName: string;
    Name      : string;
    ParentID  : integer;

    OutPut : record
               ID   : Integer;
               GUID : string;
             end;

  end;




  TdmFolder = class(TdmObject_base)
    qry_Temp: TADOQuery;
    procedure DataModuleCreate(Sender: TObject);
  private

  public
    function MoveToFolder(aID, aDest_ID: integer): Integer;

//    function  FolderIsChildOf(aFolderID, aParentFolderID: integer): boolean;

    procedure MoveObjectToFolder(aObjectName: string; aID, aNewFolderID: integer);

    function  Del (aID: integer): boolean; overload;

// TODO: DelByObject1
//  function DelByObject1(aID: integer; aObjType: TrpObjectType): boolean;

    function Add (aRec: TdmFolderAddRec): integer; overload;
    function AddEx(aObjectName,aName: string; aParentID: integer): integer; overload;

    function GetFolderGUIDList(aObjectType: TrpObjectType; aID: Integer): TStrArray;

  end;

function dmFolder: TdmFolder;

//==================================================================
implementation {$R *.DFM}
//==================================================================

var
  FdmFolder: TdmFolder;


// ---------------------------------------------------------------
function dmFolder: TdmFolder;
// ---------------------------------------------------------------
begin
  if not Assigned(FdmFolder) then
      FdmFolder := TdmFolder.Create(Application);         

  Result:=FdmFolder;
end;

// ----------------------------------------------------------------
procedure TdmFolder.DataModuleCreate(Sender: TObject);
// ----------------------------------------------------------------
begin
  inherited;

 // DisplayName:=STR_FOLDER;
  TableName:=TBL_FOLDER;
end;


// ---------------------------------------------------------------
function TdmFolder.GetFolderGUIDList(aObjectType: TrpObjectType; aID: Integer):
    TStrArray;
//-------------------------------------------------------------------

var
  sTableName: string;
  iFolderID: Integer;
  iResult: Integer;
  sGUID: string;

  oSList: TStringList;
begin
  oSList:=TStringList.Create;

  sTableName:= g_Obj.ItemByType[aObjectType].TableName;

  iResult := dmOnega_DB_data.OpenStoredProc(ADOStoredProc1,
              'sp_Folder_GetParents_for_object',
               [FLD_TABLENAME, sTableName,
                FLD_ID, aID
               ] );

 // SetLength(Result, 0);

// Folder_GetParents(ADOStoredProc1, iFolderID) ;//aID);

  if iResult>0 then
 // begin
  //  dmOnega_DB_data.Folder_GetParents(ADOStoredProc1, iFolderID) ;//aID);

    while not ADOStoredProc1.EOF do
    begin
      sGUID := ADOStoredProc1.FieldByName(FLD_GUID).AsString;

   //   if sGUID<>'' then
        oSList.Add(sGUID);


    //  if sGUID<>'' then
     //  StrArrayAddValue(Result, sGUID);
      ADOStoredProc1.Next;
    end;
//  end;



//  SetLength(Result, 0);

(*
  if gl_DB.FieldExist(sTableName, FLD_FOLDER_ID) then
  begin
    iFolderID:= gl_DB.GetIntFieldValueByID(sTableName, FLD_FOLDER_ID, aID);

    if iFolderID>0 then
    begin
      dmOnega_DB_data.Folder_GetParents(ADOStoredProc1, iFolderID) ;//aID);

      while not ADOStoredProc1.EOF do
      begin
        sGUID := ADOStoredProc1.FieldByName(FLD_GUID).AsString;

        if sGUID<>'' then
          oSList.Add(sGUID);


        if sGUID<>'' then
          StrArrayAddValue(Result, sGUID);
        ADOStoredProc1.Next;
      end;
    end;
  end;
*)





  oSList.Add(g_Obj.ItemByType[aObjectType].RootFolderGUID);

 // StrArrayAddValue(Result, g_Obj.ItemByType[aObjectType].RootFolderGUID);

  if FoundInProjectTypes(aObjectType) then
    oSList.Add(GUID_PROJECT)
   // StrArrayAddValue(Result, GUID_PROJECT);

  else
    if FoundInDictTypes(aObjectType) then
      oSList.Add(GUID_DICTS) ;
   //   StrArrayAddValue(Result, GUID_DICTS);


  Result := u_func.StringListToStrArray(oSList);


  FreeAndNil(oSList);

end;




//-------------------------------------------------------------------
function TdmFolder.Del (aID: integer): boolean;
//-------------------------------------------------------------------
var
  i: Integer;
begin
  i:=dmOnega_DB_data.ExecStoredProc_ ('sp_Folder_Del', [FLD_ID, aID]);
  Result := True;

end;


//--------------------------------------------------------------------
function TdmFolder.Add (aRec: TdmFolderAddRec): integer;
//--------------------------------------------------------------------
begin
  Result:=dmOnega_DB_data.ExecStoredProc_('sp_Folder_Add',
                [
                 FLD_ObjName,    aRec.ObjectName,
                 FLD_NAME,       aRec.Name,
                 FLD_PARENT_ID,  aRec.ParentID,
                 FLD_PROJECT_ID, dmMain.ProjectID
                 ]);

end;

//--------------------------------------------------------------------
function TdmFolder.AddEx(aObjectName,aName: string; aParentID: integer): integer;
//--------------------------------------------------------------------
var
  rec: TdmFolderAddRec;
begin
  FillChar (rec, SizeOf(rec), 0);

  rec.Name:=aName;
  rec.ObjectName:=aObjectName;
  rec.ParentID:=aParentID;

  Result:=Add (rec);

end;

// -------------------------------------------------------------------
procedure TdmFolder.MoveObjectToFolder(aObjectName: string; aID, aNewFolderID:
    integer);
// -------------------------------------------------------------------
var 
  k: Integer;
begin
  k:=dmOnega_DB_data.ExecStoredProc_ ('sp_Folder_MoveObjectToFolder',
          [FLD_ObjName,   aObjectName,
           FLD_ID,        aID,
           FLD_FOLDER_ID, aNewFolderID
          ]);
end;



function TdmFolder.MoveToFolder(aID, aDest_ID: integer): Integer;
begin
  Result:=dmOnega_DB_data.ExecStoredProc_ ('sp_Folder_MoveToFolder',
          [FLD_ID, aID,
           FLD_DEST_ID, aDest_ID
          ]);

end;


{

//----------------------------------------------------------------
function  TdmFolder.FolderIsChildOf(aFolderID, aParentFolderID: integer): boolean;
//----------------------------------------------------------------
var
  i: Integer;
begin
  i:=dmOnega_DB_data.Folder_GetParents(ADOStoredProc1, aParentFolderID);

  Result:=ADOStoredProc1.Locate(FLD_ID, aFolderID, []);
end;

}

end.


