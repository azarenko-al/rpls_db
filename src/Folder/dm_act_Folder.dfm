object dmAct_Folder: TdmAct_Folder
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Left = 593
  Top = 369
  Height = 232
  Width = 449
  object ActionList1: TActionList
    Left = 40
    Top = 20
    object act_Folder_Add: TAction
      Category = 'common Folder'
      Caption = 'act_FolderDel'
    end
    object act_Folder_Del: TAction
      Category = 'common Folder'
      Caption = 'act_FolderAdd'
    end
    object act_Folder_Rename: TAction
      Category = 'common Folder'
      Caption = 'act_Folder_Rename'
    end
    object act_Add11: TAction
      Category = 'common'
      Caption = 'act_Add11'
    end
    object act_Del_11: TAction
      Category = 'common'
      Caption = 'act_Del_11'
    end
    object act_Folder_MoveTo: TAction
      Category = 'common Folder'
      Caption = 'act_Folder_MoveTo'
    end
    object act_Del_list: TAction
      Caption = 'act_Del_list'
    end
  end
end
