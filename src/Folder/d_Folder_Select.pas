unit d_Folder_Select;

 interface

 uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, ImgList, cxInplaceContainer, cxTL, cxDBTL, DB, cxControls,
  cxPropertiesStore, rxPlacemnt, ActnList, StdCtrls,

  dm_Onega_DB_data,

  dm_Main,

  u_db,

  d_Wizard,

  u_func,
  u_types,
  u_dlg,

  cxTLData, ADODB;


 type
  Tdlg_Folder_Select = class(Tdlg_Wizard)
    pn_Explorer: TPanel;
    ADOStoredProc1: TADOStoredProc;
    cxDBTreeList1: TcxDBTreeList;
    DataSource1: TDataSource;
    cxDBTreeList1cxDBTreeListColumn1: TcxDBTreeListColumn;
    ImageList1: TImageList;
    procedure FormCreate(Sender: TObject);
  private
  public
    class function ExecDlg(aObjectName: string; var aName: string): integer;


  end;


//==================================================================
implementation {$R *.DFM}
//==================================================================

//-------------------------------------------------------------
class function Tdlg_Folder_Select.ExecDlg(aObjectName: string; var aName:
    string): integer;
//-------------------------------------------------------------
var
  k: Integer;
begin
  with Tdlg_Folder_Select.Create(Application) do
  try
    k:=dmOnega_DB_data.OpenStoredProc(ADOStoredProc1, 'sp_Folder_Select_Tree',
          [
           FLD_ObjName,    aObjectName,
           FLD_Project_ID, dmMain.ProjectID
           ]);



      //     db_View(ADOStoredProc1);


//     cxDBTreeList1.FullExpand;
//     if cxDBTreeList1.Nodes.Count>0 then
     if cxDBTreeList1.Count>0 then
       cxDBTreeList1.Items[0].Expand(False);


    if ShowModal=mrOk then begin
       Result := ADOStoredProc1.FieldByName(FLD_ID).AsInteger;
       aName  := ADOStoredProc1.FieldByName(FLD_Name).AsString;

    end else
       Result := -1;

  finally
    Free;
  end;
end;


// -------------------------------------------------------------------
procedure Tdlg_Folder_Select.FormCreate(Sender: TObject);
// -------------------------------------------------------------------
begin
  inherited;
  Caption := '����� ��������';
  SetActionName('����� ��������');

  pn_Explorer.Align:=alClient;

  cxDBTreeList1.Align :=alclient;

  cxDBTreeList1.DataController.ImageIndexField:=FLD_image_index;

end;

end.



(*
  Result:=ExecDlg (g_Obj.ItemByName[aObjectName].Name,  sName,sGUID);
end;
*)

(*//-------------------------------------------------------------
class function Tdlg_Folder_Select.ExecDlg (aObjectType: TrpObjectType;
                                         var aName,aGUID: string): integer;
//-------------------------------------------------------------
begin
  Result:=ExecDlg (g_Obj.ItemByType[aObjectType].Name, aName,aGUID);
end;


//-------------------------------------------------------------
class function Tdlg_Folder_Select.ExecDlg (aObjectName: string;
                                         var aName,aGUID: string): integer;
//-------------------------------------------------------------
begin
  with Tdlg_Folder_Select.Create(Application) do
  try
  //  Fframe_Explorer.SetViewObject(aObjectName);
   // Fframe_Explorer.Load;

    if ShowModal=mrOk then begin
       Result := FObjectID;
       aName:=FName;
    end else
       Result := -1;

  finally
    Free;
  end;

end;

*)


(*    class function ExecDlg (aObjectType: TrpObjectType;
                            var aName,aGUID: string): integer; overload;
    class function ExecDlg (aObjectName: string;
                            var aName,aGUID: string): integer; overload;
*)