unit d_Folder_add;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ActnList, StdCtrls, ExtCtrls, cxPropertiesStore, rxPlacemnt,

  d_Wizard_Add,
  dm_Main,

  dm_Folder,

  u_types,
  u_const_db,

  u_const_str, cxLookAndFeels ;

type
  Tdlg_Folder_add = class(Tdlg_Wizard_add)
    procedure FormCreate(Sender: TObject);
    procedure act_OkExecute(Sender: TObject);
    procedure ActionList1Update(Action: TBasicAction; var Handled: Boolean);

  private
    FExecAction: (atAdd, atRename);

    FRec: TdmFolderAddRec;

    FName,FObjectName: string;
    FID,FParentID: integer;

    procedure DoAppend;
    procedure DoRename;
  public
    class function ExecRename(aID: integer; var aNewName: string): boolean;

    class function ExecDlg(aObjectName: string; aParentID:
        integer): integer;
  end ;


implementation
{$R *.DFM}



//--------------------------------------------------------------------
class function Tdlg_Folder_add.ExecRename(aID: integer; var aNewName: string):
    boolean;
//--------------------------------------------------------------------
begin
  with Tdlg_Folder_add.Create(Application) do
  begin
    FExecAction:=atRename;

    FID:=aID;
    FName:=dmFolder.GetNameByID (aID);
    ed_Name_.Text:=FName;

    Result:=(ShowModal=mrOk);

    aNewName:=ed_Name_.Text;

  {  if Result then
      g_ShellEvents.Shell_RENAME_ITEM(aGUID, sNewName);  //  dmAct_Explorer.
}

{    if Result then
      PostEvent (WE_EXPLORER_RENAME_ITEM,
            [app_Par(PAR_GUID, aGUID),
             app_Par(PAR_NAME, sNewName) ]);
}

{
//--------------------------------------------------------------------
procedure TdmAct_Folder.Dlg_FolderRename(aID: integer; aGUID: string);
//--------------------------------------------------------------------
var sNewName: string;
begin
  if Tdlg_Folder_Add.Rename (aID, sNewName) then
    PostEvent (WE_EXPLORER_RENAME_ITEM,
          [app_Par(PAR_GUID, aGUID),
           app_Par(PAR_NAME, sNewName) ]);
end;}

   // Result:=FID;

    Free;
  end;    // with
end;


//--------------------------------------------------------------------
class function Tdlg_Folder_add.ExecDlg(aObjectName: string; aParentID:
    integer): integer;
//--------------------------------------------------------------------
begin
  if aObjectName='' then
    raise Exception.Create('');


  with Tdlg_Folder_add.Create(Application) do
  begin
    FExecAction:=atAdd;

    FObjectName:=aObjectName;
    FRec.ParentID:=aParentID;

    ed_Name_.Text:='�����';

    if ShowModal=mrOk then begin
      Result:=FID;
    //  aGUID:=FGUID;

    end else
      Result:=0;

    Free;
  end;    // with
end;


//--------------------------------------------------------------------
procedure Tdlg_Folder_add.FormCreate(Sender: TObject);
//--------------------------------------------------------------------
begin
  inherited;

  Caption:=DLG_CAPTION_ADD_FOLDER;
  SetActionName (DLG_CAPTION_ADD_FOLDER);

  SetDefaultSize();
 // AlignTop();
end;


procedure Tdlg_Folder_add.act_OkExecute(Sender: TObject);
begin
  inherited;
  case FExecAction of
    atAdd:    DoAppend;
    atRename: DoRename;
  end;

end;

//-------------------------------------------------------------------
procedure Tdlg_Folder_add.DoAppend;
//-------------------------------------------------------------------
//var
 // bUseProjectID: boolean;

begin

  FRec.ObjectName:=FObjectName;
  FRec.Name      :=ed_Name_.Text;
//  rec.ParentID  :=FParentID;
 // rec.Flags     :=setFlags;

  FID:=dmFolder.Add (FRec); //FObjectName, ed_Name_.Text,
                         //FParentID, setFlags);

 // FGUID:=dmFolder.GetGUIDByID(FID);
end;


//-------------------------------------------------------------------
procedure Tdlg_Folder_add.DoRename;
//-------------------------------------------------------------------
begin
  dmFolder.Rename (FID, ed_Name_.Text);
end;


procedure Tdlg_Folder_add.ActionList1Update(Action: TBasicAction;  var Handled: Boolean);
begin
  inherited;

  if FExecAction=atRename then
    act_Ok.Enabled:=(ed_Name_.Text<>FName);
end;


end.


//      if InputQuery('������������', '�������� [%]', sValue) then
//      begin
//        mapx_SetTransparence_0_100 (sFileName, AsInteger(sValue));
//
//        mapx_ReOpenMapByFileName (Fmap, sFileName);

