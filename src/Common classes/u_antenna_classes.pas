unit u_antenna_classes;

interface
uses
  DB, Classes, SysUtils,

  u_func,
  u_geo,
  u_const_db
  ;


type
  TDBAntennaPolarizationType = (ptV,ptH,ptX);


  TDBAntenna = class(TCollectionItem)
  private
    Band : string;

    function GetPolarizationType: TDBAntennaPolarizationType;
  public
    Id                  : Integer;

    BLPoint: TBLPoint;

//    Lat                 : Double;
 //   Lon                 : Double;

//    Polarization        : Integer;
    Polarization_str    : string;

    Diameter            : Double;
    Gain                : Double;
    Vert_width          : Double;
    Loss             : Double;
    Height              : Double;
    Fronttobackratio    : Double;

    function GetPolarization: Integer;
    procedure LoadFromDataset(aDataset: TDataset);

    property PolarizationType: TDBAntennaPolarizationType read GetPolarizationType;
  end;


  TDBAntennaList = class(TCollection)
  private
    function GetItems(Index: Integer): TDBAntenna;
  public
    constructor Create;
    function AddItem: TDBAntenna;

    procedure LoadFromDataset(aDataset: TDataset);

    property Items[Index: Integer]: TDBAntenna read GetItems; default;
  end;



implementation


function TDBAntenna.GetPolarizationType: TDBAntennaPolarizationType;
begin
  if Eq(Polarization_str,'h') then
    Result:=ptH else
  if Eq(Polarization_str,'v') then
    Result:=ptV
  else
    Result:=ptX;


(*  case Polarization_str of
    0: Result:=ptH;
    1: Result:=ptV;
    2: Result:=ptX;
  else
     Result:=ptH;
  end;
*)
end;


function TDBAntenna.GetPolarization: Integer;
var
  s: string;
begin
 //TTX.POLARIZATION, '��� �����������, 0-��������������,1-������������,2-�����');
  s := LowerCase(Polarization_str);

  if s='h' then  Result := 0 else
  if s='v' then  Result := 1 else Result := 2;

end;


procedure TDBAntenna.LoadFromDataset(aDataset: TDataset);
begin

  with aDataset do
  begin
    Id                  :=FieldByName(FLD_Id).AsInteger;

    BLPoint.B           :=FieldByName(FLD_Lat).AsFloat;
    BLPoint.L           :=FieldByName(FLD_Lon).AsFloat;

    Polarization_str    :=LowerCase(FieldByName(FLD_Polarization_str).AsString);
    Diameter            :=FieldByName(FLD_Diameter).AsFloat;
    Gain                :=FieldByName(FLD_Gain).AsFloat;
    Vert_width          :=FieldByName(FLD_Vert_width).AsFloat;
    Loss             :=FieldByName(FLD_Loss).AsFloat;
    Height              :=FieldByName(FLD_Height).AsFloat;
    FronttoBackratio    :=FieldByName(FLD_Fronttobackratio).AsFloat;

/////////    Band := FieldByName(FLD_BAND).AsString;
  end;
end;

constructor TDBAntennaList.Create;
begin
  inherited Create(TDBAntenna);
end;

function TDBAntennaList.AddItem: TDBAntenna;
begin
  Result := TDBAntenna (inherited Add);
end;

function TDBAntennaList.GetItems(Index: Integer): TDBAntenna;
begin
  Result := TDBAntenna(inherited Items[Index]);
end;

procedure TDBAntennaList.LoadFromDataset(aDataset: TDataset);
begin
  Clear;

  aDataset.First;
  while not aDataset.EOF do
  begin
    AddItem.LoadFromDataset(aDataset);
    aDataset.Next;
  end;
end;


end.
