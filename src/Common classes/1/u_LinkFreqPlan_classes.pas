unit u_LinkFreqPlan_classes;

interface

uses DB, Classes,

   u_LinkFreqPlan_const,
   u_const_db;

type

  //-------------------------------------------------------------------
  TnbAntenna = class(TCollectionItem)
  //-------------------------------------------------------------------
  public
    Gain_dB           :  double;
    Diameter          :  double;

    Polarization_str  : string;

    Vert_Width        :  double;
    Horz_Width        :  double;
    Freq_MHz          :  double;

    Polarization_ratio: double;

    VertMask          : string;
    HorzMask          : string;

//    HorzMaskArr,
//    VertMaskArr: TdmAntTypeMaskArray;

  public
    procedure LoadFromDataset(aDataset: TDataset);
  end;

  // ---------------------------------------------------------------
  TDBNeighbor_ = class(TCollectionItem)
  // ---------------------------------------------------------------
  public
    TX_LINKEND_ID : Integer;
    RX_LINK_ID : Integer;


  //zzzzzz    bandwidth1: Double;
    coeff_noise  : Double;

    IsLink       : Boolean;
    IsPropertyOne : Boolean;

//    RX_LINK_ID : Integer;
    POWER_dBm        : Double;
    DIAGRAM_LEVEL: Double;
    EXTRA_LOSS   : Double;

    IsNeigh : Boolean;
    CI : Double;

    POWER_NOISE : Double;
    FREQ_SPACING_MHz : Double;

    Threshold_degradation1: Double;

    // -------------------------


    procedure LoadFromDataset(aDataset: TDataset);
  end;

  // ---------------------------------------------------------------
  TDBLinkFreqPlan_ = class
  // ---------------------------------------------------------------
  public
    Id                  : Integer;
    Name                : String;
//    Linknet_id          : Integer;

    MaxCalcDistance_km: Double;
    PolarizationLevel   : Double;
    SectorFreqSpacing   : Integer;
    SiteFreqSpacing     : Integer;
    MIN_CI_dB                  : Double;
    StartSectorNum      : Integer;
    PereborQueueType    : Integer;
    FreqDistribType     : Integer;
    MaxIterCount        : Integer;
    PereborOrderType    : Integer;
    MaxFreqRepeat       : Integer;
    Interference_ChannelsKind: string;
    Threshold_degradation: Double;

    procedure LoadFromDataset(aDataset: TDataset);
  end;

  // ---------------------------------------------------------------
  TDBLinkFreqPlan_Linkend_ = class(TCollectionItem)
  // ---------------------------------------------------------------
  public
    Linkend_id : Integer;
    THRESHOLD_DEGRADATION : Double;

    Link_id : Integer;
    Link_Name : string;

    BANDWIDTH : Double;

    BANDWIDTH_TX_30 : Double;
    BANDWIDTH_TX_3  : Double;
    BANDWIDTH_TX_A  : Double;

    BANDWIDTH_RX_30 : Double;
    BANDWIDTH_RX_3  : Double;
    BANDWIDTH_RX_A  : Double;

    LINKENDTYPE_ID: Integer;

    ClutterModel_ID : Integer;

    procedure LoadFromDataset(aDataset: TDataset);
    procedure LoadFromDataset_new(aDataset: TDataset);
  end;


  TDLinkFreqPlan_Linkend_List = class(TCollection)
  private
    function Add: TDBLinkFreqPlan_Linkend_;
    function GetItems(Index: Integer): TDBLinkFreqPlan_Linkend_;
  public
    constructor Create;

    procedure LoadFromDataset(aDataset: TDataset);
    procedure LoadFromDataset_new(aDataset: TDataset);

    property Items[Index: Integer]: TDBLinkFreqPlan_Linkend_ read GetItems; default;
  end;


  TDBNeighbor_List = class(TCollection)
  private
    function Add: TDBNeighbor_;
    function GetItems(Index: Integer): TDBNeighbor_;
  public
    constructor Create;
    procedure LoadFromDataset(aDataset: TDataset);
    property Items[Index: Integer]: TDBNeighbor_ read GetItems; default;
  end;


  TnbAntennaList = class(TCollection)
  private
    function Add: TnbAntenna;
    function GetItems(Index: Integer): TnbAntenna;
  public
    constructor Create;
    procedure LoadFromDataset(aDataset: TDataset);
    property Items[Index: Integer]: TnbAntenna read GetItems; default;
  end;


implementation


procedure TDBNeighbor_.LoadFromDataset(aDataset: TDataset);
begin
  with aDataset do
  begin
    TX_LINKEND_ID:=FieldByName(FLD_TX_LINKEND_ID).AsInteger;
    RX_LINKEND_ID:=FieldByName(FLD_RX_LINKEND_ID).AsInteger;

    IsLink       :=FieldByName(FLD_IsLink).AsBoolean;
    IsPropertyOne:=FieldByName(FLD_IsPropertyOne).AsBoolean;

//    RX_LINK_ID   :=FieldValues[FLD_RX_LINK_ID];

//////////    bandwidth1  :=FieldByName(FLD_bandwidth).AsFloat;
    coeff_noise:=FieldByName(FLD_coeff_noise).AsFloat;

    POWER_dBm    :=FieldByName(FLD_POWER_dBm).AsFloat;
    DIAGRAM_LEVEL:=FieldByName(FLD_DIAGRAM_LEVEL).AsFloat;
    EXTRA_LOSS   :=FieldByName(FLD_EXTRA_LOSS).AsFloat;

  end;
end;


procedure TDBLinkFreqPlan_Linkend_.LoadFromDataset(aDataset: TDataset);
begin
  with aDataset do
  begin
    Linkend_id :=FieldByName(FLD_Linkend_id).AsInteger;

  //  LINKENDTYPE_ID:=FieldByName(FLD_LINKENDTYPE_ID).AsInteger;

(*

*)

//    THRESHOLD_DEGRADATION
  end;
end;

 // ---------------------------------------------------------------
procedure TDBLinkFreqPlan_Linkend_.LoadFromDataset_new(aDataset: TDataset);
// ---------------------------------------------------------------
begin
  with aDataset do
  begin
    Linkend_id :=FieldByName(FLD_Linkend_id).AsInteger;

    LINKENDTYPE_ID :=FieldByName(FLD_LINKENDTYPE_ID).AsInteger;

    Link_id    :=FieldByName(FLD_Link_id).AsInteger;
    Link_Name  :=FieldByName(FLD_Link_Name).AsString;

    ClutterModel_ID:= FieldByName(FLD_CLUTTER_MODEL_ID).AsInteger;

    BANDWIDTH :=FieldByName(FLD_BANDWIDTH).AsFloat;

    BANDWIDTH_TX_30 :=FieldByName(FLD_BANDWIDTH_TX_30).AsFloat;
    BANDWIDTH_TX_3  :=FieldByName(FLD_BANDWIDTH_TX_3).AsFloat;
    BANDWIDTH_TX_A  :=FieldByName(FLD_BANDWIDTH_TX_A).AsFloat;


    BANDWIDTH_RX_30 :=FieldByName(FLD_BANDWIDTH_RX_30).AsFloat;
    BANDWIDTH_RX_3  :=FieldByName(FLD_BANDWIDTH_RX_3).AsFloat;
    BANDWIDTH_RX_A  :=FieldByName(FLD_BANDWIDTH_RX_A).AsFloat;

  end;
end;


constructor TDLinkFreqPlan_Linkend_List.Create;
begin
  inherited Create(TDBLinkFreqPlan_Linkend_);
end;

function TDLinkFreqPlan_Linkend_List.Add: TDBLinkFreqPlan_Linkend_;
begin
  Result := TDBLinkFreqPlan_Linkend_ (inherited Add);
end;

function TDLinkFreqPlan_Linkend_List.GetItems(Index: Integer):
    TDBLinkFreqPlan_Linkend_;
begin
  Result := TDBLinkFreqPlan_Linkend_(inherited Items[Index]);
end;

constructor TDBNeighbor_List.Create;
begin
  inherited Create(TDBNeighbor_);
end;

function TDBNeighbor_List.Add: TDBNeighbor_;
begin
  Result := TDBNeighbor_ (inherited Add);
end;

function TDBNeighbor_List.GetItems(Index: Integer): TDBNeighbor_;
begin
  Result := TDBNeighbor_(inherited Items[Index]);
end;
       

procedure TDBLinkFreqPlan_.LoadFromDataset(aDataset: TDataset);
begin
  with aDataset do
  begin
    Id                  :=FieldByName(FLD_Id).AsInteger;
    Self.Name           :=FieldByName(FLD_Name).AsString;
  //  Linknet_id          :=FieldByName(FLD_Linknet_id).AsInteger;

    MaxCalcDistance_km  :=FieldByName(FLD_MaxCalcDistance_KM).AsFloat;
    PolarizationLevel   :=FieldByName(FLD_PolarizationLevel).AsFloat;
    SectorFreqSpacing   :=FieldByName(FLD_SectorFreqSpacing).AsInteger;
    SiteFreqSpacing     :=FieldByName(FLD_SiteFreqSpacing).AsInteger;
    MIN_CI_dB            :=FieldByName(FLD_MIN_CI_dB).AsFloat;
    StartSectorNum      :=FieldByName(FLD_StartSectorNum).AsInteger;
    PereborQueueType    :=FieldByName(FLD_PereborQueueType).AsInteger;
    FreqDistribType     :=FieldByName(FLD_FreqDistribType).AsInteger;
    MaxIterCount        :=FieldByName(FLD_MaxIterCount).AsInteger;
    PereborOrderType    :=FieldByName(FLD_PereborOrderType).AsInteger;
    MaxFreqRepeat       :=FieldByName(FLD_MaxFreqRepeat).AsInteger;
    Interference_ChannelsKind:=FieldByName(FLD_Interference_ChannelsKind).AsString;
    Threshold_degradation:=FieldByName(FLD_Threshold_degradation).AsFloat;

  end;
end;

procedure TDLinkFreqPlan_Linkend_List.LoadFromDataset(aDataset: TDataset);
begin
  Clear;
  aDataset.First;
  while not aDataset.EOF do
  begin
    Add.LoadFromDataset(aDataset);
    aDataset.Next;
  end;
end;

procedure TDLinkFreqPlan_Linkend_List.LoadFromDataset_new(aDataset: TDataset);
begin
  Clear;
  aDataset.First;
  while not aDataset.EOF do
  begin
    Add.LoadFromDataset_new(aDataset);
    aDataset.Next;
  end;
end;


procedure TDBNeighbor_List.LoadFromDataset(aDataset: TDataset);
begin
  Clear;
  aDataset.First;
  while not aDataset.EOF do
  begin
    Add.LoadFromDataset(aDataset);
    aDataset.Next;
  end;
end;


//-------------------------------------------------------------------
procedure TnbAntenna.LoadFromDataset(aDataset: TDataset);
//-------------------------------------------------------------------
begin

  with aDataset do
    if not IsEmpty then
  begin
    Gain_dB           :=FieldByName(FLD_GAIN).AsFloat;

(*    Freq_MHz          :=FieldByName(FLD_FREQ_MHZ).AsFloat;
    Diameter          :=FieldByName(FLD_Diameter).AsFloat;
  //  aRec.Polarization      :=FieldByName(FLD_POLARIZATION).AsInteger;

    Polarization_str  :=FieldByName(FLD_POLARIZATION_str).Asstring;
    POLARIZATION_RATIO:=FieldByName(FLD_POLARIZATION_RATIO).AsFloat;

    Vert_Width        :=FieldByName(FLD_Vert_Width).AsFloat;
    Horz_Width        :=FieldByName(FLD_Horz_Width).AsFloat;

    VertMask          :=FieldByName(FLD_vert_mask).AsString;
    HorzMask          :=FieldByName(FLD_horz_mask).AsString;
*)

//    Mask_StrToArr (aRec.HorzMask, aRec.HorzMaskArr);
//    Mask_StrToArr (aRec.VertMask, aRec.VertMaskArr);

  end;
end;

constructor TnbAntennaList.Create;
begin
  inherited Create(TnbAntenna);
end;

function TnbAntennaList.Add: TnbAntenna;
begin
  Result := TnbAntenna (inherited Add);
end;

function TnbAntennaList.GetItems(Index: Integer): TnbAntenna;
begin
  Result := TnbAntenna(inherited Items[Index]);
end;

procedure TnbAntennaList.LoadFromDataset(aDataset: TDataset);
begin
  Clear;
  aDataset.First;
  while not aDataset.EOF do
  begin
    Add.LoadFromDataset(aDataset);
    aDataset.Next;
  end;
end;


end.


