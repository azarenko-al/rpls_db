unit u_link_classes;

interface

uses DB, Classes, SysUtils,

     u_Link_const,
     u_const_db,

     u_func,
     u_Geo,

     u_LinkEnd_classes;


type
  TDBLinkReflectionPointList = class;

  //status
  TDBLinkWorkType = (lwtNotDefined,lwtWork,lwtNoWork);

  TDBLinkType = (ltLink,ltPmpLink);


  TDBLink = class
  private
    function Get_Tx_Freq_Ghz: double;
    function GetLength_km: double;
    function GetFRENEL_MAX_m: double;
    function GetSPACE_SPREAD_m: double;
  public
    LinkType: TDBLinkType;

    LinkEnd1_ID: Integer;
    LinkEnd2_ID: Integer;

    PmpSector_ID: Integer;
    PmpTerminal_ID: Integer;

    PropertyID1: Integer;
    PropertyID2: Integer;


    Profile_XML: string;


    { TODO -oalex -c :  STATUS Integer; 30.05.2010 14:28:22 }
    STATUS : Integer;

    BLVector   :  TBLVector;

    calc_method : Integer; // 0 "����, ITU-R � 16 ����� ��" (�� ���������)
				                   // 1 "���� � 53363 - 2009"

    ClutterModel_ID: Integer;

//    Is_profile_reversed : Boolean;

    Profile_Step_m: Integer;

    Tx_Freq_MHz : Double;

    Bitrate_Mbps: Double;
    BAND: string;

    EsrNorm  : double;
    BberNorm : Double;
    GSTLength: Double;

    LinkEndID1: Integer;
    LinkEndID2: integer;

    SESR_NORM : Double;
    SESR  : Double;

    ESR_REQUIRED : Double;
    BBER_REQUIRED : Double;
    ESR_NORM : Double;
    BBER_NORM : Double;

    KNG : Double;
    KNG_YEAR : Double;
    KNG_NORM : Double;

    FADE_MARGIN_DB : Double;

    Rx_Level_dBm: Double;

//    SPACE_SPREAD_m : Double;  //���������������� ������

    // ����� -----------------------------
    Rains: record
              IsCALCLENGTH : Boolean;
              rain_intensity_extra : Double;
          //    RAIN_RATE : Double;
              RAIN_LENGTH_km : Double;

              SIGNAL_QUALITY : string;

           end;


//    Polarization : (ptH,ptV,ptX);
    //string;

    BER_REQUIRED : Integer; //IIF((iBer_Required=0), STR_BER3 , STR_BER6);

    LOS_STATUS : Integer;

//    Rain_INTENSITY1 : Double;

//    WEAKNESS : Double;  //���������� ���������� (��)

    rain_signal_depression_dB : Double;  //���������� ���������� (��)


    TILT : Double; ////������ ����� ������ ���������(����)

    rain_WEAKENING_VERT : Double;
    rain_WEAKENING_HORZ : Double;
    rain_ALLOWABLE_INTENSE_VERT : Double;
    rain_ALLOWABLE_INTENSE_HORZ : Double;

    MARGIN_HEIGHT : Double;
    MARGIN_DISTANCE_km: Double;
    SESR_SUBREFRACTION: Double;
    SESR_RAIN: Double;
    SESR_INTERFERENCE: Double;



//    FRENEL_MAX_m : Double;

 //   FRENEL_MAX_m : Double;

    Length_m : Double;
       // eFreq
    NFrenel: Double;



{    := qry_Link.FieldByName(FLD_INTENSITY).AsFloat;
    if (rain_intense = 0) or (rain_intense = NULL) then
      rain_intense:= qry_Link.FieldByName(FLD_RAIN_RATE).AsFloat;

    //  rain_intense:= gl_DB.GetFieldValueByID(TBL_LINK, FLD_RAIN_RATE, aID);

    dmLink.GetLinkEndID(aLinkID, iLinkEndID1, iLinkEndID2);

    iPolarization:=gl_DB.GetIntFieldValueByID (VIEW_LINKEND_ANTENNAS, FLD_POLARIZATION, iLinkEndID1);
    polarization:=IIF (iPolarization=0, ptH, ptV);
}


    //TRB: array[1..4] of double;  //���������� � ����������� �������� ���
     {1=0.012      ;����������� �������� SESR ��� ����, %
      2=0.05       ;����������� �������� ���  ��� ����, %
      3=600        ;����� ����, ��
      4=0.200      ;���������� ����� ���, ������������� �������������}
  ///    vGroup:=xml_AddNodeTag (aRoot, 'TRB');

    Trb: record
      GST_TYPE : Integer;

      GST_LENGTH : Double;
      GST_SESR    : Double; //   '����������� �������� SESR ��� ����, %');
      GST_Kng     : Double; //    '����������� �������� ���  ��� ����, %');
      KNG_dop     : Double; //  '���������� ����� ���, ������������� �������������}');
      SPACE_LIMIT : double; // '���������� ������������� ������� p(a%)');
      SPACE_LIMIT_PROBABILITY: Double; //'���������� ����������� a% ������������� ������������ �������� p(a%) [%]');
   end;
    //       Length_km:=dmLink_Calc_SESR.GetRoundedLength_KM(qry_Link);
      //      aRec.Trb.Length      :=dmLink_Calc_SESR.GetRoundedLengthByID(FLinkID);  //               , '����� ����, ��');
      //   end;

    RRV: record
      gradient_diel: Double;        //'������� �������� ��������� ����.�������������,10^-8 1/�' );
      gradient_deviation : Double;  //'����������� ���������� ���������,10^-8 1/�' );

      underlying_terrain_type: Double; //'��� ������������ �������.(1...3,0->����.�����=����.���.)' );
      Terrain_type:   Integer;//'��� ���������(0...2) 1-���������� 2-���������� (��������) 3-���������� (������)' );

      Steam_wet:      Double; //  '���������� ��������� �������� ����, �/���.�');
      Q_factor:       Double; //  'Q-������ ������ �����������');
      climate_factor: double; //  '������������� ������ K��, 10^-6');
      factor_B:       Double; //  '�������� ������������ b ��� ������� ������');
      factor_C:       Double; //  '�������� ������������ c ��� ������� ������');
      FACTOR_D:       Double; //  '�������� ������������ d ��� ������� ������');
      RAIN_intensity: Double; //  '������������� ����� � ������� 0.01% �������, ��/���');
      REFRACTION:     Double; // '�����. ��������� (��� ��.���������=0, ����� �.�.=0)');

      Air_temperature	    : double;   //����������� ������� [��.�]" (�� ��������� 15)
      Atmosphere_pressure : double;   //����������� �������� [����]" (�� ��������� 1013)


      GOST_53363: record
//          Air_temperature	    : double;   //����������� ������� [��.�]" (�� ��������� 15)
//          Atmosphere_pressure : double;   //����������� �������� [����]" (�� ��������� 1013)
          BLCenter            : TBLPoint; //���������� ������� ��������� � ��������
        end;
    end;

  public
    ReflectionPoints: TDBLinkReflectionPointList;

    DBLinkEnd1: TDBLinkEnd;
    DBLinkEnd2: TDBLinkEnd;


    constructor Create;
    destructor Destroy; override;

    procedure LoadFromDataset(aDataset: TDataset);

    property Tx_Freq_GHz: double read Get_Tx_Freq_Ghz;
    property Length_km: double read GetLength_km;
    property FRENEL_MAX_m: double read GetFRENEL_MAX_m;
    property SPACE_SPREAD_m: double read GetSPACE_SPREAD_m;
  end;


  // ---------------------------------------------------------------
  TDBLinkReflectionPoint = class(TCollectionItem)
  // ---------------------------------------------------------------
  public
    DISTANCE_km : Double;   //���������� �� ������� 1-�� ������� ��������� [km]
    ABS_PROSVET_m : Double; //A��������� ������� ��� �������� 1-�� ������� �����. [m]
                            //����������� ������� ��� �������� 1-�� �������
                            //�����������.������� ��� ��������
    Length_km : Double;     //������������� 1-�� �������
    RADIUS_km : Double;        //P����� �������� 1-�� �������

    procedure LoadFromDataset(aDataset: TDataset);
  end;


  TDBLinkReflectionPointList = class(TCollection)
  private
    function GetItems(Index: Integer): TDBLinkReflectionPoint;
  public
    constructor Create;
    function Add: TDBLinkReflectionPoint;
    procedure LoadFromDataset(aDataset: TDataset);

    property Items[Index: Integer]: TDBLinkReflectionPoint read GetItems; default;
  end;


implementation

constructor TDBLink.Create;
begin
  inherited Create;
  ReflectionPoints := TDBLinkReflectionPointList.Create();
  DBLinkEnd1 := TDBLinkEnd.Create();
  DBLinkEnd2 := TDBLinkEnd.Create();
end;

destructor TDBLink.Destroy;
begin
  FreeAndNil(DBLinkEnd2);
  FreeAndNil(DBLinkEnd1);
  FreeAndNil(ReflectionPoints);
  inherited Destroy;
end;

// ---------------------------------------------------------------
procedure TDBLink.LoadFromDataset(aDataset: TDataset);
// ---------------------------------------------------------------
var

  s: string;
begin
  Assert(Assigned(aDataset), 'Value not assigned');
  Assert(aDataset.RecordCount>0, 'Value not assigned');

   // ��� - ������� ��������������� ���������

  with aDataset do
  begin
    Profile_XML   :=FieldByName(FLD_Profile_XML).AsString ;


    s := FieldByName(FLD_OBJNAME).AsString;
    if Eq (s, 'pmp_link')
      then LinkType:=ltPmpLink
      else LinkType:=ltLink;


     case LinkType of
        ltPmpLink: begin
          PmpSector_ID  :=FieldByName(FLD_PMP_SECTOR_ID).AsInteger;
          PmpTerminal_ID:=FieldByName(FLD_PMP_TERMINAL_ID).AsInteger;
        end;

        ltLink: begin
          LinkEnd1_ID :=FieldByName(FLD_LinkEnd1_ID).AsInteger;
          LinkEnd2_ID :=FieldByName(FLD_LinkEnd2_ID).AsInteger;
        end;
      end;

    PropertyID1  :=FieldByName(FLD_Property1_ID).AsInteger;
    PropertyID2  :=FieldByName(FLD_Property2_ID).AsInteger;


    EsrNorm  :=FieldByName(FLD_ESR_NORM).AsFloat;
    BberNorm :=FieldByName(FLD_BBER_NORM).AsFloat;
    GSTLength:=FieldByName(FLD_GST_LENGTH).AsFloat;


//    is_profile_reversed:=FieldByName(FLD_is_profile_reversed).AsBoolean;


    //-----------------------------
    // �������������
    //-----------------------------
   // if Assigned(FindField(FLD_Calc_Method)) and
   //    (FieldByName(FLD_Calc_Method).DataType = ftInteger)
   // then
      Calc_Method:=FieldByName(FLD_Calc_Method).AsInteger ;
   // else
     // Calc_Method:=0;


    BLVector :=db_ExtractBLVector (aDataset);


    STATUS:=FieldByName(FLD_STATUS).AsInteger;

    Profile_Step_m:=FieldByName(FLD_PROFILE_STEP).AsInteger;

    ClutterModel_ID:=FieldByName(FLD_CLUTTER_MODEL_ID).AsInteger;
    LinkEndID1:=FieldByName(FLD_LinkEnd1_ID).AsInteger;
    LinkEndID1:=FieldByName(FLD_LinkEnd2_ID).AsInteger;

/////    SPACE_LIMIT:=FieldByName(FLD_SPACE_LIMIT).AsFloat;



    Rains.rain_intensity_extra:= FieldByName(FLD_rain_INTENSITY_extra).AsFloat;
    Rains.ISCALCLENGTH:= FieldByName(FLD_rain_ISCALCLENGTH).AsBoolean;
    Rains.SIGNAL_QUALITY:= FieldByName(FLD_RAIN_SIGNAL_QUALITY).AsString;


 //   SPACE_SPREAD_m:= FieldByName(FLD_SPACE_SPREAD).AsFloat;

//    Freq_MHz := FieldByName(FLD_Freq).AsFloat;

    Tx_Freq_MHz := FieldByName(FLD_Tx_Freq_MHz).AsFloat;

    Bitrate_Mbps := FieldByName(FLD_Bitrate_Mbps).AsInteger;

  //  Rx_Level_dBm := FieldByName(FLD_POWER_dBm).AsFloat;

    Rx_Level_dBm := FieldByName(FLD_Rx_Level_dBm).AsFloat;

    LOS_STATUS := FieldByName(FLD_LOS_STATUS).AsInteger;

    BAND:=FieldByName(FLD_BAND).AsString;

    BER_REQUIRED := FieldByName(FLD_BER_REQUIRED).AsInteger;

    BBER_NORM:= FieldByName(FLD_BBER_NORM).AsFloat;

    Rains.rain_intensity_extra := FieldByName(FLD_rain_INTENSITY_extra).AsFloat;
    Rains.RAIN_LENGTH_km:=FieldByName(FLD_RAIN_LENGTH_km).AsFloat;
//    Rain_INTENSITY1:= FieldByName(FLD_Rain_INTENSITY).AsFloat;

    rain_WEAKENING_VERT:=         FieldByName(FLD_rain_WEAKENING_VERT).AsFloat;
    rain_WEAKENING_HORZ:=         FieldByName(FLD_rain_WEAKENING_HORZ).AsFloat;
    rain_ALLOWABLE_INTENSE_VERT:= FieldByName(FLD_rain_ALLOWABLE_INTENSE_VERT).AsFloat;
    rain_ALLOWABLE_INTENSE_HORZ:= FieldByName(FLD_rain_ALLOWABLE_INTENSE_HORZ).AsFloat;

    MARGIN_HEIGHT     := FieldByName(FLD_MARGIN_HEIGHT).AsFloat;
    MARGIN_DISTANCE_km := FieldByName(FLD_MARGIN_DISTANCE).AsFloat;
    SESR_SUBREFRACTION:= FieldByName(FLD_SESR_SUBREFRACTION).AsFloat;
    SESR_RAIN         := FieldByName(FLD_SESR_RAIN).AsFloat;
    SESR_INTERFERENCE := FieldByName(FLD_SESR_INTERFERENCE).AsFloat;


//    FRENEL_MAX_m:=  FieldByName(FLD_FRENEL_MAX_IN_METERS).AsFloat;

    length_m    :=FieldByName(FLD_length).AsFloat;
     // eFreq   :=qry_Link.FieldByName(FLD_FREQ).AsFloat;
    NFrenel        :=FieldByName(FLD_NFrenel).AsFloat;

    BBER_REQUIRED:= FieldByName(FLD_BBER_REQUIRED).AsFloat;
    ESR_REQUIRED := FieldByName(FLD_ESR_REQUIRED).AsFloat;
    SESR_NORM    := FieldByName(FLD_SESR_NORM).AsFloat;
    SESR         := FieldByName(FLD_SESR).AsFloat;
    ESR_NORM     := FieldByName(FLD_ESR_NORM).AsFloat;

    KNG          := FieldByName(FLD_KNG).AsFloat;
    KNG_YEAR     := FieldByName(FLD_KNG_YEAR).AsFloat;
    KNG_NORM     := FieldByName(FLD_KNG_NORM).AsFloat;

    FADE_MARGIN_DB := FieldBYName(FLD_FADE_MARGIN_DB).AsFloat;

    Tilt         := FieldBYName(FLD_Tilt).AsFloat;

//    WEAKNESS     := FieldBYName(FLD_WEAKNESS).AsFloat;
    rain_signal_depression_dB     := FieldBYName(FLD_rain_signal_depression_dB).AsFloat;



      // ��� - ������� ��������������� ���������
    //RRV: array[1..11] of double; //�������������� ����� �� �������� ��������������� ���������
     {1=-9.000     ;������� �������� ��������� ����.�������������,10^-8 1/�
      2= 7.000     ;����������� ���������� ���������,10^-8 1/�
      3= 1.000     ;��� ������������ �������.(1...3,0->����.�����=����.���.)
      4= 7.500     ;���������� ��������� �������� ����, �/���.�
      5= 1.000     ;Q-������ ������ �����������
      6=41.000     ;������������� ������ K��, 10^-6
      7= 1.500     ;�������� ������������ b ��� ������� ������
      8= 0.500     ;�������� ������������ c ��� ������� ������
      9= 2.000     ;�������� ������������ d ��� ������� ������
      10=20.000    ;������������� ����� � ������� 0.01% �������, ��/���
      11= 1.335    ;�����. ��������� (��� ��.���������=0, ����� �.�.=0)}

     RRV.gradient_diel:=FieldByName(FLD_gradient_diel).AsFloat;  //'������� �������� ��������� ����.�������������,10^-8 1/�' );
     RRV.gradient_deviation :=FieldByName(FLD_gradient_deviation).AsFloat;       //'����������� ���������� ���������,10^-8 1/�' );

{      if FIsProfileExists then
        d:=0
      else
        d:=0.00001;
}
//      d:=0;


{      RRV.RAIN_RATE    := FieldByName(FLD_RAIN_RATE).AsFloat;
      RRV.terrain_type := FieldByName(FLD_terrain_type).AsInteger;
}
      RRV.terrain_type:=   FieldByName(FLD_terrain_type).AsInteger;//+1 + d;// '��� ������������ �������.(1...3,0->����.�����=����.���.)' );
      RRV.underlying_terrain_type:= FieldByName(FLD_underlying_terrain_type).AsInteger;//+1 + d;// '��� ������������ �������.(1...3,0->����.�����=����.���.)' );

      RRV.steam_wet:=      FieldByName(FLD_steam_wet).AsFloat;   //  '���������� ��������� �������� ����, �/���.�');
      RRV.Q_factor:=       FieldByName(FLD_Q_factor).AsFloat;    //  'Q-������ ������ �����������');
      RRV.climate_factor:= FieldByName(FLD_climate_factor).AsFloat;//'������������� ������ K��, 10^-6');
      RRV.factor_B:=       FieldByName(FLD_factor_B).AsFloat;    //  '�������� ������������ b ��� ������� ������');
      RRV.factor_C:=       FieldByName(FLD_factor_C).AsFloat;    //  '�������� ������������ c ��� ������� ������');
      RRV.FACTOR_D:=       FieldByName(FLD_FACTOR_D).AsFloat;    //  '�������� ������������ d ��� ������� ������');
      RRV.RAIN_intensity:= FieldByName(FLD_RAIN_intensity).AsFloat;   //  '������������� ����� � ������� 0.01% �������, ��/���');
      RRV.REFRACTION:=     FieldByName(FLD_REFRACTION).AsFloat;  // '�����. ��������� (��� ��.���������=0, ����� �.�.=0)');

      //----------------------
      // new GOST
      //----------------------
   //   if Assigned(FindField(FLD_underlying_terrain_type)) and
  //       Assigned(FindField(FLD_Air_temperature)) and
   //      Assigned(FindField(FLD_Atmosphere_pressure))
   //   then begin


       RRV.Air_temperature	   := FieldByName(FLD_Air_temperature).AsFloat;   //����������� ������� [��.�]" (�� ��������� 15)
       RRV.Atmosphere_pressure := FieldByName(FLD_Atmosphere_pressure).AsFloat;   //����������� �������� [����]" (�� ��������� 1013)
       
(*

        with RRV.GOST_53363 do
        begin
          Air_temperature	    := FieldByName(FLD_Air_temperature).AsFloat;   //����������� ������� [��.�]" (�� ��������� 15)
          Atmosphere_pressure := FieldByName(FLD_Atmosphere_pressure).AsFloat;   //����������� �������� [����]" (�� ��������� 1013)
        end;
*)
    //  end;

  //    RRV.GOST_53363.BLCenter            := TBLPoint; //���������� ������� ��������� � ��������

    //TRB: array[1..4] of double;  //���������� � ����������� �������� ���
     {1=0.012      ;����������� �������� SESR ��� ����, %
      2=0.05       ;����������� �������� ���  ��� ����, %
      3=600        ;����� ����, ��
      4=0.200      ;���������� ����� ���, ������������� �������������}
  ///    vGroup:=xml_AddNodeTag (aRoot, 'TRB');

      Trb.GST_TYPE :=FieldByName(FLD_GST_TYPE).AsInteger;

      Trb.GST_LENGTH :=FieldByName(FLD_GST_LENGTH).AsFloat;
      Trb.GST_SESR    :=FieldByName(FLD_GST_SESR).AsFloat; //   '����������� �������� SESR ��� ����, %');
      Trb.GST_Kng     :=FieldByName(FLD_GST_Kng).AsFloat; //    '����������� �������� ���  ��� ����, %');
//    b.Length      :=dmLink_Calc_SESR.GetRoundedLengthByID(FLinkID);  //               , '����� ����, ��');
      Trb.KNG_dop     :=FieldByName(FLD_KNG_dop).AsFloat; //    '���������� ����� ���, ������������� �������������}');
      Trb.SPACE_LIMIT :=FieldByName(FLD_SPACE_LIMIT).AsFloat; // '���������� ������������� ������� p(a%)');
      Trb.SPACE_LIMIT_PROBABILITY:=FieldByName(FLD_SPACE_LIMIT_PROBABILITY).AsFloat; //'���������� ����������� a% ������������� ������������ �������� p(a%) [%]');

   //   Length_km:=dmLink_Calc_SESR.GetRoundedLength_KM(qry_Link);
//      aRec.Trb.Length      :=dmLink_Calc_SESR.GetRoundedLengthByID(FLinkID);  //               , '����� ����, ��');


  end;
end;

function TDBLink.Get_Tx_Freq_Ghz: double;
begin
  Result := Tx_Freq_MHz / 1000;
end;

function TDBLink.GetLength_km: double;
begin
  Result := Length_m / 1000;
end;

function TDBLink.GetFRENEL_MAX_m: double;
begin
  if (Tx_Freq_MHz > 0) and (length_m>0) then
    result:=Sqrt(( 300/Tx_Freq_MHz )* NFrenel * (length_m/2)* (1 - (length_m/2)/length_m))
  else
    result:=0;
end;

function TDBLink.GetSPACE_SPREAD_m: double;
begin
 //���������������� ������
  if Tx_Freq_MHz > 0 then
    Result := (300/Tx_Freq_MHz) * 200
  else
    Result := 0;
end;

constructor TDBLinkReflectionPointList.Create;
begin
  inherited Create(TDBLinkReflectionPoint);
end;

function TDBLinkReflectionPointList.Add: TDBLinkReflectionPoint;
begin
  Result := TDBLinkReflectionPoint (inherited Add);
end;

function TDBLinkReflectionPointList.GetItems(Index: Integer):
    TDBLinkReflectionPoint;
begin
  Result := TDBLinkReflectionPoint(inherited Items[Index]);
end;


procedure TDBLinkReflectionPoint.LoadFromDataset(aDataset: TDataset);
begin
  with aDataset do
  begin
    DISTANCE_km  :=FieldByName(FLD_DISTANCE).AsFloat;
    ABS_PROSVET_m:=FieldByName(FLD_ABS_PROSVET).AsFloat;   //A��������� ������� ��� �������� 1-�� ������� �����. [m]
    Length_km    :=FieldByName(FLD_Length).AsFloat;     //������������� 1-�� �������
    RADIUS_km    :=FieldByName(FLD_RADIUS).AsFloat;        //P����� �������� 1-�� �������

  end;
end;

procedure TDBLinkReflectionPointList.LoadFromDataset(aDataset: TDataset);
begin
  aDataset.First;
  while not aDataset.EOF do
  begin
    Add.LoadFromDataset(aDataset);
    aDataset.Next;
  end;
end;



end.

{

//--------------------------------------------------------------------

  //--------------------------------------------------------------------
  procedure DoAddLinkType;
  //--------------------------------------------------------------------
  var
      iStep: integer;
      d,dRefraction: double; //dLength
  begin

    with qry_Link do
    begin
      // ��� - ������� ��������������� ���������
    //RRV: array[1..11] of double; //�������������� ����� �� �������� ��������������� ���������
     {1=-9.000     ;������� �������� ��������� ����.�������������,10^-8 1/�
      2= 7.000     ;����������� ���������� ���������,10^-8 1/�
      3= 1.000     ;��� ������������ �������.(1...3,0->����.�����=����.���.)
      4= 7.500     ;���������� ��������� �������� ����, �/���.�
      5= 1.000     ;Q-������ ������ �����������
      6=41.000     ;������������� ������ K��, 10^-6
      7= 1.500     ;�������� ������������ b ��� ������� ������
      8= 0.500     ;�������� ������������ c ��� ������� ������
      9= 2.000     ;�������� ������������ d ��� ������� ������
      10=20.000    ;������������� ����� � ������� 0.01% �������, ��/���
      11= 1.335    ;�����. ��������� (��� ��.���������=0, ����� �.�.=0)}

      aRec.RRV.gradient_diel:=FieldByName(FLD_gradient_diel).AsFloat;  //'������� �������� ��������� ����.�������������,10^-8 1/�' );
      aRec.RRV.gradient     :=FieldByName(FLD_gradient).AsFloat;       //'����������� ���������� ���������,10^-8 1/�' );

      if FIsProfileExists then
        d:=0
      else
        d:=0.00001;

      aRec.RRV.terrain_type:=   FieldByName(FLD_terrain_type).AsInteger+1 + d;// '��� ������������ �������.(1...3,0->����.�����=����.���.)' );
      aRec.RRV.steam_wet:=      FieldByName(FLD_steam_wet).AsFloat;   //  '���������� ��������� �������� ����, �/���.�');
      aRec.RRV.Q_factor:=       FieldByName(FLD_Q_factor).AsFloat;    //  'Q-������ ������ �����������');
      aRec.RRV.climate_factor:= FieldByName(FLD_climate_factor).AsFloat;//'������������� ������ K��, 10^-6');
      aRec.RRV.factor_B:=       FieldByName(FLD_factor_B).AsFloat;    //  '�������� ������������ b ��� ������� ������');
      aRec.RRV.factor_C:=       FieldByName(FLD_factor_C).AsFloat;    //  '�������� ������������ c ��� ������� ������');
      aRec.RRV.FACTOR_D:=       FieldByName(FLD_FACTOR_D).AsFloat;    //  '�������� ������������ d ��� ������� ������');
      aRec.RRV.RAIN_RATE:=      FieldByName(FLD_RAIN_RATE).AsFloat;   //  '������������� ����� � ������� 0.01% �������, ��/���');
      aRec.RRV.REFRACTION:=     FieldByName(FLD_REFRACTION).AsFloat;  // '�����. ��������� (��� ��.���������=0, ����� �.�.=0)');


      // -------------------------------------------------------------------
{
      vGroup:=xml_AddNodeTag (aRoot, 'RRV');
      DoAdd (vGroup, 1,  FieldValues[FLD_gradient_diel], '������� �������� ��������� ����.�������������,10^-8 1/�' );
      DoAdd (vGroup, 2,  FieldValues[FLD_gradient],      '����������� ���������� ���������,10^-8 1/�' );

}
      if FIsProfileExists then
        d:=0
      else
        d:=0.00001;


  {    DoAdd (vGroup, 3,  FieldByName(FLD_terrain_type).AsInteger+1 + d, '��� ������������ �������.(1...3,0->����.�����=����.���.)' );
      DoAdd (vGroup, 4,  FieldValues[FLD_steam_wet],     '���������� ��������� �������� ����, �/���.�');
      DoAdd (vGroup, 5,  FieldValues[FLD_Q_factor],      'Q-������ ������ �����������');
      DoAdd (vGroup, 6,  FieldValues[FLD_climate_factor],'������������� ������ K��, 10^-6');
      DoAdd (vGroup, 7,  FieldValues[FLD_factor_B],      '�������� ������������ b ��� ������� ������');
      DoAdd (vGroup, 8,  FieldValues[FLD_factor_C],      '�������� ������������ c ��� ������� ������');
      DoAdd (vGroup, 9,  FieldValues[FLD_FACTOR_D],      '�������� ������������ d ��� ������� ������');
      DoAdd (vGroup, 10, FieldValues[FLD_RAIN_RATE],     '������������� ����� � ������� 0.01% �������, ��/���');


      DoAdd (vGroup, 11, FieldValues[FLD_REFRACTION], '�����. ��������� (��� ��.���������=0, ����� �.�.=0)');
}

//      DoAdd (vGroup, 11, 1.335 {FieldValues[FLD_refraction_gradient_0]}, '�����. ��������� (��� ��.���������=0, ����� �.�.=0)');

    //DLT: array[1..4] of double;  //���������� � �������� ��������
     {1= 0.100     ;��� ��������� �������, �� ( 0 - ��������� ����)
      2= 1.000     ;�������� ������ ������� V����(g)=V���.�, ��
      3= 1.000     ;�������� ������ ������� g(V����)=g(V���.�), 10^-8
      4=80.000     ;������������ �������� ��� ������� ������������, 10^-8}

      aRec.DLT.Step:=TruncFloat(FStep/1000, 3);

{
      vGroup:=xml_AddNodeTag (aRoot, 'DLT');
//      iStep:=FieldByName(FLD_profile_step).AsInteger;

      DoAdd (vGroup,  1, TruncFloat(FStep/1000, 3),  '��� ��������� �������, �� ( 0 - ��������� ����)');
}


//      rec.DLT.precision_V_diffraction
      aRec.DLT.precision_V_diffraction:=1;//   '�������� ������ ������� V����(g)=V���.�, ��');
      aRec.DLT.precision_g_V_diffraction:=1;// '�������� ������ ������� g(V����)=g(V���.�), 10^-8');
      aRec.DLT.max_gradient_for_subrefr:=80;//  '������������ �������� ��� ������� ������������, 10^-8');



      // default values
 //     DoAdd (vGroup,  2, 1 {FieldValues[FLD_precision_V_diffraction]},   '�������� ������ ������� V����(g)=V���.�, ��');
 //     DoAdd (vGroup,  3, 1 {FieldValues[FLD_precision_g_V_diffraction]}, '�������� ������ ������� g(V����)=g(V���.�), 10^-8');
 //     DoAdd (vGroup,  4, 80 {FieldValues[FLD_max_gradient_for_subrefr]},  '������������ �������� ��� ������� ������������, 10^-8');


    //TRB: array[1..4] of double;  //���������� � ����������� �������� ���
     {1=0.012      ;����������� �������� SESR ��� ����, %
      2=0.05       ;����������� �������� ���  ��� ����, %
      3=600        ;����� ����, ��
      4=0.200      ;���������� ����� ���, ������������� �������������}
  ///    vGroup:=xml_AddNodeTag (aRoot, 'TRB');


      aRec.Trb.GST_SESR    :=qry_Link.FieldByName(FLD_GST_SESR).AsFloat; //   '����������� �������� SESR ��� ����, %');
      aRec.Trb.GST_Kng     :=qry_Link.FieldByName(FLD_GST_Kng).AsFloat; //    '����������� �������� ���  ��� ����, %');
//      aRec.Trb.Length      :=dmLink_Calc_SESR.GetRoundedLengthByID(FLinkID);  //               , '����� ����, ��');
      aRec.Trb.KNG_dop     :=qry_Link.FieldByName(FLD_KNG_dop).AsFloat; //    '���������� ����� ���, ������������� �������������}');
      aRec.Trb.SPACE_LIMIT :=qry_Link.FieldByName(FLD_SPACE_LIMIT).AsFloat; // '���������� ������������� ������� p(a%)');
      aRec.Trb.SPACE_LIMIT_PROBABILITY:=qry_Link.FieldByName(FLD_SPACE_LIMIT_PROBABILITY).AsFloat; //'���������� ����������� a% ������������� ������������ �������� p(a%) [%]');

      aRec.Trb.Length_km:=dmLink_Calc_SESR.GetRoundedLength_KM(qry_Link);
//      aRec.Trb.Length      :=dmLink_Calc_SESR.GetRoundedLengthByID(FLinkID);  //               , '����� ����, ��');



//      dLength:=dmLink_Calc_SESR.GetRoundedLengthByID(FLinkID);

  //    DoAdd (vGroup,  1, qry_Link[FLD_GST_SESR],   '����������� �������� SESR ��� ����, %');
  //    DoAdd (vGroup,  2, qry_Link[FLD_GST_Kng],    '����������� �������� ���  ��� ����, %');
  //    DoAdd (vGroup,  3, dLength                  , '����� ����, ��');
  //    DoAdd (vGroup,  4, qry_Link[FLD_KNG_dop],    '���������� ����� ���, ������������� �������������}');
  //    DoAdd (vGroup,  5, qry_Link[FLD_SPACE_LIMIT], '���������� ������������� ������� p(a%)');
  //    DoAdd (vGroup,  6, qry_Link[FLD_SPACE_LIMIT_PROBABILITY],'���������� ����������� a% ������������� ������������ �������� p(a%) [%]');


    end;
  end;
  //--------------------------------------------------------------------






end.
