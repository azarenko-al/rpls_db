unit u_LinkEnd_classes;

interface

uses DB,SysUtils,
     u_Link_const,

     u_antenna_classes,

     u_const_db
     ;

type
  TDBLinkEnd = class
  public
    THRESHOLD_BER_6    : double;
    THRESHOLD_BER_3    : double;
    Power_dBm          : double;
 //   Rx_level_dBm        : double;

    PROPERTY_ID        : Integer;
    LinkEndType_ID    : Integer;

    TX_FREQ_MHz        : Double;

    BitRate_Mbps       : double;  //BitRate_Mbps

    BAND               : string;

    Passive_element_LOSS_dBm  : Double;
    LOSS_dBm           : Double;
    KNG                : Double; // '��� ������������ (%)1');

    KRATNOST_BY_SPACE  : Integer;

    KRATNOST_BY_FREQ   : Integer;
    FREQ_CHANNEL_COUNT : Integer;

    Freq_Spacing_MHz   : Double;

    SIGNATURE_WIDTH : double;// '������ ���������, ���');
    SIGNATURE_HEIGHT: double;// '������ ���������, ��');
    MODULATION_COUNT: double;// '����� ������� ���������');
    EQUALISER_PROFIT: double;// '�������, �������� ������������, ��');

    ATPC_profit_dB  : double;// <param id="45" value="10" comment="������� �� ATPC, ��"/>
    Use_ATPC        : Boolean;
    Threshold_degradation_dB: Double; //Threshold_degradation_dB,  '���������� ����������������, ��');

    GOST_53363_modulation : string;

  public
    DBAntennas: TDBAntennaList;

    constructor Create;
    destructor Destroy; override;

    procedure LoadFromDataset(aDataset: TDataset);
  end;

implementation


constructor TDBLinkEnd.Create;
begin
  inherited Create;
  DBAntennas := TDBAntennaList.Create();
end;

destructor TDBLinkEnd.Destroy;
begin
  FreeAndNil(DBAntennas);
  inherited Destroy;
end;


procedure TDBLinkEnd.LoadFromDataset(aDataset: TDataset);
begin
  assert (aDataset.RecordCount>0);

  with aDataset do
  begin
    THRESHOLD_BER_6   :=FieldByName(FLD_THRESHOLD_BER_6).asFloat;
    THRESHOLD_BER_3   :=FieldByName(FLD_THRESHOLD_BER_3).asFloat;

    BAND              :=FieldByName(FLD_BAND).AsString;
    TX_FREQ_MHz       :=FieldByName(FLD_TX_FREQ_MHz).AsFloat;

    LINKENDTYPE_ID   :=FieldByName(FLD_LINKENDTYPE_ID).AsInteger;
    PROPERTY_ID       :=FieldByName(FLD_PROPERTY_ID).AsInteger;

//    Rx_level_dBm      :=FieldByName(FLD_Rx_level_dBm).AsFloat;
    Power_dBm         :=FieldByName(FLD_POWER_dBm).AsFloat;

    Kng               :=FieldByName(FLD_Kng).AsFloat;
 //   Kng2              :=FieldByName(FLD_Kng2).AsFloat;
    LOSS_dBm          :=FieldByName(FLD_LOSS).AsFloat;

    Passive_element_LOSS_dBm:=FieldByName(FLD_Passive_element_LOSS).AsFloat;

    FREQ_CHANNEL_COUNT:=FieldByName(FLD_FREQ_CHANNEL_COUNT).AsInteger;

    Freq_Spacing_MHz  :=FieldByName(FLD_Freq_Spacing).AsFloat;

    KRATNOST_BY_SPACE:=FieldByName(FLD_KRATNOST_BY_SPACE).AsInteger;
    KRATNOST_BY_FREQ :=FieldByName(FLD_KRATNOST_BY_FREQ).AsInteger;

    SIGNATURE_WIDTH := FieldByName(FLD_SIGNATURE_WIDTH).AsFloat;//,  'double; //  '������ ���������, ���');
    SIGNATURE_HEIGHT:= FieldByName(FLD_SIGNATURE_HEIGHT).AsFloat;//, 'double; // '������ ���������, ��');
    MODULATION_COUNT:= FieldByName(FLD_MODULATION_COUNT).AsFloat;//,double;// '����� ������� ���������');
 //   EQUALISER_PROFIT:= FieldByName(FLD_EQUALISER_PROFIT).AsFloat;//, 'double; // '�������, �������� ������������, ��');


    BitRate_Mbps  := FieldByName(FLD_BitRate_Mbps).AsInteger;
//    BitRate_Mbps  := FieldByName(FLD_SPEED).AsFloat;

    if Assigned(FindField(FLD_ATPC_profit_dB)) then
    begin
      ATPC_profit_dB:= FieldByName(FLD_ATPC_profit_dB).AsFloat;//, 'double; // '�������, �������� ������������, ��');
      Use_ATPC      := FieldByName(FLD_Use_ATPC).AsBoolean;//, 'double; // '�������, �������� ������������, ��');
      Threshold_degradation_dB:= FieldByName(FLD_Threshold_degradation_dB).AsFloat;//, 'double; // '�������, �������� ������������, ��');

      GOST_53363_modulation := FieldByName(FLD_GOST_53363_modulation).AsString;

    end;
   //   : double;// <param id="45" value="10" comment="������� �� ATPC, ��"/>
  //  : Double; //Threshold_degradation_dB,  '���������� ����������������, ��');


  end;
end;

end.
