unit u_antenna_mask;

interface
uses
  SysUtils, Classes,

  u_Func_arrays,  
  u_func
  ;

type
  TAntennaMask1 = class
  private
    FMask: array of record
             Angle: double;
             Loss: double;
           end;
    function GetLossByAngle(aAngle: double): double;
  public
    function GetLossesStr: string;
  end;


  TAntennaMaskItem = class(TCollectionItem)
  public
    Angle: double;
    Loss: double;
  end;


  TAntennaMask = class(TCollection)
  private
    function Find_Angle(aAngle: double): boolean;
    function GetItems(Index: Integer): TAntennaMaskItem;
  public
    constructor Create;
    procedure AddAngle(aAngle, aLoss: double);

    function GetLossByAngle(aAngle: Double): Double;
    function IsOMNI: Boolean;
    function GetWidth: Integer;

    procedure LoadFromStr(aValue: string);

    property Items[Index: Integer]: TAntennaMaskItem read GetItems; default;
  end;


implementation

// ---------------------------------------------------------------
function TAntennaMask1.GetLossByAngle(aAngle: double): double;
// ---------------------------------------------------------------
var i: Integer;
begin
  Result := 0;

  for i := 0 to High(FMask) do
    if (aAngle <= FMask[i].Angle) then
    begin
      Result := FMask[i].Loss;
      Exit;
    end;
end;

// ---------------------------------------------------------------
function TAntennaMask1.GetLossesStr: string;
// ---------------------------------------------------------------
var i: Integer;
begin
  Result := '';

  for i := 0 to High(FMask) do
    Result := Result + Format('%1.1f=%1.1f', [FMask[i].Angle, FMask[i].Loss])
              + IIF(i< High(FMask),';','');
end;

// ---------------------------------------------------------------
function TAntennaMask.GetWidth: Integer;
// ---------------------------------------------------------------
var
  i, ind, iNulAngleInd: Integer;
  isOMNI: Boolean;
begin
  Result:=0;

  //def OMNI
  isOMNI:=True;
  for i:=0 to Count-1 do
    if (Items[i].Loss > 3) then begin isOMNI:=False; Break; end;

  if isOMNI then begin
    Result:=360;
    Exit;
  end;

  iNulAngleInd:=-1;
  for i:=0 to Count-1 do
    if Items[i].Loss=0 then begin iNulAngleInd:=i; Break; end;

  if iNulAngleInd<0 then
   Exit;

  for i:=0 to Count-1 do
  begin
    ind:=(iNulAngleInd + i) mod Count;
    if (Items[ind].Loss < 3) then Inc(Result) else Break;
  end;

  for i:=0 to Count-1 do
  begin
    ind:=((iNulAngleInd - i) + Count) mod Count;
    if (Items[ind].Loss < 3) then Inc(Result) else Break;
  end;
end;

// ---------------------------------------------------------------
function TAntennaMask.IsOMNI: Boolean;
// ---------------------------------------------------------------
var
  i: Integer;
begin
  Result:= True;

  for i:=0 to Count-1 do
    if Items[i].Loss>3 then begin Result:= False; Break; end;
end;

// ---------------------------------------------------------------
function TAntennaMask.GetLossByAngle(aAngle: Double): Double;
// ---------------------------------------------------------------
var
  i: Integer;
begin
  Result:=0;

  for i:=0 to Count-1 do
    if (Items[i].Angle=aAngle) then
    begin
      Result:=Items[i].Loss;
      Exit;
    end;

  for i:=0 to Count-1-1 do
    if ((Items[i].Angle>aAngle) and (Items[i+1].Angle<aAngle)) then
    begin
      Result:=Trunc(Items[i].Loss);
      Exit;
    end;
end;


constructor TAntennaMask.Create;
begin
  inherited Create(TAntennaMaskItem);
end;


procedure TAntennaMask.AddAngle(aAngle, aLoss: double);
begin
  if (aAngle<0) then
    aAngle:= 360 + aAngle;


  if Find_Angle(aAngle) then
    exit;

  with TAntennaMaskItem(Add) do
  begin
    Angle:=aAngle;
    Loss:=aLoss;
  end;
end;

// ---------------------------------------------------------------
procedure TAntennaMask.LoadFromStr(aValue: string);
// ---------------------------------------------------------------
var
  strArr1,strArr2: TStrArray;
  i: Integer;
begin
  strArr1:=StringToStrArray (aValue,';');

 // SetLength (FMask, Length(strArr1));

  for i := 0 to High(strArr1) do
  begin
    strArr2:=StringToStrArray (strArr1[i],'=');

    if Length(strArr2)=2 then
      AddAngle (AsFloat(strArr2[0]), AsFloat(strArr2[1]));

(*    begin
      FMask[i].Angle:=AsFloat(strArr2[0]);
      FMask[i].Loss :=AsFloat(strArr2[1]);
    end;
*)
  end;

 // i:=0;
end;



function TAntennaMask.GetItems(Index: Integer): TAntennaMaskItem;
begin
  Result := TAntennaMaskItem(inherited Items[Index]);
end;

//--------------------------------------------------------------------------
function TAntennaMask.Find_Angle(aAngle: double): boolean;
//--------------------------------------------------------------------------
var  I: Integer;
begin
  Result:= False;
  for i:=0 to Count-1 do
    if Items[i].Angle = aAngle then
    begin
      Result:= True;
      Exit;
    end;
end;

end.
