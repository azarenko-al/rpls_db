unit u_types;

interface
{$I ver.inc}

uses SysUtils, Classes,

   u_log,

   u_const_str,
   u_const_map,
   u_const_db
   ;

const
  OBJ_DICT                = 'dict';

  OBJ_COMMON_GROUP       = 'COMMON_GROUP';
  OBJ_GSM_GROUP          = 'GSM_GROUP';
  OBJ_LINK_GROUP         = 'LINK_GROUP';
  OBJ_PMP_GROUP          = 'PMP_GROUP';

  OBJ_GSM_DICT_GROUP     = 'GSM_DICT_GROUP';
  OBJ_LINK_DICT_GROUP    = 'LINK_DICT_GROUP';

  //-----------

  //dicts
  OBJ_ANTENNA_TYPE            = 'ANTENNA_TYPE';
  OBJ_CALC_MODEL              = 'CALC_MODEL';
  OBJ_CLUTTER_MODEL           = 'CLUTTER_MODEL';
  OBJ_COLOR_SCHEMA            = 'COLOR_SCHEMA';

//  OBJ_LINKEND_ANTENNA          = 'LINKEND_Antenna';

  OBJ_LINK_Repeater = 'LINK_Repeater';

  OBJ_BSC                     = 'BSC';
  OBJ_CALC_MAP                = 'CALC_MAP';

  OBJ_CELL                    = 'CELL';
  OBJ_CELL_ANT                = 'cell_ant';
  OBJ_CELL_LAYER              = 'CELL_LAYER';
  OBJ_COMBINER                = 'COMBINER';

  OBJ_GEO_CITY                = 'GEO_CITY';
  OBJ_GEO_REGION              = 'GEO_REGION';
//  OBJ_LAC                     = 'LAC';
  OBJ_LINK                    = 'LINK';
  
  OBJ_LINK_SDB                = 'LINK_SDB';


  OBJ_LinkFreqPlan            = 'LinkFreqPlan';
  OBJ_LinkFreqPlan_LINK       = 'LinkFreqPlan_LINK';
  OBJ_LINK_TYPE               = 'LINK_TYPE';
  OBJ_LINKEND                 = 'LINKEND';


//  OBJ_LINKEND_ANT             = 'LinkEnd_Ant';
  OBJ_LINKEND_Antenna             = 'LinkEnd_Antenna';
  OBJ_LINK_REPEATER_Antenna   = 'LINK_REPEATER_Antenna';

  OBJ_LINKEND_TYPE            = 'LINKENDTYPE';
  OBJ_LINKLINE                = 'LINKLINE';
  OBJ_LINKLINE_LINK           = 'LINKLINE_LINK';
  OBJ_LINKLINE_TYPE           = 'LINKLINE_TYPE';
  OBJ_LINKNET                 = 'LINKNET';
  OBJ_LINKNET_LINK_REF        = 'LINKNET_LINK_REF';
  OBJ_LINKNET_LINKLINE_REF    = 'LINKNET_LINKLINE_REF';
  OBJ_MAP                     = 'map';
  OBJ_MSC                     = 'MSC';

  OBJ_PMP_LINK                = 'PMP_LINK';
  OBJ_PMP_SECTOR              = 'PMP_SECTOR';
  OBJ_PMP_SECTOR_ANT          = 'PMP_SECTOR_ANT';
  OBJ_PMP_SITE                = 'PMP_SITE';
  OBJ_PMP_TERMINAL            = 'PMP_TERMINAL';
  OBJ_PMP_TERMINAL_ANT        = 'PMP_TERMINAL_ANT';
  OBJ_PMP_CALC_REGION         = 'PMP_Calc_Region';

//  OBJ_PMP_CALCREGION_SITE     = 'PMP_CALCREGION_SITE';
  OBJ_PMP_CALC_REGION_SITE    = 'PMP_CALC_REGION_SITE';

  OBJ_PROJECT                 = 'PROJECT';
  OBJ_PROPERTY                = 'PROPERTY';
  OBJ_REL_MATRIX              = 'REL_MATRIX';

  OBJ_REPORT                  = 'REPORT';
  OBJ_SITE                    = 'SITE';
  OBJ_TERMINAL_TYPE           = 'TERMINAL_TYPE';
  OBJ_TRAFFIC                 = 'TRAFFIC_MODEL';
//  OBJ_TRANSFER_TECH           = 'TRANSFER_TECH';
  OBJ_TRX_TYPE                = 'TRX';

  OBJ_COMPANY                 = 'COMPANY';
  OBJ_Status_LIST             = 'Status_LIST';


  OBJ_TEMPLATE_PMP_SITE       = 'TEMPLATE_PMP_SITE';
  OBJ_TEMPLATE_LINKEND        = 'TEMPLATE_LINKEND';
  OBJ_TEMPLATE_ANT            = 'TEMPLATE_ANT';
  OBJ_TEMPLATE_LINKEND_Antenna='TEMPLATE_LINKEND_Antenna';


  OBJ_TEMPLATE_Link                   = 'TEMPLATE_LINK';
//  OBJ_TEMPLATE_SITE                   = 'TEMPLATE_SITE';
  OBJ_TEMPLATE_Site_mast              = 'TEMPLATE_SITE_mast';
  OBJ_TEMPLATE_SITE_LINKEND           = 'TEMPLATE_SITE_LINKEND';
  OBJ_TEMPLATE_SITE_LINKEND_ANTENNA   = 'TEMPLATE_SITE_LINKEND_ANTENNA';

  OBJ_TEMPLATE_FreqPlan = 'TEMPLATE_FreqPlan';
  TBL_TEMPLATE_FreqPlan = 'lib_FreqPlan.LinkEndType';



type
  // common object types -------------
  TrpObjectType =
    (otNone,
     otUnknown,

     otLinkRepeater,
     otLinkRepeaterAntenna,

	   otRootDict,

//     otLinkendAntenna,
     otAntennaType,
     otBSC,
     otCalcModel,

//     otCalcRegion_Site,

//     otCalcRegionFPFolder,
     otCell,
     otCellAnt,
     otCellLayer,
     otClutterModel,
     otColorSchema,
     otCombiner,

     otGeoRegion,
   //  otLAC,
     otLink,
     otLink_SDB,
 //    OBJ_LINK_SDB

     otLinkend,
//     otLinkend_simple,
     otLinkEnd_Antenna,
     otLinkEndType,
     otLinkFreqPlan,
     otLinkFreqPlan_Link,
     otLinkLine,


     otLinkLine_Link,
//     otLinkLineType,
     otLinkNet,
     otLinkNet_Link_Ref,
     otLinkNet_LinkLine_Ref,
     otLinkType,
     otMapFile,
     otMSC,

     otPmpLink,
     otPMPSector,
     otPmpSectorAnt,
     otPmpSite,
     otPmpTerminal,
     otPmpTerminalAnt,
     otPmpCalcRegion,
     otPMPCalcRegion_Site,

     otProject,
     otProperty,
     otRelMatrixFile,
     otReport,
     otSite,

//     otTemplate_Link_Linkend,


     otTemplateAnt1,
     otTemplatePmpSite,
     otTemplateLinkend,
     otTemplateLinkend_antenna,

     otTerminalType,
//     otTrafficModel,
//     otTransferTech,
     otTrxType,
//     otUserGroups,
    // otUser,
   //  otUserGroup,

     otCOMMON_GROUP,
//     otGSM_GROUP,
     otLINK_GROUP,

     otPMP_GROUP,

     otGSM_DICT_GROUP,
     otLINK_DICT_GROUP,


     otCompany,
     otStatus,

//     otMast,

     otTemplate_Link,
//     otTemplate_Site,
//     otTemplate_Site_mast,
     otTemplate_Site_Linkend,
     otTemplate_Site_Linkend_ant,

     otTemplate_FreqPlan

    );

  TrpObjectTypeArr = array of TrpObjectType;
  TrpObjectTypeSet = set of TrpObjectType;


const
  GUID_DICTS = '{8B0D1531-1402-4274-9D2C-0A5467573A23}';


  ALL_MAP_OBJECTS : array[0..12] of TrpObjectType =
	(
     otSite,
     otCellAnt,

//     otRepeater,

   	 otPmpSite,
     otPmpSectorAnt,
	   otPmpTerminal,
	   otPmpTerminalAnt,

	   otLinkEnd_Antenna,
	   otProperty,
     otLink,

     otMSC,
     otBSC,

     otLinkend,

     otPmpCalcRegion
//     otCalcRegion_Pmp

//     otXConn
  //   otPMPCalcRegion,
    );


  MAP_PMP_TYPES : array[0..3] of TrpObjectType =
    (
     otPmpSite,
     otPmpSectorAnt,
	   otPmpTerminal,
   //  otPmpTerminalAnt,
     otPmpCalcRegion
    );

 MAP_LINK_TYPES : array[0..2] of TrpObjectType =
    (
     otLink,
     otLinkend,
     otLinkEnd_Antenna
    );


  MAP_COMMON_TYPES : array[0..2] of TrpObjectType =
    (
     otProperty,
     otMSC,
     otBSC
    );


// {$IFDEF link}
  MAP_OBJECTS1 : array[0..6+7-1] of TrpObjectType =
    (
     otProperty,
     otLink,

     otSite,
     otCellAnt,

     otPmpSite,
     otPmpSectorAnt,

	   otPmpTerminal,
     otPmpTerminalAnt,
     otPmpCalcRegion,

     otLinkend,
     otLinkEnd_Antenna ,
   //  otPmpCalcRegion,

   //  otXConn,
     otMSC,
     otBSC

    );

 PROJECT_LINK_TYPES : array[0..5] of TrpObjectType=
   (
//    otRepeater,
  	otLinkend,
    otLink,
  	otLinkLine,
    otLinkNet,
    otLinkFreqPlan,
    otLink_SDB
  //  otLinkRepeater
  );

 PROJECT_GIS_TYPES : array[0..1] of TrpObjectType=
   (
//    otRepeater,
  	otMapFile,
    otRelMatrixFile
  );



 PROJECT_PMP_TYPES : array[0..2] of TrpObjectType=
   (
   	otPmpSite,
    otPmpTerminal,
    otPMPCalcRegion
  );


  PROJECT_COMMON_TYPES : array[0..1] of TrpObjectType=
   (
    otMSC,
    otBSC  //,
  //  otMapFile
  );


  MISC_TYPES : array[0..0+2] of TrpObjectType=
   (
    otLinkRepeater,
//    otMast,



//     otTemplate_Site,
//     otTemplate_Site_mast,
     otTemplate_Site_Linkend,
     otTemplate_Site_Linkend_ant


    );

  PROJECT_TYPES : array[0..0 ] of TrpObjectType=
   (
    otProperty
    );

  DICT_LINK_TYPES : array[0..1] of TrpObjectType=
   (
     otLinkType,
     otLinkEndType
   );


//  DICT_TYPES : array[0..14] of TrpObjectType=

{
  DICT_TYPES1 : array[0..0] of TrpObjectType=
	 (
      otTemplate_Site
      );
 }

//  DICT_TYPES : array[0..15] of TrpObjectType=
  DICT_TYPES11111111111 : array[0..0] of TrpObjectType=
	 (

      otTemplate_FreqPlan

//      otTemplate_Site
	 );

  DICT_TYPES : array[0..16] of TrpObjectType=
	 (


      otAntennaType,
      otLinkEndType,
	    otCellLayer,
      otTrxType,
      otCombiner,
      otTerminalType,

      otTemplatePmpSite,

      otLinkType,
      otClutterModel,
      otCalcMOdel,
      otColorSchema,
      otReport,

      otGeoRegion,


//      otTransferTech,

      otCompany,
      otStatus,


      otTemplate_Link,
      otTemplate_FreqPlan

//      otTemplate_Site
	 );



  DICT_TYPES_SIMPLE : array[0..6-1] of TrpObjectType=
	 (

      otAntennaType,
      otLinkEndType,
//	    otCellLayer,
//      otTrxType,
      otCombiner,
//      otTerminalType,

//      otTemplatePmpSite,

//      otLinkType,
      otClutterModel,
//      otCalcMOdel,
      otColorSchema,
//      otReport

//      otGeoRegion,

     otTemplate_Link
//      otTemplate_Site

//      otTransferTech,

//      otCompany,
 //     otStatus
	 );



type

  //-------------------------------------------------------------------
  TObjectTypeItem = class
  //-------------------------------------------------------------------
  public
    Type_          : TrpObjectType;
    DisplayName    : string;
    Name           : string;

    TableName      : string;
    TableName_for_update : string;

    ViewName       : string;
    RootFolderName : string;
    RootFolderGUID : string;
    MapFileName    : string;
    MapCaption     : string;
    AutoLabel      : boolean;

    IsUseProjectID   : boolean;
    IsHasGeoRegionID : boolean;

  end;

  //-------------------------------------------------------------------
  TObjectEngine = class(TList)
  //-------------------------------------------------------------------
  private
    function GetItem 	   (aIndex: integer): TObjectTypeItem;
    function GetItemByType (aValue: TrpObjectType): TObjectTypeItem;
    function GetItemByName(aName: string): TObjectTypeItem;
  public
    constructor Create;

    function MakeObjectTypeArr (Values: array of TrpObjectType): TrpObjectTypeArr;
    function IsObjectNameExist(aObjectName: string): boolean;
    procedure ReAssign;

    property ItemByType [Index: TrpObjectType]: TObjectTypeItem read GetItemByType;
    property ItemByName [Index: string]:  TObjectTypeItem read GetItemByName;
//    property ItemExist  [Index: string]:  boolean read IsObjectNameExist;
    property Items      [Index: integer]: TObjectTypeItem read GetItem; default;
  end;


  function ObjNameToType (aObjName: string): TrpObjectType;
  function ObjTypeToName (aObjType: TrpObjectType): string;
  function FoundInMapObjects(aType: TrpObjectType): boolean; overload;
  function FoundInMapObjects(aObjName: string): boolean;overload;

  function FoundInDictTypes(aType: TrpObjectType): boolean;

  function FoundInProjectTypes(aType: TrpObjectType): boolean;
  function FoundInMiscTypes(aType: TrpObjectType): boolean;



var
  g_Obj: TObjectEngine;

const

  GUID_LINK_DICT_GROUP  =  '{E8A333C5-1B63-46DB-9F6C-7A4AF914C1BA}';
  GUID_PMP_GROUP        =  '{F290CE84-F847-4A70-AF50-2513DC822B93}';
  GUID_COMMON_GROUP     =  '{B2B555E1-8B62-44DE-869B-B40819CF7AE0}';
  GUID_GSM_DICT_GROUP   =  '{7AFAFD55-04F5-434A-8D21-0302287F2673}';
  GUID_GSM_GROUP        =  '{7AFAFD44-04F5-434A-8D21-0302287F2673}';

  GUID_BSC               = '{2651FDD3-8FBD-4539-8001-663BB41351D9}';
  GUID_CALC_REGION_PMP    = '{5AA034B2-5497-46CA-AC81-EDAC6B550428}';

  GUID_Link             = '{2E41DAD1-4895-439F-86A2-8BD9A7F2C524}';
  GUID_Link_SDB         = '{2E41DAD1-1895-139F-86A2-1BD9A7F2C524}';


  GUID_LINK_GROUP       =  '{E8A197C5-1B63-46DB-9F6C-7A4AF914C1BA}';
  GUID_LinkEnd          = '{E99520A6-EDCA-4569-9201-6EC4B7FF6ED9}';
  GUID_MSC                = '{0681C617-2BE8-43D1-B982-412759FFDC35}';
  GUID_PMP_SITE         = '{6FE78D4F-E835-7DF4-AD68-CE9EB46C0108}';
  GUID_PMP_TERMINAL        = '{D2B6E8B0-B11D-41F5-8E9D-422619AA17DE}';
  GUID_PROJECT          = '{3241FBD7-D2CC-4D43-96B6-0075944FEDC9}';
  GUID_PROPERTY         = '{36242720-8CF3-4530-800F-9F1CEE4651CA}';
  GUID_SITE             = '{6FE58D4F-E805-4DF4-AD68-CE9EB46C0108}';


  GUID_CALC_MODEL         = '{0A9A38B6-1F09-4A7F-9BE7-D0894E6DD3C6}';
  GUID_LinkEnd_Type       = '{9C502A18-DBD5-48E4-AA37-EE29999E4657}';
  GUID_TRANSFER_TECH      = '{94C5937A-BF71-4735-8069-1002CB671C8A}';
  GUID_ANTENNA_TYPE       = '{9C251ACC-8418-4E04-BF2E-9D739B98F48D}';
  GUID_COLOR_SCHEMA       = '{17A35912-F4ED-40C3-B86F-63D67EC5745B}';
  GUID_MAP                = '{5A567E86-477A-435C-909B-EEB557BE9153}';
  GUID_REL_MATRIX         = '{EEF6D065-EDA8-4709-BD41-ECDA8DB8E9DE}';


  GUID_LINK_REPEATER      = '{AEF6D065-EDA1-4709-BD41-ECDA8DB8E9DE}';


  GUID_COMPANY            = '{C5DEA20A-B2F2-425F-86E3-A29E7385B4A7}';

  GUID_LINKLINE           = '{CCCF2C6B-399B-4CED-B973-8159844EF62A}';
  GUID_Link_Type          = '{5A952D12-487D-404A-ACC7-20F640E996D1}';


  GUID_TEMPLATE_LINK      = '{55A35952-F5ED-10C3-F81F-13D68EC2715B}';
  GUID_TEMPLATE_FREQPLAN  = '{11A35952-35ED-88C3-F81F-13D68EC2715B}';

//  GUID_TEMPLATE_SITE      = '{55A35952-F5ED-10C3-F81F-13D68EC2715B}';



//========================================================================
implementation
//========================================================================
const
    GUID_ANT_TEMPLATE      = '{65A65952-E5ED-30C3-G86F-73D68EC2745B}';
    GUID_CELL_LAYER        = '{4F8876A2-B33E-4804-B1EA-8FFB11F48EAF}';
    GUID_CELL_TEMPLATE     = '{05A35952-F5ED-39C3-F86F-65D68EC2745B}';
    GUID_CLUTTER_MODEL     = '{43989693-28B5-4107-A59E-7318D91B7837}';
    GUID_COMBINER           = '{69454BFE-A31F-A3B2-BD65-A993D858834D}';
    GUID_LINKLINE_TYPE      = '{39654B8E-A91F-43B2-BC65-A99BD858834D}';
    GUID_PMPSECTOR          = '{42918188-60F3-42D1-9AE1-3B0795CD9CD1}';
    GUID_PMPCALCREGION =     '{42918188-60F3-42D1-1AE1-3B0795CD3CD1}';
    GUID_REPORT             = '{2E42DAD1-4895-449F-86A2-8BD9A8F2C524}';
    GUID_TERMINAL_TYPE      = '{96FF711B-888D-47E0-BDD5-3BCE79DEF36B}';
    GUID_TRAFFIC            = '{917E121C-CBFB-44B8-BC59-5B5D2CA48BAD}';
    GUID_TRX_TYPE           = '{0EF9EC80-A902-427F-AAAB-BE999836526C}';
    GUID_USER               = '{927E151C-CBFB-48B8-BC59-5B5D2CA58BAD}';
    GUID_GEOCITY            = '{70DA1431-E617-4FF8-B198-BFF20363AAA8}';
    GUID_GEOREGION          = '{7DADEC56-B4A4-4092-8196-D4B55CA4BF45}';
    GUID_LINK_FREQPLAN      = '{39654B8E-A94F-43B4-BC85-A99BD858834D}';
    GUID_LINKNET            = '{392C829C-6265-4DB5-9BAE-02ABEBEDA47F}';
    GUID_USERGROUP          = '{31C596E0-DF2F-4AB8-BBBB-821B994CFAD7}';
    GUID_CALCREGION_BS_PMP_FOLDER = '{EA7A508F-E172-4C1F-929A-462926952FDC}';

   // GUID_CALCREGION_BS_GSM_FOLDER = '{F4EFDE56-2465-44DF-99E6-93A340F29678}';
  //  GUID_CALCREGION_FP_FOLDER     = '{785E371E-87B7-4744-9C47-00C11ABE70E3}';
  //  GUID_DRIVE_TEST         = '{58BC7CF9-70DE-433D-B391-91E96D8C78C3}';
  //  GUID_FREQ_PLAN          = '{BD067F1E-6F72-43BF-AFA2-8FF97BB4CEA0}';
 //   GUID_LAC                = '{2E41DBD1-2195-579F-86A2-8BD9A7F2C524}';
 //   GUID_X_CONN             = '{845AFCCE-8CBD-4595-B094-5857A3B638D8}';
//    GUID_REGION             ='{14BE0178-0FAE-43CC-A798-2486BC0B85E3}';



    GUID_TEMPLATE_SITE_LINKEND  = '{55A35952-F5ED-10C3-F81F-63D68EC2745B}';
    GUID_TEMPLATE_PMP_SITE      = '{15A35952-F5ED-30C3-F86F-63D68EC2745B}';

(*    GUID_TEMPLATE_LINKEND   = '{05AB2AA9-43EB-4109-9CC8-08C44AD79642}';
    GUID_TEMPLATE_LINKEND_ANT = '{15A45952-F1ED-30C3-F86F-61D68EC2745B}';
*)
    GUID_Status_LIST        = '{B0DECC5D-9A24-4355-A5EF-D972063FAD5A}';


const

  // ��� ���� ����� Project ID
  TYPES_WITH_PROJECT_ID : array[0..17-1-1] of TrpObjectType=
   (

    otLinkend_Antenna,
  //  otCalcRegion_Pmp,
//    otFreqPlan,
    otBSC,
    otLink,

//    otDriveTest,
    otLinkend,

    otLinkFreqPlan,
    otLinkLine,

    otLinkNet,
    otMapFile,
    otMSC,

    otPmpSite,
  //  otPmpSector,
    otPMPTerminal,
    otPMPCalcRegion,

    otProperty,

    otRelMatrixFile,
    otSite,

    otLink_SDB

    //  otPMPCalcRegion,
//    otTrafficModel

    );


  TYPES_WITH_GEOREGION_ID : array[0..8] of TrpObjectType=
   (
    otLinkend_Antenna,
    otBSC,
    otLink,
    otLinkend,
    otMSC,
    otPmpSite,
    otPMPTerminal,
    otProperty,
    otSite
    );






function ObjNameToType (aObjName: string): TrpObjectType;
begin
  if Assigned(g_Obj.ItemByName[aObjName]) then
    Result := g_Obj.ItemByName[aObjName].Type_
  else
    Result := otNone;
end;

function ObjTypeToName (aObjType: TrpObjectType): string;
begin
  Result := g_Obj.ItemByType[aObjType].Name;
end;


//------------------------------------------------------
function FoundInMapObjects(aObjName: string): boolean;
//------------------------------------------------------
begin
  Result := FoundInMapObjects(ObjNameToType(aObjName));

end;

//------------------------------------------------------
function FoundInMapObjects(aType: TrpObjectType): boolean;
//------------------------------------------------------
var i: Integer;
begin
  Result:= false;

 // for I := 0 to High(MAP_OBJECTS) do
//    if aType = MAP_OBJECTS[i] then 	begin Result:= true; exit; end;


{
  if aType=otProperty then begin
    Result := true;
  end;

    Exit;
}


  for I := 0 to High(MAP_COMMON_TYPES) do
    if aType = MAP_COMMON_TYPES[i] then  	begin Result:= true; exit; end;


  for I := 0 to High(MAP_PMP_TYPES) do
    if aType = MAP_PMP_TYPES[i] then  	begin Result:= true; exit; end;

  for I := 0 to High(MAP_LINK_TYPES) do
    if aType = MAP_LINK_TYPES[i] then  	begin Result:= true; exit; end;


end;




//------------------------------------------------------
function FoundInMiscTypes(aType: TrpObjectType): boolean;
//------------------------------------------------------
var i: Integer;
begin
  Result:= false;

  if aType in [otPMPCalcRegion_Site,
               otCell,
               otPMPSector,
               otLinkLine_Link,

               otLinkFreqPlan_Link,

               otLinkNet_Link_ref,
               otLinkNet_LinkLine_ref,



      otTemplateLinkend,
      otTemplateLinkend_antenna,



               otPmpLink,

             //  otUser,
             //  otUserGroup,

               otLinkend_Antenna]
  then
    begin Result:= true; exit; end;


end;


//------------------------------------------------------
function FoundInProjectTypes(aType: TrpObjectType): boolean;
//------------------------------------------------------
var i: Integer;
begin
  Result:= false;


  if aType in [otMapFile,otRelMatrixFile] then
    begin Result:= true; exit; end;


  if aType in [otProject] then
    begin Result:= true; exit; end;

  for I := 0 to High(PROJECT_TYPES) do
    if aType = PROJECT_TYPES[i] then  	begin Result:= true; exit; end;

  for I := 0 to High(PROJECT_PMP_TYPES) do
    if aType = PROJECT_PMP_TYPES[i] then  	begin Result:= true; exit; end;


  for I := 0 to High(MISC_TYPES) do
    if aType = MISC_TYPES[i] then  	begin Result:= true; exit; end;


(*  for I := 0 to High(PROJECT_GSM_TYPES) do
    if aType = PROJECT_GSM_TYPES[i] then  	begin Result:= true; exit; end;
*)


  for I := 0 to High(PROJECT_LINK_TYPES) do
    if aType = PROJECT_LINK_TYPES[i] then  	begin Result:= true; exit; end;

  for I := 0 to High(PROJECT_COMMON_TYPES) do
    if aType = PROJECT_COMMON_TYPES[i] then  	begin Result:= true; exit; end;


end;

//------------------------------------------------------
function FoundInDictTypes(aType: TrpObjectType): boolean;
//------------------------------------------------------
var i: Integer;
begin
  Result:= false;

  for I := 0 to High(DICT_TYPES) do
    if aType = DICT_TYPES[i] then 	begin Result:= true; exit; end;

//  for I := 0 to High(DICT_GSM_TYPES) do
 //   if aType = DICT_GSM_TYPES[i] then 	begin Result:= true; exit; end;

end;

//------------------------------------------------------
function Eq (Value1,Value2 : string): boolean;
begin
  Result := AnsiCompareText(Trim(Value1),Trim(Value2))=0;
end;


function TObjectEngine.GetItem (aIndex: integer): TObjectTypeItem;
begin
  Result:=TObjectTypeItem (inherited Items[aIndex]);
end;


function TObjectEngine.GetItemByType (aValue: TrpObjectType): TObjectTypeItem;
var i: integer;
begin
  for i:=0 to Count-1 do
   	if Items[i].Type_=aValue then
    begin
      Result:=Items[i];
      Exit;
    end;

  Result:=TObjectTypeItem.Create;
  Result.Type_:=aValue;
  Add(Result);
end;


function TObjectEngine.GetItemByName(aName: string): TObjectTypeItem;
var i: Integer;
begin
  Assert(aName<>'', 'Value <=0');

  for i:=0 to Count-1 do
	  if Eq (Items[i].Name, aName) then
    begin
      Result:=Items[i];
      Exit;
    end;

  raise Exception.Create('function TObjectEngine.GetItemByName(aName: string): TObjectTypeItem;  ' + aName);

  g_Log.AddError('u_types: function TObjectEngine.GetItemByName (aName: string): TObjectTypeItem; Value='+aName,'');
 // raise Exception.Create('function TObjectEngine.GetItemByName (aName: string): TObjectTypeItem;');

  Result:=nil;
end;


function TObjectEngine.IsObjectNameExist(aObjectName: string): boolean;
var
  i: Integer;
begin
  for i:=0 to Count-1 do
  	if Eq (Items[i].Name, aObjectName) then begin Result:=true; Exit; end;

  Result:=False;
end;


function TObjectEngine.MakeObjectTypeArr (Values: array of TrpObjectType): TrpObjectTypeArr;
var i: integer;
begin
  SetLength (Result, High(Values)+1);
  for i:=0 to High(Values) do
    Result[i]:=Values[i];
end;


constructor TObjectEngine.Create;
begin
  inherited;
  ReAssign();
end;

//-------------------------------------------------------------------
procedure TObjectEngine.ReAssign;
//-------------------------------------------------------------------

  procedure DoAssign1 (aType: TrpObjectType;
					   aName: string;
                       aTableName: string;
                       aRootFolderName: string='';
                       aRootFolderGUID: string=''
                       );
  begin
    ItemByType[aType].Name:=aName;
    ItemByType[aType].TableName:=aTableName;
    ItemByType[aType].RootFolderName:=aRootFolderName;
    ItemByType[aType].RootFolderGUID:=aRootFolderGUID;
  end;

  procedure DoAssignMapInfo (aType: TrpObjectType; aMapFileName,aMapCaption: string);
  begin
    ItemByType[aType].MapFileName:=aMapFileName;
    ItemByType[aType].MapCaption:=aMapCaption;
  end;


var ot: TrpObjectType;
  n: Integer;
    i: integer;

//  oConn: TADOConnection;

begin
  //-------------------------------------------------------------------
  // dicts
  //-------------------------------------------------------------------


  //otTemplate_Site_mast

//  DoAssign1 (otMast,        OBJ_mast,            TBL_mast,   '', ''); //  DN_FOLDER_TEMPLATE_Site_mast,  GUID_TEMPLATE_SITE);

  DoAssign1 (otTemplate_FreqPlan,  OBJ_TEMPLATE_FreqPlan,     TBL_TEMPLATE_FreqPlan,      DN_FOLDER_TEMPLATE_FreqPlan,  GUID_TEMPLATE_FreqPlan);
  DoAssign1 (otTemplate_Link,     OBJ_TEMPLATE_Link,          TBL_TEMPLATE_Link,          DN_FOLDER_TEMPLATE_Link,  GUID_TEMPLATE_Link);
//  DoAssign1 (otTemplate_Site,             OBJ_TEMPLATE_Site,                 TBL_TEMPLATE_Site,          DN_FOLDER_TEMPLATE_Site,  GUID_TEMPLATE_SITE);

//  DoAssign1 (otTemplate_Site_mast,        OBJ_TEMPLATE_Site_mast,            TBL_TEMPLATE_Site_mast,   '', ''); //  DN_FOLDER_TEMPLATE_Site_mast,  GUID_TEMPLATE_SITE);
  DoAssign1 (otTemplate_Site_Linkend,     OBJ_TEMPLATE_Site_Linkend,         TBL_TEMPLATE_Site_Linkend,  DN_FOLDER_TEMPLATE_Site_Linkend,  GUID_TEMPLATE_SITE_LINKEND);
  DoAssign1 (otTemplate_Site_Linkend_ant, OBJ_TEMPLATE_Site_Linkend_antenna, TBL_TEMPLATE_Site_Linkend_antenna, '', '');// DN_FOLDER_TEMPLATE_Site_Linkend,  GUID_TEMPLATE_SITE_LINKEND);


 //  Assert(Assigned (ItemByName[OBJ_TEMPLATE_SITE_LINKEND] ));



  DoAssign1 (otLinkRepeater,         OBJ_LINK_Repeater,         TBL_LINK_Repeater,   DN_FOLDER_LINK_REPEATER,  GUID_LINK_REPEATER);
  DoAssign1 (otLinkRepeaterAntenna,  OBJ_LINK_Repeater_Antenna, '',      '', '');


  DoAssign1 (otAntennaType,         OBJ_ANTENNA_TYPE,           TBL_ANTENNATYPE,      DN_FOLDER_ANTENNA_TYPES, GUID_ANTENNA_TYPE);
  DoAssign1 (otCellLayer,           OBJ_CELL_LAYER,             TBL_CELLLAYER,        DN_FOLDER_CELL_LAYER, GUID_CELL_LAYER);
//////////  DoAssign1 (otCellTemplate,        OBJ_TEMPLATE_CELL,          TBL_TEMPLATE_CELL,     '');
  DoAssign1 (otClutterModel,        OBJ_CLUTTER_MODEL,          TBL_CLUTTERMODEL,     DN_FOLDER_CLUTTER_MODEL, GUID_CLUTTER_MODEL);
  DoAssign1 (otColorSchema,         OBJ_COLOR_SCHEMA,           TBL_COLORSCHEMA,      DN_FOLDER_COLOR_SCHEMAS, GUID_COLOR_SCHEMA);
 // DoAssign1 (otGeoCity,             OBJ_GEO_CITY  ,             TBL_GEOCITY,           DN_FOLDER_GEOCITY,   GUID_GEOCITY);
  DoAssign1 (otGeoRegion,           OBJ_GEO_REGION,             TBL_GEOREGION,         DN_FOLDER_GEOREGION, GUID_GEOREGION);

//  DoAssign1 (otLinkEndType,         OBJ_LINKEND_TYPE,           TBL_LINKENDTYPE,       DN_FOLDER_LinkEndType, GUID_LINKEND_TYPE);
  DoAssign1 (otLinkEndType,         OBJ_LINKEND_TYPE,           VIEW_LINKENDTYPE,       DN_FOLDER_LinkEndType, GUID_LINKEND_TYPE);

//  DoAssign1 (otLinkEndType,         OBJ_LINKEND_TYPE,           VIEW_LINKENDTYPE,       DN_FOLDER_LinkEndType, GUID_LINKEND_TYPE);

  //  DoAssign1 (otLinkEndType,         OBJ_LINKEND_TYPE,           TBL_LINKENDTYPE,       DN_FOLDER_LinkEndType, GUID_LINKEND_TYPE);
//  DoAssign1 (otLinkLineType,        OBJ_LINKLINE_TYPE,          TBL_LINKLINETYPE,      DN_FOLDER_LinkLINE_Type, GUID_LINKLINE_TYPE);
  DoAssign1 (otLinkType,            OBJ_LINK_TYPE,              TBL_LinkType,         DN_FOLDER_LinkType, GUID_LINK_TYPE);
  DoAssign1 (otReport,              OBJ_REPORT,                 TBL_REPORT,            DN_FOLDER_REPORT,       GUID_REPORT);





  DoAssign1 (otTemplatePmpSite,        OBJ_TEMPLATE_PMP_SITE,      TBL_TEMPLATE_PMP_SITE,     DN_TEMPLATE_PMP_SITE,       GUID_TEMPLATE_PMP_SITE);

  DoAssign1 (otTemplateLinkEnd,     OBJ_TEMPLATE_LinkEnd,       TBL_TEMPLATE_LinkEnd,  '',    '');
  DoAssign1 (otTemplateAnt1,         OBJ_TEMPLATE_ANT,           TBL_TEMPLATE_LINKEND_ANTENNA, '',    '');

  DoAssign1 (otTemplateLinkEnd_Antenna,   OBJ_TEMPLATE_LinkEnd_Antenna,    TBL_TEMPLATE_LINKEND_ANTENNA, '',    '');

  DoAssign1 (otTerminalType,        OBJ_TERMINAL_TYPE,          TBL_TerminalType,     DN_FOLDER_TERMINAL,     GUID_TERMINAL_TYPE);
  DoAssign1 (otTrxType,             OBJ_TRX_TYPE,               TBL_TRXTYPE,          DN_FOLDER_TRXS,         GUID_TRX_TYPE);
//  DoAssign1 (otTrxType,             OBJ_TRX_TYPE,               VIEW_TRXTYPE,           DN_FOLDER_TRXS,         GUID_TRX_TYPE);


  //ListViewName
//  DoAssign1 (otAntenna,             OBJ_LINKEND_ANTENNA,   TBL_LINKEND_ANTENNA,         '');

//  DoAssign1 (otLinkEndAntenna,      OBJ_LINKEND_ANTENNA,   TBL_LINKEND_ANTENNA,        '������� ���' );
  DoAssign1 (otLinkEnd_Antenna,     OBJ_LINKEND_Antenna,   TBL_LINKEND_ANTENNA,        '������� ���' );
  DoAssign1 (otPmpSectorAnt,        OBJ_PMP_SECTOR_ANT,    TBL_LINKEND_ANTENNA,        '������� �� PMP');
  DoAssign1 (otPmpTerminalAnt,      OBJ_PMP_TERMINAL_ANT,  TBL_LINKEND_ANTENNA,        '������� PMP terminal' );

  DoAssign1 (otBSC,                 OBJ_BSC,                   TBL_BSC,             DN_FOLDER_BSC, GUID_BSC);
  DoAssign1 (otCalcModel,           OBJ_CALC_MODEL,            TBL_CALCMODEL,      DN_FOLDER_CALC_MODEL, GUID_CALC_MODEL);
 // DoAssign1 (otCalcRegion,          OBJ_CALC_REGION,           TBL_CALCREGION,      DN_FOLDER_CALC_REGION, GUID_CALC_REGION);
//  DoAssign1 (otCalcRegion_Site,     OBJ_CALCREGION_SITE,       TBL_PMP_CALCREGIONCELLS, '');
  DoAssign1 (otCell,                OBJ_CELL,                  TBL_CELL,            '');

//  DoAssign1 (otCellAnt,             OBJ_CELL_ANT,              TBL_ANTENNA,         '������� �� GSM');

  DoAssign1 (otCombiner,            OBJ_COMBINER,              TBL_PASSIVE_COMPONENT, DN_COMBINERS, GUID_COMBINER);
 ///// DoAssign1 (otDriveTest,           OBJ_DRIVE_TEST,            TBL_DRIVE_TEST,      DN_FOLDER_DRIVE_TEST, GUID_DRIVE_TEST);
//  DoAssign1 (otFreqPlan,            OBJ_FREQPLAN,              TBL_FREQPLAN,        DN_FOLDER_FREQ_PLAN, GUID_FREQ_PLAN);
//  DoAssign1 (otLAC,                 OBJ_LAC,                   TBL_LAC,             DN_FOLDER_LAC, GUID_LAC);

  DoAssign1 (otLink_SDB,             OBJ_LINK_SDB,              TBL_LINK_SDB,        DN_FOLDER_LINKS_SDB, GUID_Link_SDB );


  DoAssign1 (otLink,                OBJ_LINK,                  TBL_LINK,            DN_FOLDER_LINKS, GUID_Link );
  DoAssign1 (otLinkEnd,             OBJ_LinkEnd,               TBL_LINKEND,         DN_FOLDER_LINKENDS, GUID_LinkEnd);


  DoAssign1 (otLinkFreqPlan,        OBJ_LINKFREQPLAN,          TBL_LINKFREQPLAN, DN_FOLDER_LINK_FREQPLAN, GUID_LINK_FREQPLAN);
  DoAssign1 (otLinkFreqPlan_Link,   OBJ_LINKFREQPLAN_LINK,     '');
  DoAssign1 (otLinkLine,            OBJ_LINKLINE,               TBL_LINKLINE, DN_FOLDER_LINK_LINES, GUID_LINKLINE);
  DoAssign1 (otLinkLine_Link,       OBJ_LINKLINE_LINK,          '');
  DoAssign1 (otLinkNet,             OBJ_LINKNET,                TBL_LINKNET, DN_FOLDER_LINKNET, GUID_LINKNET);
  DoAssign1 (otLinkNet_Link_Ref,    OBJ_LINKNET_LINK_REF,       '');
  DoAssign1 (otLinkNet_LinkLine_Ref,OBJ_LINKNET_LINKLINE_REF,   '');

  DoAssign1 (otMapFile,             OBJ_MAP,                    VIEW_MAPFILE, DN_FOLDER_MAP, GUID_MAP);
  DoAssign1 (otMSC,                 OBJ_MSC,                    TBL_MSC,      DN_FOLDER_MSC, GUID_MSC);
  DoAssign1 (otPMPCalcRegion_Site,  OBJ_PMP_CALC_REGION_SITE,    '');

  DoAssign1 (otPMPLink,             OBJ_PMP_LINK,             '',   '');

  DoAssign1 (otPMPSector,           OBJ_PMP_SECTOR,             view_PMP_SECTOR,    ''); //TBL_PMP_SECTOR,
  DoAssign1 (otPmpSite,             OBJ_PMP_SITE,               TBL_PMP_SITE,       DN_FOLDER_PMP,          GUID_PMP_SITE);

  DoAssign1 (otPmpTerminal,         OBJ_PMP_TERMINAL,           VIEW_Pmp_Terminal,   DN_FOLDER_PMPTERMINAL,  GUID_PMP_TERMINAL);

//  DoAssign1 (otPmpTerminal,         OBJ_PMP_TERMINAL,           TBL_Pmp_Terminal,   DN_FOLDER_PMPTERMINAL,  GUID_PMP_TERMINAL);

  DoAssign1 (otProject,             OBJ_PROJECT,                TBL_PROJECT,        DN_FOLDER_PROJECTS,     GUID_PROJECT);
//  DoAssign1 (otProperty,            OBJ_PROPERTY,               VIEW_PROPERTY,      DN_FOLDER_PROPERTIES,   GUID_PROPERTY);
  DoAssign1 (otProperty,            OBJ_PROPERTY,               TBL_PROPERTY,       DN_FOLDER_PROPERTIES,   GUID_PROPERTY);

//  DoAssign1 (otRelMatrixFile,       OBJ_REL_MATRIX,             TBL_RELIEF,         DN_FOLDER_REL_MATRIX,   GUID_REL_MATRIX);
  DoAssign1 (otRelMatrixFile,       OBJ_REL_MATRIX,             VIEW_RELIEF,         DN_FOLDER_REL_MATRIX,   GUID_REL_MATRIX);

  DoAssign1 (otSite,                OBJ_SITE,                   TBL_SITE,           DN_FOLDER_SITE,         GUID_SITE);
//  DoAssign1 (otTrafficModel,        OBJ_TRAFFIC,                TBL_TRAFFIC_MODEL,  DN_FOLDER_TRAFFIC,      GUID_TRAFFIC);
  //DoAssign1 (otUser,                OBJ_USER,                   TBL_USER,           DN_FOLDER_USER,         GUID_USER);
 // DoAssign1 (otUserGroup,           OBJ_USERGROUP,              TBL_USERGROUP,      DN_FOLDER_USERGROUP,    GUID_USERGROUP);
//  DoAssign1 (otXconn,               OBJ_XCONN,          TBL_XCONN,          DN_FOLDER_X_CONN,       GUID_X_CONN);
//////  DoAssign1 (otPMPCalcRegion,OBJ_PMP_CALC_REGION,TBL_PMP_CALC_REGION, DN_FOLDER_PMP_CALC_REGION, GUID_PMP_CALC_REGION);


  DoAssign1 (otCOMMON_GROUP,  OBJ_COMMON_GROUP,   '', '���������� � ����������',   GUID_COMMON_GROUP);
  DoAssign1 (otLINK_GROUP,    OBJ_LINK_GROUP,     '', '�� �����',                  GUID_LINK_GROUP);
//  DoAssign1 (otGSM_GROUP,     OBJ_GSM_GROUP,      '', '��������� ����� (2G)',      GUID_GSM_GROUP); //'GSM',
  DoAssign1 (otPMP_GROUP,     OBJ_PMP_GROUP,      '', '�����������',               GUID_PMP_GROUP);
 

  DoAssign1 (otLINK_DICT_GROUP, OBJ_LINK_DICT_GROUP, '', '��',       GUID_LINK_DICT_GROUP);
  DoAssign1 (otGSM_DICT_GROUP,  OBJ_GSM_DICT_GROUP,  '', 'GSM',      GUID_GSM_DICT_GROUP);

  //-------
//  DoAssign1 (otCalcRegionBSPmpFolder, OBJ_CALCREGION_BS_PMP_FOLDER, TBL_PMP_SITE, DN_FOLDER_PMP,       GUID_CALCREGION_BS_PMP_FOLDER);
//  DoAssign1 (otCalcRegionBSGsmFolder, OBJ_CALCREGION_BS_GSM_FOLDER, TBL_SITE,     DN_FOLDER_SITE,      GUID_CALCREGION_BS_GSM_FOLDER);

//  DoAssign1 (otCalcRegionFPFolder,    OBJ_CALCREGION_FP_FOLDER,     TBL_FREQPLAN, DN_FOLDER_FREQ_PLAN, GUID_CALCREGION_FP_FOLDER);
  //-------

//  DoAssign1 (otTransferTech,          OBJ_TRANSFER_TECH,            TBL_TRANSFER_TECH,  DN_FOLDER_TRANSFER_TECH, GUID_TRANSFER_TECH);

  DoAssign1 (otCompany,  OBJ_COMPANY,     TBL_TCOMPANY, DN_FOLDER_COMPANY,    GUID_COMPANY);
  DoAssign1 (otStatus,   OBJ_STATUS_LIST, '',           DN_FOLDER_STATUSLIST, GUID_Status_LIST);

//  DoAssign1 (otTemplateLink,   OBJ_TEMPLATE_LINK,          TBL_TEMPLATE_LINK,     DN_FOLDER_TEMPLATE_LINK,    GUID_TEMPLATE_LINK);


  //-------------------------------------------------------------------
  // ListViewName
  //-------------------------------------------------------------------
 // OBJ_ LINKEND_Antenna
// otLinkEnd_Antenna
  ItemByType[otLinkEnd_Antenna].ViewName :=VIEW_LinkEnd_Antenna;
  ItemByType[otPMPSector].ViewName       :=VIEW_PMP_Sector;


  ItemByType[otSite].ViewName       :=VIEW_SITE;
  ItemByType[otPmpSite].ViewName    :=VIEW_PMP_SITE;
  ItemByType[otPmpCalcRegion].ViewName :=VIEW_PMP_CALCREGION;
//  ItemByType[otPMPCalcRegion].ViewName:=VIEW_PMPCALCREGION;
  ItemByType[otLink].ViewName       :=VIEW_LINK;
  ItemByType[otLinkEnd].ViewName    :=VIEW_LINKEND;

  ItemByType[otPmpTerminal].ViewName:=view_PMP_TERMINAL; //_full;
  ItemByType[otPmpLink].ViewName    :=VIEW_PMP_LINK;
  ItemByType[otLinkLine].ViewName        :=VIEW_LinkLine;
  ItemByType[otProperty].ViewName        :=VIEW_PROPERTY_;
  ItemByType[otBSC].ViewName             :=VIEW_BSC;
  ItemByType[otMSC].ViewName             :=VIEW_MSC;


  ItemByType[otLinkEndType].TableName_for_update := TBL_LinkEndType;
  ItemByType[otLinkEnd].TableName_for_update     := TBL_LinkEnd;
  ItemByType[otLink].TableName_for_update        := TBL_Link;
  ItemByType[otLinkLine].TableName_for_update    := TBL_LinkLine;
  ItemByType[otProperty].TableName_for_update    := TBL_Property;

  //-------------------------------------------------------------------
  ItemByType[otProperty].DisplayName:='��������';

//  ItemByType[otFolder].DisplayName:=STR_FOLDER;


  //-------------------------------------------------------------------
  // map objects
  //-------------------------------------------------------------------
//..  DoAssignMapInfo (otCell,            MAP_CELL_ANT,         'GSM �������');

  DoAssignMapInfo (otBSC,             MAP_BSC,              'BSC');
  DoAssignMapInfo (otPmpCalcRegion,   MAP_PMP_CALCREGION,       '����� �������');
//  DoAssignMapInfo (otPmpCalcRegion,   MAP_CALCREGION,       '����� �������');
//  DoAssignMapInfo (otCellAnt,         MAP_CELL_ANT,         'GSM �������');
  DoAssignMapInfo (otLink,            MAP_LINK,             '�� ��������');
  DoAssignMapInfo (otLinkend,         MAP_LINKEND,          'PPC');
  DoAssignMapInfo (otLinkEnd_Antenna, MAP_LINKEND_antenna,  '��C �������');
  DoAssignMapInfo (otMSC,             MAP_MSC,              'MSC');
  DoAssignMapInfo (otPmpSectorAnt,    MAP_PMP_SECTOR_ANT,   'PMP �������');
  DoAssignMapInfo (otPmpSite,         MAP_PMP,              'PMP �������');
//  DoAssignMapInfo (otPmpSite,     MAP_PMP,              'PMP �������');
  DoAssignMapInfo (otPmpTerminal,     MAP_PMP_TERMINAL,     '�� PMP');
  DoAssignMapInfo (otPmpTerminalAnt,  MAP_PMP_TERMINAL_ANT,'PMP terminal �������');
  DoAssignMapInfo (otProperty,        MAP_PROPERTY,         '��������');
  DoAssignMapInfo (otSite,            MAP_SITE,             'GSM �������');
//  DoAssignMapInfo (otXConn,       MAP_X_CONN,           'X ����������');

////  DoAssignMapInfo (otPMPCalcRegion, MAP_PMP,           '����� ������� PMP');

//  DoAssignMapInfo (otXconn,       MAP_X_CONN,       'X ����������');

//  DoAssignMapInfo (otPMP,          MAP_PMP,          'PMP �����');
//  DoAssignMapInfo (otPmpSectorAnt, MAP_PMP_ANT,      'PMP �������');
{

  ItemByType[otMSC].AutoLabel:=True;
  ItemByType[otBSC].AutoLabel:=True;
  ItemByType[otProperty].AutoLabel:=True;
  ItemByType[otCalcRegion].AutoLabel:=True;
 // ItemByType[otPMPCalcRegion].AutoLabel:=True;
  ItemByType[otLink].AutoLabel:=True;
//  ItemByType[otLinkend].AutoLabel:=False;
  ItemByType[otSite].AutoLabel:=True;
 // ItemByType[otLinkEndAnt].AutoLabel:=False;
//  ItemByType[otCellAnt].AutoLabel:=False;

}

  DoAssign1 (otRootDict, OBJ_DICT, '');

  // ---------------------------------------------------------------
  // 3G
  // ---------------------------------------------------------------

  DoAssign1 (otPmpCalcRegion, OBJ_PMP_CALC_REGION, TBL_PMP_CALCREGION, DN_FOLDER_CALC_REGION_PMP, GUID_CALC_REGION_PMP);



{  ItemByType[otSite_3G].ViewName        :=VIEW_SITE_3G;
  ItemByType[otCalcRegion_3G].ViewName  :=VIEW_3G_CALCREGION;
}
  ItemByType[otPmpCalcRegion].ViewName  :=VIEW_PMP_CALCREGION;

//  DoAssignMapInfo (otPMPCalcRegion, MAP_CALCREGION, 'PMP ����� �������');
  DoAssignMapInfo (otPMPCalcRegion, MAP_PMP_CALCREGION, 'PMP ����� �������');


  for i:=0 to High(TYPES_WITH_PROJECT_ID) do
    ItemByType[TYPES_WITH_PROJECT_ID[i]].IsUseProjectID:=True;

  for i:=0 to High(TYPES_WITH_GEOREGION_ID) do
    ItemByType[TYPES_WITH_GEOREGION_ID[i]].IsHasGeoRegionID:=True;


 //  Assert(Assigned (g_Obj.ItemByName[OBJ_TEMPLATE_SITE_LINKEND] ));



end;


//end.


initialization
  g_Obj:=TObjectEngine.Create;

  Assert(Assigned (g_Obj.ItemByName[OBJ_TEMPLATE_SITE_LINKEND] ));

finalization
  FreeAndNil(g_Obj);
  
end.

