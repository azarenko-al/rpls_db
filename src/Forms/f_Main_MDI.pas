unit f_Main_MDI;

interface
{$I ver.inc}

uses

  Windows, Messages, Classes, Controls, Forms, SysUtils, ActnList, Menus,
  ExtCtrls, rxPlacemnt,  ComCtrls,
  d_Settings_WMS,

  I_Security,

  d_Objects_Deleted,
  u_config,

  u_reg,

  u_func_msg,
  u_const_msg,

  i_Options_,

  dm_Act_Map,

  dm_Main,

  u_func,
  u_files,

  dm_Act_Profile,

  I_Shell,

  f_Documents,
  f_Log,


  dm_ViewEngine,

  dm_Act_Forms,
  dm_Act_Project1,
  dm_Act_Project,

  u_types,

  u_const,
  u_geo,
  u_log,

  f_Explorer,

  cxSplitter, AppEvnts, dxBar, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxClasses, ImgList, dxSkinsCore,
  dxSkinsDefaultPainters;


type
  Tfrm_Main_MDI = class(TForm)
    act_Calc_3G: TAction;

    act_Browser: TAction;
    act_Change_Coord_system: TAction;
    act_DB_Connect: TAction;
    act_Exit: TAction;
    act_Explorer: TAction;
    act_Log: TAction;
    act_Map: TAction;
    act_Map_Rebuild_All: TAction;
    act_Map_Show_feature: TAction;
    act_Options: TAction;
    act_Profile: TAction;
    act_Project_Close: TAction;
    act_Project_Export: TAction;
    act_Project_Import: TAction;
    act_Project_New: TAction;
    act_Project_Open: TAction;
    act_ProjectExport_to_RPLS_DB_Guides: TAction;
    act_ProjectExport_to_RPLS_DB_Project: TAction;
    act_ProjectImport_from_RPLS_DB_Guides: TAction;
    act_ProjectImport_from_RPLS_DB_Project: TAction;
    act_ProjectImport_from_RPLS_XML_Project: TAction;
    act_Project_Import_from_SDB: TAction;
    act_Show_ErrorLog: TAction;
    actConnect1: TMenuItem;
    ActionList1: TActionList;
    actProjectNew1: TMenuItem;
    Asset1: TMenuItem;
    Export1: TMenuItem;
    img_Tools: TImageList;
    Import1: TMenuItem;
    MainMenu1: TMainMenu;
    mnu_Browser: TMenuItem;
    mnu_ErrorLog: TMenuItem;
    mnu_Exit: TMenuItem;
    mnu_Log: TMenuItem;
    mnu_Map: TMenuItem;
    mnu_Profile: TMenuItem;
    mnu_Project_Close: TMenuItem;
    mnu_Project_Open: TMenuItem;
    mnu_Windows: TMenuItem;
    N1: TMenuItem;
    N10: TMenuItem;
    N11: TMenuItem;
    N12: TMenuItem;
    N17: TMenuItem;
    N18: TMenuItem;
    N2: TMenuItem;
    N3: TMenuItem;
    N4: TMenuItem;
    N5: TMenuItem;
    N6: TMenuItem;
    N7: TMenuItem;
    N8: TMenuItem;
    N9: TMenuItem;
    pn_DockTop: TPanel;
    RPLSDB1: TMenuItem;
    RPLSDB2: TMenuItem;
    RPLSDB3: TMenuItem;
    RPLSDB4: TMenuItem;
    RPLSXML1: TMenuItem;
    StatusBar1: TStatusBar;
    TransportNet1: TMenuItem;
    act_Project_ExportToGoogle: TAction;
    Google1: TMenuItem;
    act_Project_Import_From_Excel1: TAction;
    act_Map1: TAction;
    act_Documents: TAction;
    est1: TMenuItem;
    dmActProject1ExecuteOnImportDone1: TMenuItem;
    pn_Explorer: TPanel;
    cxSplitter1: TcxSplitter;
    pn_Log: TPanel;
    cxSplitter2: TcxSplitter;
    act_User_Security: TAction;
    actUserSecurity1: TMenuItem;
    N14: TMenuItem;
    act_Service_Setup_link_calc_models: TAction;
    actServiceSetuplinkcalcmodels1: TMenuItem;
    Excel1: TMenuItem;
    act_Project_Import_KML: TAction;
    actProjectImportKML1: TMenuItem;
    ActionList2: TActionList;
    act_View_Simple: TAction;
    act_View_usual: TAction;
    N21: TMenuItem;
    N16: TMenuItem;
    N19: TMenuItem;
    act_Options_new: TAction;
    N13: TMenuItem;
    act_Import_SXF: TAction;
    N15: TMenuItem;
    SXF1: TMenuItem;
    NIOSS1: TMenuItem;
    FormStorage1: TFormStorage;
    ApplicationEvents1: TApplicationEvents;
    dxBarManager1: TdxBarManager;
    dxBarManager1Bar1: TdxBar;
    dxBarButton1: TdxBarButton;
    dxBarButton2: TdxBarButton;
    dxBarButton3: TdxBarButton;
    dxBarButton4: TdxBarButton;
    dxBarButton5: TdxBarButton;
    dxBarManager1Bar2: TdxBar;
    act_Settings_WMS: TAction;
    WMS1: TMenuItem;
    act_Objects_Deleted: TAction;
    N22: TMenuItem;
    procedure Action1111111Execute(Sender: TObject);
    procedure Action12222222222Execute(Sender: TObject);
    procedure FormCreate(Sender: TObject);

    procedure _Actions(Sender: TObject);

    procedure ActionList1Update(Action: TBasicAction; var Handled: Boolean);
    procedure act_Objects_DeletedExecute(Sender: TObject);
    procedure act_View_SimpleExecute(Sender: TObject);

    procedure Button1Click(Sender: TObject);
    procedure dmActProject1ExecuteOnImportDone1Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormDestroy(Sender: TObject);
    procedure FormMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
    procedure N20Click(Sender: TObject);

  private


  private

    FFormExplorer: Tfrm_Explorer;
    FFormLog: Tfrm_Log;

    FIniFileName: string;



    FCaption: string;

    FRegPath: string;

    FProcIndex: integer;

    procedure ShowStatusBar ();

    procedure ExecOnProjectChanged(Sender: TObject);

//    procedure MouseMoveDone;
//    procedure AppException(Sender: TObject; E: Exception);
//    procedure GetMessage(aMsg: integer; aParams: TEventParamList; var aHandled: Boolean);
  protected
//    procedure WMEXTERNAL_LIB_IMPORT_DONE(var Message: TMessage); message WM_EXTERNAL_LIB_IMPORT_DONE;
    procedure WMEXTERNAL_PROJECT_IMPORT_DONE(var Message: TMessage); message WM_EXTERNAL_PROJECT_IMPORT_DONE;


  public
//    procedure Init_Idle;
  end;

var
  frm_Main_MDI: Tfrm_Main_MDI;

//==================================================================
//==================================================================
implementation

 {$R *.dfm}

uses _dm_Main_Init;




const
  WM_FORM_CREATED = WM_USER + 1000;

  REGISTRY_COMMON_FORMS = 'Software\Onega\Common\Forms\';



procedure Tfrm_Main_MDI.Action1111111Execute(Sender: TObject);
begin

//    PostMessage (Application.Handle, Integer (WM_INTERFACE_SIMPLE), 0,0);
//    PostMessage (Application.Handle, Integer (WM_INTERFACE_NORMAL), 0,0);

end;

procedure Tfrm_Main_MDI.Action12222222222Execute(Sender: TObject);
begin

end;

//-------------------------------------------------------------------
procedure Tfrm_Main_MDI.FormCreate(Sender: TObject);
//-------------------------------------------------------------------
var
  id: integer;
  v: Variant;
  //b: Boolean;

begin
 // Panel2.Align:=alClient;


{  Assert(Assigned(IMapAct));
  Assert(Assigned(IActProfile));
}
  Assert ( not FormStorage1.Active );


  // -------------------------------------------------------------------
  FRegPath:=REGISTRY_COMMON_FORMS + ClassName + '\';

  FormStorage1.IniFileName:=FRegPath;
  FormStorage1.Active := True;



  FCaption:= APPLICATION_CAPTION1 + GetAppVersionStr() + '  ������: '
    + FormatDateTime ('yyyy-mm-dd hh:mm', GetFileDate (Application.Exename));

  FCaption:= APPLICATION_CAPTION1 + GetAppVersionStr(); // + '  ������: '
//    + FormatDateTime ('yyyy-mm-dd hh:mm', GetFileDate (Application.Exename));


   Caption:=FCaption; 

/////  act_DB_Connect.Visible:= false;

  act_Project_new.Caption   :=  '�������';
  act_Project_Open.Caption  :=  '�������';
  act_Project_Close.Caption :=  '�������';
  act_Exit.Caption          :=  '�����';
  act_Project_Import.Caption:=  '������';
  act_Project_Export.Caption:=  '�������';

  act_Log.Caption           :=  '������ �������';
  act_Explorer.Caption      :=  '���������';
  act_Browser.Caption       :=  'Browser';
  act_Map.Caption           :=  '�����';
  act_Profile.Caption       :=  '�������';

  act_Options.Caption       :=  '���������';
  act_Browser.Caption       :=  '������';
  act_DB_Connect.Caption    :=  '���������� � ��';





//  mnu_Map_rebuild.Action:=dmAct_Project.act_Service_Map_Rebuild;

  act_Log.Execute;
  act_Explorer.Execute;
//  act_Browser.Execute;

  SetComponentActionsExecuteProc (Self, _Actions);


 // Perform(WM_FORM_CREATED, 0,0);
  PostMessage (Handle, WM_FORM_CREATED, 0,0);

  ShowStatusBar;

//
// {$IFDEF gsm}
//   tb_GSM.Visible := True;
// {$ENDIF}

  dmAct_Project.OnProjectChanged1:=ExecOnProjectChanged;

//////////
  CreateChildForm(Tfrm_Explorer,  FFormExplorer,  pn_Explorer);
  CreateChildForm(Tfrm_Log,  FFormLog,  pn_Log);




  v:=FormStorage1.StoredValues.StoredValue['act_View_Simple.Checked'];

//  if not VarIsNull(v) then
//    act_View_Simple.Checked:= As v;

 //   FDataset_obj.Locate(FLD_OBJNAME, v, []);


  FIniFileName:= ChangeFileExt(Application.ExeName, '.ini');


  act_View_Simple.Checked:= ini_ReadBool(FIniFileName, act_View_Simple.Name, 'checked', False);

  if act_View_Simple.Checked then
    act_View_Simple.Execute  else

  if act_View_usual.Checked then
    act_View_usual.Execute;



//  Init_Idle;

//  Application.CreateForm(Tfrm_Idle, frm_Idle);



//  FProcIndex:=RegisterProc (GetMessage, Name);
end;



//-------------------------------------------------------------------
procedure Tfrm_Main_MDI.FormDestroy(Sender: TObject);
//-------------------------------------------------------------------
begin
  FormStorage1.StoredValues.StoredValue['act_View_Simple.Checked']:=act_View_Simple.Checked;


  ini_WriteBool(FIniFileName, act_View_Simple.Name, 'checked', act_View_Simple.Checked);


//  act_View_Simple



  FreeAndNil(FFormExplorer);
  FreeAndNil(FFormLog);

  inherited;

//  FreeAndNil (frm_Idle);


 // UnRegisterProc (FProcIndex);
end;

//-------------------------------------------------------------------
procedure Tfrm_Main_MDI._Actions(Sender: TObject);
//-------------------------------------------------------------------
{
    procedure DoOptions();
    var
      m: HResult;
      s: string;
    //  o: TOptionsX;
  //    vIOptions: IOptionsX;
    begin
     // Map1.Layers.Add (FilenameEdit1.FileName, EmptyParam);
     // s:=vLayer.Name;

//      vIOptions:=CoOptionsX.Create;
      s:=dmMain.GetConnectionString;

      m:=IOptions.Dlg(dmMain.GetConnectionString);


      if m=S_OK then
        PostMessage (Application.Handle, Integer(MW_THEME_CHANGED), 0,0);
      //  vIOptions.Apply_Theme(Map1.DefaultInterface);

    end;
}



var
  iNetConfigID: Integer;
//  oFormExplorer: Tfrm_Explorer;
  oForm: TForm;
  k: Integer;
  s: string;
begin
  if Sender=act_Settings_WMS then
    Tdlg_Settings_WMS.ExecDlg else


  if Sender=act_Import_SXF then
    dmAct_Project1.Import_SXF else


{
type
  TMyMesssage_ =
    (
      WM_TEST = WM_USER + 6000 + 100,
      WM_INTERFACE_NORMAL,
      WM_INTERFACE_SIMPLE
    );
 }

 {
  if Sender=act_Options_new then
  begin
    DoOptions;
  end else
 }

  // ---------------------------------------------------------------
  if Sender=act_View_Simple then
  // ---------------------------------------------------------------
  begin
    act_View_usual.Checked:=false;
    act_View_Simple.Checked:=true;
    g_Config.IsInterfaceSimple:=True;

    g_EventManager.PostEvent_(et_INTERFACE_SIMPLE, []);
//    PostMessage (Application.Handle, Integer (WM_INTERFACE_SIMPLE), 0,0);
  end else

  // ---------------------------------------------------------------
  if Sender=act_View_usual then
  // ---------------------------------------------------------------
  begin
    act_View_Simple.Checked:=false;
    act_View_usual.Checked:=true;
    g_Config.IsInterfaceSimple:=False;

    g_EventManager.PostEvent_(et_INTERFACE_NORMAL, []);
//    PostMessage (Application.Handle, Integer (WM_INTERFACE_NORMAL), 0,0);
  end else


  if Sender=act_Service_Setup_link_calc_models  then
  begin
    s:= GetApplicationDir + 'link_calc_model_setup\RRL_Params.exe';
    ShellExec(s);
  end else

  if Sender=act_User_Security then
  begin
    Init_ISecurityX.Dlg(dmMain.GetConnectionString)
//    Tdlg_User_security.ExecDlg(dmMain.ADOConnection) ;

  end else


  if Sender=act_Documents then begin
    Tfrm_Documents.CreateSingleForm();
  end else


  if Sender=act_Exit                then Close else

//  if Sender=act_Options             then
//    dmAct_Forms.ShowForm_Options else

  if Sender=act_Change_Coord_system then dmAct_Project1.ChangeCoordSystDlg() else

  if Sender=act_Project_New         then dmAct_Project.NewDlg() else
  if Sender=act_Project_Open        then dmAct_Project.dlg_Browse else
  if Sender=act_Project_Close       then dmAct_Project.Close(True) else

  if Sender=act_Profile             then dmAct_Profile.ShowForm else

  if Sender=act_Map1                then dmAct_Map.CreateNewForm else

  if Sender=act_Map then
  begin
    g_EventManager.PostEvent_(et_MAP_UPDATE_STOP,  []);
    dmAct_Map.ShowForm;

    Application.ProcessMessages;
    g_EventManager.PostEvent_(et_MAP_UPDATE_START,  []);


  end else

  if Sender=act_Browser then
  begin
    g_EventManager.PostEvent_(et_MAP_UPDATE_STOP,  []);
    dmAct_Forms.ShowForm_Browser ();

    Application.ProcessMessages;
    g_EventManager.PostEvent_(et_MAP_UPDATE_START,  []);

  end else



//..  if Sender=act_Project_Import_from_SDB              then dmAct_Project1.ImportFromSDB else

  if Sender=act_Project_Import_From_Excel1           then dmAct_Project1.ImportFromExcel else


  if Sender=act_Project_Import_KML  then dmAct_Project1.ImportKML else


  if Sender=act_ProjectImport_from_RPLS_DB_Project  then dmAct_Project1.ImportFromRplsDbProject else
  if Sender=act_ProjectImport_from_RPLS_DB_Guides   then dmAct_Project1.ImportFromRplsDbGuides else

  if Sender=act_ProjectImport_from_RPLS_XML_Project then
     dmAct_Project1.ImportFromRplsXmlProject else

  if Sender=act_Project_ExportToGoogle              then dmAct_Project1.Dlg_ExportToGoogle() else

  if Sender=act_ProjectExport_to_RPLS_DB_Project    then dmAct_Project1.ExportToRplsDbProject else
  if Sender=act_ProjectExport_to_RPLS_DB_Guides     then dmAct_Project1.ExportToRplsDbGuides else

//  if Sender=act_Project_ImportFromExcel             then dmAct_Project1.Dlg_ImportFromExcel else


  if Sender=act_Show_ErrorLog
    then g_Log.ShowFile else


  if Sender=act_DB_Connect then
  begin
    if dmMain.OpenDB_dlg then
    begin
      dmMain_Init_link.AfterOpenDatabase();

     // dmUser_Security.Open;

      {
      // -------------------------
      TdmDB_check1.Init;
      dmDB_check1.Exec;
      FreeAndNil(dmDB_check1);
      // -------------------------
     }

      g_Obj.ReAssign;
//      dmUserAuth.Load;
      dmAct_Project.Close;
      dmAct_Project.LoadLastProject;

      dmViewEngine.Clear;
    end;

  end else

  begin
    if IsFormExists (Application, Tfrm_Explorer.ClassName)<>nil then
      Exit;

  //  if Sender=act_Calc     then dmAct_CalcRegion.Calc(0) else

    if Sender=act_Explorer then
    begin

 (*     k := pn_DockLeft.Width;

    //  oFormExplorer:=Tfrm_Explorer.CreateParented(pn_DockLeft.Handle); //(Application);

      //(Application);

      oFormExplorer:=Tfrm_Explorer.CreateForm;
      k := oFormExplorer.Width;

      oFormExplorer.ManualDock(pn_DockLeft);

      k := pn_DockLeft.Width;
      if pn_DockLeft.Width<30 then
        pn_DockLeft.Width:=200;*)

    end;
  end ;
end;


//------------------------------------------------------------------
procedure Tfrm_Main_MDI.ShowStatusBar;
//------------------------------------------------------------------
begin
  assert(Assigned(IOptions));

 // if not Assigned(IOptions) then
  //  Exit;


  //������� ���������                  `
  StatusBar1.Panels[0].Text:= geo_GetCoordSysStr(IOptions.Get_CoordSys);

  StatusBar1.Panels[2].Text:= dmMain.LoginRec.ConnectionStatusStr;// GetConnectionStatusStr;

end;

//------------------------------------------------------------------
procedure Tfrm_Main_MDI.ActionList1Update(Action: TBasicAction; var Handled: Boolean);
//------------------------------------------------------------------
begin
  ShowStatusBar();
end;

procedure Tfrm_Main_MDI.act_Objects_DeletedExecute(Sender: TObject);
begin
  Tdlg_Objects_Deleted.ExecDlg
  
end;

procedure Tfrm_Main_MDI.act_View_SimpleExecute(Sender: TObject);
begin

end;


// ---------------------------------------------------------------
procedure Tfrm_Main_MDI.dmActProject1ExecuteOnImportDone1Click(Sender: TObject);
begin
  g_ShellEvents.Shell_ReLoad_Project;
 // g_Shell.Shell_ReLoad_Project;
end;


procedure Tfrm_Main_MDI.ExecOnProjectChanged(Sender: TObject);
begin
  Caption:=FCaption + ' - '+ dmMain.ProjectName;
 // Format(APPLICATION_CAPTION, [GetAppVersionStr, dmAct_Project.GetCurrentProjectName]);

end;

procedure Tfrm_Main_MDI.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action:=caFree;

end;

procedure Tfrm_Main_MDI.FormMouseMove(Sender: TObject; Shift: TShiftState; X,
    Y: Integer);
begin

 ////// dmIdle.
//  MouseMoveDone;

end;

procedure Tfrm_Main_MDI.WMEXTERNAL_PROJECT_IMPORT_DONE(var Message: TMessage);
begin
  dmAct_Project1.ExecuteOnProjectImportDone;
  // ShowMessage('WM_EXTERNAL_IMPORT_DONE');
end;



procedure Tfrm_Main_MDI.Button1Click(Sender: TObject);
begin
  PostMessage(Application.MainForm.Handle, WM_CLOSE, 0,0);

end;

procedure Tfrm_Main_MDI.N20Click(Sender: TObject);
begin
  Tdlg_Objects_Deleted.ExecDlg
end;



end.


(*



{

procedure Tfrm_Main_MDI.ApplicationEvents1Message(var Msg: tagMSG; var Handled:
    Boolean);
begin
 // debug ('procedure Tfrm_Main_MDI.ApplicationEvents1Message(var Msg: tagMSG; var Handled: - '+ TimeToStr (FLastHookTime));


{
  case Msg.message of
    WM_MOUSEMOVE:// dmIdle.
                   MouseMoveDone;
  end;
}
end;





procedure Tfrm_Main_MDI.Timer1Timer(Sender: TObject);


    function DoLastInput: DWord;
    var
      LInput: TLastInputInfo;
    begin
      LInput.cbSize := SizeOf(TLastInputInfo);
      GetLastInputInfo(LInput);
      Result := GetTickCount - LInput.dwTime;
    end;

{
    //Example:
    procedure TForm1.Timer1Timer(Sender: TObject);
    begin
      Label1.Caption := Format('System Idle since %d ms', [LastInput]);
    end;
}

var
  dt: TDateTime;
  iMS: Integer;
  s: string;
  sFile: string;
begin

{
var ms : integer;
dt : tDateTime;
begin
ms := 60000;
dt := ms / 1000 / SecsPerDay;
caption := FormatDateTime('hh:nn:ss', dt);
end;
--
-Mike
}

//  iMS:=DoLastInput();

//  dt:=EncodeTime(0, 0, iMS div 1000, iMS mod 1000 );

//  s:= FormatDateTime('hh:nn:ss', dt) ;

//  Edit1.Text := s;

  dt := DoLastInput() / 1000 / SecsPerDay;


  Edit1.Text := FormatDateTime('hh:nn:ss', dt);

//  Edit1.Text := Format('System Idle since %d ms', [DoLastInput()]);


//  Edit1.Text:=FormatDateTime('hh:nn:ss', Now() - FLastHookTime); // DiffTimeStr:=

//  Edit1.Text:=FormatDateTime('hh:nn:ss', Now() - dmIdle.FLastHookTime); // DiffTimeStr:=

  exit;


  if CompareTime(Now() - FLastHookTime, EncodeTime(0, Fidle_time_minutes, 0, 0)) > 0 then
  begin
    Timer1.Enabled:=False;

//    ShowMessage( IntToStr(dmIdle.Fidle_time_minutes) + ' minute');

    s:= FormatDateTime('hh:nn:ss', Now() - FLastHookTime) ;

    sFile:=GetTempFileNameWithExt('.txt');

    StrToFile(s, sFile);

    ShellExecute(0, 'open', pChar(sFile), nil, nil, SW_SHOWNORMAL);




  PostMessage(Application.MainForm.Handle, WM_CLOSE, 0,0);


//    Halt;
  end;

//  ed_Minutes.Text:= FormatDateTime('hh:nn:ss', myDate);

end;




// ---------------------------------------------------------------
procedure Tfrm_Main_MDI.Init_Idle;
// ---------------------------------------------------------------
var
  oIni: TIniFile;
  s: string;
  sFile: string;

begin
//  FLastHookTime:=Now();

 // s:=ChangeFileExt( Application.ExeName, '.ini');

  sFile:=ChangeFileExt( Application.ExeName, '.ini');
  Assert (FileExists(sFile));

  //-----------------------------------------------

  oIni := TIniFile.Create (sFile);

  Fidle_time_minutes:= oIni.ReadInteger ('IDLE', 'time_minutes', 1);
  Fidle_msg         := oIni.ReadString  ('IDLE', 'msg', '������ ��������� ��������� ��-�� ������������ %s');

  FreeAndNil(oIni);


//   ShowMessage( IntToStr(Fidle_time_minutes) + ' minute');


end;

