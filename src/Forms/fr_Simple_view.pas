unit fr_Simple_view;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
  Dialogs, rxPlacemnt, ExtCtrls, DB, ADODB, ComCtrls, Menus, ActnList, DBCtrls, StdCtrls,
  fr_base_View,
  u_func,
  u_dlg,
  u_db,
  fr_DBInspector,
  ToolWin, fr_View_base, ImgList,
  cxPropertiesStore, dxBar, cxBarEditItem, cxClasses, cxLookAndFeels
  ;

type
  Tframe_Simple_View = class(Tframe_View_Base)
    ADOQuery1: TADOQuery;
    pn_Inspector: TPanel;
    Label1: TLabel;
    procedure FormCreate(Sender: TObject);
  private
    FTableName: string;
    Ffrm_DBInspector: Tframe_DBInspector;
    { Private declarations }
  public
    procedure SetObjectName(aName: string); override;

    procedure View (aID: integer); override;
    { Public declarations }
  end;

var
  frame_Simple_View: Tframe_Simple_View;

implementation

uses dm_Main;

{$R *.dfm}

procedure Tframe_Simple_View.View (aID: integer);
begin
  inherited View(aID);

  db_OpenQuery(ADOQuery1, FTableName, aID );

  Ffrm_DBInspector.DoAfterView;
end;


procedure Tframe_Simple_View.SetObjectName(aName: string);
begin
  FTableName:=aName;
  Ffrm_DBInspector.SetObjectName(FTableName);
  Ffrm_DBInspector.PrepareDataSetFields (ADOQuery1);

end;


//--------------------------------------------------------------------
procedure Tframe_Simple_View.FormCreate(Sender: TObject);
//--------------------------------------------------------------------
begin
  pn_Inspector.Align:=alClient;

  pn_Header.Height:=CoolBar2.Height+2;

 // if not Assigned(dmLink) then
   // dmLink:=TdmLink.Create (Application) ;

//  pn_Inspector.Align:=alClient;

  Ffrm_DBInspector:=Tframe_DBInspector.Create(Self);
  CopyControls(Ffrm_DBInspector, pn_Inspector); //tor );

  Ffrm_DBInspector.DataSet:=ADOQuery1;

  {

  }

//  Caption:='�� ��������';
end;

end.