unit dm_Link_map;

interface
{$I ver.inc}

//{$DEFINE wms}

uses
  SysUtils, Classes, Variants, Forms, Db, ADODB,  Graphics,
  MapXLib_TLB,

  u_vars,           

//  u_Link_report_vars,

  dm_Onega_DB_data,

  u_MapDesktop_classes,

  u_ini_Link_report_params,

  dm_Main,

  d_MIF_export,

  u_Mapinfo_WOR,

  u_classes,
  u_func,
  u_db,
  u_img,
  u_files,

  u_GEO,

  u_MapX,
  u_MapX_lib,
  u_mapx_func,
//  u_mitab,

  //used for I_Options

  u_const_db,

  dm_LinkLine,

  dm_Map_Desktop
  ;

type
  TdmLink_map_ = class(TDataModule)
    qry_Properties: TADOQuery;
    qry_Temp1: TADOQuery;
    qry_Maps: TADOQuery;
    ADOStoredProc_Link: TADOStoredProc;
    ADOQuery1: TADOQuery;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  private
    FMIF_form: Tdlg_MIF_export1;
               
    FMap: TMap;

    FStyle: TmiStyleRec;

    FPropIDList: TIDList;

    FMapDesktopItem: TMapDesktopItem;

    function AssignLineStyle: TmiStyleRec;
    function AssignFontStyle: TmiStyleRec;

    procedure InitMap;

    procedure Link_SaveToImage_(aID: integer; aFileName: string);

    procedure LinkLine_SaveToImage_(aID: integer; aFileName: string; aLinkIDList:
        TIDList);

  public
    class function Init_: TdmLink_map_;

    class procedure Link_SaveToImage(aID: integer; aFileName: string);
    class procedure LinkLine_SaveToImage(aID: integer; aFileName: string;
        aLinkIDList: TIDList);
  end;


var
  dmLink_map_: TdmLink_map_;



//====================================================================
implementation
 {$R *.dfm}

uses
  dm_Link_report;


const
  IMAGE_WIDTH =19; // in sm
  IMAGE_HEIGHT=12;



class function TdmLink_map_.Init_: TdmLink_map_;
begin
  if not Assigned(dmLink_map_) then
    dmLink_map_ := TdmLink_map_.Create(Application);

  Result:=dmLink_map_;
end;

// ---------------------------------------------------------------
procedure TdmLink_map_.DataModuleCreate(Sender: TObject);
// ---------------------------------------------------------------
begin
  inherited;

//  Options_Init__1;


//  FLinkIDList:=TIDList.Create;
  FPropIDList:=TIDList.Create;

  TdmMap_Desktop.Init;

  db_SetComponentADOConnection(Self, dmMain.AdoConnection);


  FMIF_form:=Tdlg_MIF_export1.Create(nil);


  FMap:=FMIF_form.Map1;

//  FMapEngine:=TMapEngine.create(FMap);

  //  WMS_Init;
//  end;

 // FMap.Layers.RemoveAll;



{
  FTileManager:=TTileManager.Create;

  FTileManager.Set_Dir(g_ApplicationDataDir + 'WMS');
//  FTileManagerX.Set_Dir(IncludeTrailingBackslash(Trim(DirectoryEdit_WMS.Text)));

//  WMS.LayerName:='GoogleMap';

  FTileManager.Set_WMS_Layer('YandexSat');


}

end;

// ---------------------------------------------------------------
procedure TdmLink_map_.DataModuleDestroy(Sender: TObject);
// ---------------------------------------------------------------
begin
//  FreeAndNil (FTileManager);
//  FreeAndNil (FMapEngine);

  FreeAndNil (FMIF_form);

//  FreeAndNil(FMap);

//  FreeAndNil(FLinkIDList);
  FreeAndNil(FPropIDList);

  inherited;
end;



//------------------------------------------------------
procedure TdmLink_map_.InitMap;
//------------------------------------------------------
var
  i: integer;

  j: integer;
  s: string;
  s1: string;
  sFileName: string;

  vLayer: CMapXLayer;

begin

  FMap.Layers.RemoveAll;


 // WMS_Update;

//  Assert(g_Link_Report_Param.MapDesktopID>0, 'Value <=0');

  dmMap_Desktop.Open;
  dmMap_Desktop.Prepare_GeoMaps   ();
  dmMap_Desktop.Prepare_ObjectMaps();

  FMapDesktopItem:=dmMap_Desktop.MapDesktopItem;

//  ShowMessage('FMapDesktopItem.GeoMapList.Count: '+ IntToStr(FMapDesktopItem.GeoMapList.Count));

//  for I := FMapDesktopItem.GeoMapList.Count - 1 downto 0 do
  //for I := 0 to FMapDesktopItem.GeoMapList.Count - 1 do
  for I := 0 to FMapDesktopItem.MapList.Count - 1 do
 //   if FMapDesktopItem.GeoMapList[i].Visible then
    if FMapDesktopItem.MapList[i].MapKind=mkGeoMap then
  begin
//    sFileName :=FMapDesktopItem.GeoMapList[i].FileName;
    sFileName :=FMapDesktopItem.MapList[i].FileName;

   // if not FileExists(sFileName) then
    //  ShowMessage('not FileExists(sFileName) - '+ sFileName);

    if FileExists(sFileName) then
    begin
      vLayer := FMap.Layers.Add (sFileName, 0); //0

   //   s:=vLayer.FileSpec;
    //  s1:=vLayer.Name;

    //  s:=vLayer.Name;
    end;
//          sFilename:=vLayer.FileSpec;


//      FMap.Layers.Add (sFileName, FMap.Layers.Count); //0
  end;
end;



//--------------------------------------------------------------------
procedure TdmLink_map_.Link_SaveToImage_(aID: integer; aFileName: string);
//--------------------------------------------------------------------
var
  sPath: string;
  sGeoFileName: string;
  i, iCurDesktop: Integer;

  blVector: TBLVector;
  iLinkEnd1, iLinkEnd2, iProp1, iProp2: integer;
  blPoint1: TBlPoint;
  blPoint2: TBlPoint;
  bPoint1_is_left: Boolean;
  bPoint1_is_top: Boolean;

//  eDeltaL: Double;
  iProperty1: Integer;
  iProperty2: Integer;

  sTabFileName, sFile: string;

  vLayer_prop1: CMapXLayer;
  vLayer_prop2: CMapXLayer;
  vLayer_link: CMapXLayer;

//  rec: TExportMapRec;

  map_view_rec: TMapViewAddRec;
//  oMap: TmitabMap;
//  oMap: TmiMap;

  oMap_prop1: TmiMap;
  oMap_prop2: TmiMap;
  oMap_link: TmiMap;

  sName: string;
  rBLRect: TBLRect;

  oMapItem: TMapItem;
  s: string;
  sFileName: string;

  sFileName_WOR: string;
  sTabFileName_link: string;
  sTabFileName_prop1: string;
  sTabFileName_prop2: string;
 // sWorFileName: string;

begin
  InitMap();

////////  Test();

  // -------------------------


 //////// Assert(Assigned(IOptions), 'Value not assigned');

  CursorHourGlass;

 // FPropIDList:=TIDList.Create;

//  FPropIDList.Clear;
//  FLinkIDList.Clear;

//  oCheckedFileList:= TStringList.Create;

  sTabFileName:=ChangeFileExt(aFileName, '.tab');

//  sTabFileName_prop:=ChangeFileExt(aFileName, '_prop.tab');
  sTabFileName_prop1:=ChangeFileExt(aFileName, '_prop1.tab');
  sTabFileName_prop2:=ChangeFileExt(aFileName, '_prop2.tab');
  sTabFileName_link:=ChangeFileExt(aFileName, '_link.tab');

//  oMap:=TmitabMap.Create;
  oMap_prop1:=TmiMap.Create;
  oMap_prop2:=TmiMap.Create;
  oMap_link:=TmiMap.Create;

  oMap_prop1.CreateFile1 (sTabFileName_prop1,
                   [mapX_Field (FLD_ID,   miTypeInt),
                    mapX_Field (FLD_NAME, miTypeString)
                   ]);

  oMap_prop2.CreateFile1 (sTabFileName_prop2,
                   [mapX_Field (FLD_ID,   miTypeInt),
                    mapX_Field (FLD_NAME, miTypeString)
                   ]);

  oMap_link.CreateFile1 (sTabFileName_link,
                   [mapX_Field (FLD_ID,   miTypeInt),
                    mapX_Field (FLD_NAME, miTypeString)
                   ]);


  dmOnega_DB_data.Link_Select(ADOStoredProc_Link, aID);

  iProperty1:=ADOStoredProc_Link.FieldByName(FLD_PROPERTY1_ID).AsInteger;
  iProperty2:=ADOStoredProc_Link.FieldByName(FLD_PROPERTY2_ID).AsInteger;

//  FPropIDList.AddID(iProperty1);
//  FPropIDList.AddID(iProperty2);

  if dmLink_report.FLinkData.LinkRepeater.property_id>0 then
    FPropIDList.AddID(dmLink_report.FLinkData.LinkRepeater.property_id);


//  dmLink_report.FLinkData.LinkRepeater.property_id
//   LinkRepeater_LoadFromDataset(q_LInk_repeater);



  FStyle := AssignLineStyle; // (IOptions.GetDefaultStyle(OBJ_LINK ));

  oMap_link.PrepareStyle(miFeatureTypeLine, FStyle);


  if dmLink_report.FLinkData.LinkRepeater.property_id>0 then
  begin
    blVector:=dmLink_report.FLinkData.LinkRepeater.Part1.BLVector;
 //   oMap.WriteVector (blVector, [mapx_Par(FLD_ID,  aID) ]);
    oMap_link.WriteVector (blVector, []);

    blVector:=dmLink_report.FLinkData.LinkRepeater.Part2.BLVector;
//    oMap.WriteVector (blVector, [mapx_Par(FLD_ID,  aID) ]);
    oMap_link.WriteVector (blVector, []);

  end else
  begin
    blVector :=db_ExtractBLVector (ADOStoredProc_Link); //  qry_Temp
//    oMap.WriteVector (blVector, [mapx_Par(FLD_ID,  aID) ]);
    oMap_link.WriteVector (blVector, []);

    {
    FillChar(FStyle, SizeOf(FStyle), 0);

    FStyle.FontName:='Map army';
    FStyle.Character:=52;

    FStyle.FontSize:=48;
    FStyle.FontColor:=ClRed;


 //   FStyle.SymbolFontRotation:=15;
    FStyle.SymbolFontRotation:=180-Round(geo_Azimuth(blVector.Point1, blVector.Point2));
//    FStyle.r

    oMap.PrepareStyle(miFeatureTypeSymbol, FStyle);
    oMap.WriteSymbol (blVector.Point1, []);

  //  FStyle.SymbolFontRotation:=180 + 15;
    FStyle.SymbolFontRotation:=180-Round(geo_Azimuth(blVector.Point2, blVector.Point1));

    oMap.WriteSymbol (blVector.Point2, []);
    }
  end;



  FStyle := AssignFontStyle; // (IOptions.GetDefaultStyle(OBJ_Property));



//    MI_FONT_MAPINFO_SYMBOLS= 'MapInfo Symbols';
//  MI_SYMBOL_STAR = 36;

//  FStyle.SymbolFontRotation:=

  oMap_prop1.PrepareStyle(miFeatureTypeSymbol, FStyle);
  oMap_prop2.PrepareStyle(miFeatureTypeSymbol, FStyle);


//  for i := 0 to FPropIDList.Count-1 do
//  begin
//--------------------------------------------------------------------
// 1
//--------------------------------------------------------------------
  db_OpenTableByID(qry_Properties, TBL_PROPERTY, iProperty1);
  blPoint1:=db_ExtractBLPoint(qry_Properties);
  sName :=qry_Properties[FLD_NAME];

  oMap_prop1.WriteSymbol (blPoint1, //FStyle,
                 [
                    mapx_Par(FLD_ID,   iProperty1),
                    mapx_Par(FLD_NAME, sName)
                 ]);

//--------------------------------------------------------------------
// 2
//--------------------------------------------------------------------
  db_OpenTableByID(qry_Properties, TBL_PROPERTY, iProperty2);
  blPoint2:=db_ExtractBLPoint(qry_Properties);
  sName :=qry_Properties[FLD_NAME];

  oMap_prop2.WriteSymbol (blPoint2, //FStyle,
                 [
                    mapx_Par(FLD_ID,   iProperty2),
                    mapx_Par(FLD_NAME, sName)
                 ]);

 // end;

  FreeAndNil(oMap_prop1);
  FreeAndNil(oMap_prop2);
  FreeAndNil(oMap_link);


  vLayer_prop1:= FMap.Layers.Add (sTabFileName_prop1, 1);
  vLayer_prop2:= FMap.Layers.Add (sTabFileName_prop2, 2);
  vLayer_link := FMap.Layers.Add (sTabFileName_link, 3);


//  oMapItem:=FMapDesktopItem.ObjectMapList.FindByObjectName('property');
  oMapItem:=FMapDesktopItem.MapList.FindByObjectName('property');
  if Assigned(oMapItem) then
  begin
    oMapItem.LabelProperties.SaveToMapinfoLayer(vLayer_prop1);
    oMapItem.LabelProperties.SaveToMapinfoLayer(vLayer_prop2);
  end;


  oMapItem:=FMapDesktopItem.MapList.FindByObjectName('link');
  if Assigned(oMapItem) then
    oMapItem.LabelProperties.SaveToMapinfoLayer(vLayer_link);


//  vLayer.LabelProperties.Position:= 2;
  vLayer_prop1.AutoLabel:= True;
  vLayer_prop2.AutoLabel:= True;

  //blPoint1
  bPoint1_is_left:=blPoint1.L < blPoint2.L;
  bPoint1_is_top :=blPoint1.B > blPoint2.B;

  //----------------------------------------------------------------
  if bPoint1_is_left then
  begin
    if bPoint1_is_top then vLayer_prop1.LabelProperties.Position :=miPositionTR
                      else vLayer_prop1.LabelProperties.Position :=miPositionBR;

    if bPoint1_is_top then vLayer_prop2.LabelProperties.Position :=miPositionBL
                      else vLayer_prop2.LabelProperties.Position :=miPositionTL;

  end else
  begin
    if bPoint1_is_top then vLayer_prop1.LabelProperties.Position :=miPositionTL
                      else vLayer_prop1.LabelProperties.Position :=miPositionBL;

    if bPoint1_is_top then vLayer_prop2.LabelProperties.Position :=miPositionBR
                      else vLayer_prop2.LabelProperties.Position :=miPositionTR;

  end;


  vLayer_prop1.LabelProperties.Style.TextFontOpaque:=true;// RegionColor
  vLayer_prop2.LabelProperties.Style.TextFontOpaque:=true;// RegionColor

  vLayer_prop1.LabelProperties.Style.TextFontBackColor:=clYellow;
  vLayer_prop2.LabelProperties.Style.TextFontBackColor:=clYellow;


  {

  v.Style.TextFontHalo;

// Constants for enum PositionConstants
type
  PositionConstants = TOleEnum;
const
  miPositionCC = $00000000;
  miPositionTL = $00000001;
  miPositionTC = $00000002;

  miPositionTR = $00000003;
  miPositionCL = $00000004;
  miPositionCR = $00000005;

  miPositionBL = $00000006;
  miPositionBC = $00000007;
  miPositionBR = $00000008;

  }

  // -------------------------------------------------------------------

//  sGeoFileName := GetTempFileName_('gst');

/////  sFileName := ChangeFileExt(aFileName, '.gst');
/////  FMap.SaveMapAsGeoset('',sFileName);

  sFileName_WOR := ChangeFileExt(aFileName, '.wor');
  u_Mapinfo_WOR.mapx_SaveToWOR(FMap, sFileName_WOR);


  FMap.AutoRedraw := False;


  mapx_Set_Width_Height(FMap, g_Link_Report_Param.ImageWidth,
                              g_Link_Report_Param.ImageHeight);


//  FMapEngine.
//  mapx_GetMapBounds(FMap);
//  eDeltaL:=blVector.Point1.L - blVector.Point1.L;


  mapx_SetCenter(FMap, geo_GetVectorCenter (blVector));
  mapx_SetBoundsForBLVector_for_print (FMap, BLVector);

//  if FMIF_form.params.WMS.Checked and (FMIF_form.params.WMS.IsUseZ) then


//{$IFDEF wms}
   if not g_Link_Report_Param.WMS.Checked then
   begin
     mapx_SetScale (FMap, g_Link_Report_Param.Scale / 100);

   end;

//  FMIF_form.params.WMS.Z         :=g_Link_Report_Param.WMS.Z;

  FMIF_form.params.WMS.checked      :=g_Link_Report_Param.WMS.Checked;
  FMIF_form.params.WMS.IsAutoScale  :=g_Link_Report_Param.WMS.IsAutoScale;
  FMIF_form.params.WMS.Z            :=g_Link_Report_Param.WMS.Z;
  FMIF_form.params.WMS.LayerName    :=g_Link_Report_Param.WMS.LayerName;
  FMIF_form.params.WMS.URL          :=g_Link_Report_Param.WMS.URL;
  FMIF_form.params.WMS.EPSG         :=g_Link_Report_Param.WMS.EPSG;
  FMIF_form.params.WMS.Dir          :=g_ApplicationDataDir + 'WMS';


  FMIF_form.ExportMap_new(aFileName);



  if (not g_Link_Report_Param.WMS.Checked) then
    img_InsertScaleRuleIntoBMP (aFileName, aFileName, g_Link_Report_Param.Scale);


  DeleteFile(sGeoFileName);

  // -------------------------------------------------------------------

  CursorDefault;

end;





//--------------------------------------------------------------------
procedure TdmLink_map_.LinkLine_SaveToImage_(aID: integer; aFileName: string;
    aLinkIDList: TIDList);
//--------------------------------------------------------------------
var
  sGeoFileName: string;
  sTabFileName_link: string;
  sTabFileName_site: string;
  i, iCurDesktop: Integer;

  blVector: TBLVector;
  oBLPoint: TBlPoint;
  rLinkLineRect: TBLREct;
  iLinkEnd1, iLinkEnd2, iProp1, iProp2: integer;

  iFileHandle: integer;
  iID: Integer;
  iProperty1: Integer;
  iProperty2: Integer;
  sFile: string;

  vLayer: CMapXLayer;

//  oMap: TmitabMap;
  oMap: TmiMap;

  rec: TExportMapRec;
  sName: string;

  oMapItem: TMapItem;
  sWorFileName: string;

  BLPoint: TBLPoint;

begin
//////  Assert(Assigned(IOptions), 'Value not assigned');

  InitMap();

  // -------------------------


  FPropIDList.Clear;
//  FLinkIDList.Clear;

//  oCheckedFileList:= TStringList.Create;

  sTabFileName_site:=ChangeFileExt(aFileName, '_site.tab');
  sTabFileName_link:=ChangeFileExt(aFileName, '_link.tab');

 // mapx_DeleteFile (sTabFileName);


//  oMap:=TmitabMap.Create;
  oMap:=TmiMap.Create;

  oMap.CreateFile1 (sTabFileName_link,
                   [
                    mapX_Field (FLD_ID,   miTypeInt)
                   // mapX_Field (FLD_NAME, miTypeString)
                    ]);


//  dmLinkLine.GetLinkListByLinkLineID(aID, FLinkIDList);


  FStyle :=  AssignLineStyle ; //(IOptions.GetDefaultStyle(OBJ_LINK) );

  for i := 0 to aLinkIDList.Count-1 do
  begin
    iID:=aLinkIDList[i].ID;

    dmOnega_DB_data.Link_Select(ADOStoredProc_Link, iID);

    iProperty1:=ADOStoredProc_Link.FieldByName(FLD_PROPERTY1_ID).AsInteger;
    iProperty2:=ADOStoredProc_Link.FieldByName(FLD_PROPERTY2_ID).AsInteger;
    FPropIDList.AddID(iProperty1);
    FPropIDList.AddID(iProperty2);

    blVector:=db_ExtractblVector(ADOStoredProc_Link);


//////////    FStyle.LineWidth :=3;

    oMap.PrepareStyle(miFeatureTypeLine, FStyle);

    oMap.WriteVector (blVector, // FStyle,
                   [
                    mapx_Par(FLD_ID, iID)
                   ]);

  end;




  oMap.CloseFile;

  oMap.CreateFile1 (sTabFileName_site,
                   [
                    mapX_Field (FLD_ID,   miTypeInt),
                    mapX_Field (FLD_NAME, miTypeString)
           //         mapX_Field (FLD_ADDRESS, miTypeString)
//                    mapX_Field (FLD_LAT_STR, miTypeString),
  //                  mapX_Field (FLD_LON_STR, miTypeString)
                   ]);

  FStyle := AssignFontStyle (); //IOptions.GetDefaultStyle(OBJ_Property)

  for i := 0 to FPropIDList.Count-1 do
  begin
    db_OpenTableByID(qry_Properties, TBL_PROPERTY, FPropIDList[i].ID);

    oBLPoint:=db_ExtractBLPoint(qry_Properties);

(*    oBLPoint.B:=qry_Temp[FLD_LAT];
    oBLPoint.L:=qry_Temp[FLD_LON];
*)



//    FStyle.Character := DEF_SITE_Character;
//    FStyle.FontSize := FStyle.FontSize + 3;


    sName :=qry_Properties[FLD_NAME];
  //  sName :='11111\n222222\n3333';

    oMap.PrepareStyle(miFeatureTypeSymbol, FStyle);
                                  
    oMap.WriteSymbol (oBLPoint, //FStyle,
                   [
                    mapx_Par(FLD_ID,      qry_Properties[FLD_ID]),
//                    mapx_Par(FLD_NAME,    qry_Temp[FLD_NAME]),
                    mapx_Par(FLD_NAME,    sName)
               //     mapx_Par(FLD_ADDRESS, qry_Properties[FLD_ADDRESS])

//                    mapx_Par (FLD_LAT_STR, Form
 //                   mapx_Par (FLD_LON_STR,

                    ]);
  end;

  oMap.CloseFile;

  FreeAndNil(oMap);

//  oMap.Free;

  // ---------------------------------------------------------------

//  FMap.Layers.RemoveAll;

  vLayer:=FMap.Layers.Add (sTabFileName_link, 1);

  // -------------------------
  //vLayer.AutoLabel:= True;

  vLayer:=FMap.Layers.Add (sTabFileName_site, 1);


//  oMapItem:=FMapDesktopItem.ObjectMapList.FindByObjectName('property');
  oMapItem:=FMapDesktopItem.MapList.FindByObjectName('property');
  if Assigned(oMapItem) then
    oMapItem.LabelProperties.SaveToMapinfoLayer(vLayer);


  vLayer.AutoLabel:= True;

  // -------------------------

  rLinkLineRect:= dmLinkLine.GetBLRect(aID);

  Assert(rLinkLineRect.BottomRight.L<>0);

//  rLinkLineRect:= geo_ZoomRect (rLinkLineRect, 0.03);

//  mapx_SetBounds (FMap, rLinkLineRect);

  /////////////////dmMap_Desktop.ApplyLabelProperties(vLayer, 'property.tab');

//  vLayer.LabelProperties.Overlap:= true;
//  vLayer.AutoLabel:= True;

 // InsertMaps();


  // -------------------------------------------------------------------

  sGeoFileName := GetTempFileNameWithExt('gst');
  FMap.SaveMapAsGeoset('',sGeoFileName);

  sWorFileName :=ChangeFileExt(sGeoFileName, '.wor');



//{$IFNDEF wms}
  BLPoint:=geo_GetRectCenter (rLinkLineRect);

  mapx_SetCenter(FMap, geo_GetRectCenter (rLinkLineRect));

   mapx_SetScale (FMap, g_Link_Report_Param.Scale / 100);
//{$ENDIF}

  mapx_SaveToWOR(FMap, sWorFileName);


  blVector.Point1:=rLinkLineRect.TopLeft;
  blVector.Point2:=rLinkLineRect.BottomRight;

  FillChar(rec,SizeOf(rec),0);



  rec.Scale:=g_Link_Report_Param.Scale;
  rec.GeosetFileName:=sGeoFileName;
  rec.iMGFileName:=aFileName;
  rec.BLVector:=blVector;

  rec.Width_cm :=g_Link_Report_Param.ImageWidth;
  rec.Height_cm:=g_Link_Report_Param.ImageHeight;

 // rec.Mode:=mtVEctor;


  Tdlg_MIF_export1.Execute(rec);
//  MIF_ExportMap (rec);


  Assert(g_Link_Report_Param.Scale>0, 'Params.Scale>0');

  img_InsertScaleRuleIntoBMP (aFileName, aFileName, g_Link_Report_Param.Scale);


  DeleteFile(sGeoFileName);

  // -------------------------------------------------------------------


//  DeleteFile(sGeoFileName);

  // -------------------------------------------------------------------

  DeleteFile(sWorFileName);

  mapx_DeleteFile(sTabFileName_site);
  mapx_DeleteFile(sTabFileName_link);


//  FMap.Layers.RemoveAll;
end;




function TdmLink_map_.AssignLineStyle: TmiStyleRec;
begin
 // Result := aLineStyle.MIStyle;

  Result.LineStyle:=1;
  Result.LineColor:=clRed;
  Result.LineWidth:=2;


end;


function TdmLink_map_.AssignFontStyle: TmiStyleRec;
begin

  Result.FontColor:=clRed;
  Result.FontSize:=20;
  Result.FontName:=MI_FONT_MAPINFO_SYMBOLS;
  Result.Character:=MI_SYMBOL_STAR;


//  Result := aFontStyle.MIStyle;
end;

class procedure TdmLink_map_.LinkLine_SaveToImage(aID: integer; aFileName:
    string; aLinkIDList: TIDList);
begin
  Init_().LinkLine_SaveToImage_(aID, aFileName, aLinkIDList);
  FreeAndNil(dmLink_map_);
end;

class procedure TdmLink_map_.Link_SaveToImage(aID: integer; aFileName: string);
begin
  Init_().Link_SaveToImage_(aID, aFileName);
  FreeAndNil(dmLink_map_);
end;



end.


