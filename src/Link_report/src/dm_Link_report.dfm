inherited dmLink_report: TdmLink_report
  OldCreateOrder = True
  OnCreate = DataModuleCreate
  OnDestroy = DataModuleDestroy
  Left = 441
  Top = 252
  Height = 677
  Width = 1171
  object qry_LinkEnd1: TADOQuery
    Connection = ADOConnection1
    CursorType = ctStatic
    LockType = ltReadOnly
    OnCalcFields = qry_LinkEnd1CalcFields
    DataSource = ds_Link
    Parameters = <
      item
        Name = 'linkend1_id'
        DataType = ftInteger
        Precision = 10
        Value = 330706
      end>
    Prepared = True
    SQL.Strings = (
      'SELECT    *'
      'FROM      view_linkend'
      'where id=:linkend1_id')
    Left = 308
    Top = 196
  end
  object qry_Antennas1: TADOQuery
    Connection = ADOConnection1
    CursorType = ctStatic
    LockType = ltReadOnly
    DataSource = ds_LinkEnd1
    Parameters = <
      item
        Name = 'linkend1_id'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 138889
      end>
    Prepared = True
    SQL.Strings = (
      'SELECT  *'
      'FROM    linkend_antenna'
      'where linkend_id = :linkend1_id')
    Left = 544
    Top = 196
  end
  object qry_LinkEnd2: TADOQuery
    Connection = ADOConnection1
    CursorType = ctStatic
    LockType = ltReadOnly
    OnCalcFields = qry_LinkEnd1CalcFields
    DataSource = ds_Link
    Parameters = <
      item
        Name = 'linkend2_id'
        DataType = ftInteger
        Precision = 10
        Value = 330707
      end>
    Prepared = True
    SQL.Strings = (
      'SELECT    *'
      'FROM      view_linkend'
      'where id=:linkend2_id')
    Left = 428
    Top = 196
  end
  object qry_Antennas2: TADOQuery
    Connection = ADOConnection1
    CursorType = ctStatic
    LockType = ltReadOnly
    DataSource = ds_LinkEnd2
    Parameters = <
      item
        Name = 'linkend2_id'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 139269
      end>
    Prepared = True
    SQL.Strings = (
      'SELECT  *'
      'FROM    linkend_antenna'
      'where linkend_id = :linkend2_id')
    Left = 644
    Top = 196
  end
  object qry_Property1: TADOQuery
    Connection = ADOConnection1
    CursorType = ctStatic
    LockType = ltReadOnly
    OnCalcFields = qry_Property1CalcFields
    DataSource = ds_Link
    Parameters = <
      item
        Name = 'property1_id'
        DataType = ftInteger
        Precision = 10
        Value = 1137674
      end>
    Prepared = True
    SQL.Strings = (
      'SELECT    *   FROM      Property   where id=:property1_id')
    Left = 56
    Top = 196
  end
  object qry_Property2: TADOQuery
    Connection = ADOConnection1
    CursorType = ctStatic
    LockType = ltReadOnly
    OnCalcFields = qry_Property1CalcFields
    DataSource = ds_Link
    Parameters = <
      item
        Name = 'property2_id'
        DataType = ftInteger
        Precision = 10
        Value = 1137675
      end>
    Prepared = True
    SQL.Strings = (
      'SELECT    *'
      'FROM      Property'
      'where id=:property2_id')
    Left = 168
    Top = 196
  end
  object qry_LInk_repeater: TADOQuery
    DataSource = ds_Link
    Parameters = <>
    Left = 768
    Top = 200
  end
  object qry_LInk_repeater_ant: TADOQuery
    DataSource = ds_Repeater
    Parameters = <>
    Left = 1056
    Top = 200
  end
  object frxDBDataset_link: TfrxDBDataset
    UserName = 'Link'
    CloseDataSource = False
    FieldAliases.Strings = (
      'id=id'
      'name=name'
      'project_id=project_id'
      'clutter_model_id=clutter_model_id'
      'folder_id=folder_id'
      'objname=objname'
      'linkend1_id=linkend1_id'
      'linkend2_id=linkend2_id'
      'pmp_sector_id=pmp_sector_id'
      'pmp_terminal_id=pmp_terminal_id'
      'Property1_id=Property1_id'
      'Property2_id=Property2_id'
      'length=length'
      'calc_method=calc_method'
      'comments=comments'
      'refraction=refraction'
      'NFrenel=NFrenel'
      'profile_step=profile_step'
      'is_profile_reversed=is_profile_reversed'
      '_=_'
      'LOS_status=LOS_status'
      'status=status'
      'rx_level_dBm=rx_level_dBm'
      'SESR=SESR'
      'Kng=Kng'
      'kng_year=kng_year'
      'fade_margin_dB=fade_margin_dB'
      'rain_intensity_extra=rain_intensity_extra'
      'rain_IsCalcLength=rain_IsCalcLength'
      'Rain_Length_km=Rain_Length_km'
      'rain_signal_quality=rain_signal_quality'
      'rain_Weakening_vert=rain_Weakening_vert'
      'rain_Weakening_horz=rain_Weakening_horz'
      'rain_Allowable_Intense_vert=rain_Allowable_Intense_vert'
      'rain_Allowable_Intense_horz=rain_Allowable_Intense_horz'
      'rain_signal_depression_dB=rain_signal_depression_dB'
      'tilt=tilt'
      '_________=_________'
      'GST_type=GST_type'
      'GST_SESR=GST_SESR'
      'GST_KNG=GST_KNG'
      'GST_length=GST_length'
      'GST_equivalent_length_km=GST_equivalent_length_km'
      'KNG_dop=KNG_dop'
      'SESR_norm=SESR_norm'
      'KNG_norm=KNG_norm'
      'ESR_norm=ESR_norm'
      'BBER_norm=BBER_norm'
      'linkType_id=linkType_id'
      'RRV_name=RRV_name'
      '--- link conditions----=--- link conditions----'
      'gradient_diel=gradient_diel'
      'gradient_deviation=gradient_deviation'
      'terrain_type=terrain_type'
      'underlying_terrain_type=underlying_terrain_type'
      'steam_wet=steam_wet'
      'air_temperature=air_temperature'
      'atmosphere_pressure=atmosphere_pressure'
      'Q_factor=Q_factor'
      'climate_factor=climate_factor'
      'factor_B=factor_B'
      'factor_C=factor_C'
      'factor_D=factor_D'
      'rain_intensity=rain_intensity'
      'BER_required=BER_required'
      'ESR_required=ESR_required'
      'BBER_required=BBER_required'
      'space_limit=space_limit'
      'space_limit_probability=space_limit_probability'
      'margin_height=margin_height'
      'margin_distance=margin_distance'
      'sesr_subrefraction=sesr_subrefraction'
      'sesr_rain=sesr_rain'
      'sesr_interference=sesr_interference'
      'KNG_subrefraction=KNG_subrefraction'
      'KNG_rain=KNG_rain'
      'KNG_interference=KNG_interference'
      '___________=___________'
      'status_id=status_id'
      '_______=_______'
      'profile_source=profile_source'
      'profile_XML=profile_XML'
      'profile_is_custom=profile_is_custom'
      'calc_results_xml___=calc_results_xml___'
      'Name_SDB=Name_SDB'
      'Name_TN=Name_TN'
      'guid=guid'
      'date_created=date_created'
      'date_modify=date_modify'
      'user_created=user_created'
      'user_modify=user_modify'
      
        'precision_of_condition_V_diffration_is_equal_to_V_min_dB=precisi' +
        'on_of_condition_V_diffration_is_equal_to_V_min_dB'
      'azimuth1=azimuth1'
      'azimuth2=azimuth2'
      'is_UsePassiveElements=is_UsePassiveElements'
      'Is_CalcWithAdditionalRain=Is_CalcWithAdditionalRain'
      'link_repeater_id=link_repeater_id'
      '__GOST____=__GOST____'
      'GOST_name=GOST_name'
      'GOST_gradient_diel=GOST_gradient_diel'
      'GOST_gradient_deviation=GOST_gradient_deviation'
      'GOST_terrain_type=GOST_terrain_type'
      'GOST_steam_wet=GOST_steam_wet'
      'GOST_atmosphere_pressure=GOST_atmosphere_pressure'
      'GOST_underlying_terrain_type=GOST_underlying_terrain_type'
      'GOST_link_center_lat=GOST_link_center_lat'
      'GOST_link_center_lon=GOST_link_center_lon'
      'GOST_climate_factor=GOST_climate_factor'
      'GOST_air_temperature=GOST_air_temperature'
      'GOST_rain_intensity=GOST_rain_intensity'
      '__NIIR1998____=__NIIR1998____'
      'NIIR1998_name=NIIR1998_name'
      'NIIR1998_gradient_diel=NIIR1998_gradient_diel'
      'NIIR1998_gradient_deviation=NIIR1998_gradient_deviation'
      'NIIR1998_terrain_type=NIIR1998_terrain_type'
      'NIIR1998_steam_wet=NIIR1998_steam_wet'
      'NIIR1998_atmosphere_pressure=NIIR1998_atmosphere_pressure'
      'NIIR1998_air_temperature=NIIR1998_air_temperature'
      'NIIR1998_rain_region_number=NIIR1998_rain_region_number'
      'NIIR1998_Qd_region_number=NIIR1998_Qd_region_number'
      'NIIR1998_water_area_percent=NIIR1998_water_area_percent'
      'rrv_UKV_param_1=rrv_UKV_param_1'
      'rrv_UKV_param_2=rrv_UKV_param_2'
      'rrv_UKV_param_3=rrv_UKV_param_3'
      'rrv_UKV_param_4=rrv_UKV_param_4'
      '_______11111=_______11111'
      'rrv_E_band_ITU_param_1=rrv_E_band_ITU_param_1'
      'rrv_E_band_ITU_param_2=rrv_E_band_ITU_param_2'
      'rrv_E_band_ITU_param_3=rrv_E_band_ITU_param_3'
      'rrv_E_band_ITU_param_4=rrv_E_band_ITU_param_4'
      'rrv_E_band_ITU_param_5=rrv_E_band_ITU_param_5'
      'rrv_E_band_ITU_param_6=rrv_E_band_ITU_param_6'
      'length_km=length_km'
      'ELEVATION_angle_1=ELEVATION_angle_1'
      'ELEVATION_angle_2=ELEVATION_angle_2'
      'lat1=lat1'
      'lon1=lon1'
      'lat2=lat2'
      'lon2=lon2'
      'georegion1_id=georegion1_id'
      'georegion2_id=georegion2_id'
      'Property1_address=Property1_address'
      'Property2_address=Property2_address'
      'Property1_name=Property1_name'
      'Property2_name=Property2_name'
      'Property1_code=Property1_code'
      'Property2_code=Property2_code'
      'Property1_ground_height=Property1_ground_height'
      'Property2_ground_height=Property2_ground_height'
      'Property1.GeoRegion.name=Property1.GeoRegion.name'
      'Property2.GeoRegion.name=Property2.GeoRegion.name'
      'Property1.name_ERP=Property1.name_ERP'
      'Property1.name_SDB=Property1.name_SDB'
      'Property2.name_ERP=Property2.name_ERP'
      'Property2.name_SDB=Property2.name_SDB'
      'Property1.name=Property1.name'
      'Property2.name=Property2.name'
      'LinkEnd1_LinkID=LinkEnd1_LinkID'
      'LinkEnd2_LinkID=LinkEnd2_LinkID'
      'band=band'
      'bitrate_Mbps=bitrate_Mbps'
      'tx_freq_MHz=tx_freq_MHz'
      'rx_freq_MHz=rx_freq_MHz'
      'LinkEnd1_kratnostBySpace=LinkEnd1_kratnostBySpace'
      'LinkEnd2_kratnostBySpace=LinkEnd2_kratnostBySpace'
      'antenna1_diameter=antenna1_diameter'
      'antenna2_diameter=antenna2_diameter'
      'antenna1_height=antenna1_height'
      'antenna2_height=antenna2_height'
      'antenna1_polarization=antenna1_polarization'
      'antenna2_polarization=antenna2_polarization'
      'Clutter_Model_Name=Clutter_Model_Name'
      'LinkType_Name=LinkType_Name'
      'status_name=status_name'
      'State_name=State_name'
      'status_str=status_str'
      'GST_type_name=GST_type_name'
      'calc_method_name=calc_method_name'
      'LOS_status_name=LOS_status_name'
      'terrain_type_name=terrain_type_name'
      'underlying_terrain_type_name=underlying_terrain_type_name')
    DataSource = ds_Link
    BCDToCurrency = False
    Left = 148
    Top = 116
  end
  object frxDBDataset_Property1: TfrxDBDataset
    UserName = 'Site1'
    CloseDataSource = False
    FieldAliases.Strings = (
      'id=id'
      'name=name'
      'project_id=project_id'
      'folder_id=folder_id'
      'georegion_id=georegion_id'
      'address=address'
      'region=region'
      'city=city'
      '_________=_________'
      'lat=lat'
      'lon=lon'
      'lat_str=lat_str'
      'lon_str=lon_str'
      'lat_WGS=lat_WGS'
      'lon_WGS=lon_WGS'
      'lat_CK95=lat_CK95'
      'lon_CK95=lon_CK95'
      '__________=__________'
      'ground_height=ground_height'
      'town=town'
      'comments=comments'
      '______=______'
      'clutter_name=clutter_name'
      'clutter_height=clutter_height'
      '_=_'
      'guid=guid'
      'date_created=date_created'
      'date_modify=date_modify'
      'user_modify=user_modify'
      'user_created=user_created'
      'address1=address1'
      'address2=address2'
      'status=status'
      'status_id=status_id'
      '_____=_____'
      'lat_WGS_str=lat_WGS_str'
      'lon_WGS_str=lon_WGS_str'
      'lat_lon_WGS=lat_lon_WGS'
      '_____________=_____________'
      'tower_height=tower_height'
      'lat_lon_str=lat_lon_str'
      'lat_lon_kras_wgs=lat_lon_kras_wgs'
      'object_id=object_id'
      'placement_id=placement_id'
      'state_id=state_id'
      'Name_ERP=Name_ERP'
      'name_SDB=name_SDB'
      'code=code'
      'lat_CK95_str=lat_CK95_str'
      'lon_CK95_str=lon_CK95_str'
      'bee_beenet=bee_beenet')
    DataSet = qry_Property1
    BCDToCurrency = False
    Left = 52
    Top = 308
  end
  object frxDBDataset_Property2: TfrxDBDataset
    UserName = 'Site2'
    CloseDataSource = False
    FieldAliases.Strings = (
      'id=id'
      'name=name'
      'project_id=project_id'
      'folder_id=folder_id'
      'georegion_id=georegion_id'
      'address=address'
      'region=region'
      'city=city'
      '_________=_________'
      'lat=lat'
      'lon=lon'
      'lat_str=lat_str'
      'lon_str=lon_str'
      'lat_WGS=lat_WGS'
      'lon_WGS=lon_WGS'
      'lat_CK95=lat_CK95'
      'lon_CK95=lon_CK95'
      '__________=__________'
      'ground_height=ground_height'
      'town=town'
      'comments=comments'
      '______=______'
      'clutter_name=clutter_name'
      'clutter_height=clutter_height'
      '_=_'
      'guid=guid'
      'date_created=date_created'
      'date_modify=date_modify'
      'user_modify=user_modify'
      'user_created=user_created'
      'address1=address1'
      'address2=address2'
      'status=status'
      'status_id=status_id'
      '_____=_____'
      'lat_WGS_str=lat_WGS_str'
      'lon_WGS_str=lon_WGS_str'
      'lat_lon_WGS=lat_lon_WGS'
      '_____________=_____________'
      'tower_height=tower_height'
      'lat_lon_str=lat_lon_str'
      'lat_lon_kras_wgs=lat_lon_kras_wgs'
      'object_id=object_id'
      'placement_id=placement_id'
      'state_id=state_id'
      'Name_ERP=Name_ERP'
      'name_SDB=name_SDB'
      'code=code'
      'lat_CK95_str=lat_CK95_str'
      'lon_CK95_str=lon_CK95_str'
      'bee_beenet=bee_beenet')
    DataSet = qry_Property2
    BCDToCurrency = False
    Left = 172
    Top = 308
  end
  object frxDBDataset_Link_img: TfrxDBDataset
    UserName = 'Link_img'
    CloseDataSource = False
    BCDToCurrency = False
    Left = 52
    Top = 540
  end
  object ADOConnection1: TADOConnection
    ConnectionString = 
      'Provider=MSDASQL.1;Persist Security Info=False;User ID=sa;Data S' +
      'ource=server_onega_link11;'
    LoginPrompt = False
    Provider = 'MSDASQL.1'
    Left = 32
    Top = 8
  end
  object ds_Link: TDataSource
    DataSet = qry_Link
    Left = 152
    Top = 64
  end
  object frxDBDataset_LinkEnd1: TfrxDBDataset
    UserName = 'LinkEnd1'
    CloseDataSource = False
    FieldAliases.Strings = (
      'id=id'
      'name=name'
      'project_id=project_id'
      'property_id=property_id'
      'linkendtype_id=linkendtype_id'
      'folder_id=folder_id'
      'objname=objname'
      '_=_'
      'LinkEndType_mode_id=LinkEndType_mode_id'
      'LinkEndType_band_id=LinkEndType_band_id'
      '_____________=_____________'
      'band=band'
      'subband=subband'
      'rx_freq_MHz=rx_freq_MHz'
      'tx_freq_MHz=tx_freq_MHz'
      'channel_number=channel_number'
      'channel_type=channel_type'
      'loss=loss'
      'redundancy=redundancy'
      'redundancy_type=redundancy_type'
      'equipment_type=equipment_type'
      'Protection=Protection'
      '__=__'
      'passive_element_id=passive_element_id'
      'passive_element_loss=passive_element_loss'
      'power_max=power_max'
      'power_loss=power_loss'
      'power_dBm=power_dBm'
      'power_W=power_W'
      '_____=_____'
      'mode=mode'
      'modulation_type=modulation_type'
      'modulation_count=modulation_count'
      'signature_width=signature_width'
      'signature_height=signature_height'
      'equaliser_profit=equaliser_profit'
      'freq_spacing=freq_spacing'
      'channel_spacing=channel_spacing'
      'freq_channel_count=freq_channel_count'
      'kratnostByFreq=kratnostByFreq'
      'kratnostBySpace=kratnostBySpace'
      'failure_period=failure_period'
      'restore_period=restore_period'
      'kng=kng'
      'threshold_BER_3=threshold_BER_3'
      'threshold_BER_6=threshold_BER_6'
      'channel_width_MHz=channel_width_MHz'
      '_______________=_______________'
      'comments=comments'
      '________=________'
      'guid=guid'
      'date_created=date_created'
      'date_modify=date_modify'
      'user_created=user_created'
      'user_modify=user_modify'
      '____Repeater____=____Repeater____'
      'space_diversity=space_diversity'
      '______________=______________'
      'LinkID=LinkID'
      'GOST_53363_modulation=GOST_53363_modulation'
      
        'GOST_53363_RX_count_on_combined_diversity=GOST_53363_RX_count_on' +
        '_combined_diversity'
      'rx_level_dBm=rx_level_dBm'
      'threshold_degradation_dB=threshold_degradation_dB'
      'ATPC_profit_dB=ATPC_profit_dB'
      'use_ATPC=use_ATPC'
      'bitrate_Mbps=bitrate_Mbps'
      '___________=___________'
      'pmp_site_id=pmp_site_id'
      'pmp_sector_id=pmp_sector_id'
      '_____PMP____=_____PMP____'
      'cell_layer_id=cell_layer_id'
      'calc_radius_km=calc_radius_km'
      '_______=_______'
      'calc_model_id=calc_model_id'
      'k0_open=k0_open'
      'k0_closed=k0_closed'
      'k0=k0'
      
        'NIIR1998_compensator_of_noises_Ixpic_dB=NIIR1998_compensator_of_' +
        'noises_Ixpic_dB'
      
        'NIIR1998_Coefficient_cross_polarization_protection_dB=NIIR1998_C' +
        'oefficient_cross_polarization_protection_dB'
      'azimuth=azimuth'
      'LinkEndType_name=LinkEndType_name'
      'property_name=property_name'
      'lat=lat'
      'lon=lon'
      'ground_height=ground_height'
      'Property.guid=Property.guid'
      'Passive_Component.name=Passive_Component.name'
      'equipment_type_name=equipment_type_name'
      'kratnostBySpace_name=kratnostBySpace_name'
      'kratnostByFreq_name=kratnostByFreq_name'
      'redundancy_name=redundancy_name'
      'Link_id=Link_id'
      'Link_name=Link_name'
      'Link_calc_method_id=Link_calc_method_id'
      'georegion_id=georegion_id'
      'antenna_diameter=antenna_diameter'
      'antenna_height=antenna_height'
      'antenna_polarization=antenna_polarization')
    DataSet = qry_LinkEnd1
    BCDToCurrency = False
    Left = 300
    Top = 308
  end
  object frxDBDataset_LinkEnd2: TfrxDBDataset
    UserName = 'LinkEnd2'
    CloseDataSource = False
    FieldAliases.Strings = (
      'id=id'
      'name=name'
      'project_id=project_id'
      'property_id=property_id'
      'linkendtype_id=linkendtype_id'
      'folder_id=folder_id'
      'objname=objname'
      '_=_'
      'LinkEndType_mode_id=LinkEndType_mode_id'
      'LinkEndType_band_id=LinkEndType_band_id'
      '_____________=_____________'
      'band=band'
      'subband=subband'
      'rx_freq_MHz=rx_freq_MHz'
      'tx_freq_MHz=tx_freq_MHz'
      'channel_number=channel_number'
      'channel_type=channel_type'
      'loss=loss'
      'redundancy=redundancy'
      'redundancy_type=redundancy_type'
      'equipment_type=equipment_type'
      'Protection=Protection'
      '__=__'
      'passive_element_id=passive_element_id'
      'passive_element_loss=passive_element_loss'
      'power_max=power_max'
      'power_loss=power_loss'
      'power_dBm=power_dBm'
      'power_W=power_W'
      '_____=_____'
      'mode=mode'
      'modulation_type=modulation_type'
      'modulation_count=modulation_count'
      'signature_width=signature_width'
      'signature_height=signature_height'
      'equaliser_profit=equaliser_profit'
      'freq_spacing=freq_spacing'
      'channel_spacing=channel_spacing'
      'freq_channel_count=freq_channel_count'
      'kratnostByFreq=kratnostByFreq'
      'kratnostBySpace=kratnostBySpace'
      'failure_period=failure_period'
      'restore_period=restore_period'
      'kng=kng'
      'threshold_BER_3=threshold_BER_3'
      'threshold_BER_6=threshold_BER_6'
      'channel_width_MHz=channel_width_MHz'
      '_______________=_______________'
      'comments=comments'
      '________=________'
      'guid=guid'
      'date_created=date_created'
      'date_modify=date_modify'
      'user_created=user_created'
      'user_modify=user_modify'
      '____Repeater____=____Repeater____'
      'space_diversity=space_diversity'
      '______________=______________'
      'LinkID=LinkID'
      'GOST_53363_modulation=GOST_53363_modulation'
      
        'GOST_53363_RX_count_on_combined_diversity=GOST_53363_RX_count_on' +
        '_combined_diversity'
      'rx_level_dBm=rx_level_dBm'
      'threshold_degradation_dB=threshold_degradation_dB'
      'ATPC_profit_dB=ATPC_profit_dB'
      'use_ATPC=use_ATPC'
      'bitrate_Mbps=bitrate_Mbps'
      '___________=___________'
      'pmp_site_id=pmp_site_id'
      'pmp_sector_id=pmp_sector_id'
      '_____PMP____=_____PMP____'
      'cell_layer_id=cell_layer_id'
      'calc_radius_km=calc_radius_km'
      '_______=_______'
      'calc_model_id=calc_model_id'
      'k0_open=k0_open'
      'k0_closed=k0_closed'
      'k0=k0'
      
        'NIIR1998_compensator_of_noises_Ixpic_dB=NIIR1998_compensator_of_' +
        'noises_Ixpic_dB'
      
        'NIIR1998_Coefficient_cross_polarization_protection_dB=NIIR1998_C' +
        'oefficient_cross_polarization_protection_dB'
      'azimuth=azimuth'
      'LinkEndType_name=LinkEndType_name'
      'property_name=property_name'
      'lat=lat'
      'lon=lon'
      'ground_height=ground_height'
      'Property.guid=Property.guid'
      'Passive_Component.name=Passive_Component.name'
      'equipment_type_name=equipment_type_name'
      'kratnostBySpace_name=kratnostBySpace_name'
      'kratnostByFreq_name=kratnostByFreq_name'
      'redundancy_name=redundancy_name'
      'Link_id=Link_id'
      'Link_name=Link_name'
      'Link_calc_method_id=Link_calc_method_id'
      'georegion_id=georegion_id'
      'antenna_diameter=antenna_diameter'
      'antenna_height=antenna_height'
      'antenna_polarization=antenna_polarization')
    DataSet = qry_LinkEnd2
    BCDToCurrency = False
    Left = 427
    Top = 308
  end
  object frxDBDataset_group: TfrxDBDataset
    UserName = 'group'
    CloseDataSource = False
    FieldAliases.Strings = (
      'id=id'
      'caption=caption')
    BCDToCurrency = False
    Left = 356
    Top = 468
  end
  object frxDBDataset_Items: TfrxDBDataset
    UserName = 'Items'
    CloseDataSource = False
    FieldAliases.Strings = (
      'id=id'
      'caption=caption'
      'parent_id=parent_id'
      'value=value'
      'value2=value2')
    BCDToCurrency = False
    Left = 356
    Top = 524
  end
  object ds_Property2: TDataSource
    DataSet = qry_Property2
    Left = 168
    Top = 256
  end
  object ds_Property1: TDataSource
    DataSet = qry_Property1
    Left = 56
    Top = 256
  end
  object ds_LinkEnd1: TDataSource
    DataSet = qry_LinkEnd1
    Left = 304
    Top = 248
  end
  object ds_LinkEnd2: TDataSource
    DataSet = qry_LinkEnd2
    Left = 426
    Top = 248
  end
  object frxDBDataset_Profile_items: TfrxDBDataset
    UserName = 'Profile'
    CloseDataSource = False
    FieldAliases.Strings = (
      'distance_km=distance_km'
      'ground_h=ground_h'
      'clutter_h=clutter_h'
      'clutter_code=clutter_code'
      'clutter_name=clutter_name'
      'clutter_color=clutter_color'
      'earth_h=earth_h'
      'index=index'
      'link_id=link_id')
    BCDToCurrency = False
    Left = 628
    Top = 540
  end
  object frxPDFExport1: TfrxPDFExport
    UseFileCache = True
    ShowProgress = True
    OverwritePrompt = False
    CreationTime = 41908.622654074070000000
    DataOnly = False
    OpenAfterExport = False
    PrintOptimized = False
    Outline = False
    Background = False
    HTMLTags = True
    Quality = 95
    Transparency = False
    Author = 'FastReport'
    Subject = 'FastReport PDF export'
    ProtectionFlags = [ePrint, eModify, eCopy, eAnnot]
    HideToolbar = False
    HideMenubar = False
    HideWindowUI = False
    FitWindow = False
    CenterWindow = False
    PrintScaling = False
    PdfA = False
    Left = 880
    Top = 64
  end
  object frxRTFExport1: TfrxRTFExport
    FileName = 'W:\RPLS_DB\src\Link_report\test1\1.rtf'
    UseFileCache = True
    ShowProgress = True
    OverwritePrompt = False
    CreationTime = 41908.688379803240000000
    DataOnly = False
    PictureType = gpJPG
    OpenAfterExport = False
    Wysiwyg = True
    Creator = 'FastReport'
    SuppressPageHeadersFooters = False
    HeaderFooterMode = hfText
    AutoSize = False
    Left = 880
    Top = 112
  end
  object frxXLSExport1: TfrxXLSExport
    UseFileCache = True
    ShowProgress = True
    OverwritePrompt = False
    DataOnly = False
    ExportEMF = True
    AsText = False
    Background = True
    FastExport = True
    PageBreaks = True
    EmptyLines = True
    SuppressPageHeadersFooters = False
    Left = 880
    Top = 16
  end
  object qry_Link: TADOQuery
    Connection = ADOConnection1
    CursorType = ctStatic
    LockType = ltBatchOptimistic
    Parameters = <>
    Prepared = True
    SQL.Strings = (
      'SELECT  top 10  *'
      'FROM      view_link')
    Left = 152
    Top = 12
  end
  object frxDesigner1: TfrxDesigner
    CloseQuery = False
    DefaultScriptLanguage = 'PascalScript'
    DefaultFont.Charset = DEFAULT_CHARSET
    DefaultFont.Color = clWindowText
    DefaultFont.Height = -13
    DefaultFont.Name = 'Arial'
    DefaultFont.Style = []
    DefaultLeftMargin = 10.000000000000000000
    DefaultRightMargin = 10.000000000000000000
    DefaultTopMargin = 10.000000000000000000
    DefaultBottomMargin = 10.000000000000000000
    DefaultPaperSize = 9
    DefaultOrientation = poPortrait
    TemplatesExt = 'fr3'
    Restrictions = [drDontSaveReport]
    RTLLanguage = False
    MemoParentFont = False
    Left = 1024
    Top = 16
  end
  object frxDBDataset_Antenna1: TfrxDBDataset
    UserName = 'Antenna1'
    CloseDataSource = False
    FieldAliases.Strings = (
      'height=height'
      'diameter=diameter'
      'polarization=polarization'
      'link_id=link_id'
      'gain=gain'
      'tilt=tilt')
    BCDToCurrency = False
    Left = 796
    Top = 540
  end
  object frxDBDataset_Antenna2: TfrxDBDataset
    UserName = 'Antenna2'
    CloseDataSource = False
    FieldAliases.Strings = (
      'height=height'
      'diameter=diameter'
      'polarization=polarization'
      'link_id=link_id'
      'gain=gain'
      'tilt=tilt')
    BCDToCurrency = False
    Left = 964
    Top = 540
  end
  object frxReport_full: TfrxReport
    Version = '6.2.11'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.AllowEdit = False
    PreviewOptions.Buttons = [pbPrint, pbExport, pbZoom, pbTools, pbNavigator, pbExportQuick, pbNoFullScreen]
    PreviewOptions.ThumbnailVisible = True
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = #1055#1086' '#1091#1084#1086#1083#1095#1072#1085#1080#1102
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 41730.469225335600000000
    ReportOptions.LastChange = 42607.844752210650000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      ''
      '      '
      '  '
      ''
      ''
      'procedure frxReportOnStartReport(Sender: TfrxComponent);'
      'const'
      '  DEF_MAP_H = 800;'
      '  '
      '  DEF_H = 450;'
      '  DEF_OFFSET = 25;'
      ''
      '  DEF_TOP = 20;                                  '
      'begin'
      '//  Picture_map.Width:=200;                '
      '  Picture_map.Height     :=DEF_MAP_H;'
      '    '
      '  Picture_profile.Height :=DEF_H;                '
      '  Picture_profile1.Height:=DEF_H;                '
      '  Picture_profile2.Height:=DEF_H;                '
      '  Picture_profile3.Height:=DEF_H;                '
      '          '
      '  '
      '  Child_map.Height:=DEF_MAP_H;'
      '    '
      '  Child_profile.Height:=DEF_H + DEF_OFFSET;              '
      '  Child_profile1.Height:=DEF_H + DEF_OFFSET;              '
      '  Child_profile2.Height:=DEF_H + DEF_OFFSET;              '
      '  Child_profile3.Height:=DEF_H + DEF_OFFSET;'
      ''
      '  Child_profile1.Top:=DEF_TOP;              '
      '  Child_profile2.Top:=DEF_TOP;              '
      '  Child_profile3.Top:=DEF_TOP;'
      ''
      '                                    '
      ' // Picture_map.Dataset:='#39'Link_img'#39';      '
      
        '//  Picture_profile.Dataset:='#39'Link_img'#39';                        ' +
        '  '
      
        '//  Picture_profile1.Dataset:='#39'Link_img'#39';                       ' +
        '   '
      
        '//  Picture_profile2.Dataset:='#39'Link_img'#39';                       ' +
        '   '
      
        '//  Picture_profile3.Dataset:='#39'Link_img'#39';                       ' +
        '   '
      '      '
      ''
      '                 '
      'end;'
      ''
      ''
      'procedure Shape_ColorOnBeforePrint(Sender: TfrxComponent);'
      'begin'
      
        '  Shape_Color.color := <Profile."clutter_color">                ' +
        '                                                    '
      'end;'
      '        '
      ''
      '              '
      '//------------------------------------------------------------'
      'function DegreeToStr (aValue: double): String;'
      '//------------------------------------------------------------'
      'const'
      '//  STR_BLPOINT_FORMAT1 = '#39'%d'#176'%.2d'#39#39'%s%1.2f'#39#39#39#39#39';'
      '  STR_BLPOINT_FORMAT1 = '#39'%d'#176' %.2d'#39#39' %1.2f'#39#39#39#39#39';'
      'var    '
      ' iDeg,iMin:integer; '
      ' eSec: Double;   '
      '           '
      'begin'
      '  geo_DecodeDegree_(aValue, iDeg,iMin,eSec);'
      ' '
      '  Result:=Format(STR_BLPOINT_FORMAT1, [iDeg, iMin, eSec]);'
      ''
      'end;'
      ''
      '       '
      ''
      '//------------------------------------------------------------'
      'function DegreeToStr_lat (aValue: double): String;'
      '//------------------------------------------------------------'
      'var '
      '  s: string;'
      'begin'
      '  if aValue>0 then s:='#39'N'#39' else s:='#39'S'#39';'
      ''
      '  aValue:=Abs(aValue);       '
      ''
      '  Result:=DegreeToStr (aValue) + s;'
      'end;'
      ''
      '//------------------------------------------------------------'
      'function DegreeToStr_lon (aValue: double): String;'
      '//------------------------------------------------------------'
      'var '
      '  s: string;'
      'begin'
      '   if aValue<0 then begin'
      '     s:='#39'W'#39';'
      '     aValue:=Abs(aValue);'
      '   end else'
      ''
      '   if aValue>180 then begin'
      '     s:='#39'W'#39';'
      '     aValue:=360 - aValue'
      '   end else'
      '     s:='#39'E'#39';     '
      ''
      '  Result:=DegreeToStr (aValue) + s;'
      'end;                '
      ''
      ' //------------------------------------------------------------'
      
        'procedure geo_DecodeDegree_(aValue: Double; var aDeg,aMin:intege' +
        'r; var aSec:'
      '    Double);'
      '//------------------------------------------------------------'
      'var'
      '  bIsNegative: Boolean;'
      '  e: Double;'
      '  eSec: Double;'
      ''
      'begin'
      '  bIsNegative := aValue<0;'
      ''
      '  aValue :=Abs(aValue);'
      ''
      ''
      ''
      '   //    aValue:=99;                            '
      '          '
      ''
      '          '
      '  aDeg:=Trunc(aValue);'
      ''
      '  aMin:=Trunc(Frac(aValue)*60);'
      ''
      ''
      '//  e:= (aValue - Trunc(aValue));'
      '  e:= (aValue - Trunc(aValue)) * 60;'
      ''
      ''
      '  aMin:=Trunc(e);'
      '  aSec:=(((aValue - aDeg) *60) - aMin) * 60;'
      ''
      '  while aSec>(60-0.1) do'
      '  begin'
      '    aMin:=aMin+1;'
      '    aSec:=aSec-60;'
      '  end;    '
      ''
      '  while aMin>=60 do'
      '  begin'
      '      aMin:=aMin-60;'
      '      aDeg:=aDeg+1;'
      '  end;  '
      ''
      '  if aSec<0 then'
      '     aSec:=0;'
      '    '
      ''
      ''
      '  if bIsNegative then'
      '    aDeg:=-aDeg;'
      ''
      ''
      ''
      'end;'
      '  '
      ''
      '                                   '
      ''
      '//------------------------------------------------------------'
      
        'procedure geo_DecodeDegree_11(aValue: Double; var aDeg,aMin:inte' +
        'ger; var aSec:'
      '    Double);'
      '//------------------------------------------------------------'
      'const'
      ' EXP_ = 0.000000001;'
      '     //97,033333333'
      'var'
      '  bIsNegative: Boolean;'
      '  e: Double;'
      '  eMinutes: Double;'
      '  eSec: Double;'
      '  eSeconds: Double;'
      'begin'
      '  bIsNegative := aValue<0;'
      ''
      '  aValue :=Abs(aValue);'
      ''
      '  aDeg:=Trunc(aValue);'
      ''
      ' // e:=Abs(aValue)*60;'
      ''
      '//  , Frac(X) = X - Int(X).'
      ''
      '  aMin:=Trunc(Frac(aValue)*60);'
      ''
      '//  Result.Sec:=Frac(Frac(Abs(aValue))*60)*60;'
      '  e:= (aValue - Trunc(aValue)) * 60;'
      '  aMin:=Trunc(e);'
      ''
      ' eMinutes := Trunc (e);'
      ' eSeconds := (e - Trunc (e)) * 60 ;'
      ''
      ''
      '  aSec:=(((aValue - aDeg) *60) - aMin) * 60;'
      ''
      '  if (aSec - 60) > EXP_ then'
      '  begin'
      '    Inc(aMin);'
      '    aSec := 0;'
      '  end;'
      ''
      ''
      '  if bIsNegative then'
      '    aDeg:=-aDeg;'
      '          '
      'end;'
      '        '
      ''
      '       '
      'begin'
      ''
      'end.    ')
    OnStartReport = 'frxReportOnStartReport'
    Left = 438
    Top = 20
    Datasets = <
      item
        DataSet = frxDBDataset_Antenna1
        DataSetName = 'Antenna1'
      end
      item
        DataSet = frxDBDataset_Antenna2
        DataSetName = 'Antenna2'
      end
      item
        DataSet = frxDBDataset_group
        DataSetName = 'group'
      end
      item
        DataSet = frxDBDataset_Items
        DataSetName = 'Items'
      end
      item
        DataSet = frxDBDataset_link
        DataSetName = 'Link'
      end
      item
        DataSet = frxDBDataset_Link_img
        DataSetName = 'Link_img'
      end
      item
        DataSet = frxDBDataset_LinkEnd1
        DataSetName = 'LinkEnd1'
      end
      item
        DataSet = frxDBDataset_LinkEnd2
        DataSetName = 'LinkEnd2'
      end
      item
        DataSet = frxDBDataset_Profile_items
        DataSetName = 'Profile'
      end
      item
        DataSet = frxDBDataset_Property1
        DataSetName = 'Site1'
      end
      item
        DataSet = frxDBDataset_Property2
        DataSetName = 'Site2'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 9.260416666666670000
      BottomMargin = 10.583333333333300000
      Frame.Typ = []
      LargeDesignHeight = True
      OnBeforePrint = 'Page1OnBeforePrint'
      object Child_profile3: TfrxChild
        FillType = ftBrush
        Frame.Typ = []
        Height = 61.000000000000000000
        Top = 1303.937850000000000000
        Width = 718.110700000000000000
        Child = frxReport_full.Child_calc_res
        ToNRows = 0
        ToNRowsMode = rmCount
        object Memo82: TfrxMemoView
          Align = baWidth
          AllowVectorExport = True
          ShiftMode = smWhenOverlapped
          Width = 718.110700000000000000
          Height = 16.800000000000000000
          AutoWidth = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8 = (
            
              #1056#152#1056#183#1056#1109#1056#177#1057#1026#1056#176#1056#182#1056#181#1056#1029#1056#1105#1056#181' '#1056#1111#1057#1026#1056#1109#1057#8222#1056#1105#1056#187#1057#1039' '#1056#1030' '#1057#1107#1057#1027#1056#187#1056#1109#1056#1030#1056#1105#1057#1039#1057#8230' '#1057#1027#1057#1107#1056#177 +
              #1057#1026#1056#181#1057#8222#1057#1026#1056#176#1056#1108#1057#8224#1056#1105#1056#1105#1056#1109#1056#1029#1056#1029#1056#1109#1056#8470' '#1056#1029#1056#181#1057#1107#1057#1027#1057#8218#1056#1109#1056#8470#1057#8225#1056#1105#1056#1030#1056#1109#1057#1027#1057#8218#1056#1105)
          ParentFont = False
        end
        object Picture_profile3: TfrxPictureView
          Align = baWidth
          AllowVectorExport = True
          ShiftMode = smWhenOverlapped
          Top = 20.000000000000000000
          Width = 718.110700000000000000
          Height = 31.000000000000000000
          OnBeforePrint = 'Picture_profile3OnBeforePrint'
          DataField = 'img3'
          DataSet = frxDBDataset_Link_img
          DataSetName = 'Link_img'
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Stretched = False
          HightQuality = True
          Transparent = False
          TransparentColor = clWhite
        end
      end
      object MasterData3: TfrxMasterData
        FillType = ftBrush
        Frame.Typ = []
        Height = 819.244590000000000000
        Top = 105.826840000000000000
        Width = 718.110700000000000000
        Child = frxReport_full.Child_map
        DataSet = frxDBDataset_link
        DataSetName = 'Link'
        RowCount = 0
        object Memo7: TfrxMemoView
          Align = baWidth
          AllowVectorExport = True
          Width = 718.110700000000000000
          Height = 19.000000000000000000
          AutoWidth = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8 = (
            #1056#1115#1057#8218#1057#8225#1056#181#1057#8218' '#1056#1111#1056#1109' '#1057#1026#1056#176#1056#1169#1056#1105#1056#1109#1057#1026#1056#181#1056#187#1056#181#1056#8470#1056#1029#1056#1109#1056#1112#1057#1107' '#1056#1105#1056#1029#1057#8218#1056#181#1057#1026#1056#1030#1056#176#1056#187#1057#1107)
          ParentFont = False
        end
        object Linkname: TfrxMemoView
          Align = baWidth
          AllowVectorExport = True
          Top = 28.000000000000000000
          Width = 718.110700000000000000
          Height = 16.000000000000000000
          AutoWidth = True
          DataField = 'name'
          DataSet = frxDBDataset_link
          DataSetName = 'Link'
          Frame.Typ = []
          Memo.UTF8 = (
            '[Link."name"]')
        end
        object Memo_1: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 268.228510000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#8217#1057#8249#1057#1027#1056#1109#1057#8218#1056#176', m')
          ParentFont = False
        end
        object Memo_2: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 283.228510000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1106#1056#183#1056#1105#1056#1112#1057#1107#1057#8218' ')
          ParentFont = False
        end
        object Memo_3: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 298.228510000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1118#1056#1105#1056#1111' '#1056#1109#1056#177#1056#1109#1057#1026#1057#1107#1056#1169#1056#1109#1056#1030#1056#176#1056#1029#1056#1105#1057#1039' ')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 313.228510000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#167#1056#176#1057#1027#1057#8218#1056#1109#1057#8218#1056#176' '#1056#1111#1056#181#1057#1026#1056#181#1056#1169#1056#176#1057#8225#1056#1105', MHz')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 328.228510000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1038#1056#1108#1056#1109#1057#1026#1056#1109#1057#1027#1057#8218#1057#1034' '#1056#1111#1056#181#1057#1026#1056#181#1056#1169#1056#176#1057#8225#1056#1105', Mbit/s')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 343.228510000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1114#1056#1109#1057#8240#1056#1029#1056#1109#1057#1027#1057#8218#1057#1034', dBm')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 358.228510000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#8221#1056#1105#1056#176#1056#1112#1056#181#1057#8218#1057#1026' '#1056#176#1056#1029#1057#8218#1056#181#1056#1029#1056#1029#1057#8249', m')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 404.126160000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            
              #1056#1038#1057#1026#1056#181#1056#1169#1056#1029#1057#1039#1057#1039' '#1056#1112#1056#1109#1057#8240#1056#1029#1056#1109#1057#1027#1057#8218#1057#1034' '#1056#1029#1056#176' '#1056#1030#1057#8230#1056#1109#1056#1169#1056#181' '#1056#1111#1057#1026#1056#1105#1056#181#1056#1112#1056#1029#1056#1105#1056#1108 +
              #1056#176', dBm')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 373.228510000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1119#1056#1109#1056#187#1057#1039#1057#1026#1056#1105#1056#183#1056#176#1057#8224#1056#1105#1057#1039' ')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 419.126160000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1106#1056#1111#1056#1111#1056#176#1057#1026#1056#176#1057#8218#1056#1029#1056#1109#1056#181' '#1057#1026#1056#181#1056#183#1056#181#1057#1026#1056#1030#1056#1105#1057#1026#1056#1109#1056#1030#1056#176#1056#1029#1056#1105#1056#181' ')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 434.126160000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1119#1056#1109#1057#1026#1056#1109#1056#1110' '#1056#1111#1057#1026#1056#1105' BER=10-3 ')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 464.126160000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1114#1056#1105#1056#1029#1056#1105#1056#1112#1056#176#1056#187#1057#1034#1056#1029#1057#8249#1056#8470' '#1056#1109#1057#8218#1056#1029#1056#1109#1057#1027#1056#1105#1057#8218#1056#181#1056#187#1057#1034#1056#1029#1057#8249#1056#8470' '#1056#1111#1057#1026#1056#1109#1057#1027#1056#1030#1056#181#1057#8218)
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 449.126160000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1119#1056#1109#1057#1026#1056#1109#1056#1110' '#1056#1111#1057#1026#1056#1105' BER=10-6 ')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 479.126160000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1119#1056#1109#1056#187#1056#1109#1056#182#1056#181#1056#1029#1056#1105#1056#181' '#1057#8218#1056#1109#1057#8225#1056#1108#1056#1105' '#1056#1111#1057#1026#1056#1109#1057#1027#1056#1030#1056#181#1057#8218#1056#176', km ')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 494.126160000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            'SESR '#1057#1027#1057#1107#1056#177#1057#1026#1056#181#1057#8222#1056#1108#1057#8224#1056#1105#1056#1109#1056#1029#1056#1029#1056#176#1057#1039' '#1057#1027#1056#1109#1057#1027#1057#8218#1056#176#1056#1030#1056#187#1057#1039#1057#1035#1057#8240#1056#176#1057#1039', % ')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 509.126160000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            'SESR '#1056#1169#1056#1109#1056#182#1056#1169#1056#181#1056#1030#1056#176#1057#1039' '#1057#1027#1056#1109#1057#1027#1057#8218#1056#176#1056#1030#1056#187#1057#1039#1057#1035#1057#8240#1056#176#1057#1039', % ')
          ParentFont = False
        end
        object Memo25: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 524.126160000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            
              'SESR '#1056#1105#1056#1029#1057#8218#1056#181#1057#1026#1057#8222#1056#181#1057#1026#1056#181#1056#1029#1057#8224#1056#1105#1056#1109#1056#1029#1056#1029#1056#176#1057#1039' '#1057#1027#1056#1109#1057#1027#1057#8218#1056#176#1056#1030#1056#187#1057#1039#1057#1035#1057#8240#1056#176#1057#1039 +
              ', % ')
          ParentFont = False
        end
        object Memo26: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 539.126160000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            
              #1056#152#1057#8218#1056#1109#1056#1110#1056#1109#1056#1030#1057#8249#1056#8470' '#1056#1111#1056#1109#1056#1108#1056#176#1056#183#1056#176#1057#8218#1056#181#1056#187#1057#1034' '#1056#1108#1056#176#1057#8225#1056#181#1057#1027#1057#8218#1056#1030#1056#176' '#1056#1111#1056#1109' '#1056#1109#1057#8364 +
              #1056#1105#1056#177#1056#1108#1056#176#1056#1112' SESR, % ')
          ParentFont = False
        end
        object Memo27: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 554.126160000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            
              #1056#1116#1056#1109#1057#1026#1056#1112#1056#1105#1057#1026#1057#1107#1056#181#1056#1112#1056#1109#1056#181' '#1056#183#1056#1029#1056#176#1057#8225#1056#181#1056#1029#1056#1105#1056#181' SESR '#1056#1029#1056#176' '#1056#1105#1056#1029#1057#8218#1056#181#1057#1026#1056#1030#1056#176 +
              #1056#187#1056#181', % ')
          ParentFont = False
        end
        object Memo28: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 569.126160000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            
              #1056#152#1057#8218#1056#1109#1056#1110#1056#1109#1056#1030#1057#8249#1056#8470' '#1056#1111#1056#1109#1056#1108#1056#176#1056#183#1056#176#1057#8218#1056#181#1056#187#1057#1034' '#1056#1029#1056#181#1056#1110#1056#1109#1057#8218#1056#1109#1056#1030#1056#1029#1056#1109#1057#1027#1057#8218#1056#1105' '#1056 +
              #1113#1056#1029#1056#1110', %')
          ParentFont = False
        end
        object Memo29: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 584.126160000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            
              #1056#1116#1056#1109#1057#1026#1056#1112#1056#1105#1057#1026#1057#1107#1056#181#1056#1112#1056#1109#1056#181' '#1056#183#1056#1029#1056#176#1057#8225#1056#181#1056#1029#1056#1105#1056#181' '#1056#1113#1056#1029#1056#1110' '#1056#1029#1056#176' '#1056#1105#1056#1029#1057#8218#1056#181#1057#1026#1056#1030 +
              #1056#176#1056#187#1056#181', % ')
          ParentFont = False
        end
        object Memo31: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 644.126160000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            'ESR '#1057#8218#1057#1026#1056#181#1056#177', % ')
          ParentFont = False
        end
        object Memo32: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 599.126160000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            'BBER '#1056#1029#1056#1109#1057#1026#1056#1112', %')
          ParentFont = False
        end
        object Memo33: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 629.126160000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            'BBER '#1057#8218#1057#1026#1056#181#1056#177', % ')
          ParentFont = False
        end
        object Memo34: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 614.126160000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            'ESR '#1056#1029#1056#1109#1057#1026#1056#1112', % ')
          ParentFont = False
        end
        object Memo35: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 659.126160000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1119#1057#1026#1056#1105#1056#1110#1056#1109#1056#1169#1056#1029#1056#1109#1057#1027#1057#8218#1057#1034' ')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 310.000000000000000000
          Top = 253.228510000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Site1."name"]')
          ParentFont = False
        end
        object Memo36: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 485.000000000000000000
          Top = 253.228510000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Site2."name"]')
          ParentFont = False
        end
        object Memo37: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 310.000000000000000000
          Top = 283.228510000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."azimuth1"]')
          ParentFont = False
        end
        object Memo38: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 485.000000000000000000
          Top = 283.228510000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."azimuth2"]')
          ParentFont = False
        end
        object LinkEnd2threshold_BER_3: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 310.000000000000000000
          Top = 434.126160000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          DataSet = frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd1."threshold_BER_3"]')
          ParentFont = False
        end
        object LinkEnd2threshold_BER_6: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 310.000000000000000000
          Top = 449.126160000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          DataSet = frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd1."threshold_BER_6"]')
          ParentFont = False
        end
        object Memo41: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 310.000000000000000000
          Top = 358.228510000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          DataSet = frxDBDataset_link
          DataSetName = 'Link'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[antenna1."diameter"]')
          ParentFont = False
        end
        object Memo44: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 310.000000000000000000
          Top = 373.228510000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          DataSet = frxDBDataset_link
          DataSetName = 'Link'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[antenna1."polarization"]')
          ParentFont = False
        end
        object Memo50: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 310.000000000000000000
          Top = 328.228510000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          DataSet = frxDBDataset_link
          DataSetName = 'Link'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd1."bitrate_Mbps"]')
          ParentFont = False
        end
        object Memo52: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 310.000000000000000000
          Top = 343.228510000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          DataSet = frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd1."power_dBm"]')
          ParentFont = False
        end
        object Memo55: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 310.000000000000000000
          Top = 298.228510000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          DataSet = frxDBDataset_link
          DataSetName = 'Link'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd1."LinkEndType_name"]')
          ParentFont = False
        end
        object Memo56: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 310.000000000000000000
          Top = 313.228510000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          DataSet = frxDBDataset_link
          DataSetName = 'Link'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd1."tx_freq_MHz"]')
          ParentFont = False
        end
        object Memo57: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 485.000000000000000000
          Top = 298.228510000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          DataSet = frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd2."LinkEndType_name"]')
          ParentFont = False
        end
        object Memo39: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 485.000000000000000000
          Top = 313.228510000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          DataSet = frxDBDataset_link
          DataSetName = 'Link'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd2."tx_freq_MHz"]')
          ParentFont = False
        end
        object Memo40: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 485.000000000000000000
          Top = 328.228510000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          DataSet = frxDBDataset_link
          DataSetName = 'Link'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd2."bitrate_Mbps"]')
          ParentFont = False
        end
        object Memo46: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 485.000000000000000000
          Top = 343.228510000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          DataSet = frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd2."power_dBm"]')
          ParentFont = False
        end
        object Memo48: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 485.000000000000000000
          Top = 434.126160000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          DataSet = frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd2."threshold_BER_3"]')
          ParentFont = False
        end
        object Memo53: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 485.000000000000000000
          Top = 449.126160000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          DataSet = frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd2."threshold_BER_6"]')
          ParentFont = False
        end
        object Memo54: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 310.000000000000000000
          Top = 644.126160000000000000
          Width = 350.000000000000000000
          Height = 15.000000000000000000
          DataSet = frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Link."ESR_REQUIRED"]')
          ParentFont = False
        end
        object Memo58: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 310.000000000000000000
          Top = 659.126160000000000000
          Width = 350.000000000000000000
          Height = 15.000000000000000000
          DataSet = frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Link."status_str"]')
          ParentFont = False
        end
        object Memo59: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 310.000000000000000000
          Top = 614.126160000000000000
          Width = 350.000000000000000000
          Height = 15.000000000000000000
          DataSet = frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Link."ESR_NORM"]')
          ParentFont = False
        end
        object Memo60: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 310.000000000000000000
          Top = 629.126160000000000000
          Width = 350.000000000000000000
          Height = 15.000000000000000000
          DataSet = frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Link."BBER_REQUIRED"]')
          ParentFont = False
        end
        object Memo61: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 310.000000000000000000
          Top = 584.126160000000000000
          Width = 350.000000000000000000
          Height = 15.000000000000000000
          DataSet = frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Link."KNG_NORM"]')
          ParentFont = False
        end
        object Memo62: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 310.000000000000000000
          Top = 599.126160000000000000
          Width = 350.000000000000000000
          Height = 15.000000000000000000
          DataSet = frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Link."BBER_NORM"]')
          ParentFont = False
        end
        object Memo63: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 310.000000000000000000
          Top = 554.126160000000000000
          Width = 350.000000000000000000
          Height = 15.000000000000000000
          DataSet = frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Link."SESR_NORM"]')
          ParentFont = False
        end
        object Memo64: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 310.000000000000000000
          Top = 569.126160000000000000
          Width = 350.000000000000000000
          Height = 15.000000000000000000
          DataSet = frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          DisplayFormat.FormatStr = '%2.6n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Link."KNG"]')
          ParentFont = False
        end
        object Memo67: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 310.000000000000000000
          Top = 524.126160000000000000
          Width = 350.000000000000000000
          Height = 15.000000000000000000
          DataSet = frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Link."SESR_INTERFERENCE"]')
          ParentFont = False
        end
        object Memo69: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 310.000000000000000000
          Top = 539.126160000000000000
          Width = 350.000000000000000000
          Height = 15.000000000000000000
          DataSet = frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Link."SESR"]')
          ParentFont = False
        end
        object Memo71: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 310.000000000000000000
          Top = 494.126160000000000000
          Width = 350.000000000000000000
          Height = 15.000000000000000000
          DataSet = frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Link."SESR_SUBREFRACTION"]')
          ParentFont = False
        end
        object Memo73: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 310.000000000000000000
          Top = 509.126160000000000000
          Width = 350.000000000000000000
          Height = 15.000000000000000000
          DataSet = frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          DisplayFormat.FormatStr = '%2.6n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Link."SESR_RAIN"]')
          ParentFont = False
        end
        object Memo75: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 310.000000000000000000
          Top = 479.126160000000000000
          Width = 350.000000000000000000
          Height = 15.000000000000000000
          DataSet = frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Link."MARGIN_DISTANCE"]')
          ParentFont = False
        end
        object Memo76: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 310.000000000000000000
          Top = 464.126160000000000000
          Width = 350.000000000000000000
          Height = 15.000000000000000000
          DataSet = frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Link."MARGIN_HEIGHT"]')
          ParentFont = False
        end
        object Memo88: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 485.000000000000000000
          Top = 358.228510000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          DataSet = frxDBDataset_link
          DataSetName = 'Link'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[antenna2."diameter"]')
          ParentFont = False
        end
        object Memo84: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 485.000000000000000000
          Top = 373.228510000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          DataSet = frxDBDataset_link
          DataSetName = 'Link'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[antenna2."polarization"]')
          ParentFont = False
        end
        object Memo85: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 310.000000000000000000
          Top = 404.126160000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          DataSet = frxDBDataset_link
          DataSetName = 'Link'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd1."rx_level_dBm"]')
          ParentFont = False
        end
        object Memo86: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 485.000000000000000000
          Top = 404.126160000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          DataSet = frxDBDataset_link
          DataSetName = 'Link'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd2."rx_level_dBm"]')
          ParentFont = False
        end
        object Memo87: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 310.000000000000000000
          Top = 419.126160000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          DataSet = frxDBDataset_link
          DataSetName = 'Link'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd1."kratnostBySpace_name"]')
          ParentFont = False
        end
        object Memo94: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 485.000000000000000000
          Top = 419.126160000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          DataSet = frxDBDataset_link
          DataSetName = 'Link'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd2."kratnostBySpace_name"]')
          ParentFont = False
        end
        object Memo131: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 253.228510000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          ParentFont = False
        end
        object Memo65: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 310.000000000000000000
          Top = 268.228510000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          DataSet = frxDBDataset_LinkEnd1
          DataSetName = 'LinkEnd1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[antenna1."height"]')
          ParentFont = False
        end
        object Memo102: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 485.000000000000000000
          Top = 268.228510000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          DataSet = frxDBDataset_LinkEnd1
          DataSetName = 'LinkEnd1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[antenna2."height"]')
          ParentFont = False
        end
        object Memo43: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 64.252010000000000000
          Width = 110.000000000000000000
          Height = 32.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1119#1056#187#1056#1109#1057#8240#1056#176#1056#1169#1056#1108#1056#176)
          ParentFont = False
        end
        object Memo_left_1: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 110.000000000000000000
          Top = 64.252010000000000000
          Width = 117.165354330000000000
          Height = 32.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Site1."name"]')
          ParentFont = False
          UseDefaultCharset = True
        end
        object Memo68: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 126.370130000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1113#1056#1109#1056#1109#1057#1026#1056#1169#1056#1105#1056#1029#1056#176#1057#8218#1057#8249' WGS')
          ParentFont = False
        end
        object Memo70: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 156.370130000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#8217#1057#8249#1057#1027#1056#1109#1057#8218#1056#176' '#1056#1111#1056#1109#1056#1169#1056#1030#1056#181#1057#1027#1056#176', m')
          ParentFont = False
        end
        object Memo72: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 171.370130000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1106#1056#183#1056#1105#1056#1112#1057#1107#1057#8218)
          ParentFont = False
        end
        object Memo74: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 186.370130000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1032#1056#1110#1056#1109#1056#187' '#1056#1112#1056#181#1057#1027#1057#8218#1056#176)
          ParentFont = False
        end
        object Memo100: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 201.370130000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#8221#1056#1105#1056#176#1056#1112#1056#181#1057#8218#1057#1026' '#1056#176#1056#1029#1057#8218#1056#181#1056#1029#1056#1029#1057#8249)
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 110.000000000000000000
          Top = 126.370130000000000000
          Width = 117.165354330000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[DegreeToStr (<Site1."lat_WGS">)]')
          ParentFont = False
          UseDefaultCharset = True
        end
        object Memo1: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 110.000000000000000000
          Top = 156.370130000000000000
          Width = 117.165354330000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[antenna1."height"]')
          ParentFont = False
          UseDefaultCharset = True
        end
        object Memo95: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 110.000000000000000000
          Top = 171.370130000000000000
          Width = 117.165354330000000000
          Height = 15.000000000000000000
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."azimuth1"]')
          ParentFont = False
          UseDefaultCharset = True
        end
        object Memo97: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 110.000000000000000000
          Top = 201.370130000000000000
          Width = 117.165354330000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[antenna1."diameter"]')
          ParentFont = False
          UseDefaultCharset = True
        end
        object Memo98: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 110.000000000000000000
          Top = 141.370130000000000000
          Width = 117.165354330000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[DegreeToStr (<Site1."lon_WGS">)]')
          ParentFont = False
          UseDefaultCharset = True
        end
        object Memo96: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 110.000000000000000000
          Top = 186.370130000000000000
          Width = 117.165354330000000000
          Height = 15.000000000000000000
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Antenna1."tilt"]')
          ParentFont = False
          UseDefaultCharset = True
        end
        object Memo121: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 141.370130000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 96.370130000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1113#1056#1109#1056#1109#1057#1026#1056#1169#1056#1105#1056#1029#1056#176#1057#8218#1057#8249' CK-42')
          ParentFont = False
        end
        object Memo30: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 110.000000000000000000
          Top = 96.370130000000000000
          Width = 117.165354330000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[DegreeToStr (<Site1."lat">)]')
          ParentFont = False
          UseDefaultCharset = True
        end
        object Memo66: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 110.000000000000000000
          Top = 111.370130000000000000
          Width = 117.165354330000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[DegreeToStr (<Site1."lon">)]')
          ParentFont = False
          UseDefaultCharset = True
        end
        object Memo99: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 111.370130000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          ParentFont = False
        end
        object Memo136: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 216.370130000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1032#1057#1027#1056#1105#1056#187#1056#181#1056#1029#1056#1105#1056#181' '#1056#176#1056#1029#1057#8218#1056#181#1056#1029#1056#1029#1057#8249)
          ParentFont = False
        end
        object Memo139: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 110.000000000000000000
          Top = 216.370130000000000000
          Width = 117.165354330000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[antenna1."gain"]')
          ParentFont = False
          UseDefaultCharset = True
        end
        object Memo45: TfrxMemoView
          AllowVectorExport = True
          Left = 242.283474330000000000
          Top = 79.252010000000000000
          Width = 160.000000000000000000
          Height = 15.000000000000000000
          DataSet = frxDBDataset_link
          DataSetName = 'Link'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#8221#1056#187#1056#1105#1056#1029#1056#176' '#1056#1105#1056#1029#1057#8218#1056#181#1057#1026#1056#1030#1056#176#1056#187#1056#176', km')
          ParentFont = False
        end
        object Memo47: TfrxMemoView
          AllowVectorExport = True
          Left = 242.283474330000000000
          Top = 94.252010000000000000
          Width = 160.000000000000000000
          Height = 15.000000000000000000
          DataSet = frxDBDataset_link
          DataSetName = 'Link'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1032#1057#1026#1056#1109#1056#1030#1056#181#1056#1029#1057#1034' '#1057#1027#1056#1105#1056#1110#1056#1029#1056#176#1056#187#1056#176', dBm')
          ParentFont = False
        end
        object Memo49: TfrxMemoView
          AllowVectorExport = True
          Left = 242.283474330000000000
          Top = 109.252010000000000000
          Width = 160.000000000000000000
          Height = 15.000000000000000000
          DataSet = frxDBDataset_link
          DataSetName = 'Link'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#160#1056#176#1056#177#1056#1109#1057#8225#1056#176#1057#1039' '#1057#8225#1056#176#1057#1027#1057#8218#1056#1109#1057#8218#1056#176', GHz')
          ParentFont = False
        end
        object Memo51: TfrxMemoView
          AllowVectorExport = True
          Left = 242.283474330000000000
          Top = 124.252010000000000000
          Width = 160.000000000000000000
          Height = 15.000000000000000000
          DataSet = frxDBDataset_link
          DataSetName = 'Link'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1114#1056#1109#1057#8240#1056#1029#1056#1109#1057#1027#1057#8218#1057#1034' '#1056#1111#1056#181#1057#1026#1056#181#1056#1169#1056#176#1057#8218#1057#8225#1056#1105#1056#1108#1056#176', dBm')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Left = 242.283474330000000000
          Top = 139.252010000000000000
          Width = 160.000000000000000000
          Height = 15.000000000000000000
          DataSet = frxDBDataset_link
          DataSetName = 'Link'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1038#1056#1108#1056#1109#1057#1026#1056#1109#1057#1027#1057#8218#1057#1034' '#1056#1111#1056#181#1057#1026#1056#181#1056#1169#1056#176#1057#8225#1056#1105', Mbit/s')
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 242.283474330000000000
          Top = 154.252010000000000000
          Width = 160.000000000000000000
          Height = 15.000000000000000000
          DataSet = frxDBDataset_link
          DataSetName = 'Link'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1118#1056#1105#1056#1111' '#1056#1109#1056#177#1056#1109#1057#1026#1057#1107#1056#1169#1056#1109#1056#1030#1056#176#1056#1029#1056#1105#1057#1039)
          ParentFont = False
        end
        object Memo123: TfrxMemoView
          AllowVectorExport = True
          Left = 402.283474330000000000
          Top = 94.252010000000000000
          Width = 68.031496060000000000
          Height = 15.000000000000000000
          DataField = 'rx_level_dBm'
          DataSet = frxDBDataset_link
          DataSetName = 'Link'
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."rx_level_dBm"]')
          ParentFont = False
        end
        object Memo12098064: TfrxMemoView
          AllowVectorExport = True
          Left = 402.283474330000000000
          Top = 139.252010000000000000
          Width = 68.031496060000000000
          Height = 15.000000000000000000
          DataSet = frxDBDataset_link
          DataSetName = 'Link'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd1."bitrate_Mbps"]')
          ParentFont = False
        end
        object Memo12511312: TfrxMemoView
          AllowVectorExport = True
          Left = 402.283474330000000000
          Top = 154.252010000000000000
          Width = 68.031496060000000000
          Height = 15.000000000000000000
          DataSet = frxDBDataset_link
          DataSetName = 'Link'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd1."LinkEndType_name"]')
          ParentFont = False
        end
        object Memo127: TfrxMemoView
          AllowVectorExport = True
          Left = 402.283474330000000000
          Top = 109.252010000000000000
          Width = 68.031496060000000000
          Height = 15.000000000000000000
          DataSet = frxDBDataset_link
          DataSetName = 'Link'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[FloatToStr( <LinkEnd1."tx_freq_MHz"> / 1000 )]')
          ParentFont = False
        end
        object Memo128: TfrxMemoView
          AllowVectorExport = True
          Left = 402.283474330000000000
          Top = 79.252010000000000000
          Width = 68.031496060000000000
          Height = 15.000000000000000000
          DataSet = frxDBDataset_link
          DataSetName = 'Link'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."length_km"]')
          ParentFont = False
        end
        object Memo126333: TfrxMemoView
          AllowVectorExport = True
          Left = 402.283474330000000000
          Top = 124.252010000000000000
          Width = 68.031496060000000000
          Height = 15.000000000000000000
          DataSet = frxDBDataset_link
          DataSetName = 'Link'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd1."power_dBm"]')
          ParentFont = False
        end
        object Memo42: TfrxMemoView
          AllowVectorExport = True
          Left = 242.283474330000000000
          Top = 64.252010000000000000
          Width = 160.000000000000000000
          Height = 15.000000000000000000
          DataSet = frxDBDataset_link
          DataSetName = 'Link'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1119#1057#1026#1056#1105#1056#183#1056#1029#1056#176#1056#1108' '#1056#1111#1057#1026#1056#1105#1056#1110#1056#1109#1056#1169#1056#1029#1056#1109#1057#1027#1057#8218#1056#1105)
          ParentFont = False
        end
        object Memo122: TfrxMemoView
          AllowVectorExport = True
          Left = 402.283474330000000000
          Top = 64.252010000000000000
          Width = 68.031496060000000000
          Height = 15.000000000000000000
          DataSet = frxDBDataset_link
          DataSetName = 'Link'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."status_str"]')
          ParentFont = False
        end
        object Memo134: TfrxMemoView
          AllowVectorExport = True
          Left = 242.283474330000000000
          Top = 169.252010000000000000
          Width = 160.000000000000000000
          Height = 15.000000000000000000
          DataSet = frxDBDataset_link
          DataSetName = 'Link'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#8212#1056#176#1056#1111#1056#176#1057#1027' '#1056#1029#1056#176' '#1056#183#1056#176#1056#1112#1056#1105#1057#1026#1056#176#1056#1029#1056#1105#1057#1039', dB')
          ParentFont = False
        end
        object Memo135: TfrxMemoView
          AllowVectorExport = True
          Left = 402.283474330000000000
          Top = 169.252010000000000000
          Width = 68.031496060000000000
          Height = 15.000000000000000000
          DataSet = frxDBDataset_link
          DataSetName = 'Link'
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."fade_margin_dB"]')
          ParentFont = False
        end
        object Memo105: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 494.724873230000000000
          Top = 64.252010000000000000
          Width = 110.000000000000000000
          Height = 30.236218030000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1119#1056#187#1056#1109#1057#8240#1056#176#1056#1169#1056#1108#1056#176)
          ParentFont = False
        end
        object Memo106: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 604.724873230000000000
          Top = 64.252010000000000000
          Width = 113.385826770000000000
          Height = 30.236220470000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Site2."name"]')
          ParentFont = False
        end
        object Memo108: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 494.724873230000000000
          Top = 94.488228030000000000
          Width = 110.000000000000000000
          Height = 15.118110240000000000
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1113#1056#1109#1056#1109#1057#1026#1056#1169#1056#1105#1056#1029#1056#176#1057#8218#1057#8249' CK-42')
          ParentFont = False
        end
        object Memo109: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 494.724873230000000000
          Top = 154.960671420000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#8217#1057#8249#1057#1027#1056#1109#1057#8218#1056#176' '#1056#1111#1056#1109#1056#1169#1056#1030#1056#181#1057#1027#1056#176', m')
          ParentFont = False
        end
        object Memo110: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 494.724873230000000000
          Top = 170.149660000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1106#1056#183#1056#1105#1056#1112#1057#1107#1057#8218)
          ParentFont = False
        end
        object Memo111: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 494.724873230000000000
          Top = 185.149660000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1032#1056#1110#1056#1109#1056#187' '#1056#1112#1056#181#1057#1027#1057#8218#1056#176)
          ParentFont = False
        end
        object Memo112: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 494.724873230000000000
          Top = 200.149660000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#8221#1056#1105#1056#176#1056#1112#1056#181#1057#8218#1057#1026' '#1056#176#1056#1029#1057#8218#1056#181#1056#1029#1056#1029#1057#8249)
          ParentFont = False
        end
        object Memo114: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 604.724873230000000000
          Top = 94.488228030000000000
          Width = 113.385826770000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[DegreeToStr (<Site2."lat">)]')
          ParentFont = False
        end
        object Memo115: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 604.724873230000000000
          Top = 154.960671420000000000
          Width = 113.385826770000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[antenna2."height"]')
          ParentFont = False
        end
        object Memo116: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 604.724873230000000000
          Top = 170.149660000000000000
          Width = 113.385826770000000000
          Height = 15.000000000000000000
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."azimuth2"]')
          ParentFont = False
        end
        object Memo117: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 604.724873230000000000
          Top = 200.149660000000000000
          Width = 113.385826770000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Antenna2."diameter"]')
          ParentFont = False
        end
        object Memo118: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 604.724873230000000000
          Top = 109.606340710000000000
          Width = 113.385826770000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[DegreeToStr (<Site2."lon">)]')
          ParentFont = False
        end
        object Memo130: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 494.724873230000000000
          Top = 109.606340710000000000
          Width = 110.000000000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 604.724873230000000000
          Top = 185.149660000000000000
          Width = 113.385826770000000000
          Height = 15.000000000000000000
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Antenna2."tilt"]')
          ParentFont = False
        end
        object Memo107: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 494.724873230000000000
          Top = 124.724450940000000000
          Width = 110.000000000000000000
          Height = 15.118110240000000000
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1113#1056#1109#1056#1109#1057#1026#1056#1169#1056#1105#1056#1029#1056#176#1057#8218#1057#8249' WGS')
          ParentFont = False
        end
        object Memo113: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 604.724873230000000000
          Top = 124.724450940000000000
          Width = 113.385826770000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[DegreeToStr (<Site2."lat_wgs">)]')
          ParentFont = False
        end
        object Memo119: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 604.724873230000000000
          Top = 139.842561180000000000
          Width = 113.385826770000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[DegreeToStr (<Site2."lon_wgs">)]')
          ParentFont = False
        end
        object Memo129: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 494.724873230000000000
          Top = 139.842561180000000000
          Width = 110.000000000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          ParentFont = False
        end
        object Memo140: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 494.724873230000000000
          Top = 215.433112360000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1032#1057#1027#1056#1105#1056#187#1056#181#1056#1029#1056#1105#1056#181' '#1056#176#1056#1029#1057#8218#1056#181#1056#1029#1056#1029#1057#8249)
          ParentFont = False
        end
        object Memo141: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 604.724873230000000000
          Top = 215.433112360000000000
          Width = 113.385826770000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Antenna2."gain"]')
          ParentFont = False
        end
        object Memo124: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 388.291590000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            'Link ID')
          ParentFont = False
        end
        object Memo125: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 310.000000000000000000
          Top = 388.291590000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          DataSet = frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd1."linkID"]')
          ParentFont = False
        end
        object Memo126: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 485.000000000000000000
          Top = 388.291590000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          DataSet = frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd2."linkID"]')
          ParentFont = False
        end
        object Memo103: TfrxMemoView
          AllowVectorExport = True
          Left = 242.267716540000000000
          Top = 185.196970000000000000
          Width = 160.000000000000000000
          Height = 15.000000000000000000
          DataSet = frxDBDataset_link
          DataSetName = 'Link'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1114#1056#1109#1056#1169#1057#1107#1056#187#1057#1039#1057#8224#1056#1105#1057#1039)
          ParentFont = False
        end
        object Memo142: TfrxMemoView
          AllowVectorExport = True
          Left = 402.889920000000000000
          Top = 185.196970000000000000
          Width = 68.031496060000000000
          Height = 15.000000000000000000
          DataSet = frxDBDataset_link
          DataSetName = 'Link'
          DisplayFormat.FormatStr = '%2.2f'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd1."modulation_type"]')
          ParentFont = False
        end
        object Memo143: TfrxMemoView
          AllowVectorExport = True
          Left = 242.267716540000000000
          Top = 200.315090000000000000
          Width = 160.000000000000000000
          Height = 15.000000000000000000
          DataSet = frxDBDataset_link
          DataSetName = 'Link'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1025#1056#1105#1057#1026#1056#1105#1056#1029#1056#176' '#1056#1111#1056#1109#1056#187#1056#1109#1057#1027#1057#8249', MHz')
          ParentFont = False
        end
        object Memo144: TfrxMemoView
          AllowVectorExport = True
          Left = 402.889920000000000000
          Top = 200.315090000000000000
          Width = 68.031496060000000000
          Height = 15.000000000000000000
          DataSet = frxDBDataset_link
          DataSetName = 'Link'
          DisplayFormat.FormatStr = '%2.0f'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd1."channel_width_MHz"]')
          ParentFont = False
        end
      end
      object Child_profile: TfrxChild
        FillType = ftBrush
        Frame.Typ = []
        Height = 58.400000000000000000
        Top = 1024.252630000000000000
        Width = 718.110700000000000000
        Child = frxReport_full.Child_profile1
        ToNRows = 0
        ToNRowsMode = rmCount
        object Picture_profile: TfrxPictureView
          Align = baWidth
          AllowVectorExport = True
          Top = 20.000000000000000000
          Width = 718.110700000000000000
          Height = 36.000000000000000000
          OnBeforePrint = 'Picture_profileOnBeforePrint'
          DataField = 'profile_img'
          DataSet = frxDBDataset_Link_img
          DataSetName = 'Link_img'
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Stretched = False
          HightQuality = True
          Transparent = False
          TransparentColor = clWhite
        end
        object Memo80: TfrxMemoView
          Align = baWidth
          AllowVectorExport = True
          ShiftMode = smWhenOverlapped
          Width = 718.110700000000000000
          Height = 16.800000000000000000
          AutoWidth = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8 = (
            
              #1056#152#1056#183#1056#1109#1056#177#1057#1026#1056#176#1056#182#1056#181#1056#1029#1056#1105#1056#181' '#1056#1111#1057#1026#1056#1109#1057#8222#1056#1105#1056#187#1057#1039' '#1056#1111#1057#1026#1056#1105' '#1056#1029#1056#1109#1057#1026#1056#1112#1056#176#1056#187#1057#1034#1056#1029#1056#1109#1056 +
              #8470' '#1057#1026#1056#181#1057#8222#1057#1026#1056#176#1056#1108#1057#8224#1056#1105#1056#1105)
          ParentFont = False
        end
      end
      object Child_map: TfrxChild
        FillType = ftBrush
        Frame.Typ = []
        Height = 51.918120000000000000
        Top = 948.662030000000000000
        Width = 718.110700000000000000
        OnAfterCalcHeight = 'Child_mapOnAfterCalcHeight'
        OnBeforePrint = 'Child_mapOnBeforePrint'
        AllowSplit = True
        Child = frxReport_full.Child_profile
        ToNRows = 0
        ToNRowsMode = rmCount
        object Picture_map: TfrxPictureView
          Description = 'Map'
          Align = baWidth
          AllowVectorExport = True
          Top = 7.559060000000000000
          Width = 718.110700000000000000
          Height = 28.000000000000000000
          OnBeforePrint = 'Picture_mapOnBeforePrint'
          DataField = 'map_img'
          DataSet = frxDBDataset_Link_img
          DataSetName = 'Link_img'
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Stretched = False
          HightQuality = True
          Transparent = False
          TransparentColor = clWhite
        end
      end
      object Child_profile1: TfrxChild
        FillType = ftBrush
        Frame.Typ = []
        Height = 76.000000000000000000
        Top = 1107.402290000000000000
        Width = 718.110700000000000000
        Child = frxReport_full.Child_profile2
        ToNRows = 0
        ToNRowsMode = rmCount
        object Picture_profile1: TfrxPictureView
          Align = baWidth
          AllowVectorExport = True
          ShiftMode = smWhenOverlapped
          Top = 20.000000000000000000
          Width = 718.110700000000000000
          Height = 40.000000000000000000
          OnBeforePrint = 'Picture_profile1OnBeforePrint'
          DataField = 'img1'
          DataSet = frxDBDataset_Link_img
          DataSetName = 'Link_img'
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Stretched = False
          HightQuality = True
          Transparent = False
          TransparentColor = clWhite
        end
        object Memo81: TfrxMemoView
          Align = baWidth
          AllowVectorExport = True
          ShiftMode = smWhenOverlapped
          Width = 718.110700000000000000
          Height = 16.800000000000000000
          AutoWidth = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8 = (
            
              #1056#152#1056#183#1056#1109#1056#177#1057#1026#1056#176#1056#182#1056#181#1056#1029#1056#1105#1056#181' '#1056#1111#1057#1026#1056#1109#1057#8222#1056#1105#1056#187#1057#1039' '#1056#1111#1057#1026#1056#1105' 20% '#1057#1026#1056#181#1057#8222#1057#1026#1056#176#1056#1108#1057#8224#1056 +
              #1105#1056#1105)
          ParentFont = False
        end
      end
      object Child_profile2: TfrxChild
        FillType = ftBrush
        Frame.Typ = []
        Height = 76.800000000000000000
        Top = 1205.670070000000000000
        Width = 718.110700000000000000
        Child = frxReport_full.Child_profile3
        ToNRows = 0
        ToNRowsMode = rmCount
        object Picture_profile2: TfrxPictureView
          Align = baWidth
          AllowVectorExport = True
          ShiftMode = smWhenOverlapped
          Top = 20.000000000000000000
          Width = 718.110700000000000000
          Height = 43.000000000000000000
          OnBeforePrint = 'Picture_profile2OnBeforePrint'
          DataField = 'img2'
          DataSet = frxDBDataset_Link_img
          DataSetName = 'Link_img'
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Stretched = False
          HightQuality = True
          Transparent = False
          TransparentColor = clWhite
        end
        object Memo83: TfrxMemoView
          Align = baWidth
          AllowVectorExport = True
          ShiftMode = smWhenOverlapped
          Width = 718.110700000000000000
          Height = 16.800000000000000000
          AutoWidth = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8 = (
            
              #1056#152#1056#183#1056#1109#1056#177#1057#1026#1056#176#1056#182#1056#181#1056#1029#1056#1105#1056#181' '#1056#1111#1057#1026#1056#1109#1057#8222#1056#1105#1056#187#1057#1039' '#1056#1030' '#1057#1107#1057#1027#1056#187#1056#1109#1056#1030#1056#1105#1057#1039#1057#8230' '#1057#1027#1057#1107#1056#177 +
              #1057#1026#1056#181#1057#8222#1057#1026#1056#176#1056#1108#1057#8224#1056#1105#1056#1105)
          ParentFont = False
        end
      end
      object Child_Profile_items: TfrxChild
        FillType = ftBrush
        Frame.Typ = []
        Height = 40.000000000000000000
        Top = 1451.339520000000000000
        Width = 718.110700000000000000
        StartNewPage = True
        ToNRows = 0
        ToNRowsMode = rmCount
        object Subreport_Profile: TfrxSubreport
          Align = baWidth
          AllowVectorExport = True
          Width = 718.110700000000000000
          Height = 24.000000000000000000
          Page = frxReport_full.Page6
        end
      end
      object Child_calc_res: TfrxChild
        FillType = ftBrush
        Frame.Typ = []
        Height = 40.000000000000000000
        Top = 1387.087510000000000000
        Width = 718.110700000000000000
        AllowSplit = True
        Child = frxReport_full.Child_Profile_items
        StartNewPage = True
        ToNRows = 0
        ToNRowsMode = rmCount
        object Subreport_calc_res: TfrxSubreport
          Align = baLeft
          AllowVectorExport = True
          Width = 718.110700000000000000
          Height = 20.000000000000000000
          Page = frxReport_full.Page8
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        Frame.Typ = []
        Height = 24.000000000000000000
        Top = 1553.386830000000000000
        Width = 718.110700000000000000
        object Page: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 638.110700000000000000
          Width = 80.000000000000000000
          Height = 16.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8 = (
            '[Page#]')
          ParentFont = False
        end
        object Memo138: TfrxMemoView
          Align = baWidth
          AllowVectorExport = True
          Width = 638.110700000000000000
          Height = 16.000000000000000000
          AutoWidth = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clSilver
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8 = (
            
              #1056#1115#1057#8218#1057#8225#1056#181#1057#8218' '#1056#1030#1057#8249#1056#1111#1056#1109#1056#187#1056#1029#1056#181#1056#1029' '#1056#1030' '#1056#1111#1057#1026#1056#1109#1056#1110#1057#1026#1056#176#1056#1112#1056#1112#1056#181' ONEPLAN RPLS-D' +
              'B Link')
          ParentFont = False
        end
      end
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        Frame.Typ = []
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        Height = 28.000000000000000000
        ParentFont = False
        Top = 18.897650000000000000
        Width = 718.110700000000000000
        object Linkcalc_method_name1: TfrxMemoView
          Align = baWidth
          AllowVectorExport = True
          Left = 141.600000000000000000
          Width = 576.510700000000000000
          Height = 16.000000000000000000
          AutoWidth = True
          DataField = 'calc_method_name'
          DataSet = frxDBDataset_link
          DataSetName = 'Link'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clSilver
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8 = (
            '[Link."calc_method_name"]')
          ParentFont = False
        end
        object Memo137: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Width = 141.600000000000000000
          Height = 16.000000000000000000
          AutoWidth = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clSilver
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8 = (
            #1056#1114#1056#181#1057#8218#1056#1109#1056#1169#1056#1105#1056#1108#1056#176' '#1057#1026#1056#176#1057#1027#1057#8225#1056#181#1057#8218#1056#176': ')
          ParentFont = False
        end
      end
    end
    object Page6: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      object Header1: TfrxHeader
        FillType = ftBrush
        Frame.Typ = []
        Height = 35.000000000000000000
        Top = 18.897650000000000000
        Width = 718.110700000000000000
        object Memo93: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Width = 60.000000000000000000
          Height = 35.000000000000000000
          Visible = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            'Index')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 60.000000000000000000
          Width = 100.000000000000000000
          Height = 35.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#160#1056#176#1057#1027#1057#1027#1057#8218#1056#1109#1057#1039#1056#1029#1056#1105#1056#181', '#1056#1108#1056#1112)
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 260.000000000000000000
          Width = 100.000000000000000000
          Height = 35.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1113#1057#1026#1056#1105#1056#1030#1056#1105#1056#183#1056#1029#1056#176' '#1056#183#1056#181#1056#1112#1056#187#1056#1105)
          ParentFont = False
        end
        object Memo120: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 360.000000000000000000
          Width = 100.000000000000000000
          Height = 35.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#8217#1057#8249#1057#1027#1056#1109#1057#8218#1056#176' '#1056#1114#1056#1119)
          ParentFont = False
        end
        object Memo132: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 160.000000000000000000
          Width = 100.000000000000000000
          Height = 35.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#8217#1057#8249#1057#1027#1056#1109#1057#8218#1056#176' '#1057#1026#1056#181#1056#187#1057#1034#1056#181#1057#8222#1056#176)
          ParentFont = False
        end
        object Memo101: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 460.000000000000000000
          Width = 196.000000000000000000
          Height = 35.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1118#1056#1105#1056#1111' '#1056#1114#1056#1119)
          ParentFont = False
        end
      end
      object MasterData5: TfrxMasterData
        FillType = ftBrush
        Frame.Typ = []
        Height = 16.000000000000000000
        Top = 75.590600000000000000
        Width = 718.110700000000000000
        DataSet = frxDBDataset_Profile_items
        DataSetName = 'Profile'
        RowCount = 0
        object Memo89: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 60.000000000000000000
          Width = 100.000000000000000000
          Height = 16.000000000000000000
          DataField = 'distance_km'
          DataSet = frxDBDataset_Profile_items
          DataSetName = 'Profile'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Profile."distance_km"]')
          ParentFont = False
        end
        object Memo90: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Width = 60.000000000000000000
          Height = 16.000000000000000000
          Visible = False
          DataField = 'index'
          DataSet = frxDBDataset_Profile_items
          DataSetName = 'Profile'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Profile."index"]')
          ParentFont = False
        end
        object Memo91: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 260.000000000000000000
          Width = 100.000000000000000000
          Height = 16.000000000000000000
          DataField = 'earth_h'
          DataSet = frxDBDataset_Profile_items
          DataSetName = 'Profile'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Profile."earth_h"]')
          ParentFont = False
        end
        object Memo92: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 160.000000000000000000
          Width = 100.000000000000000000
          Height = 16.000000000000000000
          OnBeforePrint = 'Memo92OnBeforePrint'
          DataField = 'ground_h'
          DataSet = frxDBDataset_Profile_items
          DataSetName = 'Profile'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Profile."ground_h"]')
          ParentFont = False
        end
        object Memo133: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 360.000000000000000000
          Width = 100.000000000000000000
          Height = 16.000000000000000000
          DataField = 'clutter_h'
          DataSet = frxDBDataset_Profile_items
          DataSetName = 'Profile'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Profile."clutter_h"]')
          ParentFont = False
        end
        object Shape_Color: TfrxShapeView
          Align = baLeft
          AllowVectorExport = True
          Left = 460.000000000000000000
          Width = 20.000000000000000000
          Height = 16.000000000000000000
          OnBeforePrint = 'Shape_ColorOnBeforePrint'
          Fill.BackColor = clTeal
          Frame.Typ = []
        end
        object Memo104: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 480.000000000000000000
          Width = 176.000000000000000000
          Height = 16.000000000000000000
          DataField = 'clutter_name'
          DataSet = frxDBDataset_Profile_items
          DataSetName = 'Profile'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Profile."clutter_name"]')
          ParentFont = False
        end
      end
    end
    object Page8: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      object DetailData1: TfrxDetailData
        FillType = ftBrush
        Frame.Typ = []
        Height = 18.000000000000000000
        Top = 64.252010000000000000
        Width = 718.110700000000000000
        DataSet = frxDBDataset_Items
        DataSetName = 'Items'
        RowCount = 0
        object Itemscaption: TfrxMemoView
          Align = baWidth
          AllowVectorExport = True
          Left = 36.000000000000000000
          Width = 502.110700000000000000
          Height = 18.000000000000000000
          DataField = 'caption'
          DataSet = frxDBDataset_Items
          DataSetName = 'Items'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Items."caption"]')
          ParentFont = False
        end
        object Memo77: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 538.110700000000000000
          Width = 90.000000000000000000
          Height = 18.000000000000000000
          DataField = 'value'
          DataSet = frxDBDataset_Items
          DataSetName = 'Items'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Items."value"]')
          ParentFont = False
        end
        object Memo78: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 628.110700000000000000
          Width = 90.000000000000000000
          Height = 18.000000000000000000
          DataField = 'value2'
          DataSet = frxDBDataset_Items
          DataSetName = 'Items'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Items."value2"]')
          ParentFont = False
        end
        object Memo79: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Width = 36.000000000000000000
          Height = 18.000000000000000000
          DataSet = frxDBDataset_Items
          DataSetName = 'Items'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Items."code"]')
          ParentFont = False
        end
      end
      object MasterData6: TfrxMasterData
        FillType = ftBrush
        Frame.Typ = []
        Height = 24.000000000000000000
        Top = 18.897650000000000000
        Width = 718.110700000000000000
        DataSet = frxDBDataset_group
        DataSetName = 'group'
        RowCount = 0
        object groupcaption: TfrxMemoView
          Align = baWidth
          AllowVectorExport = True
          Left = 36.000000000000000000
          Top = 3.779530000000000000
          Width = 682.110700000000000000
          Height = 16.000000000000000000
          DataField = 'caption'
          DataSet = frxDBDataset_group
          DataSetName = 'group'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[group."caption"]')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 3.779530000000000000
          Width = 36.000000000000000000
          Height = 16.000000000000000000
          Visible = False
          DataField = 'id'
          DataSet = frxDBDataset_group
          DataSetName = 'group'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[group."id"]')
          ParentFont = False
        end
      end
    end
  end
  object ds_Repeater: TDataSource
    DataSet = qry_LInk_repeater
    Left = 768
    Top = 256
  end
  object frxDBDataset_repeater_ant: TfrxDBDataset
    UserName = 'repeater_ant'
    CloseDataSource = False
    DataSet = qry_LInk_repeater_ant
    BCDToCurrency = False
    Left = 1067
    Top = 308
  end
  object qry_LInk_repeater_property: TADOQuery
    OnCalcFields = qry_Property1CalcFields
    DataSource = ds_Repeater
    Parameters = <>
    Left = 912
    Top = 200
  end
  object frxDBDataset_repeater_property: TfrxDBDataset
    UserName = 'site3'
    CloseDataSource = False
    DataSet = qry_LInk_repeater_property
    BCDToCurrency = False
    Left = 916
    Top = 308
  end
  object frxReport_base: TfrxReport
    Version = '6.2.11'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 42217.455947581020000000
    ReportOptions.LastChange = 42217.455947581020000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    Left = 1032
    Top = 88
    Datasets = <>
    Variables = <>
    Style = <>
  end
  object frxReport_repeater: TfrxReport
    Version = '6.2.11'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.AllowEdit = False
    PreviewOptions.Buttons = [pbPrint, pbExport, pbZoom, pbTools, pbNavigator, pbExportQuick, pbNoFullScreen]
    PreviewOptions.ThumbnailVisible = True
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = #1055#1086' '#1091#1084#1086#1083#1095#1072#1085#1080#1102
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 41730.469225335600000000
    ReportOptions.LastChange = 42227.788202812500000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      ''
      '      '
      '  '
      ''
      ''
      'procedure frxReportOnStartReport(Sender: TfrxComponent);'
      'const'
      '  DEF_MAP_H = 800;'
      '  '
      '  DEF_H = 450;'
      '  DEF_OFFSET = 25;'
      ''
      '  DEF_TOP = 20;                                  '
      'begin'
      '//  Picture_map.Width:=200;                '
      '  '
      '  Picture_map.Height     :=DEF_MAP_H; '
      '  Picture_profile.Height :=DEF_H;'
      ''
      '    '
      '//  Picture_profile1.Height:=DEF_H;                '
      '//  Picture_profile2.Height:=DEF_H;                '
      '//  Picture_profile3.Height:=DEF_H;                '
      '          '
      '  '
      ' Child_map.Height    :=DEF_MAP_H; // + DEF_OFFSET;  '
      ' Child_profile.Height:=DEF_H    ; // + DEF_OFFSET;'
      '    '
      '//  Child_profile1.Height:=DEF_H + DEF_OFFSET;              '
      '//  Child_profile2.Height:=DEF_H + DEF_OFFSET;              '
      '//  Child_profile3.Height:=DEF_H + DEF_OFFSET;'
      ''
      '//  Child_profile1.Top:=DEF_TOP;              '
      '//  Child_profile2.Top:=DEF_TOP;              '
      '//  Child_profile3.Top:=DEF_TOP;'
      '  '
      ''
      '// -- Picture_map.Dataset:='#39'Link_img'#39';      '
      
        '//  --Picture_profile.Dataset:='#39'Link_img'#39';                      ' +
        '    '
      
        '//  Picture_profile1.Dataset:='#39'Link_img'#39';                       ' +
        '   '
      
        '//  Picture_profile2.Dataset:='#39'Link_img'#39';                       ' +
        '   '
      
        '//  Picture_profile3.Dataset:='#39'Link_img'#39';                       ' +
        '   '
      '              '
      ''
      ''
      '             '
      '                                       '
      'end;'
      ''
      ''
      'procedure Shape_ColorOnBeforePrint(Sender: TfrxComponent);'
      'begin'
      
        '  Shape1_Color.color := <Profile."clutter_color">               ' +
        '                                                     '
      'end;'
      '     '
      '        '
      '     '
      '  '
      '//------------------------------------------------------------'
      'function DegreeToStr (aValue: double): String;'
      '//------------------------------------------------------------'
      'const'
      '//  STR_BLPOINT_FORMAT1 = '#39'%d'#176'%.2d'#39#39'%s%1.2f'#39#39#39#39#39';'
      '  STR_BLPOINT_FORMAT1 = '#39'%d'#176' %.2d'#39#39' %1.2f'#39#39#39#39#39';'
      'var    '
      ' iDeg,iMin:integer; '
      ' eSec: Double;   '
      '           '
      'begin'
      '  geo_DecodeDegree_(aValue, iDeg,iMin,eSec);'
      ' '
      '  Result:=Format(STR_BLPOINT_FORMAT1, [iDeg, iMin, eSec]);'
      ''
      'end;'
      ''
      '       '
      ''
      '//------------------------------------------------------------'
      'function DegreeToStr_lat (aValue: double): String;'
      '//------------------------------------------------------------'
      'var '
      '  s: string;'
      'begin'
      '  if aValue>0 then s:='#39'N'#39' else s:='#39'S'#39';'
      ''
      '  aValue:=Abs(aValue);       '
      ''
      '  Result:=DegreeToStr (aValue) + s;'
      'end;'
      ''
      '//------------------------------------------------------------'
      'function DegreeToStr_lon (aValue: double): String;'
      '//------------------------------------------------------------'
      'var '
      '  s: string;'
      'begin'
      '   if aValue<0 then begin'
      '     s:='#39'W'#39';'
      '     aValue:=Abs(aValue);'
      '   end else'
      ''
      '   if aValue>180 then begin'
      '     s:='#39'W'#39';'
      '     aValue:=360 - aValue'
      '   end else'
      '     s:='#39'E'#39';     '
      ''
      '  Result:=DegreeToStr (aValue) + s;'
      'end;  '
      '               '
      ''
      ' //------------------------------------------------------------'
      
        'procedure geo_DecodeDegree_(aValue: Double; var aDeg,aMin:intege' +
        'r; var aSec:'
      '    Double);'
      '//------------------------------------------------------------'
      'var'
      '  bIsNegative: Boolean;'
      '  e: Double;'
      '  eSec: Double;'
      ''
      'begin'
      '  bIsNegative := aValue<0;'
      ''
      '  aValue :=Abs(aValue);'
      ''
      '  aDeg:=Trunc(aValue);'
      ''
      '  aMin:=Trunc(Frac(aValue)*60);'
      ''
      ''
      '//  e:= (aValue - Trunc(aValue));'
      '  e:= (aValue - Trunc(aValue)) * 60;'
      ''
      ''
      '  aMin:=Trunc(e);'
      '  aSec:=(((aValue - aDeg) *60) - aMin) * 60;'
      ''
      '    while aSec>(60-0.1) do'
      '  begin'
      '    aMin:=aMin+1;'
      '    aSec:=aSec-60;'
      '  end;    '
      ''
      '  while aMin>=60 do'
      '  begin'
      '      aMin:=aMin-60;'
      '      aDeg:=aDeg+1;'
      '  end;  '
      ''
      '  if aSec<0 then'
      '     aSec:=0;'
      '    '
      ''
      ''
      '  if bIsNegative then'
      '    aDeg:=-aDeg;'
      ''
      ''
      ''
      'end;'
      '  '
      '                               '
      ''
      '//------------------------------------------------------------'
      
        'procedure geo_DecodeDegree_11(aValue: Double; var aDeg,aMin:inte' +
        'ger; var aSec:'
      '    Double);'
      '//------------------------------------------------------------'
      'const'
      ' EXP_ = 0.000000001;'
      '     //97,033333333'
      'var'
      '  bIsNegative: Boolean;'
      '  e: Double;'
      '  eMinutes: Double;'
      '  eSec: Double;'
      '  eSeconds: Double;'
      'begin'
      '  bIsNegative := aValue<0;'
      ''
      '  aValue :=Abs(aValue);'
      ''
      '  aDeg:=Trunc(aValue);'
      ''
      ' // e:=Abs(aValue)*60;'
      ''
      '//  , Frac(X) = X - Int(X).'
      ''
      '  aMin:=Trunc(Frac(aValue)*60);'
      ''
      '//  Result.Sec:=Frac(Frac(Abs(aValue))*60)*60;'
      '  e:= (aValue - Trunc(aValue)) * 60;'
      '  aMin:=Trunc(e);'
      ''
      ' eMinutes := Trunc (e);'
      ' eSeconds := (e - Trunc (e)) * 60 ;'
      ''
      ''
      '  aSec:=(((aValue - aDeg) *60) - aMin) * 60;'
      ''
      '  if (aSec - 60) > EXP_ then'
      '  begin'
      '    Inc(aMin);'
      '    aSec := 0;'
      '  end;'
      ''
      ''
      '  if bIsNegative then'
      '    aDeg:=-aDeg;'
      '          '
      'end;'
      '        '
      ''
      '       '
      'begin'
      ''
      'end.    ')
    OnStartReport = 'frxReportOnStartReport'
    Left = 782
    Top = 20
    Datasets = <
      item
        DataSet = frxDBDataset_Antenna1
        DataSetName = 'Antenna1'
      end
      item
        DataSet = frxDBDataset_Antenna2
        DataSetName = 'Antenna2'
      end
      item
        DataSet = frxDBDataset_group
        DataSetName = 'group'
      end
      item
        DataSet = frxDBDataset_Items
        DataSetName = 'Items'
      end
      item
        DataSet = frxDBDataset_link
        DataSetName = 'Link'
      end
      item
        DataSet = frxDBDataset_Link_img
        DataSetName = 'Link_img'
      end
      item
        DataSet = frxDBDataset_LinkEnd1
        DataSetName = 'LinkEnd1'
      end
      item
        DataSet = frxDBDataset_LinkEnd2
        DataSetName = 'LinkEnd2'
      end
      item
        DataSet = frxDBDataset_Profile_items
        DataSetName = 'Profile'
      end
      item
        DataSet = frxDBDataset_repeater
        DataSetName = 'repeater'
      end
      item
        DataSet = frxDBDataset_repeater_ant
        DataSetName = 'repeater_ant'
      end
      item
        DataSet = frxDBDataset_Property1
        DataSetName = 'Site1'
      end
      item
        DataSet = frxDBDataset_Property2
        DataSetName = 'Site2'
      end
      item
        DataSet = frxDBDataset_repeater_property
        DataSetName = 'site3'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 9.260416666666670000
      BottomMargin = 10.583333333333300000
      Frame.Typ = []
      LargeDesignHeight = True
      OnBeforePrint = 'Page1OnBeforePrint'
      object MasterData3: TfrxMasterData
        FillType = ftBrush
        Frame.Typ = []
        Height = 713.638220000000000000
        Top = 100.000000000000000000
        Width = 718.110700000000000000
        Child = frxReport_repeater.Child_map_label
        DataSet = frxDBDataset_link
        DataSetName = 'Link'
        RowCount = 0
        object Memo7: TfrxMemoView
          Align = baWidth
          AllowVectorExport = True
          Width = 718.110700000000000000
          Height = 19.000000000000000000
          AutoWidth = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8 = (
            
              #1056#1115#1057#8218#1057#8225#1056#181#1057#8218' '#1056#1111#1056#1109' '#1057#1026#1056#176#1056#1169#1056#1105#1056#1109#1057#1026#1056#181#1056#187#1056#181#1056#8470#1056#1029#1056#1109#1056#1112#1057#1107' '#1056#1105#1056#1029#1057#8218#1056#181#1057#1026#1056#1030#1056#176#1056#187#1057#1107' ' +
              #1057#1027' '#1056#1111#1056#176#1057#1027#1057#1027#1056#1105#1056#1030#1056#1029#1057#8249#1056#1112' '#1057#1026#1056#181#1057#8218#1057#1026#1056#176#1056#1029#1057#1027#1056#187#1057#1039#1057#8218#1056#1109#1057#1026#1056#1109#1056#1112)
          ParentFont = False
        end
        object Linkname: TfrxMemoView
          Align = baWidth
          AllowVectorExport = True
          Top = 28.000000000000000000
          Width = 718.110700000000000000
          Height = 16.000000000000000000
          AutoWidth = True
          DataSet = frxDBDataset_link
          DataSetName = 'Link'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8 = (
            '[Site1."name"] - [Site3."name"] - [Site2."name"] ')
          ParentFont = False
        end
        object Memo_1: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 283.346630000000000000
          Width = 302.362204720000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#8217#1057#8249#1057#1027#1056#1109#1057#8218#1056#176', m')
          ParentFont = False
        end
        object Memo_2: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 298.346630000000000000
          Width = 302.362204720000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1106#1056#183#1056#1105#1056#1112#1057#1107#1057#8218' ')
          ParentFont = False
        end
        object Memo_3: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 313.346630000000000000
          Width = 302.362204720000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1118#1056#1105#1056#1111' '#1056#1109#1056#177#1056#1109#1057#1026#1057#1107#1056#1169#1056#1109#1056#1030#1056#176#1056#1029#1056#1105#1057#1039' ')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 328.346630000000000000
          Width = 302.362204720000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#167#1056#176#1057#1027#1057#8218#1056#1109#1057#8218#1056#176' '#1056#1111#1056#181#1057#1026#1056#181#1056#1169#1056#176#1057#8225#1056#1105', MHz')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 343.346630000000000000
          Width = 302.362204720000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1038#1056#1108#1056#1109#1057#1026#1056#1109#1057#1027#1057#8218#1057#1034' '#1056#1111#1056#181#1057#1026#1056#181#1056#1169#1056#176#1057#8225#1056#1105', Mbit/s')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 358.346630000000000000
          Width = 302.362204720000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1114#1056#1109#1057#8240#1056#1029#1056#1109#1057#1027#1057#8218#1057#1034', dBm')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 373.346630000000000000
          Width = 302.362204720000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#8221#1056#1105#1056#176#1056#1112#1056#181#1057#8218#1057#1026' '#1056#176#1056#1029#1057#8218#1056#181#1056#1029#1056#1029#1057#8249', m')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 419.244280000000000000
          Width = 302.362204720000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            
              #1056#1038#1057#1026#1056#181#1056#1169#1056#1029#1057#1039#1057#1039' '#1056#1112#1056#1109#1057#8240#1056#1029#1056#1109#1057#1027#1057#8218#1057#1034' '#1056#1029#1056#176' '#1056#1030#1057#8230#1056#1109#1056#1169#1056#181' '#1056#1111#1057#1026#1056#1105#1056#181#1056#1112#1056#1029#1056#1105#1056#1108 +
              #1056#176', dBm')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 388.346630000000000000
          Width = 302.362204720000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1119#1056#1109#1056#187#1057#1039#1057#1026#1056#1105#1056#183#1056#176#1057#8224#1056#1105#1057#1039' ')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 434.244280000000000000
          Width = 302.362204720000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1106#1056#1111#1056#1111#1056#176#1057#1026#1056#176#1057#8218#1056#1029#1056#1109#1056#181' '#1057#1026#1056#181#1056#183#1056#181#1057#1026#1056#1030#1056#1105#1057#1026#1056#1109#1056#1030#1056#176#1056#1029#1056#1105#1056#181' ')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 449.244280000000000000
          Width = 302.362204720000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1119#1056#1109#1057#1026#1056#1109#1056#1110' '#1056#1111#1057#1026#1056#1105' BER=10-3 ')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 479.244280000000000000
          Width = 302.362204720000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1114#1056#1105#1056#1029#1056#1105#1056#1112#1056#176#1056#187#1057#1034#1056#1029#1057#8249#1056#8470' '#1056#1109#1057#8218#1056#1029#1056#1109#1057#1027#1056#1105#1057#8218#1056#181#1056#187#1057#1034#1056#1029#1057#8249#1056#8470' '#1056#1111#1057#1026#1056#1109#1057#1027#1056#1030#1056#181#1057#8218)
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 464.244280000000000000
          Width = 302.362204720000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1119#1056#1109#1057#1026#1056#1109#1056#1110' '#1056#1111#1057#1026#1056#1105' BER=10-6 ')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 494.244280000000000000
          Width = 302.362204720000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1119#1056#1109#1056#187#1056#1109#1056#182#1056#181#1056#1029#1056#1105#1056#181' '#1057#8218#1056#1109#1057#8225#1056#1108#1056#1105' '#1056#1111#1057#1026#1056#1109#1057#1027#1056#1030#1056#181#1057#8218#1056#176', km ')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 509.244280000000000000
          Width = 302.362204720000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            'SESR '#1057#1027#1057#1107#1056#177#1057#1026#1056#181#1057#8222#1056#1108#1057#8224#1056#1105#1056#1109#1056#1029#1056#1029#1056#176#1057#1039' '#1057#1027#1056#1109#1057#1027#1057#8218#1056#176#1056#1030#1056#187#1057#1039#1057#1035#1057#8240#1056#176#1057#1039', % ')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 524.244280000000000000
          Width = 302.362204720000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            'SESR '#1056#1169#1056#1109#1056#182#1056#1169#1056#181#1056#1030#1056#176#1057#1039' '#1057#1027#1056#1109#1057#1027#1057#8218#1056#176#1056#1030#1056#187#1057#1039#1057#1035#1057#8240#1056#176#1057#1039', % ')
          ParentFont = False
        end
        object Memo25: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 539.244280000000000000
          Width = 302.362204720000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            
              'SESR '#1056#1105#1056#1029#1057#8218#1056#181#1057#1026#1057#8222#1056#181#1057#1026#1056#181#1056#1029#1057#8224#1056#1105#1056#1109#1056#1029#1056#1029#1056#176#1057#1039' '#1057#1027#1056#1109#1057#1027#1057#8218#1056#176#1056#1030#1056#187#1057#1039#1057#1035#1057#8240#1056#176#1057#1039 +
              ', % ')
          ParentFont = False
        end
        object Memo26: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 554.244280000000000000
          Width = 302.362204720000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            
              #1056#152#1057#8218#1056#1109#1056#1110#1056#1109#1056#1030#1057#8249#1056#8470' '#1056#1111#1056#1109#1056#1108#1056#176#1056#183#1056#176#1057#8218#1056#181#1056#187#1057#1034' '#1056#1108#1056#176#1057#8225#1056#181#1057#1027#1057#8218#1056#1030#1056#176' '#1056#1111#1056#1109' '#1056#1109#1057#8364 +
              #1056#1105#1056#177#1056#1108#1056#176#1056#1112' SESR, % ')
          ParentFont = False
        end
        object Memo27: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 569.244280000000000000
          Width = 302.362204720000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            
              #1056#1116#1056#1109#1057#1026#1056#1112#1056#1105#1057#1026#1057#1107#1056#181#1056#1112#1056#1109#1056#181' '#1056#183#1056#1029#1056#176#1057#8225#1056#181#1056#1029#1056#1105#1056#181' SESR '#1056#1029#1056#176' '#1056#1105#1056#1029#1057#8218#1056#181#1057#1026#1056#1030#1056#176 +
              #1056#187#1056#181', % ')
          ParentFont = False
        end
        object Memo28: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 584.244280000000000000
          Width = 302.362204720000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            
              #1056#152#1057#8218#1056#1109#1056#1110#1056#1109#1056#1030#1057#8249#1056#8470' '#1056#1111#1056#1109#1056#1108#1056#176#1056#183#1056#176#1057#8218#1056#181#1056#187#1057#1034' '#1056#1029#1056#181#1056#1110#1056#1109#1057#8218#1056#1109#1056#1030#1056#1029#1056#1109#1057#1027#1057#8218#1056#1105' '#1056 +
              #1113#1056#1029#1056#1110', %')
          ParentFont = False
        end
        object Memo29: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 599.244280000000000000
          Width = 302.362204720000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            
              #1056#1116#1056#1109#1057#1026#1056#1112#1056#1105#1057#1026#1057#1107#1056#181#1056#1112#1056#1109#1056#181' '#1056#183#1056#1029#1056#176#1057#8225#1056#181#1056#1029#1056#1105#1056#181' '#1056#1113#1056#1029#1056#1110' '#1056#1029#1056#176' '#1056#1105#1056#1029#1057#8218#1056#181#1057#1026#1056#1030 +
              #1056#176#1056#187#1056#181', % ')
          ParentFont = False
        end
        object Memo31: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 659.244280000000000000
          Width = 302.362204720000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            'ESR '#1057#8218#1057#1026#1056#181#1056#177', % ')
          ParentFont = False
        end
        object Memo32: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 614.244280000000000000
          Width = 302.362204720000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            'BBER '#1056#1029#1056#1109#1057#1026#1056#1112', %')
          ParentFont = False
        end
        object Memo33: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 644.244280000000000000
          Width = 302.362204720000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            'BBER '#1057#8218#1057#1026#1056#181#1056#177', % ')
          ParentFont = False
        end
        object Memo34: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 629.244280000000000000
          Width = 302.362204720000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            'ESR '#1056#1029#1056#1109#1057#1026#1056#1112', % ')
          ParentFont = False
        end
        object Memo35: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 674.244280000000000000
          Width = 302.362204720000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1119#1057#1026#1056#1105#1056#1110#1056#1109#1056#1169#1056#1029#1056#1109#1057#1027#1057#8218#1057#1034' ')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 302.362204720000000000
          Top = 268.346630000000000000
          Width = 132.283464570000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Site1."name"]')
          ParentFont = False
        end
        object Memo36: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 434.645669290000000000
          Top = 268.346630000000000000
          Width = 151.181151180000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[site3."name"]')
          ParentFont = False
        end
        object Memo37: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 302.362204720000000000
          Top = 298.346630000000000000
          Width = 132.283464570000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."azimuth1"]')
          ParentFont = False
        end
        object Memo38: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 434.645669290000000000
          Top = 298.346630000000000000
          Width = 75.590551180000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[repeater."azimuth1"]')
          ParentFont = False
        end
        object LinkEnd2threshold_BER_3: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 302.362204720000000000
          Top = 449.244280000000000000
          Width = 132.283464570000000000
          Height = 15.000000000000000000
          DataSet = frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd1."threshold_BER_3"]')
          ParentFont = False
        end
        object LinkEnd2threshold_BER_6: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 302.362204720000000000
          Top = 464.244280000000000000
          Width = 132.283464570000000000
          Height = 15.000000000000000000
          DataSet = frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd1."threshold_BER_6"]')
          ParentFont = False
        end
        object Memo41: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 302.362204720000000000
          Top = 373.346630000000000000
          Width = 132.283464570000000000
          Height = 15.000000000000000000
          DataSet = frxDBDataset_link
          DataSetName = 'Link'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[antenna1."diameter"]')
          ParentFont = False
        end
        object Memo44: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 302.362204720000000000
          Top = 388.346630000000000000
          Width = 132.283464570000000000
          Height = 15.000000000000000000
          DataSet = frxDBDataset_link
          DataSetName = 'Link'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[antenna1."polarization"]')
          ParentFont = False
        end
        object Memo50: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 302.362204720000000000
          Top = 343.346630000000000000
          Width = 132.283464570000000000
          Height = 15.000000000000000000
          DataSet = frxDBDataset_link
          DataSetName = 'Link'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd1."bitrate_Mbps"]')
          ParentFont = False
        end
        object Memo52: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 302.362204720000000000
          Top = 358.346630000000000000
          Width = 132.283464570000000000
          Height = 15.000000000000000000
          DataSet = frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd1."power_dBm"]')
          ParentFont = False
        end
        object Memo55: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 302.362204720000000000
          Top = 313.346630000000000000
          Width = 132.283464570000000000
          Height = 15.000000000000000000
          DataSet = frxDBDataset_link
          DataSetName = 'Link'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd1."LinkEndType_name"]')
          ParentFont = False
        end
        object Memo56: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 302.362204720000000000
          Top = 328.346630000000000000
          Width = 132.283464570000000000
          Height = 15.000000000000000000
          DataSet = frxDBDataset_link
          DataSetName = 'Link'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd1."tx_freq_MHz"]')
          ParentFont = False
        end
        object Memo57: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 434.645669290000000000
          Top = 313.346630000000000000
          Width = 75.590551180000000000
          Height = 15.000000000000000000
          DataSet = frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          ParentFont = False
        end
        object Memo39: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 434.645669290000000000
          Top = 328.346630000000000000
          Width = 75.590551180000000000
          Height = 15.000000000000000000
          DataSet = frxDBDataset_link
          DataSetName = 'Link'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          ParentFont = False
        end
        object Memo40: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 434.645669290000000000
          Top = 343.346630000000000000
          Width = 75.590551180000000000
          Height = 15.000000000000000000
          DataSet = frxDBDataset_link
          DataSetName = 'Link'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          ParentFont = False
        end
        object Memo46: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 434.645669290000000000
          Top = 358.346630000000000000
          Width = 75.590551180000000000
          Height = 15.000000000000000000
          DataSet = frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          ParentFont = False
        end
        object Memo48: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 434.645669290000000000
          Top = 449.244280000000000000
          Width = 75.590551180000000000
          Height = 15.000000000000000000
          DataSet = frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          ParentFont = False
        end
        object Memo53: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 434.645669290000000000
          Top = 464.244280000000000000
          Width = 75.590551180000000000
          Height = 15.000000000000000000
          DataSet = frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          ParentFont = False
        end
        object Memo54: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 302.362204720000000000
          Top = 659.244280000000000000
          Width = 396.850393700000000000
          Height = 15.000000000000000000
          DataSet = frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Link."ESR_REQUIRED"]')
          ParentFont = False
        end
        object Memo58: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 302.362204720000000000
          Top = 674.244280000000000000
          Width = 396.850393700000000000
          Height = 15.000000000000000000
          DataSet = frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Link."status_str"]')
          ParentFont = False
        end
        object Memo59: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 302.362204720000000000
          Top = 629.244280000000000000
          Width = 396.850393700000000000
          Height = 15.000000000000000000
          DataSet = frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Link."ESR_NORM"]')
          ParentFont = False
        end
        object Memo60: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 302.362204720000000000
          Top = 644.244280000000000000
          Width = 396.850393700000000000
          Height = 15.000000000000000000
          DataSet = frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Link."BBER_REQUIRED"]')
          ParentFont = False
        end
        object Memo61: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 302.362204720000000000
          Top = 599.244280000000000000
          Width = 396.850393700000000000
          Height = 15.000000000000000000
          DataSet = frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Link."KNG_NORM"]')
          ParentFont = False
        end
        object Memo62: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 302.362204720000000000
          Top = 614.244280000000000000
          Width = 396.850393700000000000
          Height = 15.000000000000000000
          DataSet = frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Link."BBER_NORM"]')
          ParentFont = False
        end
        object Memo63: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 302.362204720000000000
          Top = 569.244280000000000000
          Width = 396.850393700000000000
          Height = 15.000000000000000000
          DataSet = frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Link."SESR_NORM"]')
          ParentFont = False
        end
        object Memo64: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 302.362204720000000000
          Top = 584.244280000000000000
          Width = 396.850393700000000000
          Height = 15.000000000000000000
          DataSet = frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Link."KNG"]')
          ParentFont = False
        end
        object Memo67: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 302.362204720000000000
          Top = 539.244280000000000000
          Width = 396.850393700000000000
          Height = 15.000000000000000000
          DataSet = frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Link."SESR_INTERFERENCE"]')
          ParentFont = False
        end
        object Memo69: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 302.362204720000000000
          Top = 554.244280000000000000
          Width = 396.850393700000000000
          Height = 15.000000000000000000
          DataSet = frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Link."SESR"]')
          ParentFont = False
        end
        object Memo71: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 302.362204720000000000
          Top = 509.244280000000000000
          Width = 396.850393700000000000
          Height = 15.000000000000000000
          DataSet = frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Link."SESR_SUBREFRACTION"]')
          ParentFont = False
        end
        object Memo73: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 302.362204720000000000
          Top = 524.244280000000000000
          Width = 396.850393700000000000
          Height = 15.000000000000000000
          DataSet = frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Link."SESR_RAIN"]')
          ParentFont = False
        end
        object Memo75: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 302.362204720000000000
          Top = 494.244280000000000000
          Width = 396.850393700000000000
          Height = 15.000000000000000000
          DataSet = frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Link."MARGIN_DISTANCE"]')
          ParentFont = False
        end
        object Memo76: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 302.362204720000000000
          Top = 479.244280000000000000
          Width = 396.850393700000000000
          Height = 15.000000000000000000
          DataSet = frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Link."MARGIN_HEIGHT"]')
          ParentFont = False
        end
        object Memo88: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 434.645669290000000000
          Top = 373.346630000000000000
          Width = 75.590551180000000000
          Height = 15.000000000000000000
          DataSet = frxDBDataset_link
          DataSetName = 'Link'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[repeater."diameter1"]')
          ParentFont = False
        end
        object Memo84: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 434.645669290000000000
          Top = 388.346630000000000000
          Width = 75.590551180000000000
          Height = 15.000000000000000000
          DataSet = frxDBDataset_link
          DataSetName = 'Link'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[repeater."polarization1"]')
          ParentFont = False
        end
        object Memo85: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 302.362204720000000000
          Top = 419.244280000000000000
          Width = 132.283464570000000000
          Height = 15.000000000000000000
          DataSet = frxDBDataset_link
          DataSetName = 'Link'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd1."rx_level_dBm"]')
          ParentFont = False
        end
        object Memo86: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 434.645669290000000000
          Top = 419.244280000000000000
          Width = 75.590551180000000000
          Height = 15.000000000000000000
          DataSet = frxDBDataset_link
          DataSetName = 'Link'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          ParentFont = False
        end
        object Memo87: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 302.362204720000000000
          Top = 434.244280000000000000
          Width = 132.283464570000000000
          Height = 15.000000000000000000
          DataSet = frxDBDataset_link
          DataSetName = 'Link'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd1."kratnostBySpace_name"]')
          ParentFont = False
        end
        object Memo94: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 434.645669290000000000
          Top = 434.244280000000000000
          Width = 75.590551180000000000
          Height = 15.000000000000000000
          DataSet = frxDBDataset_link
          DataSetName = 'Link'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          ParentFont = False
        end
        object Memo131: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 268.346630000000000000
          Width = 302.362204720000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          ParentFont = False
        end
        object Memo65: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 302.362204720000000000
          Top = 283.346630000000000000
          Width = 132.283464570000000000
          Height = 15.000000000000000000
          DataSet = frxDBDataset_LinkEnd1
          DataSetName = 'LinkEnd1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[antenna1."height"]')
          ParentFont = False
        end
        object Memo102: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 434.645669290000000000
          Top = 283.346630000000000000
          Width = 75.590551180000000000
          Height = 15.000000000000000000
          DataSet = frxDBDataset_LinkEnd1
          DataSetName = 'LinkEnd1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[repeater."height1"]')
          ParentFont = False
        end
        object Memo43: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 64.252010000000000000
          Width = 110.000000000000000000
          Height = 30.236220472440900000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1119#1056#187#1056#1109#1057#8240#1056#176#1056#1169#1056#1108#1056#176)
          ParentFont = False
        end
        object Memo_left_1: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 110.000000000000000000
          Top = 64.252010000000000000
          Width = 117.165354330000000000
          Height = 30.236220470000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Site1."name"]')
          ParentFont = False
          UseDefaultCharset = True
        end
        object Memo68: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 140.267780000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1113#1056#1109#1056#1109#1057#1026#1056#1169#1056#1105#1056#1029#1056#176#1057#8218#1057#8249' WGS')
          ParentFont = False
        end
        object Memo70: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 170.267780000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#8217#1057#8249#1057#1027#1056#1109#1057#8218#1056#176' '#1056#1111#1056#1109#1056#1169#1056#1030#1056#181#1057#1027#1056#176', m')
          ParentFont = False
        end
        object Memo72: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 185.267780000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1106#1056#183#1056#1105#1056#1112#1057#1107#1057#8218)
          ParentFont = False
        end
        object Memo74: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 200.267780000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1032#1056#1110#1056#1109#1056#187' '#1056#1112#1056#181#1057#1027#1057#8218#1056#176)
          ParentFont = False
        end
        object Memo100: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 215.267780000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#8221#1056#1105#1056#176#1056#1112#1056#181#1057#8218#1057#1026' '#1056#176#1056#1029#1057#8218#1056#181#1056#1029#1056#1029#1057#8249)
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 110.000000000000000000
          Top = 140.267780000000000000
          Width = 117.165354330000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[DegreeToStr (<Site1."lat_WGS">)]')
          ParentFont = False
          UseDefaultCharset = True
        end
        object Memo1: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 110.000000000000000000
          Top = 170.267780000000000000
          Width = 117.165354330000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[antenna1."height"]')
          ParentFont = False
          UseDefaultCharset = True
        end
        object Memo95: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 110.000000000000000000
          Top = 185.267780000000000000
          Width = 117.165354330000000000
          Height = 15.000000000000000000
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."azimuth1"]')
          ParentFont = False
          UseDefaultCharset = True
        end
        object Memo97: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 110.000000000000000000
          Top = 215.267780000000000000
          Width = 117.165354330000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[antenna1."diameter"]')
          ParentFont = False
          UseDefaultCharset = True
        end
        object Memo98: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 110.000000000000000000
          Top = 155.267780000000000000
          Width = 117.165354330000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[DegreeToStr (<Site1."lon_WGS">)]')
          ParentFont = False
          UseDefaultCharset = True
        end
        object Memo96: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 110.000000000000000000
          Top = 200.267780000000000000
          Width = 117.165354330000000000
          Height = 15.000000000000000000
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."ELEVATION_angle_1"]')
          ParentFont = False
          UseDefaultCharset = True
        end
        object Memo121: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 155.267780000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 110.267780000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1113#1056#1109#1056#1109#1057#1026#1056#1169#1056#1105#1056#1029#1056#176#1057#8218#1057#8249' CK-42')
          ParentFont = False
        end
        object Memo30: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 110.000000000000000000
          Top = 110.267780000000000000
          Width = 117.165354330000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[DegreeToStr (<Site1."lat">)]')
          ParentFont = False
          UseDefaultCharset = True
        end
        object Memo66: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 110.000000000000000000
          Top = 125.267780000000000000
          Width = 117.165354330000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[DegreeToStr (<Site1."lon">)]')
          ParentFont = False
          UseDefaultCharset = True
        end
        object Memo99: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 125.267780000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          ParentFont = False
        end
        object Memo136: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 230.267780000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1032#1057#1027#1056#1105#1056#187#1056#181#1056#1029#1056#1105#1056#181' '#1056#176#1056#1029#1057#8218#1056#181#1056#1029#1056#1029#1057#8249)
          ParentFont = False
        end
        object Memo139: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 110.000000000000000000
          Top = 230.267780000000000000
          Width = 117.165354330000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[antenna1."gain"]')
          ParentFont = False
          UseDefaultCharset = True
        end
        object Memo45: TfrxMemoView
          AllowVectorExport = True
          Left = 242.283474330000000000
          Top = 79.252010000000000000
          Width = 160.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#8221#1056#187#1056#1105#1056#1029#1056#176' '#1056#1105#1056#1029#1057#8218#1056#181#1057#1026#1056#1030#1056#176#1056#187#1056#176', km')
          ParentFont = False
        end
        object Memo47: TfrxMemoView
          AllowVectorExport = True
          Left = 242.283474330000000000
          Top = 94.252010000000000000
          Width = 160.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1032#1057#1026#1056#1109#1056#1030#1056#181#1056#1029#1057#1034' '#1057#1027#1056#1105#1056#1110#1056#1029#1056#176#1056#187#1056#176', dBm')
          ParentFont = False
        end
        object Memo49: TfrxMemoView
          AllowVectorExport = True
          Left = 242.283474330000000000
          Top = 109.252010000000000000
          Width = 160.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            'P'#1056#176#1056#177#1056#1109#1057#8225#1056#176#1057#1039' '#1057#8225#1056#176#1057#1027#1057#8218#1056#1109#1057#8218#1056#176', GHz')
          ParentFont = False
        end
        object Memo51: TfrxMemoView
          AllowVectorExport = True
          Left = 242.283474330000000000
          Top = 124.252010000000000000
          Width = 160.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1114#1056#1109#1057#8240#1056#1029#1056#1109#1057#1027#1057#8218#1057#1034' '#1056#1111#1056#181#1057#1026#1056#181#1056#1169#1056#176#1057#8218#1057#8225#1056#1105#1056#1108#1056#176', dBm')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Left = 242.283474330000000000
          Top = 139.252010000000000000
          Width = 160.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1038#1056#1108#1056#1109#1057#1026#1056#1109#1057#1027#1057#8218#1057#1034' '#1056#1111#1056#181#1057#1026#1056#181#1056#1169#1056#176#1057#8225#1056#1105', Mbit/s')
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 242.283474330000000000
          Top = 154.252010000000000000
          Width = 160.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1118#1056#1105#1056#1111' '#1056#1109#1056#177#1056#1109#1057#1026#1057#1107#1056#1169#1056#1109#1056#1030#1056#176#1056#1029#1056#1105#1057#1039)
          ParentFont = False
        end
        object Memo123: TfrxMemoView
          AllowVectorExport = True
          Left = 402.283474330000000000
          Top = 94.252010000000000000
          Width = 68.031496060000000000
          Height = 15.000000000000000000
          DataField = 'rx_level_dBm'
          DataSet = frxDBDataset_link
          DataSetName = 'Link'
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."rx_level_dBm"]')
          ParentFont = False
        end
        object Memo124: TfrxMemoView
          AllowVectorExport = True
          Left = 402.283474330000000000
          Top = 139.252010000000000000
          Width = 68.031496060000000000
          Height = 15.000000000000000000
          DataField = 'bitrate_Mbps'
          DataSet = frxDBDataset_LinkEnd1
          DataSetName = 'LinkEnd1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd1."bitrate_Mbps"]')
          ParentFont = False
        end
        object Memo125: TfrxMemoView
          AllowVectorExport = True
          Left = 402.283474330000000000
          Top = 154.252010000000000000
          Width = 68.031496060000000000
          Height = 15.000000000000000000
          DataField = 'LinkEndType_name'
          DataSet = frxDBDataset_LinkEnd1
          DataSetName = 'LinkEnd1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd1."LinkEndType_name"]')
          ParentFont = False
        end
        object Memo127: TfrxMemoView
          AllowVectorExport = True
          Left = 402.283474330000000000
          Top = 109.252010000000000000
          Width = 68.031496060000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[FloatToStr( <LinkEnd1."tx_freq_MHz"> / 1000 )]')
          ParentFont = False
        end
        object Memo128: TfrxMemoView
          AllowVectorExport = True
          Left = 402.283474330000000000
          Top = 79.252010000000000000
          Width = 68.031496060000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."length_km"]')
          ParentFont = False
        end
        object Memo126: TfrxMemoView
          AllowVectorExport = True
          Left = 402.283474330000000000
          Top = 124.252010000000000000
          Width = 68.031496060000000000
          Height = 15.000000000000000000
          DataField = 'power_dBm'
          DataSet = frxDBDataset_LinkEnd1
          DataSetName = 'LinkEnd1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd1."power_dBm"]')
          ParentFont = False
        end
        object Memo42: TfrxMemoView
          AllowVectorExport = True
          Left = 242.283474330000000000
          Top = 64.252010000000000000
          Width = 160.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1119#1057#1026#1056#1105#1056#183#1056#1029#1056#176#1056#1108' '#1056#1111#1057#1026#1056#1105#1056#1110#1056#1109#1056#1169#1056#1029#1056#1109#1057#1027#1057#8218#1056#1105)
          ParentFont = False
        end
        object Memo122: TfrxMemoView
          AllowVectorExport = True
          Left = 402.283474330000000000
          Top = 64.252010000000000000
          Width = 68.031496060000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."status_str"]')
          ParentFont = False
        end
        object Memo134: TfrxMemoView
          AllowVectorExport = True
          Left = 242.283474330000000000
          Top = 169.252010000000000000
          Width = 160.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#8212#1056#176#1056#1111#1056#176#1057#1027' '#1056#1029#1056#176' '#1056#183#1056#176#1056#1112#1056#1105#1057#1026#1056#176#1056#1029#1056#1105#1057#1039', dB')
          ParentFont = False
        end
        object Memo135: TfrxMemoView
          AllowVectorExport = True
          Left = 402.283474330000000000
          Top = 169.252010000000000000
          Width = 68.031496060000000000
          Height = 15.000000000000000000
          DataSet = frxDBDataset_LinkEnd1
          DataSetName = 'LinkEnd1'
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."fade_margin_dB"]')
          ParentFont = False
        end
        object Memo105: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 494.724873230000000000
          Top = 64.252010000000000000
          Width = 110.000000000000000000
          Height = 30.236218030000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1119#1056#187#1056#1109#1057#8240#1056#176#1056#1169#1056#1108#1056#176)
          ParentFont = False
        end
        object Memo106: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 604.724873230000000000
          Top = 64.252010000000000000
          Width = 113.385826770000000000
          Height = 30.236220470000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Site2."name"]')
          ParentFont = False
        end
        object Memo108: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 494.724873230000000000
          Top = 110.385878030000000000
          Width = 110.000000000000000000
          Height = 15.118110240000000000
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1113#1056#1109#1056#1109#1057#1026#1056#1169#1056#1105#1056#1029#1056#176#1057#8218#1057#8249'  CK-42')
          ParentFont = False
        end
        object Memo109: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 494.724873230000000000
          Top = 170.858321420000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#8217#1057#8249#1057#1027#1056#1109#1057#8218#1056#176' '#1056#1111#1056#1109#1056#1169#1056#1030#1056#181#1057#1027#1056#176', m')
          ParentFont = False
        end
        object Memo110: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 494.724873230000000000
          Top = 186.047310000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1106#1056#183#1056#1105#1056#1112#1057#1107#1057#8218)
          ParentFont = False
        end
        object Memo111: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 494.724873230000000000
          Top = 201.047310000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1032#1056#1110#1056#1109#1056#187' '#1056#1112#1056#181#1057#1027#1057#8218#1056#176)
          ParentFont = False
        end
        object Memo112: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 494.724873230000000000
          Top = 216.047310000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#8221#1056#1105#1056#176#1056#1112#1056#181#1057#8218#1057#1026' '#1056#176#1056#1029#1057#8218#1056#181#1056#1029#1056#1029#1057#8249)
          ParentFont = False
        end
        object Memo114: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 604.724873230000000000
          Top = 110.385878030000000000
          Width = 113.385826770000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[DegreeToStr (<Site2."lat">)]')
          ParentFont = False
        end
        object Memo115: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 604.724873230000000000
          Top = 170.858321420000000000
          Width = 113.385826770000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[antenna2."height"]')
          ParentFont = False
        end
        object Memo116: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 604.724873230000000000
          Top = 186.047310000000000000
          Width = 113.385826770000000000
          Height = 15.000000000000000000
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."azimuth2"]')
          ParentFont = False
        end
        object Memo117: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 604.724873230000000000
          Top = 216.047310000000000000
          Width = 113.385826770000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Antenna2."diameter"]')
          ParentFont = False
        end
        object Memo118: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 604.724873230000000000
          Top = 125.503990710000000000
          Width = 113.385826770000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[DegreeToStr (<Site2."lon">)]')
          ParentFont = False
        end
        object Memo130: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 494.724873230000000000
          Top = 125.503990710000000000
          Width = 110.000000000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 604.724873230000000000
          Top = 201.047310000000000000
          Width = 113.385826770000000000
          Height = 15.000000000000000000
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."ELEVATION_angle_2"]')
          ParentFont = False
        end
        object Memo107: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 494.724873230000000000
          Top = 140.622100940000000000
          Width = 110.000000000000000000
          Height = 15.118110240000000000
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1113#1056#1109#1056#1109#1057#1026#1056#1169#1056#1105#1056#1029#1056#176#1057#8218#1057#8249' WGS')
          ParentFont = False
        end
        object Memo113: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 604.724873230000000000
          Top = 140.622100940000000000
          Width = 113.385826770000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[DegreeToStr (<Site2."lat_wgs">)]')
          ParentFont = False
        end
        object Memo119: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 604.724873230000000000
          Top = 155.740211180000000000
          Width = 113.385826770000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[DegreeToStr (<Site2."lon_wgs">)]')
          ParentFont = False
        end
        object Memo129: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 494.724873230000000000
          Top = 155.740211180000000000
          Width = 110.000000000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          ParentFont = False
        end
        object Memo140: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 494.724873230000000000
          Top = 231.330762360000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1032#1057#1027#1056#1105#1056#187#1056#181#1056#1029#1056#1105#1056#181' '#1056#176#1056#1029#1057#8218#1056#181#1056#1029#1056#1029#1057#8249)
          ParentFont = False
        end
        object Memo141: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 604.724873230000000000
          Top = 231.330762360000000000
          Width = 113.385826770000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Antenna2."gain"]')
          ParentFont = False
        end
        object Memo161: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 510.236220470000000000
          Top = 298.346630000000000000
          Width = 75.590551180000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[repeater."azimuth2"]')
          ParentFont = False
        end
        object Memo162: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 510.236220470000000000
          Top = 313.346630000000000000
          Width = 75.590551180000000000
          Height = 15.000000000000000000
          DataSet = frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          ParentFont = False
        end
        object Memo163: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 510.236220470000000000
          Top = 328.346630000000000000
          Width = 75.590551180000000000
          Height = 15.000000000000000000
          DataSet = frxDBDataset_link
          DataSetName = 'Link'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          ParentFont = False
        end
        object Memo164: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 510.236220470000000000
          Top = 343.346630000000000000
          Width = 75.590551180000000000
          Height = 15.000000000000000000
          DataSet = frxDBDataset_link
          DataSetName = 'Link'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          ParentFont = False
        end
        object Memo165: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 510.236220470000000000
          Top = 358.346630000000000000
          Width = 75.590551180000000000
          Height = 15.000000000000000000
          DataSet = frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          ParentFont = False
        end
        object Memo166: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 510.236220470000000000
          Top = 449.244280000000000000
          Width = 75.590551180000000000
          Height = 15.000000000000000000
          DataSet = frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          ParentFont = False
        end
        object Memo167: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 510.236220470000000000
          Top = 464.244280000000000000
          Width = 75.590551180000000000
          Height = 15.000000000000000000
          DataSet = frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          ParentFont = False
        end
        object Memo168: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 510.236220470000000000
          Top = 373.346630000000000000
          Width = 75.590551180000000000
          Height = 15.000000000000000000
          DataSet = frxDBDataset_link
          DataSetName = 'Link'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[repeater."diameter2"]')
          ParentFont = False
        end
        object Memo169: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 510.236220470000000000
          Top = 388.346630000000000000
          Width = 75.590551180000000000
          Height = 15.000000000000000000
          DataSet = frxDBDataset_link
          DataSetName = 'Link'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[repeater."polarization2"]')
          ParentFont = False
        end
        object Memo170: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 510.236220470000000000
          Top = 419.244280000000000000
          Width = 75.590551180000000000
          Height = 15.000000000000000000
          DataSet = frxDBDataset_link
          DataSetName = 'Link'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          ParentFont = False
        end
        object Memo171: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 510.236220470000000000
          Top = 434.244280000000000000
          Width = 75.590551180000000000
          Height = 15.000000000000000000
          DataSet = frxDBDataset_link
          DataSetName = 'Link'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          ParentFont = False
        end
        object Memo172: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 510.236220470000000000
          Top = 283.346630000000000000
          Width = 75.590551180000000000
          Height = 15.000000000000000000
          DataSet = frxDBDataset_LinkEnd1
          DataSetName = 'LinkEnd1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[repeater."height2"]')
          ParentFont = False
        end
        object Memo173: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 585.826820470000000000
          Top = 268.346630000000000000
          Width = 113.385826770000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Site2."name"]')
          ParentFont = False
        end
        object Memo174: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 585.826771650000000000
          Top = 298.346630000000000000
          Width = 113.385826770000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."azimuth2"]')
          ParentFont = False
        end
        object Memo175: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 585.826771650000000000
          Top = 313.346630000000000000
          Width = 113.385826770000000000
          Height = 15.000000000000000000
          DataSet = frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd2."LinkEndType_name"]')
          ParentFont = False
        end
        object Memo176: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 585.826771650000000000
          Top = 328.346630000000000000
          Width = 113.385826770000000000
          Height = 15.000000000000000000
          DataSet = frxDBDataset_link
          DataSetName = 'Link'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd2."tx_freq_MHz"]')
          ParentFont = False
        end
        object Memo177: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 585.826771650000000000
          Top = 343.346630000000000000
          Width = 113.385826770000000000
          Height = 15.000000000000000000
          DataSet = frxDBDataset_link
          DataSetName = 'Link'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd2."bitrate_Mbps"]')
          ParentFont = False
        end
        object Memo178: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 585.826771650000000000
          Top = 358.346630000000000000
          Width = 113.385826770000000000
          Height = 15.000000000000000000
          DataSet = frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd2."power_dBm"]')
          ParentFont = False
        end
        object Memo179: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 585.826771650000000000
          Top = 449.244280000000000000
          Width = 113.385826770000000000
          Height = 15.000000000000000000
          DataSet = frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd2."threshold_BER_3"]')
          ParentFont = False
        end
        object Memo180: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 585.826771650000000000
          Top = 464.244280000000000000
          Width = 113.385826770000000000
          Height = 15.000000000000000000
          DataSet = frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd2."threshold_BER_6"]')
          ParentFont = False
        end
        object Memo181: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 585.826771650000000000
          Top = 373.346630000000000000
          Width = 113.385826770000000000
          Height = 15.000000000000000000
          DataSet = frxDBDataset_link
          DataSetName = 'Link'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[antenna2."diameter"]')
          ParentFont = False
        end
        object Memo182: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 585.826771650000000000
          Top = 388.346630000000000000
          Width = 113.385826770000000000
          Height = 15.000000000000000000
          DataSet = frxDBDataset_link
          DataSetName = 'Link'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[antenna2."polarization"]')
          ParentFont = False
        end
        object Memo183: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 585.826771650000000000
          Top = 419.244280000000000000
          Width = 113.385826770000000000
          Height = 15.000000000000000000
          DataSet = frxDBDataset_link
          DataSetName = 'Link'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd2."rx_level_dBm"]')
          ParentFont = False
        end
        object Memo184: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 585.826771650000000000
          Top = 434.244280000000000000
          Width = 113.385826770000000000
          Height = 15.000000000000000000
          DataSet = frxDBDataset_link
          DataSetName = 'Link'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd2."kratnostBySpace_name"]')
          ParentFont = False
        end
        object Memo185: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 585.826771650000000000
          Top = 283.346630000000000000
          Width = 113.385826770000000000
          Height = 15.000000000000000000
          DataSet = frxDBDataset_LinkEnd1
          DataSetName = 'LinkEnd1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[antenna2."height"]')
          ParentFont = False
        end
        object Memo152: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 403.653553070000000000
          Width = 302.440940000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            'LINK  ID')
          ParentFont = False
        end
        object Memo186: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 302.440940000000000000
          Top = 403.653553070000000000
          Width = 132.283464570000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Linkend1."LINKID"]')
          ParentFont = False
        end
        object Memo187: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 434.724404570000000000
          Top = 403.653553070000000000
          Width = 75.590551180000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          ParentFont = False
        end
        object Memo188: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 510.314955750000000000
          Top = 403.653553070000000000
          Width = 75.590551180000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          ParentFont = False
        end
        object Memo189: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 585.905506930000000000
          Top = 403.653553070000000000
          Width = 113.385826770000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Linkend2."LINKID"]')
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        Frame.Typ = []
        Height = 24.000000000000000000
        Top = 1520.000000000000000000
        Width = 718.110700000000000000
        object Page: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 638.110700000000000000
          Width = 80.000000000000000000
          Height = 16.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8 = (
            '[Page#]')
          ParentFont = False
        end
        object Memo138: TfrxMemoView
          Align = baWidth
          AllowVectorExport = True
          Width = 638.110700000000000000
          Height = 16.000000000000000000
          AutoWidth = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clSilver
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8 = (
            
              #1056#1115#1057#8218#1057#8225#1056#181#1057#8218' '#1056#1030#1057#8249#1056#1111#1056#1109#1056#187#1056#1029#1056#181#1056#1029' '#1056#1030' '#1056#1111#1057#1026#1056#1109#1056#1110#1057#1026#1056#176#1056#1112#1056#1112#1056#181' ONEPLAN RPLS-D' +
              'B Link')
          ParentFont = False
        end
      end
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        Frame.Typ = []
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        Height = 24.220470000000000000
        ParentFont = False
        Top = 16.000000000000000000
        Width = 718.110700000000000000
        object Linkcalc_method_name1: TfrxMemoView
          Align = baWidth
          AllowVectorExport = True
          Left = 141.600000000000000000
          Width = 576.510700000000000000
          Height = 16.000000000000000000
          AutoWidth = True
          DataField = 'calc_method_name'
          DataSet = frxDBDataset_link
          DataSetName = 'Link'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clSilver
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8 = (
            '[Link."calc_method_name"]')
          ParentFont = False
        end
        object Memo137: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Width = 141.600000000000000000
          Height = 16.000000000000000000
          AutoWidth = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clSilver
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8 = (
            #1056#1114#1056#181#1057#8218#1056#1109#1056#1169#1056#1105#1056#1108#1056#176' '#1057#1026#1056#176#1057#1027#1057#8225#1056#181#1057#8218#1056#176': ')
          ParentFont = False
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Frame.Typ = []
        Height = 31.118120000000000000
        Top = 1388.000000000000000000
        Width = 718.110700000000000000
        DataSet = frxDBDataset_group
        DataSetName = 'group'
        RowCount = 0
        object Memo154: TfrxMemoView
          Align = baWidth
          AllowVectorExport = True
          Left = 36.000000000000000000
          Top = 3.779530000000000000
          Width = 682.110700000000000000
          Height = 16.000000000000000000
          DataField = 'caption'
          DataSet = frxDBDataset_group
          DataSetName = 'group'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[group."caption"]')
          ParentFont = False
        end
        object Memo155: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 3.779530000000000000
          Width = 36.000000000000000000
          Height = 16.000000000000000000
          DataField = 'id'
          DataSet = frxDBDataset_group
          DataSetName = 'group'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[group."id"]')
          ParentFont = False
        end
      end
      object MasterData2: TfrxMasterData
        FillType = ftBrush
        Frame.Typ = []
        Height = 16.000000000000000000
        Top = 1288.000000000000000000
        Width = 718.110700000000000000
        DataSet = frxDBDataset_Profile_items
        DataSetName = 'Profile'
        RowCount = 0
        object Memo144: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 60.000000000000000000
          Width = 100.000000000000000000
          Height = 16.000000000000000000
          DataField = 'distance_km'
          DataSet = frxDBDataset_Profile_items
          DataSetName = 'Profile'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Profile."distance_km"]')
          ParentFont = False
        end
        object Memo145: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Width = 60.000000000000000000
          Height = 16.000000000000000000
          Visible = False
          DataField = 'index'
          DataSet = frxDBDataset_Profile_items
          DataSetName = 'Profile'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Profile."index"]')
          ParentFont = False
        end
        object Memo146: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 260.000000000000000000
          Width = 100.000000000000000000
          Height = 16.000000000000000000
          DataField = 'earth_h'
          DataSet = frxDBDataset_Profile_items
          DataSetName = 'Profile'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Profile."earth_h"]')
          ParentFont = False
        end
        object Memo147: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 160.000000000000000000
          Width = 100.000000000000000000
          Height = 16.000000000000000000
          OnBeforePrint = 'Memo92OnBeforePrint'
          DataField = 'ground_h'
          DataSet = frxDBDataset_Profile_items
          DataSetName = 'Profile'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Profile."ground_h"]')
          ParentFont = False
        end
        object Memo148: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 360.000000000000000000
          Width = 100.000000000000000000
          Height = 16.000000000000000000
          DataField = 'clutter_h'
          DataSet = frxDBDataset_Profile_items
          DataSetName = 'Profile'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Profile."clutter_h"]')
          ParentFont = False
        end
        object Shape1_Color: TfrxShapeView
          Align = baLeft
          AllowVectorExport = True
          Left = 460.000000000000000000
          Width = 20.000000000000000000
          Height = 16.000000000000000000
          OnBeforePrint = 'Shape_ColorOnBeforePrint'
          Fill.BackColor = clTeal
          Frame.Typ = []
        end
        object Memo149: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 480.000000000000000000
          Width = 176.000000000000000000
          Height = 16.000000000000000000
          DataField = 'clutter_name'
          DataSet = frxDBDataset_Profile_items
          DataSetName = 'Profile'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Profile."clutter_name"]')
          ParentFont = False
        end
      end
      object Header3: TfrxHeader
        FillType = ftBrush
        Frame.Typ = []
        Height = 38.779530000000000000
        Top = 1228.000000000000000000
        Width = 718.110700000000000000
        StartNewPage = True
        object Memo80: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 3.779530000000000000
          Width = 60.000000000000000000
          Height = 35.000000000000000000
          Visible = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            'Index')
          ParentFont = False
        end
        object Memo81: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 60.000000000000000000
          Top = 3.779530000000000000
          Width = 100.000000000000000000
          Height = 35.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#160#1056#176#1057#1027#1057#1027#1057#8218#1056#1109#1057#1039#1056#1029#1056#1105#1056#181', '#1056#1108#1056#1112)
          ParentFont = False
        end
        object Memo82: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 260.000000000000000000
          Top = 3.779530000000000000
          Width = 100.000000000000000000
          Height = 35.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1113#1057#1026#1056#1105#1056#1030#1056#1105#1056#183#1056#1029#1056#176' '#1056#183#1056#181#1056#1112#1056#187#1056#1105)
          ParentFont = False
        end
        object Memo83: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 360.000000000000000000
          Top = 3.779530000000000000
          Width = 100.000000000000000000
          Height = 35.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#8217#1057#8249#1057#1027#1056#1109#1057#8218#1056#176' '#1056#1114#1056#1119)
          ParentFont = False
        end
        object Memo142: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 160.000000000000000000
          Top = 3.779530000000000000
          Width = 100.000000000000000000
          Height = 35.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#8217#1057#8249#1057#1027#1056#1109#1057#8218#1056#176' '#1057#1026#1056#181#1056#187#1057#1034#1056#181#1057#8222#1056#176)
          ParentFont = False
        end
        object Memo143: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 460.000000000000000000
          Top = 3.779530000000000000
          Width = 196.000000000000000000
          Height = 35.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1118#1056#1105#1056#1111' '#1056#1114#1056#1119)
          ParentFont = False
        end
      end
      object Header4: TfrxHeader
        FillType = ftBrush
        Frame.Typ = []
        Height = 45.354360000000000000
        Top = 1324.000000000000000000
        Width = 718.110700000000000000
        StartNewPage = True
        object Memo156: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 22.677180000000000000
          Width = 434.000000000000000000
          Height = 16.666666670000000000
          AutoWidth = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8 = (
            #1056#160#1056#181#1056#183#1057#1107#1056#187#1057#1034#1057#8218#1056#176#1057#8218#1057#8249' '#1057#1026#1056#176#1057#1027#1057#8225#1056#181#1057#8218#1056#1109#1056#1030)
          ParentFont = False
        end
      end
      object DetailData2: TfrxDetailData
        FillType = ftBrush
        Frame.Typ = []
        Height = 18.000000000000000000
        Top = 1440.000000000000000000
        Width = 718.110700000000000000
        DataSet = frxDBDataset_Items
        DataSetName = 'Items'
        RowCount = 0
        object Memo150: TfrxMemoView
          Align = baWidth
          AllowVectorExport = True
          Left = 36.000000000000000000
          Width = 592.110700000000000000
          Height = 18.000000000000000000
          DataField = 'caption'
          DataSet = frxDBDataset_Items
          DataSetName = 'Items'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Items."caption"]')
          ParentFont = False
        end
        object Memo151: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 628.110700000000000000
          Width = 90.000000000000000000
          Height = 18.000000000000000000
          DataField = 'value'
          DataSet = frxDBDataset_Items
          DataSetName = 'Items'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Items."value"]')
          ParentFont = False
        end
        object Memo153: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Width = 36.000000000000000000
          Height = 18.000000000000000000
          DataField = 'id'
          DataSet = frxDBDataset_Items
          DataSetName = 'Items'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Items."id"]')
          ParentFont = False
        end
      end
      object Child_profile_label: TfrxChild
        FillType = ftBrush
        Frame.Typ = []
        Height = 16.000000000000000000
        Top = 912.000000000000000000
        Width = 718.110700000000000000
        Child = frxReport_repeater.Child_profile
        StartNewPage = True
        ToNRows = 0
        ToNRowsMode = rmCount
        object Memo160: TfrxMemoView
          Align = baWidth
          AllowVectorExport = True
          Width = 718.110700000000000000
          Height = 16.000000000000000000
          DataSet = frxDBDataset_Profile_items
          DataSetName = 'Profile'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8 = (
            
              #1056#160#1056#1105#1057#1027#1057#1107#1056#1029#1056#1109#1056#1108' 2. '#1056#152#1056#183#1056#1109#1056#177#1057#1026#1056#176#1056#182#1056#181#1056#1029#1056#1105#1056#181' '#1056#1111#1057#1026#1056#1109#1057#8222#1056#1105#1056#187#1057#1039' '#1056#160#1056#160#1056#152' '#1056 +
              #1111#1057#1026#1056#1105' '#1056#1029#1056#1109#1057#1026#1056#1112#1056#176#1056#187#1057#1034#1056#1029#1056#1109#1056#8470' '#1057#1026#1056#181#1057#8222#1057#1026#1056#176#1056#1108#1057#8224#1056#1105#1056#1105)
          ParentFont = False
        end
      end
      object Child_map_label: TfrxChild
        FillType = ftBrush
        Frame.Typ = []
        Height = 16.000000000000000000
        Top = 832.000000000000000000
        Width = 718.110700000000000000
        Child = frxReport_repeater.Child_map
        StartNewPage = True
        ToNRows = 0
        ToNRowsMode = rmCount
        object Memo159: TfrxMemoView
          Align = baWidth
          AllowVectorExport = True
          Width = 718.110700000000000000
          Height = 16.000000000000000000
          DataSet = frxDBDataset_Profile_items
          DataSetName = 'Profile'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8 = (
            
              #1056#160#1056#1105#1057#1027#1057#1107#1056#1029#1056#1109#1056#1108' 1. '#1056#8217#1057#8249#1056#1108#1056#1109#1056#1111#1056#1105#1057#1026#1056#1109#1056#1030#1056#1108#1056#176' '#1056#1108#1056#176#1057#1026#1057#8218#1057#8249' '#1056#1112#1056#181#1057#1027#1057#8218#1056#1029#1056#1109 +
              #1057#1027#1057#8218#1056#1105)
          ParentFont = False
        end
      end
      object Header1: TfrxHeader
        FillType = ftBrush
        Frame.Typ = []
        Height = 22.677180000000000000
        Top = 996.000000000000000000
        Width = 718.110700000000000000
        StartNewPage = True
        object Memo3: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Width = 434.000000000000000000
          Height = 16.666666670000000000
          AutoWidth = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8 = (
            #1056#160#1056#181#1057#8218#1057#1026#1056#176#1056#1029#1057#1027#1056#187#1057#1039#1057#8218#1056#1109#1057#1026)
          ParentFont = False
        end
      end
      object MasterData4: TfrxMasterData
        FillType = ftBrush
        Frame.Typ = []
        Height = 133.165430000000000000
        Top = 1040.000000000000000000
        Width = 718.110700000000000000
        DataSet = frxDBDataset_repeater_property
        DataSetName = 'site3'
        RowCount = 0
        object Memo8: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Width = 110.000000000000000000
          Height = 32.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1119#1056#187#1056#1109#1057#8240#1056#176#1056#1169#1056#1108#1056#176)
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 110.000000000000000000
          Width = 177.637834330000000000
          Height = 32.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[site3."name"]')
          ParentFont = False
          UseDefaultCharset = True
        end
        object Memo132: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 32.118120000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1113#1056#1109#1056#1109#1057#1026#1056#1169#1056#1105#1056#1029#1056#176#1057#8218#1057#8249'  CK-42')
          ParentFont = False
        end
        object Memo133: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 110.000000000000000000
          Top = 32.118120000000000000
          Width = 177.637834330000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[DegreeToStr (<site3."lat">)]')
          ParentFont = False
          UseDefaultCharset = True
        end
        object Memo157: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 110.000000000000000000
          Top = 47.118120000000000000
          Width = 177.637834330000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[DegreeToStr (<site3."lon">)]')
          ParentFont = False
          UseDefaultCharset = True
        end
        object Memo158: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 47.118120000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          ParentFont = False
        end
        object Memo79: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 124.252010000000000000
          Top = 117.165430000000000000
          Width = 113.385826771654000000
          Height = 16.000000000000000000
          DataSet = frxDBDataset_Profile_items
          DataSetName = 'Profile'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1032#1057#1027#1056#1105#1056#187#1056#181#1056#1029#1056#1105#1056#181', dBI')
          ParentFont = False
        end
        object Memo90: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 117.165430000000000000
          Width = 124.252010000000000000
          Height = 16.000000000000000000
          DataSet = frxDBDataset_Profile_items
          DataSetName = 'Profile'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1106#1056#183#1056#1105#1056#1112#1057#1107#1057#8218', '#1056#1110#1057#1026#1056#176#1056#1169)
          ParentFont = False
        end
        object Memo91: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 237.637836771654000000
          Top = 117.165430000000000000
          Width = 113.385826771654000000
          Height = 16.000000000000000000
          OnBeforePrint = 'Memo92OnBeforePrint'
          DataSet = frxDBDataset_Profile_items
          DataSetName = 'Profile'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#8217#1057#8249#1057#1027#1056#1109#1057#8218#1056#176', m')
          ParentFont = False
          WordBreak = True
        end
        object Memo92: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 351.023663543308000000
          Top = 117.165430000000000000
          Width = 113.385826771654000000
          Height = 16.000000000000000000
          OnBeforePrint = 'Memo92OnBeforePrint'
          DataSet = frxDBDataset_Profile_items
          DataSetName = 'Profile'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1119#1056#1109#1057#8218#1056#181#1057#1026#1056#1105' '#1056#1106#1056#164#1056#1032', dB')
          ParentFont = False
        end
        object Memo101: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 62.362204720000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1113#1056#1109#1056#1109#1057#1026#1056#1169#1056#1105#1056#1029#1056#176#1057#8218#1057#8249'  WGS')
          ParentFont = False
        end
        object Memo103: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 110.000000000000000000
          Top = 62.362204724409500000
          Width = 177.637834330000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[DegreeToStr (<site3."lat_wgs">)]')
          ParentFont = False
          UseDefaultCharset = True
        end
        object Memo104: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 110.000000000000000000
          Top = 77.480314960629900000
          Width = 177.637834330000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[DegreeToStr (<site3."lon_wgs">)]')
          ParentFont = False
          UseDefaultCharset = True
        end
        object Memo120: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 77.480314960629900000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          ParentFont = False
        end
      end
      object DetailData1: TfrxDetailData
        FillType = ftBrush
        Frame.Typ = []
        Height = 16.000000000000000000
        Top = 1192.000000000000000000
        Width = 718.110700000000000000
        DataSet = frxDBDataset_repeater_ant
        DataSetName = 'repeater_ant'
        RowCount = 0
        object Memo77: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 124.252010000000000000
          Width = 113.385826771654000000
          Height = 16.000000000000000000
          DataSet = frxDBDataset_Profile_items
          DataSetName = 'Profile'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[repeater_ant."gain"]')
          ParentFont = False
        end
        object Memo78: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Width = 124.252010000000000000
          Height = 16.000000000000000000
          DataSet = frxDBDataset_Profile_items
          DataSetName = 'Profile'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[repeater_ant."azimuth"]')
          ParentFont = False
        end
        object Memo89: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 237.637836771654000000
          Width = 113.385826771654000000
          Height = 16.000000000000000000
          OnBeforePrint = 'Memo92OnBeforePrint'
          DataSet = frxDBDataset_Profile_items
          DataSetName = 'Profile'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[repeater_ant."height"]')
          ParentFont = False
          WordBreak = True
        end
        object Memo93: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 351.023663543308000000
          Width = 113.385826771654000000
          Height = 16.000000000000000000
          OnBeforePrint = 'Memo92OnBeforePrint'
          DataSet = frxDBDataset_Profile_items
          DataSetName = 'Profile'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[repeater_ant."loss"]')
          ParentFont = False
        end
      end
      object Child_profile: TfrxChild
        FillType = ftBrush
        Frame.Typ = []
        Height = 26.456710000000000000
        Top = 948.000000000000000000
        Width = 718.110700000000000000
        ToNRows = 0
        ToNRowsMode = rmCount
        object Picture_profile: TfrxPictureView
          Align = baClient
          AllowVectorExport = True
          Width = 718.110700000000000000
          Height = 26.456710000000000000
          OnBeforePrint = 'Picture_profileOnBeforePrint'
          DataField = 'profile_img'
          DataSet = frxDBDataset_Link_img
          DataSetName = 'Link_img'
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Stretched = False
          HightQuality = True
          Transparent = False
          TransparentColor = clWhite
        end
      end
      object Child_map: TfrxChild
        FillType = ftBrush
        Frame.Typ = []
        Height = 22.677180000000000000
        Top = 868.000000000000000000
        Width = 718.110700000000000000
        Child = frxReport_repeater.Child_profile_label
        ToNRows = 0
        ToNRowsMode = rmCount
        object Picture_map: TfrxPictureView
          Align = baClient
          AllowVectorExport = True
          Width = 718.110700000000000000
          Height = 22.677180000000000000
          OnBeforePrint = 'Picture_mapOnBeforePrint'
          DataField = 'map_img'
          DataSet = frxDBDataset_Link_img
          DataSetName = 'Link_img'
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Stretched = False
          HightQuality = True
          Transparent = False
          TransparentColor = clWhite
        end
      end
    end
  end
  object frxReport_short_WGS: TfrxReport
    Version = '6.2.11'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports2'
    PreviewOptions.AllowEdit = False
    PreviewOptions.Buttons = [pbPrint, pbExport, pbZoom, pbTools, pbNavigator, pbExportQuick, pbNoFullScreen]
    PreviewOptions.ThumbnailVisible = True
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = #1055#1086' '#1091#1084#1086#1083#1095#1072#1085#1080#1102
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 41730.469225335600000000
    ReportOptions.LastChange = 41939.805246851900000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      ''
      '         '
      ''
      ''
      'procedure frxReportOnStartReport(Sender: TfrxComponent);'
      'const'
      '  DEF_MAP_H = 800;'
      '  '
      '  DEF_H = 450;'
      '  DEF_OFFSET = 25;'
      ''
      '  DEF_TOP = 20;                                  '
      'begin'
      '//  Picture_map.Width:=200;                '
      '  Picture_map.Height     :=DEF_MAP_H;'
      '    '
      '  Picture_profile.Height :=DEF_H;                '
      '            '
      '  '
      '  Child_map.Height:=DEF_MAP_H;'
      '    '
      '  Child_profile.Height:=DEF_H + DEF_OFFSET;              '
      '                   '
      ''
      ' // Picture_map.Dataset:='#39'Link_img'#39';      '
      
        '//  Picture_profile.Dataset:='#39'Link_img'#39';                        ' +
        '  '
      '  '
      '            '
      '      '
      'end;  '
      '             '
      '      '
      '  '
      ''
      '  '
      '//------------------------------------------------------------'
      'function DegreeToStr (aValue: double): String;'
      '//------------------------------------------------------------'
      'const'
      '//  STR_BLPOINT_FORMAT1 = '#39'%d'#176'%.2d'#39#39'%s%1.2f'#39#39#39#39#39';'
      '  STR_BLPOINT_FORMAT1 = '#39'%d'#176' %.2d'#39#39' %1.2f'#39#39#39#39#39';'
      'var    '
      ' iDeg,iMin:integer; '
      ' eSec: Double;   '
      '           '
      'begin'
      '  geo_DecodeDegree_(aValue, iDeg,iMin,eSec);'
      ' '
      '  Result:=Format(STR_BLPOINT_FORMAT1, [iDeg, iMin, eSec]);'
      ''
      'end;'
      ''
      '       '
      ''
      '//------------------------------------------------------------'
      'function DegreeToStr_lat (aValue: double): String;'
      '//------------------------------------------------------------'
      'var '
      '  s: string;'
      'begin'
      '  if aValue>0 then s:='#39'N'#39' else s:='#39'S'#39';'
      ''
      '  aValue:=Abs(aValue);       '
      ''
      '  Result:=DegreeToStr (aValue) + s;'
      'end;'
      ''
      '//------------------------------------------------------------'
      'function DegreeToStr_lon (aValue: double): String;'
      '//------------------------------------------------------------'
      'var '
      '  s: string;'
      'begin'
      '   if aValue<0 then begin'
      '     s:='#39'W'#39';'
      '     aValue:=Abs(aValue);'
      '   end else'
      ''
      '   if aValue>180 then begin'
      '     s:='#39'W'#39';'
      '     aValue:=360 - aValue'
      '   end else'
      '     s:='#39'E'#39';     '
      ''
      '  Result:=DegreeToStr (aValue) + s;'
      'end;  '
      ''
      '             '
      ' //------------------------------------------------------------'
      
        'procedure geo_DecodeDegree_(aValue: Double; var aDeg,aMin:intege' +
        'r; var aSec:'
      '    Double);'
      '//------------------------------------------------------------'
      'var'
      '  bIsNegative: Boolean;'
      '  e: Double;'
      '  eSec: Double;'
      ''
      'begin'
      '  bIsNegative := aValue<0;'
      ''
      '  aValue :=Abs(aValue);'
      ''
      '  aDeg:=Trunc(aValue);'
      ''
      '  aMin:=Trunc(Frac(aValue)*60);'
      ''
      ''
      '//  e:= (aValue - Trunc(aValue));'
      '  e:= (aValue - Trunc(aValue)) * 60;'
      ''
      ''
      '  aMin:=Trunc(e);'
      '  aSec:=(((aValue - aDeg) *60) - aMin) * 60;'
      ''
      '   while aSec>(60-0.1) do'
      '  begin'
      '    aMin:=aMin+1;'
      '    aSec:=aSec-60;'
      '  end;    '
      ''
      '  while aMin>=60 do'
      '  begin'
      '      aMin:=aMin-60;'
      '      aDeg:=aDeg+1;'
      '  end;  '
      ''
      '  if aSec<0 then'
      '     aSec:=0;'
      '    '
      ''
      ''
      '  if bIsNegative then'
      '    aDeg:=-aDeg;'
      ''
      ''
      ''
      'end;'
      '  '
      '                                 '
      ''
      '//------------------------------------------------------------'
      
        'procedure geo_DecodeDegree_1111(aValue: Double; var aDeg,aMin:in' +
        'teger; var aSec:'
      '    Double);'
      '//------------------------------------------------------------'
      'const'
      ' EXP_ = 0.000000001;'
      '     //97,033333333'
      'var'
      '  bIsNegative: Boolean;'
      '  e: Double;'
      '  eMinutes: Double;'
      '  eSec: Double;'
      '  eSeconds: Double;'
      'begin'
      '  bIsNegative := aValue<0;'
      ''
      '  aValue :=Abs(aValue);'
      ''
      '  aDeg:=Trunc(aValue);'
      ''
      ' // e:=Abs(aValue)*60;'
      ''
      '//  , Frac(X) = X - Int(X).'
      ''
      '  aMin:=Trunc(Frac(aValue)*60);'
      ''
      '//  Result.Sec:=Frac(Frac(Abs(aValue))*60)*60;'
      '  e:= (aValue - Trunc(aValue)) * 60;'
      '  aMin:=Trunc(e);'
      ''
      ' eMinutes := Trunc (e);'
      ' eSeconds := (e - Trunc (e)) * 60 ;'
      ''
      ''
      '  aSec:=(((aValue - aDeg) *60) - aMin) * 60;'
      ''
      '  if (aSec - 60) > EXP_ then'
      '  begin'
      '    Inc(aMin);'
      '    aSec := 0;'
      '  end;'
      ''
      ''
      '  if bIsNegative then'
      '    aDeg:=-aDeg;'
      '          '
      'end;'
      '        '
      ''
      '       '
      'begin'
      ''
      'end.    ')
    OnStartReport = 'frxReportOnStartReport'
    Left = 582
    Top = 20
    Datasets = <
      item
        DataSet = frxDBDataset_Antenna1
        DataSetName = 'Antenna1'
      end
      item
        DataSet = frxDBDataset_Antenna2
        DataSetName = 'Antenna2'
      end
      item
        DataSet = frxDBDataset_group
        DataSetName = 'group'
      end
      item
        DataSet = frxDBDataset_Items
        DataSetName = 'Items'
      end
      item
        DataSet = frxDBDataset_link
        DataSetName = 'Link'
      end
      item
        DataSet = frxDBDataset_Link_img
        DataSetName = 'Link_img'
      end
      item
        DataSet = frxDBDataset_LinkEnd1
        DataSetName = 'LinkEnd1'
      end
      item
        DataSet = frxDBDataset_LinkEnd2
        DataSetName = 'LinkEnd2'
      end
      item
        DataSet = dmLinkLine_report.frxDBDataset_LinkLine
        DataSetName = 'LinkLine'
      end
      item
        DataSet = dmLinkLine_report.frxDBDataset_LinkLine_img
        DataSetName = 'LinkLine_img'
      end
      item
        DataSet = frxDBDataset_Profile_items
        DataSetName = 'Profile'
      end
      item
        DataSet = frxDBDataset_Property1
        DataSetName = 'Site1'
      end
      item
        DataSet = frxDBDataset_Property2
        DataSetName = 'Site2'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 9.260416666666670000
      BottomMargin = 10.583333333333300000
      Frame.Typ = []
      LargeDesignHeight = True
      OnBeforePrint = 'Page1OnBeforePrint'
      object Child_Header: TfrxChild
        FillType = ftBrush
        Frame.Typ = []
        Height = 163.220470000000000000
        Top = 181.417440000000000000
        Width = 718.110700000000000000
        Child = frxReport_short_WGS.Child_Summary
        ToNRows = 0
        ToNRowsMode = rmCount
        object Memo105: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 478.110700000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1119#1056#187#1056#1109#1057#8240#1056#176#1056#1169#1056#1108#1056#176)
          ParentFont = False
        end
        object Memo106: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 588.110700000000000000
          Width = 130.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Site2."name"]')
          ParentFont = False
        end
        object Memo108: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 478.110700000000000000
          Top = 15.000000000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1113#1056#1109#1056#1109#1057#1026#1056#1169#1056#1105#1056#1029#1056#176#1057#8218#1057#8249' WGS')
          ParentFont = False
        end
        object Memo109: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 478.110700000000000000
          Top = 45.000000000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#8217#1057#8249#1057#1027#1056#1109#1057#8218#1056#176' '#1056#1111#1056#1109#1056#1169#1056#1030#1056#181#1057#1027#1056#176', m')
          ParentFont = False
        end
        object Memo110: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 478.110700000000000000
          Top = 60.000000000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1106#1056#183#1056#1105#1056#1112#1057#1107#1057#8218)
          ParentFont = False
        end
        object Memo111: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 478.110700000000000000
          Top = 75.000000000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1032#1056#1110#1056#1109#1056#187' '#1056#1112#1056#181#1057#1027#1057#8218#1056#176)
          ParentFont = False
        end
        object Memo112: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 478.110700000000000000
          Top = 90.000000000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#8221#1056#1105#1056#176#1056#1112#1056#181#1057#8218#1057#1026' '#1056#176#1056#1029#1057#8218#1056#181#1056#1029#1056#1029#1057#8249)
          ParentFont = False
        end
        object Memo114: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 588.110700000000000000
          Top = 15.000000000000000000
          Width = 130.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[DegreeToStr (<Site2."lat_wgs">)]')
          ParentFont = False
        end
        object Memo115: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 588.110700000000000000
          Top = 45.000000000000000000
          Width = 130.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[antenna2."height"]')
          ParentFont = False
        end
        object Memo116: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 588.110700000000000000
          Top = 60.000000000000000000
          Width = 130.000000000000000000
          Height = 15.000000000000000000
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."azimuth2"]')
          ParentFont = False
        end
        object Memo117: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 588.110700000000000000
          Top = 90.000000000000000000
          Width = 130.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Antenna2."diameter"]')
          ParentFont = False
        end
        object Memo118: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 588.110700000000000000
          Top = 30.000000000000000000
          Width = 130.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[DegreeToStr (<Site2."lon_wgs">)]')
          ParentFont = False
        end
        object Memo130: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 478.110700000000000000
          Top = 30.000000000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 588.110700000000000000
          Top = 75.000000000000000000
          Width = 130.000000000000000000
          Height = 15.000000000000000000
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Antenna2."tilt"]')
          ParentFont = False
        end
        object Memo66: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 478.110700000000000000
          Top = 105.000000000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1032#1057#1027#1056#1105#1056#187#1056#181#1056#1029#1056#1105#1056#181' '#1056#176#1056#1029#1057#8218#1056#181#1056#1029#1056#1029#1057#8249)
          ParentFont = False
        end
        object Memo99: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 588.110700000000000000
          Top = 105.000000000000000000
          Width = 130.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Antenna2."gain"]')
          ParentFont = False
        end
        object Memo43: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1119#1056#187#1056#1109#1057#8240#1056#176#1056#1169#1056#1108#1056#176)
          ParentFont = False
        end
        object Memo_left_1: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 110.000000000000000000
          Width = 125.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Site1."name"]')
          ParentFont = False
        end
        object Memo68: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 15.000000000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1113#1056#1109#1056#1109#1057#1026#1056#1169#1056#1105#1056#1029#1056#176#1057#8218#1057#8249' WGS')
          ParentFont = False
        end
        object Memo70: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 45.000000000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#8217#1057#8249#1057#1027#1056#1109#1057#8218#1056#176' '#1056#1111#1056#1109#1056#1169#1056#1030#1056#181#1057#1027#1056#176', m')
          ParentFont = False
        end
        object Memo72: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 60.000000000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1106#1056#183#1056#1105#1056#1112#1057#1107#1057#8218)
          ParentFont = False
        end
        object Memo74: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 75.000000000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1032#1056#1110#1056#1109#1056#187' '#1056#1112#1056#181#1057#1027#1057#8218#1056#176)
          ParentFont = False
        end
        object Memo100: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 90.000000000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#8221#1056#1105#1056#176#1056#1112#1056#181#1057#8218#1057#1026' '#1056#176#1056#1029#1057#8218#1056#181#1056#1029#1056#1029#1057#8249)
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 110.000000000000000000
          Top = 15.000000000000000000
          Width = 125.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[DegreeToStr (<Site1."lat_WGS">)]')
          ParentFont = False
        end
        object Memo1: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 110.000000000000000000
          Top = 45.000000000000000000
          Width = 125.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[antenna1."height"]')
          ParentFont = False
        end
        object Memo95: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 110.000000000000000000
          Top = 60.000000000000000000
          Width = 125.000000000000000000
          Height = 15.000000000000000000
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."azimuth1"]')
          ParentFont = False
        end
        object Memo97: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 110.000000000000000000
          Top = 90.000000000000000000
          Width = 125.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[antenna1."diameter"]')
          ParentFont = False
        end
        object Memo98: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 110.000000000000000000
          Top = 30.000000000000000000
          Width = 125.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[DegreeToStr (<Site1."lon_WGS">)]')
          ParentFont = False
        end
        object Memo96: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 110.000000000000000000
          Top = 75.000000000000000000
          Width = 125.000000000000000000
          Height = 15.000000000000000000
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Antenna1."tilt"]')
          ParentFont = False
        end
        object Memo121: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 30.000000000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 105.000000000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1032#1057#1027#1056#1105#1056#187#1056#181#1056#1029#1056#1105#1056#181' '#1056#176#1056#1029#1057#8218#1056#181#1056#1029#1056#1029#1057#8249)
          ParentFont = False
        end
        object Memo30: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 110.000000000000000000
          Top = 105.000000000000000000
          Width = 125.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[antenna1."gain"]')
          ParentFont = False
        end
        object Memo45: TfrxMemoView
          AllowVectorExport = True
          Left = 247.000000000000000000
          Top = 15.000000000000000000
          Width = 144.204726490000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#8221#1056#187#1056#1105#1056#1029#1056#176' '#1056#1105#1056#1029#1057#8218#1056#181#1057#1026#1056#1030#1056#176#1056#187#1056#176', km')
          ParentFont = False
        end
        object Memo47: TfrxMemoView
          AllowVectorExport = True
          Left = 247.000000000000000000
          Top = 30.000000000000000000
          Width = 144.204726490000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1032#1057#1026#1056#1109#1056#1030#1056#181#1056#1029#1057#1034' '#1057#1027#1056#1105#1056#1110#1056#1029#1056#176#1056#187#1056#176', dBm')
          ParentFont = False
        end
        object Memo49: TfrxMemoView
          AllowVectorExport = True
          Left = 247.000000000000000000
          Top = 45.000000000000000000
          Width = 144.204726490000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#160#1056#176#1056#177#1056#1109#1057#8225#1056#176#1057#1039' '#1057#8225#1056#176#1057#1027#1057#8218#1056#1109#1057#8218#1056#176', GHz')
          ParentFont = False
        end
        object Memo51: TfrxMemoView
          AllowVectorExport = True
          Left = 247.000000000000000000
          Top = 60.000000000000000000
          Width = 144.204726490000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1114#1056#1109#1057#8240#1056#1029#1056#1109#1057#1027#1057#8218#1057#1034' '#1056#1111#1056#181#1057#1026#1056#181#1056#1169#1056#176#1057#8218#1057#8225#1056#1105#1056#1108#1056#176', dBm')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Left = 247.000000000000000000
          Top = 75.000000000000000000
          Width = 144.204726490000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1038#1056#1108#1056#1109#1057#1026#1056#1109#1057#1027#1057#8218#1057#1034' '#1056#1111#1056#181#1057#1026#1056#181#1056#1169#1056#176#1057#8225#1056#1105', Mbit/s')
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 247.000000000000000000
          Top = 90.000000000000000000
          Width = 144.204726490000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1118#1056#1105#1056#1111' '#1056#1109#1056#177#1056#1109#1057#1026#1057#1107#1056#1169#1056#1109#1056#1030#1056#176#1056#1029#1056#1105#1057#1039)
          ParentFont = False
        end
        object Memo123: TfrxMemoView
          AllowVectorExport = True
          Left = 391.204726490000000000
          Top = 30.000000000000000000
          Width = 74.905973510000000000
          Height = 15.000000000000000000
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."rx_level_dBm"]')
          ParentFont = False
        end
        object Memo124: TfrxMemoView
          AllowVectorExport = True
          Left = 391.204726490000000000
          Top = 75.000000000000000000
          Width = 74.905973510000000000
          Height = 15.000000000000000000
          DataField = 'bitrate_Mbps'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd1."bitrate_Mbps"]')
          ParentFont = False
        end
        object Memo125: TfrxMemoView
          AllowVectorExport = True
          Left = 391.204726490000000000
          Top = 90.000000000000000000
          Width = 74.905973510000000000
          Height = 15.000000000000000000
          DataField = 'LinkEndType_name'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd1."LinkEndType_name"]')
          ParentFont = False
        end
        object Memo127: TfrxMemoView
          AllowVectorExport = True
          Left = 391.204726490000000000
          Top = 45.000000000000000000
          Width = 74.905973510000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[FloatToStr( <LinkEnd1."tx_freq_MHz"> / 1000 )]')
          ParentFont = False
        end
        object Memo126333: TfrxMemoView
          AllowVectorExport = True
          Left = 391.204726490000000000
          Top = 60.000000000000000000
          Width = 74.905973510000000000
          Height = 15.000000000000000000
          DataField = 'power_dBm'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd1."power_dBm"]')
          ParentFont = False
        end
        object Memo42: TfrxMemoView
          AllowVectorExport = True
          Left = 247.000000000000000000
          Width = 144.204726490000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1119#1057#1026#1056#1105#1056#183#1056#1029#1056#176#1056#1108' '#1056#1111#1057#1026#1056#1105#1056#1110#1056#1109#1056#1169#1056#1029#1056#1109#1057#1027#1057#8218#1056#1105)
          ParentFont = False
        end
        object Memo122: TfrxMemoView
          AllowVectorExport = True
          Left = 391.204726490000000000
          Width = 74.905973510000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."status_str"]')
          ParentFont = False
        end
        object Memo134: TfrxMemoView
          AllowVectorExport = True
          Left = 247.000000000000000000
          Top = 105.000000000000000000
          Width = 144.204726490000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#8212#1056#176#1056#1111#1056#176#1057#1027' '#1056#1029#1056#176' '#1056#183#1056#176#1056#1112#1056#1105#1057#1026#1056#176#1056#1029#1056#1105#1057#1039', dB')
          ParentFont = False
        end
        object Memo135: TfrxMemoView
          AllowVectorExport = True
          Left = 391.204726490000000000
          Top = 105.000000000000000000
          Width = 74.905973510000000000
          Height = 15.000000000000000000
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."fade_margin_dB"]')
          ParentFont = False
        end
        object Memo128: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 391.204726490000000000
          Top = 15.000000000000000000
          Width = 74.905973510000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."length_km"]')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Left = 247.181102360000000000
          Top = 124.724490000000000000
          Width = 144.881880000000000000
          Height = 15.000000000000000000
          DataSet = frxDBDataset_link
          DataSetName = 'Link'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1114#1056#1109#1056#1169#1057#1107#1056#187#1057#1039#1057#8224#1056#1105#1057#1039)
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Left = 393.953063460000000000
          Top = 124.724490000000000000
          Width = 71.811026060000000000
          Height = 15.000000000000000000
          DataSet = frxDBDataset_link
          DataSetName = 'Link'
          DisplayFormat.FormatStr = '%2.0f'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd1."modulation_type"]')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Left = 247.181102360000000000
          Top = 139.842610000000000000
          Width = 144.881880000000000000
          Height = 15.000000000000000000
          DataSet = frxDBDataset_link
          DataSetName = 'Link'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1025#1056#1105#1057#1026#1056#1105#1056#1029#1056#176' '#1056#1111#1056#1109#1056#187#1056#1109#1057#1027#1057#8249', MHz')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          AllowVectorExport = True
          Left = 393.953063460000000000
          Top = 139.842610000000000000
          Width = 71.811026060000000000
          Height = 15.000000000000000000
          DataSet = frxDBDataset_link
          DataSetName = 'Link'
          DisplayFormat.FormatStr = '%2.0f'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd1."channel_width_MHz"]')
          ParentFont = False
        end
      end
      object Child_Summary: TfrxChild
        FillType = ftBrush
        Frame.Typ = []
        Height = 442.897650000000000000
        Top = 366.614410000000000000
        Width = 718.110700000000000000
        Child = frxReport_short_WGS.Child_map
        ToNRows = 0
        ToNRowsMode = rmCount
        object Memo3: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 15.000000000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#8217#1057#8249#1057#1027#1056#1109#1057#8218#1056#176', m')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 30.000000000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1106#1056#183#1056#1105#1056#1112#1057#1107#1057#8218' ')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 45.000000000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1118#1056#1105#1056#1111' '#1056#1109#1056#177#1056#1109#1057#1026#1057#1107#1056#1169#1056#1109#1056#1030#1056#176#1056#1029#1056#1105#1057#1039' ')
          ParentFont = False
        end
        object Memo77: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 60.000000000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#167#1056#176#1057#1027#1057#8218#1056#1109#1057#8218#1056#176' '#1056#1111#1056#181#1057#1026#1056#181#1056#1169#1056#176#1057#8225#1056#1105', MHz')
          ParentFont = False
        end
        object Memo78: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 75.000000000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1038#1056#1108#1056#1109#1057#1026#1056#1109#1057#1027#1057#8218#1057#1034' '#1056#1111#1056#181#1057#1026#1056#181#1056#1169#1056#176#1057#8225#1056#1105', Mbit/s')
          ParentFont = False
        end
        object Memo79: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 90.000000000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1114#1056#1109#1057#8240#1056#1029#1056#1109#1057#1027#1057#8218#1057#1034', dBm')
          ParentFont = False
        end
        object Memo81: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 105.000000000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#8221#1056#1105#1056#176#1056#1112#1056#181#1057#8218#1057#1026' '#1056#176#1056#1029#1057#8218#1056#181#1056#1029#1056#1029#1057#8249', m')
          ParentFont = False
        end
        object Memo82: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 151.677180000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            
              #1056#1038#1057#1026#1056#181#1056#1169#1056#1029#1057#1039#1057#1039' '#1056#1112#1056#1109#1057#8240#1056#1029#1056#1109#1057#1027#1057#8218#1057#1034' '#1056#1029#1056#176' '#1056#1030#1057#8230#1056#1109#1056#1169#1056#181' '#1056#1111#1057#1026#1056#1105#1056#181#1056#1112#1056#1029#1056#1105#1056#1108 +
              #1056#176', dBm')
          ParentFont = False
        end
        object Memo83: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 120.000000000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1119#1056#1109#1056#187#1057#1039#1057#1026#1056#1105#1056#183#1056#176#1057#8224#1056#1105#1057#1039' ')
          ParentFont = False
        end
        object Memo89: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 166.677180000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1106#1056#1111#1056#1111#1056#176#1057#1026#1056#176#1057#8218#1056#1029#1056#1109#1056#181' '#1057#1026#1056#181#1056#183#1056#181#1057#1026#1056#1030#1056#1105#1057#1026#1056#1109#1056#1030#1056#176#1056#1029#1056#1105#1056#181' ')
          ParentFont = False
        end
        object Memo90: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 181.677180000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1119#1056#1109#1057#1026#1056#1109#1056#1110' '#1056#1111#1057#1026#1056#1105' BER=10-3 ')
          ParentFont = False
        end
        object Memo91: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 211.677180000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1114#1056#1105#1056#1029#1056#1105#1056#1112#1056#176#1056#187#1057#1034#1056#1029#1057#8249#1056#8470' '#1056#1109#1057#8218#1056#1029#1056#1109#1057#1027#1056#1105#1057#8218#1056#181#1056#187#1057#1034#1056#1029#1057#8249#1056#8470' '#1056#1111#1057#1026#1056#1109#1057#1027#1056#1030#1056#181#1057#8218)
          ParentFont = False
        end
        object Memo92: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 196.677180000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1119#1056#1109#1057#1026#1056#1109#1056#1110' '#1056#1111#1057#1026#1056#1105' BER=10-6 ')
          ParentFont = False
        end
        object Memo93: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 226.677180000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1119#1056#1109#1056#187#1056#1109#1056#182#1056#181#1056#1029#1056#1105#1056#181' '#1057#8218#1056#1109#1057#8225#1056#1108#1056#1105' '#1056#1111#1057#1026#1056#1109#1057#1027#1056#1030#1056#181#1057#8218#1056#176', km ')
          ParentFont = False
        end
        object Memo101: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 241.677180000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            'SESR '#1057#1027#1057#1107#1056#177#1057#1026#1056#181#1057#8222#1056#1108#1057#8224#1056#1105#1056#1109#1056#1029#1056#1029#1056#176#1057#1039' '#1057#1027#1056#1109#1057#1027#1057#8218#1056#176#1056#1030#1056#187#1057#1039#1057#1035#1057#8240#1056#176#1057#1039', % ')
          ParentFont = False
        end
        object Memo103: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 256.677180000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            'SESR '#1056#1169#1056#1109#1056#182#1056#1169#1056#181#1056#1030#1056#176#1057#1039' '#1057#1027#1056#1109#1057#1027#1057#8218#1056#176#1056#1030#1056#187#1057#1039#1057#1035#1057#8240#1056#176#1057#1039', % ')
          ParentFont = False
        end
        object Memo104: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 271.677180000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            
              'SESR '#1056#1105#1056#1029#1057#8218#1056#181#1057#1026#1057#8222#1056#181#1057#1026#1056#181#1056#1029#1057#8224#1056#1105#1056#1109#1056#1029#1056#1029#1056#176#1057#1039' '#1057#1027#1056#1109#1057#1027#1057#8218#1056#176#1056#1030#1056#187#1057#1039#1057#1035#1057#8240#1056#176#1057#1039 +
              ', % ')
          ParentFont = False
        end
        object Memo107: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 286.677180000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            
              #1056#152#1057#8218#1056#1109#1056#1110#1056#1109#1056#1030#1057#8249#1056#8470' '#1056#1111#1056#1109#1056#1108#1056#176#1056#183#1056#176#1057#8218#1056#181#1056#187#1057#1034' '#1056#1108#1056#176#1057#8225#1056#181#1057#1027#1057#8218#1056#1030#1056#176' '#1056#1111#1056#1109' '#1056#1109#1057#8364 +
              #1056#1105#1056#177#1056#1108#1056#176#1056#1112' SESR, % ')
          ParentFont = False
        end
        object Memo113: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 301.677180000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            
              #1056#1116#1056#1109#1057#1026#1056#1112#1056#1105#1057#1026#1057#1107#1056#181#1056#1112#1056#1109#1056#181' '#1056#183#1056#1029#1056#176#1057#8225#1056#181#1056#1029#1056#1105#1056#181' SESR '#1056#1029#1056#176' '#1056#1105#1056#1029#1057#8218#1056#181#1057#1026#1056#1030#1056#176 +
              #1056#187#1056#181', % ')
          ParentFont = False
        end
        object Memo119: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 316.677180000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            
              #1056#152#1057#8218#1056#1109#1056#1110#1056#1109#1056#1030#1057#8249#1056#8470' '#1056#1111#1056#1109#1056#1108#1056#176#1056#183#1056#176#1057#8218#1056#181#1056#187#1057#1034' '#1056#1029#1056#181#1056#1110#1056#1109#1057#8218#1056#1109#1056#1030#1056#1029#1056#1109#1057#1027#1057#8218#1056#1105' '#1056 +
              #1113#1056#1029#1056#1110', %')
          ParentFont = False
        end
        object Memo120: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 331.677180000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            
              #1056#1116#1056#1109#1057#1026#1056#1112#1056#1105#1057#1026#1057#1107#1056#181#1056#1112#1056#1109#1056#181' '#1056#183#1056#1029#1056#176#1057#8225#1056#181#1056#1029#1056#1105#1056#181' '#1056#1113#1056#1029#1056#1110' '#1056#1029#1056#176' '#1056#1105#1056#1029#1057#8218#1056#181#1057#1026#1056#1030 +
              #1056#176#1056#187#1056#181', % ')
          ParentFont = False
        end
        object Memo129: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 391.677180000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            'ESR '#1057#8218#1057#1026#1056#181#1056#177', % ')
          ParentFont = False
        end
        object Memo132: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 346.677180000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            'BBER '#1056#1029#1056#1109#1057#1026#1056#1112', %')
          ParentFont = False
        end
        object Memo133: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 376.677180000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            'BBER '#1057#8218#1057#1026#1056#181#1056#177', % ')
          ParentFont = False
        end
        object Memo136: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 361.677180000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            'ESR '#1056#1029#1056#1109#1057#1026#1056#1112', % ')
          ParentFont = False
        end
        object Memo139: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 406.677180000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1119#1057#1026#1056#1105#1056#1110#1056#1109#1056#1169#1056#1029#1056#1109#1057#1027#1057#8218#1057#1034' ')
          ParentFont = False
        end
        object Memo140: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 310.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Site1."name"]')
          ParentFont = False
        end
        object Memo141: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 485.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Site2."name"]')
          ParentFont = False
        end
        object Memo142: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 310.000000000000000000
          Top = 30.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."azimuth1"]')
          ParentFont = False
        end
        object Memo143: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 485.000000000000000000
          Top = 30.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."azimuth2"]')
          ParentFont = False
        end
        object Memo144: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 310.000000000000000000
          Top = 181.677180000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd1."threshold_BER_3"]')
          ParentFont = False
        end
        object Memo145: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 310.000000000000000000
          Top = 196.677180000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd1."threshold_BER_6"]')
          ParentFont = False
        end
        object Memo146: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 310.000000000000000000
          Top = 105.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[antenna1."diameter"]')
          ParentFont = False
        end
        object Memo147: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 310.000000000000000000
          Top = 120.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[antenna1."polarization"]')
          ParentFont = False
        end
        object Memo148: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 310.000000000000000000
          Top = 75.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd1."bitrate_Mbps"]')
          ParentFont = False
        end
        object Memo149: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 310.000000000000000000
          Top = 90.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd1."power_dBm"]')
          ParentFont = False
        end
        object Memo150: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 310.000000000000000000
          Top = 45.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd1."LinkEndType_name"]')
          ParentFont = False
        end
        object Memo151: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 310.000000000000000000
          Top = 60.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd1."tx_freq_MHz"]')
          ParentFont = False
        end
        object Memo152: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 485.000000000000000000
          Top = 45.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd2."LinkEndType_name"]')
          ParentFont = False
        end
        object Memo153: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 485.000000000000000000
          Top = 60.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd2."tx_freq_MHz"]')
          ParentFont = False
        end
        object Memo154: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 485.000000000000000000
          Top = 75.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd2."bitrate_Mbps"]')
          ParentFont = False
        end
        object Memo155: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 485.000000000000000000
          Top = 90.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd2."power_dBm"]')
          ParentFont = False
        end
        object Memo156: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 485.000000000000000000
          Top = 181.677180000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd2."threshold_BER_3"]')
          ParentFont = False
        end
        object Memo157: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 485.000000000000000000
          Top = 196.677180000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd2."threshold_BER_6"]')
          ParentFont = False
        end
        object Memo158: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 310.000000000000000000
          Top = 391.677180000000000000
          Width = 350.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Link."ESR_REQUIRED"]')
          ParentFont = False
        end
        object Memo159: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 310.000000000000000000
          Top = 406.677180000000000000
          Width = 350.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Link."status_str"]')
          ParentFont = False
        end
        object Memo160: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 310.000000000000000000
          Top = 361.677180000000000000
          Width = 350.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Link."ESR_NORM"]')
          ParentFont = False
        end
        object Memo161: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 310.000000000000000000
          Top = 376.677180000000000000
          Width = 350.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Link."BBER_REQUIRED"]')
          ParentFont = False
        end
        object Memo162: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 310.000000000000000000
          Top = 331.677180000000000000
          Width = 350.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Link."KNG_NORM"]')
          ParentFont = False
        end
        object Memo163: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 310.000000000000000000
          Top = 346.677180000000000000
          Width = 350.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Link."BBER_NORM"]')
          ParentFont = False
        end
        object Memo164: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 310.000000000000000000
          Top = 301.677180000000000000
          Width = 350.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Link."SESR_NORM"]')
          ParentFont = False
        end
        object Memo165: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 310.000000000000000000
          Top = 316.677180000000000000
          Width = 350.000000000000000000
          Height = 15.000000000000000000
          DisplayFormat.FormatStr = '%2.6n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Link."KNG"]')
          ParentFont = False
        end
        object Memo166: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 310.000000000000000000
          Top = 271.677180000000000000
          Width = 350.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Link."SESR_INTERFERENCE"]')
          ParentFont = False
        end
        object Memo167: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 310.000000000000000000
          Top = 286.677180000000000000
          Width = 350.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Link."SESR"]')
          ParentFont = False
        end
        object Memo168: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 310.000000000000000000
          Top = 241.677180000000000000
          Width = 350.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Link."SESR_SUBREFRACTION"]')
          ParentFont = False
        end
        object Memo169: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 310.000000000000000000
          Top = 256.677180000000000000
          Width = 350.000000000000000000
          Height = 15.000000000000000000
          DisplayFormat.FormatStr = '%2.6n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Link."SESR_RAIN"]')
          ParentFont = False
        end
        object Memo170: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 310.000000000000000000
          Top = 226.677180000000000000
          Width = 350.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Link."MARGIN_DISTANCE"]')
          ParentFont = False
        end
        object Memo171: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 310.000000000000000000
          Top = 211.677180000000000000
          Width = 350.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Link."MARGIN_HEIGHT"]')
          ParentFont = False
        end
        object Memo172: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 485.000000000000000000
          Top = 105.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[antenna2."diameter"]')
          ParentFont = False
        end
        object Memo173: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 485.000000000000000000
          Top = 120.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[antenna2."polarization"]')
          ParentFont = False
        end
        object Memo174: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 310.000000000000000000
          Top = 151.677180000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd1."rx_level_dBm"]')
          ParentFont = False
        end
        object Memo175: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 485.000000000000000000
          Top = 151.677180000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd2."rx_level_dBm"]')
          ParentFont = False
        end
        object Memo176: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 310.000000000000000000
          Top = 166.677180000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd1."kratnostBySpace_name"]')
          ParentFont = False
        end
        object Memo177: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 485.000000000000000000
          Top = 166.677180000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd2."kratnostBySpace_name"]')
          ParentFont = False
        end
        object Memo178: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1119#1056#187#1056#1109#1057#8240#1056#176#1056#1169#1056#1108#1056#176)
          ParentFont = False
        end
        object Memo179: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 310.000000000000000000
          Top = 15.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[antenna1."height"]')
          ParentFont = False
        end
        object Memo180: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 485.000000000000000000
          Top = 15.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[antenna2."height"]')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 135.842610000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            'LINK ID')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 310.000000000000000000
          Top = 135.842610000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd1."LinkID"]')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 485.000000000000000000
          Top = 135.842610000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd2."LINKID"]')
          ParentFont = False
        end
      end
      object MasterData3: TfrxMasterData
        FillType = ftBrush
        Frame.Typ = []
        Height = 52.000000000000000000
        Top = 105.826840000000000000
        Width = 718.110700000000000000
        Child = frxReport_short_WGS.Child_Header
        DataSet = frxDBDataset_link
        DataSetName = 'Link'
        RowCount = 0
        object Memo7: TfrxMemoView
          Align = baWidth
          AllowVectorExport = True
          Width = 718.110700000000000000
          Height = 19.000000000000000000
          AutoWidth = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8 = (
            #1056#1115#1057#8218#1057#8225#1056#181#1057#8218' '#1056#1111#1056#1109' '#1057#1026#1056#176#1056#1169#1056#1105#1056#1109#1057#1026#1056#181#1056#187#1056#181#1056#8470#1056#1029#1056#1109#1056#1112#1057#1107' '#1056#1105#1056#1029#1057#8218#1056#181#1057#1026#1056#1030#1056#176#1056#187#1057#1107)
          ParentFont = False
        end
        object Linkname: TfrxMemoView
          Align = baWidth
          AllowVectorExport = True
          Top = 28.000000000000000000
          Width = 718.110700000000000000
          Height = 16.000000000000000000
          AutoWidth = True
          DataField = 'name'
          Frame.Typ = []
          Memo.UTF8 = (
            '[Link."name"]')
        end
      end
      object Child_profile: TfrxChild
        FillType = ftBrush
        Frame.Typ = []
        Height = 58.400000000000000000
        Top = 891.969080000000000000
        Width = 718.110700000000000000
        ToNRows = 0
        ToNRowsMode = rmCount
        object Picture_profile: TfrxPictureView
          Align = baWidth
          AllowVectorExport = True
          Top = 20.000000000000000000
          Width = 718.110700000000000000
          Height = 36.000000000000000000
          OnBeforePrint = 'Picture_profileOnBeforePrint'
          DataField = 'profile_img'
          DataSet = frxDBDataset_Link_img
          DataSetName = 'Link_img'
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Stretched = False
          HightQuality = True
          Transparent = False
          TransparentColor = clWhite
        end
        object Memo80: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          ShiftMode = smWhenOverlapped
          Width = 363.200000000000000000
          Height = 16.800000000000000000
          AutoWidth = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8 = (
            
              #1056#152#1056#183#1056#1109#1056#177#1057#1026#1056#176#1056#182#1056#181#1056#1029#1056#1105#1056#181' '#1056#1111#1057#1026#1056#1109#1057#8222#1056#1105#1056#187#1057#1039' '#1056#1111#1057#1026#1056#1105' '#1056#1029#1056#1109#1057#1026#1056#1112#1056#176#1056#187#1057#1034#1056#1029#1056#1109#1056 +
              #8470' '#1057#1026#1056#181#1057#8222#1057#1026#1056#176#1056#1108#1057#8224#1056#1105#1056#1105)
          ParentFont = False
        end
      end
      object Child_map: TfrxChild
        FillType = ftBrush
        Frame.Typ = []
        Height = 36.800000000000000000
        Top = 831.496600000000000000
        Width = 718.110700000000000000
        OnAfterCalcHeight = 'Child_mapOnAfterCalcHeight'
        OnBeforePrint = 'Child_mapOnBeforePrint'
        AllowSplit = True
        Child = frxReport_short_WGS.Child_profile
        ToNRows = 0
        ToNRowsMode = rmCount
        object Picture_map: TfrxPictureView
          Description = 'Map'
          Align = baWidth
          AllowVectorExport = True
          Width = 718.110700000000000000
          Height = 28.000000000000000000
          OnBeforePrint = 'Picture_mapOnBeforePrint'
          DataField = 'map_img'
          DataSet = frxDBDataset_Link_img
          DataSetName = 'Link_img'
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Stretched = False
          HightQuality = True
          Transparent = False
          TransparentColor = clWhite
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        Frame.Typ = []
        Height = 24.000000000000000000
        Top = 1012.914040000000000000
        Width = 718.110700000000000000
        object Page: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 638.110700000000000000
          Width = 80.000000000000000000
          Height = 16.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8 = (
            '[Page#]')
          ParentFont = False
        end
        object Memo138: TfrxMemoView
          Align = baWidth
          AllowVectorExport = True
          Width = 638.110700000000000000
          Height = 16.000000000000000000
          AutoWidth = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clSilver
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8 = (
            
              #1056#1115#1057#8218#1057#8225#1056#181#1057#8218' '#1056#1030#1057#8249#1056#1111#1056#1109#1056#187#1056#1029#1056#181#1056#1029' '#1056#1030' '#1056#1111#1057#1026#1056#1109#1056#1110#1057#1026#1056#176#1056#1112#1056#1112#1056#181' ONEPLAN RPLS-D' +
              'B Link')
          ParentFont = False
        end
      end
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        Frame.Typ = []
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        Height = 28.000000000000000000
        ParentFont = False
        Top = 18.897650000000000000
        Width = 718.110700000000000000
        object Linkcalc_method_name1: TfrxMemoView
          Align = baWidth
          AllowVectorExport = True
          Left = 141.600000000000000000
          Width = 576.510700000000000000
          Height = 16.000000000000000000
          AutoWidth = True
          DataField = 'calc_method_name'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clSilver
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8 = (
            '[Link."calc_method_name"]')
          ParentFont = False
        end
        object Memo137: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Width = 141.600000000000000000
          Height = 16.000000000000000000
          AutoWidth = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clSilver
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8 = (
            #1056#1114#1056#181#1057#8218#1056#1109#1056#1169#1056#1105#1056#1108#1056#176' '#1057#1026#1056#176#1057#1027#1057#8225#1056#181#1057#8218#1056#176': ')
          ParentFont = False
        end
      end
    end
  end
  object frxReport_MTS: TfrxReport
    Version = '6.2.11'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports3'
    PreviewOptions.AllowEdit = False
    PreviewOptions.Buttons = [pbPrint, pbExport, pbZoom, pbTools, pbNavigator, pbExportQuick, pbNoFullScreen]
    PreviewOptions.ThumbnailVisible = True
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = #1055#1086' '#1091#1084#1086#1083#1095#1072#1085#1080#1102
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 41730.469225335600000000
    ReportOptions.LastChange = 42795.633203159720000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      ''
      '         '
      ''
      ''
      'procedure frxReportOnStartReport(Sender: TfrxComponent);'
      'const'
      '  DEF_MAP_H = 800;'
      '  '
      '  DEF_H = 450;'
      '  DEF_OFFSET = 25;'
      ''
      '  DEF_TOP = 20;                                  '
      'begin'
      '//  Picture_map.Width:=200;                '
      '  Picture_map.Height     :=DEF_MAP_H;'
      '    '
      '  Picture_profile.Height :=DEF_H;                '
      '            '
      '  '
      '  Child_map.Height:=DEF_MAP_H;'
      ''
      ''
      ' // Picture_map.Dataset:='#39'Link_img'#39';      '
      
        ' // Picture_profile.Dataset:='#39'Link_img'#39';                        ' +
        '  '
      '                                                      '
      '               '
      '  Child_profile.Height:=DEF_H + DEF_OFFSET;              '
      '        '
      '      '
      'end;  '
      '             '
      '    '
      ''
      '  '
      '//------------------------------------------------------------'
      'function DegreeToStr (aValue: double): String;'
      '//------------------------------------------------------------'
      'const'
      '//  STR_BLPOINT_FORMAT1 = '#39'%d'#176'%.2d'#39#39'%s%1.2f'#39#39#39#39#39';'
      '  STR_BLPOINT_FORMAT1 = '#39'%d'#176' %.2d'#39#39' %1.2f'#39#39#39#39#39';'
      'var    '
      ' iDeg,iMin:integer; '
      ' eSec: Double;   '
      '           '
      'begin'
      '  geo_DecodeDegree_(aValue, iDeg,iMin,eSec);'
      ' '
      '  Result:=Format(STR_BLPOINT_FORMAT1, [iDeg, iMin, eSec]);'
      ''
      'end;'
      ''
      '       '
      ''
      '//------------------------------------------------------------'
      'function DegreeToStr_lat (aValue: double): String;'
      '//------------------------------------------------------------'
      'var '
      '  s: string;'
      'begin'
      '  if aValue>0 then s:='#39'N'#39' else s:='#39'S'#39';'
      ''
      '  aValue:=Abs(aValue);       '
      ''
      '  Result:=DegreeToStr (aValue) + s;'
      'end;'
      ''
      '//------------------------------------------------------------'
      'function DegreeToStr_lon (aValue: double): String;'
      '//------------------------------------------------------------'
      'var '
      '  s: string;'
      'begin'
      '   if aValue<0 then begin'
      '     s:='#39'W'#39';'
      '     aValue:=Abs(aValue);'
      '   end else'
      ''
      '   if aValue>180 then begin'
      '     s:='#39'W'#39';'
      '     aValue:=360 - aValue'
      '   end else'
      '     s:='#39'E'#39';     '
      ''
      '  Result:=DegreeToStr (aValue) + s;'
      'end;  '
      ''
      '        '
      ''
      '//------------------------------------------------------------'
      
        'procedure geo_DecodeDegree_old(aValue: Double; var aDeg,aMin:int' +
        'eger; var aSec:'
      '    Double);'
      '//------------------------------------------------------------'
      'const'
      ' EXP_ = 0.000000001;'
      '     //97,033333333'
      'var'
      '  bIsNegative: Boolean;'
      '  e: Double;'
      '  eMinutes: Double;'
      '  eSec: Double;'
      '  eSeconds: Double;'
      'begin'
      '  bIsNegative := aValue<0;'
      ''
      '  aValue :=Abs(aValue);'
      ''
      '  aDeg:=Trunc(aValue);'
      ''
      ' // e:=Abs(aValue)*60;'
      ''
      '//  , Frac(X) = X - Int(X).'
      ''
      '  aMin:=Trunc(Frac(aValue)*60);'
      ''
      '//  Result.Sec:=Frac(Frac(Abs(aValue))*60)*60;'
      '  e:= (aValue - Trunc(aValue)) * 60;'
      '  aMin:=Trunc(e);'
      ''
      ' eMinutes := Trunc (e);'
      ' eSeconds := (e - Trunc (e)) * 60 ;'
      ''
      ''
      '  aSec:=(((aValue - aDeg) *60) - aMin) * 60;'
      ''
      '  if (aSec - 60) > EXP_ then'
      '  begin'
      '    Inc(aMin);'
      '    aSec := 0;'
      '  end;'
      ''
      ''
      '  if bIsNegative then'
      '    aDeg:=-aDeg;'
      '          '
      'end;'
      ''
      ''
      ' //------------------------------------------------------------'
      
        'procedure geo_DecodeDegree_(aValue: Double; var aDeg,aMin:intege' +
        'r; var aSec:'
      '    Double);'
      '//------------------------------------------------------------'
      'var'
      '  bIsNegative: Boolean;'
      '  e: Double;'
      '  eSec: Double;'
      ''
      'begin'
      '  bIsNegative := aValue<0;'
      ''
      '  aValue :=Abs(aValue);'
      ''
      '  aDeg:=Trunc(aValue);'
      ''
      '  aMin:=Trunc(Frac(aValue)*60);'
      ''
      ''
      '//  e:= (aValue - Trunc(aValue));'
      '  e:= (aValue - Trunc(aValue)) * 60;'
      ''
      ''
      '  aMin:=Trunc(e);'
      '  aSec:=(((aValue - aDeg) *60) - aMin) * 60;'
      ''
      '   while aSec>(60-0.1) do'
      '  begin'
      '    aMin:=aMin+1;'
      '    aSec:=aSec-60;'
      '  end;    '
      ''
      '  while aMin>=60 do'
      '  begin'
      '      aMin:=aMin-60;'
      '      aDeg:=aDeg+1;'
      '  end;  '
      ''
      '  if aSec<0 then'
      '     aSec:=0;'
      '    '
      ''
      ''
      '  if bIsNegative then'
      '    aDeg:=-aDeg;'
      ''
      ''
      ''
      'end;'
      ''
      '     '
      ''
      '       '
      'begin'
      ''
      'end.    ')
    OnStartReport = 'frxReportOnStartReport'
    Left = 686
    Top = 20
    Datasets = <
      item
        DataSet = frxDBDataset_Antenna1
        DataSetName = 'Antenna1'
      end
      item
        DataSet = frxDBDataset_Antenna2
        DataSetName = 'Antenna2'
      end
      item
        DataSet = frxDBDataset_group
        DataSetName = 'group'
      end
      item
        DataSet = frxDBDataset_Items
        DataSetName = 'Items'
      end
      item
        DataSet = frxDBDataset_link
        DataSetName = 'Link'
      end
      item
        DataSet = frxDBDataset_Link_img
        DataSetName = 'Link_img'
      end
      item
        DataSet = frxDBDataset_LinkEnd1
        DataSetName = 'LinkEnd1'
      end
      item
        DataSet = frxDBDataset_LinkEnd2
        DataSetName = 'LinkEnd2'
      end
      item
        DataSet = dmLinkLine_report.frxDBDataset_LinkLine
        DataSetName = 'LinkLine'
      end
      item
        DataSet = dmLinkLine_report.frxDBDataset_LinkLine_img
        DataSetName = 'LinkLine_img'
      end
      item
        DataSet = frxDBDataset_Profile_items
        DataSetName = 'Profile'
      end
      item
        DataSet = frxDBDataset_Property1
        DataSetName = 'Site1'
      end
      item
        DataSet = frxDBDataset_Property2
        DataSetName = 'Site2'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 9.260416666666670000
      BottomMargin = 10.583333333333300000
      Frame.Typ = []
      LargeDesignHeight = True
      OnBeforePrint = 'Page1OnBeforePrint'
      object Child_Header: TfrxChild
        FillType = ftBrush
        Frame.Typ = []
        Height = 187.000000000000000000
        Top = 181.417440000000000000
        Width = 718.110700000000000000
        Child = frxReport_MTS.Child_Summary
        ToNRows = 0
        ToNRowsMode = rmCount
        object Memo105: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 478.110700000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1119#1056#187#1056#1109#1057#8240#1056#176#1056#1169#1056#1108#1056#176)
          ParentFont = False
        end
        object Memo106: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 588.110700000000000000
          Width = 130.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Site2."name"]')
          ParentFont = False
        end
        object Memo108: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 478.110700000000000000
          Top = 30.000000000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1113#1056#1109#1056#1109#1057#1026#1056#1169#1056#1105#1056#1029#1056#176#1057#8218#1057#8249' Pulkovo')
          ParentFont = False
        end
        object Memo109: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 478.110700000000000000
          Top = 90.000000000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#8217#1057#8249#1057#1027#1056#1109#1057#8218#1056#176' '#1056#1111#1056#1109#1056#1169#1056#1030#1056#181#1057#1027#1056#176', m')
          ParentFont = False
        end
        object Memo110: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 478.110700000000000000
          Top = 105.000000000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1106#1056#183#1056#1105#1056#1112#1057#1107#1057#8218)
          ParentFont = False
        end
        object Memo111: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 478.110700000000000000
          Top = 120.000000000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1032#1056#1110#1056#1109#1056#187' '#1056#1112#1056#181#1057#1027#1057#8218#1056#176)
          ParentFont = False
        end
        object Memo112: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 478.110700000000000000
          Top = 135.000000000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#8221#1056#1105#1056#176#1056#1112#1056#181#1057#8218#1057#1026' '#1056#176#1056#1029#1057#8218#1056#181#1056#1029#1056#1029#1057#8249)
          ParentFont = False
        end
        object Memo114: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 588.110700000000000000
          Top = 30.000000000000000000
          Width = 130.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[DegreeToStr (<Site2."lat">)]')
          ParentFont = False
        end
        object Memo115: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 588.110700000000000000
          Top = 90.000000000000000000
          Width = 130.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[antenna2."height"]')
          ParentFont = False
        end
        object Memo116: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 588.110700000000000000
          Top = 105.000000000000000000
          Width = 130.000000000000000000
          Height = 15.000000000000000000
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."azimuth2"]')
          ParentFont = False
        end
        object Memo117: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 588.110700000000000000
          Top = 135.000000000000000000
          Width = 130.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Antenna2."diameter"]')
          ParentFont = False
        end
        object Memo118: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 588.110700000000000000
          Top = 45.000000000000000000
          Width = 130.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[DegreeToStr (<Site2."lon">)]')
          ParentFont = False
        end
        object Memo130: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 478.110700000000000000
          Top = 45.000000000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 588.110700000000000000
          Top = 120.000000000000000000
          Width = 130.000000000000000000
          Height = 15.000000000000000000
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Antenna2."tilt"]')
          ParentFont = False
        end
        object Memo66: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 478.110700000000000000
          Top = 150.000000000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1032#1057#1027#1056#1105#1056#187#1056#181#1056#1029#1056#1105#1056#181' '#1056#176#1056#1029#1057#8218#1056#181#1056#1029#1056#1029#1057#8249)
          ParentFont = False
        end
        object Memo99: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 588.110700000000000000
          Top = 150.000000000000000000
          Width = 130.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Antenna2."gain"]')
          ParentFont = False
        end
        object Memo43: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1119#1056#187#1056#1109#1057#8240#1056#176#1056#1169#1056#1108#1056#176)
          ParentFont = False
        end
        object Memo_left_1: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 110.000000000000000000
          Width = 125.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Site1."name"]')
          ParentFont = False
        end
        object Memo68: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 30.000000000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1113#1056#1109#1056#1109#1057#1026#1056#1169#1056#1105#1056#1029#1056#176#1057#8218#1057#8249' Pulkovo')
          ParentFont = False
        end
        object Memo70: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 90.000000000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#8217#1057#8249#1057#1027#1056#1109#1057#8218#1056#176' '#1056#1111#1056#1109#1056#1169#1056#1030#1056#181#1057#1027#1056#176', m')
          ParentFont = False
        end
        object Memo72: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 105.000000000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1106#1056#183#1056#1105#1056#1112#1057#1107#1057#8218)
          ParentFont = False
        end
        object Memo74: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 120.000000000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1032#1056#1110#1056#1109#1056#187' '#1056#1112#1056#181#1057#1027#1057#8218#1056#176)
          ParentFont = False
        end
        object Memo100: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 135.000000000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#8221#1056#1105#1056#176#1056#1112#1056#181#1057#8218#1057#1026' '#1056#176#1056#1029#1057#8218#1056#181#1056#1029#1056#1029#1057#8249)
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 110.000000000000000000
          Top = 30.000000000000000000
          Width = 125.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[DegreeToStr (<Site1."lat">)]')
          ParentFont = False
        end
        object Memo1: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 110.000000000000000000
          Top = 90.000000000000000000
          Width = 125.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[antenna1."height"]')
          ParentFont = False
        end
        object Memo95: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 110.000000000000000000
          Top = 105.000000000000000000
          Width = 125.000000000000000000
          Height = 15.000000000000000000
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."azimuth1"]')
          ParentFont = False
        end
        object Memo97: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 110.000000000000000000
          Top = 135.000000000000000000
          Width = 125.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[antenna1."diameter"]')
          ParentFont = False
        end
        object Memo98: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 110.000000000000000000
          Top = 45.000000000000000000
          Width = 125.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[DegreeToStr (<Site1."lon">)]')
          ParentFont = False
        end
        object Memo96: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 110.000000000000000000
          Top = 120.000000000000000000
          Width = 125.000000000000000000
          Height = 15.000000000000000000
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Antenna1."tilt"]')
          ParentFont = False
        end
        object Memo121: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 45.000000000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 150.000000000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1032#1057#1027#1056#1105#1056#187#1056#181#1056#1029#1056#1105#1056#181' '#1056#176#1056#1029#1057#8218#1056#181#1056#1029#1056#1029#1057#8249)
          ParentFont = False
        end
        object Memo30: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 110.000000000000000000
          Top = 150.000000000000000000
          Width = 125.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[antenna1."gain"]')
          ParentFont = False
        end
        object Memo45: TfrxMemoView
          AllowVectorExport = True
          Left = 243.220470000000000000
          Top = 15.000000000000000000
          Width = 144.204726490000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#8221#1056#187#1056#1105#1056#1029#1056#176' '#1056#1105#1056#1029#1057#8218#1056#181#1057#1026#1056#1030#1056#176#1056#187#1056#176', km')
          ParentFont = False
        end
        object Memo47: TfrxMemoView
          AllowVectorExport = True
          Left = 243.220470000000000000
          Top = 30.000000000000000000
          Width = 144.204726490000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1032#1057#1026#1056#1109#1056#1030#1056#181#1056#1029#1057#1034' '#1057#1027#1056#1105#1056#1110#1056#1029#1056#176#1056#187#1056#176', dBm')
          ParentFont = False
        end
        object Memo49: TfrxMemoView
          AllowVectorExport = True
          Left = 243.220470000000000000
          Top = 45.000000000000000000
          Width = 144.204726490000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#160#1056#176#1056#177#1056#1109#1057#8225#1056#176#1057#1039' '#1057#8225#1056#176#1057#1027#1057#8218#1056#1109#1057#8218#1056#176', GHz')
          ParentFont = False
        end
        object Memo51: TfrxMemoView
          AllowVectorExport = True
          Left = 243.220470000000000000
          Top = 60.000000000000000000
          Width = 144.204726490000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1114#1056#1109#1057#8240#1056#1029#1056#1109#1057#1027#1057#8218#1057#1034' '#1056#1111#1056#181#1057#1026#1056#181#1056#1169#1056#176#1057#8218#1057#8225#1056#1105#1056#1108#1056#176', dBm')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Left = 243.220470000000000000
          Top = 75.000000000000000000
          Width = 144.204726490000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1038#1056#1108#1056#1109#1057#1026#1056#1109#1057#1027#1057#8218#1057#1034' '#1056#1111#1056#181#1057#1026#1056#181#1056#1169#1056#176#1057#8225#1056#1105', Mbit/s')
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 243.220470000000000000
          Top = 90.000000000000000000
          Width = 144.204726490000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1118#1056#1105#1056#1111' '#1056#1109#1056#177#1056#1109#1057#1026#1057#1107#1056#1169#1056#1109#1056#1030#1056#176#1056#1029#1056#1105#1057#1039)
          ParentFont = False
        end
        object Memo123: TfrxMemoView
          AllowVectorExport = True
          Left = 387.425196490000000000
          Top = 30.000000000000000000
          Width = 74.905973510000000000
          Height = 15.000000000000000000
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."rx_level_dBm"]')
          ParentFont = False
        end
        object Memo124: TfrxMemoView
          AllowVectorExport = True
          Left = 387.425196490000000000
          Top = 75.000000000000000000
          Width = 74.905973510000000000
          Height = 15.000000000000000000
          DataField = 'bitrate_Mbps'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd1."bitrate_Mbps"]')
          ParentFont = False
        end
        object Memo125: TfrxMemoView
          AllowVectorExport = True
          Left = 387.425196490000000000
          Top = 90.000000000000000000
          Width = 74.905973510000000000
          Height = 15.000000000000000000
          DataField = 'LinkEndType_name'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd1."LinkEndType_name"]')
          ParentFont = False
        end
        object Memo127: TfrxMemoView
          AllowVectorExport = True
          Left = 387.425196490000000000
          Top = 45.000000000000000000
          Width = 74.905973510000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[FloatToStr( <LinkEnd1."tx_freq_MHz"> / 1000 )]')
          ParentFont = False
        end
        object Memo128: TfrxMemoView
          AllowVectorExport = True
          Left = 387.425196490000000000
          Top = 15.000000000000000000
          Width = 74.905973510000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."length_km"]')
          ParentFont = False
        end
        object Memo126: TfrxMemoView
          AllowVectorExport = True
          Left = 387.425196490000000000
          Top = 60.000000000000000000
          Width = 74.905973510000000000
          Height = 15.000000000000000000
          DataField = 'power_dBm'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd1."power_dBm"]')
          ParentFont = False
        end
        object Memo42: TfrxMemoView
          AllowVectorExport = True
          Left = 243.220470000000000000
          Width = 144.204726490000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1119#1057#1026#1056#1105#1056#183#1056#1029#1056#176#1056#1108' '#1056#1111#1057#1026#1056#1105#1056#1110#1056#1109#1056#1169#1056#1029#1056#1109#1057#1027#1057#8218#1056#1105)
          ParentFont = False
        end
        object Memo122: TfrxMemoView
          AllowVectorExport = True
          Left = 387.425196490000000000
          Width = 74.905973510000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."status_str"]')
          ParentFont = False
        end
        object Memo134: TfrxMemoView
          AllowVectorExport = True
          Left = 243.220470000000000000
          Top = 105.000000000000000000
          Width = 144.204726490000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#8212#1056#176#1056#1111#1056#176#1057#1027' '#1056#1029#1056#176' '#1056#183#1056#176#1056#1112#1056#1105#1057#1026#1056#176#1056#1029#1056#1105#1057#1039', dB')
          ParentFont = False
        end
        object Memo135: TfrxMemoView
          AllowVectorExport = True
          Left = 387.425196490000000000
          Top = 105.000000000000000000
          Width = 74.905973510000000000
          Height = 15.000000000000000000
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."fade_margin_dB"]')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 60.000000000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1113#1056#1109#1056#1109#1057#1026#1056#1169#1056#1105#1056#1029#1056#176#1057#8218#1057#8249' WGS')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 110.000000000000000000
          Top = 60.000000000000000000
          Width = 125.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[DegreeToStr (<Site1."lat_WGS">)]')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 110.000000000000000000
          Top = 75.000000000000000000
          Width = 125.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[DegreeToStr (<Site1."lon_WGS">)]')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 75.000000000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 478.110700000000000000
          Top = 60.000000000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1113#1056#1109#1056#1109#1057#1026#1056#1169#1056#1105#1056#1029#1056#176#1057#8218#1057#8249' WGS')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 588.110700000000000000
          Top = 60.000000000000000000
          Width = 130.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[DegreeToStr (<Site2."lat_wgs">)]')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 588.110700000000000000
          Top = 75.000000000000000000
          Width = 130.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[DegreeToStr (<Site2."lon_wgs">)]')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 478.110700000000000000
          Top = 75.000000000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          ParentFont = False
        end
        object Memo26: TfrxMemoView
          AllowVectorExport = True
          Left = 245.669450000000000000
          Top = 132.283550000000000000
          Width = 144.881880000000000000
          Height = 15.000000000000000000
          DataSet = frxDBDataset_link
          DataSetName = 'Link'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1114#1056#1109#1056#1169#1057#1107#1056#187#1057#1039#1057#8224#1056#1105#1057#1039)
          ParentFont = False
        end
        object Memo27: TfrxMemoView
          AllowVectorExport = True
          Left = 392.441411100000000000
          Top = 132.283550000000000000
          Width = 71.811026060000000000
          Height = 15.000000000000000000
          DataSet = frxDBDataset_link
          DataSetName = 'Link'
          DisplayFormat.FormatStr = '%2.2f'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd1."modulation_type"]')
          ParentFont = False
        end
        object Memo28: TfrxMemoView
          AllowVectorExport = True
          Left = 245.669450000000000000
          Top = 147.401670000000000000
          Width = 144.881880000000000000
          Height = 15.000000000000000000
          DataSet = frxDBDataset_link
          DataSetName = 'Link'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1025#1056#1105#1057#1026#1056#1105#1056#1029#1056#176' '#1056#1111#1056#1109#1056#187#1056#1109#1057#1027#1057#8249', MHz')
          ParentFont = False
        end
        object Memo29: TfrxMemoView
          AllowVectorExport = True
          Left = 392.441411100000000000
          Top = 147.401670000000000000
          Width = 71.811026060000000000
          Height = 15.000000000000000000
          DataSet = frxDBDataset_link
          DataSetName = 'Link'
          DisplayFormat.FormatStr = '%2.0f'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd1."channel_width_MHz"]')
          ParentFont = False
        end
      end
      object Child_Summary: TfrxChild
        FillType = ftBrush
        Frame.Typ = []
        Height = 456.000000000000000000
        Top = 393.071120000000000000
        Width = 718.110700000000000000
        Child = frxReport_MTS.Child_map
        ToNRows = 0
        ToNRowsMode = rmCount
        object Memo3: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 15.000000000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#8217#1057#8249#1057#1027#1056#1109#1057#8218#1056#176', m')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 30.000000000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1106#1056#183#1056#1105#1056#1112#1057#1107#1057#8218' ')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 45.000000000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1118#1056#1105#1056#1111' '#1056#1109#1056#177#1056#1109#1057#1026#1057#1107#1056#1169#1056#1109#1056#1030#1056#176#1056#1029#1056#1105#1057#1039' ')
          ParentFont = False
        end
        object Memo77: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 60.000000000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#167#1056#176#1057#1027#1057#8218#1056#1109#1057#8218#1056#176' '#1056#1111#1056#181#1057#1026#1056#181#1056#1169#1056#176#1057#8225#1056#1105', MHz')
          ParentFont = False
        end
        object Memo78: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 75.000000000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1038#1056#1108#1056#1109#1057#1026#1056#1109#1057#1027#1057#8218#1057#1034' '#1056#1111#1056#181#1057#1026#1056#181#1056#1169#1056#176#1057#8225#1056#1105', Mbit/s')
          ParentFont = False
        end
        object Memo79: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 90.000000000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1114#1056#1109#1057#8240#1056#1029#1056#1109#1057#1027#1057#8218#1057#1034', dBm')
          ParentFont = False
        end
        object Memo81: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 105.000000000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#8221#1056#1105#1056#176#1056#1112#1056#181#1057#8218#1057#1026' '#1056#176#1056#1029#1057#8218#1056#181#1056#1029#1056#1029#1057#8249', m')
          ParentFont = False
        end
        object Memo82: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 150.000000000000100000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            
              #1056#1038#1057#1026#1056#181#1056#1169#1056#1029#1057#1039#1057#1039' '#1056#1112#1056#1109#1057#8240#1056#1029#1056#1109#1057#1027#1057#8218#1057#1034' '#1056#1029#1056#176' '#1056#1030#1057#8230#1056#1109#1056#1169#1056#181' '#1056#1111#1057#1026#1056#1105#1056#181#1056#1112#1056#1029#1056#1105#1056#1108 +
              #1056#176', dBm')
          ParentFont = False
        end
        object Memo83: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 120.000000000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1119#1056#1109#1056#187#1057#1039#1057#1026#1056#1105#1056#183#1056#176#1057#8224#1056#1105#1057#1039' ')
          ParentFont = False
        end
        object Memo89: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 165.000000000000100000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1106#1056#1111#1056#1111#1056#176#1057#1026#1056#176#1057#8218#1056#1029#1056#1109#1056#181' '#1057#1026#1056#181#1056#183#1056#181#1057#1026#1056#1030#1056#1105#1057#1026#1056#1109#1056#1030#1056#176#1056#1029#1056#1105#1056#181' ')
          ParentFont = False
        end
        object Memo90: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 180.000000000000100000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1119#1056#1109#1057#1026#1056#1109#1056#1110' '#1056#1111#1057#1026#1056#1105' BER=10-3 ')
          ParentFont = False
        end
        object Memo91: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 210.000000000000100000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1114#1056#1105#1056#1029#1056#1105#1056#1112#1056#176#1056#187#1057#1034#1056#1029#1057#8249#1056#8470' '#1056#1109#1057#8218#1056#1029#1056#1109#1057#1027#1056#1105#1057#8218#1056#181#1056#187#1057#1034#1056#1029#1057#8249#1056#8470' '#1056#1111#1057#1026#1056#1109#1057#1027#1056#1030#1056#181#1057#8218)
          ParentFont = False
        end
        object Memo92: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 195.000000000000100000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1119#1056#1109#1057#1026#1056#1109#1056#1110' '#1056#1111#1057#1026#1056#1105' BER=10-6 ')
          ParentFont = False
        end
        object Memo93: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 225.000000000000100000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1119#1056#1109#1056#187#1056#1109#1056#182#1056#181#1056#1029#1056#1105#1056#181' '#1057#8218#1056#1109#1057#8225#1056#1108#1056#1105' '#1056#1111#1057#1026#1056#1109#1057#1027#1056#1030#1056#181#1057#8218#1056#176', km ')
          ParentFont = False
        end
        object Memo101: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 240.000000000000100000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            'SESR '#1057#1027#1057#1107#1056#177#1057#1026#1056#181#1057#8222#1056#1108#1057#8224#1056#1105#1056#1109#1056#1029#1056#1029#1056#176#1057#1039' '#1057#1027#1056#1109#1057#1027#1057#8218#1056#176#1056#1030#1056#187#1057#1039#1057#1035#1057#8240#1056#176#1057#1039', % ')
          ParentFont = False
        end
        object Memo103: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 255.000000000000100000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            'SESR '#1056#1169#1056#1109#1056#182#1056#1169#1056#181#1056#1030#1056#176#1057#1039' '#1057#1027#1056#1109#1057#1027#1057#8218#1056#176#1056#1030#1056#187#1057#1039#1057#1035#1057#8240#1056#176#1057#1039', % ')
          ParentFont = False
        end
        object Memo104: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 270.000000000000100000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            
              'SESR '#1056#1105#1056#1029#1057#8218#1056#181#1057#1026#1057#8222#1056#181#1057#1026#1056#181#1056#1029#1057#8224#1056#1105#1056#1109#1056#1029#1056#1029#1056#176#1057#1039' '#1057#1027#1056#1109#1057#1027#1057#8218#1056#176#1056#1030#1056#187#1057#1039#1057#1035#1057#8240#1056#176#1057#1039 +
              ', % ')
          ParentFont = False
        end
        object Memo107: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 285.000000000000100000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            
              #1056#152#1057#8218#1056#1109#1056#1110#1056#1109#1056#1030#1057#8249#1056#8470' '#1056#1111#1056#1109#1056#1108#1056#176#1056#183#1056#176#1057#8218#1056#181#1056#187#1057#1034' '#1056#1108#1056#176#1057#8225#1056#181#1057#1027#1057#8218#1056#1030#1056#176' '#1056#1111#1056#1109' '#1056#1109#1057#8364 +
              #1056#1105#1056#177#1056#1108#1056#176#1056#1112' SESR, % ')
          ParentFont = False
        end
        object Memo113: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 300.000000000000100000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            
              #1056#1116#1056#1109#1057#1026#1056#1112#1056#1105#1057#1026#1057#1107#1056#181#1056#1112#1056#1109#1056#181' '#1056#183#1056#1029#1056#176#1057#8225#1056#181#1056#1029#1056#1105#1056#181' SESR '#1056#1029#1056#176' '#1056#1105#1056#1029#1057#8218#1056#181#1057#1026#1056#1030#1056#176 +
              #1056#187#1056#181', % ')
          ParentFont = False
        end
        object Memo119: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 315.000000000000100000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            
              #1056#152#1057#8218#1056#1109#1056#1110#1056#1109#1056#1030#1057#8249#1056#8470' '#1056#1111#1056#1109#1056#1108#1056#176#1056#183#1056#176#1057#8218#1056#181#1056#187#1057#1034' '#1056#1029#1056#181#1056#1110#1056#1109#1057#8218#1056#1109#1056#1030#1056#1029#1056#1109#1057#1027#1057#8218#1056#1105' '#1056 +
              #1113#1056#1029#1056#1110', %')
          ParentFont = False
        end
        object Memo120: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 330.000000000000100000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            
              #1056#1116#1056#1109#1057#1026#1056#1112#1056#1105#1057#1026#1057#1107#1056#181#1056#1112#1056#1109#1056#181' '#1056#183#1056#1029#1056#176#1057#8225#1056#181#1056#1029#1056#1105#1056#181' '#1056#1113#1056#1029#1056#1110' '#1056#1029#1056#176' '#1056#1105#1056#1029#1057#8218#1056#181#1057#1026#1056#1030 +
              #1056#176#1056#187#1056#181', % ')
          ParentFont = False
        end
        object Memo129: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 390.000000000000100000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            'ESR '#1057#8218#1057#1026#1056#181#1056#177', % ')
          ParentFont = False
        end
        object Memo132: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 345.000000000000100000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            'BBER '#1056#1029#1056#1109#1057#1026#1056#1112', %')
          ParentFont = False
        end
        object Memo133: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 375.000000000000100000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            'BBER '#1057#8218#1057#1026#1056#181#1056#177', % ')
          ParentFont = False
        end
        object Memo136: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 360.000000000000100000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            'ESR '#1056#1029#1056#1109#1057#1026#1056#1112', % ')
          ParentFont = False
        end
        object Memo139: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 405.000000000000100000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1119#1057#1026#1056#1105#1056#1110#1056#1109#1056#1169#1056#1029#1056#1109#1057#1027#1057#8218#1057#1034' ')
          ParentFont = False
        end
        object Memo140: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 310.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Site1."name"]')
          ParentFont = False
        end
        object Memo141: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 485.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Site2."name"]')
          ParentFont = False
        end
        object Memo142: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 310.000000000000000000
          Top = 30.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."azimuth1"]')
          ParentFont = False
        end
        object Memo143: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 485.000000000000000000
          Top = 30.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."azimuth2"]')
          ParentFont = False
        end
        object Memo144: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 310.000000000000000000
          Top = 180.000000000000100000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd1."threshold_BER_3"]')
          ParentFont = False
        end
        object Memo145: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 310.000000000000000000
          Top = 195.000000000000100000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd1."threshold_BER_6"]')
          ParentFont = False
        end
        object Memo146: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 310.000000000000000000
          Top = 105.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[antenna1."diameter"]')
          ParentFont = False
        end
        object Memo147: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 310.000000000000000000
          Top = 120.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[antenna1."polarization"]')
          ParentFont = False
        end
        object Memo148: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 310.000000000000000000
          Top = 75.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd1."bitrate_Mbps"]')
          ParentFont = False
        end
        object Memo149: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 310.000000000000000000
          Top = 90.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd1."power_dBm"]')
          ParentFont = False
        end
        object Memo150: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 310.000000000000000000
          Top = 45.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd1."LinkEndType_name"]')
          ParentFont = False
        end
        object Memo151: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 310.000000000000000000
          Top = 60.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd1."tx_freq_MHz"]')
          ParentFont = False
        end
        object Memo152: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 485.000000000000000000
          Top = 45.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd2."LinkEndType_name"]')
          ParentFont = False
        end
        object Memo153: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 485.000000000000000000
          Top = 60.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd2."tx_freq_MHz"]')
          ParentFont = False
        end
        object Memo154: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 485.000000000000000000
          Top = 75.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd2."bitrate_Mbps"]')
          ParentFont = False
        end
        object Memo155: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 485.000000000000000000
          Top = 90.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd2."power_dBm"]')
          ParentFont = False
        end
        object Memo156: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 485.000000000000000000
          Top = 180.000000000000100000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd2."threshold_BER_3"]')
          ParentFont = False
        end
        object Memo157: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 485.000000000000000000
          Top = 195.000000000000100000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd2."threshold_BER_6"]')
          ParentFont = False
        end
        object Memo158: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 310.000000000000000000
          Top = 390.000000000000100000
          Width = 350.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Link."ESR_REQUIRED"]')
          ParentFont = False
        end
        object Memo159: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 310.000000000000000000
          Top = 405.000000000000100000
          Width = 350.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Link."status_str"]')
          ParentFont = False
        end
        object Memo160: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 310.000000000000000000
          Top = 360.000000000000100000
          Width = 350.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Link."ESR_NORM"]')
          ParentFont = False
        end
        object Memo161: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 310.000000000000000000
          Top = 375.000000000000100000
          Width = 350.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Link."BBER_REQUIRED"]')
          ParentFont = False
        end
        object Memo162: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 310.000000000000000000
          Top = 330.000000000000100000
          Width = 350.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Link."KNG_NORM"]')
          ParentFont = False
        end
        object Memo163: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 310.000000000000000000
          Top = 345.000000000000100000
          Width = 350.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Link."BBER_NORM"]')
          ParentFont = False
        end
        object Memo164: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 310.000000000000000000
          Top = 300.000000000000100000
          Width = 350.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Link."SESR_NORM"]')
          ParentFont = False
        end
        object Memo165: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 310.000000000000000000
          Top = 315.000000000000100000
          Width = 350.000000000000000000
          Height = 15.000000000000000000
          DisplayFormat.FormatStr = '%2.6n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Link."KNG"]')
          ParentFont = False
        end
        object Memo166: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 310.000000000000000000
          Top = 270.000000000000100000
          Width = 350.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Link."SESR_INTERFERENCE"]')
          ParentFont = False
        end
        object Memo167: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 310.000000000000000000
          Top = 285.000000000000100000
          Width = 350.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Link."SESR"]')
          ParentFont = False
        end
        object Memo168: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 310.000000000000000000
          Top = 240.000000000000100000
          Width = 350.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Link."SESR_SUBREFRACTION"]')
          ParentFont = False
        end
        object Memo169: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 310.000000000000000000
          Top = 255.000000000000100000
          Width = 350.000000000000000000
          Height = 15.000000000000000000
          DisplayFormat.FormatStr = '%2.6n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Link."SESR_RAIN"]')
          ParentFont = False
        end
        object Memo170: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 310.000000000000000000
          Top = 225.000000000000100000
          Width = 350.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Link."MARGIN_DISTANCE"]')
          ParentFont = False
        end
        object Memo171: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 310.000000000000000000
          Top = 210.000000000000100000
          Width = 350.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Link."MARGIN_HEIGHT"]')
          ParentFont = False
        end
        object Memo172: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 485.000000000000000000
          Top = 105.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[antenna2."diameter"]')
          ParentFont = False
        end
        object Memo173: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 485.000000000000000000
          Top = 120.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[antenna2."polarization"]')
          ParentFont = False
        end
        object Memo174: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 310.000000000000000000
          Top = 150.000000000000100000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd1."rx_level_dBm"]')
          ParentFont = False
        end
        object Memo175: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 485.000000000000000000
          Top = 150.000000000000100000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd2."rx_level_dBm"]')
          ParentFont = False
        end
        object Memo176: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 310.000000000000000000
          Top = 165.000000000000100000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd1."kratnostBySpace_name"]')
          ParentFont = False
        end
        object Memo177: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 485.000000000000000000
          Top = 165.000000000000100000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd2."kratnostBySpace_name"]')
          ParentFont = False
        end
        object Memo178: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1119#1056#187#1056#1109#1057#8240#1056#176#1056#1169#1056#1108#1056#176)
          ParentFont = False
        end
        object Memo179: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 310.000000000000000000
          Top = 15.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[antenna1."height"]')
          ParentFont = False
        end
        object Memo180: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 485.000000000000000000
          Top = 15.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[antenna2."height"]')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 135.000000000000100000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            'LINK  ID')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 310.000000000000000000
          Top = 135.000000000000100000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Linkend1."LINKID"]')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 485.000000000000000000
          Top = 135.000000000000100000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Linkend2."LINKID"]')
          ParentFont = False
        end
      end
      object MasterData3: TfrxMasterData
        FillType = ftBrush
        Frame.Typ = []
        Height = 52.000000000000000000
        Top = 105.826840000000000000
        Width = 718.110700000000000000
        Child = frxReport_MTS.Child_Header
        DataSet = frxDBDataset_link
        DataSetName = 'Link'
        RowCount = 0
        object Memo7: TfrxMemoView
          Align = baWidth
          AllowVectorExport = True
          Width = 718.110700000000000000
          Height = 19.000000000000000000
          AutoWidth = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8 = (
            #1056#1115#1057#8218#1057#8225#1056#181#1057#8218' '#1056#1111#1056#1109' '#1057#1026#1056#176#1056#1169#1056#1105#1056#1109#1057#1026#1056#181#1056#187#1056#181#1056#8470#1056#1029#1056#1109#1056#1112#1057#1107' '#1056#1105#1056#1029#1057#8218#1056#181#1057#1026#1056#1030#1056#176#1056#187#1057#1107)
          ParentFont = False
        end
        object Linkname: TfrxMemoView
          Align = baWidth
          AllowVectorExport = True
          Top = 28.000000000000000000
          Width = 718.110700000000000000
          Height = 16.000000000000000000
          AutoWidth = True
          DataField = 'name'
          Frame.Typ = []
          Memo.UTF8 = (
            '[Link."name"]')
        end
      end
      object Child_profile: TfrxChild
        FillType = ftBrush
        Frame.Typ = []
        Height = 58.400000000000000000
        Top = 933.543910000000000000
        Width = 718.110700000000000000
        ToNRows = 0
        ToNRowsMode = rmCount
        object Picture_profile: TfrxPictureView
          Align = baWidth
          AllowVectorExport = True
          Top = 20.000000000000000000
          Width = 718.110700000000000000
          Height = 36.000000000000000000
          OnBeforePrint = 'Picture_profileOnBeforePrint'
          DataField = 'profile_img'
          DataSet = frxDBDataset_Link_img
          DataSetName = 'Link_img'
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Stretched = False
          HightQuality = True
          Transparent = False
          TransparentColor = clWhite
        end
        object Memo80: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          ShiftMode = smWhenOverlapped
          Width = 363.200000000000000000
          Height = 16.800000000000000000
          AutoWidth = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8 = (
            
              #1056#152#1056#183#1056#1109#1056#177#1057#1026#1056#176#1056#182#1056#181#1056#1029#1056#1105#1056#181' '#1056#1111#1057#1026#1056#1109#1057#8222#1056#1105#1056#187#1057#1039' '#1056#1111#1057#1026#1056#1105' '#1056#1029#1056#1109#1057#1026#1056#1112#1056#176#1056#187#1057#1034#1056#1029#1056#1109#1056 +
              #8470' '#1057#1026#1056#181#1057#8222#1057#1026#1056#176#1056#1108#1057#8224#1056#1105#1056#1105)
          ParentFont = False
        end
      end
      object Child_map: TfrxChild
        FillType = ftBrush
        Frame.Typ = []
        Height = 36.800000000000000000
        Top = 873.071430000000000000
        Width = 718.110700000000000000
        OnAfterCalcHeight = 'Child_mapOnAfterCalcHeight'
        OnBeforePrint = 'Child_mapOnBeforePrint'
        AllowSplit = True
        Child = frxReport_MTS.Child_profile
        ToNRows = 0
        ToNRowsMode = rmCount
        object Picture_map: TfrxPictureView
          Description = 'Map'
          Align = baWidth
          AllowVectorExport = True
          Width = 718.110700000000000000
          Height = 28.000000000000000000
          OnBeforePrint = 'Picture_mapOnBeforePrint'
          DataField = 'map_img'
          DataSet = frxDBDataset_Link_img
          DataSetName = 'Link_img'
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Stretched = False
          HightQuality = True
          Transparent = False
          TransparentColor = clWhite
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        Frame.Typ = []
        Height = 24.000000000000000000
        Top = 1054.488870000000000000
        Width = 718.110700000000000000
        object Page: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 638.110700000000000000
          Width = 80.000000000000000000
          Height = 16.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8 = (
            '[Page#]')
          ParentFont = False
        end
        object Memo138: TfrxMemoView
          Align = baWidth
          AllowVectorExport = True
          Width = 638.110700000000000000
          Height = 16.000000000000000000
          AutoWidth = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clSilver
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8 = (
            
              #1056#1115#1057#8218#1057#8225#1056#181#1057#8218' '#1056#1030#1057#8249#1056#1111#1056#1109#1056#187#1056#1029#1056#181#1056#1029' '#1056#1030' '#1056#1111#1057#1026#1056#1109#1056#1110#1057#1026#1056#176#1056#1112#1056#1112#1056#181' ONEPLAN RPLS-D' +
              'B Link')
          ParentFont = False
        end
      end
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        Frame.Typ = []
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        Height = 28.000000000000000000
        ParentFont = False
        Top = 18.897650000000000000
        Width = 718.110700000000000000
        object Linkcalc_method_name1: TfrxMemoView
          Align = baWidth
          AllowVectorExport = True
          Left = 141.600000000000000000
          Width = 576.510700000000000000
          Height = 16.000000000000000000
          AutoWidth = True
          DataField = 'calc_method_name'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clSilver
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8 = (
            '[Link."calc_method_name"]')
          ParentFont = False
        end
        object Memo137: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Width = 141.600000000000000000
          Height = 16.000000000000000000
          AutoWidth = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clSilver
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8 = (
            #1056#1114#1056#181#1057#8218#1056#1109#1056#1169#1056#1105#1056#1108#1056#176' '#1057#1026#1056#176#1057#1027#1057#8225#1056#181#1057#8218#1056#176': ')
          ParentFont = False
        end
      end
    end
  end
  object frxDBDataset_repeater: TfrxDBDataset
    UserName = 'repeater'
    CloseDataSource = False
    DataSet = qry_LInk_repeater
    BCDToCurrency = False
    Left = 768
    Top = 312
  end
  object frxReport1: TfrxReport
    Version = '6.2.11'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports3'
    PreviewOptions.AllowEdit = False
    PreviewOptions.Buttons = [pbPrint, pbExport, pbZoom, pbTools, pbNavigator, pbExportQuick, pbNoFullScreen]
    PreviewOptions.ThumbnailVisible = True
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = #1055#1086' '#1091#1084#1086#1083#1095#1072#1085#1080#1102
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 41730.469225335600000000
    ReportOptions.LastChange = 42795.633203159700000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      ''
      '         '
      ''
      ''
      'procedure frxReportOnStartReport(Sender: TfrxComponent);'
      'const'
      '  DEF_MAP_H = 800;'
      '  '
      '  DEF_H = 450;'
      '  DEF_OFFSET = 25;'
      ''
      '  DEF_TOP = 20;                                  '
      'begin'
      '//  Picture_map.Width:=200;                '
      '  Picture_map.Height     :=DEF_MAP_H;'
      '    '
      '  Picture_profile.Height :=DEF_H;                '
      '            '
      '  '
      '  Child_map.Height:=DEF_MAP_H;'
      ''
      ''
      ' // Picture_map.Dataset:='#39'Link_img'#39';      '
      
        ' // Picture_profile.Dataset:='#39'Link_img'#39';                        ' +
        '  '
      '                                                      '
      '               '
      '  Child_profile.Height:=DEF_H + DEF_OFFSET;              '
      '        '
      '      '
      'end;  '
      '             '
      '    '
      ''
      '  '
      '//------------------------------------------------------------'
      'function DegreeToStr (aValue: double): String;'
      '//------------------------------------------------------------'
      'const'
      '//  STR_BLPOINT_FORMAT1 = '#39'%d'#176'%.2d'#39#39'%s%1.2f'#39#39#39#39#39';'
      '  STR_BLPOINT_FORMAT1 = '#39'%d'#176' %.2d'#39#39' %1.2f'#39#39#39#39#39';'
      'var    '
      ' iDeg,iMin:integer; '
      ' eSec: Double;   '
      '           '
      'begin'
      '  geo_DecodeDegree_(aValue, iDeg,iMin,eSec);'
      ' '
      '  Result:=Format(STR_BLPOINT_FORMAT1, [iDeg, iMin, eSec]);'
      ''
      'end;'
      ''
      '       '
      ''
      '//------------------------------------------------------------'
      'function DegreeToStr_lat (aValue: double): String;'
      '//------------------------------------------------------------'
      'var '
      '  s: string;'
      'begin'
      '  if aValue>0 then s:='#39'N'#39' else s:='#39'S'#39';'
      ''
      '  aValue:=Abs(aValue);       '
      ''
      '  Result:=DegreeToStr (aValue) + s;'
      'end;'
      ''
      '//------------------------------------------------------------'
      'function DegreeToStr_lon (aValue: double): String;'
      '//------------------------------------------------------------'
      'var '
      '  s: string;'
      'begin'
      '   if aValue<0 then begin'
      '     s:='#39'W'#39';'
      '     aValue:=Abs(aValue);'
      '   end else'
      ''
      '   if aValue>180 then begin'
      '     s:='#39'W'#39';'
      '     aValue:=360 - aValue'
      '   end else'
      '     s:='#39'E'#39';     '
      ''
      '  Result:=DegreeToStr (aValue) + s;'
      'end;  '
      ''
      '        '
      ''
      '//------------------------------------------------------------'
      
        'procedure geo_DecodeDegree_old(aValue: Double; var aDeg,aMin:int' +
        'eger; var aSec:'
      '    Double);'
      '//------------------------------------------------------------'
      'const'
      ' EXP_ = 0.000000001;'
      '     //97,033333333'
      'var'
      '  bIsNegative: Boolean;'
      '  e: Double;'
      '  eMinutes: Double;'
      '  eSec: Double;'
      '  eSeconds: Double;'
      'begin'
      '  bIsNegative := aValue<0;'
      ''
      '  aValue :=Abs(aValue);'
      ''
      '  aDeg:=Trunc(aValue);'
      ''
      ' // e:=Abs(aValue)*60;'
      ''
      '//  , Frac(X) = X - Int(X).'
      ''
      '  aMin:=Trunc(Frac(aValue)*60);'
      ''
      '//  Result.Sec:=Frac(Frac(Abs(aValue))*60)*60;'
      '  e:= (aValue - Trunc(aValue)) * 60;'
      '  aMin:=Trunc(e);'
      ''
      ' eMinutes := Trunc (e);'
      ' eSeconds := (e - Trunc (e)) * 60 ;'
      ''
      ''
      '  aSec:=(((aValue - aDeg) *60) - aMin) * 60;'
      ''
      '  if (aSec - 60) > EXP_ then'
      '  begin'
      '    Inc(aMin);'
      '    aSec := 0;'
      '  end;'
      ''
      ''
      '  if bIsNegative then'
      '    aDeg:=-aDeg;'
      '          '
      'end;'
      ''
      ''
      ' //------------------------------------------------------------'
      
        'procedure geo_DecodeDegree_(aValue: Double; var aDeg,aMin:intege' +
        'r; var aSec:'
      '    Double);'
      '//------------------------------------------------------------'
      'var'
      '  bIsNegative: Boolean;'
      '  e: Double;'
      '  eSec: Double;'
      ''
      'begin'
      '  bIsNegative := aValue<0;'
      ''
      '  aValue :=Abs(aValue);'
      ''
      '  aDeg:=Trunc(aValue);'
      ''
      '  aMin:=Trunc(Frac(aValue)*60);'
      ''
      ''
      '//  e:= (aValue - Trunc(aValue));'
      '  e:= (aValue - Trunc(aValue)) * 60;'
      ''
      ''
      '  aMin:=Trunc(e);'
      '  aSec:=(((aValue - aDeg) *60) - aMin) * 60;'
      ''
      '   while aSec>(60-0.1) do'
      '  begin'
      '    aMin:=aMin+1;'
      '    aSec:=aSec-60;'
      '  end;    '
      ''
      '  while aMin>=60 do'
      '  begin'
      '      aMin:=aMin-60;'
      '      aDeg:=aDeg+1;'
      '  end;  '
      ''
      '  if aSec<0 then'
      '     aSec:=0;'
      '    '
      ''
      ''
      '  if bIsNegative then'
      '    aDeg:=-aDeg;'
      ''
      ''
      ''
      'end;'
      ''
      '     '
      ''
      '       '
      'begin'
      ''
      'end.    ')
    OnStartReport = 'frxReportOnStartReport'
    Left = 454
    Top = 92
    Datasets = <
      item
        DataSet = frxDBDataset_link
        DataSetName = 'Link'
      end
      item
      end
      item
        DataSet = frxDBDataset_Link_img
        DataSetName = 'Link_img'
      end
      item
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 9.260416666666670000
      BottomMargin = 10.583333333333300000
      Frame.Typ = []
      LargeDesignHeight = True
      OnBeforePrint = 'Page1OnBeforePrint'
      object Child_Header: TfrxChild
        FillType = ftBrush
        Frame.Typ = []
        Height = 335.000000000000000000
        Top = 176.000000000000000000
        Width = 718.110700000000000000
        ToNRows = 0
        ToNRowsMode = rmCount
        object Memo105: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 478.110700000000000000
          Width = 110.000000000000000000
          Height = 16.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1119#1056#187#1056#1109#1057#8240#1056#176#1056#1169#1056#1108#1056#176)
          ParentFont = False
        end
        object Memo106: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 588.110700000000000000
          Width = 130.000000000000000000
          Height = 16.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."Property2_name"]')
          ParentFont = False
        end
        object Memo109: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 478.110700000000000000
          Top = 70.000000000000000000
          Width = 110.000000000000000000
          Height = 16.000000000000000000
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#8217#1057#8249#1057#1027#1056#1109#1057#8218#1056#176' '#1056#1111#1056#1109#1056#1169#1056#1030#1056#181#1057#1027#1056#176', m')
          ParentFont = False
        end
        object Memo110: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 478.110700000000000000
          Top = 109.000000000000000000
          Width = 110.000000000000000000
          Height = 16.000000000000000000
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1106#1056#183#1056#1105#1056#1112#1057#1107#1057#8218)
          ParentFont = False
        end
        object Memo111: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 478.110700000000000000
          Top = 264.000000000000000000
          Width = 110.000000000000000000
          Height = 16.000000000000000000
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1032#1057#1026#1056#1109#1056#1030#1056#181#1056#1029#1057#1034' '#1057#1027#1056#1105#1056#1110#1056#1029#1056#176#1056#187#1056#176', dBm')
          ParentFont = False
        end
        object Memo112: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 478.110700000000000000
          Top = 51.000000000000000000
          Width = 110.000000000000000000
          Height = 16.000000000000000000
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#8221#1056#1105#1056#176#1056#1112#1056#181#1057#8218#1057#1026' '#1056#176#1056#1029#1057#8218#1056#181#1056#1029#1056#1029#1057#8249)
          ParentFont = False
        end
        object Memo115: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 588.110700000000000000
          Top = 70.000000000000000000
          Width = 130.000000000000000000
          Height = 16.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."antenna2_height"]')
          ParentFont = False
        end
        object Memo116: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 588.110700000000000000
          Top = 109.000000000000000000
          Width = 130.000000000000000000
          Height = 16.000000000000000000
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."Azimuth2"]')
          ParentFont = False
        end
        object Memo117: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 588.110700000000000000
          Top = 51.000000000000000000
          Width = 130.000000000000000000
          Height = 16.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."antenna2_diameter"]')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 588.110700000000000000
          Top = 264.000000000000000000
          Width = 130.000000000000000000
          Height = 16.000000000000000000
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          ParentFont = False
        end
        object Memo66: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 478.110700000000000000
          Top = 210.000000000000000000
          Width = 110.000000000000000000
          Height = 16.000000000000000000
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1114#1056#1109#1057#8240#1056#1029#1056#1109#1057#1027#1057#8218#1057#1034' '#1056#1111#1056#181#1057#1026#1056#181#1056#1169#1056#176#1057#8218#1057#8225#1056#1105#1056#1108#1056#176', dBm')
          ParentFont = False
        end
        object Memo99: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 588.110700000000000000
          Top = 210.000000000000000000
          Width = 130.000000000000000000
          Height = 16.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."LinkEnd2_power_dBm"]')
          ParentFont = False
        end
        object Memo43: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Width = 110.000000000000000000
          Height = 16.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1119#1056#187#1056#1109#1057#8240#1056#176#1056#1169#1056#1108#1056#176)
          ParentFont = False
        end
        object Memo_left_1: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 110.000000000000000000
          Width = 125.000000000000000000
          Height = 16.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."Property1_name"]')
          ParentFont = False
        end
        object Memo70: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 74.000000000000000000
          Width = 110.000000000000000000
          Height = 16.000000000000000000
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#8217#1057#8249#1057#1027#1056#1109#1057#8218#1056#176' '#1056#1111#1056#1109#1056#1169#1056#1030#1056#181#1057#1027#1056#176', m')
          ParentFont = False
        end
        object Memo72: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 109.000000000000000000
          Width = 110.000000000000000000
          Height = 16.000000000000000000
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1106#1056#183#1056#1105#1056#1112#1057#1107#1057#8218)
          ParentFont = False
        end
        object Memo74: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 128.000000000000000000
          Width = 110.000000000000000000
          Height = 16.000000000000000000
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1032#1056#1110#1056#1109#1056#187' '#1056#1112#1056#181#1057#1027#1057#8218#1056#176)
          ParentFont = False
        end
        object Memo100: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 51.000000000000000000
          Width = 110.000000000000000000
          Height = 16.000000000000000000
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#8221#1056#1105#1056#176#1056#1112#1056#181#1057#8218#1057#1026' '#1056#176#1056#1029#1057#8218#1056#181#1056#1029#1056#1029#1057#8249)
          ParentFont = False
        end
        object Memo1: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 110.000000000000000000
          Top = 74.000000000000000000
          Width = 125.000000000000000000
          Height = 16.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."antenna1_height"]')
          ParentFont = False
        end
        object Memo95: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 110.000000000000000000
          Top = 109.000000000000000000
          Width = 125.000000000000000000
          Height = 16.000000000000000000
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."Azimuth1"]')
          ParentFont = False
        end
        object Memo97: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 110.000000000000000000
          Top = 51.000000000000000000
          Width = 125.000000000000000000
          Height = 16.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."antenna1_diameter"]')
          ParentFont = False
        end
        object Memo96: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 110.000000000000000000
          Top = 128.000000000000000000
          Width = 125.000000000000000000
          Height = 16.000000000000000000
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."Tilt1"]')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 218.000000000000000000
          Width = 110.000000000000000000
          Height = 16.000000000000000000
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1114#1056#1109#1057#8240#1056#1029#1056#1109#1057#1027#1057#8218#1057#1034' '#1056#1111#1056#181#1057#1026#1056#181#1056#1169#1056#176#1057#8218#1057#8225#1056#1105#1056#1108#1056#176', dBm')
          ParentFont = False
        end
        object Memo30: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 110.000000000000000000
          Top = 218.000000000000000000
          Width = 125.000000000000000000
          Height = 16.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."LinkEnd1_power_dBm"]')
          ParentFont = False
        end
        object Memo45: TfrxMemoView
          AllowVectorExport = True
          Left = 243.220470000000000000
          Top = 19.000000000000000000
          Width = 144.204726490000000000
          Height = 16.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#8221#1056#187#1056#1105#1056#1029#1056#176' '#1056#1105#1056#1029#1057#8218#1056#181#1057#1026#1056#1030#1056#176#1056#187#1056#176', km')
          ParentFont = False
        end
        object Memo47: TfrxMemoView
          AllowVectorExport = True
          Left = 243.220470000000000000
          Top = 46.000000000000000000
          Width = 144.204726490000000000
          Height = 16.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#8221#1056#1105#1056#176#1056#1111#1056#176#1056#183#1056#1109#1056#1029', GHz')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Left = 243.220470000000000000
          Top = 115.000000000000000000
          Width = 144.204726490000000000
          Height = 16.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1038#1056#1108#1056#1109#1057#1026#1056#1109#1057#1027#1057#8218#1057#1034' '#1056#1111#1056#181#1057#1026#1056#181#1056#1169#1056#176#1057#8225#1056#1105', Mbit/s')
          ParentFont = False
        end
        object Memo123: TfrxMemoView
          AllowVectorExport = True
          Left = 387.425196490000000000
          Top = 46.000000000000000000
          Width = 74.905973510000000000
          Height = 16.000000000000000000
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."rx_level_dBm"]')
          ParentFont = False
        end
        object Memo124: TfrxMemoView
          AllowVectorExport = True
          Left = 387.425196490000000000
          Top = 115.000000000000000000
          Width = 74.905973510000000000
          Height = 16.000000000000000000
          DataField = 'bitrate_Mbps'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd1."bitrate_Mbps"]')
          ParentFont = False
        end
        object Memo128: TfrxMemoView
          AllowVectorExport = True
          Left = 387.425196490000000000
          Top = 19.000000000000000000
          Width = 74.905973510000000000
          Height = 16.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."length_km"]')
          ParentFont = False
        end
        object Memo42: TfrxMemoView
          AllowVectorExport = True
          Left = 243.220470000000000000
          Width = 144.204726490000000000
          Height = 16.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1119#1057#1026#1056#1105#1056#183#1056#1029#1056#176#1056#1108' '#1056#1111#1057#1026#1056#1105#1056#1110#1056#1109#1056#1169#1056#1029#1056#1109#1057#1027#1057#8218#1056#1105)
          ParentFont = False
        end
        object Memo122: TfrxMemoView
          AllowVectorExport = True
          Left = 387.425196490000000000
          Width = 74.905973510000000000
          Height = 16.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."status_str"]')
          ParentFont = False
        end
        object Memo134: TfrxMemoView
          AllowVectorExport = True
          Left = 243.220470000000000000
          Top = 149.000000000000000000
          Width = 144.204726490000000000
          Height = 16.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#8212#1056#176#1056#1111#1056#176#1057#1027' '#1056#1029#1056#176' '#1056#183#1056#176#1056#1112#1056#1105#1057#1026#1056#176#1056#1029#1056#1105#1057#1039', dB')
          ParentFont = False
        end
        object Memo135: TfrxMemoView
          AllowVectorExport = True
          Left = 387.425196490000000000
          Top = 149.000000000000000000
          Width = 74.905973510000000000
          Height = 16.000000000000000000
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."fade_margin_dB"]')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 16.000000000000000000
          Width = 110.000000000000000000
          Height = 16.000000000000000000
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1113#1056#1109#1056#1109#1057#1026#1056#1169#1056#1105#1056#1029#1056#176#1057#8218#1057#8249' WGS')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 110.000000000000000000
          Top = 16.000000000000000000
          Width = 125.000000000000000000
          Height = 16.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."Property1_lat_WGS"]')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 110.000000000000000000
          Top = 32.000000000000000000
          Width = 125.000000000000000000
          Height = 16.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."Property1_lon_WGS"]')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 32.000000000000000000
          Width = 110.000000000000000000
          Height = 16.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 478.110700000000000000
          Top = 16.000000000000000000
          Width = 110.000000000000000000
          Height = 16.000000000000000000
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1113#1056#1109#1056#1109#1057#1026#1056#1169#1056#1105#1056#1029#1056#176#1057#8218#1057#8249' WGS')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 588.110700000000000000
          Top = 16.000000000000000000
          Width = 130.000000000000000000
          Height = 16.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."Property2_lat_WGS"]')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 588.110700000000000000
          Top = 32.000000000000000000
          Width = 130.000000000000000000
          Height = 16.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."Property2_lon_WGS"]')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 478.110700000000000000
          Top = 32.000000000000000000
          Width = 110.000000000000000000
          Height = 16.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          ParentFont = False
        end
        object Memo26: TfrxMemoView
          AllowVectorExport = True
          Left = 241.669450000000000000
          Top = 100.283550000000000000
          Width = 144.881880000000000000
          Height = 16.000000000000000000
          DataSet = frxDBDataset_link
          DataSetName = 'Link'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1114#1056#1109#1056#1169#1057#1107#1056#187#1057#1039#1057#8224#1056#1105#1057#1039)
          ParentFont = False
        end
        object Memo27: TfrxMemoView
          AllowVectorExport = True
          Left = 388.441411100000000000
          Top = 100.283550000000000000
          Width = 71.811026060000000000
          Height = 16.000000000000000000
          DataSet = frxDBDataset_link
          DataSetName = 'Link'
          DisplayFormat.FormatStr = '%2.2f'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd1."modulation_type"]')
          ParentFont = False
        end
        object Memo28: TfrxMemoView
          AllowVectorExport = True
          Left = 241.669450000000000000
          Top = 83.401670000000000000
          Width = 144.881880000000000000
          Height = 16.000000000000000000
          DataSet = frxDBDataset_link
          DataSetName = 'Link'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1025#1056#1105#1057#1026#1056#1105#1056#1029#1056#176' '#1056#1111#1056#1109#1056#187#1056#1109#1057#1027#1057#8249', MHz')
          ParentFont = False
        end
        object Memo29: TfrxMemoView
          AllowVectorExport = True
          Left = 388.441411100000000000
          Top = 83.401670000000000000
          Width = 71.811026060000000000
          Height = 16.000000000000000000
          DataSet = frxDBDataset_link
          DataSetName = 'Link'
          DisplayFormat.FormatStr = '%2.0f'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd1."channel_width_MHz"]')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 92.000000000000000000
          Width = 110.000000000000000000
          Height = 16.000000000000000000
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1119#1056#1109#1056#187#1057#1039#1057#1026#1056#1105#1056#183#1056#176#1057#8224#1056#1105#1057#1039' ')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 110.000000000000000000
          Top = 92.000000000000000000
          Width = 125.000000000000000000
          Height = 16.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."antenna1_polarization_str"]')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 478.110700000000000000
          Top = 92.000000000000000000
          Width = 110.000000000000000000
          Height = 16.000000000000000000
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1119#1056#1109#1056#187#1057#1039#1057#1026#1056#1105#1056#183#1056#176#1057#8224#1056#1105#1057#1039' ')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 588.110700000000000000
          Top = 92.000000000000000000
          Width = 130.000000000000000000
          Height = 16.000000000000000000
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."antenna2_polarization_str"]')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 148.000000000000000000
          Width = 110.000000000000000000
          Height = 16.000000000000000000
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1118#1056#1105#1056#1111' '#1056#1109#1056#177#1056#1109#1057#1026#1057#1107#1056#1169#1056#1109#1056#1030#1056#176#1056#1029#1056#1105#1057#1039)
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 110.000000000000000000
          Top = 148.000000000000000000
          Width = 125.000000000000000000
          Height = 16.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."LinkEndType1_name"]')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 168.000000000000000000
          Width = 110.000000000000000000
          Height = 16.000000000000000000
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1119#1056#1109#1056#1169#1056#1169#1056#1105#1056#176#1056#1111#1056#176#1056#183#1056#1109#1056#1029)
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 110.000000000000000000
          Top = 168.000000000000000000
          Width = 125.000000000000000000
          Height = 16.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."LinkEnd1_subband"]')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 188.000000000000000000
          Width = 110.000000000000000000
          Height = 16.000000000000000000
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#167#1056#176#1057#1027#1057#8218#1056#1109#1057#8218#1056#176' '#1056#1111#1056#181#1057#1026#1056#181#1056#1169#1056#176#1057#8225#1056#1105', MHz')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 110.000000000000000000
          Top = 188.000000000000000000
          Width = 125.000000000000000000
          Height = 16.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."LinkEnd1_tx_freq_MHz"]')
          ParentFont = False
        end
        object Memo25: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 236.000000000000000000
          Width = 110.000000000000000000
          Height = 16.000000000000000000
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1032#1057#1027#1056#1105#1056#187#1056#181#1056#1029#1056#1105#1056#181' '#1056#176#1056#1029#1057#8218#1056#181#1056#1029#1056#1029#1057#8249)
          ParentFont = False
        end
        object Memo31: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 110.000000000000000000
          Top = 236.000000000000000000
          Width = 125.000000000000000000
          Height = 16.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."antenna1_gain"]')
          ParentFont = False
        end
        object Memo32: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 260.000000000000000000
          Width = 110.000000000000000000
          Height = 16.000000000000000000
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1032#1057#1026#1056#1109#1056#1030#1056#181#1056#1029#1057#1034' '#1057#1027#1056#1105#1056#1110#1056#1029#1056#176#1056#187#1056#176', dBm')
          ParentFont = False
        end
        object Memo33: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 110.000000000000000000
          Top = 260.000000000000000000
          Width = 125.000000000000000000
          Height = 16.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          ParentFont = False
        end
        object Memo34: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 478.110700000000000000
          Top = 148.000000000000000000
          Width = 110.000000000000000000
          Height = 16.000000000000000000
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1118#1056#1105#1056#1111' '#1056#1109#1056#177#1056#1109#1057#1026#1057#1107#1056#1169#1056#1109#1056#1030#1056#176#1056#1029#1056#1105#1057#1039)
          ParentFont = False
        end
        object Memo35: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 588.110700000000000000
          Top = 148.000000000000000000
          Width = 130.000000000000000000
          Height = 16.000000000000000000
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."LinkEndType2_name"]')
          ParentFont = False
        end
        object Memo36: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 478.110700000000000000
          Top = 164.000000000000000000
          Width = 110.000000000000000000
          Height = 16.000000000000000000
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1119#1056#1109#1056#1169#1056#1169#1056#1105#1056#176#1056#1111#1056#176#1056#183#1056#1109#1056#1029)
          ParentFont = False
        end
        object Memo37: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 588.110700000000000000
          Top = 164.000000000000000000
          Width = 130.000000000000000000
          Height = 16.000000000000000000
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."LinkEnd2_subband"]')
          ParentFont = False
        end
        object Memo38: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 478.110700000000000000
          Top = 184.000000000000000000
          Width = 110.000000000000000000
          Height = 16.000000000000000000
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#167#1056#176#1057#1027#1057#8218#1056#1109#1057#8218#1056#176' '#1056#1111#1056#181#1057#1026#1056#181#1056#1169#1056#176#1057#8225#1056#1105', MHz')
          ParentFont = False
        end
        object Memo39: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 588.110700000000000000
          Top = 184.000000000000000000
          Width = 130.000000000000000000
          Height = 16.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."LinkEnd2_tx_freq_MHz"]')
          ParentFont = False
        end
        object Memo44: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 478.110700000000000000
          Top = 236.000000000000000000
          Width = 110.000000000000000000
          Height = 16.000000000000000000
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1032#1057#1027#1056#1105#1056#187#1056#181#1056#1029#1056#1105#1056#181' '#1056#176#1056#1029#1057#8218#1056#181#1056#1029#1056#1029#1057#8249', dB')
          ParentFont = False
        end
        object Memo46: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 588.110700000000000000
          Top = 236.000000000000000000
          Width = 130.000000000000000000
          Height = 16.000000000000000000
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."antenna2_gain"]')
          ParentFont = False
        end
        object Memo48: TfrxMemoView
          AllowVectorExport = True
          Left = 244.000000000000000000
          Top = 244.000000000000000000
          Width = 144.204726490000000000
          Height = 48.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            
              #1056#1032#1057#1026#1056#1109#1056#1030#1056#181#1056#1029#1057#1034' '#1057#1027#1056#1105#1056#1110#1056#1029#1056#176#1056#187#1056#176' '#1056#1030' '#1057#1027#1056#1030#1056#1109#1056#177#1056#1109#1056#1169#1056#1029#1056#1109#1056#1112' '#1056#1111#1057#1026#1056#1109#1057#1027#1057#8218#1057#1026 +
              #1056#176#1056#1029#1057#1027#1057#8218#1056#1030#1056#181', dBm')
          ParentFont = False
        end
        object Memo50: TfrxMemoView
          AllowVectorExport = True
          Left = 388.204726490000000000
          Top = 244.000000000000000000
          Width = 74.905973510000000000
          Height = 48.000000000000000000
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."rx_level_dBm"]')
          ParentFont = False
        end
        object Memo52: TfrxMemoView
          AllowVectorExport = True
          Left = 243.000000000000000000
          Top = 64.000000000000000000
          Width = 144.204726490000000000
          Height = 16.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1113#1056#1109#1056#1029#1057#8222#1056#1105#1056#1110#1057#1107#1057#1026#1056#176#1057#8224#1056#1105#1057#1039' '#1056#1109#1056#177#1056#1109#1057#1026#1057#1107#1056#1169#1056#1109#1056#1030#1056#176#1056#1029#1056#1105#1057#1039)
          ParentFont = False
        end
        object Memo53: TfrxMemoView
          AllowVectorExport = True
          Left = 388.000000000000000000
          Top = 64.000000000000000000
          Width = 74.905973510000000000
          Height = 16.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[FloatToStr( <LinkEnd1."tx_freq_MHz"> / 1000 )]')
          ParentFont = False
        end
        object Memo54: TfrxMemoView
          AllowVectorExport = True
          Left = 244.000000000000000000
          Top = 132.000000000000000000
          Width = 144.204726490000000000
          Height = 16.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1116#1056#176#1056#1169#1056#181#1056#182#1056#1029#1056#1109#1057#1027#1057#8218#1057#1034' '#1057#1027#1056#1030#1057#1039#1056#183#1056#1105', %')
          ParentFont = False
        end
        object Memo55: TfrxMemoView
          AllowVectorExport = True
          Left = 388.204726490000000000
          Top = 132.000000000000000000
          Width = 74.905973510000000000
          Height = 16.000000000000000000
          DataField = 'LinkEndType_name'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd1."LinkEndType_name"]')
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 478.110700000000000000
          Top = 128.000000000000000000
          Width = 110.000000000000000000
          Height = 16.000000000000000000
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1032#1056#1110#1056#1109#1056#187' '#1056#1112#1056#181#1057#1027#1057#8218#1056#176)
          ParentFont = False
        end
        object Memo49: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 588.110700000000000000
          Top = 128.000000000000000000
          Width = 130.000000000000000000
          Height = 16.000000000000000000
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."Tilt2"]')
          ParentFont = False
        end
      end
      object MasterData3: TfrxMasterData
        FillType = ftBrush
        Frame.Typ = []
        Height = 52.000000000000000000
        Top = 104.000000000000000000
        Width = 718.110700000000000000
        DataSet = frxDBDataset_link
        DataSetName = 'Link'
        RowCount = 0
        object Memo7: TfrxMemoView
          Align = baWidth
          AllowVectorExport = True
          Width = 718.110700000000000000
          Height = 19.000000000000000000
          AutoWidth = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8 = (
            
              #1056#1115#1057#8218#1057#8225#1056#181#1057#8218' '#1056#1111#1056#1109' '#1057#1026#1056#176#1056#1169#1056#1105#1056#1109#1057#1026#1056#181#1056#187#1056#181#1056#8470#1056#1029#1056#1109#1056#1112#1057#1107' '#1056#1105#1056#1029#1057#8218#1056#181#1057#1026#1056#1030#1056#176#1056#187#1057#1107' ' +
              'SDB')
          ParentFont = False
        end
        object Linkname: TfrxMemoView
          Align = baWidth
          AllowVectorExport = True
          Top = 28.000000000000000000
          Width = 718.110700000000000000
          Height = 16.000000000000000000
          AutoWidth = True
          Frame.Typ = []
          Memo.UTF8 = (
            '[Link."name_full"]')
        end
      end
      object Child_profile: TfrxChild
        FillType = ftBrush
        Frame.Typ = []
        Height = 58.400000000000000000
        Top = 588.000000000000000000
        Width = 718.110700000000000000
        ToNRows = 0
        ToNRowsMode = rmCount
        object Picture_profile: TfrxPictureView
          Align = baWidth
          AllowVectorExport = True
          Top = 20.000000000000000000
          Width = 718.110700000000000000
          Height = 36.000000000000000000
          OnBeforePrint = 'Picture_profileOnBeforePrint'
          DataField = 'profile_img'
          DataSet = frxDBDataset_Link_img
          DataSetName = 'Link_img'
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Stretched = False
          HightQuality = True
          Transparent = False
          TransparentColor = clWhite
        end
        object Memo80: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          ShiftMode = smWhenOverlapped
          Width = 363.200000000000000000
          Height = 16.800000000000000000
          AutoWidth = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8 = (
            
              #1056#152#1056#183#1056#1109#1056#177#1057#1026#1056#176#1056#182#1056#181#1056#1029#1056#1105#1056#181' '#1056#1111#1057#1026#1056#1109#1057#8222#1056#1105#1056#187#1057#1039' '#1056#1111#1057#1026#1056#1105' '#1056#1029#1056#1109#1057#1026#1056#1112#1056#176#1056#187#1057#1034#1056#1029#1056#1109#1056 +
              #8470' '#1057#1026#1056#181#1057#8222#1057#1026#1056#176#1056#1108#1057#8224#1056#1105#1056#1105)
          ParentFont = False
        end
      end
      object Child_map: TfrxChild
        FillType = ftBrush
        Frame.Typ = []
        Height = 36.800000000000000000
        Top = 532.000000000000000000
        Width = 718.110700000000000000
        OnAfterCalcHeight = 'Child_mapOnAfterCalcHeight'
        OnBeforePrint = 'Child_mapOnBeforePrint'
        AllowSplit = True
        ToNRows = 0
        ToNRowsMode = rmCount
        object Picture_map: TfrxPictureView
          Description = 'Map'
          Align = baWidth
          AllowVectorExport = True
          Width = 718.110700000000000000
          Height = 28.000000000000000000
          OnBeforePrint = 'Picture_mapOnBeforePrint'
          DataField = 'map_img'
          DataSet = frxDBDataset_Link_img
          DataSetName = 'Link_img'
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Stretched = False
          HightQuality = True
          Transparent = False
          TransparentColor = clWhite
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        Frame.Typ = []
        Height = 24.000000000000000000
        Top = 708.000000000000000000
        Width = 718.110700000000000000
        object Page: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 638.110700000000000000
          Width = 80.000000000000000000
          Height = 16.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8 = (
            '[Page#]')
          ParentFont = False
        end
        object Memo138: TfrxMemoView
          Align = baWidth
          AllowVectorExport = True
          Width = 638.110700000000000000
          Height = 16.000000000000000000
          AutoWidth = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clSilver
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8 = (
            
              #1056#1115#1057#8218#1057#8225#1056#181#1057#8218' '#1056#1030#1057#8249#1056#1111#1056#1109#1056#187#1056#1029#1056#181#1056#1029' '#1056#1030' '#1056#1111#1057#1026#1056#1109#1056#1110#1057#1026#1056#176#1056#1112#1056#1112#1056#181' ONEPLAN RPLS-D' +
              'B Link')
          ParentFont = False
        end
      end
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        Frame.Typ = []
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        Height = 28.000000000000000000
        ParentFont = False
        Top = 16.000000000000000000
        Width = 718.110700000000000000
        object Linkcalc_method_name1: TfrxMemoView
          Align = baWidth
          AllowVectorExport = True
          Left = 141.600000000000000000
          Width = 576.510700000000000000
          Height = 16.000000000000000000
          AutoWidth = True
          DataField = 'calc_method_name'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clSilver
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8 = (
            '[Summary."calc_method_name"]')
          ParentFont = False
        end
        object Memo137: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Width = 141.600000000000000000
          Height = 16.000000000000000000
          AutoWidth = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clSilver
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8 = (
            #1056#1114#1056#181#1057#8218#1056#1109#1056#1169#1056#1105#1056#1108#1056#176' '#1057#1026#1056#176#1057#1027#1057#8225#1056#181#1057#8218#1056#176': ')
          ParentFont = False
        end
      end
    end
    object Page2: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
    end
  end
  object sp_Link: TADOStoredProc
    LockType = ltBatchOptimistic
    Parameters = <>
    Left = 224
    Top = 8
  end
end
