unit dm_App_link_report;

interface

uses
  Classes, Dialogs,  SysUtils,

  dm_Link_report,

  d_Report_Select_new,

  //u_Link_report_vars,

  //_Init


  dm_MDB,


  i_Options_,

//  x_Options,

  u_ini_Link_report_params,

  dm_Link_SDB_report,
//  dm_Link_report_xml,
//  dm_Link_report_data,
  
 // dm_Link_report,
  dm_LinkLine_report,
  

  u_vars,

  dm_Main

  ;
  //dm_LinkLine_Complex_Report11,

type
  TdmMain_app__report = class(TDataModule)
    procedure DataModuleDestroy(Sender: TObject);
    procedure DataModuleCreate(Sender: TObject);
  private
   // FfrxReport: TfrxReport;
    
//    FReportSelect: TIni_Link_Report_Param;
  public

  //  procedure UpdateReports;
  end;

var
  dmMain_app__report: TdmMain_app__report;


implementation

//uses dm_Test1;

 {$R *.DFM}

//--------------------------------------------------------------
procedure TdmMain_app__report.DataModuleCreate(Sender: TObject);
//--------------------------------------------------------------
const
//C:\Users\Alexey\AppData\Local\RPLS_DB_LINK\
  DEF_TEMP_FILENAME = 'link_report_.ini';

  REGISTRY_ROOT = 'Software\Onega\RPLS_DB\';

var
  k: Integer;
  sIniFileName: string;
  sMDB_FileName: string;
  sReportFileName: string;
  sTemplateFileName: string;


begin

(*
  TdmTest1.Init;
  dmTest1.frxReport.DesignReport(true);

  Exit;
*)
 // ---------------------------------------------------------------




  sIniFileName:=ParamStr(1);
  if sIniFileName='' then
    sIniFileName:=g_ApplicationDataDir+ DEF_TEMP_FILENAME;



  if not FileExists(sIniFileName) then
  begin
    ShowMessage(sIniFileName);
    Exit;
  end;

  // -------------------------------------------------------------------

//    dmMain.Open_ADOConnection (oIni.ConnectionString);
  

//  TdmMain.Init;

//  if not dmMain.OpenDB_reg then    // True
//    Exit;




  {$DEFINE use_dll111}

  {$IFDEF use_dll}

{
  if Load_ILink_report() then
  begin
    ILink_report.InitADO(dmMain.ADOConnection1.ConnectionObject);
 //   ILink_graph.InitAppHandle(Application.Handle);
    ILink_report.Execute(sFile);

    UnLoad_ILink_report();
  end;

  Exit;
 }
  {$ENDIF}



  g_Link_Report_Param:=TIni_Link_Report_Param.Create;
  g_Link_Report_Param.LoadFromINI(sIniFileName);

  TdmMain.Init;
  dmMain.Open_ADOConnection(g_Link_Report_Param.ConnectionString);


  TdmMDB.Init;

  k:= Random (99999999);
//  sMDB_FileName:=g_ApplicationDataDir+ 'report.mdb';
  sMDB_FileName:=g_ApplicationDataDir + Format('report-%d.mdb', [k]);

  dmMDB.CreateMDB1(sMDB_FileName);



  TdmLink_report.Init;
  TdmLink_SDB_report.Init;
  TdmLinkLine_report.Init;
 // TdmLink_map.Init;



//  WMS_Init;


//  dmLink_report.frxReport.DesignReport();

//  TdmTest1.Init;
 // dmTest1.frxReport.DesignReport(true);

 // Exit;




{
  if //(g_Link_Report_Param.FileDir='') or
     (g_Link_Report_Param.ProjectID=0)
    // (g_Link_Report_Param.Report_ID=0)
  then begin
    ShowMessage('(FileDir='''') or (ProjectID=0) or (ReportID=0) ');
    Exit;
  end;
        }
  // -------------------------------------------------------------------

 ////// ShowMessage(IntToStr(rec.ProjectID));


  dmMain.ProjectID:=g_Link_Report_Param.ProjectID;

  Options_Init__1;

//  IOptionsX_Load;

//  IOptions.ExecDlg_new;

  //:=Get_IOptionsX();


 /////////// dmLink_Map.Params.MapDesktopID:=g_ReportParams.MapDesktopID;

 // rec.Scale:=0;


//  if iScale>0 then
 //   dmLink_map.Scale:=iScale;

  Tdlg_Main_Report.ExecDlg();

//  if not Tdlg_Report_Select_new.ExecDlg() then
//    Exit;



   dmMDB.DeleteMDB;


end;



// ---------------------------------------------------------------
procedure TdmMain_app__report.DataModuleDestroy(Sender: TObject);
// ---------------------------------------------------------------
var
  I: integer;
  s: string;
  s4: string;
  s3: string;
  s2: string;
  s1: string;

begin
//  IOptionsX_UnLoad();

  FreeAndNil(g_Link_Report_Param);

 // FreeAndNil(FReportSelect);
        

(*
  FreeAndNil(dmLink_report_data);
  FreeAndNil(dmLink_report);
  FreeAndNil(dmLinkLine_report);
  FreeAndNil(dmLink_map);
  FreeAndNil(dmLink_report_xml);
  FreeAndNil(dmRel_Engine);

  FreeAndNil(dmMain);

  inherited;*)

end;


end.

