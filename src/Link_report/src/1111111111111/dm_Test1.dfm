object dmTest1: TdmTest1
  OldCreateOrder = False
  Left = 616
  Top = 449
  Height = 351
  Width = 626
  object ADOConnection_mdb: TADOConnection
    ConnectionString = 
      'Provider=Microsoft.Jet.OLEDB.4.0;Data Source=C:\report.mdb;Persi' +
      'st Security Info=False'
    CursorLocation = clUseServer
    LoginPrompt = False
    Mode = cmShareDenyNone
    Provider = 'Microsoft.Jet.OLEDB.4.0'
    Left = 64
    Top = 64
  end
  object t_Img: TADOTable
    Connection = ADOConnection_mdb
    CursorType = ctStatic
    TableDirect = True
    TableName = 'Img'
    Left = 64
    Top = 120
    object t_Img_img1: TBlobField
      FieldName = 'img1'
    end
    object t_Img_mg2: TBlobField
      FieldName = 'img2'
    end
    object t_Img_img3: TBlobField
      FieldName = 'img3'
    end
    object t_Img_profile_img: TBlobField
      FieldName = 'profile_img'
    end
    object t_Img_map_img: TBlobField
      FieldName = 'map_img'
    end
    object t_Img_link_id: TIntegerField
      FieldName = 'link_id'
    end
  end
  object ADOConnection_mdb_new: TADOConnection
    ConnectionString = 
      'Provider=Microsoft.Jet.OLEDB.4.0;User ID=Admin;Data Source=D:\aa' +
      'aa.mdb;Mode=Share Deny None;Extended Properties="";Persist Secur' +
      'ity Info=False;Jet OLEDB:System database="";Jet OLEDB:Registry P' +
      'ath="";Jet OLEDB:Database Password="";Jet OLEDB:Engine Type=5;Je' +
      't OLEDB:Database Locking Mode=1;Jet OLEDB:Global Partial Bulk Op' +
      's=2;Jet OLEDB:Global Bulk Transactions=1;Jet OLEDB:New Database ' +
      'Password="";Jet OLEDB:Create System Database=False;Jet OLEDB:Enc' +
      'rypt Database=False;Jet OLEDB:Don'#39't Copy Locale on Compact=False' +
      ';Jet OLEDB:Compact Without Replica Repair=False;Jet OLEDB:SFP=Fa' +
      'lse'
    CursorLocation = clUseServer
    LoginPrompt = False
    Mode = cmShareExclusive
    Provider = 'Microsoft.Jet.OLEDB.4.0'
    Left = 240
    Top = 64
  end
  object t_img111: TADOTable
    Connection = ADOConnection_mdb_new
    CursorType = ctStatic
    TableDirect = True
    TableName = 'Img'
    Left = 224
    Top = 216
    object BlobField1: TBlobField
      FieldName = 'img1'
    end
    object BlobField2: TBlobField
      FieldName = 'img2'
    end
    object BlobField3: TBlobField
      FieldName = 'img3'
    end
    object BlobField4: TBlobField
      FieldName = 'profile_img'
    end
    object BlobField5: TBlobField
      FieldName = 'map_img'
    end
    object IntegerField1: TIntegerField
      FieldName = 'link_id'
    end
  end
  object t_Profile_items11: TADOTable
    Connection = ADOConnection_mdb_new
    CursorType = ctStatic
    TableDirect = True
    TableName = 'Profile_items'
    Left = 328
    Top = 216
  end
  object t_groups11: TADOTable
    Connection = ADOConnection_mdb_new
    CursorType = ctStatic
    TableDirect = True
    TableName = 'group1'
    Left = 408
    Top = 216
  end
  object t_Items11: TADOTable
    Connection = ADOConnection_mdb_new
    CursorType = ctStatic
    TableDirect = True
    TableName = 'Item'
    Left = 472
    Top = 216
  end
  object frxReport: TfrxReport
    Version = '4.15.6'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.AllowEdit = False
    PreviewOptions.Buttons = [pbPrint, pbExport, pbZoom, pbTools, pbNavigator, pbExportQuick, pbNoFullScreen]
    PreviewOptions.ThumbnailVisible = True
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = #1055#1086' '#1091#1084#1086#1083#1095#1072#1085#1080#1102
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 41730.469225335600000000
    ReportOptions.LastChange = 41906.536397025500000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      ''
      ''
      ''
      
        'procedure Shape_profile_ColorOnBeforePrint(Sender: TfrxComponent' +
        ');'
      'begin'
      '//  Shape_profile_Color.Color := clBlue;'
      ''
      '//  if value         '
      '    '
      'end;'
      '  '
      ''
      ''
      'procedure frxReportOnStartReport(Sender: TfrxComponent);'
      'const'
      '  DEF_MAP_H = 800;'
      '  '
      '  DEF_H = 450;'
      '  DEF_OFFSET = 25;'
      ''
      '  DEF_TOP = 20;                                  '
      'begin'
      '//  Picture_map.Width:=200;                '
      '  Picture_map.Height     :=DEF_MAP_H;'
      '    '
      '  Picture_profile.Height :=DEF_H;                '
      '  Picture_profile1.Height:=DEF_H;                '
      '  Picture_profile2.Height:=DEF_H;                '
      '  Picture_profile3.Height:=DEF_H;                '
      '          '
      '  '
      '  Child_map.Height:=DEF_MAP_H;'
      '    '
      '  Child_profile.Height:=DEF_H + DEF_OFFSET;              '
      '  Child_profile1.Height:=DEF_H + DEF_OFFSET;              '
      '  Child_profile2.Height:=DEF_H + DEF_OFFSET;              '
      '  Child_profile3.Height:=DEF_H + DEF_OFFSET;'
      ''
      '  Child_profile1.Top:=DEF_TOP;              '
      '  Child_profile2.Top:=DEF_TOP;              '
      '  Child_profile3.Top:=DEF_TOP;'
      '  '
      '  '
      '  '
      '    '
      '//  Child_.Height:=Picture_map.Height;'
      '      '
      'end;'
      ''
      ''
      'procedure Shape_ColorOnBeforePrint(Sender: TfrxComponent);'
      'begin'
      
        '  Shape_Color.color := <Profile."clutter_color">                ' +
        '                                                    '
      'end;'
      ''
      'procedure Picture_mapOnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '//  Picture_map.FileLink:='#39'c:\reports\1.bmp'#39' ;  '
      ''
      
        '//  ShowMessage(<Link_img_path."map_img_path">);                ' +
        '                  '
      ''
      ' {     '
      '  Picture_map.FileLink:=<Link_img_path."map_img_path"> ;'
      '  Picture_profile.FileLink:=<Link_img_path."profile_img_path"> ;'
      ''
      '  Picture_profile1.FileLink:=<Link_img_path."img_path1"> ;    '
      '  Picture_profile2.FileLink:=<Link_img_path."img_path2"> ;    '
      '  Picture_profile3.FileLink:=<Link_img_path."img_path3"> ;    '
      '  }      '
      '   '
      'end;'
      ''
      'procedure Picture_profileOnBeforePrint(Sender: TfrxComponent);'
      'begin'
      
        '//  Picture_profile.FileLink:=<Link_img_path."profile_img_path">' +
        ' ;  '
      'end;'
      ''
      'procedure Picture_profile1OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '//  Picture_profile1.FileLink:=<Link_img_path."img_path1"> ;  '
      'end;'
      ''
      'procedure Picture_profile2OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '//   Picture_profile2.FileLink:=<Link_img_path."img_path2"> ;  '
      'end;'
      ''
      'procedure Picture_profile3OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '//  Picture_profile3.FileLink:=<Link_img_path."img_path3"> ;  '
      'end;'
      ''
      'begin'
      ''
      'end.')
    OnStartReport = 'frxReportOnStartReport'
    Left = 406
    Top = 20
    Datasets = <
      item
      end
      item
      end
      item
      end
      item
      end
      item
      end
      item
      end
      item
      end
      item
      end
      item
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 9.260416666666670000
      BottomMargin = 10.583333333333300000
      LargeDesignHeight = True
      OnBeforePrint = 'Page1OnBeforePrint'
      object Child_Header: TfrxChild
        Height = 67.000000000000000000
        Top = 92.000000000000000000
        Width = 718.110700000000000000
        Child = frxReport.Child_Summary
        object Subreport_center: TfrxSubreport
          Left = 248.000000000000000000
          Width = 216.000000000000000000
          Height = 55.000000000000000000
          ShowHint = False
          Page = frxReport.Page_Center
        end
        object Subreport_left: TfrxSubreport
          Width = 240.000000000000000000
          Height = 55.000000000000000000
          ShowHint = False
          Page = frxReport.Lleft
        end
        object Subreport_right: TfrxSubreport
          Align = baRight
          Left = 478.110699999999900000
          Width = 240.000000000000000000
          Height = 55.000000000000000000
          ShowHint = False
          Page = frxReport.Subreport_right_
        end
      end
      object Child_profile3: TfrxChild
        Height = 61.000000000000000000
        Top = 612.000000000000000000
        Width = 718.110700000000000000
        Child = frxReport.Child_calc_res
        object Memo82: TfrxMemoView
          Align = baLeft
          ShiftMode = smWhenOverlapped
          Width = 559.200000000000100000
          Height = 16.800000000000000000
          ShowHint = False
          AutoWidth = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            
              #1056#152#1056#183#1056#1109#1056#177#1057#1026#1056#176#1056#182#1056#181#1056#1029#1056#1105#1056#181' '#1056#1111#1057#1026#1056#1109#1057#8222#1056#1105#1056#187#1057#1039' '#1056#1030' '#1057#1107#1057#1027#1056#187#1056#1109#1056#1030#1056#1105#1057#1039#1057#8230' '#1057#1027#1057#1107#1056#177 +
              #1057#1026#1056#181#1057#8222#1057#1026#1056#176#1056#1108#1057#8224#1056#1105#1056#1105#1056#1109#1056#1029#1056#1029#1056#1109#1056#8470' '#1056#1029#1056#181#1057#1107#1057#1027#1057#8218#1056#1109#1056#8470#1057#8225#1056#1105#1056#1030#1056#1109#1057#1027#1057#8218#1056#1105)
          ParentFont = False
        end
        object Picture_profile3: TfrxPictureView
          Align = baWidth
          ShiftMode = smWhenOverlapped
          Top = 20.000000000000000000
          Width = 718.110700000000000000
          Height = 31.000000000000000000
          OnBeforePrint = 'Picture_profile3OnBeforePrint'
          ShowHint = False
          DataField = 'img3'
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Stretched = False
          HightQuality = True
          Transparent = False
          TransparentColor = clWhite
        end
      end
      object Child_Summary: TfrxChild
        Height = 60.000000000000000000
        Top = 184.000000000000000000
        Width = 718.110700000000000000
        Child = frxReport.Child_map
        object Subreport_summary: TfrxSubreport
          Align = baWidth
          Width = 718.110700000000000000
          Height = 48.000000000000000000
          ShowHint = False
          Page = frxReport.Page_Summary
        end
      end
      object MasterData3: TfrxMasterData
        Height = 52.000000000000000000
        Top = 16.000000000000000000
        Width = 718.110700000000000000
        Child = frxReport.Child_Header
        RowCount = 0
        object Memo7: TfrxMemoView
          Align = baWidth
          Top = 0.220469999999998800
          Width = 718.110700000000000000
          Height = 15.000000000000000000
          ShowHint = False
          AutoWidth = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8 = (
            #1056#1115#1057#8218#1057#8225#1056#181#1057#8218' '#1056#1111#1056#1109' '#1057#1026#1056#176#1056#1169#1056#1105#1056#1109#1057#1026#1056#181#1056#187#1056#181#1056#8470#1056#1029#1056#1109#1056#1112#1057#1107' '#1056#1105#1056#1029#1057#8218#1056#181#1057#1026#1056#1030#1056#176#1056#187#1057#1107)
          ParentFont = False
        end
        object Linkname: TfrxMemoView
          Align = baLeft
          Top = 24.000000000000000000
          Width = 132.000000000000000000
          Height = 16.000000000000000000
          ShowHint = False
          AutoWidth = True
          DataField = 'name'
          Memo.UTF8 = (
            '[Link."name"]')
        end
      end
      object Child_profile: TfrxChild
        Height = 58.400000000000000000
        Top = 328.000000000000000000
        Width = 718.110700000000000000
        Child = frxReport.Child_profile1
        object Picture_profile: TfrxPictureView
          Align = baWidth
          Top = 20.000000000000000000
          Width = 718.110700000000000000
          Height = 36.000000000000000000
          OnBeforePrint = 'Picture_profileOnBeforePrint'
          ShowHint = False
          DataField = 'profile_img'
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Stretched = False
          HightQuality = True
          Transparent = False
          TransparentColor = clWhite
        end
        object Memo80: TfrxMemoView
          Align = baLeft
          ShiftMode = smWhenOverlapped
          Width = 363.200000000000000000
          Height = 16.800000000000000000
          ShowHint = False
          AutoWidth = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            
              #1056#152#1056#183#1056#1109#1056#177#1057#1026#1056#176#1056#182#1056#181#1056#1029#1056#1105#1056#181' '#1056#1111#1057#1026#1056#1109#1057#8222#1056#1105#1056#187#1057#1039' '#1056#1111#1057#1026#1056#1105' '#1056#1029#1056#1109#1057#1026#1056#1112#1056#176#1056#187#1057#1034#1056#1029#1056#1109#1056 +
              #8470' '#1057#1026#1056#181#1057#8222#1057#1026#1056#176#1056#1108#1057#8224#1056#1105#1056#1105)
          ParentFont = False
        end
      end
      object Child_map: TfrxChild
        Height = 36.800000000000000000
        Top = 268.000000000000000000
        Width = 718.110700000000000000
        OnAfterCalcHeight = 'Child_mapOnAfterCalcHeight'
        OnBeforePrint = 'Child_mapOnBeforePrint'
        AllowSplit = True
        Child = frxReport.Child_profile
        object Picture_map: TfrxPictureView
          Description = 'Map'
          Align = baWidth
          Width = 718.110700000000000000
          Height = 28.000000000000000000
          OnBeforePrint = 'Picture_mapOnBeforePrint'
          ShowHint = False
          DataField = 'map_img'
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Stretched = False
          HightQuality = True
          Transparent = False
          TransparentColor = clWhite
        end
      end
      object Child_profile1: TfrxChild
        Height = 76.000000000000000000
        Top = 412.000000000000000000
        Width = 718.110700000000000000
        Child = frxReport.Child_profile2
        object Picture_profile1: TfrxPictureView
          Align = baWidth
          ShiftMode = smWhenOverlapped
          Top = 20.000000000000000000
          Width = 718.110700000000000000
          Height = 40.000000000000000000
          OnBeforePrint = 'Picture_profile1OnBeforePrint'
          ShowHint = False
          DataField = 'img1'
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Stretched = False
          HightQuality = True
          Transparent = False
          TransparentColor = clWhite
        end
        object Memo81: TfrxMemoView
          Align = baLeft
          ShiftMode = smWhenOverlapped
          Width = 359.200000000000000000
          Height = 16.800000000000000000
          ShowHint = False
          AutoWidth = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            
              #1056#152#1056#183#1056#1109#1056#177#1057#1026#1056#176#1056#182#1056#181#1056#1029#1056#1105#1056#181' '#1056#1111#1057#1026#1056#1109#1057#8222#1056#1105#1056#187#1057#1039' '#1056#1111#1057#1026#1056#1105' 20% '#1057#1026#1056#181#1057#8222#1057#1026#1056#176#1056#1108#1057#8224#1056 +
              #1105#1056#1105)
          ParentFont = False
        end
      end
      object Child_profile2: TfrxChild
        Height = 76.800000000000000000
        Top = 512.000000000000000000
        Width = 718.110700000000000000
        Child = frxReport.Child_profile3
        object Picture_profile2: TfrxPictureView
          Align = baWidth
          ShiftMode = smWhenOverlapped
          Top = 20.000000000000000000
          Width = 718.110700000000000000
          Height = 43.000000000000000000
          OnBeforePrint = 'Picture_profile2OnBeforePrint'
          ShowHint = False
          DataField = 'img2'
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Stretched = False
          HightQuality = True
          Transparent = False
          TransparentColor = clWhite
        end
        object Memo83: TfrxMemoView
          Align = baLeft
          ShiftMode = smWhenOverlapped
          Width = 567.200000000000100000
          Height = 16.800000000000000000
          ShowHint = False
          AutoWidth = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            
              #1056#152#1056#183#1056#1109#1056#177#1057#1026#1056#176#1056#182#1056#181#1056#1029#1056#1105#1056#181' '#1056#1111#1057#1026#1056#1109#1057#8222#1056#1105#1056#187#1057#1039' '#1056#1030' '#1057#1107#1057#1027#1056#187#1056#1109#1056#1030#1056#1105#1057#1039#1057#8230' '#1057#1027#1057#1107#1056#177 +
              #1057#1026#1056#181#1057#8222#1057#1026#1056#176#1056#1108#1057#8224#1056#1105#1056#1105)
          ParentFont = False
        end
      end
      object Child_Profile_items: TfrxChild
        Height = 40.000000000000000000
        Top = 760.000000000000000000
        Width = 718.110700000000000000
        StartNewPage = True
        object Subreport_Profile: TfrxSubreport
          Align = baWidth
          Width = 718.110700000000000000
          Height = 24.000000000000000000
          ShowHint = False
          Page = frxReport.Page6
        end
      end
      object Child_calc_res: TfrxChild
        Height = 40.000000000000000000
        Top = 696.000000000000000000
        Width = 718.110700000000000000
        AllowSplit = True
        Child = frxReport.Child_Profile_items
        StartNewPage = True
        object Subreport_calc_res: TfrxSubreport
          Align = baLeft
          Width = 718.110700000000000000
          Height = 20.000000000000000000
          ShowHint = False
          Page = frxReport.Page8
        end
      end
    end
    object Lleft: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object MasterData2: TfrxMasterData
        Height = 145.000000000000000000
        Top = 16.000000000000000000
        Width = 718.110700000000000000
        RowCount = 0
        object Memo43: TfrxMemoView
          Align = baLeft
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          ParentFont = False
        end
        object Memo_left_1: TfrxMemoView
          Align = baLeft
          Left = 110.000000000000000000
          Width = 125.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Site1."name"]')
          ParentFont = False
        end
        object Memo66: TfrxMemoView
          Align = baLeft
          Top = 15.000000000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1113#1056#1109#1056#1109#1057#1026#1056#1169#1056#1105#1056#1029#1056#176#1057#8218#1057#8249' '#1056#1038#1056#1113'-42')
          ParentFont = False
        end
        object Memo68: TfrxMemoView
          Align = baLeft
          Top = 45.000000000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1113#1056#1109#1056#1109#1057#1026#1056#1169#1056#1105#1056#1029#1056#176#1057#8218#1057#8249' WGS')
          ParentFont = False
        end
        object Memo70: TfrxMemoView
          Align = baLeft
          Top = 75.000000000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#8217#1057#8249#1057#1027#1056#1109#1057#8218#1056#176' '#1056#1111#1056#1109#1056#1169#1056#1030#1056#181#1057#1027#1056#176', m')
          ParentFont = False
        end
        object Memo72: TfrxMemoView
          Align = baLeft
          Top = 90.000000000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1106#1056#183#1056#1105#1056#1112#1057#1107#1057#8218)
          ParentFont = False
        end
        object Memo74: TfrxMemoView
          Align = baLeft
          Top = 105.000000000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1032#1056#1110#1056#1109#1056#187' '#1056#1112#1056#181#1057#1027#1057#8218#1056#176)
          ParentFont = False
        end
        object Memo100: TfrxMemoView
          Align = baLeft
          Top = 120.000000000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#8221#1056#1105#1056#176#1056#1112#1056#181#1057#8218#1057#1026' '#1056#176#1056#1029#1057#8218#1056#181#1056#1029#1056#1029#1057#8249)
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Align = baLeft
          Left = 110.000000000000000000
          Top = 15.000000000000000000
          Width = 125.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Site1."lat_str"]')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Align = baLeft
          Left = 110.000000000000000000
          Top = 45.000000000000000000
          Width = 125.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Site1."lat_WGS_str"]')
          ParentFont = False
        end
        object Memo1: TfrxMemoView
          Align = baLeft
          Left = 110.000000000000000000
          Top = 75.000000000000000000
          Width = 125.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd1."antenna_height"]')
          ParentFont = False
        end
        object Memo95: TfrxMemoView
          Align = baLeft
          Left = 110.000000000000000000
          Top = 90.000000000000000000
          Width = 125.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."azimuth1"]')
          ParentFont = False
        end
        object Memo97: TfrxMemoView
          Align = baLeft
          Left = 110.000000000000000000
          Top = 120.000000000000000000
          Width = 125.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd1."antenna_diameter"]')
          ParentFont = False
        end
        object Memo98: TfrxMemoView
          Align = baLeft
          Left = 110.000000000000000000
          Top = 60.000000000000000000
          Width = 125.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Site1."lon_WGS_str"]')
          ParentFont = False
        end
        object Memo99: TfrxMemoView
          Align = baLeft
          Left = 110.000000000000000000
          Top = 30.000000000000000000
          Width = 125.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Site1."lon_str"]')
          ParentFont = False
        end
        object Memo96: TfrxMemoView
          Align = baLeft
          Left = 110.000000000000000000
          Top = 105.000000000000000000
          Width = 125.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."ELEVATION_angle_1"]')
          ParentFont = False
        end
        object Memo30: TfrxMemoView
          Align = baLeft
          Top = 30.000000000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          ParentFont = False
        end
        object Memo121: TfrxMemoView
          Align = baLeft
          Top = 60.000000000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          ParentFont = False
        end
      end
    end
    object Page_Center: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object MasterData1: TfrxMasterData
        Height = 111.000000000000000000
        Top = 16.000000000000000000
        Width = 718.110700000000000000
        RowCount = 0
        object Memo45: TfrxMemoView
          Align = baLeft
          Top = 15.000000000000000000
          Width = 160.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#8221#1056#187#1056#1105#1056#1029#1056#176' '#1056#1105#1056#1029#1057#8218#1056#181#1057#1026#1056#1030#1056#176#1056#187#1056#176', km')
          ParentFont = False
        end
        object Memo47: TfrxMemoView
          Align = baLeft
          Top = 30.000000000000000000
          Width = 160.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1032#1057#1026#1056#1109#1056#1030#1056#181#1056#1029#1057#1034' '#1057#1027#1056#1105#1056#1110#1056#1029#1056#176#1056#187#1056#176', dBm')
          ParentFont = False
        end
        object Memo49: TfrxMemoView
          Align = baLeft
          Top = 45.000000000000000000
          Width = 160.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            'P'#1056#176#1056#177#1056#1109#1057#8225#1056#176#1057#1039' '#1057#8225#1056#176#1057#1027#1057#8218#1056#1109#1057#8218#1056#176', GHz')
          ParentFont = False
        end
        object Memo51: TfrxMemoView
          Align = baLeft
          Top = 60.000000000000000000
          Width = 160.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1114#1056#1109#1057#8240#1056#1029#1056#1109#1057#1027#1057#8218#1057#1034' '#1056#1111#1056#181#1057#1026#1056#181#1056#1169#1056#176#1057#8218#1057#8225#1056#1105#1056#1108#1056#176', dBm')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          Align = baLeft
          Top = 75.000000000000000000
          Width = 160.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1038#1056#1108#1056#1109#1057#1026#1056#1109#1057#1027#1057#8218#1057#1034' '#1056#1111#1056#181#1057#1026#1056#181#1056#1169#1056#176#1057#8225#1056#1105', Mbit/s')
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          Align = baLeft
          Top = 90.000000000000000000
          Width = 160.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1118#1056#1105#1056#1111' '#1056#1109#1056#177#1056#1109#1057#1026#1057#1107#1056#1169#1056#1109#1056#1030#1056#176#1056#1029#1056#1105#1057#1039)
          ParentFont = False
        end
        object Memo123: TfrxMemoView
          Align = baLeft
          Left = 160.000000000000000000
          Top = 30.000000000000000000
          Width = 60.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          DataField = 'rx_level_dBm'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd1."rx_level_dBm"]')
          ParentFont = False
        end
        object Memo124: TfrxMemoView
          Align = baLeft
          Left = 160.000000000000000000
          Top = 75.000000000000000000
          Width = 60.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          DataField = 'bitrate_Mbps'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd1."bitrate_Mbps"]')
          ParentFont = False
        end
        object Memo125: TfrxMemoView
          Align = baLeft
          Left = 160.000000000000000000
          Top = 90.000000000000000000
          Width = 60.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          DataField = 'LinkEndType_name'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd1."LinkEndType_name"]')
          ParentFont = False
        end
        object Memo127: TfrxMemoView
          Align = baLeft
          Left = 160.000000000000000000
          Top = 45.000000000000000000
          Width = 60.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd1."tx_freq_MHz"]')
          ParentFont = False
        end
        object Memo128: TfrxMemoView
          Align = baLeft
          Left = 160.000000000000000000
          Top = 15.000000000000000000
          Width = 60.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."length_km"]')
          ParentFont = False
        end
        object Memo126: TfrxMemoView
          Align = baLeft
          Left = 160.000000000000000000
          Top = 60.000000000000000000
          Width = 60.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          DataField = 'power_dBm'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd1."power_dBm"]')
          ParentFont = False
        end
        object Memo42: TfrxMemoView
          Align = baLeft
          Width = 160.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1119#1057#1026#1056#1105#1056#183#1056#1029#1056#176#1056#1108' '#1056#1111#1057#1026#1056#1105#1056#1110#1056#1109#1056#1169#1056#1029#1056#1109#1057#1027#1057#8218#1056#1105)
          ParentFont = False
        end
        object Memo122: TfrxMemoView
          Align = baLeft
          Left = 160.000000000000000000
          Width = 60.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."status_name"]')
          ParentFont = False
        end
      end
    end
    object Page_Summary: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object MasterData4: TfrxMasterData
        Height = 416.000000000000000000
        Top = 16.000000000000000000
        Width = 718.110700000000000000
        RowCount = 0
        object Memo_1: TfrxMemoView
          Align = baLeft
          Top = 15.000000000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#8217#1057#8249#1057#1027#1056#1109#1057#8218#1056#176', m')
          ParentFont = False
        end
        object Memo_2: TfrxMemoView
          Align = baLeft
          Top = 30.000000000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1106#1056#183#1056#1105#1056#1112#1057#1107#1057#8218' ')
          ParentFont = False
        end
        object Memo_3: TfrxMemoView
          Align = baLeft
          Top = 45.000000000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1118#1056#1105#1056#1111' '#1056#1109#1056#177#1056#1109#1057#1026#1057#1107#1056#1169#1056#1109#1056#1030#1056#176#1056#1029#1056#1105#1057#1039' ')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Align = baLeft
          Top = 60.000000000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#167#1056#176#1057#1027#1057#8218#1056#1109#1057#8218#1056#176' '#1056#1111#1056#181#1057#1026#1056#181#1056#1169#1056#176#1057#8225#1056#1105', GHz')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Align = baLeft
          Top = 75.000000000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1038#1056#1108#1056#1109#1057#1026#1056#1109#1057#1027#1057#8218#1057#1034' '#1056#1111#1056#181#1057#1026#1056#181#1056#1169#1056#176#1057#8225#1056#1105', Mbit/s')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Align = baLeft
          Top = 90.000000000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1114#1056#1109#1057#8240#1056#1029#1056#1109#1057#1027#1057#8218#1057#1034', dBm')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          Align = baLeft
          Top = 105.000000000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#8221#1056#1105#1056#176#1056#1112#1056#181#1057#8218#1057#1026' '#1056#176#1056#1029#1057#8218#1056#181#1056#1029#1056#1029#1057#8249', m')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          Align = baLeft
          Top = 135.000000000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            
              #1056#1038#1057#1026#1056#181#1056#1169#1056#1029#1057#1039#1057#1039' '#1056#1112#1056#1109#1057#8240#1056#1029#1056#1109#1057#1027#1057#8218#1057#1034' '#1056#1029#1056#176' '#1056#1030#1057#8230#1056#1109#1056#1169#1056#181' '#1056#1111#1057#1026#1056#1105#1056#181#1056#1112#1056#1029#1056#1105#1056#1108 +
              #1056#176', dBm')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          Align = baLeft
          Top = 120.000000000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1119#1056#1109#1056#187#1057#1039#1057#1026#1056#1105#1056#183#1056#176#1057#8224#1056#1105#1057#1039' ')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          Align = baLeft
          Top = 150.000000000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#160#1056#181#1056#183#1056#181#1057#1026#1056#1030#1056#1105#1057#1026#1056#1109#1056#1030#1056#176#1056#1029#1056#1105#1056#181' ')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          Align = baLeft
          Top = 165.000000000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1119#1056#1109#1057#1026#1056#1109#1056#1110' '#1056#1111#1057#1026#1056#1105' BER=10-3 ')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          Align = baLeft
          Top = 195.000000000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1114#1056#1105#1056#1029#1056#1105#1056#1112#1056#176#1056#187#1057#1034#1056#1029#1057#8249#1056#8470' '#1056#1109#1057#8218#1056#1029#1056#1109#1057#1027#1056#1105#1057#8218#1056#181#1056#187#1057#1034#1056#1029#1057#8249#1056#8470' '#1056#1111#1057#1026#1056#1109#1057#1027#1056#1030#1056#181#1057#8218)
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          Align = baLeft
          Top = 180.000000000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1119#1056#1109#1057#1026#1056#1109#1056#1110' '#1056#1111#1057#1026#1056#1105' BER=10-6 ')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          Align = baLeft
          Top = 210.000000000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1119#1056#1109#1056#187#1056#1109#1056#182#1056#181#1056#1029#1056#1105#1056#181' '#1057#8218#1056#1109#1057#8225#1056#1108#1056#1105' '#1056#1111#1057#1026#1056#1109#1057#1027#1056#1030#1056#181#1057#8218#1056#176', km ')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          Align = baLeft
          Top = 225.000000000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            'SESR '#1057#1027#1057#1107#1056#177#1057#1026#1056#181#1057#8222#1056#1108#1057#8224#1056#1105#1056#1109#1056#1029#1056#1029#1056#176#1057#1039' '#1057#1027#1056#1109#1057#1027#1057#8218#1056#176#1056#1030#1056#187#1057#1039#1057#1035#1057#8240#1056#176#1057#1039', % ')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          Align = baLeft
          Top = 240.000000000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            'SESR '#1056#1169#1056#1109#1056#182#1056#1169#1056#181#1056#1030#1056#176#1057#1039' '#1057#1027#1056#1109#1057#1027#1057#8218#1056#176#1056#1030#1056#187#1057#1039#1057#1035#1057#8240#1056#176#1057#1039', % ')
          ParentFont = False
        end
        object Memo25: TfrxMemoView
          Align = baLeft
          Top = 255.000000000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            
              'SESR '#1056#1105#1056#1029#1057#8218#1056#181#1057#1026#1057#8222#1056#181#1057#1026#1056#181#1056#1029#1057#8224#1056#1105#1056#1109#1056#1029#1056#1029#1056#176#1057#1039' '#1057#1027#1056#1109#1057#1027#1057#8218#1056#176#1056#1030#1056#187#1057#1039#1057#1035#1057#8240#1056#176#1057#1039 +
              ', % ')
          ParentFont = False
        end
        object Memo26: TfrxMemoView
          Align = baLeft
          Top = 270.000000000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            
              #1056#152#1057#8218#1056#1109#1056#1110#1056#1109#1056#1030#1057#8249#1056#8470' '#1056#1111#1056#1109#1056#1108#1056#176#1056#183#1056#176#1057#8218#1056#181#1056#187#1057#1034' '#1056#1108#1056#176#1057#8225#1056#181#1057#1027#1057#8218#1056#1030#1056#176' '#1056#1111#1056#1109' '#1056#1109#1057#8364 +
              #1056#1105#1056#177#1056#1108#1056#176#1056#1112' SESR, % ')
          ParentFont = False
        end
        object Memo27: TfrxMemoView
          Align = baLeft
          Top = 285.000000000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            
              #1056#1116#1056#1109#1057#1026#1056#1112#1056#1105#1057#1026#1057#1107#1056#181#1056#1112#1056#1109#1056#181' '#1056#183#1056#1029#1056#176#1057#8225#1056#181#1056#1029#1056#1105#1056#181' SESR '#1056#1029#1056#176' '#1056#1105#1056#1029#1057#8218#1056#181#1057#1026#1056#1030#1056#176 +
              #1056#187#1056#181', % ')
          ParentFont = False
        end
        object Memo28: TfrxMemoView
          Align = baLeft
          Top = 300.000000000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            
              #1056#152#1057#8218#1056#1109#1056#1110#1056#1109#1056#1030#1057#8249#1056#8470' '#1056#1111#1056#1109#1056#1108#1056#176#1056#183#1056#176#1057#8218#1056#181#1056#187#1057#1034' '#1056#1029#1056#181#1056#1110#1056#1109#1057#8218#1056#1109#1056#1030#1056#1029#1056#1109#1057#1027#1057#8218#1056#1105' '#1056 +
              #1113#1056#1029#1056#1110', %')
          ParentFont = False
        end
        object Memo29: TfrxMemoView
          Align = baLeft
          Top = 315.000000000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            
              #1056#1116#1056#1109#1057#1026#1056#1112#1056#1105#1057#1026#1057#1107#1056#181#1056#1112#1056#1109#1056#181' '#1056#183#1056#1029#1056#176#1057#8225#1056#181#1056#1029#1056#1105#1056#181' '#1056#1113#1056#1029#1056#1110' '#1056#1029#1056#176' '#1056#1105#1056#1029#1057#8218#1056#181#1057#1026#1056#1030 +
              #1056#176#1056#187#1056#181', % ')
          ParentFont = False
        end
        object Memo31: TfrxMemoView
          Align = baLeft
          Top = 375.000000000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            'ESR '#1057#8218#1057#1026#1056#181#1056#177', % ')
          ParentFont = False
        end
        object Memo32: TfrxMemoView
          Align = baLeft
          Top = 330.000000000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            'BBER '#1056#1029#1056#1109#1057#1026#1056#1112', %')
          ParentFont = False
        end
        object Memo33: TfrxMemoView
          Align = baLeft
          Top = 360.000000000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            'BBER '#1057#8218#1057#1026#1056#181#1056#177', % ')
          ParentFont = False
        end
        object Memo34: TfrxMemoView
          Align = baLeft
          Top = 345.000000000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            'ESR '#1056#1029#1056#1109#1057#1026#1056#1112', % ')
          ParentFont = False
        end
        object Memo35: TfrxMemoView
          Align = baLeft
          Top = 390.000000000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1119#1057#1026#1056#1105#1056#1110#1056#1109#1056#1169#1056#1029#1056#1109#1057#1027#1057#8218#1057#1034' ')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Align = baLeft
          Left = 310.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Site1."name"]')
          ParentFont = False
        end
        object Memo36: TfrxMemoView
          Align = baLeft
          Left = 485.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Site2."name"]')
          ParentFont = False
        end
        object Memo37: TfrxMemoView
          Align = baLeft
          Left = 310.000000000000000000
          Top = 30.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."azimuth1"]')
          ParentFont = False
        end
        object Memo38: TfrxMemoView
          Align = baLeft
          Left = 485.000000000000000000
          Top = 30.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."azimuth2"]')
          ParentFont = False
        end
        object LinkEnd2threshold_BER_3: TfrxMemoView
          Align = baLeft
          Left = 310.000000000000000000
          Top = 165.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd1."threshold_BER_3"]')
          ParentFont = False
        end
        object LinkEnd2threshold_BER_6: TfrxMemoView
          Align = baLeft
          Left = 310.000000000000000000
          Top = 180.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd1."threshold_BER_6"]')
          ParentFont = False
        end
        object Memo41: TfrxMemoView
          Align = baLeft
          Left = 310.000000000000000000
          Top = 105.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd1."antenna_diameter"]')
          ParentFont = False
        end
        object Memo44: TfrxMemoView
          Align = baLeft
          Left = 310.000000000000000000
          Top = 120.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd1."antenna_polarization"]')
          ParentFont = False
        end
        object Memo50: TfrxMemoView
          Align = baLeft
          Left = 310.000000000000000000
          Top = 75.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd1."bitrate_Mbps"]')
          ParentFont = False
        end
        object Memo52: TfrxMemoView
          Align = baLeft
          Left = 310.000000000000000000
          Top = 90.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd1."power_dBm"]')
          ParentFont = False
        end
        object Memo55: TfrxMemoView
          Align = baLeft
          Left = 310.000000000000000000
          Top = 45.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd1."LinkEndType_name"]')
          ParentFont = False
        end
        object Memo56: TfrxMemoView
          Align = baLeft
          Left = 310.000000000000000000
          Top = 60.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Linkend1."tx_freq_MHz"]')
          ParentFont = False
        end
        object Memo57: TfrxMemoView
          Align = baLeft
          Left = 485.000000000000000000
          Top = 45.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd2."LinkEndType_name"]')
          ParentFont = False
        end
        object Memo39: TfrxMemoView
          Align = baLeft
          Left = 485.000000000000000000
          Top = 60.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Linkend2."tx_freq_MHz"]')
          ParentFont = False
        end
        object Memo40: TfrxMemoView
          Align = baLeft
          Left = 485.000000000000000000
          Top = 75.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd2."bitrate_Mbps"]')
          ParentFont = False
        end
        object Memo46: TfrxMemoView
          Align = baLeft
          Left = 485.000000000000000000
          Top = 90.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd2."power_dBm"]')
          ParentFont = False
        end
        object Memo48: TfrxMemoView
          Align = baLeft
          Left = 485.000000000000000000
          Top = 165.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd2."threshold_BER_3"]')
          ParentFont = False
        end
        object Memo53: TfrxMemoView
          Align = baLeft
          Left = 485.000000000000000000
          Top = 180.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd2."threshold_BER_6"]')
          ParentFont = False
        end
        object Memo54: TfrxMemoView
          Align = baLeft
          Left = 310.000000000000000000
          Top = 375.000000000000000000
          Width = 350.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Link."ESR_REQUIRED"]')
          ParentFont = False
        end
        object Memo58: TfrxMemoView
          Align = baLeft
          Left = 310.000000000000000000
          Top = 390.000000000000000000
          Width = 350.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Link."status_str"]')
          ParentFont = False
        end
        object Memo59: TfrxMemoView
          Align = baLeft
          Left = 310.000000000000000000
          Top = 345.000000000000000000
          Width = 350.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Link."ESR_NORM"]')
          ParentFont = False
        end
        object Memo60: TfrxMemoView
          Align = baLeft
          Left = 310.000000000000000000
          Top = 360.000000000000000000
          Width = 350.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Link."BBER_REQUIRED"]')
          ParentFont = False
        end
        object Memo61: TfrxMemoView
          Align = baLeft
          Left = 310.000000000000000000
          Top = 315.000000000000000000
          Width = 350.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Link."KNG_NORM"]')
          ParentFont = False
        end
        object Memo62: TfrxMemoView
          Align = baLeft
          Left = 310.000000000000000000
          Top = 330.000000000000000000
          Width = 350.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Link."BBER_NORM"]')
          ParentFont = False
        end
        object Memo63: TfrxMemoView
          Align = baLeft
          Left = 310.000000000000000000
          Top = 285.000000000000000000
          Width = 350.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Link."SESR_NORM"]')
          ParentFont = False
        end
        object Memo64: TfrxMemoView
          Align = baLeft
          Left = 310.000000000000000000
          Top = 300.000000000000000000
          Width = 350.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Link."KNG"]')
          ParentFont = False
        end
        object Memo67: TfrxMemoView
          Align = baLeft
          Left = 310.000000000000000000
          Top = 255.000000000000000000
          Width = 350.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Link."SESR_INTERFERENCE"]')
          ParentFont = False
        end
        object Memo69: TfrxMemoView
          Align = baLeft
          Left = 310.000000000000000000
          Top = 270.000000000000000000
          Width = 350.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Link."SESR"]')
          ParentFont = False
        end
        object Memo71: TfrxMemoView
          Align = baLeft
          Left = 310.000000000000000000
          Top = 225.000000000000000000
          Width = 350.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Link."SESR_SUBREFRACTION"]')
          ParentFont = False
        end
        object Memo73: TfrxMemoView
          Align = baLeft
          Left = 310.000000000000000000
          Top = 240.000000000000000000
          Width = 350.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Link."SESR_RAIN"]')
          ParentFont = False
        end
        object Memo75: TfrxMemoView
          Align = baLeft
          Left = 310.000000000000000000
          Top = 210.000000000000000000
          Width = 350.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Link."MARGIN_DISTANCE"]')
          ParentFont = False
        end
        object Memo76: TfrxMemoView
          Align = baLeft
          Left = 310.000000000000000000
          Top = 195.000000000000000000
          Width = 350.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Link."MARGIN_HEIGHT"]')
          ParentFont = False
        end
        object Memo88: TfrxMemoView
          Align = baLeft
          Left = 485.000000000000000000
          Top = 105.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd2."antenna_diameter"]')
          ParentFont = False
        end
        object Memo84: TfrxMemoView
          Align = baLeft
          Left = 485.000000000000000000
          Top = 120.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd2."antenna_polarization"]')
          ParentFont = False
        end
        object Memo85: TfrxMemoView
          Align = baLeft
          Left = 310.000000000000000000
          Top = 135.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd1."rx_level_dBm"]')
          ParentFont = False
        end
        object Memo86: TfrxMemoView
          Align = baLeft
          Left = 485.000000000000000000
          Top = 135.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd2."rx_level_dBm"]')
          ParentFont = False
        end
        object Memo87: TfrxMemoView
          Align = baLeft
          Left = 310.000000000000000000
          Top = 150.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd1."kratnostBySpace_name"]')
          ParentFont = False
        end
        object Memo94: TfrxMemoView
          Align = baLeft
          Left = 485.000000000000000000
          Top = 150.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd2."kratnostBySpace_name"]')
          ParentFont = False
        end
        object Memo131: TfrxMemoView
          Align = baLeft
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          ParentFont = False
        end
        object Memo65: TfrxMemoView
          Align = baLeft
          Left = 310.000000000000000000
          Top = 15.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          DataField = 'antenna_height'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd1."antenna_height"]')
          ParentFont = False
        end
        object Memo102: TfrxMemoView
          Align = baLeft
          Left = 485.000000000000000000
          Top = 15.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd2."antenna_height"]')
          ParentFont = False
        end
      end
    end
    object Page6: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object Header1: TfrxHeader
        Height = 35.000000000000000000
        Top = 16.000000000000000000
        Width = 718.110700000000000000
        object Memo93: TfrxMemoView
          Align = baLeft
          Width = 60.000000000000000000
          Height = 35.000000000000000000
          Visible = False
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            'Index')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Align = baLeft
          Left = 60.000000000000000000
          Width = 100.000000000000000000
          Height = 35.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#160#1056#176#1057#1027#1057#1027#1057#8218#1056#1109#1057#1039#1056#1029#1056#1105#1056#181', '#1056#1108#1056#1112)
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Align = baLeft
          Left = 260.000000000000000000
          Width = 100.000000000000000000
          Height = 35.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1113#1057#1026#1056#1105#1056#1030#1056#1105#1056#183#1056#1029#1056#176' '#1056#183#1056#181#1056#1112#1056#187#1056#1105)
          ParentFont = False
        end
        object Memo120: TfrxMemoView
          Align = baLeft
          Left = 360.000000000000000000
          Width = 100.000000000000000000
          Height = 35.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#8217#1057#8249#1057#1027#1056#1109#1057#8218#1056#176' '#1056#1114#1056#1119)
          ParentFont = False
        end
        object Memo132: TfrxMemoView
          Align = baLeft
          Left = 160.000000000000000000
          Width = 100.000000000000000000
          Height = 35.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#8217#1057#8249#1057#1027#1056#1109#1057#8218#1056#176' '#1057#1026#1056#181#1056#187#1057#1034#1056#181#1057#8222#1056#176)
          ParentFont = False
        end
        object Memo101: TfrxMemoView
          Align = baLeft
          Left = 460.000000000000000000
          Width = 196.000000000000000000
          Height = 35.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1118#1056#1105#1056#1111' '#1056#1114#1056#1119)
          ParentFont = False
        end
      end
      object MasterData5: TfrxMasterData
        Height = 16.000000000000000000
        Top = 76.000000000000000000
        Width = 718.110700000000000000
        RowCount = 0
        object Memo89: TfrxMemoView
          Align = baLeft
          Left = 60.000000000000000000
          Width = 100.000000000000000000
          Height = 16.000000000000000000
          ShowHint = False
          DataField = 'distance_km'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Profile."distance_km"]')
          ParentFont = False
        end
        object Memo90: TfrxMemoView
          Align = baLeft
          Width = 60.000000000000000000
          Height = 16.000000000000000000
          Visible = False
          ShowHint = False
          DataField = 'index'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Profile."index"]')
          ParentFont = False
        end
        object Memo91: TfrxMemoView
          Align = baLeft
          Left = 260.000000000000000000
          Width = 100.000000000000000000
          Height = 16.000000000000000000
          ShowHint = False
          DataField = 'earth_h'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Profile."earth_h"]')
          ParentFont = False
        end
        object Memo92: TfrxMemoView
          Align = baLeft
          Left = 160.000000000000000000
          Width = 100.000000000000000000
          Height = 16.000000000000000000
          OnBeforePrint = 'Memo92OnBeforePrint'
          ShowHint = False
          DataField = 'ground_h'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Profile."ground_h"]')
          ParentFont = False
        end
        object Memo133: TfrxMemoView
          Align = baLeft
          Left = 360.000000000000000000
          Width = 100.000000000000000000
          Height = 16.000000000000000000
          ShowHint = False
          DataField = 'clutter_h'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Profile."clutter_h"]')
          ParentFont = False
        end
        object Shape_Color: TfrxShapeView
          Align = baLeft
          Left = 460.000000000000000000
          Width = 20.000000000000000000
          Height = 16.000000000000000000
          OnBeforePrint = 'Shape_ColorOnBeforePrint'
          ShowHint = False
          Color = clTeal
        end
        object Memo104: TfrxMemoView
          Align = baLeft
          Left = 480.000000000000000000
          Width = 176.000000000000000000
          Height = 16.000000000000000000
          ShowHint = False
          DataField = 'clutter_name'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Profile."clutter_name"]')
          ParentFont = False
        end
      end
      object Footer1: TfrxFooter
        Height = 8.000000000000000000
        Top = 116.000000000000000000
        Width = 718.110700000000000000
      end
    end
    object Page8: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object DetailData1: TfrxDetailData
        Height = 16.000000000000000000
        Top = 108.000000000000000000
        Width = 718.110700000000000000
        RowCount = 0
        object Itemscaption: TfrxMemoView
          Align = baLeft
          Left = 60.000000000000000000
          Width = 320.000000000000000000
          Height = 16.000000000000000000
          ShowHint = False
          DataField = 'caption'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Items."caption"]')
          ParentFont = False
        end
        object Memo77: TfrxMemoView
          Align = baWidth
          Left = 380.000000000000000000
          Width = 116.000000000000000000
          Height = 16.000000000000000000
          ShowHint = False
          DataField = 'value'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Items."value"]')
          ParentFont = False
        end
        object Memo78: TfrxMemoView
          Align = baLeft
          Left = 496.000000000000000000
          Width = 116.000000000000000000
          Height = 16.000000000000000000
          ShowHint = False
          DataField = 'value2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Items."value2"]')
          ParentFont = False
        end
        object Memo79: TfrxMemoView
          Align = baLeft
          Width = 60.000000000000000000
          Height = 16.000000000000000000
          ShowHint = False
          DataField = 'id'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Items."id"]')
          ParentFont = False
        end
      end
      object MasterData6: TfrxMasterData
        Height = 24.000000000000000000
        Top = 60.000000000000000000
        Width = 718.110700000000000000
        RowCount = 0
        object groupcaption: TfrxMemoView
          Align = baLeft
          Left = 60.000000000000000000
          Width = 552.000000000000000000
          Height = 16.000000000000000000
          ShowHint = False
          DataField = 'caption'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[group."caption"]')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Align = baLeft
          Width = 60.000000000000000000
          Height = 16.000000000000000000
          ShowHint = False
          DataField = 'id'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[group."id"]')
          ParentFont = False
        end
      end
      object Header2: TfrxHeader
        Height = 20.000000000000000000
        Top = 16.000000000000000000
        Width = 718.110700000000000000
      end
      object Footer2: TfrxFooter
        Height = 20.000000000000000000
        Top = 148.000000000000000000
        Width = 718.110700000000000000
      end
      object Memo103: TfrxMemoView
        Align = baLeft
        Top = 15.333333330000000000
        Width = 434.000000000000000000
        Height = 16.666666670000000000
        ShowHint = False
        AutoWidth = True
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        Memo.UTF8 = (
          #1056#160#1056#181#1056#183#1057#1107#1056#187#1057#1034#1057#8218#1056#176#1057#8218#1057#8249' '#1057#1026#1056#176#1057#1027#1057#8225#1056#181#1057#8218#1056#1109#1056#1030)
        ParentFont = False
      end
    end
    object Subreport_right_: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object MasterData7: TfrxMasterData
        Height = 143.000000000000000000
        Top = 16.000000000000000000
        Width = 718.110700000000000000
        RowCount = 0
        object Memo105: TfrxMemoView
          Align = baRight
          Left = 478.110699999999900000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          ParentFont = False
        end
        object Memo106: TfrxMemoView
          Align = baRight
          Left = 588.110700000000000000
          Width = 130.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Site2."name"]')
          ParentFont = False
        end
        object Memo107: TfrxMemoView
          Align = baRight
          Left = 478.110699999999900000
          Top = 15.000000000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1113#1056#1109#1056#1109#1057#1026#1056#1169#1056#1105#1056#1029#1056#176#1057#8218#1057#8249' '#1056#1038#1056#1113'-42')
          ParentFont = False
        end
        object Memo108: TfrxMemoView
          Align = baRight
          Left = 478.110699999999900000
          Top = 45.000000000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1113#1056#1109#1056#1109#1057#1026#1056#1169#1056#1105#1056#1029#1056#176#1057#8218#1057#8249' WGS')
          ParentFont = False
        end
        object Memo109: TfrxMemoView
          Align = baRight
          Left = 478.110699999999900000
          Top = 75.000000000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#8217#1057#8249#1057#1027#1056#1109#1057#8218#1056#176' '#1056#1111#1056#1109#1056#1169#1056#1030#1056#181#1057#1027#1056#176', m')
          ParentFont = False
        end
        object Memo110: TfrxMemoView
          Align = baRight
          Left = 478.110699999999900000
          Top = 90.000000000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1106#1056#183#1056#1105#1056#1112#1057#1107#1057#8218)
          ParentFont = False
        end
        object Memo111: TfrxMemoView
          Align = baRight
          Left = 478.110699999999900000
          Top = 105.000000000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1032#1056#1110#1056#1109#1056#187' '#1056#1112#1056#181#1057#1027#1057#8218#1056#176)
          ParentFont = False
        end
        object Memo112: TfrxMemoView
          Align = baRight
          Left = 478.110699999999900000
          Top = 120.000000000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#8221#1056#1105#1056#176#1056#1112#1056#181#1057#8218#1057#1026' '#1056#176#1056#1029#1057#8218#1056#181#1056#1029#1056#1029#1057#8249)
          ParentFont = False
        end
        object Memo113: TfrxMemoView
          Align = baRight
          Left = 588.110700000000000000
          Top = 15.000000000000000000
          Width = 130.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Site2."lat_str"]')
          ParentFont = False
        end
        object Memo114: TfrxMemoView
          Align = baRight
          Left = 588.110700000000000000
          Top = 45.000000000000000000
          Width = 130.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Site2."lat_wgs_str"]')
          ParentFont = False
        end
        object Memo115: TfrxMemoView
          Align = baRight
          Left = 588.110700000000000000
          Top = 75.000000000000000000
          Width = 130.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd2."antenna_height"]')
          ParentFont = False
        end
        object Memo116: TfrxMemoView
          Align = baRight
          Left = 588.110700000000000000
          Top = 90.000000000000000000
          Width = 130.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."azimuth2"]')
          ParentFont = False
        end
        object Memo117: TfrxMemoView
          Align = baRight
          Left = 588.110700000000000000
          Top = 120.000000000000000000
          Width = 130.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd2."antenna_diameter"]')
          ParentFont = False
        end
        object Memo118: TfrxMemoView
          Align = baRight
          Left = 588.110700000000000000
          Top = 60.000000000000000000
          Width = 130.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Site2."lon_wgs_str"]')
          ParentFont = False
        end
        object Memo119: TfrxMemoView
          Align = baRight
          Left = 588.110700000000000000
          Top = 30.000000000000000000
          Width = 130.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Site2."lon_str"]')
          ParentFont = False
        end
        object Memo129: TfrxMemoView
          Align = baRight
          Left = 478.110699999999900000
          Top = 30.000000000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          ParentFont = False
        end
        object Memo130: TfrxMemoView
          Align = baRight
          Left = 478.110699999999900000
          Top = 60.000000000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Align = baRight
          Left = 588.110700000000000000
          Top = 105.000000000000000000
          Width = 130.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."ELEVATION_angle_2"]')
          ParentFont = False
        end
      end
    end
  end
  object frxDesigner1: TfrxDesigner
    DefaultScriptLanguage = 'PascalScript'
    DefaultFont.Charset = DEFAULT_CHARSET
    DefaultFont.Color = clWindowText
    DefaultFont.Height = -13
    DefaultFont.Name = 'Arial'
    DefaultFont.Style = []
    DefaultLeftMargin = 10.000000000000000000
    DefaultRightMargin = 10.000000000000000000
    DefaultTopMargin = 10.000000000000000000
    DefaultBottomMargin = 10.000000000000000000
    DefaultPaperSize = 9
    DefaultOrientation = poPortrait
    TemplatesExt = 'fr3'
    Restrictions = []
    RTLLanguage = False
    MemoParentFont = False
    Left = 408
    Top = 80
  end
end
