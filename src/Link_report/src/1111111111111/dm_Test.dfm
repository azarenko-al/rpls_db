object dmTest: TdmTest
  OldCreateOrder = False
  Left = 709
  Top = 347
  Height = 471
  Width = 811
  object frxReport_short_WGS: TfrxReport
    Version = '4.15.6'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports2'
    PreviewOptions.AllowEdit = False
    PreviewOptions.Buttons = [pbPrint, pbExport, pbZoom, pbTools, pbNavigator, pbExportQuick, pbNoFullScreen]
    PreviewOptions.ThumbnailVisible = True
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = #1055#1086' '#1091#1084#1086#1083#1095#1072#1085#1080#1102
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 41730.469225335600000000
    ReportOptions.LastChange = 41939.805246851900000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      ''
      '         '
      ''
      ''
      'procedure frxReportOnStartReport(Sender: TfrxComponent);'
      'const'
      '  DEF_MAP_H = 800;'
      '  '
      '  DEF_H = 450;'
      '  DEF_OFFSET = 25;'
      ''
      '  DEF_TOP = 20;                                  '
      'begin'
      '//  Picture_map.Width:=200;                '
      '  Picture_map.Height     :=DEF_MAP_H;'
      '    '
      '  Picture_profile.Height :=DEF_H;                '
      '            '
      '  '
      '  Child_map.Height:=DEF_MAP_H;'
      '    '
      '  Child_profile.Height:=DEF_H + DEF_OFFSET;              '
      '        '
      '      '
      'end;  '
      '             '
      '         '
      ''
      '//------------------------------------------------------------'
      'function DegreeToStr (aValue: double): String;'
      '//------------------------------------------------------------'
      'const'
      '//  STR_BLPOINT_FORMAT1 = '#39'%d'#176'%.2d'#39#39'%s%1.2f'#39#39#39#39#39';'
      '  STR_BLPOINT_FORMAT1 = '#39'%d'#176' %.2d'#39#39' %1.2f'#39#39#39#39#39';'
      'var    '
      ' iDeg,iMin:integer; '
      ' eSec: Double;   '
      '           '
      'begin'
      '  geo_DecodeDegree_(aValue, iDeg,iMin,eSec);'
      '                           '
      ' // angle.Sec:=TruncFloat(angle.Sec,2);'
      ' '
      '    Result:=Format(STR_BLPOINT_FORMAT1, [iDeg, iMin, eSec]);'
      ''
      '  if aValue<0 then'
      '    Result:='#39'-'#39'+Result;'
      'end;'
      '  '
      '           '
      ''
      '//------------------------------------------------------------'
      
        'procedure geo_DecodeDegree_(aValue: Double; var aDeg,aMin:intege' +
        'r; var aSec:'
      '    Double);'
      '//------------------------------------------------------------'
      'const'
      ' EXP_ = 0.000000001;'
      '     //97,033333333'
      'var'
      '  bIsNegative: Boolean;'
      '  e: Double;'
      '  eMinutes: Double;'
      '  eSec: Double;'
      '  eSeconds: Double;'
      'begin'
      '  bIsNegative := aValue<0;'
      ''
      '  aValue :=Abs(aValue);'
      ''
      '  aDeg:=Trunc(aValue);'
      ''
      ' // e:=Abs(aValue)*60;'
      ''
      '//  , Frac(X) = X - Int(X).'
      ''
      '  aMin:=Trunc(Frac(aValue)*60);'
      ''
      '//  Result.Sec:=Frac(Frac(Abs(aValue))*60)*60;'
      '  e:= (aValue - Trunc(aValue)) * 60;'
      '  aMin:=Trunc(e);'
      ''
      ' eMinutes := Trunc (e);'
      ' eSeconds := (e - Trunc (e)) * 60 ;'
      ''
      ''
      '  aSec:=(((aValue - aDeg) *60) - aMin) * 60;'
      ''
      '  if (aSec - 60) > EXP_ then'
      '  begin'
      '    Inc(aMin);'
      '    aSec := 0;'
      '  end;'
      ''
      ''
      '  if bIsNegative then'
      '    aDeg:=-aDeg;'
      '          '
      'end;'
      '        '
      ''
      '       '
      'begin'
      ''
      'end.    ')
    OnStartReport = 'frxReportOnStartReport'
    Left = 310
    Top = 68
    Datasets = <
      item
      end
      item
      end
      item
      end
      item
      end
      item
      end
      item
      end
      item
      end
      item
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 9.260416666666670000
      BottomMargin = 10.583333333333300000
      LargeDesignHeight = True
      OnBeforePrint = 'Page1OnBeforePrint'
      object Child_Header: TfrxChild
        Height = 151.881880000000000000
        Top = 181.417440000000000000
        Width = 718.110700000000000000
        Child = frxReport_short_WGS.Child_Summary
        object Memo105: TfrxMemoView
          Align = baRight
          Left = 478.110699999999900000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1119#1056#187#1056#1109#1057#8240#1056#176#1056#1169#1056#1108#1056#176)
          ParentFont = False
        end
        object Memo106: TfrxMemoView
          Align = baRight
          Left = 588.110700000000000000
          Width = 130.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Site2."name"]')
          ParentFont = False
        end
        object Memo108: TfrxMemoView
          Align = baRight
          Left = 478.110699999999900000
          Top = 15.000000000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1113#1056#1109#1056#1109#1057#1026#1056#1169#1056#1105#1056#1029#1056#176#1057#8218#1057#8249' WGS')
          ParentFont = False
        end
        object Memo109: TfrxMemoView
          Align = baRight
          Left = 478.110699999999900000
          Top = 45.000000000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#8217#1057#8249#1057#1027#1056#1109#1057#8218#1056#176' '#1056#1111#1056#1109#1056#1169#1056#1030#1056#181#1057#1027#1056#176', m')
          ParentFont = False
        end
        object Memo110: TfrxMemoView
          Align = baRight
          Left = 478.110699999999900000
          Top = 60.000000000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1106#1056#183#1056#1105#1056#1112#1057#1107#1057#8218)
          ParentFont = False
        end
        object Memo111: TfrxMemoView
          Align = baRight
          Left = 478.110699999999900000
          Top = 75.000000000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1032#1056#1110#1056#1109#1056#187' '#1056#1112#1056#181#1057#1027#1057#8218#1056#176)
          ParentFont = False
        end
        object Memo112: TfrxMemoView
          Align = baRight
          Left = 478.110699999999900000
          Top = 90.000000000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#8221#1056#1105#1056#176#1056#1112#1056#181#1057#8218#1057#1026' '#1056#176#1056#1029#1057#8218#1056#181#1056#1029#1056#1029#1057#8249)
          ParentFont = False
        end
        object Memo114: TfrxMemoView
          Align = baRight
          Left = 588.110700000000000000
          Top = 15.000000000000000000
          Width = 130.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[DegreeToStr (<Site2."lat_wgs">)]')
          ParentFont = False
        end
        object Memo115: TfrxMemoView
          Align = baRight
          Left = 588.110700000000000000
          Top = 45.000000000000000000
          Width = 130.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[antenna2."height"]')
          ParentFont = False
        end
        object Memo116: TfrxMemoView
          Align = baRight
          Left = 588.110700000000000000
          Top = 60.000000000000000000
          Width = 130.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."azimuth2"]')
          ParentFont = False
        end
        object Memo117: TfrxMemoView
          Align = baRight
          Left = 588.110700000000000000
          Top = 90.000000000000000000
          Width = 130.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Antenna2."diameter"]')
          ParentFont = False
        end
        object Memo118: TfrxMemoView
          Align = baRight
          Left = 588.110700000000000000
          Top = 30.000000000000000000
          Width = 130.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[DegreeToStr (<Site2."lon_wgs">)]')
          ParentFont = False
        end
        object Memo130: TfrxMemoView
          Align = baRight
          Left = 478.110699999999900000
          Top = 30.000000000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Align = baRight
          Left = 588.110700000000000000
          Top = 75.000000000000000000
          Width = 130.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."ELEVATION_angle_2"]')
          ParentFont = False
        end
        object Memo66: TfrxMemoView
          Align = baRight
          Left = 478.110699999999900000
          Top = 105.000000000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1032#1057#1027#1056#1105#1056#187#1056#181#1056#1029#1056#1105#1056#181' '#1056#176#1056#1029#1057#8218#1056#181#1056#1029#1056#1029#1057#8249)
          ParentFont = False
        end
        object Memo99: TfrxMemoView
          Align = baRight
          Left = 588.110700000000000000
          Top = 105.000000000000000000
          Width = 130.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Antenna2."gain"]')
          ParentFont = False
        end
        object Memo43: TfrxMemoView
          Align = baLeft
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1119#1056#187#1056#1109#1057#8240#1056#176#1056#1169#1056#1108#1056#176)
          ParentFont = False
        end
        object Memo_left_1: TfrxMemoView
          Align = baLeft
          Left = 110.000000000000000000
          Width = 125.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Site1."name"]')
          ParentFont = False
        end
        object Memo68: TfrxMemoView
          Align = baLeft
          Top = 15.000000000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1113#1056#1109#1056#1109#1057#1026#1056#1169#1056#1105#1056#1029#1056#176#1057#8218#1057#8249' WGS')
          ParentFont = False
        end
        object Memo70: TfrxMemoView
          Align = baLeft
          Top = 45.000000000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#8217#1057#8249#1057#1027#1056#1109#1057#8218#1056#176' '#1056#1111#1056#1109#1056#1169#1056#1030#1056#181#1057#1027#1056#176', m')
          ParentFont = False
        end
        object Memo72: TfrxMemoView
          Align = baLeft
          Top = 60.000000000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1106#1056#183#1056#1105#1056#1112#1057#1107#1057#8218)
          ParentFont = False
        end
        object Memo74: TfrxMemoView
          Align = baLeft
          Top = 75.000000000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1032#1056#1110#1056#1109#1056#187' '#1056#1112#1056#181#1057#1027#1057#8218#1056#176)
          ParentFont = False
        end
        object Memo100: TfrxMemoView
          Align = baLeft
          Top = 90.000000000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#8221#1056#1105#1056#176#1056#1112#1056#181#1057#8218#1057#1026' '#1056#176#1056#1029#1057#8218#1056#181#1056#1029#1056#1029#1057#8249)
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Align = baLeft
          Left = 110.000000000000000000
          Top = 15.000000000000000000
          Width = 125.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[DegreeToStr (<Site1."lat_WGS">)]')
          ParentFont = False
        end
        object Memo1: TfrxMemoView
          Align = baLeft
          Left = 110.000000000000000000
          Top = 45.000000000000000000
          Width = 125.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[antenna1."height"]')
          ParentFont = False
        end
        object Memo95: TfrxMemoView
          Align = baLeft
          Left = 110.000000000000000000
          Top = 60.000000000000000000
          Width = 125.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."azimuth1"]')
          ParentFont = False
        end
        object Memo97: TfrxMemoView
          Align = baLeft
          Left = 110.000000000000000000
          Top = 90.000000000000000000
          Width = 125.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[antenna1."diameter"]')
          ParentFont = False
        end
        object Memo98: TfrxMemoView
          Align = baLeft
          Left = 110.000000000000000000
          Top = 30.000000000000000000
          Width = 125.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[DegreeToStr (<Site1."lon_WGS">)]')
          ParentFont = False
        end
        object Memo96: TfrxMemoView
          Align = baLeft
          Left = 110.000000000000000000
          Top = 75.000000000000000000
          Width = 125.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."ELEVATION_angle_1"]')
          ParentFont = False
        end
        object Memo121: TfrxMemoView
          Align = baLeft
          Top = 30.000000000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Align = baLeft
          Top = 105.000000000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1032#1057#1027#1056#1105#1056#187#1056#181#1056#1029#1056#1105#1056#181' '#1056#176#1056#1029#1057#8218#1056#181#1056#1029#1056#1029#1057#8249)
          ParentFont = False
        end
        object Memo30: TfrxMemoView
          Align = baLeft
          Left = 110.000000000000000000
          Top = 105.000000000000000000
          Width = 125.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[antenna1."gain"]')
          ParentFont = False
        end
        object Memo45: TfrxMemoView
          Left = 247.000000000000000000
          Top = 15.000000000000000000
          Width = 144.204726490000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#8221#1056#187#1056#1105#1056#1029#1056#176' '#1056#1105#1056#1029#1057#8218#1056#181#1057#1026#1056#1030#1056#176#1056#187#1056#176', km')
          ParentFont = False
        end
        object Memo47: TfrxMemoView
          Left = 247.000000000000000000
          Top = 30.000000000000000000
          Width = 144.204726490000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1032#1057#1026#1056#1109#1056#1030#1056#181#1056#1029#1057#1034' '#1057#1027#1056#1105#1056#1110#1056#1029#1056#176#1056#187#1056#176', dBm')
          ParentFont = False
        end
        object Memo49: TfrxMemoView
          Left = 247.000000000000000000
          Top = 45.000000000000000000
          Width = 144.204726490000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            'P'#1056#176#1056#177#1056#1109#1057#8225#1056#176#1057#1039' '#1057#8225#1056#176#1057#1027#1057#8218#1056#1109#1057#8218#1056#176', GHz')
          ParentFont = False
        end
        object Memo51: TfrxMemoView
          Left = 247.000000000000000000
          Top = 60.000000000000000000
          Width = 144.204726490000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1114#1056#1109#1057#8240#1056#1029#1056#1109#1057#1027#1057#8218#1057#1034' '#1056#1111#1056#181#1057#1026#1056#181#1056#1169#1056#176#1057#8218#1057#8225#1056#1105#1056#1108#1056#176', dBm')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          Left = 247.000000000000000000
          Top = 75.000000000000000000
          Width = 144.204726490000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1038#1056#1108#1056#1109#1057#1026#1056#1109#1057#1027#1057#8218#1057#1034' '#1056#1111#1056#181#1057#1026#1056#181#1056#1169#1056#176#1057#8225#1056#1105', Mbit/s')
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          Left = 247.000000000000000000
          Top = 90.000000000000000000
          Width = 144.204726490000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1118#1056#1105#1056#1111' '#1056#1109#1056#177#1056#1109#1057#1026#1057#1107#1056#1169#1056#1109#1056#1030#1056#176#1056#1029#1056#1105#1057#1039)
          ParentFont = False
        end
        object Memo123: TfrxMemoView
          Left = 391.204726490000000000
          Top = 30.000000000000000000
          Width = 74.905973510000000000
          Height = 15.000000000000000000
          ShowHint = False
          DataField = 'rx_level_dBm'
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd1."rx_level_dBm"]')
          ParentFont = False
        end
        object Memo124: TfrxMemoView
          Left = 391.204726490000000000
          Top = 75.000000000000000000
          Width = 74.905973510000000000
          Height = 15.000000000000000000
          ShowHint = False
          DataField = 'bitrate_Mbps'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd1."bitrate_Mbps"]')
          ParentFont = False
        end
        object Memo125: TfrxMemoView
          Left = 391.204726490000000000
          Top = 90.000000000000000000
          Width = 74.905973510000000000
          Height = 15.000000000000000000
          ShowHint = False
          DataField = 'LinkEndType_name'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd1."LinkEndType_name"]')
          ParentFont = False
        end
        object Memo127: TfrxMemoView
          Left = 391.204726490000000000
          Top = 45.000000000000000000
          Width = 74.905973510000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[FloatToStr( <LinkEnd1."tx_freq_MHz"> / 1000 )]')
          ParentFont = False
        end
        object Memo126: TfrxMemoView
          Left = 391.204726490000000000
          Top = 60.000000000000000000
          Width = 74.905973510000000000
          Height = 15.000000000000000000
          ShowHint = False
          DataField = 'power_dBm'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd1."power_dBm"]')
          ParentFont = False
        end
        object Memo42: TfrxMemoView
          Left = 247.000000000000000000
          Width = 144.204726490000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1119#1057#1026#1056#1105#1056#183#1056#1029#1056#176#1056#1108' '#1056#1111#1057#1026#1056#1105#1056#1110#1056#1109#1056#1169#1056#1029#1056#1109#1057#1027#1057#8218#1056#1105)
          ParentFont = False
        end
        object Memo122: TfrxMemoView
          Left = 391.204726490000000000
          Width = 74.905973510000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."status_str"]')
          ParentFont = False
        end
        object Memo134: TfrxMemoView
          Left = 247.000000000000000000
          Top = 105.000000000000000000
          Width = 144.204726490000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#8212#1056#176#1056#1111#1056#176#1057#1027' '#1056#1029#1056#176' '#1056#183#1056#176#1056#1112#1056#1105#1057#1026#1056#176#1056#1029#1056#1105#1057#1039', dB')
          ParentFont = False
        end
        object Memo135: TfrxMemoView
          Left = 391.204726490000000000
          Top = 105.000000000000000000
          Width = 74.905973510000000000
          Height = 15.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."fade_margin_dB"]')
          ParentFont = False
        end
        object Memo128: TfrxMemoView
          Align = baLeft
          Left = 391.204726490000000000
          Top = 15.000000000000000000
          Width = 74.905973510000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."length_km"]')
          ParentFont = False
        end
      end
      object Child_Summary: TfrxChild
        Height = 424.000000000000000000
        Top = 355.275820000000000000
        Width = 718.110700000000000000
        Child = frxReport_short_WGS.Child_map
        object Memo3: TfrxMemoView
          Align = baLeft
          Top = 15.000000000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          GroupIndex = 1
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#8217#1057#8249#1057#1027#1056#1109#1057#8218#1056#176', m')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Align = baLeft
          Top = 30.000000000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          GroupIndex = 1
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1106#1056#183#1056#1105#1056#1112#1057#1107#1057#8218' ')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Align = baLeft
          Top = 45.000000000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          GroupIndex = 1
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1118#1056#1105#1056#1111' '#1056#1109#1056#177#1056#1109#1057#1026#1057#1107#1056#1169#1056#1109#1056#1030#1056#176#1056#1029#1056#1105#1057#1039' ')
          ParentFont = False
        end
        object Memo77: TfrxMemoView
          Align = baLeft
          Top = 60.000000000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          GroupIndex = 1
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#167#1056#176#1057#1027#1057#8218#1056#1109#1057#8218#1056#176' '#1056#1111#1056#181#1057#1026#1056#181#1056#1169#1056#176#1057#8225#1056#1105', MHz')
          ParentFont = False
        end
        object Memo78: TfrxMemoView
          Align = baLeft
          Top = 75.000000000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          GroupIndex = 1
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1038#1056#1108#1056#1109#1057#1026#1056#1109#1057#1027#1057#8218#1057#1034' '#1056#1111#1056#181#1057#1026#1056#181#1056#1169#1056#176#1057#8225#1056#1105', Mbit/s')
          ParentFont = False
        end
        object Memo79: TfrxMemoView
          Align = baLeft
          Top = 90.000000000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          GroupIndex = 1
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1114#1056#1109#1057#8240#1056#1029#1056#1109#1057#1027#1057#8218#1057#1034', dBm')
          ParentFont = False
        end
        object Memo81: TfrxMemoView
          Align = baLeft
          Top = 105.000000000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          GroupIndex = 1
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#8221#1056#1105#1056#176#1056#1112#1056#181#1057#8218#1057#1026' '#1056#176#1056#1029#1057#8218#1056#181#1056#1029#1056#1029#1057#8249', m')
          ParentFont = False
        end
        object Memo82: TfrxMemoView
          Align = baLeft
          Top = 135.000000000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          GroupIndex = 1
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            
              #1056#1038#1057#1026#1056#181#1056#1169#1056#1029#1057#1039#1057#1039' '#1056#1112#1056#1109#1057#8240#1056#1029#1056#1109#1057#1027#1057#8218#1057#1034' '#1056#1029#1056#176' '#1056#1030#1057#8230#1056#1109#1056#1169#1056#181' '#1056#1111#1057#1026#1056#1105#1056#181#1056#1112#1056#1029#1056#1105#1056#1108 +
              #1056#176', dBm')
          ParentFont = False
        end
        object Memo83: TfrxMemoView
          Align = baLeft
          Top = 120.000000000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          GroupIndex = 1
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1119#1056#1109#1056#187#1057#1039#1057#1026#1056#1105#1056#183#1056#176#1057#8224#1056#1105#1057#1039' ')
          ParentFont = False
        end
        object Memo89: TfrxMemoView
          Align = baLeft
          Top = 150.000000000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          GroupIndex = 1
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#160#1056#181#1056#183#1056#181#1057#1026#1056#1030#1056#1105#1057#1026#1056#1109#1056#1030#1056#176#1056#1029#1056#1105#1056#181' ')
          ParentFont = False
        end
        object Memo90: TfrxMemoView
          Align = baLeft
          Top = 165.000000000000100000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          GroupIndex = 1
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1119#1056#1109#1057#1026#1056#1109#1056#1110' '#1056#1111#1057#1026#1056#1105' BER=10-3 ')
          ParentFont = False
        end
        object Memo91: TfrxMemoView
          Align = baLeft
          Top = 195.000000000000100000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          GroupIndex = 1
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1114#1056#1105#1056#1029#1056#1105#1056#1112#1056#176#1056#187#1057#1034#1056#1029#1057#8249#1056#8470' '#1056#1109#1057#8218#1056#1029#1056#1109#1057#1027#1056#1105#1057#8218#1056#181#1056#187#1057#1034#1056#1029#1057#8249#1056#8470' '#1056#1111#1057#1026#1056#1109#1057#1027#1056#1030#1056#181#1057#8218)
          ParentFont = False
        end
        object Memo92: TfrxMemoView
          Align = baLeft
          Top = 180.000000000000100000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          GroupIndex = 1
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1119#1056#1109#1057#1026#1056#1109#1056#1110' '#1056#1111#1057#1026#1056#1105' BER=10-6 ')
          ParentFont = False
        end
        object Memo93: TfrxMemoView
          Align = baLeft
          Top = 210.000000000000100000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          GroupIndex = 1
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1119#1056#1109#1056#187#1056#1109#1056#182#1056#181#1056#1029#1056#1105#1056#181' '#1057#8218#1056#1109#1057#8225#1056#1108#1056#1105' '#1056#1111#1057#1026#1056#1109#1057#1027#1056#1030#1056#181#1057#8218#1056#176', km ')
          ParentFont = False
        end
        object Memo101: TfrxMemoView
          Align = baLeft
          Top = 225.000000000000100000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          GroupIndex = 1
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            'SESR '#1057#1027#1057#1107#1056#177#1057#1026#1056#181#1057#8222#1056#1108#1057#8224#1056#1105#1056#1109#1056#1029#1056#1029#1056#176#1057#1039' '#1057#1027#1056#1109#1057#1027#1057#8218#1056#176#1056#1030#1056#187#1057#1039#1057#1035#1057#8240#1056#176#1057#1039', % ')
          ParentFont = False
        end
        object Memo103: TfrxMemoView
          Align = baLeft
          Top = 240.000000000000100000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          GroupIndex = 1
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            'SESR '#1056#1169#1056#1109#1056#182#1056#1169#1056#181#1056#1030#1056#176#1057#1039' '#1057#1027#1056#1109#1057#1027#1057#8218#1056#176#1056#1030#1056#187#1057#1039#1057#1035#1057#8240#1056#176#1057#1039', % ')
          ParentFont = False
        end
        object Memo104: TfrxMemoView
          Align = baLeft
          Top = 255.000000000000100000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          GroupIndex = 1
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            
              'SESR '#1056#1105#1056#1029#1057#8218#1056#181#1057#1026#1057#8222#1056#181#1057#1026#1056#181#1056#1029#1057#8224#1056#1105#1056#1109#1056#1029#1056#1029#1056#176#1057#1039' '#1057#1027#1056#1109#1057#1027#1057#8218#1056#176#1056#1030#1056#187#1057#1039#1057#1035#1057#8240#1056#176#1057#1039 +
              ', % ')
          ParentFont = False
        end
        object Memo107: TfrxMemoView
          Align = baLeft
          Top = 270.000000000000100000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          GroupIndex = 1
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            
              #1056#152#1057#8218#1056#1109#1056#1110#1056#1109#1056#1030#1057#8249#1056#8470' '#1056#1111#1056#1109#1056#1108#1056#176#1056#183#1056#176#1057#8218#1056#181#1056#187#1057#1034' '#1056#1108#1056#176#1057#8225#1056#181#1057#1027#1057#8218#1056#1030#1056#176' '#1056#1111#1056#1109' '#1056#1109#1057#8364 +
              #1056#1105#1056#177#1056#1108#1056#176#1056#1112' SESR, % ')
          ParentFont = False
        end
        object Memo113: TfrxMemoView
          Align = baLeft
          Top = 285.000000000000100000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          GroupIndex = 1
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            
              #1056#1116#1056#1109#1057#1026#1056#1112#1056#1105#1057#1026#1057#1107#1056#181#1056#1112#1056#1109#1056#181' '#1056#183#1056#1029#1056#176#1057#8225#1056#181#1056#1029#1056#1105#1056#181' SESR '#1056#1029#1056#176' '#1056#1105#1056#1029#1057#8218#1056#181#1057#1026#1056#1030#1056#176 +
              #1056#187#1056#181', % ')
          ParentFont = False
        end
        object Memo119: TfrxMemoView
          Align = baLeft
          Top = 300.000000000000100000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          GroupIndex = 1
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            
              #1056#152#1057#8218#1056#1109#1056#1110#1056#1109#1056#1030#1057#8249#1056#8470' '#1056#1111#1056#1109#1056#1108#1056#176#1056#183#1056#176#1057#8218#1056#181#1056#187#1057#1034' '#1056#1029#1056#181#1056#1110#1056#1109#1057#8218#1056#1109#1056#1030#1056#1029#1056#1109#1057#1027#1057#8218#1056#1105' '#1056 +
              #1113#1056#1029#1056#1110', %')
          ParentFont = False
        end
        object Memo120: TfrxMemoView
          Align = baLeft
          Top = 315.000000000000100000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          GroupIndex = 1
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            
              #1056#1116#1056#1109#1057#1026#1056#1112#1056#1105#1057#1026#1057#1107#1056#181#1056#1112#1056#1109#1056#181' '#1056#183#1056#1029#1056#176#1057#8225#1056#181#1056#1029#1056#1105#1056#181' '#1056#1113#1056#1029#1056#1110' '#1056#1029#1056#176' '#1056#1105#1056#1029#1057#8218#1056#181#1057#1026#1056#1030 +
              #1056#176#1056#187#1056#181', % ')
          ParentFont = False
        end
        object Memo129: TfrxMemoView
          Align = baLeft
          Top = 375.000000000000100000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          GroupIndex = 1
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            'ESR '#1057#8218#1057#1026#1056#181#1056#177', % ')
          ParentFont = False
        end
        object Memo132: TfrxMemoView
          Align = baLeft
          Top = 330.000000000000100000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          GroupIndex = 1
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            'BBER '#1056#1029#1056#1109#1057#1026#1056#1112', %')
          ParentFont = False
        end
        object Memo133: TfrxMemoView
          Align = baLeft
          Top = 360.000000000000100000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          GroupIndex = 1
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            'BBER '#1057#8218#1057#1026#1056#181#1056#177', % ')
          ParentFont = False
        end
        object Memo136: TfrxMemoView
          Align = baLeft
          Top = 345.000000000000100000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          GroupIndex = 1
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            'ESR '#1056#1029#1056#1109#1057#1026#1056#1112', % ')
          ParentFont = False
        end
        object Memo139: TfrxMemoView
          Align = baLeft
          Top = 390.000000000000100000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          GroupIndex = 1
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1119#1057#1026#1056#1105#1056#1110#1056#1109#1056#1169#1056#1029#1056#1109#1057#1027#1057#8218#1057#1034' ')
          ParentFont = False
        end
        object Memo140: TfrxMemoView
          Align = baLeft
          Left = 310.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          GroupIndex = 1
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Site1."name"]')
          ParentFont = False
        end
        object Memo141: TfrxMemoView
          Align = baLeft
          Left = 485.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          GroupIndex = 1
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Site2."name"]')
          ParentFont = False
        end
        object Memo142: TfrxMemoView
          Align = baLeft
          Left = 310.000000000000000000
          Top = 30.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          GroupIndex = 1
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."azimuth1"]')
          ParentFont = False
        end
        object Memo143: TfrxMemoView
          Align = baLeft
          Left = 485.000000000000000000
          Top = 30.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          GroupIndex = 1
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."azimuth2"]')
          ParentFont = False
        end
        object Memo144: TfrxMemoView
          Align = baLeft
          Left = 310.000000000000000000
          Top = 165.000000000000100000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          GroupIndex = 1
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd1."threshold_BER_3"]')
          ParentFont = False
        end
        object Memo145: TfrxMemoView
          Align = baLeft
          Left = 310.000000000000000000
          Top = 180.000000000000100000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          GroupIndex = 1
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd1."threshold_BER_6"]')
          ParentFont = False
        end
        object Memo146: TfrxMemoView
          Align = baLeft
          Left = 310.000000000000000000
          Top = 105.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          GroupIndex = 1
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[antenna1."diameter"]')
          ParentFont = False
        end
        object Memo147: TfrxMemoView
          Align = baLeft
          Left = 310.000000000000000000
          Top = 120.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          GroupIndex = 1
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[antenna1."polarization"]')
          ParentFont = False
        end
        object Memo148: TfrxMemoView
          Align = baLeft
          Left = 310.000000000000000000
          Top = 75.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          GroupIndex = 1
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd1."bitrate_Mbps"]')
          ParentFont = False
        end
        object Memo149: TfrxMemoView
          Align = baLeft
          Left = 310.000000000000000000
          Top = 90.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          GroupIndex = 1
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd1."power_dBm"]')
          ParentFont = False
        end
        object Memo150: TfrxMemoView
          Align = baLeft
          Left = 310.000000000000000000
          Top = 45.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          GroupIndex = 1
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd1."LinkEndType_name"]')
          ParentFont = False
        end
        object Memo151: TfrxMemoView
          Align = baLeft
          Left = 310.000000000000000000
          Top = 60.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          GroupIndex = 1
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd1."tx_freq_MHz"]')
          ParentFont = False
        end
        object Memo152: TfrxMemoView
          Align = baLeft
          Left = 485.000000000000000000
          Top = 45.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          GroupIndex = 1
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd2."LinkEndType_name"]')
          ParentFont = False
        end
        object Memo153: TfrxMemoView
          Align = baLeft
          Left = 485.000000000000000000
          Top = 60.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          GroupIndex = 1
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd2."tx_freq_MHz"]')
          ParentFont = False
        end
        object Memo154: TfrxMemoView
          Align = baLeft
          Left = 485.000000000000000000
          Top = 75.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          GroupIndex = 1
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd2."bitrate_Mbps"]')
          ParentFont = False
        end
        object Memo155: TfrxMemoView
          Align = baLeft
          Left = 485.000000000000000000
          Top = 90.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          GroupIndex = 1
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd2."power_dBm"]')
          ParentFont = False
        end
        object Memo156: TfrxMemoView
          Align = baLeft
          Left = 485.000000000000000000
          Top = 165.000000000000100000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          GroupIndex = 1
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd2."threshold_BER_3"]')
          ParentFont = False
        end
        object Memo157: TfrxMemoView
          Align = baLeft
          Left = 485.000000000000000000
          Top = 180.000000000000100000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          GroupIndex = 1
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd2."threshold_BER_6"]')
          ParentFont = False
        end
        object Memo158: TfrxMemoView
          Align = baLeft
          Left = 310.000000000000000000
          Top = 375.000000000000100000
          Width = 350.000000000000000000
          Height = 15.000000000000000000
          GroupIndex = 1
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Link."ESR_REQUIRED"]')
          ParentFont = False
        end
        object Memo159: TfrxMemoView
          Align = baLeft
          Left = 310.000000000000000000
          Top = 390.000000000000100000
          Width = 350.000000000000000000
          Height = 15.000000000000000000
          GroupIndex = 1
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Link."status_str"]')
          ParentFont = False
        end
        object Memo160: TfrxMemoView
          Align = baLeft
          Left = 310.000000000000000000
          Top = 345.000000000000100000
          Width = 350.000000000000000000
          Height = 15.000000000000000000
          GroupIndex = 1
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Link."ESR_NORM"]')
          ParentFont = False
        end
        object Memo161: TfrxMemoView
          Align = baLeft
          Left = 310.000000000000000000
          Top = 360.000000000000100000
          Width = 350.000000000000000000
          Height = 15.000000000000000000
          GroupIndex = 1
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Link."BBER_REQUIRED"]')
          ParentFont = False
        end
        object Memo162: TfrxMemoView
          Align = baLeft
          Left = 310.000000000000000000
          Top = 315.000000000000100000
          Width = 350.000000000000000000
          Height = 15.000000000000000000
          GroupIndex = 1
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Link."KNG_NORM"]')
          ParentFont = False
        end
        object Memo163: TfrxMemoView
          Align = baLeft
          Left = 310.000000000000000000
          Top = 330.000000000000100000
          Width = 350.000000000000000000
          Height = 15.000000000000000000
          GroupIndex = 1
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Link."BBER_NORM"]')
          ParentFont = False
        end
        object Memo164: TfrxMemoView
          Align = baLeft
          Left = 310.000000000000000000
          Top = 285.000000000000100000
          Width = 350.000000000000000000
          Height = 15.000000000000000000
          GroupIndex = 1
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Link."SESR_NORM"]')
          ParentFont = False
        end
        object Memo165: TfrxMemoView
          Align = baLeft
          Left = 310.000000000000000000
          Top = 300.000000000000100000
          Width = 350.000000000000000000
          Height = 15.000000000000000000
          GroupIndex = 1
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Link."KNG"]')
          ParentFont = False
        end
        object Memo166: TfrxMemoView
          Align = baLeft
          Left = 310.000000000000000000
          Top = 255.000000000000100000
          Width = 350.000000000000000000
          Height = 15.000000000000000000
          GroupIndex = 1
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Link."SESR_INTERFERENCE"]')
          ParentFont = False
        end
        object Memo167: TfrxMemoView
          Align = baLeft
          Left = 310.000000000000000000
          Top = 270.000000000000100000
          Width = 350.000000000000000000
          Height = 15.000000000000000000
          GroupIndex = 1
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Link."SESR"]')
          ParentFont = False
        end
        object Memo168: TfrxMemoView
          Align = baLeft
          Left = 310.000000000000000000
          Top = 225.000000000000100000
          Width = 350.000000000000000000
          Height = 15.000000000000000000
          GroupIndex = 1
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Link."SESR_SUBREFRACTION"]')
          ParentFont = False
        end
        object Memo169: TfrxMemoView
          Align = baLeft
          Left = 310.000000000000000000
          Top = 240.000000000000100000
          Width = 350.000000000000000000
          Height = 15.000000000000000000
          GroupIndex = 1
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Link."SESR_RAIN"]')
          ParentFont = False
        end
        object Memo170: TfrxMemoView
          Align = baLeft
          Left = 310.000000000000000000
          Top = 210.000000000000100000
          Width = 350.000000000000000000
          Height = 15.000000000000000000
          GroupIndex = 1
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Link."MARGIN_DISTANCE"]')
          ParentFont = False
        end
        object Memo171: TfrxMemoView
          Align = baLeft
          Left = 310.000000000000000000
          Top = 195.000000000000100000
          Width = 350.000000000000000000
          Height = 15.000000000000000000
          GroupIndex = 1
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Link."MARGIN_HEIGHT"]')
          ParentFont = False
        end
        object Memo172: TfrxMemoView
          Align = baLeft
          Left = 485.000000000000000000
          Top = 105.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          GroupIndex = 1
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[antenna2."diameter"]')
          ParentFont = False
        end
        object Memo173: TfrxMemoView
          Align = baLeft
          Left = 485.000000000000000000
          Top = 120.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          GroupIndex = 1
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[antenna2."polarization"]')
          ParentFont = False
        end
        object Memo174: TfrxMemoView
          Align = baLeft
          Left = 310.000000000000000000
          Top = 135.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          GroupIndex = 1
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd1."rx_level_dBm"]')
          ParentFont = False
        end
        object Memo175: TfrxMemoView
          Align = baLeft
          Left = 485.000000000000000000
          Top = 135.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          GroupIndex = 1
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd2."rx_level_dBm"]')
          ParentFont = False
        end
        object Memo176: TfrxMemoView
          Align = baLeft
          Left = 310.000000000000000000
          Top = 150.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          GroupIndex = 1
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd1."kratnostBySpace_name"]')
          ParentFont = False
        end
        object Memo177: TfrxMemoView
          Align = baLeft
          Left = 485.000000000000000000
          Top = 150.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          GroupIndex = 1
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd2."kratnostBySpace_name"]')
          ParentFont = False
        end
        object Memo178: TfrxMemoView
          Align = baLeft
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          GroupIndex = 1
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1119#1056#187#1056#1109#1057#8240#1056#176#1056#1169#1056#1108#1056#176)
          ParentFont = False
        end
        object Memo179: TfrxMemoView
          Align = baLeft
          Left = 310.000000000000000000
          Top = 15.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          GroupIndex = 1
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[antenna1."height"]')
          ParentFont = False
        end
        object Memo180: TfrxMemoView
          Align = baLeft
          Left = 485.000000000000000000
          Top = 15.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          GroupIndex = 1
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[antenna2."height"]')
          ParentFont = False
        end
      end
      object MasterData3: TfrxMasterData
        Height = 52.000000000000000000
        Top = 105.826840000000000000
        Width = 718.110700000000000000
        Child = frxReport_short_WGS.Child_Header
        RowCount = 0
        object Memo7: TfrxMemoView
          Align = baWidth
          Width = 718.110700000000000000
          Height = 19.000000000000000000
          ShowHint = False
          AutoWidth = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8 = (
            #1056#1115#1057#8218#1057#8225#1056#181#1057#8218' '#1056#1111#1056#1109' '#1057#1026#1056#176#1056#1169#1056#1105#1056#1109#1057#1026#1056#181#1056#187#1056#181#1056#8470#1056#1029#1056#1109#1056#1112#1057#1107' '#1056#1105#1056#1029#1057#8218#1056#181#1057#1026#1056#1030#1056#176#1056#187#1057#1107)
          ParentFont = False
        end
        object Linkname: TfrxMemoView
          Align = baWidth
          Top = 28.000000000000000000
          Width = 718.110700000000000000
          Height = 16.000000000000000000
          ShowHint = False
          AutoWidth = True
          DataField = 'name'
          Memo.UTF8 = (
            '[Link."name"]')
        end
      end
      object Child_profile: TfrxChild
        Height = 58.400000000000000000
        Top = 861.732840000000000000
        Width = 718.110700000000000000
        object Picture_profile: TfrxPictureView
          Align = baWidth
          Top = 20.000000000000000000
          Width = 718.110700000000000000
          Height = 36.000000000000000000
          OnBeforePrint = 'Picture_profileOnBeforePrint'
          ShowHint = False
          DataField = 'profile_img'
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Stretched = False
          HightQuality = True
          Transparent = False
          TransparentColor = clWhite
        end
        object Memo80: TfrxMemoView
          Align = baLeft
          ShiftMode = smWhenOverlapped
          Width = 363.200000000000000000
          Height = 16.800000000000000000
          ShowHint = False
          AutoWidth = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            
              #1056#152#1056#183#1056#1109#1056#177#1057#1026#1056#176#1056#182#1056#181#1056#1029#1056#1105#1056#181' '#1056#1111#1057#1026#1056#1109#1057#8222#1056#1105#1056#187#1057#1039' '#1056#1111#1057#1026#1056#1105' '#1056#1029#1056#1109#1057#1026#1056#1112#1056#176#1056#187#1057#1034#1056#1029#1056#1109#1056 +
              #8470' '#1057#1026#1056#181#1057#8222#1057#1026#1056#176#1056#1108#1057#8224#1056#1105#1056#1105)
          ParentFont = False
        end
      end
      object Child_map: TfrxChild
        Height = 36.800000000000000000
        Top = 801.260360000000000000
        Width = 718.110700000000000000
        OnAfterCalcHeight = 'Child_mapOnAfterCalcHeight'
        OnBeforePrint = 'Child_mapOnBeforePrint'
        AllowSplit = True
        Child = frxReport_short_WGS.Child_profile
        object Picture_map: TfrxPictureView
          Description = 'Map'
          Align = baWidth
          Width = 718.110700000000000000
          Height = 28.000000000000000000
          OnBeforePrint = 'Picture_mapOnBeforePrint'
          ShowHint = False
          DataField = 'map_img'
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Stretched = False
          HightQuality = True
          Transparent = False
          TransparentColor = clWhite
        end
      end
      object PageFooter1: TfrxPageFooter
        Height = 24.000000000000000000
        Top = 982.677800000000000000
        Width = 718.110700000000000000
        object Page: TfrxMemoView
          Align = baRight
          Left = 638.110700000000000000
          Width = 80.000000000000000000
          Height = 16.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8 = (
            '[Page#]')
          ParentFont = False
        end
        object Memo138: TfrxMemoView
          Align = baWidth
          Width = 638.110700000000000000
          Height = 16.000000000000000000
          ShowHint = False
          AutoWidth = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clSilver
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            
              #1056#1115#1057#8218#1057#8225#1056#181#1057#8218' '#1056#1030#1057#8249#1056#1111#1056#1109#1056#187#1056#1029#1056#181#1056#1029' '#1056#1030' '#1056#1111#1057#1026#1056#1109#1056#1110#1057#1026#1056#176#1056#1112#1056#1112#1056#181' ONEPLAN RPLS-D' +
              'B Link')
          ParentFont = False
        end
      end
      object PageHeader1: TfrxPageHeader
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        Height = 28.000000000000000000
        ParentFont = False
        Top = 18.897650000000000000
        Width = 718.110700000000000000
        object Linkcalc_method_name1: TfrxMemoView
          Align = baWidth
          Left = 141.600000000000000000
          Width = 576.510699999999900000
          Height = 16.000000000000000000
          ShowHint = False
          AutoWidth = True
          DataField = 'calc_method_name'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clSilver
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            '[Link."calc_method_name"]')
          ParentFont = False
        end
        object Memo137: TfrxMemoView
          Align = baLeft
          Width = 141.600000000000000000
          Height = 16.000000000000000000
          ShowHint = False
          AutoWidth = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clSilver
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            #1056#1114#1056#181#1057#8218#1056#1109#1056#1169#1056#1105#1056#1108#1056#176' '#1057#1026#1056#176#1057#1027#1057#8225#1056#181#1057#8218#1056#176': ')
          ParentFont = False
        end
      end
    end
  end
  object frxReport_full: TfrxReport
    Version = '4.15.6'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.AllowEdit = False
    PreviewOptions.Buttons = [pbPrint, pbExport, pbZoom, pbTools, pbNavigator, pbExportQuick, pbNoFullScreen]
    PreviewOptions.ThumbnailVisible = True
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = #1055#1086' '#1091#1084#1086#1083#1095#1072#1085#1080#1102
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 41730.469225335600000000
    ReportOptions.LastChange = 41939.809216030100000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      ''
      '      '
      '  '
      ''
      ''
      'procedure frxReportOnStartReport(Sender: TfrxComponent);'
      'const'
      '  DEF_MAP_H = 800;'
      '  '
      '  DEF_H = 450;'
      '  DEF_OFFSET = 25;'
      ''
      '  DEF_TOP = 20;                                  '
      'begin'
      '//  Picture_map.Width:=200;                '
      '  Picture_map.Height     :=DEF_MAP_H;'
      '    '
      '  Picture_profile.Height :=DEF_H;                '
      '  Picture_profile1.Height:=DEF_H;                '
      '  Picture_profile2.Height:=DEF_H;                '
      '  Picture_profile3.Height:=DEF_H;                '
      '          '
      '  '
      '  Child_map.Height:=DEF_MAP_H;'
      '    '
      '  Child_profile.Height:=DEF_H + DEF_OFFSET;              '
      '  Child_profile1.Height:=DEF_H + DEF_OFFSET;              '
      '  Child_profile2.Height:=DEF_H + DEF_OFFSET;              '
      '  Child_profile3.Height:=DEF_H + DEF_OFFSET;'
      ''
      '  Child_profile1.Top:=DEF_TOP;              '
      '  Child_profile2.Top:=DEF_TOP;              '
      '  Child_profile3.Top:=DEF_TOP;'
      '  '
      '  '
      '                                       '
      'end;'
      ''
      ''
      'procedure Shape_ColorOnBeforePrint(Sender: TfrxComponent);'
      'begin'
      
        '  Shape_Color.color := <Profile."clutter_color">                ' +
        '                                                    '
      'end;'
      '     '
      '        '
      ''
      ''
      '//------------------------------------------------------------'
      'function DegreeToStr (aValue: double): String;'
      '//------------------------------------------------------------'
      'const'
      '//  STR_BLPOINT_FORMAT1 = '#39'%d'#176'%.2d'#39#39'%s%1.2f'#39#39#39#39#39';'
      '  STR_BLPOINT_FORMAT1 = '#39'%d'#176' %.2d'#39#39' %1.2f'#39#39#39#39#39';'
      'var    '
      ' iDeg,iMin:integer; '
      ' eSec: Double;   '
      '           '
      'begin'
      '  geo_DecodeDegree_(aValue, iDeg,iMin,eSec);'
      '                           '
      ' // angle.Sec:=TruncFloat(angle.Sec,2);'
      ' '
      '    Result:=Format(STR_BLPOINT_FORMAT1, [iDeg, iMin, eSec]);'
      ''
      '  if aValue<0 then'
      '    Result:='#39'-'#39'+Result;'
      'end;'
      '  '
      '           '
      ''
      '//------------------------------------------------------------'
      
        'procedure geo_DecodeDegree_(aValue: Double; var aDeg,aMin:intege' +
        'r; var aSec:'
      '    Double);'
      '//------------------------------------------------------------'
      'const'
      ' EXP_ = 0.000000001;'
      '     //97,033333333'
      'var'
      '  bIsNegative: Boolean;'
      '  e: Double;'
      '  eMinutes: Double;'
      '  eSec: Double;'
      '  eSeconds: Double;'
      'begin'
      '  bIsNegative := aValue<0;'
      ''
      '  aValue :=Abs(aValue);'
      ''
      '  aDeg:=Trunc(aValue);'
      ''
      ' // e:=Abs(aValue)*60;'
      ''
      '//  , Frac(X) = X - Int(X).'
      ''
      '  aMin:=Trunc(Frac(aValue)*60);'
      ''
      '//  Result.Sec:=Frac(Frac(Abs(aValue))*60)*60;'
      '  e:= (aValue - Trunc(aValue)) * 60;'
      '  aMin:=Trunc(e);'
      ''
      ' eMinutes := Trunc (e);'
      ' eSeconds := (e - Trunc (e)) * 60 ;'
      ''
      ''
      '  aSec:=(((aValue - aDeg) *60) - aMin) * 60;'
      ''
      '  if (aSec - 60) > EXP_ then'
      '  begin'
      '    Inc(aMin);'
      '    aSec := 0;'
      '  end;'
      ''
      ''
      '  if bIsNegative then'
      '    aDeg:=-aDeg;'
      '          '
      'end;'
      '        '
      ''
      '       '
      'begin'
      ''
      'end.    ')
    OnStartReport = 'frxReportOnStartReport'
    Left = 206
    Top = 68
    Datasets = <
      item
      end
      item
      end
      item
      end
      item
      end
      item
      end
      item
      end
      item
      end
      item
      end
      item
      end
      item
      end
      item
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 9.260416666666670000
      BottomMargin = 10.583333333333300000
      LargeDesignHeight = True
      OnBeforePrint = 'Page1OnBeforePrint'
      object Child_profile3: TfrxChild
        Height = 61.000000000000000000
        Top = 721.890230000000000000
        Width = 718.110700000000000000
        Child = frxReport_full.Child_calc_res
        object Memo82: TfrxMemoView
          Align = baLeft
          ShiftMode = smWhenOverlapped
          Width = 559.200000000000000000
          Height = 16.800000000000000000
          ShowHint = False
          AutoWidth = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            
              #1056#152#1056#183#1056#1109#1056#177#1057#1026#1056#176#1056#182#1056#181#1056#1029#1056#1105#1056#181' '#1056#1111#1057#1026#1056#1109#1057#8222#1056#1105#1056#187#1057#1039' '#1056#1030' '#1057#1107#1057#1027#1056#187#1056#1109#1056#1030#1056#1105#1057#1039#1057#8230' '#1057#1027#1057#1107#1056#177 +
              #1057#1026#1056#181#1057#8222#1057#1026#1056#176#1056#1108#1057#8224#1056#1105#1056#1105#1056#1109#1056#1029#1056#1029#1056#1109#1056#8470' '#1056#1029#1056#181#1057#1107#1057#1027#1057#8218#1056#1109#1056#8470#1057#8225#1056#1105#1056#1030#1056#1109#1057#1027#1057#8218#1056#1105)
          ParentFont = False
        end
        object Picture_profile3: TfrxPictureView
          Align = baWidth
          ShiftMode = smWhenOverlapped
          Top = 20.000000000000000000
          Width = 718.110700000000000000
          Height = 31.000000000000000000
          OnBeforePrint = 'Picture_profile3OnBeforePrint'
          ShowHint = False
          DataField = 'img3'
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Stretched = False
          HightQuality = True
          Transparent = False
          TransparentColor = clWhite
        end
      end
      object MasterData3: TfrxMasterData
        Height = 252.315090000000000000
        Top = 105.826840000000000000
        Width = 718.110700000000000000
        Child = frxReport_full.Child_map
        RowCount = 0
        object Subreport_center: TfrxSubreport
          Left = 251.779530000000000000
          Top = 83.149660000000000000
          Width = 216.000000000000000000
          Height = 55.000000000000000000
          ShowHint = False
          Page = frxReport_full.Page_Center
        end
        object Subreport_left: TfrxSubreport
          Top = 83.149660000000000000
          Width = 240.000000000000000000
          Height = 55.000000000000000000
          ShowHint = False
          Page = frxReport_full.Lleft
        end
        object Subreport_right: TfrxSubreport
          Align = baRight
          Left = 478.110700000000000000
          Top = 83.149660000000000000
          Width = 240.000000000000000000
          Height = 55.000000000000000000
          ShowHint = False
          Page = frxReport_full.Subreport_right_
        end
        object Subreport_summary: TfrxSubreport
          Align = baWidth
          Top = 170.078850000000000000
          Width = 718.110700000000000000
          Height = 48.000000000000000000
          ShowHint = False
          Page = frxReport_full.Page_Summary
        end
        object Memo7: TfrxMemoView
          Align = baWidth
          Width = 718.110700000000000000
          Height = 19.000000000000000000
          ShowHint = False
          AutoWidth = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8 = (
            #1056#1115#1057#8218#1057#8225#1056#181#1057#8218' '#1056#1111#1056#1109' '#1057#1026#1056#176#1056#1169#1056#1105#1056#1109#1057#1026#1056#181#1056#187#1056#181#1056#8470#1056#1029#1056#1109#1056#1112#1057#1107' '#1056#1105#1056#1029#1057#8218#1056#181#1057#1026#1056#1030#1056#176#1056#187#1057#1107)
          ParentFont = False
        end
        object Linkname: TfrxMemoView
          Align = baWidth
          Top = 28.000000000000000000
          Width = 718.110700000000000000
          Height = 16.000000000000000000
          ShowHint = False
          AutoWidth = True
          DataField = 'name'
          Memo.UTF8 = (
            '[Link."name"]')
        end
      end
      object Child_profile: TfrxChild
        Height = 58.400000000000000000
        Top = 442.205010000000000000
        Width = 718.110700000000000000
        Child = frxReport_full.Child_profile1
        object Picture_profile: TfrxPictureView
          Align = baWidth
          Top = 20.000000000000000000
          Width = 718.110700000000000000
          Height = 36.000000000000000000
          OnBeforePrint = 'Picture_profileOnBeforePrint'
          ShowHint = False
          DataField = 'profile_img'
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Stretched = False
          HightQuality = True
          Transparent = False
          TransparentColor = clWhite
        end
        object Memo80: TfrxMemoView
          Align = baLeft
          ShiftMode = smWhenOverlapped
          Width = 363.200000000000000000
          Height = 16.800000000000000000
          ShowHint = False
          AutoWidth = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            
              #1056#152#1056#183#1056#1109#1056#177#1057#1026#1056#176#1056#182#1056#181#1056#1029#1056#1105#1056#181' '#1056#1111#1057#1026#1056#1109#1057#8222#1056#1105#1056#187#1057#1039' '#1056#1111#1057#1026#1056#1105' '#1056#1029#1056#1109#1057#1026#1056#1112#1056#176#1056#187#1057#1034#1056#1029#1056#1109#1056 +
              #8470' '#1057#1026#1056#181#1057#8222#1057#1026#1056#176#1056#1108#1057#8224#1056#1105#1056#1105)
          ParentFont = False
        end
      end
      object Child_map: TfrxChild
        Height = 36.800000000000000000
        Top = 381.732530000000000000
        Width = 718.110700000000000000
        OnAfterCalcHeight = 'Child_mapOnAfterCalcHeight'
        OnBeforePrint = 'Child_mapOnBeforePrint'
        AllowSplit = True
        Child = frxReport_full.Child_profile
        object Picture_map: TfrxPictureView
          Description = 'Map'
          Align = baWidth
          Width = 718.110700000000000000
          Height = 28.000000000000000000
          OnBeforePrint = 'Picture_mapOnBeforePrint'
          ShowHint = False
          DataField = 'map_img'
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Stretched = False
          HightQuality = True
          Transparent = False
          TransparentColor = clWhite
        end
      end
      object Child_profile1: TfrxChild
        Height = 76.000000000000000000
        Top = 525.354670000000000000
        Width = 718.110700000000000000
        Child = frxReport_full.Child_profile2
        object Picture_profile1: TfrxPictureView
          Align = baWidth
          ShiftMode = smWhenOverlapped
          Top = 20.000000000000000000
          Width = 718.110700000000000000
          Height = 40.000000000000000000
          OnBeforePrint = 'Picture_profile1OnBeforePrint'
          ShowHint = False
          DataField = 'img1'
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Stretched = False
          HightQuality = True
          Transparent = False
          TransparentColor = clWhite
        end
        object Memo81: TfrxMemoView
          Align = baLeft
          ShiftMode = smWhenOverlapped
          Width = 359.200000000000000000
          Height = 16.800000000000000000
          ShowHint = False
          AutoWidth = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            
              #1056#152#1056#183#1056#1109#1056#177#1057#1026#1056#176#1056#182#1056#181#1056#1029#1056#1105#1056#181' '#1056#1111#1057#1026#1056#1109#1057#8222#1056#1105#1056#187#1057#1039' '#1056#1111#1057#1026#1056#1105' 20% '#1057#1026#1056#181#1057#8222#1057#1026#1056#176#1056#1108#1057#8224#1056 +
              #1105#1056#1105)
          ParentFont = False
        end
      end
      object Child_profile2: TfrxChild
        Height = 76.800000000000000000
        Top = 623.622450000000000000
        Width = 718.110700000000000000
        Child = frxReport_full.Child_profile3
        object Picture_profile2: TfrxPictureView
          Align = baWidth
          ShiftMode = smWhenOverlapped
          Top = 20.000000000000000000
          Width = 718.110700000000000000
          Height = 43.000000000000000000
          OnBeforePrint = 'Picture_profile2OnBeforePrint'
          ShowHint = False
          DataField = 'img2'
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Stretched = False
          HightQuality = True
          Transparent = False
          TransparentColor = clWhite
        end
        object Memo83: TfrxMemoView
          Align = baLeft
          ShiftMode = smWhenOverlapped
          Width = 567.200000000000000000
          Height = 16.800000000000000000
          ShowHint = False
          AutoWidth = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            
              #1056#152#1056#183#1056#1109#1056#177#1057#1026#1056#176#1056#182#1056#181#1056#1029#1056#1105#1056#181' '#1056#1111#1057#1026#1056#1109#1057#8222#1056#1105#1056#187#1057#1039' '#1056#1030' '#1057#1107#1057#1027#1056#187#1056#1109#1056#1030#1056#1105#1057#1039#1057#8230' '#1057#1027#1057#1107#1056#177 +
              #1057#1026#1056#181#1057#8222#1057#1026#1056#176#1056#1108#1057#8224#1056#1105#1056#1105)
          ParentFont = False
        end
      end
      object Child_Profile_items: TfrxChild
        Height = 40.000000000000000000
        Top = 869.291900000000000000
        Width = 718.110700000000000000
        StartNewPage = True
        object Subreport_Profile: TfrxSubreport
          Align = baWidth
          Width = 718.110700000000000000
          Height = 24.000000000000000000
          ShowHint = False
          Page = frxReport_full.Page6
        end
      end
      object Child_calc_res: TfrxChild
        Height = 40.000000000000000000
        Top = 805.039890000000000000
        Width = 718.110700000000000000
        AllowSplit = True
        Child = frxReport_full.Child_Profile_items
        StartNewPage = True
        object Subreport_calc_res: TfrxSubreport
          Align = baLeft
          Width = 718.110700000000000000
          Height = 20.000000000000000000
          ShowHint = False
          Page = frxReport_full.Page8
        end
      end
      object PageFooter1: TfrxPageFooter
        Height = 24.000000000000000000
        Top = 971.339210000000000000
        Width = 718.110700000000000000
        object Page: TfrxMemoView
          Align = baRight
          Left = 638.110700000000000000
          Width = 80.000000000000000000
          Height = 16.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8 = (
            '[Page#]')
          ParentFont = False
        end
        object Memo138: TfrxMemoView
          Align = baWidth
          Width = 638.110700000000000000
          Height = 16.000000000000000000
          ShowHint = False
          AutoWidth = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clSilver
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            
              #1056#1115#1057#8218#1057#8225#1056#181#1057#8218' '#1056#1030#1057#8249#1056#1111#1056#1109#1056#187#1056#1029#1056#181#1056#1029' '#1056#1030' '#1056#1111#1057#1026#1056#1109#1056#1110#1057#1026#1056#176#1056#1112#1056#1112#1056#181' ONEPLAN RPLS-D' +
              'B Link')
          ParentFont = False
        end
      end
      object PageHeader1: TfrxPageHeader
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        Height = 28.000000000000000000
        ParentFont = False
        Top = 18.897650000000000000
        Width = 718.110700000000000000
        object Linkcalc_method_name1: TfrxMemoView
          Align = baWidth
          Left = 141.600000000000000000
          Width = 576.510700000000000000
          Height = 16.000000000000000000
          ShowHint = False
          AutoWidth = True
          DataField = 'calc_method_name'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clSilver
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            '[Link."calc_method_name"]')
          ParentFont = False
        end
        object Memo137: TfrxMemoView
          Align = baLeft
          Width = 141.600000000000000000
          Height = 16.000000000000000000
          ShowHint = False
          AutoWidth = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clSilver
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            #1056#1114#1056#181#1057#8218#1056#1109#1056#1169#1056#1105#1056#1108#1056#176' '#1057#1026#1056#176#1057#1027#1057#8225#1056#181#1057#8218#1056#176': ')
          ParentFont = False
        end
      end
    end
    object Lleft: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object MasterData2: TfrxMasterData
        Height = 180.779530000000000000
        Top = 18.897650000000000000
        Width = 718.110700000000000000
        RowCount = 0
        object Memo43: TfrxMemoView
          Align = baLeft
          Width = 110.000000000000000000
          Height = 32.118120000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1119#1056#187#1056#1109#1057#8240#1056#176#1056#1169#1056#1108#1056#176)
          ParentFont = False
        end
        object Memo_left_1: TfrxMemoView
          Align = baLeft
          Left = 110.000000000000000000
          Width = 117.165354330000000000
          Height = 32.118120000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Site1."name"]')
          ParentFont = False
          UseDefaultCharset = True
        end
        object Memo68: TfrxMemoView
          Align = baLeft
          Top = 62.118120000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1113#1056#1109#1056#1109#1057#1026#1056#1169#1056#1105#1056#1029#1056#176#1057#8218#1057#8249' WGS')
          ParentFont = False
        end
        object Memo70: TfrxMemoView
          Align = baLeft
          Top = 92.118120000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#8217#1057#8249#1057#1027#1056#1109#1057#8218#1056#176' '#1056#1111#1056#1109#1056#1169#1056#1030#1056#181#1057#1027#1056#176', m')
          ParentFont = False
        end
        object Memo72: TfrxMemoView
          Align = baLeft
          Top = 107.118120000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1106#1056#183#1056#1105#1056#1112#1057#1107#1057#8218)
          ParentFont = False
        end
        object Memo74: TfrxMemoView
          Align = baLeft
          Top = 122.118120000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1032#1056#1110#1056#1109#1056#187' '#1056#1112#1056#181#1057#1027#1057#8218#1056#176)
          ParentFont = False
        end
        object Memo100: TfrxMemoView
          Align = baLeft
          Top = 137.118120000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#8221#1056#1105#1056#176#1056#1112#1056#181#1057#8218#1057#1026' '#1056#176#1056#1029#1057#8218#1056#181#1056#1029#1056#1029#1057#8249)
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Align = baLeft
          Left = 110.000000000000000000
          Top = 62.118120000000000000
          Width = 117.165354330000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[DegreeToStr (<Site1."lat_WGS">)]')
          ParentFont = False
          UseDefaultCharset = True
        end
        object Memo1: TfrxMemoView
          Align = baLeft
          Left = 110.000000000000000000
          Top = 92.118120000000000000
          Width = 117.165354330000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[antenna1."height"]')
          ParentFont = False
          UseDefaultCharset = True
        end
        object Memo95: TfrxMemoView
          Align = baLeft
          Left = 110.000000000000000000
          Top = 107.118120000000000000
          Width = 117.165354330000000000
          Height = 15.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."azimuth1"]')
          ParentFont = False
          UseDefaultCharset = True
        end
        object Memo97: TfrxMemoView
          Align = baLeft
          Left = 110.000000000000000000
          Top = 137.118120000000000000
          Width = 117.165354330000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[antenna1."diameter"]')
          ParentFont = False
          UseDefaultCharset = True
        end
        object Memo98: TfrxMemoView
          Align = baLeft
          Left = 110.000000000000000000
          Top = 77.118120000000000000
          Width = 117.165354330000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[DegreeToStr (<Site1."lon_WGS">)]')
          ParentFont = False
          UseDefaultCharset = True
        end
        object Memo96: TfrxMemoView
          Align = baLeft
          Left = 110.000000000000000000
          Top = 122.118120000000000000
          Width = 117.165354330000000000
          Height = 15.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."ELEVATION_angle_1"]')
          ParentFont = False
          UseDefaultCharset = True
        end
        object Memo121: TfrxMemoView
          Align = baLeft
          Top = 77.118120000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Align = baLeft
          Top = 32.118120000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1113#1056#1109#1056#1109#1057#1026#1056#1169#1056#1105#1056#1029#1056#176#1057#8218#1057#8249' Pulkovo')
          ParentFont = False
        end
        object Memo30: TfrxMemoView
          Align = baLeft
          Left = 110.000000000000000000
          Top = 32.118120000000000000
          Width = 117.165354330000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[DegreeToStr (<Site1."lat">)]')
          ParentFont = False
          UseDefaultCharset = True
        end
        object Memo66: TfrxMemoView
          Align = baLeft
          Left = 110.000000000000000000
          Top = 47.118120000000000000
          Width = 117.165354330000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[DegreeToStr (<Site1."lon">)]')
          ParentFont = False
          UseDefaultCharset = True
        end
        object Memo99: TfrxMemoView
          Align = baLeft
          Top = 47.118120000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          ParentFont = False
        end
        object Memo136: TfrxMemoView
          Align = baLeft
          Top = 152.118120000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1032#1057#1027#1056#1105#1056#187#1056#181#1056#1029#1056#1105#1056#181' '#1056#176#1056#1029#1057#8218#1056#181#1056#1029#1056#1029#1057#8249)
          ParentFont = False
        end
        object Memo139: TfrxMemoView
          Align = baLeft
          Left = 110.000000000000000000
          Top = 152.118120000000000000
          Width = 117.165354330000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[antenna1."gain"]')
          ParentFont = False
          UseDefaultCharset = True
        end
      end
    end
    object Page_Center: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object MasterData1: TfrxMasterData
        Height = 127.000000000000000000
        Top = 18.897650000000000000
        Width = 718.110700000000000000
        RowCount = 0
        object Memo45: TfrxMemoView
          Align = baLeft
          Top = 15.000000000000000000
          Width = 160.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#8221#1056#187#1056#1105#1056#1029#1056#176' '#1056#1105#1056#1029#1057#8218#1056#181#1057#1026#1056#1030#1056#176#1056#187#1056#176', km')
          ParentFont = False
        end
        object Memo47: TfrxMemoView
          Align = baLeft
          Top = 30.000000000000000000
          Width = 160.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1032#1057#1026#1056#1109#1056#1030#1056#181#1056#1029#1057#1034' '#1057#1027#1056#1105#1056#1110#1056#1029#1056#176#1056#187#1056#176', dBm')
          ParentFont = False
        end
        object Memo49: TfrxMemoView
          Align = baLeft
          Top = 45.000000000000000000
          Width = 160.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            'P'#1056#176#1056#177#1056#1109#1057#8225#1056#176#1057#1039' '#1057#8225#1056#176#1057#1027#1057#8218#1056#1109#1057#8218#1056#176', GHz')
          ParentFont = False
        end
        object Memo51: TfrxMemoView
          Align = baLeft
          Top = 60.000000000000000000
          Width = 160.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1114#1056#1109#1057#8240#1056#1029#1056#1109#1057#1027#1057#8218#1057#1034' '#1056#1111#1056#181#1057#1026#1056#181#1056#1169#1056#176#1057#8218#1057#8225#1056#1105#1056#1108#1056#176', dBm')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          Align = baLeft
          Top = 75.000000000000000000
          Width = 160.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1038#1056#1108#1056#1109#1057#1026#1056#1109#1057#1027#1057#8218#1057#1034' '#1056#1111#1056#181#1057#1026#1056#181#1056#1169#1056#176#1057#8225#1056#1105', Mbit/s')
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          Align = baLeft
          Top = 90.000000000000000000
          Width = 160.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1118#1056#1105#1056#1111' '#1056#1109#1056#177#1056#1109#1057#1026#1057#1107#1056#1169#1056#1109#1056#1030#1056#176#1056#1029#1056#1105#1057#1039)
          ParentFont = False
        end
        object Memo123: TfrxMemoView
          Align = baLeft
          Left = 160.000000000000000000
          Top = 30.000000000000000000
          Width = 68.031496062992100000
          Height = 15.000000000000000000
          ShowHint = False
          DataField = 'rx_level_dBm'
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd1."rx_level_dBm"]')
          ParentFont = False
        end
        object Memo124: TfrxMemoView
          Align = baLeft
          Left = 160.000000000000000000
          Top = 75.000000000000000000
          Width = 68.031496062992100000
          Height = 15.000000000000000000
          ShowHint = False
          DataField = 'bitrate_Mbps'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd1."bitrate_Mbps"]')
          ParentFont = False
        end
        object Memo125: TfrxMemoView
          Align = baLeft
          Left = 160.000000000000000000
          Top = 90.000000000000000000
          Width = 68.031496062992100000
          Height = 15.000000000000000000
          ShowHint = False
          DataField = 'LinkEndType_name'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd1."LinkEndType_name"]')
          ParentFont = False
        end
        object Memo127: TfrxMemoView
          Align = baLeft
          Left = 160.000000000000000000
          Top = 45.000000000000000000
          Width = 68.031496062992100000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[FloatToStr( <LinkEnd1."tx_freq_MHz"> / 1000 )]')
          ParentFont = False
        end
        object Memo128: TfrxMemoView
          Align = baLeft
          Left = 160.000000000000000000
          Top = 15.000000000000000000
          Width = 68.031496062992100000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."length_km"]')
          ParentFont = False
        end
        object Memo126: TfrxMemoView
          Align = baLeft
          Left = 160.000000000000000000
          Top = 60.000000000000000000
          Width = 68.031496062992100000
          Height = 15.000000000000000000
          ShowHint = False
          DataField = 'power_dBm'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd1."power_dBm"]')
          ParentFont = False
        end
        object Memo42: TfrxMemoView
          Align = baLeft
          Width = 160.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1119#1057#1026#1056#1105#1056#183#1056#1029#1056#176#1056#1108' '#1056#1111#1057#1026#1056#1105#1056#1110#1056#1109#1056#1169#1056#1029#1056#1109#1057#1027#1057#8218#1056#1105)
          ParentFont = False
        end
        object Memo122: TfrxMemoView
          Align = baLeft
          Left = 160.000000000000000000
          Width = 68.031496062992100000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."status_str"]')
          ParentFont = False
        end
        object Memo134: TfrxMemoView
          Align = baLeft
          Top = 105.000000000000000000
          Width = 160.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#8212#1056#176#1056#1111#1056#176#1057#1027' '#1056#1029#1056#176' '#1056#183#1056#176#1056#1112#1056#1105#1057#1026#1056#176#1056#1029#1056#1105#1057#1039', dB')
          ParentFont = False
        end
        object Memo135: TfrxMemoView
          Align = baLeft
          Left = 160.000000000000000000
          Top = 105.000000000000000000
          Width = 68.031496062992100000
          Height = 15.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."fade_margin_dB"]')
          ParentFont = False
        end
      end
    end
    object Page_Summary: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object MasterData4: TfrxMasterData
        Height = 438.677180000000000000
        Top = 18.897650000000000000
        Width = 718.110700000000000000
        RowCount = 0
        object Memo_1: TfrxMemoView
          Align = baLeft
          Top = 15.000000000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#8217#1057#8249#1057#1027#1056#1109#1057#8218#1056#176', m')
          ParentFont = False
        end
        object Memo_2: TfrxMemoView
          Align = baLeft
          Top = 30.000000000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1106#1056#183#1056#1105#1056#1112#1057#1107#1057#8218' ')
          ParentFont = False
        end
        object Memo_3: TfrxMemoView
          Align = baLeft
          Top = 45.000000000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1118#1056#1105#1056#1111' '#1056#1109#1056#177#1056#1109#1057#1026#1057#1107#1056#1169#1056#1109#1056#1030#1056#176#1056#1029#1056#1105#1057#1039' ')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Align = baLeft
          Top = 60.000000000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#167#1056#176#1057#1027#1057#8218#1056#1109#1057#8218#1056#176' '#1056#1111#1056#181#1057#1026#1056#181#1056#1169#1056#176#1057#8225#1056#1105', MHz')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Align = baLeft
          Top = 75.000000000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1038#1056#1108#1056#1109#1057#1026#1056#1109#1057#1027#1057#8218#1057#1034' '#1056#1111#1056#181#1057#1026#1056#181#1056#1169#1056#176#1057#8225#1056#1105', Mbit/s')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Align = baLeft
          Top = 90.000000000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1114#1056#1109#1057#8240#1056#1029#1056#1109#1057#1027#1057#8218#1057#1034', dBm')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          Align = baLeft
          Top = 105.000000000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#8221#1056#1105#1056#176#1056#1112#1056#181#1057#8218#1057#1026' '#1056#176#1056#1029#1057#8218#1056#181#1056#1029#1056#1029#1057#8249', m')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          Align = baLeft
          Top = 135.000000000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            
              #1056#1038#1057#1026#1056#181#1056#1169#1056#1029#1057#1039#1057#1039' '#1056#1112#1056#1109#1057#8240#1056#1029#1056#1109#1057#1027#1057#8218#1057#1034' '#1056#1029#1056#176' '#1056#1030#1057#8230#1056#1109#1056#1169#1056#181' '#1056#1111#1057#1026#1056#1105#1056#181#1056#1112#1056#1029#1056#1105#1056#1108 +
              #1056#176', dBm')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          Align = baLeft
          Top = 120.000000000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1119#1056#1109#1056#187#1057#1039#1057#1026#1056#1105#1056#183#1056#176#1057#8224#1056#1105#1057#1039' ')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          Align = baLeft
          Top = 150.000000000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#160#1056#181#1056#183#1056#181#1057#1026#1056#1030#1056#1105#1057#1026#1056#1109#1056#1030#1056#176#1056#1029#1056#1105#1056#181' ')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          Align = baLeft
          Top = 165.000000000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1119#1056#1109#1057#1026#1056#1109#1056#1110' '#1056#1111#1057#1026#1056#1105' BER=10-3 ')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          Align = baLeft
          Top = 195.000000000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1114#1056#1105#1056#1029#1056#1105#1056#1112#1056#176#1056#187#1057#1034#1056#1029#1057#8249#1056#8470' '#1056#1109#1057#8218#1056#1029#1056#1109#1057#1027#1056#1105#1057#8218#1056#181#1056#187#1057#1034#1056#1029#1057#8249#1056#8470' '#1056#1111#1057#1026#1056#1109#1057#1027#1056#1030#1056#181#1057#8218)
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          Align = baLeft
          Top = 180.000000000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1119#1056#1109#1057#1026#1056#1109#1056#1110' '#1056#1111#1057#1026#1056#1105' BER=10-6 ')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          Align = baLeft
          Top = 210.000000000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1119#1056#1109#1056#187#1056#1109#1056#182#1056#181#1056#1029#1056#1105#1056#181' '#1057#8218#1056#1109#1057#8225#1056#1108#1056#1105' '#1056#1111#1057#1026#1056#1109#1057#1027#1056#1030#1056#181#1057#8218#1056#176', km ')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          Align = baLeft
          Top = 225.000000000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            'SESR '#1057#1027#1057#1107#1056#177#1057#1026#1056#181#1057#8222#1056#1108#1057#8224#1056#1105#1056#1109#1056#1029#1056#1029#1056#176#1057#1039' '#1057#1027#1056#1109#1057#1027#1057#8218#1056#176#1056#1030#1056#187#1057#1039#1057#1035#1057#8240#1056#176#1057#1039', % ')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          Align = baLeft
          Top = 240.000000000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            'SESR '#1056#1169#1056#1109#1056#182#1056#1169#1056#181#1056#1030#1056#176#1057#1039' '#1057#1027#1056#1109#1057#1027#1057#8218#1056#176#1056#1030#1056#187#1057#1039#1057#1035#1057#8240#1056#176#1057#1039', % ')
          ParentFont = False
        end
        object Memo25: TfrxMemoView
          Align = baLeft
          Top = 255.000000000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            
              'SESR '#1056#1105#1056#1029#1057#8218#1056#181#1057#1026#1057#8222#1056#181#1057#1026#1056#181#1056#1029#1057#8224#1056#1105#1056#1109#1056#1029#1056#1029#1056#176#1057#1039' '#1057#1027#1056#1109#1057#1027#1057#8218#1056#176#1056#1030#1056#187#1057#1039#1057#1035#1057#8240#1056#176#1057#1039 +
              ', % ')
          ParentFont = False
        end
        object Memo26: TfrxMemoView
          Align = baLeft
          Top = 270.000000000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            
              #1056#152#1057#8218#1056#1109#1056#1110#1056#1109#1056#1030#1057#8249#1056#8470' '#1056#1111#1056#1109#1056#1108#1056#176#1056#183#1056#176#1057#8218#1056#181#1056#187#1057#1034' '#1056#1108#1056#176#1057#8225#1056#181#1057#1027#1057#8218#1056#1030#1056#176' '#1056#1111#1056#1109' '#1056#1109#1057#8364 +
              #1056#1105#1056#177#1056#1108#1056#176#1056#1112' SESR, % ')
          ParentFont = False
        end
        object Memo27: TfrxMemoView
          Align = baLeft
          Top = 285.000000000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            
              #1056#1116#1056#1109#1057#1026#1056#1112#1056#1105#1057#1026#1057#1107#1056#181#1056#1112#1056#1109#1056#181' '#1056#183#1056#1029#1056#176#1057#8225#1056#181#1056#1029#1056#1105#1056#181' SESR '#1056#1029#1056#176' '#1056#1105#1056#1029#1057#8218#1056#181#1057#1026#1056#1030#1056#176 +
              #1056#187#1056#181', % ')
          ParentFont = False
        end
        object Memo28: TfrxMemoView
          Align = baLeft
          Top = 300.000000000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            
              #1056#152#1057#8218#1056#1109#1056#1110#1056#1109#1056#1030#1057#8249#1056#8470' '#1056#1111#1056#1109#1056#1108#1056#176#1056#183#1056#176#1057#8218#1056#181#1056#187#1057#1034' '#1056#1029#1056#181#1056#1110#1056#1109#1057#8218#1056#1109#1056#1030#1056#1029#1056#1109#1057#1027#1057#8218#1056#1105' '#1056 +
              #1113#1056#1029#1056#1110', %')
          ParentFont = False
        end
        object Memo29: TfrxMemoView
          Align = baLeft
          Top = 315.000000000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            
              #1056#1116#1056#1109#1057#1026#1056#1112#1056#1105#1057#1026#1057#1107#1056#181#1056#1112#1056#1109#1056#181' '#1056#183#1056#1029#1056#176#1057#8225#1056#181#1056#1029#1056#1105#1056#181' '#1056#1113#1056#1029#1056#1110' '#1056#1029#1056#176' '#1056#1105#1056#1029#1057#8218#1056#181#1057#1026#1056#1030 +
              #1056#176#1056#187#1056#181', % ')
          ParentFont = False
        end
        object Memo31: TfrxMemoView
          Align = baLeft
          Top = 375.000000000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            'ESR '#1057#8218#1057#1026#1056#181#1056#177', % ')
          ParentFont = False
        end
        object Memo32: TfrxMemoView
          Align = baLeft
          Top = 330.000000000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            'BBER '#1056#1029#1056#1109#1057#1026#1056#1112', %')
          ParentFont = False
        end
        object Memo33: TfrxMemoView
          Align = baLeft
          Top = 360.000000000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            'BBER '#1057#8218#1057#1026#1056#181#1056#177', % ')
          ParentFont = False
        end
        object Memo34: TfrxMemoView
          Align = baLeft
          Top = 345.000000000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            'ESR '#1056#1029#1056#1109#1057#1026#1056#1112', % ')
          ParentFont = False
        end
        object Memo35: TfrxMemoView
          Align = baLeft
          Top = 390.000000000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1119#1057#1026#1056#1105#1056#1110#1056#1109#1056#1169#1056#1029#1056#1109#1057#1027#1057#8218#1057#1034' ')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Align = baLeft
          Left = 310.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Site1."name"]')
          ParentFont = False
        end
        object Memo36: TfrxMemoView
          Align = baLeft
          Left = 485.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Site2."name"]')
          ParentFont = False
        end
        object Memo37: TfrxMemoView
          Align = baLeft
          Left = 310.000000000000000000
          Top = 30.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."azimuth1"]')
          ParentFont = False
        end
        object Memo38: TfrxMemoView
          Align = baLeft
          Left = 485.000000000000000000
          Top = 30.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."azimuth2"]')
          ParentFont = False
        end
        object LinkEnd2threshold_BER_3: TfrxMemoView
          Align = baLeft
          Left = 310.000000000000000000
          Top = 165.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd1."threshold_BER_3"]')
          ParentFont = False
        end
        object LinkEnd2threshold_BER_6: TfrxMemoView
          Align = baLeft
          Left = 310.000000000000000000
          Top = 180.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd1."threshold_BER_6"]')
          ParentFont = False
        end
        object Memo41: TfrxMemoView
          Align = baLeft
          Left = 310.000000000000000000
          Top = 105.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[antenna1."diameter"]')
          ParentFont = False
        end
        object Memo44: TfrxMemoView
          Align = baLeft
          Left = 310.000000000000000000
          Top = 120.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[antenna1."polarization"]')
          ParentFont = False
        end
        object Memo50: TfrxMemoView
          Align = baLeft
          Left = 310.000000000000000000
          Top = 75.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd1."bitrate_Mbps"]')
          ParentFont = False
        end
        object Memo52: TfrxMemoView
          Align = baLeft
          Left = 310.000000000000000000
          Top = 90.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd1."power_dBm"]')
          ParentFont = False
        end
        object Memo55: TfrxMemoView
          Align = baLeft
          Left = 310.000000000000000000
          Top = 45.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd1."LinkEndType_name"]')
          ParentFont = False
        end
        object Memo56: TfrxMemoView
          Align = baLeft
          Left = 310.000000000000000000
          Top = 60.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd1."tx_freq_MHz"]')
          ParentFont = False
        end
        object Memo57: TfrxMemoView
          Align = baLeft
          Left = 485.000000000000000000
          Top = 45.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd2."LinkEndType_name"]')
          ParentFont = False
        end
        object Memo39: TfrxMemoView
          Align = baLeft
          Left = 485.000000000000000000
          Top = 60.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd2."tx_freq_MHz"]')
          ParentFont = False
        end
        object Memo40: TfrxMemoView
          Align = baLeft
          Left = 485.000000000000000000
          Top = 75.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd2."bitrate_Mbps"]')
          ParentFont = False
        end
        object Memo46: TfrxMemoView
          Align = baLeft
          Left = 485.000000000000000000
          Top = 90.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd2."power_dBm"]')
          ParentFont = False
        end
        object Memo48: TfrxMemoView
          Align = baLeft
          Left = 485.000000000000000000
          Top = 165.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd2."threshold_BER_3"]')
          ParentFont = False
        end
        object Memo53: TfrxMemoView
          Align = baLeft
          Left = 485.000000000000000000
          Top = 180.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd2."threshold_BER_6"]')
          ParentFont = False
        end
        object Memo54: TfrxMemoView
          Align = baLeft
          Left = 310.000000000000000000
          Top = 375.000000000000000000
          Width = 350.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Link."ESR_REQUIRED"]')
          ParentFont = False
        end
        object Memo58: TfrxMemoView
          Align = baLeft
          Left = 310.000000000000000000
          Top = 390.000000000000000000
          Width = 350.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Link."status_str"]')
          ParentFont = False
        end
        object Memo59: TfrxMemoView
          Align = baLeft
          Left = 310.000000000000000000
          Top = 345.000000000000000000
          Width = 350.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Link."ESR_NORM"]')
          ParentFont = False
        end
        object Memo60: TfrxMemoView
          Align = baLeft
          Left = 310.000000000000000000
          Top = 360.000000000000000000
          Width = 350.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Link."BBER_REQUIRED"]')
          ParentFont = False
        end
        object Memo61: TfrxMemoView
          Align = baLeft
          Left = 310.000000000000000000
          Top = 315.000000000000000000
          Width = 350.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Link."KNG_NORM"]')
          ParentFont = False
        end
        object Memo62: TfrxMemoView
          Align = baLeft
          Left = 310.000000000000000000
          Top = 330.000000000000000000
          Width = 350.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Link."BBER_NORM"]')
          ParentFont = False
        end
        object Memo63: TfrxMemoView
          Align = baLeft
          Left = 310.000000000000000000
          Top = 285.000000000000000000
          Width = 350.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Link."SESR_NORM"]')
          ParentFont = False
        end
        object Memo64: TfrxMemoView
          Align = baLeft
          Left = 310.000000000000000000
          Top = 300.000000000000000000
          Width = 350.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Link."KNG"]')
          ParentFont = False
        end
        object Memo67: TfrxMemoView
          Align = baLeft
          Left = 310.000000000000000000
          Top = 255.000000000000000000
          Width = 350.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Link."SESR_INTERFERENCE"]')
          ParentFont = False
        end
        object Memo69: TfrxMemoView
          Align = baLeft
          Left = 310.000000000000000000
          Top = 270.000000000000000000
          Width = 350.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Link."SESR"]')
          ParentFont = False
        end
        object Memo71: TfrxMemoView
          Align = baLeft
          Left = 310.000000000000000000
          Top = 225.000000000000000000
          Width = 350.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Link."SESR_SUBREFRACTION"]')
          ParentFont = False
        end
        object Memo73: TfrxMemoView
          Align = baLeft
          Left = 310.000000000000000000
          Top = 240.000000000000000000
          Width = 350.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Link."SESR_RAIN"]')
          ParentFont = False
        end
        object Memo75: TfrxMemoView
          Align = baLeft
          Left = 310.000000000000000000
          Top = 210.000000000000000000
          Width = 350.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Link."MARGIN_DISTANCE"]')
          ParentFont = False
        end
        object Memo76: TfrxMemoView
          Align = baLeft
          Left = 310.000000000000000000
          Top = 195.000000000000000000
          Width = 350.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Link."MARGIN_HEIGHT"]')
          ParentFont = False
        end
        object Memo88: TfrxMemoView
          Align = baLeft
          Left = 485.000000000000000000
          Top = 105.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[antenna2."diameter"]')
          ParentFont = False
        end
        object Memo84: TfrxMemoView
          Align = baLeft
          Left = 485.000000000000000000
          Top = 120.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[antenna2."polarization"]')
          ParentFont = False
        end
        object Memo85: TfrxMemoView
          Align = baLeft
          Left = 310.000000000000000000
          Top = 135.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd1."rx_level_dBm"]')
          ParentFont = False
        end
        object Memo86: TfrxMemoView
          Align = baLeft
          Left = 485.000000000000000000
          Top = 135.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd2."rx_level_dBm"]')
          ParentFont = False
        end
        object Memo87: TfrxMemoView
          Align = baLeft
          Left = 310.000000000000000000
          Top = 150.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd1."kratnostBySpace_name"]')
          ParentFont = False
        end
        object Memo94: TfrxMemoView
          Align = baLeft
          Left = 485.000000000000000000
          Top = 150.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd2."kratnostBySpace_name"]')
          ParentFont = False
        end
        object Memo131: TfrxMemoView
          Align = baLeft
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          ParentFont = False
        end
        object Memo65: TfrxMemoView
          Align = baLeft
          Left = 310.000000000000000000
          Top = 15.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[antenna1."height"]')
          ParentFont = False
        end
        object Memo102: TfrxMemoView
          Align = baLeft
          Left = 485.000000000000000000
          Top = 15.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[antenna2."height"]')
          ParentFont = False
        end
      end
    end
    object Page6: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object Header1: TfrxHeader
        Height = 35.000000000000000000
        Top = 16.000000000000000000
        Width = 718.110700000000000000
        object Memo93: TfrxMemoView
          Align = baLeft
          Width = 60.000000000000000000
          Height = 35.000000000000000000
          Visible = False
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            'Index')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Align = baLeft
          Left = 60.000000000000000000
          Width = 100.000000000000000000
          Height = 35.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#160#1056#176#1057#1027#1057#1027#1057#8218#1056#1109#1057#1039#1056#1029#1056#1105#1056#181', '#1056#1108#1056#1112)
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Align = baLeft
          Left = 260.000000000000000000
          Width = 100.000000000000000000
          Height = 35.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1113#1057#1026#1056#1105#1056#1030#1056#1105#1056#183#1056#1029#1056#176' '#1056#183#1056#181#1056#1112#1056#187#1056#1105)
          ParentFont = False
        end
        object Memo120: TfrxMemoView
          Align = baLeft
          Left = 360.000000000000000000
          Width = 100.000000000000000000
          Height = 35.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#8217#1057#8249#1057#1027#1056#1109#1057#8218#1056#176' '#1056#1114#1056#1119)
          ParentFont = False
        end
        object Memo132: TfrxMemoView
          Align = baLeft
          Left = 160.000000000000000000
          Width = 100.000000000000000000
          Height = 35.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#8217#1057#8249#1057#1027#1056#1109#1057#8218#1056#176' '#1057#1026#1056#181#1056#187#1057#1034#1056#181#1057#8222#1056#176)
          ParentFont = False
        end
        object Memo101: TfrxMemoView
          Align = baLeft
          Left = 460.000000000000000000
          Width = 196.000000000000000000
          Height = 35.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1118#1056#1105#1056#1111' '#1056#1114#1056#1119)
          ParentFont = False
        end
      end
      object MasterData5: TfrxMasterData
        Height = 16.000000000000000000
        Top = 76.000000000000000000
        Width = 718.110700000000000000
        RowCount = 0
        object Memo89: TfrxMemoView
          Align = baLeft
          Left = 60.000000000000000000
          Width = 100.000000000000000000
          Height = 16.000000000000000000
          ShowHint = False
          DataField = 'distance_km'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Profile."distance_km"]')
          ParentFont = False
        end
        object Memo90: TfrxMemoView
          Align = baLeft
          Width = 60.000000000000000000
          Height = 16.000000000000000000
          Visible = False
          ShowHint = False
          DataField = 'index'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Profile."index"]')
          ParentFont = False
        end
        object Memo91: TfrxMemoView
          Align = baLeft
          Left = 260.000000000000000000
          Width = 100.000000000000000000
          Height = 16.000000000000000000
          ShowHint = False
          DataField = 'earth_h'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Profile."earth_h"]')
          ParentFont = False
        end
        object Memo92: TfrxMemoView
          Align = baLeft
          Left = 160.000000000000000000
          Width = 100.000000000000000000
          Height = 16.000000000000000000
          OnBeforePrint = 'Memo92OnBeforePrint'
          ShowHint = False
          DataField = 'ground_h'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Profile."ground_h"]')
          ParentFont = False
        end
        object Memo133: TfrxMemoView
          Align = baLeft
          Left = 360.000000000000000000
          Width = 100.000000000000000000
          Height = 16.000000000000000000
          ShowHint = False
          DataField = 'clutter_h'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Profile."clutter_h"]')
          ParentFont = False
        end
        object Shape_Color: TfrxShapeView
          Align = baLeft
          Left = 460.000000000000000000
          Width = 20.000000000000000000
          Height = 16.000000000000000000
          OnBeforePrint = 'Shape_ColorOnBeforePrint'
          ShowHint = False
          Color = clTeal
        end
        object Memo104: TfrxMemoView
          Align = baLeft
          Left = 480.000000000000000000
          Width = 176.000000000000000000
          Height = 16.000000000000000000
          ShowHint = False
          DataField = 'clutter_name'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Profile."clutter_name"]')
          ParentFont = False
        end
      end
      object Footer1: TfrxFooter
        Height = 8.000000000000000000
        Top = 116.000000000000000000
        Width = 718.110700000000000000
      end
    end
    object Page8: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object DetailData1: TfrxDetailData
        Height = 18.000000000000000000
        Top = 105.826840000000000000
        Width = 718.110700000000000000
        RowCount = 0
        object Itemscaption: TfrxMemoView
          Align = baWidth
          Left = 36.000000000000000000
          Width = 502.110700000000000000
          Height = 18.000000000000000000
          ShowHint = False
          DataField = 'caption'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Items."caption"]')
          ParentFont = False
        end
        object Memo77: TfrxMemoView
          Align = baRight
          Left = 538.110700000000000000
          Width = 90.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          DataField = 'value'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Items."value"]')
          ParentFont = False
        end
        object Memo78: TfrxMemoView
          Align = baRight
          Left = 628.110700000000000000
          Width = 90.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          DataField = 'value2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Items."value2"]')
          ParentFont = False
        end
        object Memo79: TfrxMemoView
          Align = baLeft
          Width = 36.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          DataField = 'id'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Items."id"]')
          ParentFont = False
        end
      end
      object MasterData6: TfrxMasterData
        Height = 24.000000000000000000
        Top = 60.472480000000000000
        Width = 718.110700000000000000
        RowCount = 0
        object groupcaption: TfrxMemoView
          Align = baWidth
          Left = 36.000000000000000000
          Width = 682.110700000000000000
          Height = 16.000000000000000000
          ShowHint = False
          DataField = 'caption'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[group."caption"]')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Align = baLeft
          Width = 36.000000000000000000
          Height = 16.000000000000000000
          ShowHint = False
          DataField = 'id'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[group."id"]')
          ParentFont = False
        end
      end
      object Header2: TfrxHeader
        Height = 20.000000000000000000
        Top = 18.897650000000000000
        Width = 718.110700000000000000
      end
      object Footer2: TfrxFooter
        Height = 20.000000000000000000
        Top = 147.401670000000000000
        Width = 718.110700000000000000
      end
      object Memo103: TfrxMemoView
        Align = baLeft
        Top = 15.333333330000000000
        Width = 434.000000000000000000
        Height = 16.666666670000000000
        ShowHint = False
        AutoWidth = True
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        Memo.UTF8 = (
          #1056#160#1056#181#1056#183#1057#1107#1056#187#1057#1034#1057#8218#1056#176#1057#8218#1057#8249' '#1057#1026#1056#176#1057#1027#1057#8225#1056#181#1057#8218#1056#1109#1056#1030)
        ParentFont = False
      end
    end
    object Subreport_right_: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object MasterData7: TfrxMasterData
        Height = 188.858282360000000000
        Top = 18.897650000000000000
        Width = 718.110700000000000000
        RowCount = 0
        object Memo105: TfrxMemoView
          Align = baRight
          Left = 494.724873228346500000
          Width = 110.000000000000000000
          Height = 30.236218030000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1119#1056#187#1056#1109#1057#8240#1056#176#1056#1169#1056#1108#1056#176)
          ParentFont = False
        end
        object Memo106: TfrxMemoView
          Align = baRight
          Left = 604.724873228346500000
          Width = 113.385826771653500000
          Height = 30.236220472440900000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Site2."name"]')
          ParentFont = False
        end
        object Memo108: TfrxMemoView
          Align = baRight
          Left = 494.724873228346500000
          Top = 30.236218030000000000
          Width = 110.000000000000000000
          Height = 15.118110236220500000
          ShowHint = False
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1113#1056#1109#1056#1109#1057#1026#1056#1169#1056#1105#1056#1029#1056#176#1057#8218#1057#8249' Pulkovo')
          ParentFont = False
        end
        object Memo109: TfrxMemoView
          Align = baRight
          Left = 494.724873228346500000
          Top = 90.708661420000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#8217#1057#8249#1057#1027#1056#1109#1057#8218#1056#176' '#1056#1111#1056#1109#1056#1169#1056#1030#1056#181#1057#1027#1056#176', m')
          ParentFont = False
        end
        object Memo110: TfrxMemoView
          Align = baRight
          Left = 494.724873228346500000
          Top = 105.897650000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1106#1056#183#1056#1105#1056#1112#1057#1107#1057#8218)
          ParentFont = False
        end
        object Memo111: TfrxMemoView
          Align = baRight
          Left = 494.724873228346500000
          Top = 120.897650000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1032#1056#1110#1056#1109#1056#187' '#1056#1112#1056#181#1057#1027#1057#8218#1056#176)
          ParentFont = False
        end
        object Memo112: TfrxMemoView
          Align = baRight
          Left = 494.724873228346500000
          Top = 135.897650000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#8221#1056#1105#1056#176#1056#1112#1056#181#1057#8218#1057#1026' '#1056#176#1056#1029#1057#8218#1056#181#1056#1029#1056#1029#1057#8249)
          ParentFont = False
        end
        object Memo114: TfrxMemoView
          Align = baRight
          Left = 604.724873228346500000
          Top = 30.236218030000000000
          Width = 113.385826771653500000
          Height = 15.118110236220500000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[DegreeToStr (<Site2."lat">)]')
          ParentFont = False
        end
        object Memo115: TfrxMemoView
          Align = baRight
          Left = 604.724873228346500000
          Top = 90.708661420000000000
          Width = 113.385826771653500000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[antenna2."height"]')
          ParentFont = False
        end
        object Memo116: TfrxMemoView
          Align = baRight
          Left = 604.724873228346500000
          Top = 105.897650000000000000
          Width = 113.385826771653500000
          Height = 15.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."azimuth2"]')
          ParentFont = False
        end
        object Memo117: TfrxMemoView
          Align = baRight
          Left = 604.724873228346500000
          Top = 135.897650000000000000
          Width = 113.385826771653500000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Antenna2."diameter"]')
          ParentFont = False
        end
        object Memo118: TfrxMemoView
          Align = baRight
          Left = 604.724873228346500000
          Top = 45.354330708661400000
          Width = 113.385826771653500000
          Height = 15.118110236220500000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[DegreeToStr (<Site2."lon">)]')
          ParentFont = False
        end
        object Memo130: TfrxMemoView
          Align = baRight
          Left = 494.724873228346500000
          Top = 45.354330708661400000
          Width = 110.000000000000000000
          Height = 15.118110236220500000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Align = baRight
          Left = 604.724873228346500000
          Top = 120.897650000000000000
          Width = 113.385826771653500000
          Height = 15.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."ELEVATION_angle_2"]')
          ParentFont = False
        end
        object Memo107: TfrxMemoView
          Align = baRight
          Left = 494.724873228346500000
          Top = 60.472440944881890000
          Width = 110.000000000000000000
          Height = 15.118110236220500000
          ShowHint = False
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1113#1056#1109#1056#1109#1057#1026#1056#1169#1056#1105#1056#1029#1056#176#1057#8218#1057#8249' WGS')
          ParentFont = False
        end
        object Memo113: TfrxMemoView
          Align = baRight
          Left = 604.724873228346500000
          Top = 60.472440940000000000
          Width = 113.385826771653500000
          Height = 15.118110236220500000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[DegreeToStr (<Site2."lat_wgs">)]')
          ParentFont = False
        end
        object Memo119: TfrxMemoView
          Align = baRight
          Left = 604.724873228346500000
          Top = 75.590551180000000000
          Width = 113.385826771653500000
          Height = 15.118110236220500000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[DegreeToStr (<Site2."lon_wgs">)]')
          ParentFont = False
        end
        object Memo129: TfrxMemoView
          Align = baRight
          Left = 494.724873228346500000
          Top = 75.590551181102400000
          Width = 110.000000000000000000
          Height = 15.118110236220500000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          ParentFont = False
        end
        object Memo140: TfrxMemoView
          Align = baRight
          Left = 494.724873228346500000
          Top = 151.181102360000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1032#1057#1027#1056#1105#1056#187#1056#181#1056#1029#1056#1105#1056#181' '#1056#176#1056#1029#1057#8218#1056#181#1056#1029#1056#1029#1057#8249)
          ParentFont = False
        end
        object Memo141: TfrxMemoView
          Align = baRight
          Left = 604.724873228346500000
          Top = 151.181102360000000000
          Width = 113.385826771653500000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Antenna2."gain"]')
          ParentFont = False
        end
      end
    end
  end
  object frxReport_MTS: TfrxReport
    Version = '4.15.6'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports3'
    PreviewOptions.AllowEdit = False
    PreviewOptions.Buttons = [pbPrint, pbExport, pbZoom, pbTools, pbNavigator, pbExportQuick, pbNoFullScreen]
    PreviewOptions.ThumbnailVisible = True
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = #1055#1086' '#1091#1084#1086#1083#1095#1072#1085#1080#1102
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 41730.469225335600000000
    ReportOptions.LastChange = 41939.805246851900000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      ''
      '         '
      ''
      ''
      'procedure frxReportOnStartReport(Sender: TfrxComponent);'
      'const'
      '  DEF_MAP_H = 800;'
      '  '
      '  DEF_H = 450;'
      '  DEF_OFFSET = 25;'
      ''
      '  DEF_TOP = 20;                                  '
      'begin'
      '//  Picture_map.Width:=200;                '
      '  Picture_map.Height     :=DEF_MAP_H;'
      '    '
      '  Picture_profile.Height :=DEF_H;                '
      '            '
      '  '
      '  Child_map.Height:=DEF_MAP_H;'
      '    '
      '  Child_profile.Height:=DEF_H + DEF_OFFSET;              '
      '        '
      '      '
      'end;  '
      '             '
      '         '
      ''
      '//------------------------------------------------------------'
      'function DegreeToStr (aValue: double): String;'
      '//------------------------------------------------------------'
      'const'
      '//  STR_BLPOINT_FORMAT1 = '#39'%d'#176'%.2d'#39#39'%s%1.2f'#39#39#39#39#39';'
      '  STR_BLPOINT_FORMAT1 = '#39'%d'#176' %.2d'#39#39' %1.2f'#39#39#39#39#39';'
      'var    '
      ' iDeg,iMin:integer; '
      ' eSec: Double;   '
      '           '
      'begin'
      '  geo_DecodeDegree_(aValue, iDeg,iMin,eSec);'
      '                           '
      ' // angle.Sec:=TruncFloat(angle.Sec,2);'
      ' '
      '    Result:=Format(STR_BLPOINT_FORMAT1, [iDeg, iMin, eSec]);'
      ''
      '  if aValue<0 then'
      '    Result:='#39'-'#39'+Result;'
      'end;'
      '  '
      '           '
      ''
      '//------------------------------------------------------------'
      
        'procedure geo_DecodeDegree_(aValue: Double; var aDeg,aMin:intege' +
        'r; var aSec:'
      '    Double);'
      '//------------------------------------------------------------'
      'const'
      ' EXP_ = 0.000000001;'
      '     //97,033333333'
      'var'
      '  bIsNegative: Boolean;'
      '  e: Double;'
      '  eMinutes: Double;'
      '  eSec: Double;'
      '  eSeconds: Double;'
      'begin'
      '  bIsNegative := aValue<0;'
      ''
      '  aValue :=Abs(aValue);'
      ''
      '  aDeg:=Trunc(aValue);'
      ''
      ' // e:=Abs(aValue)*60;'
      ''
      '//  , Frac(X) = X - Int(X).'
      ''
      '  aMin:=Trunc(Frac(aValue)*60);'
      ''
      '//  Result.Sec:=Frac(Frac(Abs(aValue))*60)*60;'
      '  e:= (aValue - Trunc(aValue)) * 60;'
      '  aMin:=Trunc(e);'
      ''
      ' eMinutes := Trunc (e);'
      ' eSeconds := (e - Trunc (e)) * 60 ;'
      ''
      ''
      '  aSec:=(((aValue - aDeg) *60) - aMin) * 60;'
      ''
      '  if (aSec - 60) > EXP_ then'
      '  begin'
      '    Inc(aMin);'
      '    aSec := 0;'
      '  end;'
      ''
      ''
      '  if bIsNegative then'
      '    aDeg:=-aDeg;'
      '          '
      'end;'
      '        '
      ''
      '       '
      'begin'
      ''
      'end.    ')
    OnStartReport = 'frxReportOnStartReport'
    Left = 422
    Top = 68
    Datasets = <
      item
      end
      item
      end
      item
      end
      item
      end
      item
      end
      item
      end
      item
      end
      item
      end
      item
      end
      item
      end
      item
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 9.260416666666670000
      BottomMargin = 10.583333333333300000
      LargeDesignHeight = True
      OnBeforePrint = 'Page1OnBeforePrint'
      object Child_Header: TfrxChild
        Height = 187.000000000000000000
        Top = 181.417440000000000000
        Width = 718.110700000000000000
        Child = frxReport_MTS.Child_Summary
        object Memo105: TfrxMemoView
          Align = baRight
          Left = 478.110700000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1119#1056#187#1056#1109#1057#8240#1056#176#1056#1169#1056#1108#1056#176)
          ParentFont = False
        end
        object Memo106: TfrxMemoView
          Align = baRight
          Left = 588.110700000000000000
          Width = 130.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Site2."name"]')
          ParentFont = False
        end
        object Memo108: TfrxMemoView
          Align = baRight
          Left = 478.110700000000000000
          Top = 30.000000000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1113#1056#1109#1056#1109#1057#1026#1056#1169#1056#1105#1056#1029#1056#176#1057#8218#1057#8249' Pulkovo')
          ParentFont = False
        end
        object Memo109: TfrxMemoView
          Align = baRight
          Left = 478.110700000000000000
          Top = 90.000000000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#8217#1057#8249#1057#1027#1056#1109#1057#8218#1056#176' '#1056#1111#1056#1109#1056#1169#1056#1030#1056#181#1057#1027#1056#176', m')
          ParentFont = False
        end
        object Memo110: TfrxMemoView
          Align = baRight
          Left = 478.110700000000000000
          Top = 105.000000000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1106#1056#183#1056#1105#1056#1112#1057#1107#1057#8218)
          ParentFont = False
        end
        object Memo111: TfrxMemoView
          Align = baRight
          Left = 478.110700000000000000
          Top = 120.000000000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1032#1056#1110#1056#1109#1056#187' '#1056#1112#1056#181#1057#1027#1057#8218#1056#176)
          ParentFont = False
        end
        object Memo112: TfrxMemoView
          Align = baRight
          Left = 478.110700000000000000
          Top = 135.000000000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#8221#1056#1105#1056#176#1056#1112#1056#181#1057#8218#1057#1026' '#1056#176#1056#1029#1057#8218#1056#181#1056#1029#1056#1029#1057#8249)
          ParentFont = False
        end
        object Memo114: TfrxMemoView
          Align = baRight
          Left = 588.110700000000000000
          Top = 30.000000000000000000
          Width = 130.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[DegreeToStr (<Site2."lat">)]')
          ParentFont = False
        end
        object Memo115: TfrxMemoView
          Align = baRight
          Left = 588.110700000000000000
          Top = 90.000000000000000000
          Width = 130.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[antenna2."height"]')
          ParentFont = False
        end
        object Memo116: TfrxMemoView
          Align = baRight
          Left = 588.110700000000000000
          Top = 105.000000000000000000
          Width = 130.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."azimuth2"]')
          ParentFont = False
        end
        object Memo117: TfrxMemoView
          Align = baRight
          Left = 588.110700000000000000
          Top = 135.000000000000000000
          Width = 130.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Antenna2."diameter"]')
          ParentFont = False
        end
        object Memo118: TfrxMemoView
          Align = baRight
          Left = 588.110700000000000000
          Top = 45.000000000000000000
          Width = 130.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[DegreeToStr (<Site2."lon">)]')
          ParentFont = False
        end
        object Memo130: TfrxMemoView
          Align = baRight
          Left = 478.110700000000000000
          Top = 45.000000000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Align = baRight
          Left = 588.110700000000000000
          Top = 120.000000000000000000
          Width = 130.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."ELEVATION_angle_2"]')
          ParentFont = False
        end
        object Memo66: TfrxMemoView
          Align = baRight
          Left = 478.110700000000000000
          Top = 150.000000000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1032#1057#1027#1056#1105#1056#187#1056#181#1056#1029#1056#1105#1056#181' '#1056#176#1056#1029#1057#8218#1056#181#1056#1029#1056#1029#1057#8249)
          ParentFont = False
        end
        object Memo99: TfrxMemoView
          Align = baRight
          Left = 588.110700000000000000
          Top = 150.000000000000000000
          Width = 130.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Antenna2."gain"]')
          ParentFont = False
        end
        object Memo43: TfrxMemoView
          Align = baLeft
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1119#1056#187#1056#1109#1057#8240#1056#176#1056#1169#1056#1108#1056#176)
          ParentFont = False
        end
        object Memo_left_1: TfrxMemoView
          Align = baLeft
          Left = 110.000000000000000000
          Width = 125.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Site1."name"]')
          ParentFont = False
        end
        object Memo68: TfrxMemoView
          Align = baLeft
          Top = 30.000000000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1113#1056#1109#1056#1109#1057#1026#1056#1169#1056#1105#1056#1029#1056#176#1057#8218#1057#8249' Pulkovo')
          ParentFont = False
        end
        object Memo70: TfrxMemoView
          Align = baLeft
          Top = 90.000000000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#8217#1057#8249#1057#1027#1056#1109#1057#8218#1056#176' '#1056#1111#1056#1109#1056#1169#1056#1030#1056#181#1057#1027#1056#176', m')
          ParentFont = False
        end
        object Memo72: TfrxMemoView
          Align = baLeft
          Top = 105.000000000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1106#1056#183#1056#1105#1056#1112#1057#1107#1057#8218)
          ParentFont = False
        end
        object Memo74: TfrxMemoView
          Align = baLeft
          Top = 120.000000000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1032#1056#1110#1056#1109#1056#187' '#1056#1112#1056#181#1057#1027#1057#8218#1056#176)
          ParentFont = False
        end
        object Memo100: TfrxMemoView
          Align = baLeft
          Top = 135.000000000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#8221#1056#1105#1056#176#1056#1112#1056#181#1057#8218#1057#1026' '#1056#176#1056#1029#1057#8218#1056#181#1056#1029#1056#1029#1057#8249)
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Align = baLeft
          Left = 110.000000000000000000
          Top = 30.000000000000000000
          Width = 125.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[DegreeToStr (<Site1."lat">)]')
          ParentFont = False
        end
        object Memo1: TfrxMemoView
          Align = baLeft
          Left = 110.000000000000000000
          Top = 90.000000000000000000
          Width = 125.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[antenna1."height"]')
          ParentFont = False
        end
        object Memo95: TfrxMemoView
          Align = baLeft
          Left = 110.000000000000000000
          Top = 105.000000000000000000
          Width = 125.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."azimuth1"]')
          ParentFont = False
        end
        object Memo97: TfrxMemoView
          Align = baLeft
          Left = 110.000000000000000000
          Top = 135.000000000000000000
          Width = 125.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[antenna1."diameter"]')
          ParentFont = False
        end
        object Memo98: TfrxMemoView
          Align = baLeft
          Left = 110.000000000000000000
          Top = 45.000000000000000000
          Width = 125.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[DegreeToStr (<Site1."lon">)]')
          ParentFont = False
        end
        object Memo96: TfrxMemoView
          Align = baLeft
          Left = 110.000000000000000000
          Top = 120.000000000000000000
          Width = 125.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."ELEVATION_angle_1"]')
          ParentFont = False
        end
        object Memo121: TfrxMemoView
          Align = baLeft
          Top = 45.000000000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Align = baLeft
          Top = 150.000000000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1032#1057#1027#1056#1105#1056#187#1056#181#1056#1029#1056#1105#1056#181' '#1056#176#1056#1029#1057#8218#1056#181#1056#1029#1056#1029#1057#8249)
          ParentFont = False
        end
        object Memo30: TfrxMemoView
          Align = baLeft
          Left = 110.000000000000000000
          Top = 150.000000000000000000
          Width = 125.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[antenna1."gain"]')
          ParentFont = False
        end
        object Memo45: TfrxMemoView
          Left = 243.220470000000000000
          Top = 15.000000000000000000
          Width = 144.204726490000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#8221#1056#187#1056#1105#1056#1029#1056#176' '#1056#1105#1056#1029#1057#8218#1056#181#1057#1026#1056#1030#1056#176#1056#187#1056#176', km')
          ParentFont = False
        end
        object Memo47: TfrxMemoView
          Left = 243.220470000000000000
          Top = 30.000000000000000000
          Width = 144.204726490000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1032#1057#1026#1056#1109#1056#1030#1056#181#1056#1029#1057#1034' '#1057#1027#1056#1105#1056#1110#1056#1029#1056#176#1056#187#1056#176', dBm')
          ParentFont = False
        end
        object Memo49: TfrxMemoView
          Left = 243.220470000000000000
          Top = 45.000000000000000000
          Width = 144.204726490000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            'P'#1056#176#1056#177#1056#1109#1057#8225#1056#176#1057#1039' '#1057#8225#1056#176#1057#1027#1057#8218#1056#1109#1057#8218#1056#176', GHz')
          ParentFont = False
        end
        object Memo51: TfrxMemoView
          Left = 243.220470000000000000
          Top = 60.000000000000000000
          Width = 144.204726490000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1114#1056#1109#1057#8240#1056#1029#1056#1109#1057#1027#1057#8218#1057#1034' '#1056#1111#1056#181#1057#1026#1056#181#1056#1169#1056#176#1057#8218#1057#8225#1056#1105#1056#1108#1056#176', dBm')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          Left = 243.220470000000000000
          Top = 75.000000000000000000
          Width = 144.204726490000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1038#1056#1108#1056#1109#1057#1026#1056#1109#1057#1027#1057#8218#1057#1034' '#1056#1111#1056#181#1057#1026#1056#181#1056#1169#1056#176#1057#8225#1056#1105', Mbit/s')
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          Left = 243.220470000000000000
          Top = 90.000000000000000000
          Width = 144.204726490000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1118#1056#1105#1056#1111' '#1056#1109#1056#177#1056#1109#1057#1026#1057#1107#1056#1169#1056#1109#1056#1030#1056#176#1056#1029#1056#1105#1057#1039)
          ParentFont = False
        end
        object Memo123: TfrxMemoView
          Left = 387.425196490000000000
          Top = 30.000000000000000000
          Width = 74.905973510000000000
          Height = 15.000000000000000000
          ShowHint = False
          DataField = 'rx_level_dBm'
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd1."rx_level_dBm"]')
          ParentFont = False
        end
        object Memo124: TfrxMemoView
          Left = 387.425196490000000000
          Top = 75.000000000000000000
          Width = 74.905973510000000000
          Height = 15.000000000000000000
          ShowHint = False
          DataField = 'bitrate_Mbps'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd1."bitrate_Mbps"]')
          ParentFont = False
        end
        object Memo125: TfrxMemoView
          Left = 387.425196490000000000
          Top = 90.000000000000000000
          Width = 74.905973510000000000
          Height = 15.000000000000000000
          ShowHint = False
          DataField = 'LinkEndType_name'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd1."LinkEndType_name"]')
          ParentFont = False
        end
        object Memo127: TfrxMemoView
          Left = 387.425196490000000000
          Top = 45.000000000000000000
          Width = 74.905973510000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[FloatToStr( <LinkEnd1."tx_freq_MHz"> / 1000 )]')
          ParentFont = False
        end
        object Memo128: TfrxMemoView
          Left = 387.425196490000000000
          Top = 15.000000000000000000
          Width = 74.905973510000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."length_km"]')
          ParentFont = False
        end
        object Memo126: TfrxMemoView
          Left = 387.425196490000000000
          Top = 60.000000000000000000
          Width = 74.905973510000000000
          Height = 15.000000000000000000
          ShowHint = False
          DataField = 'power_dBm'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd1."power_dBm"]')
          ParentFont = False
        end
        object Memo42: TfrxMemoView
          Left = 243.220470000000000000
          Width = 144.204726490000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1119#1057#1026#1056#1105#1056#183#1056#1029#1056#176#1056#1108' '#1056#1111#1057#1026#1056#1105#1056#1110#1056#1109#1056#1169#1056#1029#1056#1109#1057#1027#1057#8218#1056#1105)
          ParentFont = False
        end
        object Memo122: TfrxMemoView
          Left = 387.425196490000000000
          Width = 74.905973510000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."status_str"]')
          ParentFont = False
        end
        object Memo134: TfrxMemoView
          Left = 243.220470000000000000
          Top = 105.000000000000000000
          Width = 144.204726490000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#8212#1056#176#1056#1111#1056#176#1057#1027' '#1056#1029#1056#176' '#1056#183#1056#176#1056#1112#1056#1105#1057#1026#1056#176#1056#1029#1056#1105#1057#1039', dB')
          ParentFont = False
        end
        object Memo135: TfrxMemoView
          Left = 387.425196490000000000
          Top = 105.000000000000000000
          Width = 74.905973510000000000
          Height = 15.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."fade_margin_dB"]')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Align = baLeft
          Top = 60.000000000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1113#1056#1109#1056#1109#1057#1026#1056#1169#1056#1105#1056#1029#1056#176#1057#8218#1057#8249' WGS')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Align = baLeft
          Left = 110.000000000000000000
          Top = 60.000000000000000000
          Width = 125.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[DegreeToStr (<Site1."lat_WGS">)]')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Align = baLeft
          Left = 110.000000000000000000
          Top = 75.000000000000000000
          Width = 125.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[DegreeToStr (<Site1."lon_WGS">)]')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Align = baLeft
          Top = 75.000000000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          Align = baRight
          Left = 478.110700000000000000
          Top = 60.000000000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1113#1056#1109#1056#1109#1057#1026#1056#1169#1056#1105#1056#1029#1056#176#1057#8218#1057#8249' WGS')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          Align = baRight
          Left = 588.110700000000000000
          Top = 60.000000000000000000
          Width = 130.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[DegreeToStr (<Site2."lat_wgs">)]')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          Align = baRight
          Left = 588.110700000000000000
          Top = 75.000000000000000000
          Width = 130.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[DegreeToStr (<Site2."lon_wgs">)]')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          Align = baRight
          Left = 478.110700000000000000
          Top = 75.000000000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          Align = baLeft
          Top = 15.000000000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1113#1056#1109#1056#1169)
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          Align = baLeft
          Left = 110.000000000000000000
          Top = 15.000000000000000000
          Width = 125.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Site1."bee_beenet"]')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          Align = baRight
          Left = 478.110700000000000000
          Top = 15.000000000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1113#1056#1109#1056#1169)
          ParentFont = False
        end
        object Memo25: TfrxMemoView
          Align = baRight
          Left = 588.110700000000000000
          Top = 15.000000000000000000
          Width = 130.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Site2."bee_beenet"]')
          ParentFont = False
        end
      end
      object Child_Summary: TfrxChild
        Height = 456.000000000000000000
        Top = 393.071120000000000000
        Width = 718.110700000000000000
        Child = frxReport_MTS.Child_map
        object Memo3: TfrxMemoView
          Align = baLeft
          Top = 15.000000000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#8217#1057#8249#1057#1027#1056#1109#1057#8218#1056#176', m')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Align = baLeft
          Top = 30.000000000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1106#1056#183#1056#1105#1056#1112#1057#1107#1057#8218' ')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Align = baLeft
          Top = 45.000000000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1118#1056#1105#1056#1111' '#1056#1109#1056#177#1056#1109#1057#1026#1057#1107#1056#1169#1056#1109#1056#1030#1056#176#1056#1029#1056#1105#1057#1039' ')
          ParentFont = False
        end
        object Memo77: TfrxMemoView
          Align = baLeft
          Top = 60.000000000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#167#1056#176#1057#1027#1057#8218#1056#1109#1057#8218#1056#176' '#1056#1111#1056#181#1057#1026#1056#181#1056#1169#1056#176#1057#8225#1056#1105', MHz')
          ParentFont = False
        end
        object Memo78: TfrxMemoView
          Align = baLeft
          Top = 75.000000000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1038#1056#1108#1056#1109#1057#1026#1056#1109#1057#1027#1057#8218#1057#1034' '#1056#1111#1056#181#1057#1026#1056#181#1056#1169#1056#176#1057#8225#1056#1105', Mbit/s')
          ParentFont = False
        end
        object Memo79: TfrxMemoView
          Align = baLeft
          Top = 90.000000000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1114#1056#1109#1057#8240#1056#1029#1056#1109#1057#1027#1057#8218#1057#1034', dBm')
          ParentFont = False
        end
        object Memo81: TfrxMemoView
          Align = baLeft
          Top = 105.000000000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#8221#1056#1105#1056#176#1056#1112#1056#181#1057#8218#1057#1026' '#1056#176#1056#1029#1057#8218#1056#181#1056#1029#1056#1029#1057#8249', m')
          ParentFont = False
        end
        object Memo82: TfrxMemoView
          Align = baLeft
          Top = 150.000000000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            
              #1056#1038#1057#1026#1056#181#1056#1169#1056#1029#1057#1039#1057#1039' '#1056#1112#1056#1109#1057#8240#1056#1029#1056#1109#1057#1027#1057#8218#1057#1034' '#1056#1029#1056#176' '#1056#1030#1057#8230#1056#1109#1056#1169#1056#181' '#1056#1111#1057#1026#1056#1105#1056#181#1056#1112#1056#1029#1056#1105#1056#1108 +
              #1056#176', dBm')
          ParentFont = False
        end
        object Memo83: TfrxMemoView
          Align = baLeft
          Top = 120.000000000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1119#1056#1109#1056#187#1057#1039#1057#1026#1056#1105#1056#183#1056#176#1057#8224#1056#1105#1057#1039' ')
          ParentFont = False
        end
        object Memo89: TfrxMemoView
          Align = baLeft
          Top = 165.000000000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#160#1056#181#1056#183#1056#181#1057#1026#1056#1030#1056#1105#1057#1026#1056#1109#1056#1030#1056#176#1056#1029#1056#1105#1056#181' ')
          ParentFont = False
        end
        object Memo90: TfrxMemoView
          Align = baLeft
          Top = 180.000000000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1119#1056#1109#1057#1026#1056#1109#1056#1110' '#1056#1111#1057#1026#1056#1105' BER=10-3 ')
          ParentFont = False
        end
        object Memo91: TfrxMemoView
          Align = baLeft
          Top = 210.000000000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1114#1056#1105#1056#1029#1056#1105#1056#1112#1056#176#1056#187#1057#1034#1056#1029#1057#8249#1056#8470' '#1056#1109#1057#8218#1056#1029#1056#1109#1057#1027#1056#1105#1057#8218#1056#181#1056#187#1057#1034#1056#1029#1057#8249#1056#8470' '#1056#1111#1057#1026#1056#1109#1057#1027#1056#1030#1056#181#1057#8218)
          ParentFont = False
        end
        object Memo92: TfrxMemoView
          Align = baLeft
          Top = 195.000000000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1119#1056#1109#1057#1026#1056#1109#1056#1110' '#1056#1111#1057#1026#1056#1105' BER=10-6 ')
          ParentFont = False
        end
        object Memo93: TfrxMemoView
          Align = baLeft
          Top = 225.000000000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1119#1056#1109#1056#187#1056#1109#1056#182#1056#181#1056#1029#1056#1105#1056#181' '#1057#8218#1056#1109#1057#8225#1056#1108#1056#1105' '#1056#1111#1057#1026#1056#1109#1057#1027#1056#1030#1056#181#1057#8218#1056#176', km ')
          ParentFont = False
        end
        object Memo101: TfrxMemoView
          Align = baLeft
          Top = 240.000000000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            'SESR '#1057#1027#1057#1107#1056#177#1057#1026#1056#181#1057#8222#1056#1108#1057#8224#1056#1105#1056#1109#1056#1029#1056#1029#1056#176#1057#1039' '#1057#1027#1056#1109#1057#1027#1057#8218#1056#176#1056#1030#1056#187#1057#1039#1057#1035#1057#8240#1056#176#1057#1039', % ')
          ParentFont = False
        end
        object Memo103: TfrxMemoView
          Align = baLeft
          Top = 255.000000000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            'SESR '#1056#1169#1056#1109#1056#182#1056#1169#1056#181#1056#1030#1056#176#1057#1039' '#1057#1027#1056#1109#1057#1027#1057#8218#1056#176#1056#1030#1056#187#1057#1039#1057#1035#1057#8240#1056#176#1057#1039', % ')
          ParentFont = False
        end
        object Memo104: TfrxMemoView
          Align = baLeft
          Top = 270.000000000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            
              'SESR '#1056#1105#1056#1029#1057#8218#1056#181#1057#1026#1057#8222#1056#181#1057#1026#1056#181#1056#1029#1057#8224#1056#1105#1056#1109#1056#1029#1056#1029#1056#176#1057#1039' '#1057#1027#1056#1109#1057#1027#1057#8218#1056#176#1056#1030#1056#187#1057#1039#1057#1035#1057#8240#1056#176#1057#1039 +
              ', % ')
          ParentFont = False
        end
        object Memo107: TfrxMemoView
          Align = baLeft
          Top = 285.000000000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            
              #1056#152#1057#8218#1056#1109#1056#1110#1056#1109#1056#1030#1057#8249#1056#8470' '#1056#1111#1056#1109#1056#1108#1056#176#1056#183#1056#176#1057#8218#1056#181#1056#187#1057#1034' '#1056#1108#1056#176#1057#8225#1056#181#1057#1027#1057#8218#1056#1030#1056#176' '#1056#1111#1056#1109' '#1056#1109#1057#8364 +
              #1056#1105#1056#177#1056#1108#1056#176#1056#1112' SESR, % ')
          ParentFont = False
        end
        object Memo113: TfrxMemoView
          Align = baLeft
          Top = 300.000000000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            
              #1056#1116#1056#1109#1057#1026#1056#1112#1056#1105#1057#1026#1057#1107#1056#181#1056#1112#1056#1109#1056#181' '#1056#183#1056#1029#1056#176#1057#8225#1056#181#1056#1029#1056#1105#1056#181' SESR '#1056#1029#1056#176' '#1056#1105#1056#1029#1057#8218#1056#181#1057#1026#1056#1030#1056#176 +
              #1056#187#1056#181', % ')
          ParentFont = False
        end
        object Memo119: TfrxMemoView
          Align = baLeft
          Top = 315.000000000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            
              #1056#152#1057#8218#1056#1109#1056#1110#1056#1109#1056#1030#1057#8249#1056#8470' '#1056#1111#1056#1109#1056#1108#1056#176#1056#183#1056#176#1057#8218#1056#181#1056#187#1057#1034' '#1056#1029#1056#181#1056#1110#1056#1109#1057#8218#1056#1109#1056#1030#1056#1029#1056#1109#1057#1027#1057#8218#1056#1105' '#1056 +
              #1113#1056#1029#1056#1110', %')
          ParentFont = False
        end
        object Memo120: TfrxMemoView
          Align = baLeft
          Top = 330.000000000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            
              #1056#1116#1056#1109#1057#1026#1056#1112#1056#1105#1057#1026#1057#1107#1056#181#1056#1112#1056#1109#1056#181' '#1056#183#1056#1029#1056#176#1057#8225#1056#181#1056#1029#1056#1105#1056#181' '#1056#1113#1056#1029#1056#1110' '#1056#1029#1056#176' '#1056#1105#1056#1029#1057#8218#1056#181#1057#1026#1056#1030 +
              #1056#176#1056#187#1056#181', % ')
          ParentFont = False
        end
        object Memo129: TfrxMemoView
          Align = baLeft
          Top = 390.000000000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            'ESR '#1057#8218#1057#1026#1056#181#1056#177', % ')
          ParentFont = False
        end
        object Memo132: TfrxMemoView
          Align = baLeft
          Top = 345.000000000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            'BBER '#1056#1029#1056#1109#1057#1026#1056#1112', %')
          ParentFont = False
        end
        object Memo133: TfrxMemoView
          Align = baLeft
          Top = 375.000000000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            'BBER '#1057#8218#1057#1026#1056#181#1056#177', % ')
          ParentFont = False
        end
        object Memo136: TfrxMemoView
          Align = baLeft
          Top = 360.000000000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            'ESR '#1056#1029#1056#1109#1057#1026#1056#1112', % ')
          ParentFont = False
        end
        object Memo139: TfrxMemoView
          Align = baLeft
          Top = 405.000000000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1119#1057#1026#1056#1105#1056#1110#1056#1109#1056#1169#1056#1029#1056#1109#1057#1027#1057#8218#1057#1034' ')
          ParentFont = False
        end
        object Memo140: TfrxMemoView
          Align = baLeft
          Left = 310.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Site1."name"]')
          ParentFont = False
        end
        object Memo141: TfrxMemoView
          Align = baLeft
          Left = 485.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Site2."name"]')
          ParentFont = False
        end
        object Memo142: TfrxMemoView
          Align = baLeft
          Left = 310.000000000000000000
          Top = 30.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."azimuth1"]')
          ParentFont = False
        end
        object Memo143: TfrxMemoView
          Align = baLeft
          Left = 485.000000000000000000
          Top = 30.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."azimuth2"]')
          ParentFont = False
        end
        object Memo144: TfrxMemoView
          Align = baLeft
          Left = 310.000000000000000000
          Top = 180.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd1."threshold_BER_3"]')
          ParentFont = False
        end
        object Memo145: TfrxMemoView
          Align = baLeft
          Left = 310.000000000000000000
          Top = 195.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd1."threshold_BER_6"]')
          ParentFont = False
        end
        object Memo146: TfrxMemoView
          Align = baLeft
          Left = 310.000000000000000000
          Top = 105.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[antenna1."diameter"]')
          ParentFont = False
        end
        object Memo147: TfrxMemoView
          Align = baLeft
          Left = 310.000000000000000000
          Top = 120.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[antenna1."polarization"]')
          ParentFont = False
        end
        object Memo148: TfrxMemoView
          Align = baLeft
          Left = 310.000000000000000000
          Top = 75.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd1."bitrate_Mbps"]')
          ParentFont = False
        end
        object Memo149: TfrxMemoView
          Align = baLeft
          Left = 310.000000000000000000
          Top = 90.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd1."power_dBm"]')
          ParentFont = False
        end
        object Memo150: TfrxMemoView
          Align = baLeft
          Left = 310.000000000000000000
          Top = 45.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd1."LinkEndType_name"]')
          ParentFont = False
        end
        object Memo151: TfrxMemoView
          Align = baLeft
          Left = 310.000000000000000000
          Top = 60.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd1."tx_freq_MHz"]')
          ParentFont = False
        end
        object Memo152: TfrxMemoView
          Align = baLeft
          Left = 485.000000000000000000
          Top = 45.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd2."LinkEndType_name"]')
          ParentFont = False
        end
        object Memo153: TfrxMemoView
          Align = baLeft
          Left = 485.000000000000000000
          Top = 60.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd2."tx_freq_MHz"]')
          ParentFont = False
        end
        object Memo154: TfrxMemoView
          Align = baLeft
          Left = 485.000000000000000000
          Top = 75.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd2."bitrate_Mbps"]')
          ParentFont = False
        end
        object Memo155: TfrxMemoView
          Align = baLeft
          Left = 485.000000000000000000
          Top = 90.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd2."power_dBm"]')
          ParentFont = False
        end
        object Memo156: TfrxMemoView
          Align = baLeft
          Left = 485.000000000000000000
          Top = 180.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd2."threshold_BER_3"]')
          ParentFont = False
        end
        object Memo157: TfrxMemoView
          Align = baLeft
          Left = 485.000000000000000000
          Top = 195.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd2."threshold_BER_6"]')
          ParentFont = False
        end
        object Memo158: TfrxMemoView
          Align = baLeft
          Left = 310.000000000000000000
          Top = 390.000000000000000000
          Width = 350.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Link."ESR_REQUIRED"]')
          ParentFont = False
        end
        object Memo159: TfrxMemoView
          Align = baLeft
          Left = 310.000000000000000000
          Top = 405.000000000000000000
          Width = 350.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Link."status_str"]')
          ParentFont = False
        end
        object Memo160: TfrxMemoView
          Align = baLeft
          Left = 310.000000000000000000
          Top = 360.000000000000000000
          Width = 350.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Link."ESR_NORM"]')
          ParentFont = False
        end
        object Memo161: TfrxMemoView
          Align = baLeft
          Left = 310.000000000000000000
          Top = 375.000000000000000000
          Width = 350.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Link."BBER_REQUIRED"]')
          ParentFont = False
        end
        object Memo162: TfrxMemoView
          Align = baLeft
          Left = 310.000000000000000000
          Top = 330.000000000000000000
          Width = 350.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Link."KNG_NORM"]')
          ParentFont = False
        end
        object Memo163: TfrxMemoView
          Align = baLeft
          Left = 310.000000000000000000
          Top = 345.000000000000000000
          Width = 350.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Link."BBER_NORM"]')
          ParentFont = False
        end
        object Memo164: TfrxMemoView
          Align = baLeft
          Left = 310.000000000000000000
          Top = 300.000000000000000000
          Width = 350.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Link."SESR_NORM"]')
          ParentFont = False
        end
        object Memo165: TfrxMemoView
          Align = baLeft
          Left = 310.000000000000000000
          Top = 315.000000000000000000
          Width = 350.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Link."KNG"]')
          ParentFont = False
        end
        object Memo166: TfrxMemoView
          Align = baLeft
          Left = 310.000000000000000000
          Top = 270.000000000000000000
          Width = 350.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Link."SESR_INTERFERENCE"]')
          ParentFont = False
        end
        object Memo167: TfrxMemoView
          Align = baLeft
          Left = 310.000000000000000000
          Top = 285.000000000000000000
          Width = 350.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Link."SESR"]')
          ParentFont = False
        end
        object Memo168: TfrxMemoView
          Align = baLeft
          Left = 310.000000000000000000
          Top = 240.000000000000000000
          Width = 350.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Link."SESR_SUBREFRACTION"]')
          ParentFont = False
        end
        object Memo169: TfrxMemoView
          Align = baLeft
          Left = 310.000000000000000000
          Top = 255.000000000000000000
          Width = 350.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Link."SESR_RAIN"]')
          ParentFont = False
        end
        object Memo170: TfrxMemoView
          Align = baLeft
          Left = 310.000000000000000000
          Top = 225.000000000000000000
          Width = 350.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Link."MARGIN_DISTANCE"]')
          ParentFont = False
        end
        object Memo171: TfrxMemoView
          Align = baLeft
          Left = 310.000000000000000000
          Top = 210.000000000000000000
          Width = 350.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Link."MARGIN_HEIGHT"]')
          ParentFont = False
        end
        object Memo172: TfrxMemoView
          Align = baLeft
          Left = 485.000000000000000000
          Top = 105.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[antenna2."diameter"]')
          ParentFont = False
        end
        object Memo173: TfrxMemoView
          Align = baLeft
          Left = 485.000000000000000000
          Top = 120.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[antenna2."polarization"]')
          ParentFont = False
        end
        object Memo174: TfrxMemoView
          Align = baLeft
          Left = 310.000000000000000000
          Top = 150.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd1."rx_level_dBm"]')
          ParentFont = False
        end
        object Memo175: TfrxMemoView
          Align = baLeft
          Left = 485.000000000000000000
          Top = 150.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd2."rx_level_dBm"]')
          ParentFont = False
        end
        object Memo176: TfrxMemoView
          Align = baLeft
          Left = 310.000000000000000000
          Top = 165.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd1."kratnostBySpace_name"]')
          ParentFont = False
        end
        object Memo177: TfrxMemoView
          Align = baLeft
          Left = 485.000000000000000000
          Top = 165.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd2."kratnostBySpace_name"]')
          ParentFont = False
        end
        object Memo178: TfrxMemoView
          Align = baLeft
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1119#1056#187#1056#1109#1057#8240#1056#176#1056#1169#1056#1108#1056#176)
          ParentFont = False
        end
        object Memo179: TfrxMemoView
          Align = baLeft
          Left = 310.000000000000000000
          Top = 15.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[antenna1."height"]')
          ParentFont = False
        end
        object Memo180: TfrxMemoView
          Align = baLeft
          Left = 485.000000000000000000
          Top = 15.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[antenna2."height"]')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          Align = baLeft
          Top = 135.000000000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            'LINK  ID')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          Align = baLeft
          Left = 310.000000000000000000
          Top = 135.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Linkend1."LINKID"]')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          Align = baLeft
          Left = 485.000000000000000000
          Top = 135.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Linkend2."LINKID"]')
          ParentFont = False
        end
      end
      object MasterData3: TfrxMasterData
        Height = 52.000000000000000000
        Top = 105.826840000000000000
        Width = 718.110700000000000000
        Child = frxReport_MTS.Child_Header
        RowCount = 0
        object Memo7: TfrxMemoView
          Align = baWidth
          Width = 718.110700000000000000
          Height = 19.000000000000000000
          ShowHint = False
          AutoWidth = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8 = (
            #1056#1115#1057#8218#1057#8225#1056#181#1057#8218' '#1056#1111#1056#1109' '#1057#1026#1056#176#1056#1169#1056#1105#1056#1109#1057#1026#1056#181#1056#187#1056#181#1056#8470#1056#1029#1056#1109#1056#1112#1057#1107' '#1056#1105#1056#1029#1057#8218#1056#181#1057#1026#1056#1030#1056#176#1056#187#1057#1107)
          ParentFont = False
        end
        object Linkname: TfrxMemoView
          Align = baWidth
          Top = 28.000000000000000000
          Width = 718.110700000000000000
          Height = 16.000000000000000000
          ShowHint = False
          AutoWidth = True
          DataField = 'name'
          Memo.UTF8 = (
            '[Link."name"]')
        end
      end
      object Child_profile: TfrxChild
        Height = 58.400000000000000000
        Top = 933.543910000000000000
        Width = 718.110700000000000000
        object Picture_profile: TfrxPictureView
          Align = baWidth
          Top = 20.000000000000000000
          Width = 718.110700000000000000
          Height = 36.000000000000000000
          OnBeforePrint = 'Picture_profileOnBeforePrint'
          ShowHint = False
          DataField = 'profile_img'
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Stretched = False
          HightQuality = True
          Transparent = False
          TransparentColor = clWhite
        end
        object Memo80: TfrxMemoView
          Align = baLeft
          ShiftMode = smWhenOverlapped
          Width = 363.200000000000000000
          Height = 16.800000000000000000
          ShowHint = False
          AutoWidth = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            
              #1056#152#1056#183#1056#1109#1056#177#1057#1026#1056#176#1056#182#1056#181#1056#1029#1056#1105#1056#181' '#1056#1111#1057#1026#1056#1109#1057#8222#1056#1105#1056#187#1057#1039' '#1056#1111#1057#1026#1056#1105' '#1056#1029#1056#1109#1057#1026#1056#1112#1056#176#1056#187#1057#1034#1056#1029#1056#1109#1056 +
              #8470' '#1057#1026#1056#181#1057#8222#1057#1026#1056#176#1056#1108#1057#8224#1056#1105#1056#1105)
          ParentFont = False
        end
      end
      object Child_map: TfrxChild
        Height = 36.800000000000000000
        Top = 873.071430000000000000
        Width = 718.110700000000000000
        OnAfterCalcHeight = 'Child_mapOnAfterCalcHeight'
        OnBeforePrint = 'Child_mapOnBeforePrint'
        AllowSplit = True
        Child = frxReport_MTS.Child_profile
        object Picture_map: TfrxPictureView
          Description = 'Map'
          Align = baWidth
          Width = 718.110700000000000000
          Height = 28.000000000000000000
          OnBeforePrint = 'Picture_mapOnBeforePrint'
          ShowHint = False
          DataField = 'map_img'
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Stretched = False
          HightQuality = True
          Transparent = False
          TransparentColor = clWhite
        end
      end
      object PageFooter1: TfrxPageFooter
        Height = 24.000000000000000000
        Top = 1054.488870000000000000
        Width = 718.110700000000000000
        object Page: TfrxMemoView
          Align = baRight
          Left = 638.110700000000000000
          Width = 80.000000000000000000
          Height = 16.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8 = (
            '[Page#]')
          ParentFont = False
        end
        object Memo138: TfrxMemoView
          Align = baWidth
          Width = 638.110700000000000000
          Height = 16.000000000000000000
          ShowHint = False
          AutoWidth = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clSilver
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            
              #1056#1115#1057#8218#1057#8225#1056#181#1057#8218' '#1056#1030#1057#8249#1056#1111#1056#1109#1056#187#1056#1029#1056#181#1056#1029' '#1056#1030' '#1056#1111#1057#1026#1056#1109#1056#1110#1057#1026#1056#176#1056#1112#1056#1112#1056#181' ONEPLAN RPLS-D' +
              'B Link')
          ParentFont = False
        end
      end
      object PageHeader1: TfrxPageHeader
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        Height = 28.000000000000000000
        ParentFont = False
        Top = 18.897650000000000000
        Width = 718.110700000000000000
        object Linkcalc_method_name1: TfrxMemoView
          Align = baWidth
          Left = 141.600000000000000000
          Width = 576.510700000000000000
          Height = 16.000000000000000000
          ShowHint = False
          AutoWidth = True
          DataField = 'calc_method_name'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clSilver
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            '[Link."calc_method_name"]')
          ParentFont = False
        end
        object Memo137: TfrxMemoView
          Align = baLeft
          Width = 141.600000000000000000
          Height = 16.000000000000000000
          ShowHint = False
          AutoWidth = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clSilver
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            #1056#1114#1056#181#1057#8218#1056#1109#1056#1169#1056#1105#1056#1108#1056#176' '#1057#1026#1056#176#1057#1027#1057#8225#1056#181#1057#8218#1056#176': ')
          ParentFont = False
        end
      end
    end
  end
  object frxReport_rep: TfrxReport
    Version = '4.15.6'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.AllowEdit = False
    PreviewOptions.Buttons = [pbPrint, pbExport, pbZoom, pbTools, pbNavigator, pbExportQuick, pbNoFullScreen]
    PreviewOptions.ThumbnailVisible = True
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = #1055#1086' '#1091#1084#1086#1083#1095#1072#1085#1080#1102
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 41730.469225335600000000
    ReportOptions.LastChange = 41939.809216030100000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      ''
      '      '
      '  '
      ''
      ''
      'procedure frxReportOnStartReport(Sender: TfrxComponent);'
      'const'
      '  DEF_MAP_H = 800;'
      '  '
      '  DEF_H = 450;'
      '  DEF_OFFSET = 25;'
      ''
      '  DEF_TOP = 20;                                  '
      'begin'
      '//  Picture_map.Width:=200;                '
      '  Picture_map.Height     :=DEF_MAP_H;'
      '    '
      '  Picture_profile.Height :=DEF_H;                '
      '  Picture_profile1.Height:=DEF_H;                '
      '  Picture_profile2.Height:=DEF_H;                '
      '  Picture_profile3.Height:=DEF_H;                '
      '          '
      '  '
      '  Child_map.Height:=DEF_MAP_H;'
      '    '
      '  Child_profile.Height:=DEF_H + DEF_OFFSET;              '
      '  Child_profile1.Height:=DEF_H + DEF_OFFSET;              '
      '  Child_profile2.Height:=DEF_H + DEF_OFFSET;              '
      '  Child_profile3.Height:=DEF_H + DEF_OFFSET;'
      ''
      '  Child_profile1.Top:=DEF_TOP;              '
      '  Child_profile2.Top:=DEF_TOP;              '
      '  Child_profile3.Top:=DEF_TOP;'
      '  '
      '  '
      '                                       '
      'end;'
      ''
      ''
      'procedure Shape_ColorOnBeforePrint(Sender: TfrxComponent);'
      'begin'
      
        '  Shape_Color.color := <Profile."clutter_color">                ' +
        '                                                    '
      'end;'
      '     '
      '        '
      ''
      ''
      '//------------------------------------------------------------'
      'function DegreeToStr (aValue: double): String;'
      '//------------------------------------------------------------'
      'const'
      '//  STR_BLPOINT_FORMAT1 = '#39'%d'#176'%.2d'#39#39'%s%1.2f'#39#39#39#39#39';'
      '  STR_BLPOINT_FORMAT1 = '#39'%d'#176' %.2d'#39#39' %1.2f'#39#39#39#39#39';'
      'var    '
      ' iDeg,iMin:integer; '
      ' eSec: Double;   '
      '           '
      'begin'
      '  geo_DecodeDegree_(aValue, iDeg,iMin,eSec);'
      '                           '
      ' // angle.Sec:=TruncFloat(angle.Sec,2);'
      ' '
      '    Result:=Format(STR_BLPOINT_FORMAT1, [iDeg, iMin, eSec]);'
      ''
      '  if aValue<0 then'
      '    Result:='#39'-'#39'+Result;'
      'end;'
      '  '
      '           '
      ''
      '//------------------------------------------------------------'
      
        'procedure geo_DecodeDegree_(aValue: Double; var aDeg,aMin:intege' +
        'r; var aSec:'
      '    Double);'
      '//------------------------------------------------------------'
      'const'
      ' EXP_ = 0.000000001;'
      '     //97,033333333'
      'var'
      '  bIsNegative: Boolean;'
      '  e: Double;'
      '  eMinutes: Double;'
      '  eSec: Double;'
      '  eSeconds: Double;'
      'begin'
      '  bIsNegative := aValue<0;'
      ''
      '  aValue :=Abs(aValue);'
      ''
      '  aDeg:=Trunc(aValue);'
      ''
      ' // e:=Abs(aValue)*60;'
      ''
      '//  , Frac(X) = X - Int(X).'
      ''
      '  aMin:=Trunc(Frac(aValue)*60);'
      ''
      '//  Result.Sec:=Frac(Frac(Abs(aValue))*60)*60;'
      '  e:= (aValue - Trunc(aValue)) * 60;'
      '  aMin:=Trunc(e);'
      ''
      ' eMinutes := Trunc (e);'
      ' eSeconds := (e - Trunc (e)) * 60 ;'
      ''
      ''
      '  aSec:=(((aValue - aDeg) *60) - aMin) * 60;'
      ''
      '  if (aSec - 60) > EXP_ then'
      '  begin'
      '    Inc(aMin);'
      '    aSec := 0;'
      '  end;'
      ''
      ''
      '  if bIsNegative then'
      '    aDeg:=-aDeg;'
      '          '
      'end;'
      '        '
      ''
      '       '
      'begin'
      ''
      'end.    ')
    OnStartReport = 'frxReportOnStartReport'
    Left = 542
    Top = 140
    Datasets = <
      item
      end
      item
      end
      item
      end
      item
      end
      item
      end
      item
      end
      item
      end
      item
      end
      item
      end
      item
      end
      item
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 9.260416666666670000
      BottomMargin = 10.583333333333300000
      LargeDesignHeight = True
      OnBeforePrint = 'Page1OnBeforePrint'
      object Child_profile3: TfrxChild
        Height = 61.000000000000000000
        Top = 721.890230000000000000
        Width = 718.110700000000000000
        Child = frxReport_full.Child_calc_res
        object Memo82: TfrxMemoView
          Align = baLeft
          ShiftMode = smWhenOverlapped
          Width = 559.200000000000000000
          Height = 16.800000000000000000
          ShowHint = False
          AutoWidth = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            
              #1056#152#1056#183#1056#1109#1056#177#1057#1026#1056#176#1056#182#1056#181#1056#1029#1056#1105#1056#181' '#1056#1111#1057#1026#1056#1109#1057#8222#1056#1105#1056#187#1057#1039' '#1056#1030' '#1057#1107#1057#1027#1056#187#1056#1109#1056#1030#1056#1105#1057#1039#1057#8230' '#1057#1027#1057#1107#1056#177 +
              #1057#1026#1056#181#1057#8222#1057#1026#1056#176#1056#1108#1057#8224#1056#1105#1056#1105#1056#1109#1056#1029#1056#1029#1056#1109#1056#8470' '#1056#1029#1056#181#1057#1107#1057#1027#1057#8218#1056#1109#1056#8470#1057#8225#1056#1105#1056#1030#1056#1109#1057#1027#1057#8218#1056#1105)
          ParentFont = False
        end
        object Picture_profile3: TfrxPictureView
          Align = baWidth
          ShiftMode = smWhenOverlapped
          Top = 20.000000000000000000
          Width = 718.110700000000000000
          Height = 31.000000000000000000
          OnBeforePrint = 'Picture_profile3OnBeforePrint'
          ShowHint = False
          DataField = 'img3'
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Stretched = False
          HightQuality = True
          Transparent = False
          TransparentColor = clWhite
        end
      end
      object MasterData3: TfrxMasterData
        Height = 252.315090000000000000
        Top = 105.826840000000000000
        Width = 718.110700000000000000
        Child = frxReport_full.Child_map
        RowCount = 0
        object Subreport_center: TfrxSubreport
          Left = 251.779530000000000000
          Top = 83.149660000000000000
          Width = 216.000000000000000000
          Height = 55.000000000000000000
          ShowHint = False
          Page = frxReport_full.Page_Center
        end
        object Subreport_left: TfrxSubreport
          Top = 83.149660000000000000
          Width = 240.000000000000000000
          Height = 55.000000000000000000
          ShowHint = False
          Page = frxReport_full.Lleft
        end
        object Subreport_right: TfrxSubreport
          Align = baRight
          Left = 478.110700000000000000
          Top = 83.149660000000000000
          Width = 240.000000000000000000
          Height = 55.000000000000000000
          ShowHint = False
          Page = frxReport_full.Subreport_right_
        end
        object Subreport_summary: TfrxSubreport
          Align = baWidth
          Top = 170.078850000000000000
          Width = 718.110700000000000000
          Height = 48.000000000000000000
          ShowHint = False
          Page = frxReport_full.Page_Summary
        end
        object Memo7: TfrxMemoView
          Align = baWidth
          Width = 718.110700000000000000
          Height = 19.000000000000000000
          ShowHint = False
          AutoWidth = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8 = (
            #1056#1115#1057#8218#1057#8225#1056#181#1057#8218' '#1056#1111#1056#1109' '#1057#1026#1056#176#1056#1169#1056#1105#1056#1109#1057#1026#1056#181#1056#187#1056#181#1056#8470#1056#1029#1056#1109#1056#1112#1057#1107' '#1056#1105#1056#1029#1057#8218#1056#181#1057#1026#1056#1030#1056#176#1056#187#1057#1107)
          ParentFont = False
        end
        object Linkname: TfrxMemoView
          Align = baWidth
          Top = 28.000000000000000000
          Width = 718.110700000000000000
          Height = 16.000000000000000000
          ShowHint = False
          AutoWidth = True
          DataField = 'name'
          Memo.UTF8 = (
            '[Link."name"]')
        end
      end
      object Child_profile: TfrxChild
        Height = 58.400000000000000000
        Top = 442.205010000000000000
        Width = 718.110700000000000000
        Child = frxReport_full.Child_profile1
        object Picture_profile: TfrxPictureView
          Align = baWidth
          Top = 20.000000000000000000
          Width = 718.110700000000000000
          Height = 36.000000000000000000
          OnBeforePrint = 'Picture_profileOnBeforePrint'
          ShowHint = False
          DataField = 'profile_img'
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Stretched = False
          HightQuality = True
          Transparent = False
          TransparentColor = clWhite
        end
        object Memo80: TfrxMemoView
          Align = baLeft
          ShiftMode = smWhenOverlapped
          Width = 363.200000000000000000
          Height = 16.800000000000000000
          ShowHint = False
          AutoWidth = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            
              #1056#152#1056#183#1056#1109#1056#177#1057#1026#1056#176#1056#182#1056#181#1056#1029#1056#1105#1056#181' '#1056#1111#1057#1026#1056#1109#1057#8222#1056#1105#1056#187#1057#1039' '#1056#1111#1057#1026#1056#1105' '#1056#1029#1056#1109#1057#1026#1056#1112#1056#176#1056#187#1057#1034#1056#1029#1056#1109#1056 +
              #8470' '#1057#1026#1056#181#1057#8222#1057#1026#1056#176#1056#1108#1057#8224#1056#1105#1056#1105)
          ParentFont = False
        end
      end
      object Child_map: TfrxChild
        Height = 36.800000000000000000
        Top = 381.732530000000000000
        Width = 718.110700000000000000
        OnAfterCalcHeight = 'Child_mapOnAfterCalcHeight'
        OnBeforePrint = 'Child_mapOnBeforePrint'
        AllowSplit = True
        Child = frxReport_full.Child_profile
        object Picture_map: TfrxPictureView
          Description = 'Map'
          Align = baWidth
          Width = 718.110700000000000000
          Height = 28.000000000000000000
          OnBeforePrint = 'Picture_mapOnBeforePrint'
          ShowHint = False
          DataField = 'map_img'
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Stretched = False
          HightQuality = True
          Transparent = False
          TransparentColor = clWhite
        end
      end
      object Child_profile1: TfrxChild
        Height = 76.000000000000000000
        Top = 525.354670000000000000
        Width = 718.110700000000000000
        Child = frxReport_full.Child_profile2
        object Picture_profile1: TfrxPictureView
          Align = baWidth
          ShiftMode = smWhenOverlapped
          Top = 20.000000000000000000
          Width = 718.110700000000000000
          Height = 40.000000000000000000
          OnBeforePrint = 'Picture_profile1OnBeforePrint'
          ShowHint = False
          DataField = 'img1'
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Stretched = False
          HightQuality = True
          Transparent = False
          TransparentColor = clWhite
        end
        object Memo81: TfrxMemoView
          Align = baLeft
          ShiftMode = smWhenOverlapped
          Width = 359.200000000000000000
          Height = 16.800000000000000000
          ShowHint = False
          AutoWidth = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            
              #1056#152#1056#183#1056#1109#1056#177#1057#1026#1056#176#1056#182#1056#181#1056#1029#1056#1105#1056#181' '#1056#1111#1057#1026#1056#1109#1057#8222#1056#1105#1056#187#1057#1039' '#1056#1111#1057#1026#1056#1105' 20% '#1057#1026#1056#181#1057#8222#1057#1026#1056#176#1056#1108#1057#8224#1056 +
              #1105#1056#1105)
          ParentFont = False
        end
      end
      object Child_profile2: TfrxChild
        Height = 76.800000000000000000
        Top = 623.622450000000000000
        Width = 718.110700000000000000
        Child = frxReport_full.Child_profile3
        object Picture_profile2: TfrxPictureView
          Align = baWidth
          ShiftMode = smWhenOverlapped
          Top = 20.000000000000000000
          Width = 718.110700000000000000
          Height = 43.000000000000000000
          OnBeforePrint = 'Picture_profile2OnBeforePrint'
          ShowHint = False
          DataField = 'img2'
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Stretched = False
          HightQuality = True
          Transparent = False
          TransparentColor = clWhite
        end
        object Memo83: TfrxMemoView
          Align = baLeft
          ShiftMode = smWhenOverlapped
          Width = 567.200000000000000000
          Height = 16.800000000000000000
          ShowHint = False
          AutoWidth = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            
              #1056#152#1056#183#1056#1109#1056#177#1057#1026#1056#176#1056#182#1056#181#1056#1029#1056#1105#1056#181' '#1056#1111#1057#1026#1056#1109#1057#8222#1056#1105#1056#187#1057#1039' '#1056#1030' '#1057#1107#1057#1027#1056#187#1056#1109#1056#1030#1056#1105#1057#1039#1057#8230' '#1057#1027#1057#1107#1056#177 +
              #1057#1026#1056#181#1057#8222#1057#1026#1056#176#1056#1108#1057#8224#1056#1105#1056#1105)
          ParentFont = False
        end
      end
      object Child_Profile_items: TfrxChild
        Height = 40.000000000000000000
        Top = 869.291900000000000000
        Width = 718.110700000000000000
        StartNewPage = True
        object Subreport_Profile: TfrxSubreport
          Align = baWidth
          Width = 718.110700000000000000
          Height = 24.000000000000000000
          ShowHint = False
          Page = frxReport_full.Page6
        end
      end
      object Child_calc_res: TfrxChild
        Height = 40.000000000000000000
        Top = 805.039890000000000000
        Width = 718.110700000000000000
        AllowSplit = True
        Child = frxReport_full.Child_Profile_items
        StartNewPage = True
        object Subreport_calc_res: TfrxSubreport
          Align = baLeft
          Width = 718.110700000000000000
          Height = 20.000000000000000000
          ShowHint = False
          Page = frxReport_full.Page8
        end
      end
      object PageFooter1: TfrxPageFooter
        Height = 24.000000000000000000
        Top = 971.339210000000000000
        Width = 718.110700000000000000
        object Page: TfrxMemoView
          Align = baRight
          Left = 638.110700000000000000
          Width = 80.000000000000000000
          Height = 16.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8 = (
            '[Page#]')
          ParentFont = False
        end
        object Memo138: TfrxMemoView
          Align = baWidth
          Width = 638.110700000000000000
          Height = 16.000000000000000000
          ShowHint = False
          AutoWidth = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clSilver
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            
              #1056#1115#1057#8218#1057#8225#1056#181#1057#8218' '#1056#1030#1057#8249#1056#1111#1056#1109#1056#187#1056#1029#1056#181#1056#1029' '#1056#1030' '#1056#1111#1057#1026#1056#1109#1056#1110#1057#1026#1056#176#1056#1112#1056#1112#1056#181' ONEPLAN RPLS-D' +
              'B Link')
          ParentFont = False
        end
      end
      object PageHeader1: TfrxPageHeader
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        Height = 28.000000000000000000
        ParentFont = False
        Top = 18.897650000000000000
        Width = 718.110700000000000000
        object Linkcalc_method_name1: TfrxMemoView
          Align = baWidth
          Left = 141.600000000000000000
          Width = 576.510700000000000000
          Height = 16.000000000000000000
          ShowHint = False
          AutoWidth = True
          DataField = 'calc_method_name'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clSilver
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            '[Link."calc_method_name"]')
          ParentFont = False
        end
        object Memo137: TfrxMemoView
          Align = baLeft
          Width = 141.600000000000000000
          Height = 16.000000000000000000
          ShowHint = False
          AutoWidth = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clSilver
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            #1056#1114#1056#181#1057#8218#1056#1109#1056#1169#1056#1105#1056#1108#1056#176' '#1057#1026#1056#176#1057#1027#1057#8225#1056#181#1057#8218#1056#176': ')
          ParentFont = False
        end
      end
    end
    object Lleft: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object MasterData2: TfrxMasterData
        Height = 180.779530000000000000
        Top = 18.897650000000000000
        Width = 718.110700000000000000
        RowCount = 0
        object Memo43: TfrxMemoView
          Align = baLeft
          Width = 110.000000000000000000
          Height = 32.118120000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1119#1056#187#1056#1109#1057#8240#1056#176#1056#1169#1056#1108#1056#176)
          ParentFont = False
        end
        object Memo_left_1: TfrxMemoView
          Align = baLeft
          Left = 110.000000000000000000
          Width = 117.165354330000000000
          Height = 32.118120000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Site1."name"]')
          ParentFont = False
          UseDefaultCharset = True
        end
        object Memo68: TfrxMemoView
          Align = baLeft
          Top = 62.118120000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1113#1056#1109#1056#1109#1057#1026#1056#1169#1056#1105#1056#1029#1056#176#1057#8218#1057#8249' WGS')
          ParentFont = False
        end
        object Memo70: TfrxMemoView
          Align = baLeft
          Top = 92.118120000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#8217#1057#8249#1057#1027#1056#1109#1057#8218#1056#176' '#1056#1111#1056#1109#1056#1169#1056#1030#1056#181#1057#1027#1056#176', m')
          ParentFont = False
        end
        object Memo72: TfrxMemoView
          Align = baLeft
          Top = 107.118120000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1106#1056#183#1056#1105#1056#1112#1057#1107#1057#8218)
          ParentFont = False
        end
        object Memo74: TfrxMemoView
          Align = baLeft
          Top = 122.118120000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1032#1056#1110#1056#1109#1056#187' '#1056#1112#1056#181#1057#1027#1057#8218#1056#176)
          ParentFont = False
        end
        object Memo100: TfrxMemoView
          Align = baLeft
          Top = 137.118120000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#8221#1056#1105#1056#176#1056#1112#1056#181#1057#8218#1057#1026' '#1056#176#1056#1029#1057#8218#1056#181#1056#1029#1056#1029#1057#8249)
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Align = baLeft
          Left = 110.000000000000000000
          Top = 62.118120000000000000
          Width = 117.165354330000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[DegreeToStr (<Site1."lat_WGS">)]')
          ParentFont = False
          UseDefaultCharset = True
        end
        object Memo1: TfrxMemoView
          Align = baLeft
          Left = 110.000000000000000000
          Top = 92.118120000000000000
          Width = 117.165354330000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[antenna1."height"]')
          ParentFont = False
          UseDefaultCharset = True
        end
        object Memo95: TfrxMemoView
          Align = baLeft
          Left = 110.000000000000000000
          Top = 107.118120000000000000
          Width = 117.165354330000000000
          Height = 15.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."azimuth1"]')
          ParentFont = False
          UseDefaultCharset = True
        end
        object Memo97: TfrxMemoView
          Align = baLeft
          Left = 110.000000000000000000
          Top = 137.118120000000000000
          Width = 117.165354330000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[antenna1."diameter"]')
          ParentFont = False
          UseDefaultCharset = True
        end
        object Memo98: TfrxMemoView
          Align = baLeft
          Left = 110.000000000000000000
          Top = 77.118120000000000000
          Width = 117.165354330000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[DegreeToStr (<Site1."lon_WGS">)]')
          ParentFont = False
          UseDefaultCharset = True
        end
        object Memo96: TfrxMemoView
          Align = baLeft
          Left = 110.000000000000000000
          Top = 122.118120000000000000
          Width = 117.165354330000000000
          Height = 15.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."ELEVATION_angle_1"]')
          ParentFont = False
          UseDefaultCharset = True
        end
        object Memo121: TfrxMemoView
          Align = baLeft
          Top = 77.118120000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Align = baLeft
          Top = 32.118120000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1113#1056#1109#1056#1109#1057#1026#1056#1169#1056#1105#1056#1029#1056#176#1057#8218#1057#8249' Pulkovo')
          ParentFont = False
        end
        object Memo30: TfrxMemoView
          Align = baLeft
          Left = 110.000000000000000000
          Top = 32.118120000000000000
          Width = 117.165354330000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[DegreeToStr (<Site1."lat">)]')
          ParentFont = False
          UseDefaultCharset = True
        end
        object Memo66: TfrxMemoView
          Align = baLeft
          Left = 110.000000000000000000
          Top = 47.118120000000000000
          Width = 117.165354330000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[DegreeToStr (<Site1."lon">)]')
          ParentFont = False
          UseDefaultCharset = True
        end
        object Memo99: TfrxMemoView
          Align = baLeft
          Top = 47.118120000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          ParentFont = False
        end
        object Memo136: TfrxMemoView
          Align = baLeft
          Top = 152.118120000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1032#1057#1027#1056#1105#1056#187#1056#181#1056#1029#1056#1105#1056#181' '#1056#176#1056#1029#1057#8218#1056#181#1056#1029#1056#1029#1057#8249)
          ParentFont = False
        end
        object Memo139: TfrxMemoView
          Align = baLeft
          Left = 110.000000000000000000
          Top = 152.118120000000000000
          Width = 117.165354330000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[antenna1."gain"]')
          ParentFont = False
          UseDefaultCharset = True
        end
      end
    end
    object Page_Center: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object MasterData1: TfrxMasterData
        Height = 127.000000000000000000
        Top = 18.897650000000000000
        Width = 718.110700000000000000
        RowCount = 0
        object Memo45: TfrxMemoView
          Align = baLeft
          Top = 15.000000000000000000
          Width = 160.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#8221#1056#187#1056#1105#1056#1029#1056#176' '#1056#1105#1056#1029#1057#8218#1056#181#1057#1026#1056#1030#1056#176#1056#187#1056#176', km')
          ParentFont = False
        end
        object Memo47: TfrxMemoView
          Align = baLeft
          Top = 30.000000000000000000
          Width = 160.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1032#1057#1026#1056#1109#1056#1030#1056#181#1056#1029#1057#1034' '#1057#1027#1056#1105#1056#1110#1056#1029#1056#176#1056#187#1056#176', dBm')
          ParentFont = False
        end
        object Memo49: TfrxMemoView
          Align = baLeft
          Top = 45.000000000000000000
          Width = 160.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            'P'#1056#176#1056#177#1056#1109#1057#8225#1056#176#1057#1039' '#1057#8225#1056#176#1057#1027#1057#8218#1056#1109#1057#8218#1056#176', GHz')
          ParentFont = False
        end
        object Memo51: TfrxMemoView
          Align = baLeft
          Top = 60.000000000000000000
          Width = 160.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1114#1056#1109#1057#8240#1056#1029#1056#1109#1057#1027#1057#8218#1057#1034' '#1056#1111#1056#181#1057#1026#1056#181#1056#1169#1056#176#1057#8218#1057#8225#1056#1105#1056#1108#1056#176', dBm')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          Align = baLeft
          Top = 75.000000000000000000
          Width = 160.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1038#1056#1108#1056#1109#1057#1026#1056#1109#1057#1027#1057#8218#1057#1034' '#1056#1111#1056#181#1057#1026#1056#181#1056#1169#1056#176#1057#8225#1056#1105', Mbit/s')
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          Align = baLeft
          Top = 90.000000000000000000
          Width = 160.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1118#1056#1105#1056#1111' '#1056#1109#1056#177#1056#1109#1057#1026#1057#1107#1056#1169#1056#1109#1056#1030#1056#176#1056#1029#1056#1105#1057#1039)
          ParentFont = False
        end
        object Memo123: TfrxMemoView
          Align = baLeft
          Left = 160.000000000000000000
          Top = 30.000000000000000000
          Width = 68.031496062992100000
          Height = 15.000000000000000000
          ShowHint = False
          DataField = 'rx_level_dBm'
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd1."rx_level_dBm"]')
          ParentFont = False
        end
        object Memo124: TfrxMemoView
          Align = baLeft
          Left = 160.000000000000000000
          Top = 75.000000000000000000
          Width = 68.031496062992100000
          Height = 15.000000000000000000
          ShowHint = False
          DataField = 'bitrate_Mbps'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd1."bitrate_Mbps"]')
          ParentFont = False
        end
        object Memo125: TfrxMemoView
          Align = baLeft
          Left = 160.000000000000000000
          Top = 90.000000000000000000
          Width = 68.031496062992100000
          Height = 15.000000000000000000
          ShowHint = False
          DataField = 'LinkEndType_name'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd1."LinkEndType_name"]')
          ParentFont = False
        end
        object Memo127: TfrxMemoView
          Align = baLeft
          Left = 160.000000000000000000
          Top = 45.000000000000000000
          Width = 68.031496062992100000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[FloatToStr( <LinkEnd1."tx_freq_MHz"> / 1000 )]')
          ParentFont = False
        end
        object Memo128: TfrxMemoView
          Align = baLeft
          Left = 160.000000000000000000
          Top = 15.000000000000000000
          Width = 68.031496062992100000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."length_km"]')
          ParentFont = False
        end
        object Memo126: TfrxMemoView
          Align = baLeft
          Left = 160.000000000000000000
          Top = 60.000000000000000000
          Width = 68.031496062992100000
          Height = 15.000000000000000000
          ShowHint = False
          DataField = 'power_dBm'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd1."power_dBm"]')
          ParentFont = False
        end
        object Memo42: TfrxMemoView
          Align = baLeft
          Width = 160.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1119#1057#1026#1056#1105#1056#183#1056#1029#1056#176#1056#1108' '#1056#1111#1057#1026#1056#1105#1056#1110#1056#1109#1056#1169#1056#1029#1056#1109#1057#1027#1057#8218#1056#1105)
          ParentFont = False
        end
        object Memo122: TfrxMemoView
          Align = baLeft
          Left = 160.000000000000000000
          Width = 68.031496062992100000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."status_str"]')
          ParentFont = False
        end
        object Memo134: TfrxMemoView
          Align = baLeft
          Top = 105.000000000000000000
          Width = 160.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#8212#1056#176#1056#1111#1056#176#1057#1027' '#1056#1029#1056#176' '#1056#183#1056#176#1056#1112#1056#1105#1057#1026#1056#176#1056#1029#1056#1105#1057#1039', dB')
          ParentFont = False
        end
        object Memo135: TfrxMemoView
          Align = baLeft
          Left = 160.000000000000000000
          Top = 105.000000000000000000
          Width = 68.031496062992100000
          Height = 15.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."fade_margin_dB"]')
          ParentFont = False
        end
      end
    end
    object Page_Summary: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object MasterData4: TfrxMasterData
        Height = 438.677180000000000000
        Top = 18.897650000000000000
        Width = 718.110700000000000000
        RowCount = 0
        object Memo_1: TfrxMemoView
          Align = baLeft
          Top = 15.000000000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#8217#1057#8249#1057#1027#1056#1109#1057#8218#1056#176', m')
          ParentFont = False
        end
        object Memo_2: TfrxMemoView
          Align = baLeft
          Top = 30.000000000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1106#1056#183#1056#1105#1056#1112#1057#1107#1057#8218' ')
          ParentFont = False
        end
        object Memo_3: TfrxMemoView
          Align = baLeft
          Top = 45.000000000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1118#1056#1105#1056#1111' '#1056#1109#1056#177#1056#1109#1057#1026#1057#1107#1056#1169#1056#1109#1056#1030#1056#176#1056#1029#1056#1105#1057#1039' ')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Align = baLeft
          Top = 60.000000000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#167#1056#176#1057#1027#1057#8218#1056#1109#1057#8218#1056#176' '#1056#1111#1056#181#1057#1026#1056#181#1056#1169#1056#176#1057#8225#1056#1105', MHz')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Align = baLeft
          Top = 75.000000000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1038#1056#1108#1056#1109#1057#1026#1056#1109#1057#1027#1057#8218#1057#1034' '#1056#1111#1056#181#1057#1026#1056#181#1056#1169#1056#176#1057#8225#1056#1105', Mbit/s')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Align = baLeft
          Top = 90.000000000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1114#1056#1109#1057#8240#1056#1029#1056#1109#1057#1027#1057#8218#1057#1034', dBm')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          Align = baLeft
          Top = 105.000000000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#8221#1056#1105#1056#176#1056#1112#1056#181#1057#8218#1057#1026' '#1056#176#1056#1029#1057#8218#1056#181#1056#1029#1056#1029#1057#8249', m')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          Align = baLeft
          Top = 135.000000000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            
              #1056#1038#1057#1026#1056#181#1056#1169#1056#1029#1057#1039#1057#1039' '#1056#1112#1056#1109#1057#8240#1056#1029#1056#1109#1057#1027#1057#8218#1057#1034' '#1056#1029#1056#176' '#1056#1030#1057#8230#1056#1109#1056#1169#1056#181' '#1056#1111#1057#1026#1056#1105#1056#181#1056#1112#1056#1029#1056#1105#1056#1108 +
              #1056#176', dBm')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          Align = baLeft
          Top = 120.000000000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1119#1056#1109#1056#187#1057#1039#1057#1026#1056#1105#1056#183#1056#176#1057#8224#1056#1105#1057#1039' ')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          Align = baLeft
          Top = 150.000000000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#160#1056#181#1056#183#1056#181#1057#1026#1056#1030#1056#1105#1057#1026#1056#1109#1056#1030#1056#176#1056#1029#1056#1105#1056#181' ')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          Align = baLeft
          Top = 165.000000000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1119#1056#1109#1057#1026#1056#1109#1056#1110' '#1056#1111#1057#1026#1056#1105' BER=10-3 ')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          Align = baLeft
          Top = 195.000000000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1114#1056#1105#1056#1029#1056#1105#1056#1112#1056#176#1056#187#1057#1034#1056#1029#1057#8249#1056#8470' '#1056#1109#1057#8218#1056#1029#1056#1109#1057#1027#1056#1105#1057#8218#1056#181#1056#187#1057#1034#1056#1029#1057#8249#1056#8470' '#1056#1111#1057#1026#1056#1109#1057#1027#1056#1030#1056#181#1057#8218)
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          Align = baLeft
          Top = 180.000000000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1119#1056#1109#1057#1026#1056#1109#1056#1110' '#1056#1111#1057#1026#1056#1105' BER=10-6 ')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          Align = baLeft
          Top = 210.000000000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1119#1056#1109#1056#187#1056#1109#1056#182#1056#181#1056#1029#1056#1105#1056#181' '#1057#8218#1056#1109#1057#8225#1056#1108#1056#1105' '#1056#1111#1057#1026#1056#1109#1057#1027#1056#1030#1056#181#1057#8218#1056#176', km ')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          Align = baLeft
          Top = 225.000000000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            'SESR '#1057#1027#1057#1107#1056#177#1057#1026#1056#181#1057#8222#1056#1108#1057#8224#1056#1105#1056#1109#1056#1029#1056#1029#1056#176#1057#1039' '#1057#1027#1056#1109#1057#1027#1057#8218#1056#176#1056#1030#1056#187#1057#1039#1057#1035#1057#8240#1056#176#1057#1039', % ')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          Align = baLeft
          Top = 240.000000000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            'SESR '#1056#1169#1056#1109#1056#182#1056#1169#1056#181#1056#1030#1056#176#1057#1039' '#1057#1027#1056#1109#1057#1027#1057#8218#1056#176#1056#1030#1056#187#1057#1039#1057#1035#1057#8240#1056#176#1057#1039', % ')
          ParentFont = False
        end
        object Memo25: TfrxMemoView
          Align = baLeft
          Top = 255.000000000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            
              'SESR '#1056#1105#1056#1029#1057#8218#1056#181#1057#1026#1057#8222#1056#181#1057#1026#1056#181#1056#1029#1057#8224#1056#1105#1056#1109#1056#1029#1056#1029#1056#176#1057#1039' '#1057#1027#1056#1109#1057#1027#1057#8218#1056#176#1056#1030#1056#187#1057#1039#1057#1035#1057#8240#1056#176#1057#1039 +
              ', % ')
          ParentFont = False
        end
        object Memo26: TfrxMemoView
          Align = baLeft
          Top = 270.000000000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            
              #1056#152#1057#8218#1056#1109#1056#1110#1056#1109#1056#1030#1057#8249#1056#8470' '#1056#1111#1056#1109#1056#1108#1056#176#1056#183#1056#176#1057#8218#1056#181#1056#187#1057#1034' '#1056#1108#1056#176#1057#8225#1056#181#1057#1027#1057#8218#1056#1030#1056#176' '#1056#1111#1056#1109' '#1056#1109#1057#8364 +
              #1056#1105#1056#177#1056#1108#1056#176#1056#1112' SESR, % ')
          ParentFont = False
        end
        object Memo27: TfrxMemoView
          Align = baLeft
          Top = 285.000000000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            
              #1056#1116#1056#1109#1057#1026#1056#1112#1056#1105#1057#1026#1057#1107#1056#181#1056#1112#1056#1109#1056#181' '#1056#183#1056#1029#1056#176#1057#8225#1056#181#1056#1029#1056#1105#1056#181' SESR '#1056#1029#1056#176' '#1056#1105#1056#1029#1057#8218#1056#181#1057#1026#1056#1030#1056#176 +
              #1056#187#1056#181', % ')
          ParentFont = False
        end
        object Memo28: TfrxMemoView
          Align = baLeft
          Top = 300.000000000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            
              #1056#152#1057#8218#1056#1109#1056#1110#1056#1109#1056#1030#1057#8249#1056#8470' '#1056#1111#1056#1109#1056#1108#1056#176#1056#183#1056#176#1057#8218#1056#181#1056#187#1057#1034' '#1056#1029#1056#181#1056#1110#1056#1109#1057#8218#1056#1109#1056#1030#1056#1029#1056#1109#1057#1027#1057#8218#1056#1105' '#1056 +
              #1113#1056#1029#1056#1110', %')
          ParentFont = False
        end
        object Memo29: TfrxMemoView
          Align = baLeft
          Top = 315.000000000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            
              #1056#1116#1056#1109#1057#1026#1056#1112#1056#1105#1057#1026#1057#1107#1056#181#1056#1112#1056#1109#1056#181' '#1056#183#1056#1029#1056#176#1057#8225#1056#181#1056#1029#1056#1105#1056#181' '#1056#1113#1056#1029#1056#1110' '#1056#1029#1056#176' '#1056#1105#1056#1029#1057#8218#1056#181#1057#1026#1056#1030 +
              #1056#176#1056#187#1056#181', % ')
          ParentFont = False
        end
        object Memo31: TfrxMemoView
          Align = baLeft
          Top = 375.000000000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            'ESR '#1057#8218#1057#1026#1056#181#1056#177', % ')
          ParentFont = False
        end
        object Memo32: TfrxMemoView
          Align = baLeft
          Top = 330.000000000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            'BBER '#1056#1029#1056#1109#1057#1026#1056#1112', %')
          ParentFont = False
        end
        object Memo33: TfrxMemoView
          Align = baLeft
          Top = 360.000000000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            'BBER '#1057#8218#1057#1026#1056#181#1056#177', % ')
          ParentFont = False
        end
        object Memo34: TfrxMemoView
          Align = baLeft
          Top = 345.000000000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            'ESR '#1056#1029#1056#1109#1057#1026#1056#1112', % ')
          ParentFont = False
        end
        object Memo35: TfrxMemoView
          Align = baLeft
          Top = 390.000000000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1119#1057#1026#1056#1105#1056#1110#1056#1109#1056#1169#1056#1029#1056#1109#1057#1027#1057#8218#1057#1034' ')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Align = baLeft
          Left = 310.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Site1."name"]')
          ParentFont = False
        end
        object Memo36: TfrxMemoView
          Align = baLeft
          Left = 485.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Site2."name"]')
          ParentFont = False
        end
        object Memo37: TfrxMemoView
          Align = baLeft
          Left = 310.000000000000000000
          Top = 30.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."azimuth1"]')
          ParentFont = False
        end
        object Memo38: TfrxMemoView
          Align = baLeft
          Left = 485.000000000000000000
          Top = 30.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."azimuth2"]')
          ParentFont = False
        end
        object LinkEnd2threshold_BER_3: TfrxMemoView
          Align = baLeft
          Left = 310.000000000000000000
          Top = 165.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd1."threshold_BER_3"]')
          ParentFont = False
        end
        object LinkEnd2threshold_BER_6: TfrxMemoView
          Align = baLeft
          Left = 310.000000000000000000
          Top = 180.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd1."threshold_BER_6"]')
          ParentFont = False
        end
        object Memo41: TfrxMemoView
          Align = baLeft
          Left = 310.000000000000000000
          Top = 105.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[antenna1."diameter"]')
          ParentFont = False
        end
        object Memo44: TfrxMemoView
          Align = baLeft
          Left = 310.000000000000000000
          Top = 120.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[antenna1."polarization"]')
          ParentFont = False
        end
        object Memo50: TfrxMemoView
          Align = baLeft
          Left = 310.000000000000000000
          Top = 75.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd1."bitrate_Mbps"]')
          ParentFont = False
        end
        object Memo52: TfrxMemoView
          Align = baLeft
          Left = 310.000000000000000000
          Top = 90.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd1."power_dBm"]')
          ParentFont = False
        end
        object Memo55: TfrxMemoView
          Align = baLeft
          Left = 310.000000000000000000
          Top = 45.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd1."LinkEndType_name"]')
          ParentFont = False
        end
        object Memo56: TfrxMemoView
          Align = baLeft
          Left = 310.000000000000000000
          Top = 60.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd1."tx_freq_MHz"]')
          ParentFont = False
        end
        object Memo57: TfrxMemoView
          Align = baLeft
          Left = 485.000000000000000000
          Top = 45.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd2."LinkEndType_name"]')
          ParentFont = False
        end
        object Memo39: TfrxMemoView
          Align = baLeft
          Left = 485.000000000000000000
          Top = 60.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd2."tx_freq_MHz"]')
          ParentFont = False
        end
        object Memo40: TfrxMemoView
          Align = baLeft
          Left = 485.000000000000000000
          Top = 75.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd2."bitrate_Mbps"]')
          ParentFont = False
        end
        object Memo46: TfrxMemoView
          Align = baLeft
          Left = 485.000000000000000000
          Top = 90.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd2."power_dBm"]')
          ParentFont = False
        end
        object Memo48: TfrxMemoView
          Align = baLeft
          Left = 485.000000000000000000
          Top = 165.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd2."threshold_BER_3"]')
          ParentFont = False
        end
        object Memo53: TfrxMemoView
          Align = baLeft
          Left = 485.000000000000000000
          Top = 180.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd2."threshold_BER_6"]')
          ParentFont = False
        end
        object Memo54: TfrxMemoView
          Align = baLeft
          Left = 310.000000000000000000
          Top = 375.000000000000000000
          Width = 350.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Link."ESR_REQUIRED"]')
          ParentFont = False
        end
        object Memo58: TfrxMemoView
          Align = baLeft
          Left = 310.000000000000000000
          Top = 390.000000000000000000
          Width = 350.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Link."status_str"]')
          ParentFont = False
        end
        object Memo59: TfrxMemoView
          Align = baLeft
          Left = 310.000000000000000000
          Top = 345.000000000000000000
          Width = 350.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Link."ESR_NORM"]')
          ParentFont = False
        end
        object Memo60: TfrxMemoView
          Align = baLeft
          Left = 310.000000000000000000
          Top = 360.000000000000000000
          Width = 350.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Link."BBER_REQUIRED"]')
          ParentFont = False
        end
        object Memo61: TfrxMemoView
          Align = baLeft
          Left = 310.000000000000000000
          Top = 315.000000000000000000
          Width = 350.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Link."KNG_NORM"]')
          ParentFont = False
        end
        object Memo62: TfrxMemoView
          Align = baLeft
          Left = 310.000000000000000000
          Top = 330.000000000000000000
          Width = 350.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Link."BBER_NORM"]')
          ParentFont = False
        end
        object Memo63: TfrxMemoView
          Align = baLeft
          Left = 310.000000000000000000
          Top = 285.000000000000000000
          Width = 350.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Link."SESR_NORM"]')
          ParentFont = False
        end
        object Memo64: TfrxMemoView
          Align = baLeft
          Left = 310.000000000000000000
          Top = 300.000000000000000000
          Width = 350.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Link."KNG"]')
          ParentFont = False
        end
        object Memo67: TfrxMemoView
          Align = baLeft
          Left = 310.000000000000000000
          Top = 255.000000000000000000
          Width = 350.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Link."SESR_INTERFERENCE"]')
          ParentFont = False
        end
        object Memo69: TfrxMemoView
          Align = baLeft
          Left = 310.000000000000000000
          Top = 270.000000000000000000
          Width = 350.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Link."SESR"]')
          ParentFont = False
        end
        object Memo71: TfrxMemoView
          Align = baLeft
          Left = 310.000000000000000000
          Top = 225.000000000000000000
          Width = 350.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Link."SESR_SUBREFRACTION"]')
          ParentFont = False
        end
        object Memo73: TfrxMemoView
          Align = baLeft
          Left = 310.000000000000000000
          Top = 240.000000000000000000
          Width = 350.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Link."SESR_RAIN"]')
          ParentFont = False
        end
        object Memo75: TfrxMemoView
          Align = baLeft
          Left = 310.000000000000000000
          Top = 210.000000000000000000
          Width = 350.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Link."MARGIN_DISTANCE"]')
          ParentFont = False
        end
        object Memo76: TfrxMemoView
          Align = baLeft
          Left = 310.000000000000000000
          Top = 195.000000000000000000
          Width = 350.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Link."MARGIN_HEIGHT"]')
          ParentFont = False
        end
        object Memo88: TfrxMemoView
          Align = baLeft
          Left = 485.000000000000000000
          Top = 105.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[antenna2."diameter"]')
          ParentFont = False
        end
        object Memo84: TfrxMemoView
          Align = baLeft
          Left = 485.000000000000000000
          Top = 120.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[antenna2."polarization"]')
          ParentFont = False
        end
        object Memo85: TfrxMemoView
          Align = baLeft
          Left = 310.000000000000000000
          Top = 135.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd1."rx_level_dBm"]')
          ParentFont = False
        end
        object Memo86: TfrxMemoView
          Align = baLeft
          Left = 485.000000000000000000
          Top = 135.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd2."rx_level_dBm"]')
          ParentFont = False
        end
        object Memo87: TfrxMemoView
          Align = baLeft
          Left = 310.000000000000000000
          Top = 150.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd1."kratnostBySpace_name"]')
          ParentFont = False
        end
        object Memo94: TfrxMemoView
          Align = baLeft
          Left = 485.000000000000000000
          Top = 150.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd2."kratnostBySpace_name"]')
          ParentFont = False
        end
        object Memo131: TfrxMemoView
          Align = baLeft
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          ParentFont = False
        end
        object Memo65: TfrxMemoView
          Align = baLeft
          Left = 310.000000000000000000
          Top = 15.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[antenna1."height"]')
          ParentFont = False
        end
        object Memo102: TfrxMemoView
          Align = baLeft
          Left = 485.000000000000000000
          Top = 15.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[antenna2."height"]')
          ParentFont = False
        end
      end
    end
    object Page6: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object Header1: TfrxHeader
        Height = 35.000000000000000000
        Top = 16.000000000000000000
        Width = 718.110700000000000000
        object Memo93: TfrxMemoView
          Align = baLeft
          Width = 60.000000000000000000
          Height = 35.000000000000000000
          Visible = False
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            'Index')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Align = baLeft
          Left = 60.000000000000000000
          Width = 100.000000000000000000
          Height = 35.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#160#1056#176#1057#1027#1057#1027#1057#8218#1056#1109#1057#1039#1056#1029#1056#1105#1056#181', '#1056#1108#1056#1112)
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Align = baLeft
          Left = 260.000000000000000000
          Width = 100.000000000000000000
          Height = 35.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1113#1057#1026#1056#1105#1056#1030#1056#1105#1056#183#1056#1029#1056#176' '#1056#183#1056#181#1056#1112#1056#187#1056#1105)
          ParentFont = False
        end
        object Memo120: TfrxMemoView
          Align = baLeft
          Left = 360.000000000000000000
          Width = 100.000000000000000000
          Height = 35.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#8217#1057#8249#1057#1027#1056#1109#1057#8218#1056#176' '#1056#1114#1056#1119)
          ParentFont = False
        end
        object Memo132: TfrxMemoView
          Align = baLeft
          Left = 160.000000000000000000
          Width = 100.000000000000000000
          Height = 35.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#8217#1057#8249#1057#1027#1056#1109#1057#8218#1056#176' '#1057#1026#1056#181#1056#187#1057#1034#1056#181#1057#8222#1056#176)
          ParentFont = False
        end
        object Memo101: TfrxMemoView
          Align = baLeft
          Left = 460.000000000000000000
          Width = 196.000000000000000000
          Height = 35.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1118#1056#1105#1056#1111' '#1056#1114#1056#1119)
          ParentFont = False
        end
      end
      object MasterData5: TfrxMasterData
        Height = 16.000000000000000000
        Top = 76.000000000000000000
        Width = 718.110700000000000000
        RowCount = 0
        object Memo89: TfrxMemoView
          Align = baLeft
          Left = 60.000000000000000000
          Width = 100.000000000000000000
          Height = 16.000000000000000000
          ShowHint = False
          DataField = 'distance_km'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Profile."distance_km"]')
          ParentFont = False
        end
        object Memo90: TfrxMemoView
          Align = baLeft
          Width = 60.000000000000000000
          Height = 16.000000000000000000
          Visible = False
          ShowHint = False
          DataField = 'index'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Profile."index"]')
          ParentFont = False
        end
        object Memo91: TfrxMemoView
          Align = baLeft
          Left = 260.000000000000000000
          Width = 100.000000000000000000
          Height = 16.000000000000000000
          ShowHint = False
          DataField = 'earth_h'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Profile."earth_h"]')
          ParentFont = False
        end
        object Memo92: TfrxMemoView
          Align = baLeft
          Left = 160.000000000000000000
          Width = 100.000000000000000000
          Height = 16.000000000000000000
          OnBeforePrint = 'Memo92OnBeforePrint'
          ShowHint = False
          DataField = 'ground_h'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Profile."ground_h"]')
          ParentFont = False
        end
        object Memo133: TfrxMemoView
          Align = baLeft
          Left = 360.000000000000000000
          Width = 100.000000000000000000
          Height = 16.000000000000000000
          ShowHint = False
          DataField = 'clutter_h'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Profile."clutter_h"]')
          ParentFont = False
        end
        object Shape_Color: TfrxShapeView
          Align = baLeft
          Left = 460.000000000000000000
          Width = 20.000000000000000000
          Height = 16.000000000000000000
          OnBeforePrint = 'Shape_ColorOnBeforePrint'
          ShowHint = False
          Color = clTeal
        end
        object Memo104: TfrxMemoView
          Align = baLeft
          Left = 480.000000000000000000
          Width = 176.000000000000000000
          Height = 16.000000000000000000
          ShowHint = False
          DataField = 'clutter_name'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Profile."clutter_name"]')
          ParentFont = False
        end
      end
      object Footer1: TfrxFooter
        Height = 8.000000000000000000
        Top = 116.000000000000000000
        Width = 718.110700000000000000
      end
    end
    object Page8: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object DetailData1: TfrxDetailData
        Height = 18.000000000000000000
        Top = 105.826840000000000000
        Width = 718.110700000000000000
        RowCount = 0
        object Itemscaption: TfrxMemoView
          Align = baWidth
          Left = 36.000000000000000000
          Width = 502.110700000000000000
          Height = 18.000000000000000000
          ShowHint = False
          DataField = 'caption'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Items."caption"]')
          ParentFont = False
        end
        object Memo77: TfrxMemoView
          Align = baRight
          Left = 538.110700000000000000
          Width = 90.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          DataField = 'value'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Items."value"]')
          ParentFont = False
        end
        object Memo78: TfrxMemoView
          Align = baRight
          Left = 628.110700000000000000
          Width = 90.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          DataField = 'value2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Items."value2"]')
          ParentFont = False
        end
        object Memo79: TfrxMemoView
          Align = baLeft
          Width = 36.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          DataField = 'id'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Items."id"]')
          ParentFont = False
        end
      end
      object MasterData6: TfrxMasterData
        Height = 24.000000000000000000
        Top = 60.472480000000000000
        Width = 718.110700000000000000
        RowCount = 0
        object groupcaption: TfrxMemoView
          Align = baWidth
          Left = 36.000000000000000000
          Width = 682.110700000000000000
          Height = 16.000000000000000000
          ShowHint = False
          DataField = 'caption'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[group."caption"]')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Align = baLeft
          Width = 36.000000000000000000
          Height = 16.000000000000000000
          ShowHint = False
          DataField = 'id'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[group."id"]')
          ParentFont = False
        end
      end
      object Header2: TfrxHeader
        Height = 20.000000000000000000
        Top = 18.897650000000000000
        Width = 718.110700000000000000
      end
      object Footer2: TfrxFooter
        Height = 20.000000000000000000
        Top = 147.401670000000000000
        Width = 718.110700000000000000
      end
      object Memo103: TfrxMemoView
        Align = baLeft
        Top = 15.333333330000000000
        Width = 434.000000000000000000
        Height = 16.666666670000000000
        ShowHint = False
        AutoWidth = True
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        Memo.UTF8 = (
          #1056#160#1056#181#1056#183#1057#1107#1056#187#1057#1034#1057#8218#1056#176#1057#8218#1057#8249' '#1057#1026#1056#176#1057#1027#1057#8225#1056#181#1057#8218#1056#1109#1056#1030)
        ParentFont = False
      end
    end
    object Subreport_right_: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object MasterData7: TfrxMasterData
        Height = 188.858282360000000000
        Top = 18.897650000000000000
        Width = 718.110700000000000000
        RowCount = 0
        object Memo105: TfrxMemoView
          Align = baRight
          Left = 494.724873228346500000
          Width = 110.000000000000000000
          Height = 30.236218030000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1119#1056#187#1056#1109#1057#8240#1056#176#1056#1169#1056#1108#1056#176)
          ParentFont = False
        end
        object Memo106: TfrxMemoView
          Align = baRight
          Left = 604.724873228346500000
          Width = 113.385826771653500000
          Height = 30.236220472440900000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Site2."name"]')
          ParentFont = False
        end
        object Memo108: TfrxMemoView
          Align = baRight
          Left = 494.724873228346500000
          Top = 30.236218030000000000
          Width = 110.000000000000000000
          Height = 15.118110236220500000
          ShowHint = False
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1113#1056#1109#1056#1109#1057#1026#1056#1169#1056#1105#1056#1029#1056#176#1057#8218#1057#8249' Pulkovo')
          ParentFont = False
        end
        object Memo109: TfrxMemoView
          Align = baRight
          Left = 494.724873228346500000
          Top = 90.708661420000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#8217#1057#8249#1057#1027#1056#1109#1057#8218#1056#176' '#1056#1111#1056#1109#1056#1169#1056#1030#1056#181#1057#1027#1056#176', m')
          ParentFont = False
        end
        object Memo110: TfrxMemoView
          Align = baRight
          Left = 494.724873228346500000
          Top = 105.897650000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1106#1056#183#1056#1105#1056#1112#1057#1107#1057#8218)
          ParentFont = False
        end
        object Memo111: TfrxMemoView
          Align = baRight
          Left = 494.724873228346500000
          Top = 120.897650000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1032#1056#1110#1056#1109#1056#187' '#1056#1112#1056#181#1057#1027#1057#8218#1056#176)
          ParentFont = False
        end
        object Memo112: TfrxMemoView
          Align = baRight
          Left = 494.724873228346500000
          Top = 135.897650000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#8221#1056#1105#1056#176#1056#1112#1056#181#1057#8218#1057#1026' '#1056#176#1056#1029#1057#8218#1056#181#1056#1029#1056#1029#1057#8249)
          ParentFont = False
        end
        object Memo114: TfrxMemoView
          Align = baRight
          Left = 604.724873228346500000
          Top = 30.236218030000000000
          Width = 113.385826771653500000
          Height = 15.118110236220500000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[DegreeToStr (<Site2."lat">)]')
          ParentFont = False
        end
        object Memo115: TfrxMemoView
          Align = baRight
          Left = 604.724873228346500000
          Top = 90.708661420000000000
          Width = 113.385826771653500000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[antenna2."height"]')
          ParentFont = False
        end
        object Memo116: TfrxMemoView
          Align = baRight
          Left = 604.724873228346500000
          Top = 105.897650000000000000
          Width = 113.385826771653500000
          Height = 15.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."azimuth2"]')
          ParentFont = False
        end
        object Memo117: TfrxMemoView
          Align = baRight
          Left = 604.724873228346500000
          Top = 135.897650000000000000
          Width = 113.385826771653500000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Antenna2."diameter"]')
          ParentFont = False
        end
        object Memo118: TfrxMemoView
          Align = baRight
          Left = 604.724873228346500000
          Top = 45.354330708661400000
          Width = 113.385826771653500000
          Height = 15.118110236220500000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[DegreeToStr (<Site2."lon">)]')
          ParentFont = False
        end
        object Memo130: TfrxMemoView
          Align = baRight
          Left = 494.724873228346500000
          Top = 45.354330708661400000
          Width = 110.000000000000000000
          Height = 15.118110236220500000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Align = baRight
          Left = 604.724873228346500000
          Top = 120.897650000000000000
          Width = 113.385826771653500000
          Height = 15.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."ELEVATION_angle_2"]')
          ParentFont = False
        end
        object Memo107: TfrxMemoView
          Align = baRight
          Left = 494.724873228346500000
          Top = 60.472440944881890000
          Width = 110.000000000000000000
          Height = 15.118110236220500000
          ShowHint = False
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1113#1056#1109#1056#1109#1057#1026#1056#1169#1056#1105#1056#1029#1056#176#1057#8218#1057#8249' WGS')
          ParentFont = False
        end
        object Memo113: TfrxMemoView
          Align = baRight
          Left = 604.724873228346500000
          Top = 60.472440940000000000
          Width = 113.385826771653500000
          Height = 15.118110236220500000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[DegreeToStr (<Site2."lat_wgs">)]')
          ParentFont = False
        end
        object Memo119: TfrxMemoView
          Align = baRight
          Left = 604.724873228346500000
          Top = 75.590551180000000000
          Width = 113.385826771653500000
          Height = 15.118110236220500000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[DegreeToStr (<Site2."lon_wgs">)]')
          ParentFont = False
        end
        object Memo129: TfrxMemoView
          Align = baRight
          Left = 494.724873228346500000
          Top = 75.590551181102400000
          Width = 110.000000000000000000
          Height = 15.118110236220500000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          ParentFont = False
        end
        object Memo140: TfrxMemoView
          Align = baRight
          Left = 494.724873228346500000
          Top = 151.181102360000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          ShowHint = False
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1032#1057#1027#1056#1105#1056#187#1056#181#1056#1029#1056#1105#1056#181' '#1056#176#1056#1029#1057#8218#1056#181#1056#1029#1056#1029#1057#8249)
          ParentFont = False
        end
        object Memo141: TfrxMemoView
          Align = baRight
          Left = 604.724873228346500000
          Top = 151.181102360000000000
          Width = 113.385826771653500000
          Height = 15.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Antenna2."gain"]')
          ParentFont = False
        end
      end
    end
  end
end
