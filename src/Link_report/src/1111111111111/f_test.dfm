object dlg_Test: Tdlg_Test
  Left = 572
  Top = 58
  Width = 868
  Height = 753
  Caption = 'dlg_Test'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl1: TPageControl
    Left = 0
    Top = 0
    Width = 852
    Height = 689
    ActivePage = TabSheet2
    Align = alTop
    TabOrder = 0
    object TabSheet2: TTabSheet
      Caption = 'TabSheet2'
      ImageIndex = 1
      object DBGrid1: TDBGrid
        Left = 0
        Top = 0
        Width = 844
        Height = 81
        Align = alTop
        DataSource = dmLink_report.ds_Link
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
      end
      object Panel1: TPanel
        Left = 0
        Top = 81
        Width = 844
        Height = 72
        Align = alTop
        Caption = 'Panel1'
        TabOrder = 1
        object DBGrid2: TDBGrid
          Left = 322
          Top = 1
          Width = 321
          Height = 70
          Align = alLeft
          DataSource = dmLink_report.ds_Property2
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
        end
        object DBGrid3: TDBGrid
          Left = 1
          Top = 1
          Width = 321
          Height = 70
          Align = alLeft
          DataSource = dmLink_report.ds_Property1
          TabOrder = 1
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
        end
      end
      object Panel2: TPanel
        Left = 0
        Top = 153
        Width = 844
        Height = 80
        Align = alTop
        Caption = 'Panel2'
        TabOrder = 2
        object DBGrid5: TDBGrid
          Left = 1
          Top = 1
          Width = 321
          Height = 78
          Align = alLeft
          DataSource = dmLink_report.ds_LinkEnd1
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
        end
        object DBGrid4: TDBGrid
          Left = 322
          Top = 1
          Width = 321
          Height = 78
          Align = alLeft
          DataSource = dmLink_report.ds_LinkEnd2
          TabOrder = 1
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
        end
      end
      object Panel3: TPanel
        Left = 0
        Top = 449
        Width = 844
        Height = 136
        Align = alTop
        Caption = 'Panel3'
        TabOrder = 3
        object DBGrid6: TDBGrid
          Left = 322
          Top = 1
          Width = 321
          Height = 134
          Align = alLeft
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
        end
        object DBGrid7: TDBGrid
          Left = 1
          Top = 1
          Width = 321
          Height = 134
          Align = alLeft
          TabOrder = 1
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
        end
      end
      object Panel4: TPanel
        Left = 0
        Top = 233
        Width = 844
        Height = 96
        Align = alTop
        Caption = 'Panel3'
        TabOrder = 4
        object DBGrid9: TDBGrid
          Left = 1
          Top = 1
          Width = 432
          Height = 94
          Align = alLeft
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
        end
        object DBImage1: TDBImage
          Left = 432
          Top = 8
          Width = 129
          Height = 73
          DataField = 'map_img'
          TabOrder = 1
        end
      end
      object Panel5: TPanel
        Left = 0
        Top = 329
        Width = 844
        Height = 120
        Align = alTop
        Caption = 'Panel3'
        TabOrder = 5
        object DBGrid8: TDBGrid
          Left = 1
          Top = 1
          Width = 432
          Height = 118
          Align = alLeft
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
        end
      end
    end
    object TabSheet1: TTabSheet
      Caption = 'TabSheet1'
      object DBGrid10: TDBGrid
        Left = 0
        Top = 0
        Width = 853
        Height = 161
        Align = alTop
        DataSource = dmLink_report.ds_Property1
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
      end
    end
    object TabSheet3: TTabSheet
      Caption = 'TabSheet3'
      ImageIndex = 2
      object DBGrid11: TDBGrid
        Left = 0
        Top = 0
        Width = 432
        Height = 661
        Align = alLeft
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
      end
    end
    object TabSheet4: TTabSheet
      Caption = 'TabSheet4'
      ImageIndex = 3
      object DBImage2: TDBImage
        Left = 8
        Top = 8
        Width = 465
        Height = 457
        DataField = 'map_img'
        TabOrder = 0
      end
    end
  end
end
