unit f_test_linkline;

interface

uses
  Classes, Controls, Forms,
  DBGrids, ExtCtrls, DBCtrls, Grids;

type
  Tdlg_Test_linkline = class(TForm)
    DBGrid1: TDBGrid;
    Panel4: TPanel;
    DBGrid9: TDBGrid;
    DBImage1: TDBImage;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    class procedure ExecDlg;
  end;

var
  dlg_Test_linkline: Tdlg_Test_linkline;

implementation
{$R *.dfm}

uses
  dm_Link_report;

//uses dm_Link_report;

//uses //dm_LinkLine_report,
// dm_LinkLine_report_new;


procedure Tdlg_Test_linkline.FormCreate(Sender: TObject);
begin
  Assert(dmLink_report.ds_Property1.DataSet.RecordCount>0);
  Assert(dmLink_report.ds_Property2.DataSet.RecordCount>0);

end;


class procedure Tdlg_Test_linkline.ExecDlg;
begin
  with Tdlg_Test_linkline.Create(Application) do
  begin
    ShowModal;
    Free;
  end;    // with

end;

end.
