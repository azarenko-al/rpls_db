unit dm_Test1;

interface

uses
  Classes, DB, ADODB, Forms,

  u_const_db,
                
  u_db_mdb,
  u_db, frxClass, frxDesgn

  ;

type
  TdmTest1 = class(TDataModule)
    ADOConnection_mdb: TADOConnection;
    t_Img: TADOTable;
    t_Img_img1: TBlobField;
    t_Img_mg2: TBlobField;
    t_Img_img3: TBlobField;
    t_Img_profile_img: TBlobField;
    t_Img_map_img: TBlobField;
    t_Img_link_id: TIntegerField;
    ADOConnection_mdb_new: TADOConnection;
    t_img111: TADOTable;
    BlobField1: TBlobField;
    BlobField2: TBlobField;
    BlobField3: TBlobField;
    BlobField4: TBlobField;
    BlobField5: TBlobField;
    IntegerField1: TIntegerField;
    t_Profile_items11: TADOTable;
    t_groups11: TADOTable;
    t_Items11: TADOTable;
    frxReport: TfrxReport;
    frxDesigner1: TfrxDesigner;
  private
    procedure CreateTempData(aFileName: string);
    { Private declarations }
  public
    class procedure Init;

  end;

var
  dmTest1: TdmTest1;

implementation

{$R *.dfm}


class procedure TdmTest1.Init;
begin
  if not Assigned(dmTest1) then
    dmTest1 := TdmTest1.Create(Application);
end;



// ---------------------------------------------------------------
procedure TdmTest1.CreateTempData(aFileName: string);
// ---------------------------------------------------------------
const
  TBL_IMG = 'img';

  TBL_GROUP = 'group1';
  TBL_ITEM  = 'item';
  TBL_Profile_items = 'Profile_items';

var
 // sFileName: string;
  sSQL: string;
begin
  Assert(aFileName<>'');

//  sFileName:='d:\aaaa.mdb';

  mdb_CreateFile(aFileName);

  mdb_OpenConnection(ADOConnection_mdb_new, aFileName);


 sSQL:=mdb_MakeCreateSQL(TBL_GROUP,
            [
              db_Field('id1',       ftAutoInc),

              db_Field(FLD_id,      ftInteger),
              db_Field(FLD_link_id, ftInteger),
              db_Field(FLD_CAPTION, ftString)
            ]);

  ADOConnection_mdb_new.Execute(sSQL);

  sSQL:=mdb_MakeCreateSQL(TBL_ITEM,
            [
              db_Field(FLD_id,        ftInteger),
              db_Field(FLD_parent_id, ftInteger),
              db_Field(FLD_CAPTION,   ftString),
              db_Field(FLD_VALUE,     ftFloat),
              db_Field(FLD_VALUE2,    ftFloat)
            ]);

  ADOConnection_mdb_new.Execute(sSQL);



  sSQL:=mdb_MakeCreateSQL( TBL_IMG ,
            [
              db_Field(FLD_link_id, ftInteger),
              db_Field('img1', ftGraphic),
              db_Field('img2', ftGraphic),
              db_Field('img3', ftGraphic),
              db_Field('profile_img', ftGraphic),
              db_Field('map_img', ftGraphic)
            ]);

//  GetTableCreateSQL (aDataset, aTableName, aKeyFieldName);

  ADOConnection_mdb_new.Execute(sSQL);


  sSQL:=mdb_MakeCreateSQL( TBL_Profile_items, // 'Profile_items',
            [
              db_Field('link_id',     ftInteger),
              db_Field(FLD_INDEX,     ftInteger),
              db_Field('distance_km', ftFloat),
              db_Field('ground_h',    ftFloat),
              db_Field('clutter_h',   ftFloat),
              db_Field('earth_h',     ftFloat),

              db_Field('clutter_code',  ftInteger),
              db_Field('clutter_color', ftInteger),
              db_Field('clutter_name',  ftString)
            ]);

  ADOConnection_mdb_new.Execute(sSQL);


 // db_SetComponentsADOConn ([t_img1],

(*  ,
                            t_Profile_items,
                            t_groups,
                            t_Items],
*)
                       //   ADOConnection_mdb_new);


 // t_img1.TableName          :=TBL_IMG;

(*  t_Profile_items.TableName :=TBL_Profile_items;
  t_groups.TableName        :=TBL_GROUP;
  t_Items.TableName         :=TBL_ITEM;
*)


 // t_img1.Open;

(*  t_Profile_items.Open;
  t_groups.Open;
  t_Items.Open;
*)




end;





end.
