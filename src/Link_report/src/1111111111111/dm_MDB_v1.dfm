object dmMDB_v1: TdmMDB_v1
  OldCreateOrder = True
  OnCreate = DataModuleCreate
  Left = 1578
  Top = 448
  Height = 696
  Width = 1261
  object ds_Property2: TDataSource
    DataSet = t_Property2
    Left = 312
    Top = 240
  end
  object ds_Property1: TDataSource
    DataSet = t_Property1
    Left = 200
    Top = 240
  end
  object ds_LinkEnd1: TDataSource
    DataSet = t_LinkEnd1
    Left = 440
    Top = 248
  end
  object ds_LinkEnd2: TDataSource
    DataSet = t_LinkEnd2
    Left = 546
    Top = 248
  end
  object ds_Link: TDataSource
    DataSet = t_link
    Left = 56
    Top = 208
  end
  object t_LInk_repeater: TADOTable
    CursorType = ctStatic
    TableName = 'LInk_repeater'
    Left = 600
    Top = 472
  end
  object t_LInk_repeater_ant: TADOTable
    CursorType = ctStatic
    MasterSource = ds_LInk_repeater
    TableName = 'lInk_repeater_ant'
    Left = 720
    Top = 472
  end
  object ds_LInk_repeater: TDataSource
    DataSet = t_LInk_repeater
    Left = 600
    Top = 528
  end
  object t_LInk_repeater_property: TADOTable
    CursorType = ctStatic
    MasterSource = ds_LInk_repeater
    TableName = 'LInk_repeater_property'
    Left = 912
    Top = 480
  end
  object t_Profile_items: TADOTable
    CursorType = ctStatic
    IndexFieldNames = 'link_id'
    MasterFields = 'id'
    MasterSource = ds_Link
    TableName = 'Profile_items'
    Left = 400
    Top = 472
  end
  object t_Items: TADOTable
    CursorType = ctStatic
    IndexFieldNames = 'parent_id'
    MasterFields = 'id'
    MasterSource = ds_groups
    TableName = 'Items'
    Left = 272
    Top = 472
  end
  object t_link_img11111: TADOTable
    CursorType = ctStatic
    IndexFieldNames = 'link_id'
    MasterFields = 'id'
    MasterSource = ds_Link
    TableName = 'link_img'
    Left = 56
    Top = 352
  end
  object t_groups: TADOTable
    CursorType = ctStatic
    IndexFieldNames = 'link_id'
    MasterFields = 'id'
    MasterSource = ds_Link
    TableName = 'groups'
    Left = 160
    Top = 464
  end
  object ds_groups: TDataSource
    DataSet = t_groups
    Left = 160
    Top = 536
  end
  object ClientDataSet_Link_img: TClientDataSet
    Active = True
    Aggregates = <>
    IndexFieldNames = 'link_id'
    MasterFields = 'id'
    MasterSource = ds_Link
    PacketRecords = 0
    Params = <>
    Left = 56
    Top = 424
    Data = {
      980100009619E0BD01000000180000000B0000000000030000009801076C696E
      6B5F6964040001000400000004696D673104004B000000010007535542545950
      4502004900090047726170686963730004696D673204004B0000000100075355
      425459504502004900090047726170686963730004696D673304004B00000001
      0007535542545950450200490009004772617068696373000B70726F66696C65
      5F696D6704004B00000001000753554254595045020049000900477261706869
      637300076D61705F696D6704004B000000010007535542545950450200490009
      004772617068696373000D696D67315F66696C656E616D650100490000000100
      0557494454480200020064000D696D67325F66696C656E616D65010049000000
      01000557494454480200020064000D696D67335F66696C656E616D6501004900
      000001000557494454480200020064001470726F66696C655F696D675F66696C
      656E616D6501004900000001000557494454480200020064000C6D61705F6669
      6C656E616D6501004900000001000557494454480200020064000000}
    object ClientDataSet_Link_imglink_id: TIntegerField
      FieldName = 'link_id'
      Required = True
    end
    object ClientDataSet_Link_imgimg1: TGraphicField
      FieldName = 'img1'
      BlobType = ftGraphic
    end
    object ClientDataSet_Link_imgimg2: TGraphicField
      FieldName = 'img2'
      BlobType = ftGraphic
    end
    object ClientDataSet_Link_imgimg3: TGraphicField
      FieldName = 'img3'
      BlobType = ftGraphic
    end
    object ClientDataSet_Link_imgprofile_img: TGraphicField
      FieldName = 'profile_img'
      BlobType = ftGraphic
    end
    object ClientDataSet_Link_imgmap_img: TGraphicField
      FieldName = 'map_img'
      BlobType = ftGraphic
    end
    object ClientDataSet_Link_imgimg1_filename: TStringField
      FieldName = 'img1_filename'
      Size = 100
    end
    object ClientDataSet_Link_imgimg2_filename: TStringField
      FieldName = 'img2_filename'
      Size = 100
    end
    object ClientDataSet_Link_imgimg3_filename: TStringField
      FieldName = 'img3_filename'
      Size = 100
    end
    object ClientDataSet_Link_imgprofile_img_filename: TStringField
      FieldName = 'profile_img_filename'
      Size = 100
    end
    object ClientDataSet_Link_imgmap_filename: TStringField
      FieldName = 'map_filename'
      Size = 100
    end
  end
  object qry_LinkLine: TADOQuery
    Parameters = <>
    Left = 288
    Top = 32
  end
  object qry_LinkLine_links: TADOQuery
    Parameters = <>
    Left = 432
    Top = 32
  end
  object t_LinkLine: TADOTable
    CursorType = ctStatic
    TableName = 'Antennas1'
    Left = 664
    Top = 16
  end
  object t_LinkLine_links: TADOTable
    CursorType = ctStatic
    TableName = 'Antennas2'
    Left = 808
    Top = 8
  end
  object ClientDataSet_linkline_img: TClientDataSet
    Active = True
    Aggregates = <>
    Params = <>
    Left = 344
    Top = 584
    Data = {
      410000009619E0BD0100000018000000010000000000030000004100076D6170
      5F696D6704004B00000001000753554254595045020049000900477261706869
      6373000000}
    object ClientDataSet_linkline_imgmap_img: TGraphicField
      FieldName = 'map_img'
      BlobType = ftGraphic
    end
  end
  object t_link: TClientDataSet
    Aggregates = <>
    Params = <>
    Left = 56
    Top = 120
  end
  object t_Property2: TClientDataSet
    Aggregates = <>
    IndexFieldNames = 'id'
    MasterFields = 'Property2_id'
    Params = <>
    Left = 304
    Top = 120
  end
  object t_Property1: TClientDataSet
    Aggregates = <>
    IndexFieldNames = 'id'
    MasterFields = 'Property1_id'
    Params = <>
    Left = 192
    Top = 128
  end
  object t_LinkEnd1: TClientDataSet
    Aggregates = <>
    IndexFieldNames = 'id'
    MasterFields = 'linkend1_id'
    MasterSource = ds_Link
    PacketRecords = 0
    Params = <>
    Left = 448
    Top = 120
  end
  object t_LinkEnd2: TClientDataSet
    Aggregates = <>
    IndexFieldNames = 'id'
    MasterFields = 'linkend2_id'
    MasterSource = ds_Link
    PacketRecords = 0
    Params = <>
    Left = 544
    Top = 120
  end
  object t_Antennas1: TClientDataSet
    Aggregates = <>
    Params = <>
    Left = 720
    Top = 120
  end
  object t_Antennas2: TClientDataSet
    Aggregates = <>
    Params = <>
    Left = 864
    Top = 112
  end
  object t_LinkLine_: TClientDataSet
    Aggregates = <>
    Params = <>
    Left = 1032
    Top = 128
  end
  object ClientDataSet2: TClientDataSet
    Aggregates = <>
    Params = <>
    Left = 1064
    Top = 48
  end
  object ClientDataSet1: TClientDataSet
    Aggregates = <>
    Params = <>
    Left = 1032
    Top = 128
  end
  object t_groups_: TClientDataSet
    Aggregates = <>
    IndexFieldNames = 'id'
    MasterFields = 'Property1_id'
    Params = <>
    Left = 168
    Top = 368
  end
  object t_Items_: TClientDataSet
    Aggregates = <>
    IndexFieldNames = 'parent_id'
    MasterFields = 'id'
    MasterSource = ds_groups
    PacketRecords = 0
    Params = <>
    Left = 280
    Top = 376
  end
  object t_Profile_items_: TClientDataSet
    Aggregates = <>
    IndexFieldNames = 'link_id'
    MasterFields = 'id'
    MasterSource = ds_Link
    PacketRecords = 0
    Params = <>
    Left = 400
    Top = 376
  end
end
