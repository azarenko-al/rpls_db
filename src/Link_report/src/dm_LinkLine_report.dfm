object dmLinkLine_report: TdmLinkLine_report
  OldCreateOrder = True
  OnCreate = DataModuleCreate
  OnDestroy = DataModuleDestroy
  Left = 954
  Top = 517
  Height = 392
  Width = 548
  object qry_LinkLine: TADOQuery
    Parameters = <>
    Left = 56
    Top = 128
  end
  object frxDBDataset_LinkLine: TfrxDBDataset
    UserName = 'LinkLine'
    CloseDataSource = False
    FieldAliases.Strings = (
      'id=id'
      'name=name'
      'project_id=project_id'
      'folder_id=folder_id'
      'length=length'
      'length_km=length_km'
      '_=_'
      'SESR=SESR'
      'Kng=Kng'
      'Kng_year=Kng_year'
      'SESR_required=SESR_required'
      'KNG_required=KNG_required'
      'ESR_required=ESR_required'
      'BBER_required=BBER_required'
      'ESR_norm=ESR_norm'
      'BBER_norm=BBER_norm'
      'bitrate_Mbps=bitrate_Mbps'
      '_____=_____'
      'GST_TYPE=GST_TYPE'
      'GST_length=GST_length'
      'GST_SESR=GST_SESR'
      'GST_KNG=GST_KNG'
      'comment_norm=comment_norm'
      'status=status'
      'comment=comment'
      '__=__'
      'guid=guid'
      'date_created=date_created'
      'date_modify=date_modify'
      'user_created=user_created'
      'user_modify=user_modify'
      'GST_type_name=GST_type_name'
      'status_name=status_name'
      'status_str=status_str')
    DataSet = qry_LinkLine
    BCDToCurrency = False
    Left = 57
    Top = 260
  end
  object frxReport: TfrxReport
    Version = '6.2.11'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = #1055#1086' '#1091#1084#1086#1083#1095#1072#1085#1080#1102
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 41730.469225335600000000
    ReportOptions.LastChange = 42067.554385648150000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      ''
      'procedure rep_LinkLineOnStartReport(Sender: TfrxComponent);'
      'const'
      '  DEF_MAP_H = 800;'
      ''
      '  '
      ' // DEF_MAP_H = 800;'
      '  '
      '  DEF_H = 450;'
      '  DEF_OFFSET = 25;'
      ''
      '  DEF_TOP = 20;  '
      ''
      '    '
      'begin                                  '
      '    '
      '  Picture_linkline_map.Height  :=DEF_MAP_H;                '
      '  Child_linkline_map.Height    :=DEF_MAP_H;'
      ''
      '   '
      '//  Picture_map.Width:=200;                '
      '  Picture_map.Height     :=DEF_MAP_H;'
      '    '
      '  Picture_profile.Height :=DEF_H;                '
      '  Picture_profile1.Height:=DEF_H;                '
      '  Picture_profile2.Height:=DEF_H;                '
      '  Picture_profile3.Height:=DEF_H;                '
      '          '
      '  '
      '  Child_map.Height:=DEF_MAP_H;'
      '    '
      '  Child_profile.Height:=DEF_H + DEF_OFFSET;              '
      '  Child_profile1.Height:=DEF_H + DEF_OFFSET;              '
      '  Child_profile2.Height:=DEF_H + DEF_OFFSET;              '
      '  Child_profile3.Height:=DEF_H + DEF_OFFSET;'
      ''
      '  Child_profile1.Top:=DEF_TOP;              '
      '  Child_profile2.Top:=DEF_TOP;              '
      '  Child_profile3.Top:=DEF_TOP;'
      '                                        '
      '  '
      '    '
      '//  Child_.Height:=Picture_map.Height;'
      '      '
      'end;  '
      '        '
      '              '
      '                          '
      '                          '
      ''
      '//------------------------------------------------------------'
      'function DegreeToStr (aValue: double): String;'
      '//------------------------------------------------------------'
      'const'
      '//  STR_BLPOINT_FORMAT1 = '#39'%d'#176'%.2d'#39#39'%s%1.2f'#39#39#39#39#39';'
      '  STR_BLPOINT_FORMAT1 = '#39'%d'#176' %.2d'#39#39' %1.2f'#39#39#39#39#39';'
      'var    '
      ' iDeg,iMin:integer; '
      ' eSec: Double;   '
      '           '
      'begin'
      '  geo_DecodeDegree_(aValue, iDeg,iMin,eSec);'
      '                           '
      ' // angle.Sec:=TruncFloat(angle.Sec,2);'
      ' '
      '    Result:=Format(STR_BLPOINT_FORMAT1, [iDeg, iMin, eSec]);'
      ''
      '  if aValue<0 then'
      '    Result:='#39'-'#39'+Result;'
      'end;'
      '  '
      '           '
      ''
      '//------------------------------------------------------------'
      
        'procedure geo_DecodeDegree_(aValue: Double; var aDeg,aMin:intege' +
        'r; var aSec:'
      '    Double);'
      '//------------------------------------------------------------'
      'const'
      ' EXP_ = 0.000000001;'
      '     //97,033333333'
      'var'
      '  bIsNegative: Boolean;'
      '  e: Double;'
      '  eMinutes: Double;'
      '  eSec: Double;'
      '  eSeconds: Double;'
      'begin'
      '  bIsNegative := aValue<0;'
      ''
      '  aValue :=Abs(aValue);'
      ''
      '  aDeg:=Trunc(aValue);'
      ''
      ' // e:=Abs(aValue)*60;'
      ''
      '//  , Frac(X) = X - Int(X).'
      ''
      '  aMin:=Trunc(Frac(aValue)*60);'
      ''
      '//  Result.Sec:=Frac(Frac(Abs(aValue))*60)*60;'
      '  e:= (aValue - Trunc(aValue)) * 60;'
      '  aMin:=Trunc(e);'
      ''
      ' eMinutes := Trunc (e);'
      ' eSeconds := (e - Trunc (e)) * 60 ;'
      ''
      ''
      '  aSec:=(((aValue - aDeg) *60) - aMin) * 60;'
      ''
      '  if (aSec - 60) > EXP_ then'
      '  begin'
      '    Inc(aMin);'
      '    aSec := 0;'
      '  end;'
      ''
      ''
      '  if bIsNegative then'
      '    aDeg:=-aDeg;'
      '          '
      'end;'
      '  '
      '                '
      ''
      'procedure Shape_ColorOnBeforePrint(Sender: TfrxComponent);'
      'begin'
      
        '  Shape_Color.color := <Profile."clutter_color">                ' +
        '                                                    '
      'end;'
      '                  '
      ''
      'begin'
      'end.          ')
    OnStartReport = 'rep_LinkLineOnStartReport'
    Left = 193
    Top = 22
    Datasets = <
      item
        DataSet = dmLink_report.frxDBDataset_Antenna1
        DataSetName = 'Antenna1'
      end
      item
        DataSet = dmLink_report.frxDBDataset_Antenna2
        DataSetName = 'Antenna2'
      end
      item
        DataSet = dmLink_report.frxDBDataset_group
        DataSetName = 'group'
      end
      item
        DataSet = dmLink_report.frxDBDataset_Items
        DataSetName = 'Items'
      end
      item
        DataSet = dmLink_report.frxDBDataset_link
        DataSetName = 'Link'
      end
      item
        DataSet = dmLink_report.frxDBDataset_Link_img
        DataSetName = 'Link_img'
      end
      item
        DataSet = dmLink_report.frxDBDataset_LinkEnd1
        DataSetName = 'LinkEnd1'
      end
      item
        DataSet = dmLink_report.frxDBDataset_LinkEnd2
        DataSetName = 'LinkEnd2'
      end
      item
        DataSet = frxDBDataset_LinkLine
        DataSetName = 'LinkLine'
      end
      item
        DataSet = frxDBDataset_LinkLine_img
        DataSetName = 'LinkLine_img'
      end
      item
        DataSet = frxDBDataset_LinkLine_links
        DataSetName = 'LinkLine_links'
      end
      item
        DataSet = dmLink_report.frxDBDataset_Profile_items
        DataSetName = 'Profile'
      end
      item
        DataSet = dmLink_report.frxDBDataset_Property1
        DataSetName = 'Site1'
      end
      item
        DataSet = dmLink_report.frxDBDataset_Property2
        DataSetName = 'Site2'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 7.937500000000000000
      RightMargin = 9.260416666666670000
      TopMargin = 9.260416666666670000
      BottomMargin = 10.583333333333300000
      Frame.Typ = []
      LargeDesignHeight = True
      OnBeforePrint = 'Page1OnBeforePrint'
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Frame.Typ = []
        Height = 46.677180000000000000
        Top = 18.897650000000000000
        Width = 728.701258020833300000
        Child = frxReport.Child_Main_top
        DataSet = frxDBDataset_LinkLine
        DataSetName = 'LinkLine'
        RowCount = 0
        object Memo5: TfrxMemoView
          Align = baWidth
          AllowVectorExport = True
          Top = 4.000000000000000000
          Width = 728.701258020833300000
          Height = 13.763760000000000000
          CharSpacing = 1.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Color = clGray
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8 = (
            #1056#1115#1056#1118#1056#167#1056#8226#1056#1118' '#1056#1119#1056#1115' '#1056#160#1056#1106#1056#8221#1056#152#1056#1115#1056#160#1056#8226#1056#8250#1056#8226#1056#8482#1056#1116#1056#1115#1056#8482' '#1056#8250#1056#152#1056#1116#1056#152#1056#152)
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 20.000000000000000000
          Width = 243.701228960000000000
          Height = 22.677180000000000000
          DataField = 'name'
          DataSet = frxDBDataset_LinkLine
          DataSetName = 'LinkLine'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            '[LinkLine."name"]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object Child_Main_top: TfrxChild
        FillType = ftBrush
        Frame.Typ = []
        Height = 206.803340000000000000
        Top = 86.929190000000000000
        Width = 728.701258020833300000
        Child = frxReport.Child_linkline_map
        ToNRows = 0
        ToNRowsMode = rmCount
        object LineValue: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 340.157480310000000000
          Width = 151.181200000000000000
          Height = 15.000000000000000000
          DataField = 'length_km'
          DataSet = frxDBDataset_LinkLine
          DataSetName = 'LinkLine'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            '[LinkLine."length_km"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo12: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Width = 340.157480310000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#8221#1056#187#1056#1105#1056#1029#1056#176' '#1056#187#1056#1105#1056#1029#1056#1105#1056#1105', '#1056#1108#1056#1112)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo18: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 15.000000000000000000
          Width = 340.157480310000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1119#1057#1026#1056#1105#1056#183#1056#1029#1056#176#1056#1108' '#1056#1111#1057#1026#1056#1105#1056#1110#1056#1109#1056#1169#1056#1029#1056#1109#1057#1027#1057#8218#1056#1105)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo19: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 30.000000000000000000
          Width = 340.157480310000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1116#1056#1109#1057#1026#1056#1112#1056#1105#1057#1026#1057#1107#1056#181#1056#1112#1056#1109#1056#181' '#1056#183#1056#1029#1056#176#1057#8225#1056#181#1056#1029#1056#1105#1056#181' SESR '#1056#1029#1056#176' '#1056#187#1056#1105#1056#1029#1056#1105#1056#1105', %')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo20: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 45.000000000000000000
          Width = 340.157480310000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            
              #1056#1116#1056#1109#1057#1026#1056#1112#1056#1105#1057#1026#1057#1107#1056#181#1056#1112#1056#1109#1056#181' '#1056#183#1056#1029#1056#176#1057#8225#1056#181#1056#1029#1056#1105#1056#181' '#1056#1113#1056#1029#1056#1110' '#1056#1029#1056#176' '#1056#187#1056#1105#1056#1029#1056#1105#1056#1105', ' +
              '%')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo21: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 60.000000000000000000
          Width = 340.157480310000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1116#1056#1109#1057#1026#1056#1112#1056#1105#1057#1026#1057#1107#1056#181#1056#1112#1056#1109#1056#181' '#1056#183#1056#1029#1056#176#1057#8225#1056#181#1056#1029#1056#1105#1056#181' ESR '#1056#1029#1056#176' '#1056#187#1056#1105#1056#1029#1056#1105#1056#1105', %')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo30: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 75.000000000000000000
          Width = 340.157480310000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1116#1056#1109#1057#1026#1056#1112#1056#1105#1057#1026#1057#1107#1056#181#1056#1112#1056#1109#1056#181' '#1056#183#1056#1029#1056#176#1057#8225#1056#181#1056#1029#1056#1105#1056#181' BBER '#1056#1029#1056#176' '#1056#187#1056#1105#1056#1029#1056#1105#1056#1105', %')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo39: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 90.000000000000000000
          Width = 340.157480310000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1118#1057#1026#1056#181#1056#177#1057#1107#1056#181#1056#1112#1056#1109#1056#181' '#1056#183#1056#1029#1056#176#1057#8225#1056#181#1056#1029#1056#1105#1056#181' ESR '#1056#1029#1056#176' '#1056#187#1056#1105#1056#1029#1056#1105#1056#1105', %')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo44: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 105.000000000000000000
          Width = 340.157480310000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1118#1057#1026#1056#181#1056#177#1057#1107#1056#181#1056#1112#1056#1109#1056#181' '#1056#183#1056#1029#1056#176#1057#8225#1056#181#1056#1029#1056#1105#1056#181' BBER '#1056#1029#1056#176' '#1056#187#1056#1105#1056#1029#1056#1105#1056#1105', %')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo42: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 120.000000000000000000
          Width = 340.157480310000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            
              #1056#152#1057#8218#1056#1109#1056#1110#1056#1109#1056#1030#1057#8249#1056#8470' '#1056#1111#1056#1109#1056#1108#1056#176#1056#183#1056#176#1057#8218#1056#181#1056#187#1057#1034' '#1056#1108#1056#176#1057#8225#1056#181#1057#1027#1057#8218#1056#1030#1056#176' '#1056#1111#1056#1109' '#1056#1109#1057#8364 +
              #1056#1105#1056#177#1056#1108#1056#176#1056#1112' SESR, %')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo45: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 135.000000000000000000
          Width = 340.157480310000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            
              #1056#152#1057#8218#1056#1109#1056#1110#1056#1109#1056#1030#1057#8249#1056#8470' '#1056#1111#1056#1109#1056#1108#1056#176#1056#183#1056#176#1057#8218#1056#181#1056#187#1057#1034' '#1056#1029#1056#181#1056#1110#1056#1109#1057#8218#1056#1109#1056#1030#1056#1029#1056#1109#1057#1027#1057#8218#1056#1105' '#1056 +
              #1113#1056#1029#1056#1110', %')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo46: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 150.000000000000000000
          Width = 340.157480310000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1116#1056#1109#1057#1026#1056#1112#1056#1105#1057#1026#1057#1107#1056#181#1056#1112#1056#1109#1056#181' '#1056#183#1056#1029#1056#176#1057#8225#1056#181#1056#1029#1056#1105#1056#181' SESR '#1056#1169#1056#187#1057#1039' '#1056#8220#1056#173#1056#1118#1056#166', %')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo47: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 165.000000000000000000
          Width = 340.157480310000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            
              #1056#1116#1056#1109#1057#1026#1056#1112#1056#1105#1057#1026#1057#1107#1056#181#1056#1112#1056#1109#1056#181' '#1056#183#1056#1029#1056#176#1057#8225#1056#181#1056#1029#1056#1105#1056#181' '#1056#1113#1056#1029#1056#1110' '#1056#1169#1056#187#1057#1039' '#1056#8220#1056#173#1056#1118#1056#166', ' +
              '%')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 180.000000000000000000
          Width = 340.157480310000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#8221#1056#187#1056#1105#1056#1029#1056#176' '#1056#8220#1056#173#1056#1118#1056#166', '#1056#1108#1056#1112)
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo6: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 340.157480310000000000
          Top = 15.000000000000000000
          Width = 151.181200000000000000
          Height = 15.000000000000000000
          DataField = 'status_name'
          DataSet = frxDBDataset_LinkLine
          DataSetName = 'LinkLine'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            '[LinkLine."status_name"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo49: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 340.157480310000000000
          Top = 30.000000000000000000
          Width = 151.181200000000000000
          Height = 15.000000000000000000
          Hint = #1053#1086#1088#1084#1080#1088#1091#1077#1084#1086#1077' '#1079#1085#1072#1095#1077#1085#1080#1077' SESR '#1085#1072' '#1080#1085#1090#1077#1088#1074#1072#1083#1077' [%]'
          DataField = 'SESR_required'
          DataSet = frxDBDataset_LinkLine
          DataSetName = 'LinkLine'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            '[LinkLine."SESR_required"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo50: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 340.157480310000000000
          Top = 45.000000000000000000
          Width = 151.181200000000000000
          Height = 15.000000000000000000
          DataField = 'KNG_required'
          DataSet = frxDBDataset_LinkLine
          DataSetName = 'LinkLine'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            '[LinkLine."KNG_required"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo51: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 340.157480310000000000
          Top = 60.000000000000000000
          Width = 151.181200000000000000
          Height = 15.000000000000000000
          DataField = 'ESR_norm'
          DataSet = frxDBDataset_LinkLine
          DataSetName = 'LinkLine'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            '[LinkLine."ESR_norm"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo52: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 340.157480310000000000
          Top = 75.000000000000000000
          Width = 151.181200000000000000
          Height = 15.000000000000000000
          DataField = 'BBER_norm'
          DataSet = frxDBDataset_LinkLine
          DataSetName = 'LinkLine'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            '[LinkLine."BBER_norm"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo53: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 340.157480310000000000
          Top = 90.000000000000000000
          Width = 151.181200000000000000
          Height = 15.000000000000000000
          DataField = 'ESR_required'
          DataSet = frxDBDataset_LinkLine
          DataSetName = 'LinkLine'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            '[LinkLine."ESR_required"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo54: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 340.157480310000000000
          Top = 105.000000000000000000
          Width = 151.181200000000000000
          Height = 15.000000000000000000
          DataField = 'BBER_required'
          DataSet = frxDBDataset_LinkLine
          DataSetName = 'LinkLine'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            '[LinkLine."BBER_required"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo55: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 340.157480310000000000
          Top = 120.000000000000000000
          Width = 151.181200000000000000
          Height = 15.000000000000000000
          DataField = 'SESR'
          DataSet = frxDBDataset_LinkLine
          DataSetName = 'LinkLine'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            '[LinkLine."SESR"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo56: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 340.157480310000000000
          Top = 135.000000000000000000
          Width = 151.181200000000000000
          Height = 15.000000000000000000
          DataField = 'Kng'
          DataSet = frxDBDataset_LinkLine
          DataSetName = 'LinkLine'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            '[LinkLine."Kng"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo1: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 340.157480310000000000
          Top = 150.000000000000000000
          Width = 151.181200000000000000
          Height = 15.000000000000000000
          DataField = 'GST_SESR'
          DataSet = frxDBDataset_LinkLine
          DataSetName = 'LinkLine'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            '[LinkLine."GST_SESR"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo4: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 340.157480310000000000
          Top = 165.000000000000000000
          Width = 151.181200000000000000
          Height = 15.000000000000000000
          DataField = 'GST_KNG'
          DataSet = frxDBDataset_LinkLine
          DataSetName = 'LinkLine'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            '[LinkLine."GST_KNG"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo7: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 340.157480310000000000
          Top = 180.000000000000000000
          Width = 151.181200000000000000
          Height = 15.000000000000000000
          DataField = 'GST_length'
          DataSet = frxDBDataset_LinkLine
          DataSetName = 'LinkLine'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            '[LinkLine."GST_length"]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object Child_linkline_map: TfrxChild
        FillType = ftBrush
        Frame.Typ = []
        Height = 35.614361560000000000
        Top = 317.480520000000000000
        Width = 728.701258020833300000
        StartNewPage = True
        ToNRows = 0
        ToNRowsMode = rmCount
        object Picture_linkline_map: TfrxPictureView
          Align = baWidth
          AllowVectorExport = True
          ShiftMode = smWhenOverlapped
          Width = 728.701258020833300000
          Height = 27.614361560000000000
          AutoSize = True
          DataField = 'map_img'
          DataSet = frxDBDataset_LinkLine_img
          DataSetName = 'LinkLine_img'
          Frame.Typ = []
          HightQuality = False
          Transparent = False
          TransparentColor = clWhite
        end
      end
      object MasterData2: TfrxMasterData
        FillType = ftBrush
        Frame.Typ = []
        Height = 48.000000000000000000
        Top = 464.882190000000000000
        Width = 728.701258020833300000
        Child = frxReport.Child_top
        DataSet = dmLink_report.frxDBDataset_link
        DataSetName = 'Link'
        RowCount = 0
        StartNewPage = True
        object Memo8: TfrxMemoView
          Align = baWidth
          AllowVectorExport = True
          Width = 728.701258020833300000
          Height = 15.000000000000000000
          AutoWidth = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8 = (
            #1056#1115#1057#8218#1057#8225#1056#181#1057#8218' '#1056#1111#1056#1109' '#1057#1026#1056#176#1056#1169#1056#1105#1056#1109#1057#1026#1056#181#1056#187#1056#181#1056#8470#1056#1029#1056#1109#1056#1112#1057#1107' '#1056#1105#1056#1029#1057#8218#1056#181#1057#1026#1056#1030#1056#176#1056#187#1057#1107)
          ParentFont = False
        end
        object Linkname: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 23.779530000000020000
          Width = 132.000000000000000000
          Height = 16.000000000000000000
          AutoWidth = True
          DataField = 'name'
          DataSet = dmLink_report.frxDBDataset_link
          DataSetName = 'Link'
          Frame.Typ = []
          Memo.UTF8 = (
            '[Link."name"]')
        end
      end
      object Child_top: TfrxChild
        FillType = ftBrush
        Frame.Typ = []
        Height = 67.000000000000000000
        Top = 536.693260000000000000
        Width = 728.701258020833300000
        Child = frxReport.Child_summary
        ToNRows = 0
        ToNRowsMode = rmCount
        object Subreport_left11: TfrxSubreport
          Align = baLeft
          AllowVectorExport = True
          Width = 226.771653540000000000
          Height = 50.000000000000000000
          Page = frxReport.Page2
        end
        object Subreport_center11: TfrxSubreport
          Align = baCenter
          AllowVectorExport = True
          Left = 240.114389010416700000
          Width = 248.472480000000000000
          Height = 50.000000000000000000
          Page = frxReport.Page3
        end
        object Subreport_R11: TfrxSubreport
          Align = baRight
          AllowVectorExport = True
          Left = 501.929604480833300000
          Width = 226.771653540000000000
          Height = 50.000000000000000000
          Page = frxReport.Page4
        end
      end
      object Child_summary: TfrxChild
        FillType = ftBrush
        Frame.Typ = []
        Height = 48.000000000000000000
        Top = 627.401980000000000000
        Width = 728.701258020833300000
        Child = frxReport.Child_Map
        ToNRows = 0
        ToNRowsMode = rmCount
        object Subreport_summary2: TfrxSubreport
          Align = baWidth
          AllowVectorExport = True
          Width = 728.701258020833300000
          Height = 32.000000000000000000
          Page = frxReport.Page5
        end
      end
      object Child_Map: TfrxChild
        FillType = ftBrush
        Frame.Typ = []
        Height = 48.000000000000000000
        Top = 699.213050000000000000
        Width = 728.701258020833300000
        Child = frxReport.Child_Profile
        ToNRows = 0
        ToNRowsMode = rmCount
        object Picture_map: TfrxPictureView
          Description = 'Map'
          Align = baWidth
          AllowVectorExport = True
          Width = 728.701258020833300000
          Height = 28.000000000000000000
          OnBeforePrint = 'Picture_mapOnBeforePrint'
          DataField = 'map_img'
          DataSet = dmLink_report.frxDBDataset_Link_img
          DataSetName = 'Link_img'
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Stretched = False
          HightQuality = True
          Transparent = False
          TransparentColor = clWhite
        end
      end
      object Child_Profile: TfrxChild
        FillType = ftBrush
        Frame.Typ = []
        Height = 76.000000000000000000
        Top = 771.024120000000000000
        Width = 728.701258020833300000
        Child = frxReport.Child_Profile1
        ToNRows = 0
        ToNRowsMode = rmCount
        object Picture_profile: TfrxPictureView
          Align = baWidth
          AllowVectorExport = True
          Top = 20.000000000000000000
          Width = 728.701258020833300000
          Height = 36.000000000000000000
          OnBeforePrint = 'Picture_profileOnBeforePrint'
          DataField = 'profile_img'
          DataSet = dmLink_report.frxDBDataset_Link_img
          DataSetName = 'Link_img'
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Stretched = False
          HightQuality = True
          Transparent = False
          TransparentColor = clWhite
        end
        object Memo141: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          ShiftMode = smWhenOverlapped
          Width = 363.200000000000000000
          Height = 16.800000000000000000
          AutoWidth = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8 = (
            
              #1056#152#1056#183#1056#1109#1056#177#1057#1026#1056#176#1056#182#1056#181#1056#1029#1056#1105#1056#181' '#1056#1111#1057#1026#1056#1109#1057#8222#1056#1105#1056#187#1057#1039' '#1056#1111#1057#1026#1056#1105' '#1056#1029#1056#1109#1057#1026#1056#1112#1056#176#1056#187#1057#1034#1056#1029#1056#1109#1056 +
              #8470' '#1057#1026#1056#181#1057#8222#1057#1026#1056#176#1056#1108#1057#8224#1056#1105#1056#1105)
          ParentFont = False
        end
      end
      object Child_Profile1: TfrxChild
        FillType = ftBrush
        Frame.Typ = []
        Height = 80.000000000000000000
        Top = 869.291900000000000000
        Width = 728.701258020833300000
        Child = frxReport.Child_Profile2
        ToNRows = 0
        ToNRowsMode = rmCount
        object Picture_profile1: TfrxPictureView
          Align = baWidth
          AllowVectorExport = True
          ShiftMode = smWhenOverlapped
          Top = 24.000000000000000000
          Width = 728.701258020833300000
          Height = 40.000000000000000000
          OnBeforePrint = 'Picture_profile1OnBeforePrint'
          DataField = 'img1'
          DataSet = dmLink_report.frxDBDataset_Link_img
          DataSetName = 'Link_img'
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Stretched = False
          HightQuality = True
          Transparent = False
          TransparentColor = clWhite
        end
        object Memo142: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          ShiftMode = smWhenOverlapped
          Width = 359.200000000000000000
          Height = 16.800000000000000000
          AutoWidth = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8 = (
            
              #1056#152#1056#183#1056#1109#1056#177#1057#1026#1056#176#1056#182#1056#181#1056#1029#1056#1105#1056#181' '#1056#1111#1057#1026#1056#1109#1057#8222#1056#1105#1056#187#1057#1039' '#1056#1111#1057#1026#1056#1105' 20% '#1057#1026#1056#181#1057#8222#1057#1026#1056#176#1056#1108#1057#8224#1056 +
              #1105#1056#1105)
          ParentFont = False
        end
      end
      object Child_Profile2: TfrxChild
        FillType = ftBrush
        Frame.Typ = []
        Height = 80.000000000000000000
        Top = 971.339210000000000000
        Width = 728.701258020833300000
        Child = frxReport.Child_Profile3
        ToNRows = 0
        ToNRowsMode = rmCount
        object Picture_profile2: TfrxPictureView
          Align = baWidth
          AllowVectorExport = True
          ShiftMode = smWhenOverlapped
          Top = 24.000000000000000000
          Width = 728.701258020833300000
          Height = 43.000000000000000000
          OnBeforePrint = 'Picture_profile2OnBeforePrint'
          DataField = 'img2'
          DataSet = dmLink_report.frxDBDataset_Link_img
          DataSetName = 'Link_img'
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Stretched = False
          HightQuality = True
          Transparent = False
          TransparentColor = clWhite
        end
        object Memo143: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          ShiftMode = smWhenOverlapped
          Width = 567.200000000000000000
          Height = 16.800000000000000000
          AutoWidth = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8 = (
            
              #1056#152#1056#183#1056#1109#1056#177#1057#1026#1056#176#1056#182#1056#181#1056#1029#1056#1105#1056#181' '#1056#1111#1057#1026#1056#1109#1057#8222#1056#1105#1056#187#1057#1039' '#1056#1030' '#1057#1107#1057#1027#1056#187#1056#1109#1056#1030#1056#1105#1057#1039#1057#8230' '#1057#1027#1057#1107#1056#177 +
              #1057#1026#1056#181#1057#8222#1057#1026#1056#176#1056#1108#1057#8224#1056#1105#1056#1105)
          ParentFont = False
        end
      end
      object Child_Profile3: TfrxChild
        FillType = ftBrush
        Frame.Typ = []
        Height = 68.000000000000000000
        Top = 1073.386520000000000000
        Width = 728.701258020833300000
        Child = frxReport.Child_calc_res
        ToNRows = 0
        ToNRowsMode = rmCount
        object Memo140: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          ShiftMode = smWhenOverlapped
          Width = 559.200000000000000000
          Height = 16.800000000000000000
          AutoWidth = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8 = (
            
              #1056#152#1056#183#1056#1109#1056#177#1057#1026#1056#176#1056#182#1056#181#1056#1029#1056#1105#1056#181' '#1056#1111#1057#1026#1056#1109#1057#8222#1056#1105#1056#187#1057#1039' '#1056#1030' '#1057#1107#1057#1027#1056#187#1056#1109#1056#1030#1056#1105#1057#1039#1057#8230' '#1057#1027#1057#1107#1056#177 +
              #1057#1026#1056#181#1057#8222#1057#1026#1056#176#1056#1108#1057#8224#1056#1105#1056#1105#1056#1109#1056#1029#1056#1029#1056#1109#1056#8470' '#1056#1029#1056#181#1057#1107#1057#1027#1057#8218#1056#1109#1056#8470#1057#8225#1056#1105#1056#1030#1056#1109#1057#1027#1057#8218#1056#1105)
          ParentFont = False
        end
        object Picture_profile3: TfrxPictureView
          Align = baWidth
          AllowVectorExport = True
          ShiftMode = smWhenOverlapped
          Top = 24.000000000000000000
          Width = 728.701258020833300000
          Height = 31.000000000000000000
          OnBeforePrint = 'Picture_profile3OnBeforePrint'
          DataField = 'img3'
          DataSet = dmLink_report.frxDBDataset_Link_img
          DataSetName = 'Link_img'
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Stretched = False
          HightQuality = True
          Transparent = False
          TransparentColor = clWhite
        end
      end
      object Child_calc_res: TfrxChild
        FillType = ftBrush
        Frame.Typ = []
        Height = 48.000000000000000000
        Top = 1164.095240000000000000
        Width = 728.701258020833300000
        Child = frxReport.Child_Profile_items
        StartNewPage = True
        ToNRows = 0
        ToNRowsMode = rmCount
        object Subreport_calc_res1: TfrxSubreport
          Align = baWidth
          AllowVectorExport = True
          Width = 728.701258020833300000
          Height = 36.000000000000000000
          Page = frxReport.Page6
        end
      end
      object Child_Profile_items: TfrxChild
        FillType = ftBrush
        Frame.Typ = []
        Height = 48.000000000000000000
        Top = 1235.906310000000000000
        Visible = False
        Width = 728.701258020833300000
        StartNewPage = True
        ToNRows = 0
        ToNRowsMode = rmCount
        object Subreport1: TfrxSubreport
          Align = baWidth
          AllowVectorExport = True
          Width = 728.701258020833300000
          Height = 28.000000000000000000
          Page = frxReport.Page7
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        Frame.Typ = []
        Height = 20.000000000000000000
        Top = 1345.512680000000000000
        Width = 728.701258020833300000
        object Page: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 648.701258020833300000
          Width = 80.000000000000000000
          Height = 16.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8 = (
            '[Page#]')
          ParentFont = False
        end
      end
      object MasterData10: TfrxMasterData
        FillType = ftBrush
        Frame.Typ = []
        Height = 22.677180000000000000
        Top = 374.173470000000000000
        Width = 728.701258020833300000
        DataSet = frxDBDataset_LinkLine_links
        DataSetName = 'LinkLine_links'
        RowCount = 0
        object Memo162: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Width = 496.929738960000000000
          Height = 22.677180000000000000
          DataSet = frxDBDataset_LinkLine
          DataSetName = 'LinkLine'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            '[LinkLine_links."name"]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object Header3: TfrxHeader
        FillType = ftBrush
        Frame.Typ = []
        Height = 22.677180000000000000
        Top = 419.527830000000000000
        Width = 728.701258020833300000
      end
    end
    object Subreport_left_: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
    end
    object Page2: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      object MasterData3: TfrxMasterData
        FillType = ftBrush
        Frame.Typ = []
        Height = 147.000000000000000000
        Top = 18.897650000000000000
        Width = 718.110700000000000000
        DataSet = dmLink_report.frxDBDataset_Property1
        DataSetName = 'Site1'
        RowCount = 0
        object Memo43: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1119#1056#187#1056#1109#1057#8240#1056#176#1056#1169#1056#1108#1056#176)
          ParentFont = False
        end
        object Memo_left_1: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 110.000000000000000000
          Width = 125.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Site1."name"]')
          ParentFont = False
        end
        object Memo66: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 15.000000000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1113#1056#1109#1056#1109#1057#1026#1056#1169#1056#1105#1056#1029#1056#176#1057#8218#1057#8249' '#1056#1038#1056#1113'-42')
          ParentFont = False
        end
        object Memo68: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 45.000000000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1113#1056#1109#1056#1109#1057#1026#1056#1169#1056#1105#1056#1029#1056#176#1057#8218#1057#8249' WGS')
          ParentFont = False
        end
        object Memo70: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 75.000000000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#8217#1057#8249#1057#1027#1056#1109#1057#8218#1056#176' '#1056#1111#1056#1109#1056#1169#1056#1030#1056#181#1057#1027#1056#176', m')
          ParentFont = False
        end
        object Memo72: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 90.000000000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1106#1056#183#1056#1105#1056#1112#1057#1107#1057#8218)
          ParentFont = False
        end
        object Memo74: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 105.000000000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1032#1056#1110#1056#1109#1056#187' '#1056#1112#1056#181#1057#1027#1057#8218#1056#176)
          ParentFont = False
        end
        object Memo100: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 120.000000000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#8221#1056#1105#1056#176#1056#1112#1056#181#1057#8218#1057#1026' '#1056#176#1056#1029#1057#8218#1056#181#1056#1029#1056#1029#1057#8249)
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 110.000000000000000000
          Top = 15.000000000000000000
          Width = 125.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[DegreeToStr (<Site1."lat">)]')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 110.000000000000000000
          Top = 45.000000000000000000
          Width = 125.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[DegreeToStr (<Site1."lat_WGS">)]')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 110.000000000000000000
          Top = 75.000000000000000000
          Width = 125.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[antenna1."height"]')
          ParentFont = False
        end
        object Memo95: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 110.000000000000000000
          Top = 90.000000000000000000
          Width = 125.000000000000000000
          Height = 15.000000000000000000
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."azimuth1"]')
          ParentFont = False
        end
        object Memo97: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 110.000000000000000000
          Top = 120.000000000000000000
          Width = 125.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[antenna1."diameter"]')
          ParentFont = False
        end
        object Memo98: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 110.000000000000000000
          Top = 60.000000000000000000
          Width = 125.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[DegreeToStr (<Site1."lon_WGS">)]')
          ParentFont = False
        end
        object Memo99: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 110.000000000000000000
          Top = 30.000000000000000000
          Width = 125.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[DegreeToStr (<Site1."lon">)]')
          ParentFont = False
        end
        object Memo96: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 110.000000000000000000
          Top = 105.000000000000000000
          Width = 125.000000000000000000
          Height = 15.000000000000000000
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."ELEVATION_angle_1"]')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 30.000000000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          ParentFont = False
        end
        object Memo121: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 60.000000000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          ParentFont = False
        end
      end
    end
    object Page3: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      object MasterData4: TfrxMasterData
        FillType = ftBrush
        Frame.Typ = []
        Height = 144.000000000000000000
        Top = 18.897650000000000000
        Width = 718.110700000000000000
        DataSet = dmLink_report.frxDBDataset_Property1
        DataSetName = 'Site1'
        RowCount = 0
        object Memo13: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 15.000000000000000000
          Width = 160.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#8221#1056#187#1056#1105#1056#1029#1056#176' '#1056#1105#1056#1029#1057#8218#1056#181#1057#1026#1056#1030#1056#176#1056#187#1056#176', km')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 30.000000000000000000
          Width = 160.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1032#1057#1026#1056#1109#1056#1030#1056#181#1056#1029#1057#1034' '#1057#1027#1056#1105#1056#1110#1056#1029#1056#176#1056#187#1056#176', dBm')
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 45.000000000000000000
          Width = 160.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            'P'#1056#176#1056#177#1056#1109#1057#8225#1056#176#1057#1039' '#1057#8225#1056#176#1057#1027#1057#8218#1056#1109#1057#8218#1056#176', GHz')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 60.000000000000000000
          Width = 160.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1114#1056#1109#1057#8240#1056#1029#1056#1109#1057#1027#1057#8218#1057#1034' '#1056#1111#1056#181#1057#1026#1056#181#1056#1169#1056#176#1057#8218#1057#8225#1056#1105#1056#1108#1056#176', dBm')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 75.000000000000000000
          Width = 160.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1038#1056#1108#1056#1109#1057#1026#1056#1109#1057#1027#1057#8218#1057#1034' '#1056#1111#1056#181#1057#1026#1056#181#1056#1169#1056#176#1057#8225#1056#1105', Mbit/s')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 90.000000000000000000
          Width = 160.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1118#1056#1105#1056#1111' '#1056#1109#1056#177#1056#1109#1057#1026#1057#1107#1056#1169#1056#1109#1056#1030#1056#176#1056#1029#1056#1105#1057#1039)
          ParentFont = False
        end
        object Memo123: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 160.000000000000000000
          Top = 30.000000000000000000
          Width = 75.590551181102360000
          Height = 15.000000000000000000
          DataField = 'rx_level_dBm'
          DataSet = dmLink_report.frxDBDataset_LinkEnd1
          DataSetName = 'LinkEnd1'
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd1."rx_level_dBm"]')
          ParentFont = False
        end
        object Memo124: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 160.000000000000000000
          Top = 75.000000000000000000
          Width = 75.590551181102360000
          Height = 15.000000000000000000
          DataField = 'bitrate_Mbps'
          DataSet = dmLink_report.frxDBDataset_LinkEnd1
          DataSetName = 'LinkEnd1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd1."bitrate_Mbps"]')
          ParentFont = False
        end
        object Memo125: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 160.000000000000000000
          Top = 90.000000000000000000
          Width = 75.590551181102360000
          Height = 15.000000000000000000
          DataField = 'LinkEndType_name'
          DataSet = dmLink_report.frxDBDataset_LinkEnd1
          DataSetName = 'LinkEnd1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd1."LinkEndType_name"]')
          ParentFont = False
        end
        object Memo127: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 160.000000000000000000
          Top = 45.000000000000000000
          Width = 75.590551181102360000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[FloatToStr( <LinkEnd1."tx_freq_MHz"> / 1000 )]')
          ParentFont = False
        end
        object Memo128: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 160.000000000000000000
          Top = 15.000000000000000000
          Width = 75.590551181102360000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."length_km"]')
          ParentFont = False
        end
        object Memo126: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 160.000000000000000000
          Top = 60.000000000000000000
          Width = 75.590551181102360000
          Height = 15.000000000000000000
          DataField = 'power_dBm'
          DataSet = dmLink_report.frxDBDataset_LinkEnd1
          DataSetName = 'LinkEnd1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd1."power_dBm"]')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Width = 160.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1119#1057#1026#1056#1105#1056#183#1056#1029#1056#176#1056#1108' '#1056#1111#1057#1026#1056#1105#1056#1110#1056#1109#1056#1169#1056#1029#1056#1109#1057#1027#1057#8218#1056#1105)
          ParentFont = False
        end
        object Memo122: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 160.000000000000000000
          Width = 75.590551180000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."status_str"]')
          ParentFont = False
        end
      end
    end
    object Page4: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      object MasterData5: TfrxMasterData
        FillType = ftBrush
        Frame.Typ = []
        Height = 140.000000000000000000
        Top = 18.897650000000000000
        Width = 718.110700000000000000
        DataSet = dmLink_report.frxDBDataset_Property1
        DataSetName = 'Site1'
        RowCount = 0
        object Memo105: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 478.110700000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1119#1056#187#1056#1109#1057#8240#1056#176#1056#1169#1056#1108#1056#176)
          ParentFont = False
        end
        object Memo106: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 588.110700000000000000
          Width = 130.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Site2."name"]')
          ParentFont = False
        end
        object Memo107: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 478.110700000000000000
          Top = 15.000000000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1113#1056#1109#1056#1109#1057#1026#1056#1169#1056#1105#1056#1029#1056#176#1057#8218#1057#8249' '#1056#1038#1056#1113'-42')
          ParentFont = False
        end
        object Memo108: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 478.110700000000000000
          Top = 45.000000000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1113#1056#1109#1056#1109#1057#1026#1056#1169#1056#1105#1056#1029#1056#176#1057#8218#1057#8249' WGS')
          ParentFont = False
        end
        object Memo109: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 478.110700000000000000
          Top = 75.000000000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#8217#1057#8249#1057#1027#1056#1109#1057#8218#1056#176' '#1056#1111#1056#1109#1056#1169#1056#1030#1056#181#1057#1027#1056#176', m')
          ParentFont = False
        end
        object Memo110: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 478.110700000000000000
          Top = 90.000000000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1106#1056#183#1056#1105#1056#1112#1057#1107#1057#8218)
          ParentFont = False
        end
        object Memo111: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 478.110700000000000000
          Top = 105.000000000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1032#1056#1110#1056#1109#1056#187' '#1056#1112#1056#181#1057#1027#1057#8218#1056#176)
          ParentFont = False
        end
        object Memo112: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 478.110700000000000000
          Top = 120.000000000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#8221#1056#1105#1056#176#1056#1112#1056#181#1057#8218#1057#1026' '#1056#176#1056#1029#1057#8218#1056#181#1056#1029#1056#1029#1057#8249)
          ParentFont = False
        end
        object Memo113: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 588.110700000000000000
          Top = 15.000000000000000000
          Width = 130.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[DegreeToStr (<Site2."lat">)]')
          ParentFont = False
        end
        object Memo114: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 588.110700000000000000
          Top = 45.000000000000000000
          Width = 130.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[DegreeToStr (<Site2."lat_WGS">)]')
          ParentFont = False
        end
        object Memo115: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 588.110700000000000000
          Top = 75.000000000000000000
          Width = 130.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[antenna2."height"]')
          ParentFont = False
        end
        object Memo116: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 588.110700000000000000
          Top = 90.000000000000000000
          Width = 130.000000000000000000
          Height = 15.000000000000000000
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."azimuth2"]')
          ParentFont = False
        end
        object Memo117: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 588.110700000000000000
          Top = 120.000000000000000000
          Width = 130.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[antenna2."diameter"]')
          ParentFont = False
        end
        object Memo118: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 588.110700000000000000
          Top = 60.000000000000000000
          Width = 130.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[DegreeToStr (<Site2."lon_WGS">)]')
          ParentFont = False
        end
        object Memo119: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 588.110700000000000000
          Top = 30.000000000000000000
          Width = 130.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[DegreeToStr (<Site2."lon">)]')
          ParentFont = False
        end
        object Memo129: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 478.110700000000000000
          Top = 30.000000000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          ParentFont = False
        end
        object Memo130: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 478.110700000000000000
          Top = 60.000000000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 588.110700000000000000
          Top = 105.000000000000000000
          Width = 130.000000000000000000
          Height = 15.000000000000000000
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."ELEVATION_angle_2"]')
          ParentFont = False
        end
      end
    end
    object Page5: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      object MasterData6: TfrxMasterData
        FillType = ftBrush
        Frame.Typ = []
        Height = 420.000000000000000000
        Top = 18.897650000000000000
        Width = 718.110700000000000000
        DataSet = dmLink_report.frxDBDataset_Property1
        DataSetName = 'Site1'
        RowCount = 0
        object Memo_1: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 15.000000000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#8217#1057#8249#1057#1027#1056#1109#1057#8218#1056#176', m')
          ParentFont = False
        end
        object Memo_2: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 30.000000000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1106#1056#183#1056#1105#1056#1112#1057#1107#1057#8218' ')
          ParentFont = False
        end
        object Memo_3: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 45.000000000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1118#1056#1105#1056#1111' '#1056#1109#1056#177#1056#1109#1057#1026#1057#1107#1056#1169#1056#1109#1056#1030#1056#176#1056#1029#1056#1105#1057#1039' ')
          ParentFont = False
        end
        object Memo25: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 60.000000000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#167#1056#176#1057#1027#1057#8218#1056#1109#1057#8218#1056#176' '#1056#1111#1056#181#1057#1026#1056#181#1056#1169#1056#176#1057#8225#1056#1105', GHz')
          ParentFont = False
        end
        object Memo26: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 75.000000000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1038#1056#1108#1056#1109#1057#1026#1056#1109#1057#1027#1057#8218#1057#1034' '#1056#1111#1056#181#1057#1026#1056#181#1056#1169#1056#176#1057#8225#1056#1105', Mbit/s')
          ParentFont = False
        end
        object Memo27: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 90.000000000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1114#1056#1109#1057#8240#1056#1029#1056#1109#1057#1027#1057#8218#1057#1034', dBm')
          ParentFont = False
        end
        object Memo28: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 105.000000000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#8221#1056#1105#1056#176#1056#1112#1056#181#1057#8218#1057#1026' '#1056#176#1056#1029#1057#8218#1056#181#1056#1029#1056#1029#1057#8249', m')
          ParentFont = False
        end
        object Memo29: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 135.000000000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            
              #1056#1038#1057#1026#1056#181#1056#1169#1056#1029#1057#1039#1057#1039' '#1056#1112#1056#1109#1057#8240#1056#1029#1056#1109#1057#1027#1057#8218#1057#1034' '#1056#1029#1056#176' '#1056#1030#1057#8230#1056#1109#1056#1169#1056#181' '#1056#1111#1057#1026#1056#1105#1056#181#1056#1112#1056#1029#1056#1105#1056#1108 +
              #1056#176', dBm')
          ParentFont = False
        end
        object Memo31: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 120.000000000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1119#1056#1109#1056#187#1057#1039#1057#1026#1056#1105#1056#183#1056#176#1057#8224#1056#1105#1057#1039' ')
          ParentFont = False
        end
        object Memo32: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 150.000000000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#160#1056#181#1056#183#1056#181#1057#1026#1056#1030#1056#1105#1057#1026#1056#1109#1056#1030#1056#176#1056#1029#1056#1105#1056#181' ')
          ParentFont = False
        end
        object Memo33: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 165.000000000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1119#1056#1109#1057#1026#1056#1109#1056#1110' '#1056#1111#1057#1026#1056#1105' BER=10-3 ')
          ParentFont = False
        end
        object Memo34: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 195.000000000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1114#1056#1105#1056#1029#1056#1105#1056#1112#1056#176#1056#187#1057#1034#1056#1029#1057#8249#1056#8470' '#1056#1109#1057#8218#1056#1029#1056#1109#1057#1027#1056#1105#1057#8218#1056#181#1056#187#1057#1034#1056#1029#1057#8249#1056#8470' '#1056#1111#1057#1026#1056#1109#1057#1027#1056#1030#1056#181#1057#8218)
          ParentFont = False
        end
        object Memo35: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 180.000000000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1119#1056#1109#1057#1026#1056#1109#1056#1110' '#1056#1111#1057#1026#1056#1105' BER=10-6 ')
          ParentFont = False
        end
        object Memo36: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 210.000000000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1119#1056#1109#1056#187#1056#1109#1056#182#1056#181#1056#1029#1056#1105#1056#181' '#1057#8218#1056#1109#1057#8225#1056#1108#1056#1105' '#1056#1111#1057#1026#1056#1109#1057#1027#1056#1030#1056#181#1057#8218#1056#176', km ')
          ParentFont = False
        end
        object Memo37: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 225.000000000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            'SESR '#1057#1027#1057#1107#1056#177#1057#1026#1056#181#1057#8222#1056#1108#1057#8224#1056#1105#1056#1109#1056#1029#1056#1029#1056#176#1057#1039' '#1057#1027#1056#1109#1057#1027#1057#8218#1056#176#1056#1030#1056#187#1057#1039#1057#1035#1057#8240#1056#176#1057#1039', % ')
          ParentFont = False
        end
        object Memo38: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 240.000000000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            'SESR '#1056#1169#1056#1109#1056#182#1056#1169#1056#181#1056#1030#1056#176#1057#1039' '#1057#1027#1056#1109#1057#1027#1057#8218#1056#176#1056#1030#1056#187#1057#1039#1057#1035#1057#8240#1056#176#1057#1039', % ')
          ParentFont = False
        end
        object Memo40: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 255.000000000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            
              'SESR '#1056#1105#1056#1029#1057#8218#1056#181#1057#1026#1057#8222#1056#181#1057#1026#1056#181#1056#1029#1057#8224#1056#1105#1056#1109#1056#1029#1056#1029#1056#176#1057#1039' '#1057#1027#1056#1109#1057#1027#1057#8218#1056#176#1056#1030#1056#187#1057#1039#1057#1035#1057#8240#1056#176#1057#1039 +
              ', % ')
          ParentFont = False
        end
        object Memo41: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 270.000000000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            
              #1056#152#1057#8218#1056#1109#1056#1110#1056#1109#1056#1030#1057#8249#1056#8470' '#1056#1111#1056#1109#1056#1108#1056#176#1056#183#1056#176#1057#8218#1056#181#1056#187#1057#1034' '#1056#1108#1056#176#1057#8225#1056#181#1057#1027#1057#8218#1056#1030#1056#176' '#1056#1111#1056#1109' '#1056#1109#1057#8364 +
              #1056#1105#1056#177#1056#1108#1056#176#1056#1112' SESR, % ')
          ParentFont = False
        end
        object Memo57: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 285.000000000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            
              #1056#1116#1056#1109#1057#1026#1056#1112#1056#1105#1057#1026#1057#1107#1056#181#1056#1112#1056#1109#1056#181' '#1056#183#1056#1029#1056#176#1057#8225#1056#181#1056#1029#1056#1105#1056#181' SESR '#1056#1029#1056#176' '#1056#1105#1056#1029#1057#8218#1056#181#1057#1026#1056#1030#1056#176 +
              #1056#187#1056#181', % ')
          ParentFont = False
        end
        object Memo58: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 300.000000000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            
              #1056#152#1057#8218#1056#1109#1056#1110#1056#1109#1056#1030#1057#8249#1056#8470' '#1056#1111#1056#1109#1056#1108#1056#176#1056#183#1056#176#1057#8218#1056#181#1056#187#1057#1034' '#1056#1029#1056#181#1056#1110#1056#1109#1057#8218#1056#1109#1056#1030#1056#1029#1056#1109#1057#1027#1057#8218#1056#1105' '#1056 +
              #1113#1056#1029#1056#1110', %')
          ParentFont = False
        end
        object Memo59: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 315.000000000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            
              #1056#1116#1056#1109#1057#1026#1056#1112#1056#1105#1057#1026#1057#1107#1056#181#1056#1112#1056#1109#1056#181' '#1056#183#1056#1029#1056#176#1057#8225#1056#181#1056#1029#1056#1105#1056#181' '#1056#1113#1056#1029#1056#1110' '#1056#1029#1056#176' '#1056#1105#1056#1029#1057#8218#1056#181#1057#1026#1056#1030 +
              #1056#176#1056#187#1056#181', % ')
          ParentFont = False
        end
        object Memo60: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 375.000000000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            'ESR '#1057#8218#1057#1026#1056#181#1056#177', % ')
          ParentFont = False
        end
        object Memo61: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 330.000000000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            'BBER '#1056#1029#1056#1109#1057#1026#1056#1112', %')
          ParentFont = False
        end
        object Memo62: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 360.000000000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            'BBER '#1057#8218#1057#1026#1056#181#1056#177', % ')
          ParentFont = False
        end
        object Memo63: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 345.000000000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            'ESR '#1056#1029#1056#1109#1057#1026#1056#1112', % ')
          ParentFont = False
        end
        object Memo64: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 390.000000000000000000
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1119#1057#1026#1056#1105#1056#1110#1056#1109#1056#1169#1056#1029#1056#1109#1057#1027#1057#8218#1057#1034' ')
          ParentFont = False
        end
        object Memo65: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 310.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Site1."name"]')
          ParentFont = False
        end
        object Memo67: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 485.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Site2."name"]')
          ParentFont = False
        end
        object Memo69: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 310.000000000000000000
          Top = 30.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."azimuth1"]')
          ParentFont = False
        end
        object Memo71: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 485.000000000000000000
          Top = 30.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."azimuth2"]')
          ParentFont = False
        end
        object LinkEnd2threshold_BER_3: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 310.000000000000000000
          Top = 165.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd1."threshold_BER_3"]')
          ParentFont = False
        end
        object LinkEnd2threshold_BER_6: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 310.000000000000000000
          Top = 180.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd1."threshold_BER_6"]')
          ParentFont = False
        end
        object Memo73: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 310.000000000000000000
          Top = 105.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          DataSet = dmLink_report.frxDBDataset_link
          DataSetName = 'Link'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[antenna1."diameter"]')
          ParentFont = False
        end
        object Memo75: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 310.000000000000000000
          Top = 120.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          DataSet = dmLink_report.frxDBDataset_link
          DataSetName = 'Link'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[antenna1."polarization"]')
          ParentFont = False
        end
        object Memo76: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 310.000000000000000000
          Top = 75.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          DataSet = dmLink_report.frxDBDataset_link
          DataSetName = 'Link'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd1."bitrate_Mbps"]')
          ParentFont = False
        end
        object Memo77: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 310.000000000000000000
          Top = 90.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd1."power_dBm"]')
          ParentFont = False
        end
        object Memo78: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 310.000000000000000000
          Top = 45.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          DataSet = dmLink_report.frxDBDataset_link
          DataSetName = 'Link'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd1."LinkEndType_name"]')
          ParentFont = False
        end
        object Memo79: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 310.000000000000000000
          Top = 60.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          DataSet = dmLink_report.frxDBDataset_link
          DataSetName = 'Link'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd1."tx_freq_MHz"]')
          ParentFont = False
        end
        object Memo80: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 485.000000000000000000
          Top = 45.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd2."LinkEndType_name"]')
          ParentFont = False
        end
        object Memo81: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 485.000000000000000000
          Top = 60.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          DataSet = dmLink_report.frxDBDataset_link
          DataSetName = 'Link'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd2."tx_freq_MHz"]')
          ParentFont = False
        end
        object Memo82: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 485.000000000000000000
          Top = 75.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          DataSet = dmLink_report.frxDBDataset_link
          DataSetName = 'Link'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd2."bitrate_Mbps"]')
          ParentFont = False
        end
        object Memo83: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 485.000000000000000000
          Top = 90.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd2."power_dBm"]')
          ParentFont = False
        end
        object Memo84: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 485.000000000000000000
          Top = 165.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd2."threshold_BER_3"]')
          ParentFont = False
        end
        object Memo85: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 485.000000000000000000
          Top = 180.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd2."threshold_BER_6"]')
          ParentFont = False
        end
        object Memo86: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 310.000000000000000000
          Top = 375.000000000000000000
          Width = 350.000000000000000000
          Height = 15.000000000000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Link."ESR_REQUIRED"]')
          ParentFont = False
        end
        object Memo87: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 310.000000000000000000
          Top = 390.000000000000000000
          Width = 350.000000000000000000
          Height = 15.000000000000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Link."status_str"]')
          ParentFont = False
        end
        object Memo88: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 310.000000000000000000
          Top = 345.000000000000000000
          Width = 350.000000000000000000
          Height = 15.000000000000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Link."ESR_NORM"]')
          ParentFont = False
        end
        object Memo89: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 310.000000000000000000
          Top = 360.000000000000000000
          Width = 350.000000000000000000
          Height = 15.000000000000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Link."BBER_REQUIRED"]')
          ParentFont = False
        end
        object Memo90: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 310.000000000000000000
          Top = 315.000000000000000000
          Width = 350.000000000000000000
          Height = 15.000000000000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Link."KNG_NORM"]')
          ParentFont = False
        end
        object Memo91: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 310.000000000000000000
          Top = 330.000000000000000000
          Width = 350.000000000000000000
          Height = 15.000000000000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Link."BBER_NORM"]')
          ParentFont = False
        end
        object Memo92: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 310.000000000000000000
          Top = 285.000000000000000000
          Width = 350.000000000000000000
          Height = 15.000000000000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Link."SESR_NORM"]')
          ParentFont = False
        end
        object Memo93: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 310.000000000000000000
          Top = 300.000000000000000000
          Width = 350.000000000000000000
          Height = 15.000000000000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Link."KNG"]')
          ParentFont = False
        end
        object Memo94: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 310.000000000000000000
          Top = 255.000000000000000000
          Width = 350.000000000000000000
          Height = 15.000000000000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Link."SESR_INTERFERENCE"]')
          ParentFont = False
        end
        object Memo101: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 310.000000000000000000
          Top = 270.000000000000000000
          Width = 350.000000000000000000
          Height = 15.000000000000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Link."SESR"]')
          ParentFont = False
        end
        object Memo102: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 310.000000000000000000
          Top = 225.000000000000000000
          Width = 350.000000000000000000
          Height = 15.000000000000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Link."SESR_SUBREFRACTION"]')
          ParentFont = False
        end
        object Memo103: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 310.000000000000000000
          Top = 240.000000000000000000
          Width = 350.000000000000000000
          Height = 15.000000000000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Link."SESR_RAIN"]')
          ParentFont = False
        end
        object Memo104: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 310.000000000000000000
          Top = 210.000000000000000000
          Width = 350.000000000000000000
          Height = 15.000000000000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Link."MARGIN_DISTANCE"]')
          ParentFont = False
        end
        object Memo120: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 310.000000000000000000
          Top = 195.000000000000000000
          Width = 350.000000000000000000
          Height = 15.000000000000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd2
          DataSetName = 'LinkEnd2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            '[Link."MARGIN_HEIGHT"]')
          ParentFont = False
        end
        object Memo131: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 485.000000000000000000
          Top = 105.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          DataSet = dmLink_report.frxDBDataset_link
          DataSetName = 'Link'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[antenna2."diameter"]')
          ParentFont = False
        end
        object Memo132: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 485.000000000000000000
          Top = 120.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          DataSet = dmLink_report.frxDBDataset_link
          DataSetName = 'Link'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[antenna2."polarization"]')
          ParentFont = False
        end
        object Memo133: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 310.000000000000000000
          Top = 135.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          DataSet = dmLink_report.frxDBDataset_link
          DataSetName = 'Link'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd1."rx_level_dBm"]')
          ParentFont = False
        end
        object Memo134: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 485.000000000000000000
          Top = 135.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          DataSet = dmLink_report.frxDBDataset_link
          DataSetName = 'Link'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd2."rx_level_dBm"]')
          ParentFont = False
        end
        object Memo135: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 310.000000000000000000
          Top = 150.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          DataSet = dmLink_report.frxDBDataset_link
          DataSetName = 'Link'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd1."kratnostBySpace_name"]')
          ParentFont = False
        end
        object Memo136: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 485.000000000000000000
          Top = 150.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          DataSet = dmLink_report.frxDBDataset_link
          DataSetName = 'Link'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd2."kratnostBySpace_name"]')
          ParentFont = False
        end
        object Memo137: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Width = 310.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          ParentFont = False
        end
        object Memo138: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 310.000000000000000000
          Top = 15.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd1
          DataSetName = 'LinkEnd1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[antenna1."height"]')
          ParentFont = False
        end
        object Memo139: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 485.000000000000000000
          Top = 15.000000000000000000
          Width = 175.000000000000000000
          Height = 15.000000000000000000
          DataSet = dmLink_report.frxDBDataset_LinkEnd1
          DataSetName = 'LinkEnd1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[antenna2."height"]')
          ParentFont = False
        end
      end
    end
    object Page6: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      object MasterData8: TfrxMasterData
        FillType = ftBrush
        Frame.Typ = []
        Height = 23.559060000000000000
        Top = 64.252010000000000000
        Width = 718.110700000000000000
        DataSet = dmLink_report.frxDBDataset_group
        DataSetName = 'group'
        RowCount = 0
        object groupcaption: TfrxMemoView
          Align = baWidth
          AllowVectorExport = True
          Left = 30.000000000000000000
          Top = 3.779530000000000000
          Width = 688.110700000000000000
          Height = 16.000000000000000000
          DataField = 'caption'
          DataSet = dmLink_report.frxDBDataset_group
          DataSetName = 'group'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[group."caption"]')
          ParentFont = False
        end
        object Memo159: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 3.779530000000000000
          Width = 30.000000000000000000
          Height = 16.000000000000000000
          Visible = False
          DataField = 'id'
          DataSet = dmLink_report.frxDBDataset_group
          DataSetName = 'group'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[group."id"]')
          ParentFont = False
        end
      end
      object DetailData1: TfrxDetailData
        FillType = ftBrush
        Frame.Typ = []
        Height = 16.000000000000000000
        Top = 109.606370000000000000
        Width = 718.110700000000000000
        DataSet = dmLink_report.frxDBDataset_Items
        DataSetName = 'Items'
        RowCount = 0
        object Itemscaption: TfrxMemoView
          Align = baWidth
          AllowVectorExport = True
          Left = 30.000000000000000000
          Width = 508.110700000000000000
          Height = 16.000000000000000000
          DataField = 'caption'
          DataSet = dmLink_report.frxDBDataset_Items
          DataSetName = 'Items'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Items."caption"]')
          ParentFont = False
        end
        object Memo156: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 538.110700000000000000
          Width = 90.000000000000000000
          Height = 16.000000000000000000
          DataField = 'value'
          DataSet = dmLink_report.frxDBDataset_Items
          DataSetName = 'Items'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Items."value"]')
          ParentFont = False
        end
        object Memo157: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 628.110700000000000000
          Width = 90.000000000000000000
          Height = 16.000000000000000000
          DataField = 'value2'
          DataSet = dmLink_report.frxDBDataset_Items
          DataSetName = 'Items'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Items."value2"]')
          ParentFont = False
        end
        object Memo158: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Width = 30.000000000000000000
          Height = 16.000000000000000000
          DataSet = dmLink_report.frxDBDataset_Items
          DataSetName = 'Items'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Items."code"]')
          ParentFont = False
        end
      end
      object Header2: TfrxHeader
        FillType = ftBrush
        Frame.Typ = []
        Height = 24.000000000000000000
        Top = 18.897650000000000000
        Width = 718.110700000000000000
        object Memo160: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Width = 434.000000000000000000
          Height = 16.666666670000000000
          AutoWidth = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8 = (
            #1056#160#1056#181#1056#183#1057#1107#1056#187#1057#1034#1057#8218#1056#176#1057#8218#1057#8249' '#1057#1026#1056#176#1057#1027#1057#8225#1056#181#1057#8218#1056#1109#1056#1030)
          ParentFont = False
        end
      end
      object Footer1: TfrxFooter
        FillType = ftBrush
        Frame.Typ = []
        Height = 20.000000000000000000
        Top = 147.401670000000000000
        Width = 718.110700000000000000
      end
    end
    object Page7: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      object MasterData7: TfrxMasterData
        FillType = ftBrush
        Frame.Typ = []
        Height = 16.000000000000000000
        Top = 75.590600000000000000
        Width = 718.110700000000000000
        DataSet = dmLink_report.frxDBDataset_Profile_items
        DataSetName = 'Profile'
        RowCount = 0
        object Memo150: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 60.000000000000000000
          Width = 100.000000000000000000
          Height = 16.000000000000000000
          DataField = 'distance_km'
          DataSet = dmLink_report.frxDBDataset_Profile_items
          DataSetName = 'Profile'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Profile."distance_km"]')
          ParentFont = False
        end
        object Memo151: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Width = 60.000000000000000000
          Height = 16.000000000000000000
          Visible = False
          DataField = 'index'
          DataSet = dmLink_report.frxDBDataset_Profile_items
          DataSetName = 'Profile'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Profile."index"]')
          ParentFont = False
        end
        object Memo152: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 260.000000000000000000
          Width = 100.000000000000000000
          Height = 16.000000000000000000
          DataField = 'earth_h'
          DataSet = dmLink_report.frxDBDataset_Profile_items
          DataSetName = 'Profile'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Profile."earth_h"]')
          ParentFont = False
        end
        object Memo153: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 160.000000000000000000
          Width = 100.000000000000000000
          Height = 16.000000000000000000
          OnBeforePrint = 'Memo92OnBeforePrint'
          DataField = 'ground_h'
          DataSet = dmLink_report.frxDBDataset_Profile_items
          DataSetName = 'Profile'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Profile."ground_h"]')
          ParentFont = False
        end
        object Memo154: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 360.000000000000000000
          Width = 100.000000000000000000
          Height = 16.000000000000000000
          DataField = 'clutter_h'
          DataSet = dmLink_report.frxDBDataset_Profile_items
          DataSetName = 'Profile'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Profile."clutter_h"]')
          ParentFont = False
        end
        object Shape_Color: TfrxShapeView
          Align = baLeft
          AllowVectorExport = True
          Left = 460.000000000000000000
          Width = 20.000000000000000000
          Height = 16.000000000000000000
          OnBeforePrint = 'Shape_ColorOnBeforePrint'
          Fill.BackColor = clTeal
          Frame.Typ = []
        end
        object Memo155: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 480.000000000000000000
          Width = 176.000000000000000000
          Height = 16.000000000000000000
          DataField = 'clutter_name'
          DataSet = dmLink_report.frxDBDataset_Profile_items
          DataSetName = 'Profile'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Profile."clutter_name"]')
          ParentFont = False
        end
      end
      object Header1: TfrxHeader
        FillType = ftBrush
        Frame.Typ = []
        Height = 35.000000000000000000
        Top = 18.897650000000000000
        Width = 718.110700000000000000
        object Memo144: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Width = 60.000000000000000000
          Height = 35.000000000000000000
          Visible = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            'Index')
          ParentFont = False
        end
        object Memo145: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 60.000000000000000000
          Width = 100.000000000000000000
          Height = 35.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#160#1056#176#1057#1027#1057#1027#1057#8218#1056#1109#1057#1039#1056#1029#1056#1105#1056#181', '#1056#1108#1056#1112)
          ParentFont = False
        end
        object Memo146: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 260.000000000000000000
          Width = 100.000000000000000000
          Height = 35.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1113#1057#1026#1056#1105#1056#1030#1056#1105#1056#183#1056#1029#1056#176' '#1056#183#1056#181#1056#1112#1056#187#1056#1105)
          ParentFont = False
        end
        object Memo147: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 360.000000000000000000
          Width = 100.000000000000000000
          Height = 35.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#8217#1057#8249#1057#1027#1056#1109#1057#8218#1056#176' '#1056#1114#1056#1119)
          ParentFont = False
        end
        object Memo148: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 160.000000000000000000
          Width = 100.000000000000000000
          Height = 35.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#8217#1057#8249#1057#1027#1056#1109#1057#8218#1056#176' '#1057#1026#1056#181#1056#187#1057#1034#1056#181#1057#8222#1056#176)
          ParentFont = False
        end
        object Memo149: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 460.000000000000000000
          Width = 196.000000000000000000
          Height = 35.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1118#1056#1105#1056#1111' '#1056#1114#1056#1119)
          ParentFont = False
        end
      end
    end
  end
  object ADOConnection1: TADOConnection
    ConnectionString = 
      'Provider=MSDASQL.1;Persist Security Info=False;User ID=sa;Data S' +
      'ource=server_onega_link1'
    LoginPrompt = False
    Provider = 'MSDASQL.1'
    Left = 64
    Top = 16
  end
  object frxDBDataset_LinkLine_img: TfrxDBDataset
    UserName = 'LinkLine_img'
    CloseDataSource = False
    FieldAliases.Strings = (
      'map_img=map_img')
    BCDToCurrency = False
    Left = 380
    Top = 260
  end
  object frxRTFExport1: TfrxRTFExport
    UseFileCache = True
    ShowProgress = True
    OverwritePrompt = False
    DataOnly = False
    PictureType = gpJPG
    OpenAfterExport = False
    Wysiwyg = True
    Creator = 'FastReport'
    SuppressPageHeadersFooters = False
    HeaderFooterMode = hfText
    AutoSize = False
    Left = 328
    Top = 96
  end
  object frxXLSExport1: TfrxXLSExport
    UseFileCache = True
    ShowProgress = True
    OverwritePrompt = False
    DataOnly = False
    ExportEMF = True
    AsText = False
    Background = True
    FastExport = True
    PageBreaks = True
    EmptyLines = True
    SuppressPageHeadersFooters = False
    Left = 408
    Top = 24
  end
  object frxPDFExport1: TfrxPDFExport
    UseFileCache = True
    ShowProgress = True
    OverwritePrompt = False
    DataOnly = False
    OpenAfterExport = False
    PrintOptimized = False
    Outline = False
    Background = False
    HTMLTags = True
    Quality = 95
    Transparency = False
    Author = 'FastReport'
    Subject = 'FastReport PDF export'
    ProtectionFlags = [ePrint, eModify, eCopy, eAnnot]
    HideToolbar = False
    HideMenubar = False
    HideWindowUI = False
    FitWindow = False
    CenterWindow = False
    PrintScaling = False
    PdfA = False
    Left = 304
    Top = 24
  end
  object ds_LinkLine: TDataSource
    DataSet = qry_LinkLine
    Left = 56
    Top = 200
  end
  object frxDesigner1: TfrxDesigner
    DefaultScriptLanguage = 'PascalScript'
    DefaultFont.Charset = DEFAULT_CHARSET
    DefaultFont.Color = clWindowText
    DefaultFont.Height = -13
    DefaultFont.Name = 'Arial'
    DefaultFont.Style = []
    DefaultLeftMargin = 10.000000000000000000
    DefaultRightMargin = 10.000000000000000000
    DefaultTopMargin = 10.000000000000000000
    DefaultBottomMargin = 10.000000000000000000
    DefaultPaperSize = 9
    DefaultOrientation = poPortrait
    TemplatesExt = 'fr3'
    Restrictions = []
    RTLLanguage = False
    MemoParentFont = False
    Left = 195
    Top = 72
  end
  object qry_LinkLine_links: TADOQuery
    LockType = ltBatchOptimistic
    DataSource = ds_LinkLine
    Parameters = <>
    Left = 200
    Top = 184
  end
  object frxDBDataset_LinkLine_links: TfrxDBDataset
    UserName = 'LinkLine_links'
    CloseDataSource = False
    FieldAliases.Strings = (
      'id=id'
      'name=name'
      'project_id=project_id'
      'folder_id=folder_id'
      'length=length'
      'length_km=length_km'
      '_=_'
      'SESR=SESR'
      'Kng=Kng'
      'Kng_year=Kng_year'
      'SESR_required=SESR_required'
      'KNG_required=KNG_required'
      'ESR_required=ESR_required'
      'BBER_required=BBER_required'
      'ESR_norm=ESR_norm'
      'BBER_norm=BBER_norm'
      'bitrate_Mbps=bitrate_Mbps'
      '_____=_____'
      'GST_TYPE=GST_TYPE'
      'GST_length=GST_length'
      'GST_SESR=GST_SESR'
      'GST_KNG=GST_KNG'
      'comment_norm=comment_norm'
      'status=status'
      'comment=comment'
      '__=__'
      'guid=guid'
      'date_created=date_created'
      'date_modify=date_modify'
      'user_created=user_created'
      'user_modify=user_modify'
      'GST_type_name=GST_type_name'
      'status_name=status_name'
      'status_str=status_str')
    DataSet = qry_LinkLine_links
    BCDToCurrency = False
    Left = 201
    Top = 260
  end
end
