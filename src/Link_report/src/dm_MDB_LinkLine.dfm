object dmMDB_LinkLine: TdmMDB_LinkLine
  OldCreateOrder = True
  OnCreate = DataModuleCreate
  Left = 1310
  Top = 681
  Height = 338
  Width = 811
  object ADOConnection_MDB: TADOConnection
    ConnectionString = 
      'Provider=Microsoft.Jet.OLEDB.4.0;Data Source=D:\1.mdb;Persist Se' +
      'curity Info=False;'
    LoginPrompt = False
    Mode = cmShareDenyNone
    Provider = 'Microsoft.Jet.OLEDB.4.0'
    Left = 60
    Top = 12
  end
  object qry_LinkLine: TADOQuery
    Parameters = <>
    Left = 288
    Top = 32
  end
  object qry_LinkLine_links: TADOQuery
    Parameters = <>
    Left = 432
    Top = 32
  end
  object t_LinkLine: TADOTable
    Connection = ADOConnection_MDB
    CursorType = ctStatic
    TableName = 'Antennas1'
    Left = 640
    Top = 40
  end
  object t_LinkLine_links: TADOTable
    Connection = ADOConnection_MDB
    CursorType = ctStatic
    TableName = 'Antennas2'
    Left = 632
    Top = 120
  end
  object ClientDataSet_linkline_img: TClientDataSet
    Active = True
    Aggregates = <>
    Params = <>
    Left = 304
    Top = 200
    Data = {
      410000009619E0BD0100000018000000010000000000030000004100076D6170
      5F696D6704004B00000001000753554254595045020049000900477261706869
      6373000000}
    object ClientDataSet_linkline_imgmap_img: TGraphicField
      FieldName = 'map_img'
      BlobType = ftGraphic
    end
  end
end
