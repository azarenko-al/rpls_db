unit u_report_classes;

interface

uses
  Classes, XMLIntf, SysUtils,

  u_xml_document;

type

  TCalcResultItem = class(TCollectionItem)
  public
    Caption : string;
    VALUE   : string;

  end;

  TCalcResultItemList = class(TCollection)
  private
  //  function AddItem: TCalcResultItem;
  public
    constructor Create;

    function AddItem(aCaption : string; aVALUE : string): TCalcResultItem;
  end;

  // ---------------------------------------------------------------
  TReport1 = class(TObject)
  // ---------------------------------------------------------------
  private
    procedure AddCalcResults(aNode: IXMLNode);
    procedure AddProfile(aNode: IXMLNode);
    procedure SaveToXml;
  public
    CalcResultItemList: TCalcResultItemList;

    Relief_Profile: array of
        record
           Distance_KM  : Double;
           rel_h        : Double;
           Clutter_h    : Double;
           Clutter_Code : smallint;
           earth_h      : Double; //test
        end;


    constructor Create;
    destructor Destroy; override;

  end;

implementation


// ---------------------------------------------------------------
procedure TReport1.SaveToXml;
// ---------------------------------------------------------------
var
  oXMLDoc: TXmlDocumentEx;
begin
  oXMLDoc := TXmlDocumentEx.Create();

  AddCalcResults(oXMLDoc.DocumentElement);
  AddProfile(oXMLDoc.DocumentElement);

  FreeAndNil(oXMLDoc);

  // TODO -cMM: TReport1.SaveToXml default body inserted
end;

constructor TCalcResultItemList.Create;
begin
  inherited Create(TCalcResultItem);
end;

 
function TCalcResultItemList.AddItem(aCaption : string; aVALUE : string):
    TCalcResultItem;
begin
  Result := TCalcResultItem (inherited Add);
  Result.Caption :=aCaption;
  Result.Value   :=aVALUE;  
end;

constructor TReport1.Create;
begin
  inherited Create;
  CalcResultItemList := TCalcResultItemList.Create();
end;

destructor TReport1.Destroy;
begin
  FreeAndNil(CalcResultItemList);
  inherited Destroy;
end;

//--------------------------------------------------------------------
procedure TReport1.AddProfile(aNode: IXMLNode);
//--------------------------------------------------------------------
const
  TAG_PROFILE = 'PROFILE';
  TAG_ITEM = 'ITEM';

var i: integer;
  vNode: IXMLNode;
begin
  vNode:=xml_AddNodeTag (aNode, TAG_PROFILE);

//  dmLink_report.relProfile.SaveToXmlFile('e:\111111.xml');

(*
  with dmLink_report.relProfile do
     for i:=0 to Count-1 do
       xml_AddNode (vNode, TAG_ITEM,
          [//xml_ATT ('R',          TruncFloat(Data.Items[i].Distance,3) ),
           xml_ATT ('distance_km',   TruncFloat(Items[i].Distance_KM,3) ),
           xml_ATT ('ground_h',     Items[i].Rel_H ),
           xml_ATT ('clutter_h',    Items[i].Clutter_H ),
           xml_ATT ('clutter_code', Items[i].Clutter_Code),
           xml_ATT ('earth_h',      TruncFloat(Items[i].Earth_H)),

           xml_ATT ('clutter_color',ColorToHEX(GetClutterColorByCode(Items[i].Clutter_Code))),
//           xml_ATT ('clutter_color_HEX',GetClutterColorByCode(Data.Items[i].Clutter_Code)),
           xml_ATT ('clutter_name', GetClutterNameByCode(Items[i].Clutter_Code))
      ]);
*)

end;
         
//--------------------------------------------------------------------
procedure TReport1.AddCalcResults(aNode: IXMLNode);
//--------------------------------------------------------------------
var
  I: Integer;
  sCaption: string;
  vNode, vNode1, vGroup: IXMLNode;
begin
  vNode := xml_AddNodeTag(aNode,'CALC_RESULTS');

(*
  for I := 0 to CalcResultItemList.Count - 1 do
    xml_AddNode(vNode, TAG_ITEM,
                    [
                    //xml_att(ATT_ID,      0),
                  //   xml_att(ATT_IS_GROUP, FieldValues[FLD_IS_GROUP]),
                     xml_att(ATT_caption, CalcResultItemList[i].),
                     xml_att(ATT_VALUE,   CalcResultItemList[i])
                    ]);
*)

 (*
  dmLink_report_data.mem_Calc_Results.First;

  with dmLink_report_data.mem_Calc_Results do
     while not Eof do
  begin
    sCaption:=FieldValues[FLD_NAME];

    if not FieldBYName(FLD_IS_GROUP).AsBoolean then
      sCaption:='-  -'+ sCaption;

    xml_AddNode(vNode, TAG_ITEM,
                    [
                    //xml_att(ATT_ID,      0),
                     xml_att(ATT_IS_GROUP, FieldValues[FLD_IS_GROUP]),
                     xml_att(ATT_caption, sCaption),
                     xml_att(ATT_VALUE,   FieldValues[FLD_VALUE])
                    ]);

    Next;
  end;
*)
end;




end.



   {


//--------------------------------------------------------------------
procedure TdmLink_report_xml.AddCalcResults (aNode: IXMLNode);
//--------------------------------------------------------------------
var
  sCaption: string;
  vNode, vNode1, vGroup: IXMLNode;
begin
  vNode := xml_AddNodeTag(aNode,'CALC_RESULTS');

  dmLink_report_data.mem_Calc_Results.First;

  with dmLink_report_data.mem_Calc_Results do
     while not Eof do
  begin
    sCaption:=FieldValues[FLD_NAME];

    if not FieldBYName(FLD_IS_GROUP).AsBoolean then
      sCaption:='-  -'+ sCaption;

    xml_AddNode(vNode, TAG_ITEM,
                    [
                    //xml_att(ATT_ID,      0),
                     xml_att(ATT_IS_GROUP, FieldValues[FLD_IS_GROUP]),
                     xml_att(ATT_caption, sCaption),
                     xml_att(ATT_VALUE,   FieldValues[FLD_VALUE])
                    ]);

    Next;
  end;




 { qry_LinkCalcResults_Group.First;

  with qry_LinkCalcResults_Group do     while not Eof do
  begin
    if Eq(qry_LinkCalcResults_Group[FLD_CAPTION], '�������� ������ (XML)') then
    begin
      Next;
      Continue;
    end;

      vGroup:=xml_AddNode(vNode, TAG_GROUP,
             [xml_Att(ATT_CAPTION, qry_LinkCalcResults_Group[FLD_CAPTION])]);

      db_OpenQuery (qry_LinkCalcResults_Item, SQL_SELECT_CALC_RES_ITEM,
        [db_Par(FLD_LINK_ID,   qry_LinkCalcResults_Group[FLD_LINK_ID]),
         db_Par(FLD_PARENT_ID, qry_LinkCalcResults_Group[FLD_ID])]);

      DoAddItems(vGroup, qry_LinkCalcResults_Item);
      Next;
    end;}

end;



 {
//--------------------------------------------------------------------
procedure TdmLink_report_xml.AddProfile (aNode: IXMLNode);
//--------------------------------------------------------------------
var i: integer;
  vNode: IXMLNode;
begin
  vNode:=xml_AddNodeTag (aNode, TAG_PROFILE);

//  dmLink_report.relProfile.SaveToXmlFile('e:\111111.xml');


  with dmLink_report.relProfile do
     for i:=0 to Count-1 do
       xml_AddNode (vNode, TAG_ITEM,
          [//xml_ATT ('R',          TruncFloat(Data.Items[i].Distance,3) ),
           xml_ATT ('distance_km',   TruncFloat(Items[i].Distance_KM,3) ),
           xml_ATT ('ground_h',     Items[i].Rel_H ),
           xml_ATT ('clutter_h',    Items[i].Clutter_H ),
           xml_ATT ('clutter_code', Items[i].Clutter_Code),
           xml_ATT ('earth_h',      TruncFloat(Items[i].Earth_H)),

           xml_ATT ('clutter_color',ColorToHEX(GetClutterColorByCode(Items[i].Clutter_Code))),
//           xml_ATT ('clutter_color_HEX',GetClutterColorByCode(Data.Items[i].Clutter_Code)),
           xml_ATT ('clutter_name', GetClutterNameByCode(Items[i].Clutter_Code))
      ]);


end;

 Length_KM: Double; //����� ���������

//      Count : Integer;
    //  Items1: array[0..20000] of //packed

      Items: array of //packed
        record
           Distance_KM  : Double;
           rel_h        : Double;
           Clutter_h    : Double;
           Clutter_Code : smallint;

           earth_h      : Double;//test
        end;