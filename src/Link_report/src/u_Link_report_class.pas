unit u_Link_report_class;

interface

uses
  SysUtils, Classes,

  u_link_calc_classes_main,

  u_reflection_points,

  u_Func_arrays,
  u_func,
  u_const_db,

  u_geo,

  DB;

type
  TdmLinkType = (ltLink,ltPmpLink);

(*
  TRelReflectionPointRecArray1111 = array of
  record
     Distance_KM: Double;
     Length_KM: Double;
     Radius_KM: Double;
     abs_prosvet_m: double;
  end;

*)
//  TDBLinkRepeater = class;

 // ---------------------------------------------------------------
  TProperty = class
  // ---------------------------------------------------------------
  public
    ID : Integer;
    BLPoint: TBLPoint;

   // Ground_H           : double;

    Ground_Height      : Double;
   //eight_with_ground : Double;

    procedure LoadFromDataset(aDataSet: TDataSet);

  end;


  // ---------------------------------------------------------------
  TAntenna = class(TCollectionItem)
  // ---------------------------------------------------------------
  public
    Ground_Height      : Double;
    Height_with_ground : Double;

    Height             : double;
    Loss               : double;
    Gain               : double;
    Diameter           : Double;

    POLARIZATION_STR   : string;
  end;

  // ---------------------------------------------------------------
  TAntennaList = class(TCollection)
  // ---------------------------------------------------------------
  private
    function GetItems(Index: Integer): TAntenna;
    function GetValue(aParam: string): string; // ?????



  public
    constructor Create;
    function AddItem: TAntenna;

    function GetMaxHeight_with_ground: double;
    function GetAntennaHeightArr: u_Func.TDoubleArray;

    function GetGainStr: string;
    function GetHeightStr: string;
    function GetDiameterStr: string;
    function GetPOLARIZATIONStr: string;


    procedure LoadFromDataset(aDataSet: TDataSet; aGround_Height: Double);

    property Items[Index: Integer]: TAntenna read GetItems; default;

  end;


  TLinkData = class(TObject)
  private
  public
    Property1: TProperty;
    Property2: TProperty;


//    LinkType: TdmLinkType;
//    is_profile_reversed : Boolean;  //����������� ���������� �������

    BLVector: TBLVector;     //updated for CalcDirection

    Length_KM : double;

    PROFILE_Step_M: integer; //��� ��������� �������


    Refraction : double;

    ID: integer;

    ClutterModel_ID: integer;

//    PmpSectorID: Integer;
//    PmpTerminalID: integer;

    // linkends ---------------------------
    LinkEndID1: Integer;
    LinkEndID2: integer;

    PropertyID1: Integer;
    PropertyID2: Integer;

(*    Property1_Ground_Height1: Variant;
    Property2_Ground_Height1: Variant;
*)

    Property1_Pos: TBLPoint;
    Property2_Pos: TBLPoint;


    TX_FREQ_MHz : double;

    Name : string;


    Profile_XML: string;

    CalcResultsXML : string;

//     db_Par(FLD_CALC_RESULTS_XML,    sCalcResultsXML)

  public
    LinkRepeater: TDBLinkRepeater;

    ReflectionPointsArray : TRelReflectionPointRecArray;

   // LinkEnd_Antennas: array[0..1] of  TAntennaList;

    LinkEnd1_Antennas: TAntennaList;
    LinkEnd2_Antennas: TAntennaList;


    constructor Create;
    destructor Destroy; override;

    function GetAntennaHeightArr(aDataSet: TDataSet): u_Func.TDoubleArray;

    procedure LinkRepeater_LoadFromDataset(aDataset: TDataset);
    procedure LoadFromDataset_ReflectionPoints(aDataset: TDataset);

    procedure LoadFromDataset(aDataset: TDataset);
  end;



implementation


constructor TLinkData.Create;
begin
  inherited Create;
  LinkRepeater := TDBLinkRepeater.Create();

(*  LinkEnd_Antennas[0] := TAntennaList.Create();
  LinkEnd_Antennas[1] := TAntennaList.Create();
*)
  LinkEnd1_Antennas := TAntennaList.Create();

  LinkEnd2_Antennas := TAntennaList.Create();
  Property1 := TProperty.Create();
  Property2 := TProperty.Create();
end;

destructor TLinkData.Destroy;
begin
(*  FreeAndNil(LinkEnd_Antennas[0]);
  FreeAndNil(LinkEnd_Antennas[1]);
*)

  FreeAndNil(Property2);
  FreeAndNil(Property1);
  FreeAndNil(LinkEnd2_Antennas);
  FreeAndNil(LinkEnd1_Antennas);
  FreeAndNil(LinkRepeater);
  inherited Destroy;
end;

// ---------------------------------------------------------------
procedure TLinkData.LinkRepeater_LoadFromDataset(aDataset: TDataset);
// ---------------------------------------------------------------
begin
  LinkRepeater.LoadFromDataset(aDataset);


  LinkRepeater.Part1.BLVector := MakeBLVector(Property1.BLPoint, LinkRepeater.Property_Pos);
  LinkRepeater.Part2.BLVector := MakeBLVector(LinkRepeater.Property_Pos, Property2.BLPoint);


  LinkRepeater.Part1.Length_KM  := geo_Distance_km(LinkRepeater.Part1.BLVector);
  LinkRepeater.Part2.Length_KM  := geo_Distance_km(LinkRepeater.Part2.BLVector);


{  LinkRepeater.sdscalc_results_xml_1:=FieldByName(FLD_calc_results_xml_1).AsString;
  LinkRepeater.calc_results_xml_2:=FieldByName(FLD_calc_results_xml_2).AsString;
  LinkRepeater.calc_results_xml_3:=FieldByName(FLD_calc_results_xml_3).AsString;
}

end;

// ---------------------------------------------------------------
procedure TLinkData.LoadFromDataset(aDataset: TDataset);
// ---------------------------------------------------------------
begin
  with aDataset do
  begin
  {
    if Eq (FieldByName(FLD_OBJNAME).AsString, 'pmp_link')
      then LinkType:=ltPmpLink
      else LinkType:=ltLink;
   }


    LinkEndID1   :=FieldByName(FLD_LINKEND1_ID).AsInteger;
    LinkEndID2   :=FieldByName(FLD_LINKEND2_ID).AsInteger;

    {
    case LinkType of    //
      ltPmpLink: begin
   //     PmpSiteID    :=FieldByName(FLD_PMP_SITE_ID).AsInteger;
//        PmpSectorID  :=FieldByName(FLD_PMP_SECTOR_ID).AsInteger;
//        PmpTerminalID:=FieldByName(FLD_PMP_TERMINAL_ID).AsInteger;

      end;

      ltLink: begin
      end;
    end;    // case
    }

    ID  :=FieldByName(FLD_ID).AsInteger;

    PropertyID1  :=FieldByName(FLD_Property1_ID).AsInteger;
    PropertyID2  :=FieldByName(FLD_Property2_ID).AsInteger;

    Refraction :=FieldByName(FLD_Refraction).AsFloat;


    TX_FREQ_MHz  :=FieldByName(FLD_TX_FREQ_MHz).AsFloat;

    // -------------------------
    //Variant
    // -------------------------

//    Property1_Ground_Height1 := FieldByName(FLD_Property1_Ground_Height).AsVariant;
 //   Property2_Ground_Height1 := FieldByName(FLD_Property2_Ground_Height).AsVariant;

    BLVector :=db_ExtractBLVector (aDataset);

    Property1_Pos:=BLVector.Point1;
    Property2_Pos:=BLVector.Point2;


 //   is_profile_reversed:=FieldByName(FLD_is_profile_reversed).AsBoolean;


//    case LinkType of
//      ltLink:
{
      if is_profile_reversed then
              begin
                ExchangeInt(LinkEndID1,  LinkEndID2);
                ExchangeInt(PropertyID1, PropertyID2);
              end;
              }
//    end;


    ClutterModel_ID:=FieldByName(FLD_CLUTTER_MODEL_ID).AsInteger;

 //   BLVector     :=db_ExtractBLVector (aDataset);

    Length_KM:= geo_Distance_km(BLVector);

    Profile_XML   :=FieldByName(FLD_Profile_XML).AsString ;
    PROFILE_Step_M:=FieldByName(FLD_PROFILE_STEP).AsInteger;


    LinkRepeater.ID := FieldByName(FLD_Link_Repeater_ID).AsInteger;


    CalcResultsXML:=FieldByName(FLD_CALC_RESULTS_XML).AsString;

    Self.Name:=FieldByName(FLD_Name).AsString;

  end;

  //  ID:=aID;  

end;


//--------------------------------------------------------------------
function TLinkData.GetAntennaHeightArr(aDataSet: TDataSet): u_Func.TDoubleArray;
//--------------------------------------------------------------------
var
  i: integer;
begin
  Assert(aDataSet.RecordCount>0);

  Setlength(Result, aDataSet.RecordCount);

  with aDataSet do
  begin
    First;
    i:=0;

    while not Eof do
    begin
      Result[i]:=FieldByName(FLD_HEIGHT).AsFloat;
      Next;
      Inc(i);
    end;
  end;

  DoubleArraySort (Result);
end;



// ---------------------------------------------------------------
procedure TLinkData.LoadFromDataset_ReflectionPoints(aDataset: TDataset);
// ---------------------------------------------------------------
var
  I: Integer;
begin
  SetLength (ReflectionPointsArray, aDataset.RecordCount);

 // if Length(Result)=0 then
//    Exit;

  with aDataset do
    for I := 0 to RecordCount-1 do
    begin
      ReflectionPointsArray[i].DISTANCE_to_center_km    :=FieldByName(FLD_DISTANCE).AsFloat;
      ReflectionPointsArray[i].Length_km      :=FieldByName(FLD_LENGTH).AsFloat;
      ReflectionPointsArray[i].Radius_KM      :=FieldByName(FLD_RADIUS).AsFloat;
      ReflectionPointsArray[i].abs_prosvet_m  :=FieldByName(FLD_ABS_PROSVET).AsFloat;

      Next;
    end;
end;



function TAntennaList.GetPOLARIZATIONStr: string;
begin
  Result := GetValue('POLARIZATION');
end;


function TAntennaList.GetDiameterStr: string;
begin
  Result := GetValue('Diameter');
end;


function TAntennaList.GetHeightStr: string;
begin
  Result := GetValue('Height');
end;

// ---------------------------------------------------------------
function TAntennaList.GetValue(aParam: string): string;
// ---------------------------------------------------------------
var
  I: Integer;
begin
  for I := 0 to Count - 1 do
  begin
    Result := Result + '/';

    if Eq(aParam,'Height') then
      Result := Result + FloatToStr(Items[i].Height)

    else if Eq(aParam,'gain') then
      Result := Result + FloatToStr(Items[i].gain)

    else if Eq(aParam,'diameter') then
      Result := Result + FloatToStr(Items[i].diameter)

    else if Eq(aParam,'POLARIZATION') then
      Result := Result + Format('%s', [Items[i].POLARIZATION_STR])

    else
      raise Exception.Create('');
  end;

  Result := Copy(Result, 2, 100);

end;



constructor TAntennaList.Create;
begin
  inherited Create(TAntenna);
end;


function TAntennaList.AddItem: TAntenna;
begin
  Result := TAntenna (inherited Add);
end;


function TAntennaList.GetItems(Index: Integer): TAntenna;
begin
  Result := TAntenna(inherited Items[Index]);
end;




//--------------------------------------------------------------------
function TAntennaList.GetAntennaHeightArr: u_Func.TDoubleArray;
//--------------------------------------------------------------------
var
  i: integer;
begin
  Setlength(Result, Count);

  for I := 0 to Count - 1 do
    Result[i]:= Items[i].Height;

  DoubleArraySort (Result);
end;

//--------------------------------------------------------------------
function TAntennaList.GetMaxHeight_with_ground: double;
//--------------------------------------------------------------------
var
  i: integer;
begin
  Result := 0;

  for I := 0 to Count - 1 do
    if result< Items[i].Ground_Height + Items[i].Height then
      result := Items[i].Ground_Height + Items[i].Height;

      //aGround_Height + oAntenna.Height
end;


function TAntennaList.GetGainStr: string;
begin
  Result := GetValue('Gain');
end;


//--------------------------------------------------------------------
procedure TAntennaList.LoadFromDataset(aDataSet: TDataSet; aGround_Height:
    Double);
//--------------------------------------------------------------------
var
 // eGround_Height: Double;
  i: integer;
  oAntenna: TAntenna;
begin
  Clear;

  Assert(aDataSet.RecordCount>0);

//  eGround_Height:=aGround_Height.FieldByName(FLD_Ground_Height).AsFloat;


//  Setlength(Result, aDataSet.RecordCount);

  with aDataSet do
    while not Eof do
    begin
      oAntenna:=AddItem();

      oAntenna.POLARIZATION_STR:=FieldByName(FLD_POLARIZATION_STR).AsString;

      oAntenna.Height  :=FieldByName(FLD_HEIGHT).AsFloat;
      oAntenna.Diameter:=FieldByName(FLD_Diameter).AsFloat;
      oAntenna.Loss    :=FieldByName(FLD_Loss).AsFloat;
      oAntenna.Gain    :=FieldByName(FLD_Gain).AsFloat;


      oAntenna.Ground_Height:=aGround_Height;

    //  if not VarIsNull(oAntenna.Ground_Height) then
      oAntenna.Height_with_ground:=aGround_Height + oAntenna.Height;

     // oAntenna.:=aGround_Height.FieldByName(FLD_Ground_Height).AsFloat;

      Next;
    end;


end;

// ---------------------------------------------------------------
procedure TProperty.LoadFromDataset(aDataSet: TDataSet);
// ---------------------------------------------------------------
begin
  Assert(aDataSet.RecordCount=1, 'aDataSet.RecordCount<>1');

  with aDataSet do
    while not Eof do
    begin
      ID := FieldByName(FLD_ID).AsInteger;

      Ground_Height := FieldByName(FLD_Ground_Height).AsFloat;
      BLPoint       := db_ExtractBLPoint (aDataset);

      Assert(BLPoint.B<> 0);
      Assert(BLPoint.L<> 0);

      Next;
    end;

end;


end.


  {

    u_reflection_points,



//--------------------------------------------------------------------
function TdmLink.GetReflectionPointArray(aID: integer):
    TrelReflectionPointRecArray;
//--------------------------------------------------------------------
const
  SQL_SELECT_reflection_POINTS =
    'SELECT * FROM '+ TBL_Link_reflection_points + ' WHERE (link_id=:link_id)';
var
  I: Integer;
begin
  Assert(aID>0);


  db_OpenQuery(qry_Temp, SQL_SELECT_reflection_POINTS, [db_Par(FLD_LINK_ID, aID)]);

 // OpenDB_ReflectionPoints (aID, mem_Points);

  SetLength (Result, qry_Temp.RecordCount);

  if Length(Result)=0 then
    Exit;

  with qry_Temp do
    for I := 0 to RecordCount-1 do
    begin
      Result[i].Distance_KM    :=FieldByName(FLD_DISTANCE).AsFloat;
      Result[i].Length_km      :=FieldByName(FLD_LENGTH).AsFloat;
      Result[i].Radius_KM      :=FieldByName(FLD_RADIUS).AsFloat;
      Result[i].abs_prosvet_m:=FieldByName(FLD_ABS_PROSVET).AsFloat;
      Next;
    end;

end;



//--------------------------------------------------------------------
function TdmLink_report_new.GetAntennaHeightArr (aQryAntenna: TADOQuery): u_func.TDoubleArray ;
//--------------------------------------------------------------------
var
  i: integer;
begin
  Setlength(Result, aQryAntenna.RecordCount);

  with aQryAntenna do
  begin
    First; i:=0;

    while not Eof do
    begin
      Result[i]:=FieldByName(FLD_HEIGHT).AsFloat;
      Next;
      Inc(i);
    end;
  end;

  DoubleArraySort (Result);
end;



