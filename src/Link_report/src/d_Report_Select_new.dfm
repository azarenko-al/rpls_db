inherited dlg_Main_Report: Tdlg_Main_Report
  Left = 889
  Top = 189
  VertScrollBar.Range = 0
  BorderStyle = bsDialog
  Caption = #1042#1099#1073#1086#1088' '#1086#1090#1095#1077#1090#1072
  ClientHeight = 712
  ClientWidth = 571
  OldCreateOrder = True
  Position = poDefaultPosOnly
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  inherited pn_Buttons: TPanel
    Top = 677
    Width = 571
    TabOrder = 2
    inherited Bevel1: TBevel
      Width = 571
    end
    inherited Panel3: TPanel
      Left = 396
      Width = 175
      inherited btn_Ok: TButton
        Tag = 5
        Left = 8
        Top = 6
      end
      inherited btn_Cancel: TButton
        Tag = 5
        Left = 92
        Top = 6
      end
    end
    object b_DesignReport: TButton
      Tag = 5
      Left = 7
      Top = 8
      Width = 110
      Height = 23
      Action = act_DesignReport
      TabOrder = 1
    end
    object Button4: TButton
      Left = 120
      Top = 8
      Width = 110
      Height = 23
      Action = act_Preview
      TabOrder = 2
    end
  end
  inherited pn_Top_: TPanel
    Width = 571
    TabOrder = 3
    inherited Bevel2: TBevel
      Width = 571
    end
    inherited pn_Header: TPanel
      Width = 571
    end
  end
  object ToolBar1: TToolBar [2]
    Left = 0
    Top = 60
    Width = 571
    Height = 101
    Caption = 'ToolBar1'
    Color = clBtnShadow
    ParentColor = False
    TabOrder = 0
    Visible = False
  end
  object pn_Main: TPanel [3]
    Left = 0
    Top = 216
    Width = 571
    Height = 401
    Align = alBottom
    BevelOuter = bvNone
    BorderWidth = 5
    TabOrder = 1
    object cxVerticalGrid1: TcxVerticalGrid
      Left = 5
      Top = 5
      Width = 561
      Height = 380
      BorderStyle = cxcbsNone
      Align = alTop
      LookAndFeel.Kind = lfOffice11
      OptionsView.ShowEditButtons = ecsbAlways
      OptionsView.PaintStyle = psDelphi
      OptionsView.RowHeaderWidth = 237
      OptionsBehavior.ImmediateEditor = False
      TabOrder = 0
      Version = 1
      object row_Template_ID: TcxEditorRow
        Properties.Caption = #1064#1072#1073#1083#1086#1085
        Properties.EditPropertiesClassName = 'TcxLookupComboBoxProperties'
        Properties.EditProperties.DropDownListStyle = lsFixedList
        Properties.EditProperties.DropDownRows = 30
        Properties.EditProperties.ImmediatePost = True
        Properties.EditProperties.KeyFieldNames = 'id'
        Properties.EditProperties.ListColumns = <
          item
            FieldName = 'name'
          end>
        Properties.EditProperties.ListOptions.AnsiSort = True
        Properties.EditProperties.ListOptions.ShowHeader = False
        Properties.EditProperties.ListSource = ds_Report
        Properties.DataBinding.ValueType = 'Integer'
        Properties.Value = Null
        ID = 0
        ParentID = -1
        Index = 0
        Version = 1
      end
      object row_folder: TcxEditorRow
        Properties.Caption = #1057#1086#1093#1088#1072#1085#1103#1090#1100' '#1086#1090#1095#1077#1090#1099' '#1074' '#1087#1072#1087#1082#1091':'
        Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
        Properties.EditProperties.Buttons = <
          item
            Default = True
            Kind = bkEllipsis
          end>
        Properties.EditProperties.OnButtonClick = row_Report_folderEditPropertiesButtonClick
        Properties.DataBinding.ValueType = 'String'
        Properties.Value = ''
        ID = 1
        ParentID = -1
        Index = 1
        Version = 1
      end
      object cxVerticalGrid1CategoryRow4: TcxCategoryRow
        ID = 2
        ParentID = -1
        Index = 2
        Version = 1
      end
      object row_IsAutoScale_1111: TcxEditorRow
        Properties.Caption = 'Auto '#1084#1072#1089#1096#1090#1072#1073
        Properties.EditPropertiesClassName = 'TcxCheckBoxProperties'
        Properties.EditProperties.ImmediatePost = True
        Properties.EditProperties.UseAlignmentWhenInplace = True
        Properties.DataBinding.ValueType = 'Boolean'
        Properties.Value = False
        Visible = False
        ID = 3
        ParentID = -1
        Index = 3
        Version = 1
      end
      object row_Scale: TcxEditorRow
        Properties.Caption = #1063#1080#1089#1083#1077#1085#1085#1099#1081' '#1084#1072#1089#1096#1090#1072#1073
        Properties.EditPropertiesClassName = 'TcxComboBoxProperties'
        Properties.EditProperties.Items.Strings = (
          '10 000'
          '50 000'
          '100 000'
          '200 000'
          '500 000'
          '1 000 000')
        Properties.DataBinding.ValueType = 'String'
        Properties.Options.ShowEditButtons = eisbAlways
        Properties.Value = '200 000'
        ID = 4
        ParentID = -1
        Index = 4
        Version = 1
      end
      object row_IsOpenReportAfterDone: TcxEditorRow
        Properties.Caption = #1055#1086#1082#1072#1079#1072#1090#1100' '#1086#1090#1095#1077#1090
        Properties.EditPropertiesClassName = 'TcxCheckBoxProperties'
        Properties.EditProperties.Alignment = taLeftJustify
        Properties.EditProperties.ImmediatePost = True
        Properties.EditProperties.UseAlignmentWhenInplace = True
        Properties.DataBinding.ValueType = 'Boolean'
        Properties.Value = True
        ID = 5
        ParentID = -1
        Index = 5
        Version = 1
      end
      object cxVerticalGrid1CategoryRow1: TcxCategoryRow
        ID = 6
        ParentID = -1
        Index = 6
        Version = 1
      end
      object row_Is_make_map: TcxEditorRow
        Properties.Caption = #1060#1086#1088#1084#1080#1088#1086#1074#1072#1090#1100' '#1082#1072#1088#1090#1091
        Properties.EditPropertiesClassName = 'TcxCheckBoxProperties'
        Properties.EditProperties.Alignment = taLeftJustify
        Properties.EditProperties.UseAlignmentWhenInplace = True
        Properties.EditProperties.ValueGrayed = 'False'
        Properties.DataBinding.ValueType = 'String'
        Properties.Value = 'True'
        ID = 7
        ParentID = -1
        Index = 7
        Version = 1
      end
      object col_Group_sizes: TcxCategoryRow
        Properties.Caption = #1056#1072#1079#1084#1077#1088#1099' '#1074#1099#1082#1086#1087#1080#1088#1086#1074#1082#1080
        ID = 8
        ParentID = -1
        Index = 8
        Version = 1
      end
      object row_H: TcxEditorRow
        Properties.Caption = #1042#1099#1089#1086#1090#1072' [cm]'
        Properties.EditPropertiesClassName = 'TcxSpinEditProperties'
        Properties.DataBinding.ValueType = 'String'
        Properties.Value = '20'
        ID = 9
        ParentID = 8
        Index = 0
        Version = 1
      end
      object row_W: TcxEditorRow
        Properties.Caption = #1064#1080#1088#1080#1085#1072' [cm]'
        Properties.EditPropertiesClassName = 'TcxSpinEditProperties'
        Properties.DataBinding.ValueType = 'String'
        Properties.Value = '20'
        ID = 10
        ParentID = 8
        Index = 1
        Version = 1
      end
      object cxVerticalGrid1CategoryRow3: TcxCategoryRow
        ID = 11
        ParentID = -1
        Index = 9
        Version = 1
      end
      object row_IsShowFrenelZones: TcxEditorRow
        Properties.Caption = #1047#1086#1085#1099' '#1060#1088#1077#1085#1077#1083#1103' '#1086#1090' '#1088#1072#1079#1085#1077#1089#1077#1085#1085#1099#1093' '#1072#1085#1090#1077#1085#1085
        Properties.EditPropertiesClassName = 'TcxCheckBoxProperties'
        Properties.EditProperties.ImmediatePost = True
        Properties.EditProperties.UseAlignmentWhenInplace = True
        Properties.DataBinding.ValueType = 'Boolean'
        Properties.Value = True
        ID = 12
        ParentID = -1
        Index = 10
        Version = 1
      end
      object row_SaveToDB: TcxEditorRow
        Properties.Caption = #1057#1086#1093#1088#1072#1085#1103#1090#1100' '#1086#1090#1095#1077#1090' '#1074' '#1041#1044
        Properties.EditPropertiesClassName = 'TcxCheckBoxProperties'
        Properties.EditProperties.ImmediatePost = True
        Properties.EditProperties.UseAlignmentWhenInplace = True
        Properties.DataBinding.ValueType = 'Boolean'
        Properties.Value = False
        ID = 13
        ParentID = -1
        Index = 11
        Version = 1
      end
      object row_IsShowReflectionPoints: TcxEditorRow
        Properties.Caption = #1056#1080#1089#1086#1074#1072#1090#1100' '#1090#1086#1095#1082#1080' '#1086#1090#1088#1072#1078#1077#1085#1080#1103
        Properties.EditPropertiesClassName = 'TcxCheckBoxProperties'
        Properties.EditProperties.ImmediatePost = True
        Properties.EditProperties.NullStyle = nssUnchecked
        Properties.EditProperties.UseAlignmentWhenInplace = True
        Properties.DataBinding.ValueType = 'Boolean'
        Properties.Value = True
        ID = 14
        ParentID = -1
        Index = 12
        Version = 1
      end
      object cxVerticalGrid1CategoryRow5: TcxCategoryRow
        ID = 15
        ParentID = -1
        Index = 13
        Version = 1
      end
      object row_FileType: TcxEditorRow
        Properties.Caption = #1060#1086#1088#1084#1072#1090' '#1092#1072#1081#1083#1072
        Properties.EditPropertiesClassName = 'TcxRadioGroupProperties'
        Properties.EditProperties.Columns = 3
        Properties.EditProperties.ImmediatePost = True
        Properties.EditProperties.Items = <
          item
            Caption = 'Word'
            Value = 'word'
          end
          item
            Caption = 'PDF'
            Value = 'pdf'
          end
          item
            Caption = 'Excel'
            Value = 'excel'
          end>
        Properties.DataBinding.ValueType = 'String'
        Properties.Value = 'word'
        ID = 16
        ParentID = -1
        Index = 14
        Version = 1
      end
      object row_WMS: TcxEditorRow
        Properties.Caption = 'WMS'
        Properties.EditPropertiesClassName = 'TcxCheckBoxProperties'
        Properties.EditProperties.Alignment = taLeftJustify
        Properties.EditProperties.ImmediatePost = True
        Properties.EditProperties.NullStyle = nssUnchecked
        Properties.EditProperties.UseAlignmentWhenInplace = True
        Properties.EditProperties.ValueGrayed = 'null'
        Properties.EditProperties.OnEditValueChanged = row_WMSEditPropertiesEditValueChanged
        Properties.DataBinding.ValueType = 'Boolean'
        Properties.Value = True
        ID = 17
        ParentID = -1
        Index = 15
        Version = 1
      end
      object row_WMS_items: TcxEditorRow
        Properties.Caption = #1089#1083#1086#1080
        Properties.EditPropertiesClassName = 'TcxComboBoxProperties'
        Properties.EditProperties.DropDownListStyle = lsEditFixedList
        Properties.EditProperties.DropDownRows = 20
        Properties.EditProperties.ImmediatePost = True
        Properties.EditProperties.Items.Strings = (
          '1'
          '2'
          '3')
        Properties.DataBinding.ValueType = 'String'
        Properties.Value = 'YandexMap'
        ID = 18
        ParentID = 17
        Index = 0
        Version = 1
      end
      object row_WMS_items1: TcxEditorRow
        Tag = 1
        Properties.Caption = #1089#1083#1086#1080
        Properties.EditPropertiesClassName = 'TcxLookupComboBoxProperties'
        Properties.EditProperties.KeyFieldNames = 'name'
        Properties.EditProperties.ListColumns = <
          item
            FieldName = 'caption'
          end>
        Properties.EditProperties.ListOptions.ShowHeader = False
        Properties.DataBinding.ValueType = 'String'
        Properties.Value = Null
        ID = 19
        ParentID = 17
        Index = 1
        Version = 1
      end
      object row_WMS_Z: TcxEditorRow
        Properties.Caption = 'Z'
        Properties.EditPropertiesClassName = 'TcxComboBoxProperties'
        Properties.EditProperties.DropDownListStyle = lsFixedList
        Properties.EditProperties.DropDownRows = 20
        Properties.EditProperties.ImmediatePost = True
        Properties.DataBinding.ValueType = 'String'
        Properties.Value = Null
        ID = 20
        ParentID = 17
        Index = 2
        Version = 1
      end
      object row_AutoScale: TcxEditorRow
        Properties.Caption = #1040#1074#1090#1086#1084#1072#1089#1096#1090#1072#1073
        Properties.EditPropertiesClassName = 'TcxCheckBoxProperties'
        Properties.EditProperties.ImmediatePost = True
        Properties.EditProperties.NullStyle = nssUnchecked
        Properties.EditProperties.UseAlignmentWhenInplace = True
        Properties.DataBinding.ValueType = 'String'
        Properties.Value = 'True'
        ID = 21
        ParentID = 17
        Index = 3
        Version = 1
      end
    end
  end
  object Panel1: TPanel [4]
    Left = 0
    Top = 617
    Width = 571
    Height = 41
    Align = alBottom
    BevelOuter = bvLowered
    TabOrder = 4
    object Button1: TButton
      Left = 8
      Top = 8
      Width = 209
      Height = 25
      Action = act_Test_Add
      TabOrder = 0
    end
    object Button2: TButton
      Left = 256
      Top = 8
      Width = 75
      Height = 25
      Caption = 'Button2'
      TabOrder = 1
      Visible = False
    end
    object Button5: TButton
      Left = 376
      Top = 8
      Width = 75
      Height = 25
      Caption = 'Button5'
      TabOrder = 2
    end
  end
  object StatusBar1: TStatusBar [5]
    Left = 0
    Top = 658
    Width = 571
    Height = 19
    Panels = <>
  end
  inherited ActionList1: TActionList
    Left = 352
  end
  inherited FormStorage1: TFormStorage
    Left = 324
  end
  inherited cxPropertiesStore: TcxPropertiesStore
    StorageName = 'cxPropertiesStore_report'
    Left = 295
    Top = 3
  end
  object SaveDialog111: TSaveDialog
    DefaultExt = '*.htm'
    FileName = 'report'
    Filter = #1060#1072#1081#1083#1099' HTML|*.htm'
    Options = [ofOverwritePrompt, ofHideReadOnly, ofEnableSizing]
    Title = #1042#1099#1073#1086#1088' '#1092#1072#1081#1083#1072
    Left = 24
    Top = 61
  end
  object ds_Report: TDataSource
    DataSet = sp_Reports
    Left = 185
    Top = 110
  end
  object OpenDialog111: TOpenDialog
    Filter = 'XSL|*.xsl'
    Left = 56
    Top = 60
  end
  object ActionList2: TActionList
    OnUpdate = ActionList2Update
    Left = 488
    Top = 84
    object act_DesignReport: TAction
      Caption = 'act_DesignReport'
      OnExecute = act_DesignReportExecute
    end
    object act_Preview: TAction
      Caption = 'act_Preview'
      OnExecute = act_DesignReportExecute
    end
    object act_Run: TAction
      Caption = 'act_Run'
      OnExecute = act_DesignReportExecute
    end
    object act_Test_Add: TAction
      Caption = #1057#1086#1079#1076#1072#1090#1100' '#1086#1090#1095#1077#1090#1099' '#1074' '#1073#1072#1079#1077
      ShortCut = 16468
      OnExecute = act_DesignReportExecute
    end
  end
  object ApplicationEvents1: TApplicationEvents
    OnIdle = ApplicationEvents1Idle
    Left = 48
    Top = 168
  end
  object sp_Reports: TADOStoredProc
    Parameters = <>
    Left = 184
    Top = 64
  end
end
