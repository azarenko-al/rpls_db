unit d_Report_Select_new;

interface
uses
  SysUtils, Classes, Controls, Forms, Dialogs,
  FileCtrl,
  ComCtrls, StdCtrls, ExtCtrls, ActnList,   Db, ADODB,    

  cxDropDownEdit,
  AppEvnts,

  Variants,

  dm_Onega_DB_data,

  dm_Settings,
  

  dm_Link_SDB_report,
  dm_Link_report,
  dm_LinkLine_report,

  cxVGrid,
  dm_Report,


  d_Wizard,

  dm_Main,

 // I_Report,

 // dm_Report,

  u_func,
  u_db,

  u_files,

  u_cx_vgrid,
  u_cx_vgrid_export,

 // u_XML,

  

  u_const_db,


  u_ini_Link_report_params,

  
  cxDBLookupComboBox,
  cxInplaceContainer, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, cxEdit, cxButtonEdit, cxCheckBox,
  cxSpinEdit, cxRadioGroup, ToolWin, cxPropertiesStore, rxPlacemnt,
  dxSkinsCore, dxSkinsDefaultPainters;



type

  Tdlg_Main_Report = class(Tdlg_Wizard)
    SaveDialog111: TSaveDialog;
    ds_Report: TDataSource;
    OpenDialog111: TOpenDialog;
    ToolBar1: TToolBar;
    pn_Main: TPanel;
    ActionList2: TActionList;
    cxVerticalGrid1: TcxVerticalGrid;
    row_IsAutoScale_1111: TcxEditorRow;
    row_Scale: TcxEditorRow;
    row_IsOpenReportAfterDone: TcxEditorRow;
    cxVerticalGrid1CategoryRow1: TcxCategoryRow;
    col_Group_sizes: TcxCategoryRow;
    row_H: TcxEditorRow;
    row_W: TcxEditorRow;
    cxVerticalGrid1CategoryRow3: TcxCategoryRow;
    row_IsShowFrenelZones: TcxEditorRow;
    row_SaveToDB: TcxEditorRow;
    row_IsShowReflectionPoints: TcxEditorRow;
    row_FileType: TcxEditorRow;
    row_Template_ID: TcxEditorRow;
    row_folder: TcxEditorRow;
    cxVerticalGrid1CategoryRow4: TcxCategoryRow;
    ApplicationEvents1: TApplicationEvents;
    b_DesignReport: TButton;
    act_DesignReport: TAction;
    act_Preview: TAction;
    Button4: TButton;
    act_Run: TAction;
    cxVerticalGrid1CategoryRow5: TcxCategoryRow;
    act_Test_Add: TAction;
    Panel1: TPanel;
    Button1: TButton;
    StatusBar1: TStatusBar;
    Button2: TButton;
    row_WMS: TcxEditorRow;
    row_WMS_items: TcxEditorRow;
    row_WMS_Z: TcxEditorRow;
    row_Is_make_map: TcxEditorRow;
    row_AutoScale: TcxEditorRow;
    row_WMS_items1: TcxEditorRow;
    Button5: TButton;
    sp_Reports: TADOStoredProc;
    procedure FormDestroy(Sender: TObject);
    procedure FormCreate(Sender: TObject);
 //   procedure FormDestroy(Sender: TObject);
    procedure ActionList1Update(Action: TBasicAction;  var Handled: Boolean);
    procedure ActionList2Update(Action: TBasicAction; var Handled: Boolean);
    procedure act_DesignReportExecute(Sender: TObject);
    procedure act_OkExecute(Sender: TObject);
//    procedure act_Test_AddExecute(Sender: TObject);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
//    procedure Button2Click(Sender: TObject);
//    procedure Button5Click(Sender: TObject);
 //   procedure Button1Click(Sender: TObject);
    procedure b_DesignReportClick(Sender: TObject);
//    procedure pn_MainClick(Sender: TObject);
//    procedure cxGrid1DBTableView1DblClick(Sender: TObject);
    procedure row_Report_folderEditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
//    procedure StatusBar1DblClick(Sender: TObject);
    procedure row_WMSEditPropertiesEditValueChanged(Sender: TObject);
//    procedure StatusBar1DblClick(Sender: TObject);
  private
    FRegPath: string;

    FChanged : boolean;


    function Check_Report_ID(aID: Integer): Boolean;
//    procedure Copy;

    procedure DesignReport;
    procedure OpenDB;
//    procedure Preview;

    procedure Run;
  //  FReportSelect: TIni_Link_Report_Param;

    procedure SaveToClass(aReportParam: TIni_Link_Report_Param);
//    procedure Test_Add;
  protected
    procedure CreateParams(var Params: TCreateParams); override;
  public
    class function ExecDlg: boolean;
  end;


//==================================================================
//implementation
//==================================================================
implementation

 {$R *.DFM}


//------------------------------------------------------------------
class function Tdlg_Main_Report.ExecDlg: boolean;
//------------------------------------------------------------------
begin
 // Assert(Assigned(aReportSelect), 'Value not assigned');


  with Tdlg_Main_Report.Create(Application) do
  try
   // FReportSelect := aReportSelect;

  //  db_OpenTable (qry_Report, TBL_REPORT, [db_Par(FLD_CATEGORY, aCategory)]);

    OpenDB();

//    if qry_Reports.RecordCount=0 then
//      Test_Add();

//    db_OpenQuery(qry_Report,
//               'SELECT id,name,FileName FROM '+  TBL_REPORT+
//                ' WHERE CATEGORY=:CATEGORY ORDER BY NAME',
//
//                [db_Par(FLD_CATEGORY, g_Link_Report_Param.Category)]
//                );
//


    Result:=(ShowModal=mrOk);

    if Result then
    begin
      act_Run.Execute;

(*      SaveToClass(g_Link_Report_Param);

      g_Link_Report_Param.IsPreview:=False;
      Run();
*)

    end;

  finally
    Free;
  end;
end;




//------------------------------------------------------------------
procedure Tdlg_Main_Report.SaveToClass(aReportParam: TIni_Link_Report_Param);
//------------------------------------------------------------------
var sType: string;
  iScale: Integer;
  k: Integer;
  s: string;
  v: Variant;
 // i: integer;
begin
////  aReportParam.IsPreview             :=row_IsPreview.Properties.Value;
  v:= sp_Reports.Lookup(FLD_Id, row_Template_ID.Properties.Value, FLD_Id);

  if VarIsNull(v) then
    Exit;


  aReportParam.Report_ID:= AsInteger( row_Template_ID.Properties.Value);


  aReportParam.FileDir := IncludeTrailingBackslash (row_Folder.Properties.Value);

  iScale :=AsInteger (ReplaceStr(row_Scale.Properties.Value,' ',''));
  if iScale>0 then aReportParam.Scale :=iScale
              else aReportParam.Scale :=100000;

  aReportParam.IsMakeMap:=row_Is_make_map.Properties.Value = True;

  aReportParam.ImageWidth :=row_W.Properties.Value;
  aReportParam.ImageHeight:=row_H.Properties.Value;



  aReportParam.IsShowReflectionPoints:=row_IsShowReflectionPoints.Properties.Value;
  aReportParam.IsOpenReportAfterDone :=row_IsOpenReportAfterDone.Properties.Value;
  aReportParam.IsSaveToDB:=row_SaveToDB.Properties.Value;
//  aReportParam.IsAutoScale:=row_IsAutoScale.Properties.Value;

  aReportParam.Profile_IsShowFrenelZoneBottomLeftRight := row_IsShowFrenelZones.Properties.Value;

  // ------------------------

  s:=row_FileType.Properties.Value;

  if eq(s,'word')  then aReportParam.FileType := ftFileWord else
  if eq(s,'Excel') then aReportParam.FileType := ftFileExcel else
  if eq(s,'PDF')   then aReportParam.FileType := ftFilePDF ;

  row_WMS.Properties.Value;

  aReportParam.WMS.Checked     := row_WMS.Properties.Value;
  aReportParam.WMS.Z           := StrToInt(row_WMS_Z.Properties.Value);
  aReportParam.WMS.IsAutoScale := row_AutoScale.Properties.Value;


 // aReportParam.WMS.LayerName := row_WMS_items.Properties.Value;
  aReportParam.WMS.LayerName := row_WMS_items1.Properties.Value;

   if dmSettings.sp_WMS_SEL.Locate ('name',aReportParam.WMS.LayerName,[]) then
   begin
     aReportParam.WMS.URL  :=dmSettings.sp_WMS_SEL['url'];
     aReportParam.WMS.epsg :=dmSettings.sp_WMS_SEL['epsg'];
     aReportParam.WMS.z_max:=dmSettings.sp_WMS_SEL['max_z'];

   end;



{
  k:=cx_GetRowItemIndex(row_WMS_items);
  if k>=0 then
    aReportParam.WMS.LayerName :=DEF_WMS1[k].Name;
 }

  ///  v:=


end;


//------------------------------------------------------------------
procedure Tdlg_Main_Report.FormCreate(Sender: TObject);
//------------------------------------------------------------------
var
  I: Integer;
 // iResult: Integer;
  k: Integer;
  oComboBoxProperties: TcxComboBoxProperties;
  s: string;
begin
  inherited;

//  if ADOConnection1.Connected then
//    ShowMessage ('procedure Tdlg_Tdlg_Settings_WMSLinkFreqPlan_Add_from_map.FormCreate(Sender: TObject);');


  Height:=650;

  Caption:= '����� | '+  '������: '  + FormatDateTime ('yyyy-mm-dd hh:mm', GetFileDate (Application.Exename));

//  Caption:='�����: ' +  GetAppVersionStr();

  FRegPath:= 'Software\Onega\link_report\Forms\'+ ClassName+'\';
  FRegPath:=FRegPath + g_Link_Report_Param.Category;


//  ShowMessage(FRegPath);

  StatusBar1.SimpleText:=dmMain.LoginRec.ConnectionStatusStr;


 // cxGrid1.Align := alClient;
  pn_Main.Align         := alClient;
  cxVerticalGrid1.Align := alClient;

//  row_IsAutoScale.Properties.Caption  :='���� �������';
//  row_IsPreview.Properties.Caption  :='������������';

  row_IsOpenReportAfterDone.Properties.Caption  :='������� ����� ����� ��������';


//  col_Name.Caption      := '�������� ������';
 // col_FileName.Caption  := '��� �����';

  db_SetComponentADOConnection (Self, dmMain.ADOConnection);

//  sdfsdf

  if row_folder.Properties.Value='' then
    row_folder.Properties.Value:=GetSpecialFolderPath_MyDocuments + 'RPLS Reports';

//    GetSpecialFolderPath_MyDocuments

//  if Trim(ed_OutputDir.Text)='' then
//    ed_OutputDir.Text:='c:\Reports';

//  (g_Link_Report_Param.Category,'Link')


//  cx_VerticalGrid_LoadFromReg(cxVerticalGrid1, FRegPath);


  act_DesignReport.Caption:='�������� ������';


{
  FRegPath:= REGISTRY_COMMON_FORMS + ClassName +'\';

  FormStorage1.IniFileName:=FRegPath;
  FormStorage1.IniSection:='Window';
  FormStorage1.Active:=True;
}

  Assert(g_Link_Report_Param.ProjectID > 0, 'Value <=0');

  cx_InitVerticalGrid(cxVerticalGrid1);


 // row_IsPreview.Visible := False;
//  row_IsAutoScale.Visible := False;

  act_Preview.Caption:= '������������';



  row_WMS.Visible:=True;


  // ---------------------------------------------------------------

  oComboBoxProperties:=TcxComboBoxProperties(row_WMS_items.Properties.EditProperties);
 // oProperties.ImmediatePost := True;

 // oProperties.ite

//  oProperties.NullStyle:=nssUnchecked;

  oComboBoxProperties.Items.Clear;

  {
 for I := 0 to High(DEF_WMS1) do    // Iterate
//    oComboBoxProperties.Items.AddObject (DEF_WMS[i].Name, Pointer(i));
    oComboBoxProperties.Items.AddObject (DEF_WMS1[i].Caption, Pointer(i));

  cx_SetRowItemIndex(row_WMS_items, DEF_DEFAULT_WMS_INDEX);
   }


  // ---------------------------------------------------------------
  oComboBoxProperties:=TcxComboBoxProperties(row_WMS_Z.Properties.EditProperties);

  oComboBoxProperties.Items.Clear;

//  oComboBoxProperties.Items.Add ('-');


  for I := 2 to 20 do    // Iterate
    oComboBoxProperties.Items.AddObject (Format('%d', [i] ), Pointer(i));


  row_WMS_Z.Properties.Value:='12';

  // ---------------------------------------------------------------

////////////
  row_WMS_items.Visible:=false;


  TdmSettings.Init.Open_WMS;
  TcxLookupComboBoxProperties(row_WMS_items1.Properties.EditProperties).ListSource:=dmSettings.ds_WMS;
  TcxLookupComboBoxProperties(row_WMS_items1.Properties.EditProperties).KeyFieldNames:='name';
  TcxLookupComboBoxProperties(row_WMS_items1.Properties.EditProperties).ListFieldNames:='caption';



//  TcxLookupComboBoxProperties(row_WMS_items1.Properties.EditProperties).v


//  row_WMS_items1.Properties. Assign  i ngValue:= dmSettings.q_WMS.FieldValues['name'];

//  EditingValue

//  v:=row_WMS_items1.Properties.Value;


//     TcxLookupComboBoxProperties(cxLookupComboBox1.Properties.EditProperties).list:=dmSettings.ds_WMS;
//   TcxLookupComboBoxProperties(cxLookupComboBox1.Properties.EditProperties).KeyFieldNames:='id';
//   TcxLookupComboBoxProperties(cxLookupComboBox1.Properties.EditProperties).ListFieldNames:='caption';


  //LookupComboBox
//  s:=dmSettings.q_WMS.FieldByName('name').AsString;

  cx_VerticalGrid_LoadFromReg(cxVerticalGrid1, FRegPath);


  if VarToStr(row_folder.Properties.Value)='' then
    row_folder.Properties.Value:= GetSpecialFolderPath_MyDocuments ();

//  s:=dmSettings.q_WMS.FieldValues['name'];
  row_WMS_items1.Properties.Value:= dmSettings.WMS_Get_Default_Map_name(); // q_WMS.FieldValues['name'];

//  row_WMS_items1.Properties.


  dmOnega_DB_data.OpenStoredProc(sp_Reports, 'lib.sp_report_sel', ['category', 'link_freq_plan']);


//      if VarToStr(row_folder.Properties.Value)='' then
//    row_folder.Properties.Value:= GetSpecialFolderPath_MyDocuments ();


  {
  row_WMS_items1.Properties.Value:=s; //dmSettings.q_WMS.FieldValues['name'];
  }

//  row_WMS_Z.Properties.ItemIndex:=0;


 // row_Z.Visible:=False;


//  row_WMS_Z_.Visible:=False;



//  ItemIndex:=15;

 /////

//   FormStyle := fsStayOnTop;;
end;


procedure Tdlg_Main_Report.FormDestroy(Sender: TObject);
begin
  cx_VerticalGrid_SaveToReg(cxVerticalGrid1, FRegPath);

  inherited;
end;

procedure Tdlg_Main_Report.CreateParams(var Params: TCreateParams);
begin
  inherited;
//  Params.WndParent := 0;
end;

//------------------------------------------------------------------
procedure Tdlg_Main_Report.ActionList1Update(Action: TBasicAction; var Handled: Boolean);
//------------------------------------------------------------------
begin
  act_Ok.Enabled:=not sp_Reports.IsEmpty;

end;

procedure Tdlg_Main_Report.ActionList2Update(Action: TBasicAction; var
    Handled: Boolean);
var
  bAuto: Boolean;
begin
  bAuto:=row_AutoScale.Properties.Value;

  row_WMS_Z.Properties.Options.Editing:=not bAuto;

end;



// ---------------------------------------------------------------
procedure Tdlg_Main_Report.act_DesignReportExecute(Sender: TObject);
// ---------------------------------------------------------------
//var
//  iID: Integer;
begin
  // -------------------------
  if Sender=act_Test_Add then
  // -------------------------
  begin
    Test_Add;
    dmLink_report.Add_Default;
//    dmLinkLine_report.TestAdd;

    OpenDB();

    iID:=qry_Reports.FieldByName(FLD_ID).AsInteger;

    row_Template_ID.Properties.Value:=iID;
  end else


  if Sender=act_DesignReport then
    DesignReport();

  // -------------------------
//  if Sender=act_Copy then
//    Copy() else

  // -------------------------
  if Sender=act_Preview then
  begin
    SaveToClass(g_Link_Report_Param);

    g_Link_Report_Param.IsPreview := True;

    Run();
  end else

  // -------------------------
  if Sender=act_Run then
  begin
   // Run();

    SaveToClass(g_Link_Report_Param);

    g_Link_Report_Param.IsPreview:=False;
    Run();


  end;



end;




procedure Tdlg_Main_Report.act_OkExecute(Sender: TObject);
begin
// don't remove
end;


//---------------------------------------------------------------------------
procedure Tdlg_Main_Report.OpenDB;
//---------------------------------------------------------------------------
begin
  dmOnega_DB_data.OpenStoredProc(sp_Reports, 'lib.sp_report_SEL',
                ['CATEGORY', g_Link_Report_Param.Category] );

  row_Template_ID.Properties.Value:=sp_Reports.FieldByName('id').AsInteger;

//  iTemplate_ID:= AsInteger(row_Template_ID.Properties.Value);


//  v:= sp_Reports.Lookup(FLD_Id, iTemplate_ID, FLD_Id);

end;


// ---------------------------------------------------------------
function Tdlg_Main_Report.Check_Report_ID(aID: Integer): Boolean;
// ---------------------------------------------------------------
begin
//  result:=True;
   result :=
     sp_Reports.Active and
     sp_Reports.Locate(FLD_ID, aID, []);

end;


// ---------------------------------------------------------------
procedure Tdlg_Main_Report.ApplicationEvents1Idle(Sender: TObject; var
    Done: Boolean);
// ---------------------------------------------------------------
var
  b: Boolean;
  iTemplate_ID: Integer;
  v: Variant;
begin
  iTemplate_ID:= AsInteger(row_Template_ID.Properties.Value);


  v:= sp_Reports.Lookup(FLD_Id, iTemplate_ID, FLD_Id);

 // if IsNull(v) then
  //  Exit;

//  exit;

//  b:=Check_Report_ID( AsInteger( row_Template_ID.Properties.Value));

  act_Ok.Enabled:=  //b and
//    not VarIsNull( row_Template_ID.Properties.Value) and
    (not VarIsNull(v)) and

//    (AsInteger( row_Template_ID.Properties.Value) > 0) and
    (Trim(AsString(row_folder.Properties.Value))<>'');

  act_Preview.Enabled:=act_Ok.Enabled;

end;



// ---------------------------------------------------------------
procedure Tdlg_Main_Report.row_Report_folderEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
// ---------------------------------------------------------------
var
  sDir: string;
begin
  sDir:=AsString(row_folder.Properties.Value);

  //FileCtrl
  if FileCtrl.SelectDirectory('����� �����...','',sDir) then
    cx_SetButtonEditText(Sender, sDir);

//  PBFolderDialog1.Folder := AsString(row_folder.Properties.Value);

//   if PBFolderDialog1.Execute then
//    cx_SetButtonEditText(Sender, PBFolderDialog1.Folder);
  //  row_Report_folder.Properties.Value:=PBFolderDialog1.Folder

(*

  Exit;

  JvBrowseForFolderDialog1.Directory := AsString(row_folder.Properties.Value);

//  PBFolderDialog1.Folder:=AsString(row_folder.Properties.Value);

  if JvBrowseForFolderDialog1.Execute then
    cx_SetButtonEditText(Sender, JvBrowseForFolderDialog1.Directory)
  //  row_Report_folder.Properties.Value:=PBFolderDialog1.Folder
*)
end;



procedure Tdlg_Main_Report.b_DesignReportClick(Sender: TObject);
begin
  DesignReport();

end;



// ---------------------------------------------------------------
procedure Tdlg_Main_Report.DesignReport;
// ---------------------------------------------------------------
begin
  inherited;

  SaveToClass(g_Link_Report_Param);

  dmLink_report.DesignReport();

end;



// ---------------------------------------------------------------
procedure Tdlg_Main_Report.Run;
// ---------------------------------------------------------------
{.$DEFINE test}

var
  sTemplateFileName: string;
begin
//  g_Link_Report_Param.IsPreview:=False;


//  sTemplateFileName := g_Link_Report_Param.FileDir + Format('report_%d.rep', [g_Link_Report_Param.Report_ID]);
  sTemplateFileName := g_Link_Report_Param.GetTempReportFile();// FileDir + Format('report_%d.rep', [g_Link_Report_Param.Report_ID]);
 // Assert(Params.Report_ID>0);

  dmReport.SaveToFile(g_Link_Report_Param.Report_ID, sTemplateFileName);


  case g_Link_Report_Param.Category_Type of
    ctLink: begin
              dmLink_report.frxReport_base.LoadFromFile(sTemplateFileName);

              //  FfrxReport:=dmLink_report.frxReport;
              dmLink_report.ExecuteProc;
            end;

     ctLinkLine:
            begin
              dmLinkLine_report.frxReport.LoadFromFile(sTemplateFileName);
              dmLinkLine_report.ExecuteAll;
            end;

    ctLink_SDB:
            begin
           //   TdmLink_SDB_report.Init;
             Assert( assigned(dmLink_SDB_report));

              dmLink_SDB_report.frxReport1.LoadFromFile(sTemplateFileName);
              dmLink_SDB_report.ExecuteProc;
            end
  end;



  DeleteFile(sTemplateFileName);


end;




procedure Tdlg_Main_Report.row_WMSEditPropertiesEditValueChanged(
  Sender: TObject);
begin
  FChanged:=True;

end;

end.


{

procedure Tdlg_Main_Report.Button2Click(Sender: TObject);
begin
  inherited;

 //////..... dmMain.ADOConnection.Execute('DELETE FROM report');
 // dmMain.ADOConnection.Execute('DELETE FROM Map_Desktops');


end;

procedure Tdlg_Main_Report.Button5Click(Sender: TObject);
begin
/////  row_WMS_items1.Properties.Value:='asdfasdfasdfsdf';

//    ds_WMS.DataSet:= dmSettings.q_WMS;

end;





procedure Tdlg_Main_Report.pn_MainClick(Sender: TObject);
begin
  inherited;
end;





// ---------------------------------------------------------------
procedure Tdlg_Main_Report.Copy;
// ---------------------------------------------------------------
var
  s: string;
  v: Variant;
begin

  v:=row_Template_ID.Properties.Value;

//  row_Template_ID.t


  s:=InputBox('Copy','Copy','copy');

  s:='';

(*     function InputBox(const ACaption, APrompt, ADefault: string): string;
function InputBox(const ACaption, APrompt, ADefault: WideString ): WideString; overload;

*)


// don't remove
end;


// ---------------------------------------------------------------
procedure Tdlg_Main_Report.Test_Add;
// ---------------------------------------------------------------
var
  iID: Integer;
begin

  case g_Link_Report_Param.Category_Type of
    ctLink:       dmLink_report.Add_Default;
    ctLinkLine:   dmLinkLine_report.TestAdd;
    ctLink_SDB:   dmLink_SDB_report.Add_Default;
  end;


  qry_Reports.Close;
  qry_Reports.Open;

//  OpenDB();

  iID:=qry_Reports.FieldByName(FLD_ID).AsInteger;

  row_Template_ID.Properties.Value:=iID;


  cxVerticalGrid1.HideEdit;

//  cxVerticalGrid1.Focused := False;


//  cx_PostEditValue ();

 // cxVerticalGrid1.Refresh;

end;



procedure Tdlg_Main_Report.StatusBar1DblClick(Sender: TObject);
begin

  if dmMain.OpenDB_dlg then
//  if dmMain.OpenDlg(True) then
  begin
    StatusBar1.SimpleText:='Onega:'+ dmMain.LoginRec.ConnectionStatusStr;

    OpenDB();

  //  DoOnOnegaConnected;
  //  dmDB_RPLS_Lib.OpenData;
  end;

end;

