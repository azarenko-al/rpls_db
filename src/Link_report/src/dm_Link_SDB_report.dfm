inherited dmLink_SDB_report: TdmLink_SDB_report
  OldCreateOrder = True
  OnCreate = DataModuleCreate
  OnDestroy = DataModuleDestroy
  Left = 670
  Top = 281
  Height = 541
  Width = 859
  object frxDBDataset_link: TfrxDBDataset
    UserName = 'Link'
    CloseDataSource = False
    FieldAliases.Strings = (
      'name_full=name_full'
      'id=id'
      'name=name'
      'length=length'
      'length_km=length_km'
      'fade_margin_dB=fade_margin_dB'
      'rx_level_dBm=rx_level_dBm'
      'profile_XML=profile_XML'
      'PROFILE_Step=PROFILE_Step'
      'clutter_model_id=clutter_model_id'
      'tx_freq_MHz=tx_freq_MHz'
      'simple_rx_level=simple_rx_level'
      'Reliability=Reliability'
      'status_str=status_str'
      'Property1_name=Property1_name'
      'Property2_name=Property2_name'
      'Azimuth1=Azimuth1'
      'Azimuth2=Azimuth2'
      'Tilt1=Tilt1'
      'Tilt2=Tilt2'
      'lat1=lat1'
      'lon1=lon1'
      'lat2=lat2'
      'lon2=lon2'
      'Property1_lat_WGS=Property1_lat_WGS'
      'Property1_lon_WGS=Property1_lon_WGS'
      'Property2_lat_WGS=Property2_lat_WGS'
      'Property2_lon_WGS=Property2_lon_WGS'
      'LinkEndType2_name=LinkEndType2_name'
      'LinkEndType1_name=LinkEndType1_name'
      'band=band'
      'LinkEnd1_subband=LinkEnd1_subband'
      'LinkEnd2_subband=LinkEnd2_subband'
      'LinkEnd1_tx_freq_MHz=LinkEnd1_tx_freq_MHz'
      'LinkEnd2_tx_freq_MHz=LinkEnd2_tx_freq_MHz'
      'LinkEnd1_power_dBm=LinkEnd1_power_dBm'
      'LinkEnd2_power_dBm=LinkEnd2_power_dBm'
      'modulation_type=modulation_type'
      'channel_width_MHz=channel_width_MHz'
      'bitrate_Mbps=bitrate_Mbps'
      'antenna1_height=antenna1_height'
      'antenna2_height=antenna2_height'
      'antenna1_polarization_str=antenna1_polarization_str'
      'antenna2_polarization_str=antenna2_polarization_str'
      'antenna1_diameter=antenna1_diameter'
      'antenna2_diameter=antenna2_diameter'
      'antenna1_gain=antenna1_gain'
      'antenna2_gain=antenna2_gain')
    DataSet = ADOStoredProc_Link
    BCDToCurrency = True
    Left = 52
    Top = 116
  end
  object frxDBDataset_Link_img: TfrxDBDataset
    UserName = 'profile_img'
    CloseDataSource = False
    FieldAliases.Strings = (
      'RecId=RecId'
      'img=img'
      'filename=filename'
      'link_name=link_name')
    DataSet = dxMemData_Profile
    BCDToCurrency = False
    Left = 52
    Top = 332
  end
  object ADOConnection1: TADOConnection
    ConnectionString = 
      'Provider=SQLOLEDB.1;Password=sa;Persist Security Info=True;User ' +
      'ID=sa;Initial Catalog=onega_link;Data Source=server1'
    LoginPrompt = False
    Provider = 'SQLOLEDB.1'
    Left = 32
    Top = 8
  end
  object frxPDFExport1: TfrxPDFExport
    UseFileCache = True
    ShowProgress = True
    OverwritePrompt = False
    CreationTime = 41908.622654074070000000
    DataOnly = False
    OpenAfterExport = False
    PrintOptimized = False
    Outline = False
    Background = False
    HTMLTags = True
    Quality = 95
    Transparency = False
    Author = 'FastReport'
    Subject = 'FastReport PDF export'
    ProtectionFlags = [ePrint, eModify, eCopy, eAnnot]
    HideToolbar = False
    HideMenubar = False
    HideWindowUI = False
    FitWindow = False
    CenterWindow = False
    PrintScaling = False
    PdfA = False
    Left = 504
    Top = 64
  end
  object frxRTFExport1: TfrxRTFExport
    FileName = 'W:\RPLS_DB\src\Link_report\test1\1.rtf'
    UseFileCache = True
    ShowProgress = True
    OverwritePrompt = False
    CreationTime = 41908.688379803240000000
    DataOnly = False
    PictureType = gpJPG
    OpenAfterExport = False
    Wysiwyg = True
    Creator = 'FastReport'
    SuppressPageHeadersFooters = False
    HeaderFooterMode = hfText
    AutoSize = False
    Left = 504
    Top = 112
  end
  object frxXLSExport1: TfrxXLSExport
    UseFileCache = True
    ShowProgress = True
    OverwritePrompt = False
    DataOnly = False
    ExportEMF = True
    AsText = False
    Background = True
    FastExport = True
    PageBreaks = True
    EmptyLines = True
    SuppressPageHeadersFooters = False
    Left = 504
    Top = 16
  end
  object frxDesigner1: TfrxDesigner
    CloseQuery = False
    DefaultScriptLanguage = 'PascalScript'
    DefaultFont.Charset = DEFAULT_CHARSET
    DefaultFont.Color = clWindowText
    DefaultFont.Height = -13
    DefaultFont.Name = 'Arial'
    DefaultFont.Style = []
    DefaultLeftMargin = 10.000000000000000000
    DefaultRightMargin = 10.000000000000000000
    DefaultTopMargin = 10.000000000000000000
    DefaultBottomMargin = 10.000000000000000000
    DefaultPaperSize = 9
    DefaultOrientation = poPortrait
    TemplatesExt = 'fr3'
    Restrictions = [drDontSaveReport]
    RTLLanguage = False
    MemoParentFont = False
    Left = 600
    Top = 16
  end
  object dxMemData_Profile: TdxMemData
    Active = True
    Indexes = <>
    SortOptions = []
    Left = 56
    Top = 400
    object dxMemData_Profileprofile_img: TBlobField
      FieldName = 'img'
    end
    object dxMemData_Profilefilename: TStringField
      FieldName = 'filename'
      Size = 200
    end
    object dxMemData_Profilelink_name: TStringField
      FieldName = 'link_name'
      Size = 100
    end
  end
  object frxDBDataset_map: TfrxDBDataset
    UserName = 'map_img'
    CloseDataSource = False
    FieldAliases.Strings = (
      'RecId=RecId'
      'img=img'
      'filename=filename')
    DataSet = dxMemData_map
    BCDToCurrency = False
    Left = 204
    Top = 332
  end
  object dxMemData_map: TdxMemData
    Active = True
    Indexes = <>
    SortOptions = []
    Left = 208
    Top = 400
    object dxMemData_mapprofile_img: TBlobField
      FieldName = 'img'
    end
    object dxMemData_mapfilename: TStringField
      FieldName = 'filename'
      Size = 200
    end
  end
  object frxReport1: TfrxReport
    Version = '6.2.11'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports3'
    PreviewOptions.AllowEdit = False
    PreviewOptions.Buttons = [pbPrint, pbExport, pbZoom, pbTools, pbNavigator, pbExportQuick, pbNoFullScreen]
    PreviewOptions.ThumbnailVisible = True
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = #1055#1086' '#1091#1084#1086#1083#1095#1072#1085#1080#1102
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 41730.469225335600000000
    ReportOptions.LastChange = 42795.633203159700000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      ''
      '         '
      ''
      ''
      'procedure frxReportOnStartReport(Sender: TfrxComponent);'
      'const'
      '  DEF_MAP_H = 800;'
      '  '
      '  DEF_H = 450;'
      '  DEF_OFFSET = 25;'
      ''
      '  DEF_TOP = 20;                                  '
      'begin'
      '//  Picture_map.Width:=200;                '
      '  Picture_map.Height     :=DEF_MAP_H;'
      '    '
      '  Picture_profile.Height :=DEF_H;                '
      '            '
      '  '
      '  Child_map.Height:=DEF_MAP_H;'
      ''
      ''
      ' // Picture_map.Dataset:='#39'Link_img'#39';      '
      
        ' // Picture_profile.Dataset:='#39'Link_img'#39';                        ' +
        '  '
      '                                                      '
      '               '
      '  Child_profile.Height:=DEF_H + DEF_OFFSET;              '
      '        '
      '      '
      'end;  '
      '             '
      '    '
      ''
      '  '
      '//------------------------------------------------------------'
      'function DegreeToStr (aValue: double): String;'
      '//------------------------------------------------------------'
      'const'
      '//  STR_BLPOINT_FORMAT1 = '#39'%d'#176'%.2d'#39#39'%s%1.2f'#39#39#39#39#39';'
      '  STR_BLPOINT_FORMAT1 = '#39'%d'#176' %.2d'#39#39' %1.2f'#39#39#39#39#39';'
      'var    '
      ' iDeg,iMin:integer; '
      ' eSec: Double;   '
      '           '
      'begin'
      '  geo_DecodeDegree_(aValue, iDeg,iMin,eSec);'
      ' '
      '  Result:=Format(STR_BLPOINT_FORMAT1, [iDeg, iMin, eSec]);'
      ''
      'end;'
      ''
      '       '
      ''
      '//------------------------------------------------------------'
      'function DegreeToStr_lat (aValue: double): String;'
      '//------------------------------------------------------------'
      'var '
      '  s: string;'
      'begin'
      '  if aValue>0 then s:='#39'N'#39' else s:='#39'S'#39';'
      ''
      '  aValue:=Abs(aValue);       '
      ''
      '  Result:=DegreeToStr (aValue) + s;'
      'end;'
      ''
      '//------------------------------------------------------------'
      'function DegreeToStr_lon (aValue: double): String;'
      '//------------------------------------------------------------'
      'var '
      '  s: string;'
      'begin'
      '   if aValue<0 then begin'
      '     s:='#39'W'#39';'
      '     aValue:=Abs(aValue);'
      '   end else'
      ''
      '   if aValue>180 then begin'
      '     s:='#39'W'#39';'
      '     aValue:=360 - aValue'
      '   end else'
      '     s:='#39'E'#39';     '
      ''
      '  Result:=DegreeToStr (aValue) + s;'
      'end;  '
      ''
      '        '
      ''
      '//------------------------------------------------------------'
      
        'procedure geo_DecodeDegree_old(aValue: Double; var aDeg,aMin:int' +
        'eger; var aSec:'
      '    Double);'
      '//------------------------------------------------------------'
      'const'
      ' EXP_ = 0.000000001;'
      '     //97,033333333'
      'var'
      '  bIsNegative: Boolean;'
      '  e: Double;'
      '  eMinutes: Double;'
      '  eSec: Double;'
      '  eSeconds: Double;'
      'begin'
      '  bIsNegative := aValue<0;'
      ''
      '  aValue :=Abs(aValue);'
      ''
      '  aDeg:=Trunc(aValue);'
      ''
      ' // e:=Abs(aValue)*60;'
      ''
      '//  , Frac(X) = X - Int(X).'
      ''
      '  aMin:=Trunc(Frac(aValue)*60);'
      ''
      '//  Result.Sec:=Frac(Frac(Abs(aValue))*60)*60;'
      '  e:= (aValue - Trunc(aValue)) * 60;'
      '  aMin:=Trunc(e);'
      ''
      ' eMinutes := Trunc (e);'
      ' eSeconds := (e - Trunc (e)) * 60 ;'
      ''
      ''
      '  aSec:=(((aValue - aDeg) *60) - aMin) * 60;'
      ''
      '  if (aSec - 60) > EXP_ then'
      '  begin'
      '    Inc(aMin);'
      '    aSec := 0;'
      '  end;'
      ''
      ''
      '  if bIsNegative then'
      '    aDeg:=-aDeg;'
      '          '
      'end;'
      ''
      ''
      ' //------------------------------------------------------------'
      
        'procedure geo_DecodeDegree_(aValue: Double; var aDeg,aMin:intege' +
        'r; var aSec:'
      '    Double);'
      '//------------------------------------------------------------'
      'var'
      '  bIsNegative: Boolean;'
      '  e: Double;'
      '  eSec: Double;'
      ''
      'begin'
      '  bIsNegative := aValue<0;'
      ''
      '  aValue :=Abs(aValue);'
      ''
      '  aDeg:=Trunc(aValue);'
      ''
      '  aMin:=Trunc(Frac(aValue)*60);'
      ''
      ''
      '//  e:= (aValue - Trunc(aValue));'
      '  e:= (aValue - Trunc(aValue)) * 60;'
      ''
      ''
      '  aMin:=Trunc(e);'
      '  aSec:=(((aValue - aDeg) *60) - aMin) * 60;'
      ''
      '   while aSec>(60-0.1) do'
      '  begin'
      '    aMin:=aMin+1;'
      '    aSec:=aSec-60;'
      '  end;    '
      ''
      '  while aMin>=60 do'
      '  begin'
      '      aMin:=aMin-60;'
      '      aDeg:=aDeg+1;'
      '  end;  '
      ''
      '  if aSec<0 then'
      '     aSec:=0;'
      '    '
      ''
      ''
      '  if bIsNegative then'
      '    aDeg:=-aDeg;'
      ''
      ''
      ''
      'end;'
      ''
      '     '
      ''
      '       '
      'begin'
      ''
      'end.    ')
    OnStartReport = 'frxReportOnStartReport'
    Left = 342
    Top = 12
    Datasets = <
      item
        DataSet = frxDBDataset_link
        DataSetName = 'Link'
      end
      item
        DataSet = frxDBDataset_Link_img
        DataSetName = 'profile_img'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 9.260416666666670000
      BottomMargin = 10.583333333333300000
      Frame.Typ = []
      LargeDesignHeight = True
      OnBeforePrint = 'Page1OnBeforePrint'
      object Child_Header: TfrxChild
        FillType = ftBrush
        Frame.Typ = []
        Height = 187.000000000000000000
        Top = 176.000000000000000000
        Width = 718.110700000000000000
        ToNRows = 0
        ToNRowsMode = rmCount
        object Memo105: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 478.110700000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1119#1056#187#1056#1109#1057#8240#1056#176#1056#1169#1056#1108#1056#176)
          ParentFont = False
        end
        object Memo106: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 588.110700000000000000
          Width = 130.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."Property2_name"]')
          ParentFont = False
        end
        object Memo109: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 478.110700000000000000
          Top = 90.000000000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#8217#1057#8249#1057#1027#1056#1109#1057#8218#1056#176' '#1056#1111#1056#1109#1056#1169#1056#1030#1056#181#1057#1027#1056#176', m')
          ParentFont = False
        end
        object Memo110: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 478.110700000000000000
          Top = 105.000000000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1106#1056#183#1056#1105#1056#1112#1057#1107#1057#8218)
          ParentFont = False
        end
        object Memo111: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 478.110700000000000000
          Top = 120.000000000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1032#1056#1110#1056#1109#1056#187' '#1056#1112#1056#181#1057#1027#1057#8218#1056#176)
          ParentFont = False
        end
        object Memo112: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 478.110700000000000000
          Top = 135.000000000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#8221#1056#1105#1056#176#1056#1112#1056#181#1057#8218#1057#1026' '#1056#176#1056#1029#1057#8218#1056#181#1056#1029#1056#1029#1057#8249)
          ParentFont = False
        end
        object Memo115: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 588.110700000000000000
          Top = 90.000000000000000000
          Width = 130.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[antenna2."height"]')
          ParentFont = False
        end
        object Memo116: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 588.110700000000000000
          Top = 105.000000000000000000
          Width = 130.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."azimuth2"]')
          ParentFont = False
        end
        object Memo117: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 588.110700000000000000
          Top = 135.000000000000000000
          Width = 130.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Antenna2."diameter"]')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 588.110700000000000000
          Top = 120.000000000000000000
          Width = 130.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Antenna2."tilt"]')
          ParentFont = False
        end
        object Memo66: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 478.110700000000000000
          Top = 150.000000000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1032#1057#1027#1056#1105#1056#187#1056#181#1056#1029#1056#1105#1056#181' '#1056#176#1056#1029#1057#8218#1056#181#1056#1029#1056#1029#1057#8249)
          ParentFont = False
        end
        object Memo99: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 588.110700000000000000
          Top = 150.000000000000000000
          Width = 130.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Antenna2."gain"]')
          ParentFont = False
        end
        object Memo43: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1119#1056#187#1056#1109#1057#8240#1056#176#1056#1169#1056#1108#1056#176)
          ParentFont = False
        end
        object Memo_left_1: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 110.000000000000000000
          Width = 125.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."Property1_name"]')
          ParentFont = False
        end
        object Memo70: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 90.000000000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#8217#1057#8249#1057#1027#1056#1109#1057#8218#1056#176' '#1056#1111#1056#1109#1056#1169#1056#1030#1056#181#1057#1027#1056#176', m')
          ParentFont = False
        end
        object Memo72: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 105.000000000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1106#1056#183#1056#1105#1056#1112#1057#1107#1057#8218)
          ParentFont = False
        end
        object Memo74: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 120.000000000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1032#1056#1110#1056#1109#1056#187' '#1056#1112#1056#181#1057#1027#1057#8218#1056#176)
          ParentFont = False
        end
        object Memo100: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 135.000000000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#8221#1056#1105#1056#176#1056#1112#1056#181#1057#8218#1057#1026' '#1056#176#1056#1029#1057#8218#1056#181#1056#1029#1056#1029#1057#8249)
          ParentFont = False
        end
        object Memo1: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 110.000000000000000000
          Top = 90.000000000000000000
          Width = 125.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[antenna1."height"]')
          ParentFont = False
        end
        object Memo95: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 110.000000000000000000
          Top = 105.000000000000000000
          Width = 125.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."azimuth1"]')
          ParentFont = False
        end
        object Memo97: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 110.000000000000000000
          Top = 135.000000000000000000
          Width = 125.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[antenna1."diameter"]')
          ParentFont = False
        end
        object Memo96: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 110.000000000000000000
          Top = 120.000000000000000000
          Width = 125.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Antenna1."tilt"]')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 150.000000000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1032#1057#1027#1056#1105#1056#187#1056#181#1056#1029#1056#1105#1056#181' '#1056#176#1056#1029#1057#8218#1056#181#1056#1029#1056#1029#1057#8249)
          ParentFont = False
        end
        object Memo30: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 110.000000000000000000
          Top = 150.000000000000000000
          Width = 125.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[antenna1."gain"]')
          ParentFont = False
        end
        object Memo45: TfrxMemoView
          AllowVectorExport = True
          Left = 243.220470000000000000
          Top = 15.000000000000000000
          Width = 144.204726490000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#8221#1056#187#1056#1105#1056#1029#1056#176' '#1056#1105#1056#1029#1057#8218#1056#181#1057#1026#1056#1030#1056#176#1056#187#1056#176', km')
          ParentFont = False
        end
        object Memo47: TfrxMemoView
          AllowVectorExport = True
          Left = 243.220470000000000000
          Top = 30.000000000000000000
          Width = 144.204726490000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1032#1057#1026#1056#1109#1056#1030#1056#181#1056#1029#1057#1034' '#1057#1027#1056#1105#1056#1110#1056#1029#1056#176#1056#187#1056#176', dBm')
          ParentFont = False
        end
        object Memo49: TfrxMemoView
          AllowVectorExport = True
          Left = 243.220470000000000000
          Top = 45.000000000000000000
          Width = 144.204726490000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#160#1056#176#1056#177#1056#1109#1057#8225#1056#176#1057#1039' '#1057#8225#1056#176#1057#1027#1057#8218#1056#1109#1057#8218#1056#176', GHz')
          ParentFont = False
        end
        object Memo51: TfrxMemoView
          AllowVectorExport = True
          Left = 243.220470000000000000
          Top = 60.000000000000000000
          Width = 144.204726490000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1114#1056#1109#1057#8240#1056#1029#1056#1109#1057#1027#1057#8218#1057#1034' '#1056#1111#1056#181#1057#1026#1056#181#1056#1169#1056#176#1057#8218#1057#8225#1056#1105#1056#1108#1056#176', dBm')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Left = 243.220470000000000000
          Top = 75.000000000000000000
          Width = 144.204726490000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1038#1056#1108#1056#1109#1057#1026#1056#1109#1057#1027#1057#8218#1057#1034' '#1056#1111#1056#181#1057#1026#1056#181#1056#1169#1056#176#1057#8225#1056#1105', Mbit/s')
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 243.220470000000000000
          Top = 90.000000000000000000
          Width = 144.204726490000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1118#1056#1105#1056#1111' '#1056#1109#1056#177#1056#1109#1057#1026#1057#1107#1056#1169#1056#1109#1056#1030#1056#176#1056#1029#1056#1105#1057#1039)
          ParentFont = False
        end
        object Memo123: TfrxMemoView
          AllowVectorExport = True
          Left = 387.425196490000000000
          Top = 30.000000000000000000
          Width = 74.905973510000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."rx_level_dBm"]')
          ParentFont = False
        end
        object Memo124: TfrxMemoView
          AllowVectorExport = True
          Left = 387.425196490000000000
          Top = 75.000000000000000000
          Width = 74.905973510000000000
          Height = 15.000000000000000000
          DataField = 'bitrate_Mbps'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd1."bitrate_Mbps"]')
          ParentFont = False
        end
        object Memo125: TfrxMemoView
          AllowVectorExport = True
          Left = 387.425196490000000000
          Top = 90.000000000000000000
          Width = 74.905973510000000000
          Height = 15.000000000000000000
          DataField = 'LinkEndType_name'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd1."LinkEndType_name"]')
          ParentFont = False
        end
        object Memo127: TfrxMemoView
          AllowVectorExport = True
          Left = 387.425196490000000000
          Top = 45.000000000000000000
          Width = 74.905973510000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[FloatToStr( <LinkEnd1."tx_freq_MHz"> / 1000 )]')
          ParentFont = False
        end
        object Memo128: TfrxMemoView
          AllowVectorExport = True
          Left = 387.425196490000000000
          Top = 15.000000000000000000
          Width = 74.905973510000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."length_km"]')
          ParentFont = False
        end
        object Memo126: TfrxMemoView
          AllowVectorExport = True
          Left = 387.425196490000000000
          Top = 60.000000000000000000
          Width = 74.905973510000000000
          Height = 15.000000000000000000
          DataField = 'power_dBm'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd1."power_dBm"]')
          ParentFont = False
        end
        object Memo42: TfrxMemoView
          AllowVectorExport = True
          Left = 243.220470000000000000
          Width = 144.204726490000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1119#1057#1026#1056#1105#1056#183#1056#1029#1056#176#1056#1108' '#1056#1111#1057#1026#1056#1105#1056#1110#1056#1109#1056#1169#1056#1029#1056#1109#1057#1027#1057#8218#1056#1105)
          ParentFont = False
        end
        object Memo122: TfrxMemoView
          AllowVectorExport = True
          Left = 387.425196490000000000
          Width = 74.905973510000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."status_str"]')
          ParentFont = False
        end
        object Memo134: TfrxMemoView
          AllowVectorExport = True
          Left = 243.220470000000000000
          Top = 105.000000000000000000
          Width = 144.204726490000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#8212#1056#176#1056#1111#1056#176#1057#1027' '#1056#1029#1056#176' '#1056#183#1056#176#1056#1112#1056#1105#1057#1026#1056#176#1056#1029#1056#1105#1057#1039', dB')
          ParentFont = False
        end
        object Memo135: TfrxMemoView
          AllowVectorExport = True
          Left = 387.425196490000000000
          Top = 105.000000000000000000
          Width = 74.905973510000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."fade_margin_dB"]')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 60.000000000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1113#1056#1109#1056#1109#1057#1026#1056#1169#1056#1105#1056#1029#1056#176#1057#8218#1057#8249' WGS')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 110.000000000000000000
          Top = 60.000000000000000000
          Width = 125.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[DegreeToStr (<Site1."lat_WGS">)]')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 110.000000000000000000
          Top = 75.000000000000000000
          Width = 125.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[DegreeToStr (<Site1."lon_WGS">)]')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 75.000000000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 478.110700000000000000
          Top = 60.000000000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          DataSetName = 'Site'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1113#1056#1109#1056#1109#1057#1026#1056#1169#1056#1105#1056#1029#1056#176#1057#8218#1057#8249' WGS')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 588.110700000000000000
          Top = 60.000000000000000000
          Width = 130.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[DegreeToStr (<Site2."lat_wgs">)]')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 588.110700000000000000
          Top = 75.000000000000000000
          Width = 130.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[DegreeToStr (<Site2."lon_wgs">)]')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 478.110700000000000000
          Top = 75.000000000000000000
          Width = 110.000000000000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          ParentFont = False
        end
        object Memo26: TfrxMemoView
          AllowVectorExport = True
          Left = 245.669450000000000000
          Top = 132.283550000000000000
          Width = 144.881880000000000000
          Height = 15.000000000000000000
          DataSet = frxDBDataset_link
          DataSetName = 'Link'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1114#1056#1109#1056#1169#1057#1107#1056#187#1057#1039#1057#8224#1056#1105#1057#1039)
          ParentFont = False
        end
        object Memo27: TfrxMemoView
          AllowVectorExport = True
          Left = 392.441411100000000000
          Top = 132.283550000000000000
          Width = 71.811026060000000000
          Height = 15.000000000000000000
          DataSet = frxDBDataset_link
          DataSetName = 'Link'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd1."modulation_type"]')
          ParentFont = False
        end
        object Memo28: TfrxMemoView
          AllowVectorExport = True
          Left = 245.669450000000000000
          Top = 147.401670000000000000
          Width = 144.881880000000000000
          Height = 15.000000000000000000
          DataSet = frxDBDataset_link
          DataSetName = 'Link'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1025#1056#1105#1057#1026#1056#1105#1056#1029#1056#176' '#1056#1111#1056#1109#1056#187#1056#1109#1057#1027#1057#8249', MHz')
          ParentFont = False
        end
        object Memo29: TfrxMemoView
          AllowVectorExport = True
          Left = 392.441411100000000000
          Top = 147.401670000000000000
          Width = 71.811026060000000000
          Height = 15.000000000000000000
          DataSet = frxDBDataset_link
          DataSetName = 'Link'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[LinkEnd1."channel_width_MHz"]')
          ParentFont = False
        end
      end
      object MasterData3: TfrxMasterData
        FillType = ftBrush
        Frame.Typ = []
        Height = 52.000000000000000000
        Top = 104.000000000000000000
        Width = 718.110700000000000000
        DataSet = frxDBDataset_link
        DataSetName = 'Link'
        RowCount = 0
        object Memo7: TfrxMemoView
          Align = baWidth
          AllowVectorExport = True
          Width = 718.110700000000000000
          Height = 19.000000000000000000
          AutoWidth = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8 = (
            
              #1056#1115#1057#8218#1057#8225#1056#181#1057#8218' '#1056#1111#1056#1109' '#1057#1026#1056#176#1056#1169#1056#1105#1056#1109#1057#1026#1056#181#1056#187#1056#181#1056#8470#1056#1029#1056#1109#1056#1112#1057#1107' '#1056#1105#1056#1029#1057#8218#1056#181#1057#1026#1056#1030#1056#176#1056#187#1057#1107' ' +
              'SDB')
          ParentFont = False
        end
        object Linkname: TfrxMemoView
          Align = baWidth
          AllowVectorExport = True
          Top = 28.000000000000000000
          Width = 718.110700000000000000
          Height = 16.000000000000000000
          AutoWidth = True
          DataField = 'name'
          Frame.Typ = []
          Memo.UTF8 = (
            '[Link."name"]')
        end
      end
      object Child_profile: TfrxChild
        FillType = ftBrush
        Frame.Typ = []
        Height = 58.400000000000000000
        Top = 440.000000000000000000
        Width = 718.110700000000000000
        ToNRows = 0
        ToNRowsMode = rmCount
        object Picture_profile: TfrxPictureView
          Align = baWidth
          AllowVectorExport = True
          Top = 20.000000000000000000
          Width = 718.110700000000000000
          Height = 36.000000000000000000
          OnBeforePrint = 'Picture_profileOnBeforePrint'
          DataField = 'profile_img'
          DataSet = frxDBDataset_Link_img
          DataSetName = 'profile_img'
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Stretched = False
          HightQuality = True
          Transparent = False
          TransparentColor = clWhite
        end
        object Memo80: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          ShiftMode = smWhenOverlapped
          Width = 363.200000000000000000
          Height = 16.800000000000000000
          AutoWidth = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8 = (
            
              #1056#152#1056#183#1056#1109#1056#177#1057#1026#1056#176#1056#182#1056#181#1056#1029#1056#1105#1056#181' '#1056#1111#1057#1026#1056#1109#1057#8222#1056#1105#1056#187#1057#1039' '#1056#1111#1057#1026#1056#1105' '#1056#1029#1056#1109#1057#1026#1056#1112#1056#176#1056#187#1057#1034#1056#1029#1056#1109#1056 +
              #8470' '#1057#1026#1056#181#1057#8222#1057#1026#1056#176#1056#1108#1057#8224#1056#1105#1056#1105)
          ParentFont = False
        end
      end
      object Child_map: TfrxChild
        FillType = ftBrush
        Frame.Typ = []
        Height = 36.800000000000000000
        Top = 384.000000000000000000
        Width = 718.110700000000000000
        OnAfterCalcHeight = 'Child_mapOnAfterCalcHeight'
        OnBeforePrint = 'Child_mapOnBeforePrint'
        AllowSplit = True
        ToNRows = 0
        ToNRowsMode = rmCount
        object Picture_map: TfrxPictureView
          Description = 'Map'
          Align = baWidth
          AllowVectorExport = True
          Width = 718.110700000000000000
          Height = 28.000000000000000000
          OnBeforePrint = 'Picture_mapOnBeforePrint'
          DataField = 'map_img'
          DataSet = frxDBDataset_Link_img
          DataSetName = 'profile_img'
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Stretched = False
          HightQuality = True
          Transparent = False
          TransparentColor = clWhite
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        Frame.Typ = []
        Height = 24.000000000000000000
        Top = 560.000000000000000000
        Width = 718.110700000000000000
        object Page: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 638.110700000000000000
          Width = 80.000000000000000000
          Height = 16.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8 = (
            '[Page#]')
          ParentFont = False
        end
        object Memo138: TfrxMemoView
          Align = baWidth
          AllowVectorExport = True
          Width = 638.110700000000000000
          Height = 16.000000000000000000
          AutoWidth = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clSilver
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8 = (
            
              #1056#1115#1057#8218#1057#8225#1056#181#1057#8218' '#1056#1030#1057#8249#1056#1111#1056#1109#1056#187#1056#1029#1056#181#1056#1029' '#1056#1030' '#1056#1111#1057#1026#1056#1109#1056#1110#1057#1026#1056#176#1056#1112#1056#1112#1056#181' ONEPLAN RPLS-D' +
              'B Link')
          ParentFont = False
        end
      end
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        Frame.Typ = []
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        Height = 28.000000000000000000
        ParentFont = False
        Top = 16.000000000000000000
        Width = 718.110700000000000000
        object Linkcalc_method_name1: TfrxMemoView
          Align = baWidth
          AllowVectorExport = True
          Left = 141.600000000000000000
          Width = 576.510700000000000000
          Height = 16.000000000000000000
          AutoWidth = True
          DataField = 'calc_method_name'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clSilver
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8 = (
            '[Link."calc_method_name"]')
          ParentFont = False
        end
        object Memo137: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Width = 141.600000000000000000
          Height = 16.000000000000000000
          AutoWidth = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clSilver
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8 = (
            #1056#1114#1056#181#1057#8218#1056#1109#1056#1169#1056#1105#1056#1108#1056#176' '#1057#1026#1056#176#1057#1027#1057#8225#1056#181#1057#8218#1056#176': ')
          ParentFont = False
        end
      end
    end
    object Page2: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
    end
  end
  object ADOStoredProc_Link: TADOStoredProc
    Connection = ADOConnection1
    CursorType = ctStatic
    LockType = ltBatchOptimistic
    ProcedureName = 'link_sdb.sp_link_SDB_report_SEL'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@ID'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 2041
      end>
    Left = 56
    Top = 200
  end
  object ADOStoredProc_Link_SDB: TADOStoredProc
    Connection = ADOConnection1
    CursorType = ctStatic
    ProcedureName = 'link_sdb.sp_link_SDB_report_summary_SEL'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@ID'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 2041
      end>
    Left = 184
    Top = 200
  end
  object frxDBDataset_Header: TfrxDBDataset
    UserName = 'Summary'
    CloseDataSource = False
    FieldAliases.Strings = (
      'calc_method_name=calc_method_name')
    DataSet = ADOStoredProc_Link_SDB
    BCDToCurrency = True
    Left = 188
    Top = 116
  end
  object frxReport_SDB: TfrxReport
    Version = '6.2.11'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports3'
    PreviewOptions.AllowEdit = False
    PreviewOptions.Buttons = [pbPrint, pbExport, pbZoom, pbTools, pbNavigator, pbExportQuick, pbNoFullScreen]
    PreviewOptions.ThumbnailVisible = True
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = #1055#1086' '#1091#1084#1086#1083#1095#1072#1085#1080#1102
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 41730.469225335600000000
    ReportOptions.LastChange = 42795.633203159700000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      ''
      '         '
      ''
      ''
      'procedure frxReportOnStartReport(Sender: TfrxComponent);'
      'const'
      '  DEF_MAP_H = 800;'
      '  '
      '  DEF_H = 450;'
      '  DEF_OFFSET = 25;'
      ''
      '  DEF_TOP = 20;                                  '
      'begin'
      '//  Picture_map.Width:=200;                '
      '//  Picture_map.Height     :=DEF_MAP_H;'
      '//  Picture_profile.Height :=DEF_H;                '
      '            '
      '  Picture_map_.Height     :=DEF_MAP_H;'
      '  Picture_profile_.Height :=DEF_H;                '
      '  '
      '  '
      '//  Child_map.Height:=DEF_MAP_H;'
      '//  Child_profile.Height:=DEF_H + DEF_OFFSET;              '
      ''
      '  MasterData_map.Height:=DEF_MAP_H;        '
      '  MasterData_profile.Height:=DEF_H + DEF_OFFSET;'
      '                  '
      ' // Picture_map.Dataset:='#39'Link_img'#39';      '
      
        ' // Picture_profile.Dataset:='#39'Link_img'#39';                        ' +
        '  '
      '                                                      '
      '               '
      '        '
      '      '
      'end;  '
      '             '
      '    '
      '           '
      ''
      '       '
      'begin'
      ''
      'end.    ')
    OnStartReport = 'frxReportOnStartReport'
    Left = 174
    Top = 12
    Datasets = <
      item
        DataSet = frxDBDataset_items
        DataSetName = 'items'
      end
      item
        DataSet = frxDBDataset_link
        DataSetName = 'Link'
      end
      item
        DataSet = frxDBDataset_map
        DataSetName = 'map_img'
      end
      item
        DataSet = frxDBDataset_Link_img
        DataSetName = 'profile_img'
      end
      item
        DataSet = frxDBDataset_Header
        DataSetName = 'Summary'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 5.291666666666670000
      RightMargin = 5.291666666666670000
      TopMargin = 9.260416666666670000
      BottomMargin = 10.583333333333300000
      Frame.Typ = []
      LargeDesignHeight = True
      OnBeforePrint = 'Page1OnBeforePrint'
      object Child_Header: TfrxChild
        FillType = ftBrush
        Frame.Typ = []
        Height = 243.000000000000000000
        Top = 294.803340000000000000
        Width = 753.701274166666600000
        ToNRows = 0
        ToNRowsMode = rmCount
        object Memo105: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 488.701274166666600000
          Width = 155.000000000000000000
          Height = 16.000000000000000000
          DataSet = frxDBDataset_link
          DataSetName = 'Link'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1119#1056#187#1056#1109#1057#8240#1056#176#1056#1169#1056#1108#1056#176)
          ParentFont = False
        end
        object Memo106: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 643.701274166666600000
          Width = 110.000000000000000000
          Height = 16.000000000000000000
          DataSet = frxDBDataset_link
          DataSetName = 'Link'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."Property2_name"]')
          ParentFont = False
          UseDefaultCharset = True
        end
        object Memo109: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 488.701274166666600000
          Top = 64.000000000000000000
          Width = 155.000000000000000000
          Height = 16.000000000000000000
          DataSet = frxDBDataset_link
          DataSetName = 'Link'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#8217#1057#8249#1057#1027#1056#1109#1057#8218#1056#176' '#1056#1111#1056#1109#1056#1169#1056#1030#1056#181#1057#1027#1056#176', m')
          ParentFont = False
        end
        object Memo110: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 488.701274166666600000
          Top = 96.000000000000000000
          Width = 155.000000000000000000
          Height = 16.000000000000000000
          DataSet = frxDBDataset_link
          DataSetName = 'Link'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1106#1056#183#1056#1105#1056#1112#1057#1107#1057#8218', '#1042#176)
          ParentFont = False
        end
        object Memo111: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 488.701274166666600000
          Top = 208.000000000000000000
          Width = 155.000000000000000000
          Height = 16.000000000000000000
          DataSet = frxDBDataset_link
          DataSetName = 'Link'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1032#1057#1026#1056#1109#1056#1030#1056#181#1056#1029#1057#1034' '#1057#1027#1056#1105#1056#1110#1056#1029#1056#176#1056#187#1056#176', dBm')
          ParentFont = False
        end
        object Memo112: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 488.701274166666600000
          Top = 48.000000000000000000
          Width = 155.000000000000000000
          Height = 16.000000000000000000
          DataSet = frxDBDataset_link
          DataSetName = 'Link'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#8221#1056#1105#1056#176#1056#1112#1056#181#1057#8218#1057#1026' '#1056#176#1056#1029#1057#8218#1056#181#1056#1029#1056#1029#1057#8249', m')
          ParentFont = False
        end
        object Memo115: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 643.701274166666600000
          Top = 64.000000000000000000
          Width = 110.000000000000000000
          Height = 16.000000000000000000
          DataSet = frxDBDataset_link
          DataSetName = 'Link'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."antenna2_height"]')
          ParentFont = False
          UseDefaultCharset = True
        end
        object Memo116: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 643.701274166666600000
          Top = 96.000000000000000000
          Width = 110.000000000000000000
          Height = 16.000000000000000000
          DataSet = frxDBDataset_link
          DataSetName = 'Link'
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."Azimuth2"]')
          ParentFont = False
          UseDefaultCharset = True
        end
        object Memo117: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 643.701274166666600000
          Top = 48.000000000000000000
          Width = 110.000000000000000000
          Height = 16.000000000000000000
          DataSet = frxDBDataset_link
          DataSetName = 'Link'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."antenna2_diameter"]')
          ParentFont = False
          UseDefaultCharset = True
        end
        object Memo5: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 643.701274166666600000
          Top = 208.000000000000000000
          Width = 110.000000000000000000
          Height = 16.000000000000000000
          DataSet = frxDBDataset_link
          DataSetName = 'Link'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."rx_level_dBm"]')
          ParentFont = False
          UseDefaultCharset = True
        end
        object Memo66: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 488.701274166666600000
          Top = 176.000000000000000000
          Width = 155.000000000000000000
          Height = 16.000000000000000000
          DataSet = frxDBDataset_link
          DataSetName = 'Link'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1114#1056#1109#1057#8240#1056#1029#1056#1109#1057#1027#1057#8218#1057#1034' '#1056#1111#1056#181#1057#1026#1056#181#1056#1169#1056#176#1057#8218#1057#8225#1056#1105#1056#1108#1056#176', dBm')
          ParentFont = False
        end
        object Memo99: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 643.701274166666600000
          Top = 176.000000000000000000
          Width = 110.000000000000000000
          Height = 16.000000000000000000
          DataSet = frxDBDataset_link
          DataSetName = 'Link'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."LinkEnd2_power_dBm"]')
          ParentFont = False
          UseDefaultCharset = True
        end
        object Memo43: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Width = 155.000000000000000000
          Height = 16.000000000000000000
          DataSet = frxDBDataset_link
          DataSetName = 'Link'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1119#1056#187#1056#1109#1057#8240#1056#176#1056#1169#1056#1108#1056#176)
          ParentFont = False
        end
        object Memo_left_1: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 155.000000000000000000
          Width = 110.000000000000000000
          Height = 16.000000000000000000
          DataSet = frxDBDataset_link
          DataSetName = 'Link'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."Property1_name"]')
          ParentFont = False
        end
        object Memo70: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 64.000000000000000000
          Width = 155.000000000000000000
          Height = 16.000000000000000000
          DataSet = frxDBDataset_link
          DataSetName = 'Link'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#8217#1057#8249#1057#1027#1056#1109#1057#8218#1056#176' '#1056#1111#1056#1109#1056#1169#1056#1030#1056#181#1057#1027#1056#176', m')
          ParentFont = False
        end
        object Memo72: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 96.000000000000000000
          Width = 155.000000000000000000
          Height = 16.000000000000000000
          DataSet = frxDBDataset_link
          DataSetName = 'Link'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1106#1056#183#1056#1105#1056#1112#1057#1107#1057#8218', '#1042#176)
          ParentFont = False
        end
        object Memo74: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 112.000000000000000000
          Width = 155.000000000000000000
          Height = 16.000000000000000000
          DataSet = frxDBDataset_link
          DataSetName = 'Link'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1032#1056#1110#1056#1109#1056#187' '#1056#1112#1056#181#1057#1027#1057#8218#1056#176', '#1042#176)
          ParentFont = False
        end
        object Memo100: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 48.000000000000000000
          Width = 155.000000000000000000
          Height = 16.000000000000000000
          DataSet = frxDBDataset_link
          DataSetName = 'Link'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#8221#1056#1105#1056#176#1056#1112#1056#181#1057#8218#1057#1026' '#1056#176#1056#1029#1057#8218#1056#181#1056#1029#1056#1029#1057#8249', m')
          ParentFont = False
        end
        object Memo1: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 155.000000000000000000
          Top = 64.000000000000000000
          Width = 110.000000000000000000
          Height = 16.000000000000000000
          DataSet = frxDBDataset_link
          DataSetName = 'Link'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."antenna1_height"]')
          ParentFont = False
        end
        object Memo95: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 155.000000000000000000
          Top = 96.000000000000000000
          Width = 110.000000000000000000
          Height = 16.000000000000000000
          DataSet = frxDBDataset_link
          DataSetName = 'Link'
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."azimuth1"]')
          ParentFont = False
        end
        object Memo97: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 155.000000000000000000
          Top = 48.000000000000000000
          Width = 110.000000000000000000
          Height = 16.000000000000000000
          DataSet = frxDBDataset_link
          DataSetName = 'Link'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."antenna1_diameter"]')
          ParentFont = False
        end
        object Memo96: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 155.000000000000000000
          Top = 112.000000000000000000
          Width = 110.000000000000000000
          Height = 16.000000000000000000
          DataSet = frxDBDataset_link
          DataSetName = 'Link'
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."Tilt1"]')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 176.000000000000000000
          Width = 155.000000000000000000
          Height = 16.000000000000000000
          DataSet = frxDBDataset_link
          DataSetName = 'Link'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1114#1056#1109#1057#8240#1056#1029#1056#1109#1057#1027#1057#8218#1057#1034' '#1056#1111#1056#181#1057#1026#1056#181#1056#1169#1056#176#1057#8218#1057#8225#1056#1105#1056#1108#1056#176', dBm')
          ParentFont = False
        end
        object Memo30: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 155.000000000000000000
          Top = 176.000000000000000000
          Width = 110.000000000000000000
          Height = 16.000000000000000000
          DataSet = frxDBDataset_link
          DataSetName = 'Link'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."LinkEnd1_power_dBm"]')
          ParentFont = False
        end
        object Memo45: TfrxMemoView
          AllowVectorExport = True
          Left = 270.000000000000000000
          Top = 16.000000000000000000
          Width = 144.000000000000000000
          Height = 16.000000000000000000
          DataSet = frxDBDataset_link
          DataSetName = 'Link'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#8221#1056#187#1056#1105#1056#1029#1056#176' '#1056#1105#1056#1029#1057#8218#1056#181#1057#1026#1056#1030#1056#176#1056#187#1056#176', km')
          ParentFont = False
        end
        object Memo47: TfrxMemoView
          AllowVectorExport = True
          Left = 270.000000000000000000
          Top = 32.000000000000000000
          Width = 144.000000000000000000
          Height = 16.000000000000000000
          DataSet = frxDBDataset_link
          DataSetName = 'Link'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#8221#1056#1105#1056#176#1056#1111#1056#176#1056#183#1056#1109#1056#1029', GHz')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Left = 270.000000000000000000
          Top = 96.000000000000000000
          Width = 144.000000000000000000
          Height = 16.000000000000000000
          DataSet = frxDBDataset_link
          DataSetName = 'Link'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1038#1056#1108#1056#1109#1057#1026#1056#1109#1057#1027#1057#8218#1057#1034' '#1056#1111#1056#181#1057#1026#1056#181#1056#1169#1056#176#1057#8225#1056#1105', Mbit/s')
          ParentFont = False
        end
        object Memo123: TfrxMemoView
          AllowVectorExport = True
          Left = 414.000000000000000000
          Top = 32.000000000000000000
          Width = 65.000000000000000000
          Height = 16.000000000000000000
          DataSet = frxDBDataset_link
          DataSetName = 'Link'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."band"]')
          ParentFont = False
        end
        object Memo124: TfrxMemoView
          AllowVectorExport = True
          Left = 414.000000000000000000
          Top = 96.000000000000000000
          Width = 65.000000000000000000
          Height = 16.000000000000000000
          DataSet = frxDBDataset_link
          DataSetName = 'Link'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."bitrate_Mbps"]')
          ParentFont = False
        end
        object Memo128: TfrxMemoView
          AllowVectorExport = True
          Left = 414.000000000000000000
          Top = 16.000000000000000000
          Width = 65.000000000000000000
          Height = 16.000000000000000000
          DataSet = frxDBDataset_link
          DataSetName = 'Link'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."length_km"]')
          ParentFont = False
        end
        object Memo42: TfrxMemoView
          AllowVectorExport = True
          Left = 270.000000000000000000
          Width = 144.000000000000000000
          Height = 16.000000000000000000
          DataSet = frxDBDataset_link
          DataSetName = 'Link'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1119#1057#1026#1056#1105#1056#183#1056#1029#1056#176#1056#1108' '#1056#1111#1057#1026#1056#1105#1056#1110#1056#1109#1056#1169#1056#1029#1056#1109#1057#1027#1057#8218#1056#1105)
          ParentFont = False
        end
        object Memo122: TfrxMemoView
          AllowVectorExport = True
          Left = 414.000000000000000000
          Width = 65.000000000000000000
          Height = 16.000000000000000000
          DataSet = frxDBDataset_link
          DataSetName = 'Link'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."status_str"]')
          ParentFont = False
        end
        object Memo134: TfrxMemoView
          AllowVectorExport = True
          Left = 270.000000000000000000
          Top = 128.000000000000000000
          Width = 144.000000000000000000
          Height = 16.000000000000000000
          DataSet = frxDBDataset_link
          DataSetName = 'Link'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#8212#1056#176#1056#1111#1056#176#1057#1027' '#1056#1029#1056#176' '#1056#183#1056#176#1056#1112#1056#1105#1057#1026#1056#176#1056#1029#1056#1105#1057#1039', dB')
          ParentFont = False
        end
        object Memo135: TfrxMemoView
          AllowVectorExport = True
          Left = 414.000000000000000000
          Top = 128.000000000000000000
          Width = 65.000000000000000000
          Height = 16.000000000000000000
          DataSet = frxDBDataset_link
          DataSetName = 'Link'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."fade_margin_dB"]')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 16.000000000000000000
          Width = 155.000000000000000000
          Height = 16.000000000000000000
          DataSet = frxDBDataset_link
          DataSetName = 'Link'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1113#1056#1109#1056#1109#1057#1026#1056#1169#1056#1105#1056#1029#1056#176#1057#8218#1057#8249' WGS')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 155.000000000000000000
          Top = 16.000000000000000000
          Width = 110.000000000000000000
          Height = 16.000000000000000000
          DataSet = frxDBDataset_link
          DataSetName = 'Link'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."Property1_lat_WGS"]')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 155.000000000000000000
          Top = 32.000000000000000000
          Width = 110.000000000000000000
          Height = 16.000000000000000000
          DataSet = frxDBDataset_link
          DataSetName = 'Link'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."Property1_lon_WGS"]')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 32.000000000000000000
          Width = 155.000000000000000000
          Height = 16.000000000000000000
          DataSet = frxDBDataset_link
          DataSetName = 'Link'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 488.701274166666600000
          Top = 16.000000000000000000
          Width = 155.000000000000000000
          Height = 16.000000000000000000
          DataSet = frxDBDataset_link
          DataSetName = 'Link'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1113#1056#1109#1056#1109#1057#1026#1056#1169#1056#1105#1056#1029#1056#176#1057#8218#1057#8249' WGS')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 643.701274166666600000
          Top = 16.000000000000000000
          Width = 110.000000000000000000
          Height = 16.000000000000000000
          DataSet = frxDBDataset_link
          DataSetName = 'Link'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."Property2_lat_WGS"]')
          ParentFont = False
          UseDefaultCharset = True
        end
        object Memo17: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 643.701274166666600000
          Top = 32.000000000000000000
          Width = 110.000000000000000000
          Height = 16.000000000000000000
          DataSet = frxDBDataset_link
          DataSetName = 'Link'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."Property2_lon_WGS"]')
          ParentFont = False
          UseDefaultCharset = True
        end
        object Memo18: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 488.701274166666600000
          Top = 32.000000000000000000
          Width = 155.000000000000000000
          Height = 16.000000000000000000
          DataSet = frxDBDataset_link
          DataSetName = 'Link'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          ParentFont = False
        end
        object Memo26: TfrxMemoView
          AllowVectorExport = True
          Left = 270.000000000000000000
          Top = 80.000000000000000000
          Width = 144.000000000000000000
          Height = 16.000000000000000000
          DataSet = frxDBDataset_link
          DataSetName = 'Link'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1114#1056#1109#1056#1169#1057#1107#1056#187#1057#1039#1057#8224#1056#1105#1057#1039)
          ParentFont = False
        end
        object Memo27: TfrxMemoView
          AllowVectorExport = True
          Left = 414.000000000000000000
          Top = 80.000000000000000000
          Width = 65.000000000000000000
          Height = 16.000000000000000000
          DataSet = frxDBDataset_link
          DataSetName = 'Link'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."modulation_type"]')
          ParentFont = False
        end
        object Memo28: TfrxMemoView
          AllowVectorExport = True
          Left = 270.000000000000000000
          Top = 64.000000000000000000
          Width = 144.000000000000000000
          Height = 16.000000000000000000
          DataSet = frxDBDataset_link
          DataSetName = 'Link'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1025#1056#1105#1057#1026#1056#1105#1056#1029#1056#176' '#1056#1111#1056#1109#1056#187#1056#1109#1057#1027#1057#8249', MHz')
          ParentFont = False
        end
        object Memo29: TfrxMemoView
          AllowVectorExport = True
          Left = 414.000000000000000000
          Top = 64.000000000000000000
          Width = 65.000000000000000000
          Height = 16.000000000000000000
          DataSet = frxDBDataset_link
          DataSetName = 'Link'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."channel_width_MHz"]')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 80.000000000000000000
          Width = 155.000000000000000000
          Height = 16.000000000000000000
          DataSet = frxDBDataset_link
          DataSetName = 'Link'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1119#1056#1109#1056#187#1057#1039#1057#1026#1056#1105#1056#183#1056#176#1057#8224#1056#1105#1057#1039' ')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 155.000000000000000000
          Top = 80.000000000000000000
          Width = 110.000000000000000000
          Height = 16.000000000000000000
          DataSet = frxDBDataset_link
          DataSetName = 'Link'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."antenna1_polarization_str"]')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 488.701274166666600000
          Top = 80.000000000000000000
          Width = 155.000000000000000000
          Height = 16.000000000000000000
          DataSet = frxDBDataset_link
          DataSetName = 'Link'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1119#1056#1109#1056#187#1057#1039#1057#1026#1056#1105#1056#183#1056#176#1057#8224#1056#1105#1057#1039' ')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 643.701274166666600000
          Top = 80.000000000000000000
          Width = 110.000000000000000000
          Height = 16.000000000000000000
          DataSet = frxDBDataset_link
          DataSetName = 'Link'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."antenna2_polarization_str"]')
          ParentFont = False
          UseDefaultCharset = True
        end
        object Memo19: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 128.000000000000000000
          Width = 155.000000000000000000
          Height = 16.000000000000000000
          DataSet = frxDBDataset_link
          DataSetName = 'Link'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1118#1056#1105#1056#1111' '#1056#1109#1056#177#1056#1109#1057#1026#1057#1107#1056#1169#1056#1109#1056#1030#1056#176#1056#1029#1056#1105#1057#1039)
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 155.000000000000000000
          Top = 128.000000000000000000
          Width = 110.000000000000000000
          Height = 16.000000000000000000
          DataSet = frxDBDataset_link
          DataSetName = 'Link'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."LinkEndType1_name"]')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 144.000000000000000000
          Width = 155.000000000000000000
          Height = 16.000000000000000000
          DataSet = frxDBDataset_link
          DataSetName = 'Link'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1119#1056#1109#1056#1169#1056#1169#1056#1105#1056#176#1056#1111#1056#176#1056#183#1056#1109#1056#1029)
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 155.000000000000000000
          Top = 144.000000000000000000
          Width = 110.000000000000000000
          Height = 16.000000000000000000
          DataSet = frxDBDataset_link
          DataSetName = 'Link'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."LinkEnd1_subband"]')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 160.000000000000000000
          Width = 155.000000000000000000
          Height = 16.000000000000000000
          DataSet = frxDBDataset_link
          DataSetName = 'Link'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#167#1056#176#1057#1027#1057#8218#1056#1109#1057#8218#1056#176' '#1056#1111#1056#181#1057#1026#1056#181#1056#1169#1056#176#1057#8225#1056#1105', MHz')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 155.000000000000000000
          Top = 160.000000000000000000
          Width = 110.000000000000000000
          Height = 16.000000000000000000
          DataSet = frxDBDataset_link
          DataSetName = 'Link'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."LinkEnd1_tx_freq_MHz"]')
          ParentFont = False
        end
        object Memo25: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 192.000000000000000000
          Width = 155.000000000000000000
          Height = 16.000000000000000000
          DataSet = frxDBDataset_link
          DataSetName = 'Link'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1032#1057#1027#1056#1105#1056#187#1056#181#1056#1029#1056#1105#1056#181' '#1056#176#1056#1029#1057#8218#1056#181#1056#1029#1056#1029#1057#8249', dB')
          ParentFont = False
        end
        object Memo31: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 155.000000000000000000
          Top = 192.000000000000000000
          Width = 110.000000000000000000
          Height = 16.000000000000000000
          DataSet = frxDBDataset_link
          DataSetName = 'Link'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."antenna1_gain"]')
          ParentFont = False
        end
        object Memo32: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Top = 208.000000000000000000
          Width = 155.000000000000000000
          Height = 16.000000000000000000
          DataSet = frxDBDataset_link
          DataSetName = 'Link'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1032#1057#1026#1056#1109#1056#1030#1056#181#1056#1029#1057#1034' '#1057#1027#1056#1105#1056#1110#1056#1029#1056#176#1056#187#1056#176', dBm')
          ParentFont = False
        end
        object Memo33: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 155.000000000000000000
          Top = 208.000000000000000000
          Width = 110.000000000000000000
          Height = 16.000000000000000000
          DataSet = frxDBDataset_link
          DataSetName = 'Link'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."rx_level_dBm"]')
          ParentFont = False
        end
        object Memo34: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 488.701274166666600000
          Top = 128.000000000000000000
          Width = 155.000000000000000000
          Height = 16.000000000000000000
          DataSet = frxDBDataset_link
          DataSetName = 'Link'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1118#1056#1105#1056#1111' '#1056#1109#1056#177#1056#1109#1057#1026#1057#1107#1056#1169#1056#1109#1056#1030#1056#176#1056#1029#1056#1105#1057#1039)
          ParentFont = False
        end
        object Memo35: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 643.701274166666600000
          Top = 128.000000000000000000
          Width = 110.000000000000000000
          Height = 16.000000000000000000
          DataSet = frxDBDataset_link
          DataSetName = 'Link'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."LinkEndType2_name"]')
          ParentFont = False
          UseDefaultCharset = True
        end
        object Memo36: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 488.701274166666600000
          Top = 144.000000000000000000
          Width = 155.000000000000000000
          Height = 16.000000000000000000
          DataSet = frxDBDataset_link
          DataSetName = 'Link'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1119#1056#1109#1056#1169#1056#1169#1056#1105#1056#176#1056#1111#1056#176#1056#183#1056#1109#1056#1029)
          ParentFont = False
        end
        object Memo37: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 643.701274166666600000
          Top = 144.000000000000000000
          Width = 110.000000000000000000
          Height = 16.000000000000000000
          DataSet = frxDBDataset_link
          DataSetName = 'Link'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."LinkEnd2_subband"]')
          ParentFont = False
          UseDefaultCharset = True
        end
        object Memo38: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 488.701274166666600000
          Top = 160.000000000000000000
          Width = 155.000000000000000000
          Height = 16.000000000000000000
          DataSet = frxDBDataset_link
          DataSetName = 'Link'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#167#1056#176#1057#1027#1057#8218#1056#1109#1057#8218#1056#176' '#1056#1111#1056#181#1057#1026#1056#181#1056#1169#1056#176#1057#8225#1056#1105', MHz')
          ParentFont = False
        end
        object Memo39: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 643.701274166666600000
          Top = 160.000000000000000000
          Width = 110.000000000000000000
          Height = 16.000000000000000000
          DataSet = frxDBDataset_link
          DataSetName = 'Link'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."LinkEnd2_tx_freq_MHz"]')
          ParentFont = False
          UseDefaultCharset = True
        end
        object Memo44: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 488.701274166666600000
          Top = 192.000000000000000000
          Width = 155.000000000000000000
          Height = 16.000000000000000000
          DataSet = frxDBDataset_link
          DataSetName = 'Link'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1032#1057#1027#1056#1105#1056#187#1056#181#1056#1029#1056#1105#1056#181' '#1056#176#1056#1029#1057#8218#1056#181#1056#1029#1056#1029#1057#8249', dB')
          ParentFont = False
        end
        object Memo46: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 643.701274166666600000
          Top = 192.000000000000000000
          Width = 110.000000000000000000
          Height = 16.000000000000000000
          DataSet = frxDBDataset_link
          DataSetName = 'Link'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."antenna2_gain"]')
          ParentFont = False
          UseDefaultCharset = True
        end
        object Memo48: TfrxMemoView
          AllowVectorExport = True
          Left = 270.000000000000000000
          Top = 180.000000000000000000
          Width = 144.204726490000000000
          Height = 44.000000000000000000
          DataSet = frxDBDataset_link
          DataSetName = 'Link'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            
              #1056#1032#1057#1026#1056#1109#1056#1030#1056#181#1056#1029#1057#1034' '#1057#1027#1056#1105#1056#1110#1056#1029#1056#176#1056#187#1056#176' '#1056#1030' '#1057#1027#1056#1030#1056#1109#1056#177#1056#1109#1056#1169#1056#1029#1056#1109#1056#1112' '#1056#1111#1057#1026#1056#1109#1057#1027#1057#8218#1057#1026 +
              #1056#176#1056#1029#1057#1027#1057#8218#1056#1030#1056#181', dBm')
          ParentFont = False
        end
        object Memo50: TfrxMemoView
          AllowVectorExport = True
          Left = 414.000000000000000000
          Top = 180.000000000000000000
          Width = 65.000000000000000000
          Height = 44.000000000000000000
          DataSet = frxDBDataset_link
          DataSetName = 'Link'
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."simple_rx_level"]')
          ParentFont = False
        end
        object Memo52: TfrxMemoView
          AllowVectorExport = True
          Left = 270.000000000000000000
          Top = 48.000000000000000000
          Width = 144.000000000000000000
          Height = 16.000000000000000000
          DataSet = frxDBDataset_link
          DataSetName = 'Link'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1113#1056#1109#1056#1029#1057#8222#1056#1105#1056#1110#1057#1107#1057#1026#1056#176#1057#8224#1056#1105#1057#1039' '#1056#1109#1056#177#1056#1109#1057#1026#1057#1107#1056#1169#1056#1109#1056#1030#1056#176#1056#1029#1056#1105#1057#1039)
          ParentFont = False
        end
        object Memo53: TfrxMemoView
          AllowVectorExport = True
          Left = 414.000000000000000000
          Top = 48.000000000000000000
          Width = 65.000000000000000000
          Height = 16.000000000000000000
          DataSet = frxDBDataset_link
          DataSetName = 'Link'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          ParentFont = False
        end
        object Memo54: TfrxMemoView
          AllowVectorExport = True
          Left = 270.000000000000000000
          Top = 112.000000000000000000
          Width = 144.000000000000000000
          Height = 16.000000000000000000
          DataSet = frxDBDataset_link
          DataSetName = 'Link'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapX = 5.000000000000000000
          Memo.UTF8 = (
            #1056#1116#1056#176#1056#1169#1056#181#1056#182#1056#1029#1056#1109#1057#1027#1057#8218#1057#1034' '#1057#1027#1056#1030#1057#1039#1056#183#1056#1105', %')
          ParentFont = False
        end
        object Memo55: TfrxMemoView
          AllowVectorExport = True
          Left = 414.000000000000000000
          Top = 112.000000000000000000
          Width = 65.000000000000000000
          Height = 16.000000000000000000
          DataSet = frxDBDataset_link
          DataSetName = 'Link'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."Reliability"]')
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 488.701274166666600000
          Top = 112.000000000000000000
          Width = 155.000000000000000000
          Height = 16.000000000000000000
          DataSet = frxDBDataset_link
          DataSetName = 'Link'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1032#1056#1110#1056#1109#1056#187' '#1056#1112#1056#181#1057#1027#1057#8218#1056#176', '#1042#176)
          ParentFont = False
        end
        object Memo49: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 643.701274166666600000
          Top = 112.000000000000000000
          Width = 110.000000000000000000
          Height = 16.000000000000000000
          DataSet = frxDBDataset_link
          DataSetName = 'Link'
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[Link."Tilt2"]')
          ParentFont = False
          UseDefaultCharset = True
        end
      end
      object MasterData3: TfrxMasterData
        FillType = ftBrush
        Frame.Typ = []
        Height = 28.000000000000000000
        Top = 245.669450000000000000
        Width = 753.701274166666600000
        Child = frxReport_SDB.Child_Header
        DataSet = frxDBDataset_link
        DataSetName = 'Link'
        RowCount = 0
        object Linkname: TfrxMemoView
          Align = baWidth
          AllowVectorExport = True
          Top = 4.000000000000000000
          Width = 753.701274166666600000
          Height = 16.000000000000000000
          AutoWidth = True
          DataSet = frxDBDataset_link
          DataSetName = 'Link'
          Frame.Typ = []
          Memo.UTF8 = (
            '[Link."name_full"]')
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        Frame.Typ = []
        Height = 24.000000000000000000
        Top = 597.165740000000000000
        Width = 753.701274166666600000
        object Page: TfrxMemoView
          Align = baRight
          AllowVectorExport = True
          Left = 673.701274166666600000
          Width = 80.000000000000000000
          Height = 16.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8 = (
            '[Page#]')
          ParentFont = False
        end
        object Memo138: TfrxMemoView
          Align = baWidth
          AllowVectorExport = True
          Width = 673.701274166666600000
          Height = 16.000000000000000000
          AutoWidth = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clSilver
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8 = (
            
              #1056#1115#1057#8218#1057#8225#1056#181#1057#8218' '#1056#1030#1057#8249#1056#1111#1056#1109#1056#187#1056#1029#1056#181#1056#1029' '#1056#1030' '#1056#1111#1057#1026#1056#1109#1056#1110#1057#1026#1056#176#1056#1112#1056#1112#1056#181' ONEPLAN RPLS-D' +
              'B Link')
          ParentFont = False
        end
      end
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        Frame.Typ = []
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        Height = 28.000000000000000000
        ParentFont = False
        Top = 18.897650000000000000
        Width = 753.701274166666600000
        object Linkcalc_method_name1: TfrxMemoView
          Align = baWidth
          AllowVectorExport = True
          Left = 141.600000000000000000
          Width = 612.101274166666600000
          Height = 16.000000000000000000
          AutoWidth = True
          DataField = 'calc_method_name'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clSilver
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8 = (
            '[Summary."calc_method_name"]')
          ParentFont = False
        end
        object Memo137: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Width = 141.600000000000000000
          Height = 16.000000000000000000
          AutoWidth = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clSilver
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8 = (
            #1056#1114#1056#181#1057#8218#1056#1109#1056#1169#1056#1105#1056#1108#1056#176' '#1057#1026#1056#176#1057#1027#1057#8225#1056#181#1057#8218#1056#176': ')
          ParentFont = False
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Frame.Typ = []
        Height = 16.000000000000000000
        Top = 207.874150000000000000
        Width = 753.701274166666600000
        DataSet = frxDBDataset_items
        DataSetName = 'items'
        RowCount = 0
        object Memo84: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Width = 80.000000000000000000
          Height = 16.000000000000000000
          DataField = 'name_sdb'
          DataSet = frxDBDataset_items
          DataSetName = 'items'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[items."name_sdb"]')
          ParentFont = False
        end
        object Memo85: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 80.000000000000000000
          Width = 55.000000000000000000
          Height = 16.000000000000000000
          DataField = 'name'
          DataSet = frxDBDataset_items
          DataSetName = 'items'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[items."name"]')
          ParentFont = False
        end
        object Memo86: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 135.000000000000000000
          Width = 80.000000000000000000
          Height = 16.000000000000000000
          DataField = 'stream_type'
          DataSet = frxDBDataset_items
          DataSetName = 'items'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[items."stream_type"]')
          ParentFont = False
        end
        object Memo87: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 215.000000000000000000
          Width = 55.000000000000000000
          Height = 16.000000000000000000
          DataField = 'stream_bitrate'
          DataSet = frxDBDataset_items
          DataSetName = 'items'
          DisplayFormat.FormatStr = '%2.0f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8 = (
            '[items."stream_bitrate"]')
          ParentFont = False
        end
        object Memo88: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 270.000000000000000000
          Width = 55.000000000000000000
          Height = 16.000000000000000000
          DataField = 'SESR'
          DataSet = frxDBDataset_items
          DataSetName = 'items'
          DisplayFormat.FormatStr = '%2.6f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8 = (
            '[items."SESR"]')
          ParentFont = False
        end
        object Memo89: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 325.000000000000000000
          Width = 55.000000000000000000
          Height = 16.000000000000000000
          DataField = 'kng'
          DataSet = frxDBDataset_items
          DataSetName = 'items'
          DisplayFormat.FormatStr = '%2.6f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8 = (
            '[items."kng"]')
          ParentFont = False
        end
        object Memo90: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 425.000000000000000000
          Width = 55.000000000000000000
          Height = 16.000000000000000000
          DataField = 'SESR_required'
          DataSet = frxDBDataset_items
          DataSetName = 'items'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8 = (
            '[items."SESR_required"]')
          ParentFont = False
        end
        object Memo91: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 480.000000000000000000
          Width = 55.000000000000000000
          Height = 16.000000000000000000
          DataField = 'kng_required'
          DataSet = frxDBDataset_items
          DataSetName = 'items'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8 = (
            '[items."kng_required"]')
          ParentFont = False
        end
        object Memo92: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 380.000000000000000000
          Width = 45.000000000000000000
          Height = 16.000000000000000000
          DataField = 'kng_year'
          DataSet = frxDBDataset_items
          DataSetName = 'items'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8 = (
            '[items."kng_year"]')
          ParentFont = False
        end
        object Memo93: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 535.000000000000000000
          Width = 45.000000000000000000
          Height = 16.000000000000000000
          DataField = 'bandwidth'
          DataSet = frxDBDataset_items
          DataSetName = 'items'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8 = (
            '[items."bandwidth"]')
          ParentFont = False
        end
        object Memo94: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 580.000000000000000000
          Width = 40.000000000000000000
          Height = 16.000000000000000000
          DataField = 'length_m'
          DataSet = frxDBDataset_items
          DataSetName = 'items'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8 = (
            '[items."length_m"]')
          ParentFont = False
        end
        object Memo98: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 620.000000000000000000
          Width = 55.000000000000000000
          Height = 16.000000000000000000
          DataField = 'band'
          DataSet = frxDBDataset_items
          DataSetName = 'items'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8 = (
            '[items."band"]')
          ParentFont = False
        end
        object Memo101: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 675.000000000000000000
          Width = 67.000000000000000000
          Height = 16.000000000000000000
          DataField = 'status_str'
          DataSet = frxDBDataset_items
          DataSetName = 'items'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            '[items."status_str"]')
          ParentFont = False
        end
      end
      object Header2: TfrxHeader
        FillType = ftBrush
        Frame.Typ = []
        Height = 30.000000000000000000
        Top = 154.960730000000000000
        Width = 753.701274166666600000
        object Memo102: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Width = 80.000000000000000000
          Height = 30.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1118#1056#1105#1056#1111' '#1056#160#1056#160#1056#152)
          ParentFont = False
        end
        object Memo103: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 80.000000000000000000
          Width = 55.000000000000000000
          Height = 30.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1116#1056#176#1056#183#1056#1030#1056#176#1056#1029#1056#1105#1056#181)
          ParentFont = False
        end
        object Memo104: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 135.000000000000000000
          Width = 80.000000000000000000
          Height = 30.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1118#1056#1105#1056#1111' '#1056#1111#1056#1109#1057#8218#1056#1109#1056#1108#1056#176)
          ParentFont = False
        end
        object Memo107: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 215.000000000000000000
          Width = 55.000000000000000000
          Height = 30.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1038#1056#1108#1056#1109#1057#1026#1056#1109#1057#1027#1057#8218#1057#1034' Mbit/s')
          ParentFont = False
        end
        object Memo108: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 270.000000000000000000
          Width = 55.000000000000000000
          Height = 30.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            'SESR, %')
          ParentFont = False
        end
        object Memo113: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 325.000000000000000000
          Width = 55.000000000000000000
          Height = 30.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1113#1056#1116#1056#1110',  %')
          ParentFont = False
        end
        object Memo114: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 425.000000000000000000
          Width = 55.000000000000000000
          Height = 30.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            'SESR '#1057#8218#1057#1026#1056#181#1056#177', %')
          ParentFont = False
        end
        object Memo118: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 480.000000000000000000
          Width = 55.000000000000000000
          Height = 30.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1113#1056#1116#1056#1110' '#1057#8218#1057#1026#1056#181#1056#177', %')
          ParentFont = False
        end
        object Memo119: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 380.000000000000000000
          Width = 45.000000000000000000
          Height = 30.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1113#1056#1116#1056#1110' '#1056#1112#1056#1105#1056#1029' '#1056#1110#1056#1109#1056#1169)
          ParentFont = False
        end
        object Memo120: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 535.000000000000000000
          Width = 45.000000000000000000
          Height = 30.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1119#1056#1109#1056#187#1056#1109#1057#1027#1056#176', MHz')
          ParentFont = False
        end
        object Memo121: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 580.000000000000000000
          Width = 40.000000000000000000
          Height = 30.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#8221#1056#187#1056#1105#1056#1029#1056#176', m')
          ParentFont = False
        end
        object Memo125: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 620.000000000000000000
          Width = 55.000000000000000000
          Height = 30.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#8221#1056#1105#1056#176#1056#1111#1056#176#1056#183#1056#1109#1056#1029', GHz')
          ParentFont = False
        end
        object Memo126: TfrxMemoView
          Align = baLeft
          AllowVectorExport = True
          Left = 675.000000000000000000
          Width = 67.000000000000000000
          Height = 30.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            #1056#1119#1057#1026#1056#1105#1056#1110#1056#1109#1056#1169#1056#1029#1056#1109#1057#1027#1057#8218#1057#1034)
          ParentFont = False
        end
      end
      object ColumnHeader1: TfrxColumnHeader
        FillType = ftBrush
        Frame.Typ = []
        Height = 27.000000000000000000
        Top = 68.031540000000000000
        Width = 753.701274166666600000
        object Memo7: TfrxMemoView
          Align = baWidth
          AllowVectorExport = True
          Width = 753.701274166666600000
          Height = 19.000000000000000000
          AutoWidth = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8 = (
            
              #1056#1115#1057#8218#1057#8225#1056#181#1057#8218' '#1056#1111#1056#1109' '#1057#1026#1056#176#1056#1169#1056#1105#1056#1109#1057#1026#1056#181#1056#187#1056#181#1056#8470#1056#1029#1056#1109#1056#1112#1057#1107' '#1056#1105#1056#1029#1057#8218#1056#181#1057#1026#1056#1030#1056#176#1056#187#1057#1107' ' +
              'SDB')
          ParentFont = False
        end
      end
    end
    object Page2: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      object MasterData_profile: TfrxMasterData
        FillType = ftBrush
        Frame.Typ = []
        Height = 56.000000000000000000
        Top = 16.000000000000000000
        Width = 718.110700000000000000
        DataSet = frxDBDataset_Link_img
        DataSetName = 'profile_img'
        RowCount = 0
        object Picture_profile_: TfrxPictureView
          Align = baWidth
          AllowVectorExport = True
          Top = 20.000000000000000000
          Width = 718.110700000000000000
          Height = 20.000000000000000000
          OnBeforePrint = 'Picture_profileOnBeforePrint'
          DataField = 'img'
          DataSet = frxDBDataset_Link_img
          DataSetName = 'profile_img'
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Stretched = False
          HightQuality = True
          Transparent = False
          TransparentColor = clWhite
        end
        object Memo40: TfrxMemoView
          Align = baWidth
          AllowVectorExport = True
          ShiftMode = smWhenOverlapped
          Width = 718.110700000000000000
          Height = 16.800000000000000000
          AutoWidth = True
          DataSet = frxDBDataset_Link_img
          DataSetName = 'profile_img'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8 = (
            
              '[profile_img."link_name"] '#1056#152#1056#183#1056#1109#1056#177#1057#1026#1056#176#1056#182#1056#181#1056#1029#1056#1105#1056#181' '#1056#1111#1057#1026#1056#1109#1057#8222#1056#1105#1056#187#1057#1039' ' +
              #1056#1111#1057#1026#1056#1105' '#1056#1029#1056#1109#1057#1026#1056#1112#1056#176#1056#187#1057#1034#1056#1029#1056#1109#1056#8470' '#1057#1026#1056#181#1057#8222#1057#1026#1056#176#1056#1108#1057#8224#1056#1105#1056#1105)
          ParentFont = False
        end
      end
      object MasterData_map: TfrxMasterData
        FillType = ftBrush
        Frame.Typ = []
        Height = 28.000000000000000000
        Top = 92.000000000000000000
        Width = 718.110700000000000000
        DataSet = frxDBDataset_map
        DataSetName = 'map_img'
        RowCount = 0
        object Picture_map_: TfrxPictureView
          Description = 'Map'
          Align = baWidth
          AllowVectorExport = True
          Width = 718.110700000000000000
          Height = 20.000000000000000000
          OnBeforePrint = 'Picture_mapOnBeforePrint'
          DataField = 'img'
          DataSet = frxDBDataset_map
          DataSetName = 'map_img'
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Stretched = False
          HightQuality = True
          Transparent = False
          TransparentColor = clWhite
        end
      end
    end
  end
  object ADOStoredProc_items: TADOStoredProc
    Connection = ADOConnection1
    CursorType = ctStatic
    LockType = ltBatchOptimistic
    ProcedureName = 'link_sdb.sp_Link_SDB_items_SEL'
    Parameters = <>
    Left = 344
    Top = 200
  end
  object frxDBDataset_items: TfrxDBDataset
    UserName = 'items'
    CloseDataSource = False
    FieldAliases.Strings = (
      'ID=ID'
      'name_sdb=name_sdb'
      'name=name'
      'stream_type=stream_type'
      'stream_bitrate=stream_bitrate'
      'SESR=SESR'
      'kng=kng'
      'kng_year=kng_year'
      'SESR_required=SESR_required'
      'kng_required=kng_required'
      'bandwidth=bandwidth'
      'length_m=length_m'
      'band=band'
      'status=status'
      'status_str=status_str')
    DataSet = ADOStoredProc_items
    BCDToCurrency = True
    Left = 340
    Top = 124
  end
end
