unit dm_LinkLine_report;

interface

uses
  SysUtils, Classes, Forms, Db, ADODB,  dialogs,
  frxExportPDF, frxExportXLS, frxClass,  frxExportRTF,


  u_doc,

  dm_Document,

  dm_MDB,

  u_FastReport,

  dm_Report,

  u_func,

  u_files,

  dm_Link_report,

  u_ini_Link_report_params,

  dm_Main,

//  dm_Link_report_data,
//  dm_Link_report_XML,

  u_const_db,

  u_classes,
  u_db,

 //.. dm_Link_report,
  dm_Link_map,
  dm_LinkLine,

  frxDBSet, frxDesgn, frxExportBaseDialog
  ;

type
  TdmLinkLine_report = class(TDataModule)
    qry_LinkLine: TADOQuery;
    frxDBDataset_LinkLine: TfrxDBDataset;
    frxReport: TfrxReport;
    ADOConnection1: TADOConnection;
    frxDBDataset_LinkLine_img: TfrxDBDataset;
    frxRTFExport1: TfrxRTFExport;
    frxXLSExport1: TfrxXLSExport;
    frxPDFExport1: TfrxPDFExport;
    ds_LinkLine: TDataSource;
    frxDesigner1: TfrxDesigner;
    qry_LinkLine_links: TADOQuery;
    frxDBDataset_LinkLine_links: TfrxDBDataset;
    procedure DataModuleDestroy(Sender: TObject);
//    procedure DataModuleDestroy(Sender: TObject);
    procedure DataModuleCreate(Sender: TObject);

  private
    FTempFileDirList : TStringList;

    FIsDone : boolean;

    FLinkLineID : Integer;

    FTempFileDir : string;

    procedure DesignReport1;
    
    procedure Execute_LinkLine(aLinkLineID: integer);
    procedure MakeReport;

    procedure ReportExportByFilter(aExportFilter: TfrxCustomExportFilter; aExt,
        aFileName: String);
        
  public
    function DesignReport: Boolean;
    procedure ExecuteAll;

    class procedure Init;
    procedure TestAdd;
  end;


var
  dmLinkLine_report: TdmLinkLine_report;


//function dmLinkLine_report: TdmLinkLine_report;

//==================================================================
//==================================================================
implementation
 {$R *.dfm}



const
  FLD_map_img = 'map_img';



procedure TdmLinkLine_report.DataModuleDestroy(Sender: TObject);
var
  i: Integer;
begin
  for i:=0 to FTempFileDirList.Count-1 do
    DeleteDirectory (FTempFileDirList[i]);


   FreeAndNil(FTempFileDirList);
end;

class procedure TdmLinkLine_report.Init;
begin
  if not Assigned(dmLinkLine_report) then
    dmLinkLine_report := TdmLinkLine_report.Create(Application);
end;


// ---------------------------------------------------------------
procedure TdmLinkLine_report.DataModuleCreate(Sender: TObject);
// ---------------------------------------------------------------

  procedure DoAdd(aItem: TfrxDataset);
  begin
    if not Assigned(frxReport.DataSets.Find(aItem)) then
      frxReport.DataSets.Add(aItem);
  end;

                                                           
var                                           
  k: Integer;
begin

  if adoconnection1.connected then
    ShowMessage('adoconnection1.connected');

(*
 if ADOConnection_mdb.connected then
    ShowMessage('ADOConnection_mdb.connected');
*)


  inherited;
//  FLinkIDList := TIDList.Create();

  FTempFileDirList :=TStringList.Create;


  db_SetComponentADOConnection(Self, dmMain.AdoConnection);

  frxDBDataset_LinkLine_img.DataSet   := dmMDB.ClientDataSet_linkline_img;
  frxDBDataset_LinkLine.DataSet       := dmMDB.t_LinkLine;
  frxDBDataset_LinkLine_links.DataSet := dmMDB.t_LinkLine_links;




  k:=frxReport.DataSets.Count;


  DoAdd(frxDBDataset_LinkLine);
  DoAdd(frxDBDataset_LinkLine_img);
  DoAdd(frxDBDataset_LinkLine_links);



  DoAdd(dmLink_report.frxDBDataset_link);
  DoAdd(dmLink_report.frxDBDataset_Property1);
  DoAdd(dmLink_report.frxDBDataset_Property2);
  DoAdd(dmLink_report.frxDBDataset_LinkEnd1);
  DoAdd(dmLink_report.frxDBDataset_LinkEnd2);
  DoAdd(dmLink_report.frxDBDataset_Profile_items);
  DoAdd(dmLink_report.frxDBDataset_group);
  DoAdd(dmLink_report.frxDBDataset_Items);

 // ClientDataSet_linkline_img.Open;
//
  qry_LinkLine_links.sql.text:=
//     'SELECT * FROM '+ TBL_LINKLINE_LINK_XREF +' WHERE linkline_id = :linkline_id',
//     'SELECT name FROM '+ VIEW_LINKLINE_LINKS +' WHERE linkline_id = :id  ORDER BY name '
     'SELECT * FROM '+ VIEW_LINKLINE_LINKS +' WHERE linkline_id = :id'


//  rep_LinkLine.DataSets.

end;


// ---------------------------------------------------------------
procedure TdmLinkLine_report.ReportExportByFilter(aExportFilter:
    TfrxCustomExportFilter; aExt, aFileName: String);
// ---------------------------------------------------------------
var
  b: Boolean;
  k: Integer;
  sDocFile: string;
begin
  assert(aFileName<>'');

  aFileName:= ChangeFileExt(aFileName, aExt);

  b:= DeleteFile (aFileName);

  k:=Random (100000);


  aExportFilter.FileName:= aFileName;
  aExportFilter.ShowDialog:= False;
  frxReport.Export(aExportFilter);

//      aExportFilter.ShowDialog:= True;

  if Eq(aExt, '.rtf') then
  begin
    sDocFile:=ChangeFileExt(aFileName, '.doc');

    b:= DeleteFile (sDocFile);

    if FileExists(sDocFile) then
       sDocFile:= ChangeFileExt(sDocFile, Format('_%d',[Random (100000)]) + '.doc');


    RTF_to_doc (aFileName,  sDocFile);

   // RTF_to_doc (aFileName,  ChangeFileExt(aFileName, '.doc'));

    b:=DeleteFile (aFileName);

    aFileName:= ChangeFileExt(aFileName, '.doc');

  end;



  if g_Link_Report_Param.IsSaveToDB then
    dmDocument.Add_link(g_Link_Report_Param.ProjectID,
                        FLinkLineID,
                        'Name',
                        aFileName);

 if g_Link_Report_Param.IsOpenReportAfterDone then
    ShellExec (aFileName,'');
                        
end;




//--------------------------------------------------------------
procedure TdmLinkLine_report.ExecuteAll;
//--------------------------------------------------------------
var
  i: integer;
  iID: Integer;
begin
//  if FIsDone then
  //  exit;


//  FIsDone:=True;

//  dmLink_report_new.Params.IsPrintReport := False;


  for i:=0 to g_Link_Report_Param.IDList.Count-1 do
  begin
    iID:=g_Link_Report_Param.IDList[i].id;

//    dlg_Progress.Progress1(i, FLinkIDList.Count);

    Execute_LinkLine (iID);
  end;

end;


//--------------------------------------------------------------------
procedure TdmLinkLine_report.Execute_LinkLine(aLinkLineID: integer);
//    aReportTemplateFileName: string
//--------------------------------------------------------------------
var i: integer;
  oLinkIDList: TIDList;
 // s: string;
  sExt: string;
  sImgFileName: string;
  sName: string;
  sTempFileName: string;

begin
  if FIsDone then
  begin
     MakeReport;
    exit;
  end;

  FIsDone:=True;


  oLinkIDList:=TIDList.Create;

  FLinkLineID:=aLinkLineID;


  db_OpenTableByID(qry_LinkLine, VIEW_LINKLINE, aLinkLineID);

  qry_LinkLine_links.Open;


  if not dmMDB.t_LinkLine.Active  then
  begin

//db_View(qry_LinkLine_links);   

    dmMDB.CreateTableFromDataset('LinkLine', qry_LinkLine, dmMDB.t_LinkLine);
    dmMDB.CreateTableFromDataset('LinkLine_links', qry_LinkLine_links, dmMDB.t_LinkLine_links);

  end;

  db_AddRecordFromDataSet (qry_LinkLine, dmMDB.t_LinkLine);
  db_AddRecordsFromDataSet(qry_LinkLine_links, dmMDB.t_LinkLine_links);
                              

//  db_View (qry_LinkLine_links);
  {
  with qry_LinkLine do
  begin
    First;

    while not EOF do
    begin
      Next;
    end;    // while
  end;    // with

  }

  dmLinkLine.GetLinkListByLinkLineID(aLinkLineID, oLinkIDList);

//  oLinkIDList.

//  s:=oLinkIDList.MakeWhereString;


 // dmLink_report.Params.IsPrintReport := False;

  dmLink_report.Params.IsLinkLine:=True;
  dmLink_report.SetLinkQuery_for_LinkLine_(oLinkIDList);


   FTempFileDir:= g_Link_Report_Param.FileDir + Format('LinkLine%d\', [aLinkLineID]);
   FTempFileDirList.Add(FTempFileDir);


  sImgFileName:=FTempFileDir + Format('map_lilnkline_%d.bmp', [aLinkLineID]);



//  sJpgFileName:=ChangeFileExt(sImgFileName,'.jpg');


//  xml_AddNode(vNode, 'MAP_IMAGE', [xml_Att(ATT_FILENAME, sShortDir+ Format('%d_map.bmp', [aLinkLineID]))]);

  TdmLink_map_.LinkLine_SaveToImage (aLinkLineID, sImgFileName, oLinkIDList);



  dmMDB.ClientDataSet_linkline_img.Append;
  TGraphicField(dmMDB.ClientDataSet_linkline_img.FieldByName(FLD_map_img)).LoadFromFile(sImgFileName);
  dmMDB.ClientDataSet_linkline_img.Post;


  frxDBDataset_UpdateAll(Self);

  FreeAndNil(oLinkIDList);




  MakeReport;


end;

// ---------------------------------------------------------------
procedure TdmLinkLine_report.DesignReport1;
// ---------------------------------------------------------------
var
  b: Boolean;
begin
  frxReport.DesignReport(True);
  b:=frxReport.Modified;

end;


// ---------------------------------------------------------------
function TdmLinkLine_report.DesignReport: Boolean;
// ---------------------------------------------------------------
var
  b: Boolean;
  oStream1: TMemoryStream;
  oStream2: TMemoryStream;
begin
//  frxReport.PrepareReport(true);
//  frxReport.ShowReport;
  oStream1:=TMemoryStream.Create;
  oStream2:=TMemoryStream.Create;

  frxReport.SaveToStream(oStream1);


  frxReport.DesignReport(True);
//  frxReport.m
  b:=frxReport.Modified;

  frxReport.SaveToStream(oStream2);


  b:=not u_func.CompareMemoryStreams(oStream1,oStream2);

  Result := b;

  FreeAndNil(oStream1);
  FreeAndNil(oStream2);

end;


// ---------------------------------------------------------------
procedure TdmLinkLine_report.MakeReport;
// ---------------------------------------------------------------
var
  sName: string;
  sTempFileName: string;
begin
//  g_Link_Report_Param.


  // -------------------------
  if g_Link_Report_Param.IsPreview then
   frxReport.ShowReport

  else begin
   sName :=CorrectFileName(qry_LinkLine.FieldByName(FLD_NAME).AsString);
   sTempFileName := g_Link_Report_Param.FileDir + sName;


   frxReport.PrepareReport;

  // frxReport_tele2.ShowReport;

   case g_Link_Report_Param.FileType of
     ftFileExcel : ReportExportByFilter(frxXLSExport1, '.xls', sTempFileName);
     ftFileWord  : ReportExportByFilter(frxRTFExport1, '.doc', sTempFileName);
     ftFilePDF   : ReportExportByFilter(frxPDFExport1, '.pdf', sTempFileName);
   end;
  end;

end;

// ---------------------------------------------------------------
procedure TdmLinkLine_report.TestAdd;
// ---------------------------------------------------------------
const
  DEF_category = 'Linkline';
  DEF_Name = 'FR - ����� �� �����';

var
  k: Integer;
  rec: TdmReportAddRec;
  s: string;
begin
  s:= Format('DELETE from %s where category=''%s''',
             [TBL_REPORT, DEF_category]); // 'select * from property where (id=:property2_id)');

  dmMain.ADOConnection1.Execute(s);


  // ---------------------------------------------------------------

  FillChar(rec,SizeOf(rec),0);

  rec.Name:=DEF_Name;
  rec.Category:=DEF_category;
 // rec.FileName:='c:\report.tmp';
  rec.FileName:=GetTempFileDir() + rec.Name + '.fr3';


  frxReport.SaveToFile(rec.FileName);

  k:=dmReport.Add(rec);

  DeleteFile(rec.FileName);

  ShowMessage('������� ������� ���������.');

end;




begin
end.

          
