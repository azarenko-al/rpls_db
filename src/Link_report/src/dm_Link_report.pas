unit dm_Link_report;

interface

// {$DEFINE gost}
//   {$IFDEF new}

  {.$DEFINE view_data}


  {.$DEFINE test_img}

uses
  SysUtils, Classes, Controls, Forms, Dialogs, Db, ADODB, Variants,
  frxClass, frxExportRTF, frxExportPDF, frxExportXLS, frxDBSet,

  //dm_MDB_v1,

  dm_Report,

  u_doc,

//  dm_MDB,


  u_FastReport,

  u_radio,

  u_geo_convert_new,
  u_dll_geo_convert,

  u_ini_Link_report_params,


  u_LinkCalcResult_new,

  //dm_FRep_short,

  dm_Onega_DB_data,

  //u_Link_report_vars,

  fr_Profile_Rel_Graph,

  dm_Main,

  dm_Rel_Engine,
  dm_Link,
  dm_Link_map,

  dm_Document,

  //dm_Link_report_data,

  u_const_db,

  dm_Progress,

  u_classes,

  u_func,

  u_db,
  u_files,

//  u_img_png,

  u_Geo,

  u_rel_Profile,

  u_Link_report_class,

  frxDesgn, frxExportBaseDialog;

type
  TdmLink_report = class(TdmProgress)
    qry_LinkEnd1: TADOQuery;
    qry_Antennas1: TADOQuery;
    qry_LinkEnd2: TADOQuery;
    qry_Antennas2: TADOQuery;
    qry_Property1: TADOQuery;
    qry_Property2: TADOQuery;
    qry_LInk_repeater: TADOQuery;
    qry_LInk_repeater_ant: TADOQuery;
    frxDBDataset_link: TfrxDBDataset;
    frxDBDataset_Property1: TfrxDBDataset;
    frxDBDataset_Property2: TfrxDBDataset;
    frxDBDataset_Link_img: TfrxDBDataset;
    ADOConnection1: TADOConnection;
    ds_Link: TDataSource;
    frxDBDataset_LinkEnd1: TfrxDBDataset;
    frxDBDataset_LinkEnd2: TfrxDBDataset;
    frxDBDataset_group: TfrxDBDataset;
    frxDBDataset_Items: TfrxDBDataset;
    ds_Property2: TDataSource;
    ds_Property1: TDataSource;
    ds_LinkEnd1: TDataSource;
    ds_LinkEnd2: TDataSource;
    frxDBDataset_Profile_items: TfrxDBDataset;
    frxPDFExport1: TfrxPDFExport;
    frxRTFExport1: TfrxRTFExport;
    frxXLSExport1: TfrxXLSExport;
    qry_Link: TADOQuery;
    frxDesigner1: TfrxDesigner;
    frxDBDataset_Antenna1: TfrxDBDataset;
    frxDBDataset_Antenna2: TfrxDBDataset;
    frxReport_full: TfrxReport;
    ds_Repeater: TDataSource;
    frxDBDataset_repeater_ant: TfrxDBDataset;
    qry_LInk_repeater_property: TADOQuery;
    frxDBDataset_repeater_property: TfrxDBDataset;
    frxReport_base: TfrxReport;
    frxReport_repeater: TfrxReport;
    frxReport_short_WGS: TfrxReport;
    frxReport_MTS: TfrxReport;
    frxDBDataset_repeater: TfrxDBDataset;
    frxReport1: TfrxReport;
    sp_Link: TADOStoredProc;
//    procedure ADOStoredProc_LinkAfterScroll(DataSet: TDataSet);
//    procedure ClientDataSet_groupsAfterScroll(DataSet: TDataSet);
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
//    procedure frxDesigner1GetTemplateList(List: TStrings);
    procedure qry_LinkEnd1CalcFields(DataSet: TDataSet);
 //   procedure qry_LinkEnd1CalcFields(DataSet: TDataSet);
    procedure qry_Property1CalcFields(DataSet: TDataSet);
//    procedure ds_ClientDataSet_Profile_itemsDataChange(Sender: TObject; Field:
  //      TField);
  private
    FTempFileDirList : TStringList;

    FfrxReport: TfrxReport;

    FDataReady : boolean;

    FTempFileDir : string;


    FAntenna1_Heights: TDoubleArray;
    FAntenna2_Heights: TDoubleArray;

    FIsDebug : Boolean;

   // FDataset_Link : TDataset;

//..    FImgFileList: TStringList;


//    FLinkCalcResult: TLinkCalcResult;
    FDataSet_link          : TDataSet;

    FDataSet_Property1      : TDataSet;
    FDataSet_Property2      : TDataSet;

    FDataSet_groups        : TDataSet;
    FDataSet_items         : TDataSet;
    FDataSet_Profile_items : TDataSet;
    FDataSet_img           : TDataSet;

    FDataSet_antenna1      : TDataSet;
    FDataSet_antenna2      : TDataSet;



    FLink_ID: Integer;

//    FVector: TBLVector;
//    FClutterModelID: integer;


    procedure ReportExportByFilter(aExportFilter: TfrxCustomExportFilter; aExt,
        aFileName: String);

    procedure MakeProfileImage;

    procedure MakeProfileImage_Repeater;
    procedure Profile_Open;
    procedure Profile_Open_Repeater;
  private

    FRelProfile1: TrelProfile;
    FRelProfile2: TrelProfile;


    FRefractionArr: array[1..3] of Double;// = (22,158,169);
  // 22  - 3. ���������� ������� �������� ��� a% ��������� - ����������� ���������
  // 158 - 6. ���������� ������� ���������� � ���.������������ V���
  // 169 - 18. ���.������� ���������������� ������������ ��������������


    FRelProfile: TrelProfile;

    procedure Init_Datasets(aReport: TfrxReport);
    function OpenLinkData(aID: integer): Boolean;
    function ShowDesignReport: Boolean;
    procedure Update_Tilt;

//    procedure SetLinkQuery_for_LinkLine(aID_str: string);

//    procedure UpdateAliases;
  public
    FLinkData: TLinkData;

    Params: record
              IsLinkLine: boolean;
            end;

    procedure DesignReport;

    procedure ExecuteProc; override;

    procedure PrintReport;

    class procedure Init;

    procedure Add_Default;

    procedure SetLinkQuery_for_LinkLine_(aLinkIDList: TIDList);
  end;


//function dmLink_report: TdmLink_report;
var
  dmLink_report: TdmLink_report;

//==================================================================
// implementation
//==================================================================
implementation

uses dm_MDB;
{$R *.dfm}

//uses
 // f_Test,
//  dm_MDB;



const
  FLD_profile_img_path='profile_img_path';
  FLD_profile_img='profile_img';
  FLD_map_img='map_img';

  FLD_HEIGHT        ='HEIGHT';
  FLD_POLARIZATION  ='POLARIZATION';
  FLD_Diameter      ='Diameter';

  FLD_map_filename = 'map_filename';



class procedure TdmLink_report.Init;
begin
  if not Assigned(dmLink_report) then
    dmLink_report := TdmLink_report.Create(Application);
end;




// ---------------------------------------------------------------
procedure TdmLink_report.ReportExportByFilter(aExportFilter:  TfrxCustomExportFilter; aExt, aFileName: String);
// ---------------------------------------------------------------
var
  b: Boolean;
  k: Integer;
  sDocFile: string;
begin
  assert(aFileName<>'');

  aFileName:= ChangeFileExt(aFileName, aExt);

  b:= DeleteFile (aFileName);

  if FileExists(aFileName) then
     aFileName:= ChangeFileExt(aFileName, Format('_%d',[Random (100000)]) + aExt);

//  k:=Random (100000);



  aExportFilter.FileName := aFileName;
  aExportFilter.ShowDialog:= False;
  FfrxReport.Export(aExportFilter);

  if Eq(aExt, '.rtf') then
  begin
    sDocFile:=ChangeFileExt(aFileName, '.doc');

    b:= DeleteFile (sDocFile);

    if FileExists(sDocFile) then
       sDocFile:= ChangeFileExt(sDocFile, Format('_%d',[Random (100000)]) + '.doc');


    RTF_to_doc (aFileName,  sDocFile);

    DeleteFile (aFileName);

   aFileName:= ChangeFileExt(aFileName, '.doc');

  end;



  if g_Link_Report_Param.IsSaveToDB then
    dmDocument.Add_link(g_Link_Report_Param.ProjectID,
                        FLinkData.ID,
                        FLinkData.Name,
                        aFileName);



 if g_Link_Report_Param.IsOpenReportAfterDone then
    ShellExec (aFileName,'');

//      aExportFilter.ShowDialog:= True;
end;



// ---------------------------------------------------------------
procedure TdmLink_report.DesignReport;
// ---------------------------------------------------------------
var
  sTemplateFileName: string;
begin
  inherited;

  sTemplateFileName := g_Link_Report_Param.FileDir + Format('report_%d.rep', [g_Link_Report_Param.Report_ID]);


 // Assert(Params.Report_ID>0);

  dmReport.SaveToFile(g_Link_Report_Param.Report_ID, sTemplateFileName);

//  bSave := False;

  case g_Link_Report_Param.Category_Type of
    ctLink_SDB,
    ctLink,
    ctLinkLIne: FfrxReport.LoadFromFile(sTemplateFileName);
  else
    raise Exception.Create('' );

  end;


  if ShowDesignReport then
  begin
    FfrxReport.SaveToFile(sTemplateFileName);

    dmReport.LoadFromFile(g_Link_Report_Param.Report_ID, sTemplateFileName);

  end;

(*
  if bSave then
     dmReport.LoadFromFile(g_Link_Report_Param.Report_ID, sTemplateFileName);
*)

end;



// ---------------------------------------------------------------
procedure TdmLink_report.SetLinkQuery_for_LinkLine_(aLinkIDList: TIDList);
// ---------------------------------------------------------------
const
  SQL_SELECT_LINK = 'SELECT * FROM '+ view_LINK + ' WHERE id=%d';

var
  I: Integer;

begin
  ds_Link.DataSet:=qry_Link;

//  FDataSet_link:=qry_Link;



 // ds_Link.DataSet:=qry_Link;

  FDataSet_link:=qry_Link;


  for I := 0 to aLinkIDList.Count - 1 do    // Iterate
  begin
    dmOnega_DB_data.OpenQuery (qry_Link, Format(SQL_SELECT_LINK, [aLinkIDList[i].ID]) );

    OpenLinkData (aLinkIDList[i].ID);

  end;    // for

end;


//--------------------------------------------------------------------
procedure TdmLink_report.Init_Datasets(aReport: TfrxReport);
//--------------------------------------------------------------------
  procedure DoAdd(aItem: TfrxDataset);
  begin
    if not Assigned(aReport.DataSets.Find(aItem)) then
      aReport.DataSets.Add(aItem);
  end;

begin
//  exit;
  //////////////////

//  aReport.

  aReport.DataSets.Clear;

  DoAdd(frxDBDataset_link);
  DoAdd(frxDBDataset_Property1);
  DoAdd(frxDBDataset_Property2);
  DoAdd(frxDBDataset_LinkEnd1);
  DoAdd(frxDBDataset_LinkEnd2);

  DoAdd(frxDBDataset_Antenna1);
  DoAdd(frxDBDataset_Antenna2);


  DoAdd(frxDBDataset_Profile_items);

  DoAdd(frxDBDataset_group);
  DoAdd(frxDBDataset_Items);

  DoAdd(frxDBDataset_repeater);
  DoAdd(frxDBDataset_repeater_property);
  DoAdd(frxDBDataset_repeater_ant);

end;



//--------------------------------------------------------------------
procedure TdmLink_report.DataModuleCreate(Sender: TObject);
//--------------------------------------------------------------------

//var
//  sFileName: string;
 // sFileName_MDB: string;
begin
  inherited;

  if adoconnection1.connected then
    ShowMessage('TdmLink_report: adoconnection1.connected');


  FfrxReport:=frxReport_base;


//  sFileName_MDB:=g_ApplicationDataDir + 'report.mdb';

  {
  TdmMDB.Init;

  dmMDB.CreateMDB(sFileName_MDB);
  dmMDB.Open_data;
  }


//
 // Params.IsPrintReport := True;

//  FImgFileList:=TStringList.Create;



  FRelProfile:=TrelProfile.Create;

  FRelProfile1:=TrelProfile.Create;
  FRelProfile2:=TrelProfile.Create;


  db_SetComponentADOConnection (Self, dmMain.ADOConnection);

  FLinkData := TLinkData.Create();


  //---------------------------
  // cala fields
  //---------------------------
  dmOnega_DB_data.OpenQuery(qry_Property1, 'SELECT * FROM '+ TBL_Property  +' where id=0');

  db_CreateFieldsForDataset(qry_Property1);
  db_CreateCalculatedField(qry_Property1, FLD_lat_wgs, ftFloat);
  db_CreateCalculatedField(qry_Property1, FLD_lon_wgs, ftFloat);

  db_CreateCalculatedField(qry_Property1, FLD_lat_CK95, ftFloat);
  db_CreateCalculatedField(qry_Property1, FLD_lon_CK95, ftFloat);

  db_CreateCalculatedField(qry_Property1, FLD_LAT_GCK_2011, ftFloat);
  db_CreateCalculatedField(qry_Property1, FLD_LON_GCK_2011, ftFloat);



  db_CreateFields_FromSrcToDest(qry_Property1, qry_Property2);

  db_CreateFields_FromSrcToDest(qry_Property1, qry_LInk_repeater_property);


  //---------------------------
  // cala fields
  //---------------------------
  dmOnega_DB_data.OpenQuery(qry_LinkEnd1, 'SELECT * FROM '+ VIEW_LINKEND  +' where id=0');

  db_CreateFieldsForDataset(qry_LinkEnd1);
  db_CreateCalculatedField(qry_LinkEnd1, FLD_POWER_W, ftFloat);

  db_CreateFields_FromSrcToDest(qry_LinkEnd1, qry_LinkEnd2);



  qry_LInk_repeater.SQL.Text     := 'SELECT * FROM '+ VIEW_LINK_REPEATER          +' where link_id=:id';
  qry_LInk_repeater_ant.SQL.Text := 'SELECT * FROM '+ VIEW_LINK_REPEATER_ANTENNA  +' where LINK_REPEATER_id=:id';

  qry_LInk_repeater_property.SQL.Text := 'SELECT * FROM '+ TBL_PROPERTY  +' where id=:PROPERTY_id';



  qry_Property1.SQL.Text := 'SELECT * FROM '+ TBL_Property  +' where id=:property1_id';
  qry_Property2.SQL.Text := 'SELECT * FROM '+ TBL_Property  +' where id=:property2_id';



  FDataSet_link:=qry_Link;  // ADOStoredProc_Link;


  FDataSet_antenna1:=dmMDB.t_Antennas1;//  ClientDataSet_Antenna1;
  FDataSet_antenna2:=dmMDB.t_Antennas2;//ClientDataSet_Antenna2;


  FDataSet_groups := dmMDB.t_groups;
  FDataSet_items  := dmMDB.t_Items;

  FDataSet_img    := dmMDB.clientDataSet_Link_img;
  FDataSet_Profile_items:= dmMDB.t_Profile_items;



  // ---------------------------------------------------------------

  frxDBDataset_link.DataSet     :=dmMDB.t_link;
  frxDBDataset_Property1.DataSet:=dmMDB.t_Property1;
  frxDBDataset_Property2.DataSet:=dmMDB.t_Property2;

  frxDBDataset_LinkEnd1.DataSet :=dmMDB.t_LinkEnd1;
  frxDBDataset_LinkEnd2.DataSet :=dmMDB.t_LinkEnd2;



  frxDBDataset_group.DataSet:=FDataSet_groups;
  frxDBDataset_Items.DataSet:=FDataSet_items;

  frxDBDataset_Profile_items.DataSet:=FDataSet_Profile_items; //dmMDB.t_Profile_items;

  frxDBDataset_Antenna1.DataSet:=dmMDB.t_Antennas1;
  frxDBDataset_Antenna2.DataSet:=dmMDB.t_Antennas2;

  frxDBDataset_Link_img.DataSet:=dmMDB.clientDataSet_Link_img;

  FTempFileDirList :=TStringList.create;


end;

//--------------------------------------------------------------------
procedure TdmLink_report.DataModuleDestroy(Sender: TObject);
//--------------------------------------------------------------------
var
  i: Integer;
begin
  for i:=0 to FTempFileDirList.Count-1 do
    DeleteDirectory (FTempFileDirList[i]);

  FreeAndNil(FTempFileDirList);

//  FreeAndNil(FImgFileList);


  FreeAndNil(FRelProfile);

  FreeAndNil(FRelProfile1);
  FreeAndNil(FRelProfile2);

  FreeAndNil(FLinkData);

  inherited;
end;


//--------------------------------------------------------------------
function TdmLink_report.OpenLinkData(aID: integer): Boolean;
//--------------------------------------------------------------------
const

  // -------------------------
  // ANTENNAS
  // -------------------------
  SQL_SELECT_LINKEND_ANT =
     'SELECT *  FROM '+ view_LINKEND_ANTENNAS + ' WHERE linkend_id=:linkend_id ORDER BY height desc';

  SQL_SELECT_LINKEND_ANT1 =
     'SELECT *  FROM '+ view_LINKEND_ANTENNAS + ' WHERE linkend_id=:id ORDER BY height desc';

//  SQL_SELECT_LINKEND=  'SELECT * FROM '+ VIEW_LINKEND +' WHERE id=:id ';
  SQL_SELECT_LINKEND1= 'SELECT * FROM '+ VIEW_LINKEND +' WHERE id=:linkend1_id ';
  SQL_SELECT_LINKEND2= 'SELECT * FROM '+ VIEW_LINKEND +' WHERE id=:linkend2_id ';

  SQL_SELECT_reflection_POINTS = 'SELECT * FROM '+ TBL_Link_reflection_points + ' WHERE (link_id=:link_id)';



var
//  eH1: Double;
//  eH2: Double;
//  eTilt1: Double;
//  eTilt2: Double;

  iPmpSectorID,iPmpTerminalID,
  iLinkEndID1,iLinkEndID2: integer;

  iProperty1: Integer;
  iProperty2: Integer;
  i: integer;
  iCount: Integer;
  iRes: Integer;
  j: integer;
  k: Integer;
  k1: Integer;
  k2: Integer;
  oLinkCalcResult: TLinkCalcResult;
  s: string;
 // sImgFileName: string;
  sMapImgFileName: string;


begin
  dmOnega_DB_data.OpenQuery(qry_Link, 'SELECT * FROM view_link_report WHERE id=:ID', ['id', aID]);


//  db_view (qry_Link);

//  db_view (qry_Link);

// --------------------------

//      k:=dmOnega_DB_data.Link_Select(ADOStoredProc_Link, iID);


  FLink_ID:=aID;

  Assert(FLink_ID>0, 'Value <=0');


  FTempFileDir:= IncludeTrailingBackslash (  g_Link_Report_Param.FileDir + Format('link_%d', [aID])  );
  FTempFileDirList.add(FTempFileDir);


 // iRes:=dmOnega_DB_data.Link_Select(ADOStoredProc_Link, aID);

 // FDataSet_link:=

 // db_OpenQuery (qry_Link, Format('SELECT type FROM '+ TBL_LINK + ' WHERE id=%d', [aID]));
  i :=FDataSet_link.RecordCount;

//  FLinkData.

   Assert(FDataSet_link.RecordCount>0, 'FDataSet_link.RecordCount>0');


  if (FDataSet_link.RecordCount=0) then
  begin
    ShowMessage('Link not found!');
    Exit;
  end;

//  FVector        :=FLinkData.BLVector;

//  FClutterModelID:=FLinkData.ClutterModel_ID;

//  SQL_SELECT_LINKEND1= 'SELECT * FROM '+ VIEW_LINKEND +' WHERE id=:linkend1_id ';
//  SQL_SELECT_LINKEND2= 'SELECT * FROM '+ VIEW_LINKEND +' WHERE id=:linkend2_id ';
  qry_LinkEnd1.DataSource:=nil;
  qry_LinkEnd2.DataSource:=nil;


  dmOnega_DB_data.OpenQuery(qry_LinkEnd1,
      'SELECT top 1 * FROM '+ VIEW_LINKEND +' WHERE id=:id', ['id', qry_Link['linkend1_id'] ]);

  dmOnega_DB_data.OpenQuery(qry_LinkEnd2,
      'SELECT top 1  * FROM '+ VIEW_LINKEND +' WHERE id=:id', ['id', qry_Link['linkend2_id'] ]);

//   qry_LinkEnd1.SQL.Text:= SQL_SELECT_LINKEND1;
//   qry_LinkEnd2.SQL.Text:= SQL_SELECT_LINKEND2;

//   qry_LinkEnd1.Open;
//   qry_LinkEnd2.Open;

   assert (qry_LinkEnd1.RecordCount=1 , IntToStr (qry_LinkEnd1.RecordCount)) ;
   assert (qry_LinkEnd2.RecordCount=1 , IntToStr (qry_LinkEnd2.RecordCount)) ;


//  db_View (qry_LinkEnd1);


//   k:=qry_LinkEnd1.RecordCount;

//   assert ((not qry_LinkEnd1.IsEmpty) and (not qry_LinkEnd2.IsEmpty)) ;

//       then
//       raise Exception.Create ('qry_LinkEnd1.IsEmpty or qry_LinkEnd2.IsEmpty');

//  SQL_SELECT_LINKEND_ANT =
//     'SELECT *  FROM '+ view_LINKEND_ANTENNAS + ' WHERE linkend_id=:linkend_id ORDER BY height desc';

  qry_Antennas1.DataSource:=nil;
  qry_Antennas2.DataSource:=nil;

  dmOnega_DB_data.OpenQuery(qry_Antennas1,
      'SELECT *  FROM '+ view_LINKEND_ANTENNAS + ' WHERE linkend_id=:linkend_id ORDER BY height desc',
       ['linkend_id', qry_LinkEnd1['id'] ]);

  dmOnega_DB_data.OpenQuery(qry_Antennas2,
      'SELECT *  FROM '+ view_LINKEND_ANTENNAS + ' WHERE linkend_id=:linkend_id ORDER BY height desc',
       ['linkend_id', qry_LinkEnd2['id'] ]);


//   qry_Antennas1.SQL.Text:= SQL_SELECT_LINKEND_ANT1;
//   qry_Antennas2.SQL.Text:= SQL_SELECT_LINKEND_ANT1;

//   qry_Antennas1.Open;
//   qry_Antennas2.Open;


   k1:=qry_Antennas1.RecordCount;
   k2:=qry_Antennas2.RecordCount;

   Assert(qry_Antennas1.RecordCount>0, 'Value =0');
   Assert(qry_Antennas2.RecordCount>0, 'Value =0');


   FLinkData.LoadFromDataset(FDataSet_link);

   iLinkEndID1:=FLinkData.LinkEndID1;
   iLinkEndID2:=FLinkData.LinkEndID2;

   iProperty1:=FLinkData.PropertyID1;
   iProperty2:=FLinkData.PropertyID2;


//  db_View (FDataSet_link);

//  dmOnega_DB_data.OpenQuery(qry_Property1, 'SELECT * FROM '+ TBL_Property  +' where id=0');

  qry_Property1.DataSource:=nil;
  qry_Property2.DataSource:=nil;

  dmOnega_DB_data.OpenQuery(qry_Property1, 'SELECT * FROM '+ TBL_Property  +' where id=:id', ['id',qry_Link['property1_id']]);
  dmOnega_DB_data.OpenQuery(qry_Property2, 'SELECT * FROM '+ TBL_Property  +' where id=:id', ['id',qry_Link['property2_id']]);

//  qry_Property1.Open;
//  qry_Property2.Open;

//  db_View (qry_Property1);
//  db_View (qry_Property2);

  Assert(qry_Property1.RecordCount=1, 'qry_Property1.RecordCount<>1');
  Assert(qry_Property2.RecordCount=1, 'qry_Property2.RecordCount<>1');


  FLinkData.Property1.LoadFromDataset(qry_Property1);
  FLinkData.Property2.LoadFromDataset(qry_Property2);


  if not dmMDB.t_Link.Active  then
  begin

//  db_View(FDataSet_link);

    dmMDB.CreateTableFromDataset('link', FDataSet_link, dmMDB.t_Link);


    dmMDB.CreateTableFromDataset('Property1', qry_Property1, dmMDB.t_Property1);
    dmMDB.CreateTableFromDataset('Property2', qry_Property1, dmMDB.t_Property2);

    dmMDB.CreateTableFromDataset('LinkEnd1', qry_LinkEnd1, dmMDB.t_LinkEnd1);
    dmMDB.CreateTableFromDataset('LinkEnd2', qry_LinkEnd1, dmMDB.t_LinkEnd2);


  end;


  dmMDB.ADOConnection_MDB.BeginTrans;

  if not Params.IsLinkLine then
    dmMDB.Clear;

  db_AddRecordFromDataSet(FDataSet_link, dmMDB.t_Link);

  db_AddRecordFromDataSet(qry_Property1, dmMDB.t_Property1);
  db_AddRecordFromDataSet(qry_Property2, dmMDB.t_Property2);

  db_AddRecordFromDataSet(qry_LinkEnd1, dmMDB.t_LinkEnd1);
  db_AddRecordFromDataSet(qry_LinkEnd2, dmMDB.t_LinkEnd2);




//  Assert(iProperty1>0, 'Value =0');
 // Assert(iProperty2>0, 'Value =0');

  //
//    property Items[Index: Integer]: TLinkCalcResultItem read GetItems; default;

  Assert(qry_property1.Active);
  Assert(qry_property2.Active);

//  db_View(qry_property1);
//  db_View(qry_property2);

//  db_View(ADOStoredProc_Link);


//  k:=qry_property2.FieldByName(FLD_ID).AsInteger;
//  k:=qry_property1.FieldByName(FLD_ID).AsInteger;


 // FLinkData.P

 /////// FLinkData.Property1.Ground_Height

  ;


  FLinkData.LinkEnd1_Antennas.LoadFromDataset(qry_Antennas1, FLinkData.Property1.Ground_Height);
  FLinkData.LinkEnd2_Antennas.LoadFromDataset(qry_Antennas2, FLinkData.Property2.Ground_Height);

(*
  FLinkData.LinkEnd1_Antennas.LoadFromDataset(qry_Antennas1, qry_property1);
  FLinkData.LinkEnd2_Antennas.LoadFromDataset(qry_Antennas2, qry_property2);
*)


  FAntenna1_Heights:=FLinkData.LinkEnd1_Antennas.GetAntennaHeightArr;
  FAntenna2_Heights:=FLinkData.LinkEnd2_Antennas.GetAntennaHeightArr;

//  Update_Tilt;


   oLinkCalcResult := TLinkCalcResult.Create();


//  if FLinkData.CalcResultsXML<>'' then
  begin


 //   iCount1:=FDataSet_groups.RecordCount;
  //  iCount2:=FDataSet_items.RecordCount;

//    FDataSet_groups.Tag


    if FLinkData.LinkRepeater.ID= 0 then
    begin
      oLinkCalcResult.LoadFromXmlString(FLinkData.CalcResultsXML);


      oLinkCalcResult.SaveToDataset_2_Datasets(FDataSet_groups, FDataSet_items, FLink_ID);

//      oLinkCalcResult.SaveToDataset_2_Datasets(dmMDB.t_groups, dmMDB.t_Items, FLink_ID);

    end ;



 //   FreeAndNil ( oLinkCalcResult );
  end;


//  db_AddRecord_(ClientDataSet_img, [FLD_LINK_ID, FLink_ID]);
  db_AddRecord_(FDataSet_img, [FLD_LINK_ID, FLink_ID]);

//  db_AddRecord_(dmMDB.t_link_img, [FLD_LINK_ID, FLink_ID]);

//  dmMDB.


  Assert(qry_Property1.RecordCount>0);
  Assert(qry_Property2.RecordCount>0);


  qry_LInk_repeater.Open;
  qry_LInk_repeater_ant.Open;

  qry_LInk_repeater_property.Open;


 // db_View (qry_LInk_repeater_property);

//  db_View (qry_LInk_repeater);
//  db_View (qry_LInk_repeater_ant);


  if FLinkData.LinkRepeater.ID > 0 then
  begin


    Assert(qry_LInk_repeater.RecordCount>0);
    Assert(qry_LInk_repeater_ant.RecordCount>0);
    Assert(qry_LInk_repeater_property.RecordCount>0);

    FLinkData.LinkRepeater_LoadFromDataset(qry_LInk_repeater);


    if FLinkData.LinkRepeater.ID> 0 then
      if (FLinkData.LinkRepeater.calc_results_xml_1<>'') and
         (FLinkData.LinkRepeater.calc_results_xml_2<>'')
         //and (FLinkData.LinkRepeater.calc_results_xml_3<>'')

     then

    begin
    //////  Assert (FLinkData.LinkRepeater.calc_results_xml_1<>'');


      oLinkCalcResult.LoadFromXmlString(FLinkData.LinkRepeater.calc_results_xml_3);
      oLinkCalcResult.SaveToDataset_2_Datasets(FDataSet_groups, FDataSet_items, FLink_ID, '1 -> ������������');

 //     oLinkCalcResult.SaveToDataset_2_Datasets(dmMDB.t_groups, dmMDB.t_Items, FLink_ID);


      oLinkCalcResult.LoadFromXmlString(FLinkData.LinkRepeater.calc_results_xml_2);
      oLinkCalcResult.SaveToDataset_2_Datasets(FDataSet_groups, FDataSet_items, FLink_ID, '������������ -> 2');

    //  oLinkCalcResult.SaveToDataset_2_Datasets(dmMDB.t_groups, dmMDB.t_Items, FLink_ID);

    end;
  end;

(*
  FSite1_Rel_H:=qry_property1.FieldByName(FLD_ground_height).AsFloat;
  FSite2_Rel_H:=qry_property2.FieldByName(FLD_ground_height).AsFloat;
*)

  //u_LinkCalcResult_new
//  oLinkCalcResult.LoadFromXmlString(FLinkData.CalcResultsXML);


 // FLinkCalcResult.SaveToDataset_2_Datasets(dxMemData_Groups, dxMemData_Items);


  FRefractionArr[1]:=oLinkCalcResult.Groups.GetCalcParamValueByID(22);
  FRefractionArr[2]:=oLinkCalcResult.Groups.GetCalcParamValueByID(158);
  FRefractionArr[3]:=oLinkCalcResult.Groups.GetCalcParamValueByID(169);

  FreeAndNil(oLinkCalcResult);


(*
   <GROUP name="3" caption=" 3.���������� ������� �������� ��� a% ���������">
      <ITEM id="22" value="1.07685" caption=" ����������� ��������� (��� a% ���������)"/>

   <GROUP name="6" caption=" 6.���������� ������� ���������� � ���.������������ V���">
      <ITEM id="158" value="0.57333" caption=" ����������� ��������� � ���.������������"/>

    <GROUP name="18" caption=" 18.���.������� ���������������� ������������ ��������������">
      <ITEM id="169" value="0.2909" caption=" ����������� ��������� (��� ����.�����.)"/>


*)
//  FImgFileList.Clear;


  if FLinkData.LinkRepeater.ID = 0 then
    Profile_Open()
  else
    Profile_Open_Repeater();


  if FLinkData.LinkRepeater.ID =0 then
    MakeProfileImage()
  else begin
   // sImgFileName:=g_Link_Report_Param.FileDir + Format('link%d_profile.bmp',[aID]);

    MakeProfileImage_Repeater; //(sImgFileName);
  end;

  Assert (FTempFileDir<>'');



 // sMapImgFileName:=g_Link_Report_Param.FileDir + Format('link%d_map.bmp', [aID]);

  // ----------------Link_SaveToImage-----------------------------------------------

  if g_Link_Report_Param.IsMakeMap then
  begin

    DoProgressMsg ('���������� �����...');
   ///////////
    sMapImgFileName:=FTempFileDir + Format('map_link_%d.bmp',[aID]);

    TdmLink_map_.Link_SaveToImage (aID, sMapImgFileName);

//    FImgFileList.Add(sMapImgFileName);


    Assert(FDataSet_img.RecordCount>0);

//-------------------------------
{
    FDataSet_link.Edit;
    FDataSet_link[FLD_map_filename]:=sMapImgFileName;
    TGraphicField(FDataSet_link.FieldByName(FLD_map_img)).LoadFromFile(sMapImgFileName);

    FDataSet_link.Post;
}
//-------------------------------
    FDataSet_img.Edit;

    k:=FDataSet_img[FLD_LINK_ID];

    FDataSet_img[FLD_map_filename]:=sMapImgFileName;

  {
    dmMDB.t_link_img.Edit;
    dmMDB.t_link_img[FLD_map_filename]:=sMapImgFileName;
    dmMDB.t_link_img.Post;
   }

    TGraphicField(FDataSet_img.FieldByName(FLD_map_img)).LoadFromFile(sMapImgFileName);
    FDataSet_img.Post;

//    FImgFileList.Add(sMapImgFileName);


//    for i:=0 to FImgFileList.Count-1 do
//      DeleteFile(FImgFileList[i]);

  end;

//  db_View (FDataSet_img);

/////////////////  Tdlg_Test.ExecDlg;

  Result:=True;

 // UpdateAliases();


  dmMDB.ADOConnection_MDB.CommitTrans;


  frxDBDataset_UpdateAll (Self);



end;


//--------------------------------------------------------------------
procedure TdmLink_report.MakeProfileImage;
//--------------------------------------------------------------------
//const
//  PARAM_IDs: array[1..3] of integer = (22,158,169);
  // 22  - 3. ���������� ������� �������� ��� a% ��������� - ����������� ���������
  // 158 - 6. ���������� ������� ���������� � ���.������������ V���
  // 169 - 18. ���.������� ���������������� ������������ ��������������

var
  oForm: Tframe_Profile_Rel_Graph;
  sDir: string;
  i: Integer;
  dRefraction: double;
//  dRefraction, dOldRefraction: double;
  b: Boolean;
  d: Double;
  eAzimuth1: Double;
  eAzimuth2: Double;
  eH1: Double;
  eH2: Double;
  eMax1: Double;
  eMax2: Double;
  eTilt1: Double;
  eTilt2: Double;
//  iDirection: integer;
//  is_profile_reversed: Boolean;
  s: string;
//  oGraphicField: TGraphicField;
  sFieldName: string;
//  sFileName: string;
  sImgFileName: string;
//  sPNGFileName: string;
//  sPNGFileName: string;
begin
 Assert(FLink_ID>0);

//  sImgFileName:=g_Link_Report_Param.FileDir + Format('link%d_profile.bmp',[FLink_ID]);
//  FImgFileList.Add(sImgFileName);


  Assert (FTempFileDir<>'');

  sImgFileName:=FTempFileDir + 'profile.bmp';





  oForm:=Tframe_Profile_Rel_Graph.Create(nil);


 // oForm.Params. IsShowSites:=True;

 oForm.Params.IsShowReflectionPoints  := g_Link_Report_Param.IsShowReflectionPoints;

  oForm.Params.IsShowFrenelZoneTop:=True;
 // oForm.Params.IsShowFrenelZoneBottom:=True;

 // b:=Params.Profile_IsShowFrenelZoneBottomLeftRight;
  b :=True;
  oForm.Params.IsShowFrenelZoneBottomLeft:=b;
  oForm.Params.IsShowFrenelZoneBottomRight:=b;


 // oForm.Params.IsShowReflectionPoints:=True;

  oForm.Width :=550;
  oForm.Height:=400;

 // oForm.SetDefaultSize;
  oForm.Height:= Trunc(oForm.Height*1.2);
  oForm.Width := Trunc(oForm.Width *1.2);

  {
  s:=FLinkData.Profile_XML;

  ShellExec_Notepad_temp1(s);
   }

///////  dOldRefraction:= FRelProfile.Refraction;
  oForm.RelProfile_Ref:=FRelProfile;

  d:=FLinkData.Length_KM;

(*

  // -------------------------
  if not VarIsNull(oDBLink.Link_repeater.Property_Ground_Height) then
    eRel1 :=oDBLink.Link_repeater.Property_Ground_Height
  else
    eRel1 :=FrelProfile2.FirstItem().Rel_H;

  // -------------------------
  if not VarIsNull(oDBLink.Property2_Ground_Height) then
    eRel2 :=oDBLink.Property2_Ground_Height
  else
    eRel2 :=FrelProfile2.LastItem().Rel_H;

  // -------------------------
  Fframe_Profile_Rel.Params.SiteGroups[1].Site1.Rel_H:=eRel1;
  Fframe_Profile_Rel.Params.SiteGroups[1].Site2.Rel_H:=eRel2;


*)
 // is_profile_reversed:=FLinkData.is_profile_reversed;

//  is_profile_reversed:=ADOStoredProc_Link.FieldByName(FLD_is_profile_reversed).AsBoolean;

  // -------------------------
//  if FLinkData.LinkRepeater.ID =0 then
  // -------------------------
 // begin
    oForm.Params.SetSiteGroupCount(1);

    oForm.Params.SiteGroups[0].Distance_KM:=Flinkdata.Length_KM;
    //FRelProfile.GeoDistance_KM();

  //  iDirection:=ADOStoredProc_Link.FieldByName(FLD_CALC_DIRECTION).AsInteger;



    oForm.Params.SiteGroups[0].Site1.AntennaHeights:=FAntenna1_Heights;
    oForm.Params.SiteGroups[0].Site2.AntennaHeights:=FAntenna2_Heights;

//    oForm.Params.SiteGroups[0].Site1.AntennaHeights:=GetAntennaHeightArr(qry_Antennas1);
//    oForm.Params.SiteGroups[0].Site2.AntennaHeights:=GetAntennaHeightArr(qry_Antennas2);


    oForm.Params.SiteGroups[0].Site1.Rel_H:=FRelProfile.FirstItem.Rel_H;
    oForm.Params.SiteGroups[0].Site2.Rel_H:=FRelProfile.LastItem.Rel_H;

//    Double


    eMax1:=DoubleArray_GetMax(FAntenna1_Heights);
    eMax2:=DoubleArray_GetMax(FAntenna2_Heights);


    eH1:=oForm.Params.SiteGroups[0].Site1.Rel_H + eMax1;
    eH2:=oForm.Params.SiteGroups[0].Site2.Rel_H + eMax2;

   // g_log.Add();



    eTilt1:=geo_TiltAngle  (FLinkData.Property1_Pos, FLinkData.Property2_Pos,  eH1,eH2);

    eTilt2:=geo_TiltAngle  (FLinkData.Property2_Pos, FLinkData.Property1_Pos, eH2,eH1);

(*
    eAzimuth1:=geo_Azimuth (FLinkData.Property1_Pos, FLinkData.Property2_Pos);
    eAzimuth2:=geo_Azimuth (FLinkData.Property2_Pos, FLinkData.Property1_Pos);

*)

  //  aPoint1,aPoint2: TBLPoint; aSiteHeight1,aSiteHeight2: double): double;

    {

    db_UpdateRecord__(FDataSet_link,
           [
       //     FLD_AZIMUTH1, eAzimuth1,
        //    FLD_AZIMUTH2, eAzimuth2,

            FLD_ELEVATION_angle_1, eTilt1,
            FLD_ELEVATION_angle_2, eTilt2
           ]);
     }
//     Assert(FDataSet_link);
{
  db_view (dmMDB.t_Link);


  dmMDB.t_Link.Edit;
  dmMDB.t_Link[FLD_ELEVATION_angle_1]:=eTilt1;
  dmMDB.t_Link[FLD_ELEVATION_angle_2]:=eTilt2;
  dmMDB.t_Link.Post;


    db_UpdateRecord__(dmMDB.t_Link,
           [
       //     FLD_AZIMUTH1, eAzimuth1,
        //    FLD_AZIMUTH2, eAzimuth2,

            FLD_ELEVATION_angle_1, eTilt1,
            FLD_ELEVATION_angle_2, eTilt2
           ]);

}

//    FLinkData.Property1_Ground_Height



//    oForm.Params.SiteGroups[0].Site1.Rel_H:=FRelProfile.FirstItem.Rel_H;
//    oForm.Params.SiteGroups[0].Site2.Rel_H:=FRelProfile.LastItem.Rel_H;



//
    oForm.Params.SiteGroups[0].ReflectionPointArr:=dmLink.GetReflectionPointArray(FLink_ID);

//  end;


  oForm.Params.Freq_MHz:= FLinkData.TX_FREQ_MHz;
  //qry_LinkEnd1.FieldByName(FLD_TX_FREQ_MHz).AsFloat;

///    (qry_LinkEnd1.FieldByName(FLD_TX_FREQ).AsFloat +
  ///   qry_LinkEnd1.FieldByName(FLD_RX_FREQ).AsFloat) / 2;


  oForm.ShowGraph;
  oForm.SaveToBmpFile (sImgFileName);


//  ShellExec(sImgFileName);



(*
  sPNGFileName:= ChangeFileExt(sImgFileName, '.png');

  img_BMP_to_PNG(sImgFileName, sPNGFileName);
*)


  FDataSet_img.Edit;

  Assert(FDataSet_img.State = dsEdit);


  Assert(FDataSet_img.RecordCount>0);

  Assert(FDataSet_img.State = dsEdit);


//  FDataSet_img.Edit;
  TGraphicField(FDataSet_img.FieldByName(FLD_profile_img)).LoadFromFile(sImgFileName);
//  FImgFileList.Add(sImgFileName);



//  for i:=1 to High(PARAM_IDs) do
  for i:=1 to High(FRefractionArr) do
  begin
  //  b:=GetCalcResult_Refraction(PARAM_IDs[i], dRefraction);

    dRefraction:=FRefractionArr[i];


//    dRefraction:= GetCalcResult(PARAM_IDs[i], dRefraction);

    FRelProfile.SetRefraction(dRefraction);
//    FRelProfile.SetDirection(iDirection);
//    FRelProfile.IsDirectionReversed := is_profile_reversed;


    oForm.Params.IsShowReflectionPoints:= false;
    oForm.RelProfile_Ref:=FRelProfile;

    oForm.ShowGraph;

   // sImgFileName:=g_Link_Report_Param.FileDir + Format('link%d_profile_%d.bmp',[FLink_ID,i]);

  Assert (FTempFileDir<>'');

    sImgFileName:=FTempFileDir + Format('profile_%d.bmp',[i]);



//    sImgFileName:= ChangeFileExt(sImgFileName,Format('%d.bmp', [i]));
    oForm.SaveToBmpFile (sImgFileName);

(*
    sPNGFileName:= ChangeFileExt(sImgFileName, '.png');

    img_BMP_to_PNG(sImgFileName, sPNGFileName);
*)


    sFieldName:= Format('img%d', [i]);

       Assert(FDataSet_img.RecordCount>0);

  Assert(FDataSet_img.State = dsEdit);

    FDataSet_img[sFieldName+'_filename']:=sImgFileName;


    TGraphicField(FDataSet_img.FieldBYName(sFieldName)).LoadFromFile(sImgFileName);
    
//    FImgFileList//.Add(sImgFileName);


//    oGraphicField:=TGraphicField(t_Img.FindField(sFieldName));
//    oGraphicField.LoadFromFile(sFileName);

  //  sFieldName:= Format('img_path%d', [i]);

//    ClientDataSet_img_path[sFieldName] := sPNGFileName;// sFileName;
//    ClientDataSet_img_path[sFieldName] := sFileName;

  //  FImgFileList.Add(sFieldName);

  //  .LoadFromFile(sPNGFileName);

  end;

//////////////  FRelProfile.SetRefraction(dOldRefraction);

  FreeAndNil(oForm);

  FDataSet_img.Post;

end;


//--------------------------------------------------------------------
procedure TdmLink_report.MakeProfileImage_Repeater;
//--------------------------------------------------------------------
//const
//  PARAM_IDs: array[1..3] of integer = (22,158,169);
  // 22  - 3. ���������� ������� �������� ��� a% ��������� - ����������� ���������
  // 158 - 6. ���������� ������� ���������� � ���.������������ V���
  // 169 - 18. ���.������� ���������������� ������������ ��������������

var
  oForm: Tframe_Profile_Rel_Graph;
  sDir: string;
  i: Integer;
//  dRefraction, dOldRefraction: double;
  dRefraction: double;
  b: Boolean;
  d: Double;
//  iDirection: integer;
//  is_profile_reversed: Boolean;
  sFieldName: string;
  sImgFileName: string;
begin
  Assert(FLink_ID>0);

  Assert(FLinkData.Refraction>0, 'FLinkData.Refraction>0');

//  sImgFileName:=g_Link_Report_Param.FileDir + Format('link%d_profile.bmp',[FLink_ID]);

  Assert (FTempFileDir<>'');


  sImgFileName:=FTempFileDir + 'profile.bmp';


//  FImgFileList.Add(sImgFileName);

  FDataSet_img.Edit;



  oForm:=Tframe_Profile_Rel_Graph.Create(nil);


 // oForm.Params. IsShowSites:=True;

 oForm.Params.IsShowReflectionPoints  := g_Link_Report_Param.IsShowReflectionPoints;

  oForm.Params.IsShowFrenelZoneTop:=True;
 // oForm.Params.IsShowFrenelZoneBottom:=True;

 // b:=Params.Profile_IsShowFrenelZoneBottomLeftRight;
  b :=True;
  oForm.Params.IsShowFrenelZoneBottomLeft:=b;
  oForm.Params.IsShowFrenelZoneBottomRight:=b;

 // oForm.Params.IsShowReflectionPoints:=True;

//                             ionPoints:=True;

  oForm.Width :=550;
  oForm.Height:=400;

 // oForm.SetDefaultSize;
  oForm.Height:= Trunc(oForm.Height*1.2);
  oForm.Width := Trunc(oForm.Width *1.2);

  FRelProfile1.SetRefraction(FLinkData.Refraction);
  FRelProfile2.SetRefraction(FLinkData.Refraction);
 // oForm.Params.IsShowReflectLinkData.Refraction);

  FRelProfile.Assign(FRelProfile1);
  FRelProfile.Join  (FRelProfile2);

//  FRelProfile.in;


//  FLinkData.Refraction

 // dOldRefraction:= FLinkData.Refraction;
  oForm.RelProfile_Ref:=FRelProfile;

  d:=FLinkData.Length_KM;

(*


*)

//  is_profile_reversed:=FLinkData.is_profile_reversed;// ADOStoredProc_Link.FieldByName(FLD_is_profile_reversed).AsBoolean;
//  is_profile_reversed:=ADOStoredProc_Link.FieldByName(FLD_is_profile_reversed).AsBoolean;

  // -------------------------
//  if FLinkData.LinkRepeater.ID =0 then
  // -------------------------
  begin
    oForm.Params.SetSiteGroupCount(2);

    oForm.Params.SiteGroups[0].Distance_KM:=FLinkData.LinkRepeater.Part1.Length_KM; // FRelProfile1.GeoDistance_KM();
    oForm.Params.SiteGroups[1].Distance_KM:=FLinkData.LinkRepeater.Part2.Length_KM; //FRelProfile2.GeoDistance_KM();

  //  iDirection:=ADOStoredProc_Link.FieldByName(FLD_CALC_DIRECTION).AsInteger;

    // -------------------------
    // part 1
    // -------------------------
//    oForm.Params.SiteGroups[0].Site1.AntennaHeights:=GetAntennaHeightArr(qry_Antennas1);
    oForm.Params.SiteGroups[0].Site1.AntennaHeights:= FAntenna1_Heights;//  GetAntennaHeightArr(qry_Antennas1);

    oForm.Params.SiteGroups[0].Site2.AntennaHeights:=
        MakeDoubleArray( FLinkData.LinkRepeater.Antenna1.Height);

    oForm.Params.SiteGroups[0].Site1.Rel_H:= FLinkData.Property1.Ground_Height;
    oForm.Params.SiteGroups[0].Site2.Rel_H:= FLinkData.LinkRepeater.Property_Ground_Height;


 ////////////
 oForm.Params.SiteGroups[0].ReflectionPointArr:=dmLink.GetReflectionPointArray(FLink_ID, 1);

//    FRelProfile1.FirstItem().Rel_H;

    // -------------------------
    // part 2
    // -------------------------
//    GetAntennaHeightArr(qry_Antennas1);
    oForm.Params.SiteGroups[1].Site1.AntennaHeights:=
        MakeDoubleArray( FLinkData.LinkRepeater.Antenna2.Height);

//      GetAntennaHeightArr(qry_Antennas2);


 //   oForm.Params.SiteGroups[0].Site2.AntennaHeights:=GetAntennaHeightArr(qry_Antennas1);
    oForm.Params.SiteGroups[1].Site2.AntennaHeights:=FAntenna2_Heights;
//    GetAntennaHeightArr(qry_Antennas2);

    oForm.Params.SiteGroups[1].Site1.Rel_H:=FLinkData.LinkRepeater.Property_Ground_Height;
    oForm.Params.SiteGroups[1].Site2.Rel_H:=FLinkData.Property2.Ground_Height;
     //FRelProfile2.LastItem().Rel_H;


  ////////////
  oForm.Params.SiteGroups[1].ReflectionPointArr:=dmLink.GetReflectionPointArray(FLink_ID, 2);


//
//    oForm.Params.SiteGroups[0].ReflectionPointArr:=dmLink.GetReflectionPointArray(FLink_ID);

  end;


  oForm.Params.Freq_MHz:= FLinkData.TX_FREQ_MHz;

//  qry_LinkEnd1.FieldByName(FLD_TX_FREQ_MHz).AsFloat;

///    (qry_LinkEnd1.FieldByName(FLD_TX_FREQ).AsFloat +
  ///   qry_LinkEnd1.FieldByName(FLD_RX_FREQ).AsFloat) / 2;


  oForm.ShowGraph;
  oForm.SaveToBmpFile (sImgFileName);

  ////////////////////////
////////  ShellExec(sImgFileName);
  //////////////////////



  Assert(FDataSet_img.RecordCount>0);

  FDataSet_img[FLD_profile_img+'_filename']:=sImgFileName;


//  dmMDB.t_link_img[FLD_profile_img+'_filename']:=sImgFileName;


  TGraphicField(FDataSet_img.FieldByName(FLD_profile_img)).LoadFromFile(sImgFileName);
//  FImgFileList.Add(sImgFileName);



(*const
  PARAM_IDs: array[1..3] of integer = (22,158,169);
*)

//  for i:=1 to High(PARAM_IDs) do
  for i:=1 to High(FRefractionArr) do
  begin
    dRefraction:=FRefractionArr[i];

  //  b:=GetCalcResult_Refraction(PARAM_IDs[i], dRefraction);

//    dRefraction:= GetCalcResult(PARAM_IDs[i]);

  //  FRelProfile.SetRefraction(dRefraction);


    FRelProfile1.SetRefraction(dRefraction);
    FRelProfile2.SetRefraction(dRefraction);

    FRelProfile.Assign(FRelProfile1);
    FRelProfile.Join  (FRelProfile2);


//    FRelProfile.SetDirection(iDirection);

//////////    FRelProfile.IsDirectionReversed := is_profile_reversed;


    oForm.Params.IsShowReflectionPoints:= false;
    oForm.RelProfile_Ref:=FRelProfile;

    oForm.ShowGraph;

   // sImgFileName:=g_Link_Report_Param.FileDir + Format('link%d_profile_%d.bmp',[FLink_ID,i]);

  Assert (FTempFileDir<>'');


    sImgFileName:=FTempFileDir + Format('profile_%d.bmp',[i]);


//    sImgFileName:= ChangeFileExt(sImgFileName,Format('%d.bmp', [i]));
    oForm.SaveToBmpFile (sImgFileName);


    sFieldName:= Format('img%d', [i]);


    FDataSet_img[sFieldName+'_filename']:=sImgFileName;

//    dmMDB.t_link_img[sFieldName+'_filename']:=sImgFileName;


    TGraphicField(FDataSet_img.FieldBYName(sFieldName)).LoadFromFile(sImgFileName);
//    FImgFileList.Add(sImgFileName);

  end;

///////////////  FRelProfile.SetRefraction(dOldRefraction);

  FreeAndNil(oForm);

  FDataSet_img.Post;

//  dmMDB.t_link_img.Post;

//  oForm.Free;
end;









// ---------------------------------------------------------------
procedure TdmLink_report.Profile_Open;
// ---------------------------------------------------------------
var
  sProfile_XML: string;
  s: string;
  b: Boolean;
//  eH1: Double;
//  eH2: Double;
  eLen_km: Double;
  i: Integer;
//  i: Integer;
  k: Integer;
begin

  DoProgressMsg ('���������� �������...');

//  sProfile_XML:=ADOStoredProc_Link.FieldByName(FLD_profile_XML).AsString;
  sProfile_XML:=FLinkData.profile_XML;

  //Assert(Assigned(dmRel_Engine), 'Value not assigned');

  dmRel_Engine.AssignClutterModel (FLinkData.ClutterModel_ID, FRelProfile.Clutters);


  eLen_km:= geo_Distance_KM(FLinkData.BLVector);


  if (sProfile_XML='') or (not FRelProfile.LoadFromXml(sProfile_XML, eLen_km)) then
  begin
    dmRel_Engine.Open; //ByVector (FVector);
    b:=dmRel_Engine.BuildProfile1 (FRelProfile, FLinkData.BLVector, FLinkData.PROFILE_Step_M);//, FClutterModelID);


   // FRelProfile.Property1_id:=FLinkData.PropertyID1;
    //FRelProfile.Property2_id:=FLinkData.PropertyID2;

    sProfile_XML:=FRelProfile.GetXML();

//////    db_UpdateRecord(ADOStoredProc_Link, [db_Par(FLD_profile_XML, sProfile_XML)]);
  end ;

  //Update_Tilt


//  else
//    FRelProfile.isDirectionReversed := FLinkData.is_profile_reversed;

  // -------------------------

  Update_Tilt;


(*
  if VarIsNull(FLinkData.Property1_Ground_Height) then
    FLinkData.Property1_Ground_Height := FRelProfile.FirstItem().Rel_H;

  if VarIsNull(FLinkData.Property2_Ground_Height) then
    FLinkData.Property2_Ground_Height := FRelProfile.LastItem().Rel_H;
*)
  // -------------------------


 // oForm.Params.SiteGroups[0].Site1.Rel_H:=FRelProfile.FirstItem.Rel_H;
//  oForm.Params.SiteGroups[0].Site2.Rel_H:=FRelProfile.LastItem.Rel_H;

//  k:=ClientDataSet_Profile_items.RecordCount;

//  ClientDataSet_Profile_items.EmptyDataSet;

  //k:=ClientDataSet_Profile_items.RecordCount;


  FRelProfile.ApplyDefaultClutterHeights;

  

{
  FDataSet_Profile_items.Close;
  FDataSet_Profile_items.Open;

  while FDataSet_Profile_items.RecordCount>0 do
    FDataSet_Profile_items.delete;

}




  with FRelProfile do
     for i:=0 to Count-1 do
     begin
       db_AddRecord_ (FDataSet_Profile_items,
         [//xml_ATT ('R',          TruncFloat(Data.Items[i].Distance,3) ),


           FLD_LINK_ID,  FLink_ID ,

           FLD_INDEX,  i+1 ,


           'distance_km',  TruncFloat(Items[i].Distance_KM,3) ,
           'ground_h',     Items[i].Rel_H ,
           'clutter_h',    Items[i].Clutter_H ,
           'earth_h',      TruncFloat(Items[i].Earth_H),

           'clutter_code',  Items[i].Clutter_Code,
           'clutter_color', GetClutterColorByCode(Items[i].Clutter_Code),
           'clutter_name',  GetClutterNameByCode(Items[i].Clutter_Code)
         ]);
                
     end;


 // db_View(ClientDataSet_Profile);


// [Link."ELEVATION_angle_1"]



end;



// ---------------------------------------------------------------
procedure TdmLink_report.Profile_Open_Repeater;
// ---------------------------------------------------------------
var
  sProfile_XML1: string;
  sProfile_XML2: string;
  s: string;
  b: Boolean;

  blVector: TBLVector;
  eLen_km: Double;
begin

  DoProgressMsg ('���������� �������...');

//  sProfile_XML:=ADOStoredProc_Link.FieldByName(FLD_profile_XML).AsString;
  sProfile_XML1:=FLinkData.LinkRepeater.Part1.Profile_XML;
  sProfile_XML2:=FLinkData.LinkRepeater.Part2.Profile_XML;

  //Assert(Assigned(dmRel_Engine), 'Value not assigned');

  dmRel_Engine.Open; //ByVector (FLinkData.blVector);


  dmRel_Engine.AssignClutterModel (FLinkData.ClutterModel_ID, FRelProfile1.Clutters);
  dmRel_Engine.AssignClutterModel (FLinkData.ClutterModel_ID, FRelProfile2.Clutters);

  eLen_km:= geo_Distance_KM(FLinkData.LinkRepeater.Part1.BLVector);

  // -------------------------
  if (sProfile_XML1='') or (not FRelProfile1.LoadFromXml(sProfile_XML1, eLen_km)) then
  begin
    blVector := FLinkData.LinkRepeater.Part1.BLVector;

//    dmRel_Engine.OpenByVector (blVector);
    b:=dmRel_Engine.BuildProfile1 (FRelProfile1, blVector, FLinkData.PROFILE_Step_M);//, FClutterModelID);


   // FRelProfile.Property1_id:=FLinkData.PropertyID1;
    //FRelProfile.Property2_id:=FLinkData.PropertyID2;

    sProfile_XML1:=FRelProfile2.GetXML();

//////    db_UpdateRecord(ADOStoredProc_Link, [db_Par(FLD_profile_XML, sProfile_XML)]);
  end;
 //  else
  //  FRelProfile.isDirectionReversed := FLinkData.is_profile_reversed;

  eLen_km:= geo_Distance_KM(FLinkData.LinkRepeater.Part2.BLVector);

  // -------------------------
  if (sProfile_XML2='') or (not FRelProfile2.LoadFromXml(sProfile_XML2, eLen_km)) then
  begin
    blVector := FLinkData.LinkRepeater.Part2.BLVector;

  //  dmRel_Engine.OpenByVector (blVector);
    b:=dmRel_Engine.BuildProfile1 (FRelProfile2, blVector, FLinkData.PROFILE_Step_M);//, FClutterModelID);


   // FRelProfile.Property1_id:=FLinkData.PropertyID1;
    //FRelProfile.Property2_id:=FLinkData.PropertyID2;

    sProfile_XML2:=FRelProfile2.GetXML();

//////    db_UpdateRecord(ADOStoredProc_Link, [db_Par(FLD_profile_XML, sProfile_XML)]);
  end;


 // -------------------------
/////  if VarIsNull(FLinkData.Property1.Ground_Height) then
    FLinkData.Property1.Ground_Height := FRelProfile1.FirstItem().Rel_H;

//////  if VarIsNull(FLinkData.Property2.Ground_Height) then
    FLinkData.Property2.Ground_Height := FRelProfile2.LastItem().Rel_H;

/////  if VarIsNull(FLinkData.LinkRepeater.Property_Ground_Height) then
    FLinkData.LinkRepeater.Property_Ground_Height := FRelProfile1.LastItem().Rel_H;



end;



// ---------------------------------------------------------------
procedure TdmLink_report.Add_Default;
// ---------------------------------------------------------------
const
  DEF_link = 'Link';

 // DEF_NAME_CK42='FR - CK42';

  DEF_NAME           ='FR - ����� �� �� ���������';
//  DEF_NAME_no_profile  ='FR - ����� �� �� ��������� (��� �������)';

  DEF_NAME_SHORT_WGS ='FR - ����� �� �� ��������� (����������� WGS)';
  DEF_NAME_FOR_MTS   ='FR - ����� �� �� ��������� (����������� ��� MTS-T)';

  DEF_NAME_REPEATER   ='FR - ����� �� �� ��������� (��������� ������������)';

 // DEF_NAME_FOR_MTS_passiv ='FR - ����� �� �� ��������� (����������� ��� MTS-T) ��������� ������������';

 DEF_NAMES: array[0..3] of string =
      (
       DEF_NAME,
       DEF_NAME_SHORT_WGS,
       DEF_NAME_FOR_MTS,
       DEF_NAME_REPEATER
       );

var
  I: Integer;
  k: Integer;
  rec: TdmReportAddRec;
  s: string;
  sTempDir: string;
//  sTempFileName: string;
begin
  for I := 0 to High(DEF_NAMES) do    // Iterate
  begin
    s:= Format('DELETE from %s where name=''%s''', [TBL_REPORT, DEF_NAMES[i]]); // 'select * from property where (id=:property2_id)');
    dmMain.ADOConnection1.Execute(s);

  end;    // for


//  s:= Format('DELETE from %s ', [TBL_REPORT]); // 'select * from property where (id=:property2_id)');
 // dmMain.ADOConnection1.Execute(s);


{
  s:= Format('DELETE from %s where name=''%s''', [TBL_REPORT, DEF_NAME_no_profile]); // 'select * from property where (id=:property2_id)');
  dmMain.ADOConnection1.Execute(s);
}

{
  s:= Format('DELETE from %s where name=''%s''', [TBL_REPORT, DEF_NAME]); // 'select * from property where (id=:property2_id)');
  dmMain.ADOConnection1.Execute(s);


  s:= Format('DELETE from %s where name=''%s''', [TBL_REPORT, DEF_NAME_SHORT_WGS]); // 'select * from property where (id=:property2_id)');
  dmMain.ADOConnection1.Execute(s);

  s:= Format('DELETE from %s where name=''%s''', [TBL_REPORT, DEF_NAME_FOR_MTS]); // 'select * from property where (id=:property2_id)');
  dmMain.ADOConnection1.Execute(s);

  s:= Format('DELETE from %s where name=''%s''', [TBL_REPORT, DEF_NAME_REPEATER]); // 'select * from property where (id=:property2_id)');
  dmMain.ADOConnection1.Execute(s);

}


//  s:= Format('DELETE from %s where name=''%s''', [TBL_REPORT, DEF_NAME_FOR_MTS_passiv]); // 'select * from property where (id=:property2_id)');
//  dmMain.ADOConnection1.Execute(s);



 // sTempFileName:=GetTempFileNameWithExt('.fr3');

  sTempDir:=GetTempFileDir;

//  sTempDir:='d:\report\';

//  ForceDirectories(sTempDir);



  // -------------------------

  FillChar(rec,SizeOf(rec),0);

  rec.Category:=DEF_link;
//  rec.FileName:=sTempFileName;
//  rec.FileName:= sTempDir +  DEF_NAME + '.fr3';


  rec.Name:=DEF_NAME; //'FR - ����� �� �� ���������';
  rec.FileName:= sTempDir +  rec.Name + '_full.fr3';

  frxReport_full.SaveToFile(rec.FileName);

  k:=dmReport.Add(rec);


{

 // -------------------------
  rec.Name:=DEF_NAME_no_profile;
  rec.FileName:= sTempDir +  rec.Name + '.fr3';

  frxReport1_no_profile.SaveToFile(rec.FileName);
  k:=dmReport.Add(rec);
}



 // -------------------------
  rec.Name:=DEF_NAME_SHORT_WGS;
  rec.FileName:= sTempDir +  rec.Name + '.fr3';

  frxReport_short_WGS.SaveToFile(rec.FileName);

  k:=dmReport.Add(rec);

 // -------------------------
  rec.Name:=DEF_NAME_FOR_MTS;
  rec.FileName:= sTempDir +  rec.Name + '.fr3';

  frxReport_MTS.SaveToFile(rec.FileName);

  k:=dmReport.Add(rec);


 // -------------------------
  rec.Name:=DEF_NAME_REPEATER;
  rec.FileName:= sTempDir +  rec.Name + '_REPEATER.fr3';


//  dmReport_template.frxReport_full.SaveToFile(rec.FileName);

  frxReport_repeater.SaveToFile(rec.FileName);

  k:=dmReport.Add(rec);

  // -------------------------
 //////////// DeleteFile(rec.FileName);

end;

// ---------------------------------------------------------------
function TdmLink_report.ShowDesignReport: Boolean;
// ---------------------------------------------------------------
var
  b: Boolean;
  oStream1: TMemoryStream;
  oStream2: TMemoryStream;
begin
  dmOnega_DB_data.OpenQuery(qry_Link, 'SELECT * FROM view_link_report WHERE id=:ID', ['id', 0]);

 // db_view (qry_Link);

//  frxDBDataset_UpdateAll(Self);

{
s:=Format('SELECT * FROM %s where id=0', [View_Link_freq_order]);

 // /'/  SELECT * FROM	view_Link  where id IN (@ID_STR)  order by name

  db_OpenQuery(qry_LInks, s);

  frxDBDataset_UpdateAll(Self);
 }

//  frxDBDataset_UpdateAll (Self);

  //--------------------------------

//  Init_Datasets (FfrxReport);

//  frxReport.PrepareReport(true);
//  frxReport.ShowReport;
  oStream1:=TMemoryStream.Create;
  oStream2:=TMemoryStream.Create;

  FfrxReport.SaveToStream(oStream1);


  FfrxReport.DesignReport(True);
//  frxReport.m
  b:=FfrxReport.Modified;

  FfrxReport.SaveToStream(oStream2);


  b:=not u_func.CompareMemoryStreams(oStream1,oStream2);

  Result := b;

  FreeAndNil(oStream1);
  FreeAndNil(oStream2);

end;


//--------------------------------------------------------------------
procedure TdmLink_report.ExecuteProc;
//--------------------------------------------------------------------
var
  i: integer;
//  iID: Integer;
 // k: Integer;
begin
  Assert (g_Link_Report_Param.IDList.Count>0);


 // CreateTempData( ChangeFileExt(g_Link_Report_Param.TemplateFileName, '.mdb') );


  //153522

  CursorHourGlass;

  Init_Datasets (FfrxReport);


    if  g_Link_Report_Param.IsPreview  then
    begin
      OpenLinkData (g_Link_Report_Param.IDList[0].ID);

      FfrxReport.ShowReport;

      dmMDB.Clear;
    end

    else
//  if not FDataReady then

    for i:=0 to g_Link_Report_Param.IDList.Count-1 do
    begin
      DoProgress (i, g_Link_Report_Param.IDList.Count);


   ///   iID:=;


     // Link_ExecuteItem (iID)  ;

      //DoProgressMsg ('���������� ������...');

   //   SELECT * FROM view_link WHERE id=@ID

     // k:=dmOnega_DB_data.Link_Select(ADOStoredProc_Link, iID);
      OpenLinkData (g_Link_Report_Param.IDList[i].ID);

      Assert (FDataSet_link.RecordCount=1);                      
      Assert (frxDBDataset_link.DataSet.RecordCount=1);

      PrintReport();

      dmMDB.Clear;

  //    Init_Datasets (FfrxReport);



      //and  Params.IsPrintReport
   //   if (not g_Link_Report_Param.IsPreview)   then
    //    PrintReport();

    end;
  ////////////////////////////

  {$IFDEF view_data}
//    Tdlg_Test.ExecDlg;
  {$ENDIF}

 //

  ///////////////////////////////



  FDataReady:=True;


  CursorDefault;


{
  if (not g_Link_Report_Param.IsPreview)   then
     PrintReport();



  if g_Link_Report_Param.IsPreview then
    FfrxReport.ShowReport;
 }

end;


// ---------------------------------------------------------------
procedure TdmLink_report.PrintReport;
// ---------------------------------------------------------------
var
  k: Integer;
  sName, sTempFileName: string;
begin
  sName:=FDataSet_link.FieldByName(FLD_NAME).AsString;



//db_View (dxMemData_link);


//    sTempFileName := g_Link_Report_Param.FileDir + CorrectFileName(FLinkData.Name);

    sTempFileName := g_Link_Report_Param.FileDir + CorrectFileName(sName);

    dmOnega_DB_data.Activity_Log('link_report', dmMain.ProjectID);

    FfrxReport.PrepareReport;

    case g_Link_Report_Param.FileType of
      ftFileExcel : ReportExportByFilter(frxXLSExport1, '.xls', sTempFileName);
      ftFilePDF   : ReportExportByFilter(frxPDFExport1, '.pdf', sTempFileName);

      ftFileWord  : ReportExportByFilter(frxRTFExport1, '.rtf', sTempFileName);

    end;

end;

// ---------------------------------------------------------------
procedure TdmLink_report.qry_LinkEnd1CalcFields(DataSet: TDataSet);
// ---------------------------------------------------------------
var
  ePower_dBm: Double;
begin
  ePower_dBm:=DataSet.FieldByName(FLD_POWER_dBm).AsFloat;

  DataSet[FLD_Power_W]:=dbm_to_wt(ePower_dBm);

end;


// ---------------------------------------------------------------
procedure TdmLink_report.qry_Property1CalcFields(DataSet: TDataSet);
// ---------------------------------------------------------------
var
  bl: TBLPoint;
  bl_CK95: TBLPoint;
  bl_WGS: TBLPoint;
  bl_GCK: TBLPoint;
begin


  bl.B:=DataSet.FieldByName(FLD_lat).AsFloat;
  bl.L:=DataSet.FieldByName(FLD_lon).AsFloat;

  bl_WGS :=geo_Pulkovo42_to_WGS84(bl);
  bl_CK95:=geo_Pulkovo42_to_CK_95(bl);

  CK42_to_GSK_2011(bl.B, bl.L, bl_GCK.B, bl_GCK.L);


  DataSet[FLD_lat_wgs]:=bl_WGS.B;
  DataSet[FLD_lon_wgs]:=bl_WGS.L;

  DataSet[FLD_lat_CK95]:=bl_CK95.B;
  DataSet[FLD_lon_CK95]:=bl_CK95.L;

  DataSet[FLD_LAT_GCK_2011]:=bl_GCK.B;
  DataSet[FLD_LON_GCK_2011]:=bl_GCK.L;


end;

// ---------------------------------------------------------------
procedure TdmLink_report.Update_Tilt;
// ---------------------------------------------------------------
var
  eH1: Double;
  eH2: Double;
  eRel_H1: Double;
  eRel_H2: Double;
  eTilt1: Double;
  eTilt2: Double;
  i: Integer;
begin
  eRel_H1 := FRelProfile.FirstItem().Rel_H;
  eRel_H2 := FRelProfile.LastItem().Rel_H;

//  FLinkData.LinkEnd1_Antennas

  for i:=0 to FLinkData.LinkEnd1_Antennas.Count-1 do
    FLinkData.LinkEnd1_Antennas[i].Ground_Height:=eRel_H1;

  for i:=0 to FLinkData.LinkEnd2_Antennas.Count-1 do
    FLinkData.LinkEnd2_Antennas[i].Ground_Height:=eRel_H2;



  eH1 := FLinkData.LinkEnd1_Antennas.GetMaxHeight_with_ground();
  eH2 := FLinkData.LinkEnd2_Antennas.GetMaxHeight_with_ground();


  eTilt1:=geo_TiltAngle  (FLinkData.Property1_Pos, FLinkData.Property2_Pos,
                          eH1,  eH2);

  eTilt2:=geo_TiltAngle  (FLinkData.Property2_Pos, FLinkData.Property1_Pos,
                          eH2,  eH1);


  db_AddRecord_(FDataSet_antenna1,
      [
        FLD_LINK_ID, FLink_ID,

        FLD_HEIGHT,       FLinkData.LinkEnd1_Antennas.GetHeightStr,
        FLD_POLARIZATION, FLinkData.LinkEnd1_Antennas.GetPOLARIZATIONStr,
        FLD_Diameter,     FLinkData.LinkEnd1_Antennas.GetDiameterStr,
        FLD_GAIN,         FLinkData.LinkEnd1_Antennas.GetGainStr,
        FLD_TILT,         eTilt1
      ]);

  db_AddRecord_(FDataSet_antenna2,
      [
        FLD_LINK_ID, FLink_ID,

        FLD_HEIGHT,       FLinkData.LinkEnd2_Antennas.GetHeightStr,
        FLD_POLARIZATION, FLinkData.LinkEnd2_Antennas.GetPOLARIZATIONStr,
        FLD_Diameter,     FLinkData.LinkEnd2_Antennas.GetDiameterStr,
        FLD_GAIN,         FLinkData.LinkEnd2_Antennas.GetGainStr,
        FLD_TILT,         eTilt2
      ]);
end;


begin

end.



