object dmMDB: TdmMDB
  OldCreateOrder = True
  OnCreate = DataModuleCreate
  Left = 599
  Top = 231
  Height = 597
  Width = 1321
  object ADOConnection_MDB: TADOConnection
    ConnectionString = 
      'Provider=Microsoft.Jet.OLEDB.4.0;Data Source=D:\1.mdb;Persist Se' +
      'curity Info=False;'
    LoginPrompt = False
    Mode = cmShareDenyNone
    Provider = 'Microsoft.Jet.OLEDB.4.0'
    Left = 60
    Top = 12
  end
  object t_Link: TADOTable
    Connection = ADOConnection_MDB
    CursorType = ctStatic
    TableName = 'Link'
    Left = 56
    Top = 120
  end
  object t_Property1: TADOTable
    Connection = ADOConnection_MDB
    CursorType = ctStatic
    IndexFieldNames = 'id'
    MasterFields = 'Property1_id'
    MasterSource = ds_Link
    TableName = 'property1'
    Left = 200
    Top = 128
  end
  object t_LinkEnd1: TADOTable
    Connection = ADOConnection_MDB
    CursorType = ctStatic
    IndexFieldNames = 'id'
    MasterFields = 'linkend1_id'
    MasterSource = ds_Link
    TableName = 'LinkEnd1'
    Left = 440
    Top = 128
  end
  object t_Property2: TADOTable
    Connection = ADOConnection_MDB
    CursorType = ctStatic
    IndexFieldNames = 'id'
    MasterFields = 'Property2_id'
    MasterSource = ds_Link
    TableName = 'property2'
    Left = 304
    Top = 128
  end
  object t_LinkEnd2: TADOTable
    Connection = ADOConnection_MDB
    CursorType = ctStatic
    IndexFieldNames = 'id'
    MasterFields = 'linkend2_id'
    MasterSource = ds_Link
    TableName = 'LinkEnd2'
    Left = 544
    Top = 128
  end
  object ds_Property2: TDataSource
    DataSet = t_Property2
    Left = 304
    Top = 216
  end
  object ds_Property1: TDataSource
    DataSet = t_Property1
    Left = 192
    Top = 216
  end
  object ds_LinkEnd1: TDataSource
    DataSet = t_LinkEnd1
    Left = 440
    Top = 208
  end
  object ds_LinkEnd2: TDataSource
    DataSet = t_LinkEnd2
    Left = 546
    Top = 208
  end
  object ds_Link: TDataSource
    DataSet = t_Link
    Left = 56
    Top = 208
  end
  object t_Antennas1: TADOTable
    Connection = ADOConnection_MDB
    CursorType = ctStatic
    IndexFieldNames = 'link_id'
    MasterFields = 'id'
    MasterSource = ds_Link
    TableName = 'Antennas1'
    Left = 720
    Top = 128
  end
  object t_Antennas2: TADOTable
    Connection = ADOConnection_MDB
    CursorType = ctStatic
    IndexFieldNames = 'link_id'
    MasterFields = 'id'
    MasterSource = ds_Link
    TableName = 'Antennas2'
    Left = 856
    Top = 128
  end
  object DataSource1: TDataSource
    Left = 720
    Top = 208
  end
  object DataSource2: TDataSource
    Left = 858
    Top = 208
  end
  object t_LInk_repeater: TADOTable
    Connection = ADOConnection_MDB
    CursorType = ctStatic
    TableName = 'LInk_repeater'
    Left = 592
    Top = 368
  end
  object t_LInk_repeater_ant: TADOTable
    Connection = ADOConnection_MDB
    CursorType = ctStatic
    MasterSource = ds_LInk_repeater
    TableName = 'lInk_repeater_ant'
    Left = 712
    Top = 368
  end
  object ds_LInk_repeater: TDataSource
    DataSet = t_LInk_repeater
    Left = 592
    Top = 424
  end
  object t_LInk_repeater_property: TADOTable
    Connection = ADOConnection_MDB
    CursorType = ctStatic
    MasterSource = ds_LInk_repeater
    TableName = 'LInk_repeater_property'
    Left = 904
    Top = 376
  end
  object t_Profile_items: TADOTable
    Connection = ADOConnection_MDB
    CursorType = ctStatic
    IndexFieldNames = 'link_id'
    MasterFields = 'id'
    MasterSource = ds_Link
    TableName = 'Profile_items'
    Left = 392
    Top = 368
  end
  object t_Items: TADOTable
    Connection = ADOConnection_MDB
    CursorType = ctStatic
    IndexFieldNames = 'parent_id'
    MasterFields = 'id'
    MasterSource = ds_groups
    TableName = 'Items'
    Left = 264
    Top = 368
  end
  object t_groups: TADOTable
    Connection = ADOConnection_MDB
    CursorType = ctStatic
    IndexFieldNames = 'link_id'
    MasterFields = 'id'
    MasterSource = ds_Link
    TableName = 'groups'
    Left = 152
    Top = 368
  end
  object ds_groups: TDataSource
    DataSet = t_groups
    Left = 152
    Top = 432
  end
  object ClientDataSet_Link_img: TClientDataSet
    Active = True
    Aggregates = <>
    IndexFieldNames = 'link_id'
    MasterFields = 'id'
    MasterSource = ds_Link
    PacketRecords = 0
    Params = <>
    Left = 48
    Top = 360
    Data = {
      980100009619E0BD01000000180000000B0000000000030000009801076C696E
      6B5F6964040001000400000004696D673104004B000000010007535542545950
      4502004900090047726170686963730004696D673204004B0000000100075355
      425459504502004900090047726170686963730004696D673304004B00000001
      0007535542545950450200490009004772617068696373000B70726F66696C65
      5F696D6704004B00000001000753554254595045020049000900477261706869
      637300076D61705F696D6704004B000000010007535542545950450200490009
      004772617068696373000D696D67315F66696C656E616D650100490000000100
      0557494454480200020064000D696D67325F66696C656E616D65010049000000
      01000557494454480200020064000D696D67335F66696C656E616D6501004900
      000001000557494454480200020064001470726F66696C655F696D675F66696C
      656E616D6501004900000001000557494454480200020064000C6D61705F6669
      6C656E616D6501004900000001000557494454480200020064000000}
    object ClientDataSet_Link_imglink_id: TIntegerField
      FieldName = 'link_id'
      Required = True
    end
    object ClientDataSet_Link_imgimg1: TGraphicField
      FieldName = 'img1'
      BlobType = ftGraphic
    end
    object ClientDataSet_Link_imgimg2: TGraphicField
      FieldName = 'img2'
      BlobType = ftGraphic
    end
    object ClientDataSet_Link_imgimg3: TGraphicField
      FieldName = 'img3'
      BlobType = ftGraphic
    end
    object ClientDataSet_Link_imgprofile_img: TGraphicField
      FieldName = 'profile_img'
      BlobType = ftGraphic
    end
    object ClientDataSet_Link_imgmap_img: TGraphicField
      FieldName = 'map_img'
      BlobType = ftGraphic
    end
    object ClientDataSet_Link_imgimg1_filename: TStringField
      FieldName = 'img1_filename'
      Size = 100
    end
    object ClientDataSet_Link_imgimg2_filename: TStringField
      FieldName = 'img2_filename'
      Size = 100
    end
    object ClientDataSet_Link_imgimg3_filename: TStringField
      FieldName = 'img3_filename'
      Size = 100
    end
    object ClientDataSet_Link_imgprofile_img_filename: TStringField
      FieldName = 'profile_img_filename'
      Size = 100
    end
    object ClientDataSet_Link_imgmap_filename: TStringField
      FieldName = 'map_filename'
      Size = 100
    end
  end
  object qry_LinkLine: TADOQuery
    Parameters = <>
    Left = 288
    Top = 32
  end
  object qry_LinkLine_links: TADOQuery
    Parameters = <>
    Left = 432
    Top = 32
  end
  object t_LinkLine: TADOTable
    Connection = ADOConnection_MDB
    CursorType = ctStatic
    TableName = 'Antennas1'
    Left = 664
    Top = 16
  end
  object t_LinkLine_links: TADOTable
    Connection = ADOConnection_MDB
    CursorType = ctStatic
    TableName = 'Antennas2'
    Left = 800
    Top = 16
  end
  object ClientDataSet_linkline_img: TClientDataSet
    Active = True
    Aggregates = <>
    Params = <>
    Left = 336
    Top = 480
    Data = {
      410000009619E0BD0100000018000000010000000000030000004100076D6170
      5F696D6704004B00000001000753554254595045020049000900477261706869
      6373000000}
    object ClientDataSet_linkline_imgmap_img: TGraphicField
      FieldName = 'map_img'
      BlobType = ftGraphic
    end
  end
end
