object dmLink_map_: TdmLink_map_
  OldCreateOrder = True
  OnCreate = DataModuleCreate
  OnDestroy = DataModuleDestroy
  Left = 1550
  Top = 744
  Height = 242
  Width = 370
  object qry_Properties: TADOQuery
    LockType = ltReadOnly
    Parameters = <>
    Left = 136
    Top = 28
  end
  object qry_Temp1: TADOQuery
    LockType = ltReadOnly
    Parameters = <>
    Left = 54
    Top = 29
  end
  object qry_Maps: TADOQuery
    LockType = ltReadOnly
    Parameters = <>
    Left = 226
    Top = 29
  end
  object ADOStoredProc_Link: TADOStoredProc
    Parameters = <>
    Left = 56
    Top = 120
  end
  object ADOQuery1: TADOQuery
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      
        'SELECT     dbo.Property.name, dbo.LinkEnd.project_id, dbo.LinkEn' +
        'dType.name AS Expr1, dbo.Property.lat, dbo.Property.lon'
      'FROM         dbo.LinkEnd LEFT OUTER JOIN'
      
        '                      dbo.LinkEndType ON dbo.LinkEnd.id = dbo.Li' +
        'nkEndType.id LEFT OUTER JOIN'
      
        '                      dbo.Property ON dbo.LinkEnd.property_id = ' +
        'dbo.Property.id'
      '')
    Left = 222
    Top = 117
  end
end
