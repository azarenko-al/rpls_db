unit dm_Link_SDB_report;

interface

// {$DEFINE gost}
//   {$IFDEF new}

  {.$DEFINE view_data}


  {.$DEFINE test_img}

uses
//  dm_MDB,                  


  //dm_MDB_v1,

  dm_Report,
         
  u_doc,

//  dm_MDB,

  u_ini_Link_report_params,

  //dm_FRep_short,

  dm_Onega_DB_data,

  //u_Link_report_vars,

  fr_Profile_Rel_Graph,

  dm_Main,

  dm_Rel_Engine,
  dm_Link,
  dm_Link_map,

//  dm_Document,

  //dm_Link_report_data,

  u_const_db,

  dm_Progress,

  u_classes,

  u_func,

  u_db,
  u_files,

//  u_img_png,

  u_Geo,

  u_rel_Profile,

  

  SysUtils, Classes, Controls, Forms, Dialogs, Db, ADODB, Variants,
  frxClass, frxExportRTF, frxExportPDF, frxExportXLS, frxDBSet,  frxDesgn, dxmdaset,
  frxExportBaseDialog;

type
  TdmLink_SDB_report = class(TdmProgress)
    frxDBDataset_link: TfrxDBDataset;
    frxDBDataset_Link_img: TfrxDBDataset;
    ADOConnection1: TADOConnection;
    frxPDFExport1: TfrxPDFExport;
    frxRTFExport1: TfrxRTFExport;
    frxXLSExport1: TfrxXLSExport;
    frxDesigner1: TfrxDesigner;
    dxMemData_Profile: TdxMemData;
    frxDBDataset_map: TfrxDBDataset;
    dxMemData_map: TdxMemData;
    frxReport1: TfrxReport;
    ADOStoredProc_Link: TADOStoredProc;
    ADOStoredProc_Link_SDB: TADOStoredProc;
    frxDBDataset_Header: TfrxDBDataset;
    frxReport_SDB: TfrxReport;
    ADOStoredProc_items: TADOStoredProc;
    frxDBDataset_items: TfrxDBDataset;
    dxMemData_mapprofile_img: TBlobField;
    dxMemData_Profileprofile_img: TBlobField;
    dxMemData_Profilefilename: TStringField;
    dxMemData_mapfilename: TStringField;
    dxMemData_Profilelink_name: TStringField;
//    procedure ADOStoredProc_LinkAfterScroll(DataSet: TDataSet);
//    procedure ClientDataSet_groupsAfterScroll(DataSet: TDataSet);
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
//    procedure frxDesigner1GetTemplateList(List: TStrings);
//    procedure qry_LinkEnd1CalcFields(DataSet: TDataSet);
 //   procedure qry_LinkEnd1CalcFields(DataSet: TDataSet);
  //  procedure qry_Property1CalcFields(DataSet: TDataSet);
//    procedure ds_ClientDataSet_Profile_itemsDataChange(Sender: TObject; Field:
  //      TField);
  private
    FTempFileDirList : TStringList;

//    FfrxReport: TfrxReport;

    Flinkdata : record
      ID: Integer;
      Name: string;

      Length_KM: double;
      profile_XML: string;

      ClutterModel_ID: Integer;

      TX_FREQ_MHz: double;

      BLVector: TBLVector;

      PROFILE_Step_M: Double;

      Antenna1_Heights: TDoubleArray;
      Antenna2_Heights: TDoubleArray;

    end;

//    FDataReady : boolean;

    FTempFileDir : string;


//    FIsDebug : Boolean;

   // FDataset_Link : TDataset;

//    FImgFileList: TStringList;


//    FLinkCalcResult: TLinkCalcResult;
    FDataSet_link          : TDataSet;
    FDataSet_link_SDB      : TDataSet;
    FDataSet_map           : TDataSet;
    FDataSet_Profile       : TDataSet;

//    FDataSet_Property1      : TDataSet;
//    FDataSet_Property2      : TDataSet;

//    FDataSet_groups        : TDataSet;
//    FDataSet_items         : TDataSet;
//    FDataSet_Profile_items : TDataSet;

//    FDataSet_antenna1      : TDataSet;
//    FDataSet_antenna2      : TDataSet;


//    FLink_ID: Integer;

//    FVector: TBLVector;
//    FClutterModelID: integer;


    procedure ReportExportByFilter(aExportFilter: TfrxCustomExportFilter; aExt,
        aFileName: String);

    procedure MakeProfileImage;

//    procedure MakeProfileImage_Repeater;
    procedure Profile_Open;
//    procedure Profile_Open_Repeater;
  private
//    FLinkData: TLinkData;

//    FRelProfile: TrelProfile;
//    FRelProfile2: TrelProfile;


//    FRefractionArr: array[1..3] of Double;// = (22,158,169);
  // 22  - 3. ���������� ������� �������� ��� a% ��������� - ����������� ���������
  // 158 - 6. ���������� ������� ���������� � ���.������������ V���
  // 169 - 18. ���.������� ���������������� ������������ ��������������


    FRelProfile: TrelProfile;

    procedure Init_Datasets(aReport: TfrxReport);

    procedure Open_SDB(aID: integer);

    procedure PrintReport;

//    function ShowDesignReport: Boolean;

//    procedure SetLinkQuery_for_LinkLine(aID_str: string);

//    procedure UpdateAliases;
  public

  //  procedure DesignReport;

    procedure ExecuteProc; override;



    class procedure Init;

    procedure Add_Default;

//    procedure SetLinkQuery_for_LinkLine_(aLinkIDList: TIDList);
  end;


//function dmLink_report: TdmLink_report;
var
  dmLink_SDB_report: TdmLink_SDB_report;

//==================================================================
// implementation
//==================================================================
implementation


{$R *.dfm}

//uses
 // f_Test,
//  dm_MDB;



const
//  FLD_profile_img_path='profile_img_path';
  FLD_img='img';

  FLD_HEIGHT        ='HEIGHT';
  FLD_POLARIZATION  ='POLARIZATION';
  FLD_Diameter      ='Diameter';

  FLD_map_filename = 'map_filename';
                          


class procedure TdmLink_SDB_report.Init;
begin
  if not Assigned(dmLink_SDB_report) then
    dmLink_SDB_report := TdmLink_SDB_report.Create(Application);
end;


// ---------------------------------------------------------------
procedure TdmLink_SDB_report.ReportExportByFilter(aExportFilter:  TfrxCustomExportFilter; aExt, aFileName: String);
// ---------------------------------------------------------------
var
  b: Boolean;
  k: Integer;
  sDocFile: string;
begin
  assert(aFileName<>'');

  aFileName:= ChangeFileExt(aFileName, aExt);

  b:= DeleteFile (aFileName);

  if FileExists(aFileName) then
     aFileName:= ChangeFileExt(aFileName, Format('_%d',[Random (100000)]) + aExt);

//  k:=Random (100000);



  aExportFilter.FileName := aFileName;
  aExportFilter.ShowDialog:= False;

  frxReport1.Export(aExportFilter);

  if Eq(aExt, '.rtf') then
  begin
    sDocFile:=ChangeFileExt(aFileName, '.doc');

    b:= DeleteFile (sDocFile);

    if FileExists(sDocFile) then
       sDocFile:= ChangeFileExt(sDocFile, Format('_%d',[Random (100000)]) + '.doc');


    RTF_to_doc (aFileName,  sDocFile);

    DeleteFile (aFileName);

   aFileName:= ChangeFileExt(aFileName, '.doc');

  end;


{
  if g_Link_Report_Param.IsSaveToDB then
    dmDocument.Add_link(g_Link_Report_Param.ProjectID,
                        FLinkData.ID,
                        FLinkData.Name,
                        aFileName);
 }


 if g_Link_Report_Param.IsOpenReportAfterDone then
    ShellExec (aFileName,'');

//      aExportFilter.ShowDialog:= True;
end;




//--------------------------------------------------------------------
procedure TdmLink_SDB_report.Init_Datasets(aReport: TfrxReport);
//--------------------------------------------------------------------
  procedure DoAdd(aItem: TfrxDataset);
  begin
    if not Assigned(aReport.DataSets.Find(aItem)) then
      aReport.DataSets.Add(aItem);
  end;

begin
//  exit;
  //////////////////

//  aReport.
  Assert (assigned (aReport));


  aReport.DataSets.Clear;

  DoAdd(frxDBDataset_link);
  DoAdd(frxDBDataset_Link_img);
  DoAdd(frxDBDataset_map);
  DoAdd(frxDBDataset_Header);
  DoAdd(frxDBDataset_items);


//  DoAdd(frxDBDataset_Property1);
//  DoAdd(frxDBDataset_Property2);
//  DoAdd(frxDBDataset_LinkEnd1);
//  DoAdd(frxDBDataset_LinkEnd2);

//  DoAdd(frxDBDataset_Antenna1);
//  DoAdd(frxDBDataset_Antenna2);


//  DoAdd(frxDBDataset_Profile_items);

//  DoAdd(frxDBDataset_group);
//  DoAdd(frxDBDataset_Items);

end;



//--------------------------------------------------------------------
procedure TdmLink_SDB_report.DataModuleCreate(Sender: TObject);
//--------------------------------------------------------------------

//var
//  sFileName: string;
 // sFileName_MDB: string;
begin
  inherited;

  if adoconnection1.connected then
    ShowMessage('TdmLink_SDB_report: adoconnection1.connected');


//  FfrxReport:=frxReport_base;


//  sFileName_MDB:=g_ApplicationDataDir + 'report.mdb';

  {
  TdmMDB.Init;

  dmMDB.CreateMDB(sFileName_MDB);
  dmMDB.Open_data;
  }


//
 // Params.IsPrintReport := True;

//  FImgFileList:=TStringList.Create;



  FRelProfile:=TrelProfile.Create;

//  FRelProfile1:=TrelProfile.Create;
//  FRelProfile2:=TrelProfile.Create;


//  db_SetComponentADOConnection (Self, dmMain.ADOConnection);

//  FLinkData := TLinkData.Create();


  //---------------------------
  // cala fields
  //---------------------------

  {
  db_OpenQuery(qry_Property1, 'SELECT * FROM '+ TBL_Property  +' where id=0');

  db_CreateFieldsForDataset(qry_Property1);
  db_CreateCalculatedField(qry_Property1, FLD_lat_wgs, ftFloat);
  db_CreateCalculatedField(qry_Property1, FLD_lon_wgs, ftFloat);

  db_CreateCalculatedField(qry_Property1, FLD_lat_CK95, ftFloat);
  db_CreateCalculatedField(qry_Property1, FLD_lon_CK95, ftFloat);


  db_CreateFields_FromSrcToDest(qry_Property1, qry_Property2);
 }

//  db_CreateFields_FromSrcToDest(qry_Property1, qry_LInk_repeater_property);

  {
  //---------------------------
  // cala fields
  //---------------------------
  db_OpenQuery(qry_LinkEnd1, 'SELECT * FROM '+ VIEW_LINKEND  +' where id=0');

  db_CreateFieldsForDataset(qry_LinkEnd1);
  db_CreateCalculatedField(qry_LinkEnd1, FLD_POWER_W, ftFloat);

  db_CreateFields_FromSrcToDest(qry_LinkEnd1, qry_LinkEnd2);
  }


//  qry_LInk_repeater.SQL.Text     := 'SELECT * FROM '+ VIEW_LINK_REPEATER          +' where link_id=:id';
//  qry_LInk_repeater_ant.SQL.Text := 'SELECT * FROM '+ VIEW_LINK_REPEATER_ANTENNA  +' where LINK_REPEATER_id=:id';

//  qry_LInk_repeater_property.SQL.Text := 'SELECT * FROM '+ TBL_PROPERTY  +' where id=:PROPERTY_id';



////  qry_Property1.SQL.Text := 'SELECT * FROM '+ TBL_Property  +' where id=:property1_id';
//  qry_Property2.SQL.Text := 'SELECT * FROM '+ TBL_Property  +' where id=:property2_id';



  FDataSet_link    :=ADOStoredProc_Link;  // ADOStoredProc_Link;
  FDataSet_map     :=dxMemData_map;
  FDataSet_Profile :=dxMemData_Profile;
  FDataSet_link_SDB:=ADOStoredProc_items;


//  FDataSet_antenna1:=dmMDB.t_Antennas1;//  ClientDataSet_Antenna1;
//  FDataSet_antenna2:=dmMDB.t_Antennas2;//ClientDataSet_Antenna2;


//  FDataSet_groups := dmMDB.t_groups;
//  FDataSet_items  := dmMDB.t_Items;




  // ---------------------------------------------------------------

//  frxDBDataset_link.DataSet     :=dmMDB.t_link;
//  frxDBDataset_Property1.DataSet:=dmMDB.t_Property1;
//  frxDBDataset_Property2.DataSet:=dmMDB.t_Property2;

//  frxDBDataset_LinkEnd1.DataSet :=dmMDB.t_LinkEnd1;
//  frxDBDataset_LinkEnd2.DataSet :=dmMDB.t_LinkEnd2;



//  frxDBDataset_group.DataSet:=FDataSet_groups;
//  frxDBDataset_Items.DataSet:=FDataSet_items;

//  frxDBDataset_Profile_items.DataSet:=FDataSet_Profile_items; //dmMDB.t_Profile_items;

//  frxDBDataset_Antenna1.DataSet:=dmMDB.t_Antennas1;
//  frxDBDataset_Antenna2.DataSet:=dmMDB.t_Antennas2;

//  frxDBDataset_Link_img.DataSet:=dmMDB.clientDataSet_Link_img;

  FTempFileDirList :=TStringList.create;

  dxMemData_Profile.Open;
  dxMemData_map.Open;

end;

//--------------------------------------------------------------------
procedure TdmLink_SDB_report.DataModuleDestroy(Sender: TObject);
//--------------------------------------------------------------------
var
  i: Integer;
begin
  for i:=0 to FTempFileDirList.Count-1 do
    DeleteDirectory (FTempFileDirList[i]);

  FreeAndNil(FTempFileDirList);

//  FreeAndNil(FImgFileList);


  FreeAndNil(FRelProfile);

//  FreeAndNil(FRelProfile1);
//  FreeAndNil(FRelProfile2);

//  FreeAndNil(FLinkData);

  inherited;
end;


//--------------------------------------------------------------------
procedure TdmLink_SDB_report.Open_SDB(aID: integer);
//--------------------------------------------------------------------
//const

  // -------------------------
  // ANTENNAS
  // -------------------------
{
  SQL_SELECT_LINKEND_ANT =
     'SELECT *  FROM '+ view_LINKEND_ANTENNAS + ' WHERE linkend_id=:linkend_id ORDER BY height desc';

  SQL_SELECT_LINKEND_ANT1 =
     'SELECT *  FROM '+ view_LINKEND_ANTENNAS + ' WHERE linkend_id=:id ORDER BY height desc';

  SQL_SELECT_LINKEND=  'SELECT * FROM '+ VIEW_LINKEND +' WHERE id=:id ';
  SQL_SELECT_LINKEND1= 'SELECT * FROM '+ VIEW_LINKEND +' WHERE id=:linkend1_id ';
  SQL_SELECT_LINKEND2= 'SELECT * FROM '+ VIEW_LINKEND +' WHERE id=:linkend2_id ';
 }

//  SQL_SELECT_reflection_POINTS = 'SELECT * FROM '+ TBL_Link_reflection_points + ' WHERE (link_id=:link_id)';



var
  eH1: Double;
  eH2: Double;
  eTilt1: Double;
  eTilt2: Double;

//  iPmpSectorID,iPmpTerminalID,
//  iLinkEndID1,iLinkEndID2: integer;

//  iProperty1: Integer;
//  iProperty2: Integer;
  i: integer;
  iCount: Integer;
  iRes: Integer;
  j: integer;
  k: Integer;
  k1: Integer;
  k2: Integer;
//  oLinkCalcResult: TLinkCalcResult;
  s: string;
 // sImgFileName: string;
  sMapImgFileName: string;


begin
  dmOnega_DB_data.OpenStoredProc(ADOStoredProc_Link,     'link_sdb.sp_link_SDB_report_SEL', ['id', aID]);
  dmOnega_DB_data.OpenStoredProc(ADOStoredProc_Link_SDB, 'link_sdb.sp_link_SDB_report_summary_SEL', ['id', aID]);
  dmOnega_DB_data.OpenStoredProc(ADOStoredProc_items,    'link_sdb.sp_Link_SDB_items_SEL', ['id', aID]);
                 

   Assert(ADOStoredProc_Link.RecordCount>0, 'FDataSet_link.RecordCount>0');

{
  if (FDataSet_link.RecordCount=0) then
  begin
    ShowMessage('Link not found!');
    Exit;
  end;
}

//  db_view (qry_Link);

//  db_view (qry_Link);

// --------------------------

//      k:=dmOnega_DB_data.Link_Select(ADOStoredProc_Link, iID);


//  FLink_ID:=aID;

//  Assert(FLink_ID>0, 'Value <=0');


  FTempFileDir:= IncludeTrailingBackslash (  g_Link_Report_Param.FileDir + Format('link_sdb_%d', [aID])  );
  FTempFileDirList.add(FTempFileDir);


 // iRes:=dmOnega_DB_data.Link_Select(ADOStoredProc_Link, aID);

 // FDataSet_link:=

 // db_OpenQuery (qry_Link, Format('SELECT type FROM '+ TBL_LINK + ' WHERE id=%d', [aID]));
//  i :=FDataSet_link.RecordCount;

//  FLinkData.




//  FVector        :=FLinkData.BLVector;

//  FClutterModelID:=FLinkData.ClutterModel_ID;


{
   iLinkEndID1:=FLinkData.LinkEndID1;
   iLinkEndID2:=FLinkData.LinkEndID2;

   iProperty1:=FLinkData.PropertyID1;
   iProperty2:=FLinkData.PropertyID2;


   qry_LinkEnd1.SQL.Text:= SQL_SELECT_LINKEND1;
   qry_LinkEnd2.SQL.Text:= SQL_SELECT_LINKEND2;

   qry_LinkEnd1.Open;
   qry_LinkEnd2.Open;
 }

//   k:=qry_LinkEnd1.RecordCount;

//   assert ((not qry_LinkEnd1.IsEmpty) and (not qry_LinkEnd2.IsEmpty)) ;

//       then
//       raise Exception.Create ('qry_LinkEnd1.IsEmpty or qry_LinkEnd2.IsEmpty');


//   qry_Antennas1.SQL.Text:= SQL_SELECT_LINKEND_ANT1;
//   qry_Antennas2.SQL.Text:= SQL_SELECT_LINKEND_ANT1;

//   qry_Antennas1.Open;
//   qry_Antennas2.Open;

{

   k1:=qry_Antennas1.RecordCount;
   k2:=qry_Antennas2.RecordCount;

   Assert(qry_Antennas1.RecordCount>0, 'Value =0');
   Assert(qry_Antennas2.RecordCount>0, 'Value =0');
 }

 //  FLinkData.LoadFromDataset(FDataSet_link);



//  qry_Property1.Open;
//  qry_Property2.Open;

//  FLinkData.Property1.LoadFromDataset(qry_Property1);
//  FLinkData.Property2.LoadFromDataset(qry_Property2);



//  dmMDB.ADOConnection_MDB.BeginTrans;
//  dmMDB.Clear;

//  db_AddRecordFromDataSet(FDataSet_link, dmMDB.t_Link);

  {
  db_AddRecordFromDataSet(qry_Property1, dmMDB.t_Property1);
  db_AddRecordFromDataSet(qry_Property2, dmMDB.t_Property2);

  db_AddRecordFromDataSet(qry_LinkEnd1, dmMDB.t_LinkEnd1);
  db_AddRecordFromDataSet(qry_LinkEnd2, dmMDB.t_LinkEnd2);
   }




//  Assert(iProperty1>0, 'Value =0');
 // Assert(iProperty2>0, 'Value =0');

  //
//    property Items[Index: Integer]: TLinkCalcResultItem read GetItems; default;

//  Assert(qry_property1.Active);
//  Assert(qry_property2.Active);

//  db_View(qry_property1);
//  db_View(qry_property2);

//  db_View(ADOStoredProc_Link);


//  k:=qry_property2.FieldByName(FLD_ID).AsInteger;
//  k:=qry_property1.FieldByName(FLD_ID).AsInteger;


 // FLinkData.P

 /////// FLinkData.Property1.Ground_Height

  ;


//  FLinkData.LinkEnd1_Antennas.LoadFromDataset(qry_Antennas1, FLinkData.Property1.Ground_Height);
//  FLinkData.LinkEnd2_Antennas.LoadFromDataset(qry_Antennas2, FLinkData.Property2.Ground_Height);

(*
  FLinkData.LinkEnd1_Antennas.LoadFromDataset(qry_Antennas1, qry_property1);
  FLinkData.LinkEnd2_Antennas.LoadFromDataset(qry_Antennas2, qry_property2);
*)

{
  FAntenna1_Heights:=FLinkData.LinkEnd1_Antennas.GetAntennaHeightArr;
  FAntenna2_Heights:=FLinkData.LinkEnd2_Antennas.GetAntennaHeightArr;


  eH1 := FLinkData.LinkEnd1_Antennas.GetMaxHeight_with_ground();
  eH2 := FLinkData.LinkEnd2_Antennas.GetMaxHeight_with_ground();


  eTilt1:=geo_TiltAngle  (FLinkData.Property1_Pos, FLinkData.Property2_Pos,
                          eH1,  eH2);

  eTilt2:=geo_TiltAngle  (FLinkData.Property2_Pos, FLinkData.Property1_Pos,
                          eH2,  eH1);
 }

{
  db_AddRecord_(FDataSet_antenna1,
      [
        FLD_LINK_ID, FLink_ID,

        FLD_HEIGHT,       FLinkData.LinkEnd1_Antennas.GetHeightStr,
        FLD_POLARIZATION, FLinkData.LinkEnd1_Antennas.GetPOLARIZATIONStr,
        FLD_Diameter,     FLinkData.LinkEnd1_Antennas.GetDiameterStr,
        FLD_GAIN,         FLinkData.LinkEnd1_Antennas.GetGainStr,
        FLD_TILT,         eTilt1
      ]);

  db_AddRecord_(FDataSet_antenna2,
      [
        FLD_LINK_ID, FLink_ID,

        FLD_HEIGHT,       FLinkData.LinkEnd2_Antennas.GetHeightStr,
        FLD_POLARIZATION, FLinkData.LinkEnd2_Antennas.GetPOLARIZATIONStr,
        FLD_Diameter,     FLinkData.LinkEnd2_Antennas.GetDiameterStr,
        FLD_GAIN,         FLinkData.LinkEnd2_Antennas.GetGainStr,
        FLD_TILT,         eTilt2
      ]);
   }


//   oLinkCalcResult := TLinkCalcResult.Create();

{
//  if FLinkData.CalcResultsXML<>'' then
  begin


 //   iCount1:=FDataSet_groups.RecordCount;
  //  iCount2:=FDataSet_items.RecordCount;

//    FDataSet_groups.Tag


    if FLinkData.LinkRepeater.ID= 0 then
    begin
      oLinkCalcResult.LoadFromXmlString(FLinkData.CalcResultsXML);


//      oLinkCalcResult.SaveToDataset_2_Datasets(FDataSet_groups, FDataSet_items, FLink_ID);

//      oLinkCalcResult.SaveToDataset_2_Datasets(dmMDB.t_groups, dmMDB.t_Items, FLink_ID);

    end ;



 //   FreeAndNil ( oLinkCalcResult );
  end;
 }

//  db_AddRecord_(ClientDataSet_img, [FLD_LINK_ID, FLink_ID]);
///  db_AddRecord_(FDataSet_img, [FLD_LINK_ID, FLink_ID]);

//  db_AddRecord_(dmMDB.t_link_img, [FLD_LINK_ID, FLink_ID]);

//  dmMDB.


//  Assert(qry_Property1.RecordCount>0);
//  Assert(qry_Property2.RecordCount>0);


//  qry_LInk_repeater.Open;
//  qry_LInk_repeater_ant.Open;

//  qry_LInk_repeater_property.Open;


 // db_View (qry_LInk_repeater_property);

//  db_View (qry_LInk_repeater);
//  db_View (qry_LInk_repeater_ant);



(*
  FSite1_Rel_H:=qry_property1.FieldByName(FLD_ground_height).AsFloat;
  FSite2_Rel_H:=qry_property2.FieldByName(FLD_ground_height).AsFloat;
*)

  //u_LinkCalcResult_new
//  oLinkCalcResult.LoadFromXmlString(FLinkData.CalcResultsXML);


 // FLinkCalcResult.SaveToDataset_2_Datasets(dxMemData_Groups, dxMemData_Items);

  {
  FRefractionArr[1]:=oLinkCalcResult.Groups.GetCalcParamValueByID(22);
  FRefractionArr[2]:=oLinkCalcResult.Groups.GetCalcParamValueByID(158);
  FRefractionArr[3]:=oLinkCalcResult.Groups.GetCalcParamValueByID(169);

  FreeAndNil(oLinkCalcResult);
  }

(*
   <GROUP name="3" caption=" 3.���������� ������� �������� ��� a% ���������">
      <ITEM id="22" value="1.07685" caption=" ����������� ��������� (��� a% ���������)"/>

   <GROUP name="6" caption=" 6.���������� ������� ���������� � ���.������������ V���">
      <ITEM id="158" value="0.57333" caption=" ����������� ��������� � ���.������������"/>

    <GROUP name="18" caption=" 18.���.������� ���������������� ������������ ��������������">
      <ITEM id="169" value="0.2909" caption=" ����������� ��������� (��� ����.�����.)"/>


*)
//  FImgFileList.Clear;

  with ADOStoredProc_Link do
    while not EOF do
  begin
    Flinkdata.ID   :=FieldByName(FLD_ID).AsInteger;
    Flinkdata.Name :=FieldByName(FLD_NAME).AsString;

    Flinkdata.Length_KM  :=FieldByName(FLD_Length_KM).AsFloat;
    Flinkdata.profile_XML:=FieldByName(FLD_profile_XML).AsString;
    Flinkdata.ClutterModel_ID:=FieldByName(FLD_CLUTTER_MODEL_ID).AsInteger;

    Flinkdata.TX_FREQ_MHz:=FieldByName(FLD_TX_FREQ_MHz).AsFloat;
    Flinkdata.BLVector :=db_ExtractBLVector (ADOStoredProc_Link);
//    Flinkdata.BLVector: TBLVector;
    Flinkdata.PROFILE_Step_M:=FieldByName(FLD_PROFILE_STEP).AsInteger;

    Flinkdata.Antenna1_Heights:= MakeDoubleArray([FieldByName('antenna1_height').AsFloat]);
    Flinkdata.Antenna2_Heights:= MakeDoubleArray([FieldByName('antenna2_height').AsFloat]);

//      Antenna1_Heights: TDoubleArray;
  //    Antenna2_Heights: TDoubleArray;


  //  if FLinkData.LinkRepeater.ID = 0 then
    Profile_Open() ;
                         

    MakeProfileImage();

    Next;
  end;


//  Assert (FTempFileDir<>'');



 // sMapImgFileName:=g_Link_Report_Param.FileDir + Format('link%d_map.bmp', [aID]);

  // ----------------Link_SaveToImage-----------------------------------------------

  if g_Link_Report_Param.IsMakeMap then
  begin

    DoProgressMsg ('���������� �����...');
   ///////////
    sMapImgFileName:=FTempFileDir + 'map.bmp';

    TdmLink_map_.Link_SaveToImage ( Flinkdata.ID, sMapImgFileName);

//    FImgFileList.Add(sMapImgFileName);


//    Assert(FDataSet_img.RecordCount>0);


    FDataSet_map.Append;
//    FDataSet_img[FLD_map_filename]:=sMapImgFileName;

    TGraphicField(FDataSet_map.FieldByName(FLD_img)).LoadFromFile(sMapImgFileName);
    FDataSet_map.Post;

//    FImgFileList.Add(sMapImgFileName);


//    for i:=0 to FImgFileList.Count-1 do
//      DeleteFile(FImgFileList[i]);

  end;

/////////////////  Tdlg_Test.ExecDlg;

 // Result:=True;

 // UpdateAliases();


 // dmMDB.ADOConnection_MDB.CommitTrans;


 // frxDBDataset_UpdateAll (Self);



end;


//--------------------------------------------------------------------
procedure TdmLink_SDB_report.MakeProfileImage;
//--------------------------------------------------------------------
//const
//  PARAM_IDs: array[1..3] of integer = (22,158,169);
  // 22  - 3. ���������� ������� �������� ��� a% ��������� - ����������� ���������
  // 158 - 6. ���������� ������� ���������� � ���.������������ V���
  // 169 - 18. ���.������� ���������������� ������������ ��������������

var
  oForm: Tframe_Profile_Rel_Graph;
  sDir: string;
  i: Integer;
  dRefraction: double;
//  dRefraction, dOldRefraction: double;
  b: Boolean;
  d: Double;
  eAzimuth1: Double;
  eAzimuth2: Double;
  eH1: Double;
  eH2: Double;
  eMax1: Double;
  eMax2: Double;
  eTilt1: Double;
  eTilt2: Double;
//  iDirection: integer;
//  is_profile_reversed: Boolean;
  s: string;
//  oGraphicField: TGraphicField;
  sFieldName: string;
//  sFileName: string;
  sImgFileName: string;
//  sPNGFileName: string;
//  sPNGFileName: string;
begin
// Assert(FLink_ID>0);

//  sImgFileName:=g_Link_Report_Param.FileDir + Format('link%d_profile.bmp',[FLink_ID]);
//  FImgFileList.Add(sImgFileName);


  Assert (FTempFileDir<>'');

  sImgFileName:=FTempFileDir + Format ('profile_%d.bmp', [FLinkData.ID]);


  oForm:=Tframe_Profile_Rel_Graph.Create(nil);


 // oForm.Params. IsShowSites:=True;

 oForm.Params.IsShowReflectionPoints  := g_Link_Report_Param.IsShowReflectionPoints;

  oForm.Params.IsShowFrenelZoneTop:=True;
 // oForm.Params.IsShowFrenelZoneBottom:=True;

 // b:=Params.Profile_IsShowFrenelZoneBottomLeftRight;
  b :=True;
  oForm.Params.IsShowFrenelZoneBottomLeft:=b;
  oForm.Params.IsShowFrenelZoneBottomRight:=b;


 // oForm.Params.IsShowReflectionPoints:=True;

  oForm.Width :=550;
  oForm.Height:=400;

 // oForm.SetDefaultSize;
  oForm.Height:= Trunc(oForm.Height*1.2);
  oForm.Width := Trunc(oForm.Width *1.2);

  {
  s:=FLinkData.Profile_XML;

  ShellExec_Notepad_temp1(s);
   }

///////  dOldRefraction:= FRelProfile.Refraction;
  oForm.RelProfile_Ref:=FRelProfile;

//  d:=FLinkData.Length_KM;

(*

  // -------------------------
  if not VarIsNull(oDBLink.Link_repeater.Property_Ground_Height) then
    eRel1 :=oDBLink.Link_repeater.Property_Ground_Height
  else
    eRel1 :=FrelProfile2.FirstItem().Rel_H;

  // -------------------------
  if not VarIsNull(oDBLink.Property2_Ground_Height) then
    eRel2 :=oDBLink.Property2_Ground_Height
  else
    eRel2 :=FrelProfile2.LastItem().Rel_H;

  // -------------------------
  Fframe_Profile_Rel.Params.SiteGroups[1].Site1.Rel_H:=eRel1;
  Fframe_Profile_Rel.Params.SiteGroups[1].Site2.Rel_H:=eRel2;


*)
 // is_profile_reversed:=FLinkData.is_profile_reversed;

//  is_profile_reversed:=ADOStoredProc_Link.FieldByName(FLD_is_profile_reversed).AsBoolean;

  // -------------------------
//  if FLinkData.LinkRepeater.ID =0 then
  // -------------------------
 // begin
    oForm.Params.SetSiteGroupCount(1);

    oForm.Params.SiteGroups[0].Distance_KM:=Flinkdata.Length_KM;
    //FRelProfile.GeoDistance_KM();

  //  iDirection:=ADOStoredProc_Link.FieldByName(FLD_CALC_DIRECTION).AsInteger;



    oForm.Params.SiteGroups[0].Site1.AntennaHeights:=Flinkdata.Antenna1_Heights;
    oForm.Params.SiteGroups[0].Site2.AntennaHeights:=Flinkdata.Antenna2_Heights;

//    oForm.Params.SiteGroups[0].Site1.AntennaHeights:=GetAntennaHeightArr(qry_Antennas1);
//    oForm.Params.SiteGroups[0].Site2.AntennaHeights:=GetAntennaHeightArr(qry_Antennas2);


    oForm.Params.SiteGroups[0].Site1.Rel_H:=FRelProfile.FirstItem.Rel_H;
    oForm.Params.SiteGroups[0].Site2.Rel_H:=FRelProfile.LastItem.Rel_H;

//    Double


    eMax1:=DoubleArray_GetMax(Flinkdata.Antenna1_Heights);
    eMax2:=DoubleArray_GetMax(Flinkdata.Antenna2_Heights);


    eH1:=oForm.Params.SiteGroups[0].Site1.Rel_H + eMax1;
    eH2:=oForm.Params.SiteGroups[0].Site2.Rel_H + eMax2;

   // g_log.Add();



  //  eTilt1:=geo_TiltAngle  (FLinkData.Property1_Pos, FLinkData.Property2_Pos,  eH1,eH2);

   // eTilt2:=geo_TiltAngle  (FLinkData.Property2_Pos, FLinkData.Property1_Pos, eH2,eH1);

(*
    eAzimuth1:=geo_Azimuth (FLinkData.Property1_Pos, FLinkData.Property2_Pos);
    eAzimuth2:=geo_Azimuth (FLinkData.Property2_Pos, FLinkData.Property1_Pos);

*)

  //  aPoint1,aPoint2: TBLPoint; aSiteHeight1,aSiteHeight2: double): double;

    {

    db_UpdateRecord__(FDataSet_link,
           [
       //     FLD_AZIMUTH1, eAzimuth1,
        //    FLD_AZIMUTH2, eAzimuth2,

            FLD_ELEVATION_angle_1, eTilt1,
            FLD_ELEVATION_angle_2, eTilt2
           ]);
     }
//     Assert(FDataSet_link);
{
  db_view (dmMDB.t_Link);


  dmMDB.t_Link.Edit;
  dmMDB.t_Link[FLD_ELEVATION_angle_1]:=eTilt1;
  dmMDB.t_Link[FLD_ELEVATION_angle_2]:=eTilt2;
  dmMDB.t_Link.Post;


    db_UpdateRecord__(dmMDB.t_Link,
           [
       //     FLD_AZIMUTH1, eAzimuth1,
        //    FLD_AZIMUTH2, eAzimuth2,

            FLD_ELEVATION_angle_1, eTilt1,
            FLD_ELEVATION_angle_2, eTilt2
           ]);

}

//    FLinkData.Property1_Ground_Height



//    oForm.Params.SiteGroups[0].Site1.Rel_H:=FRelProfile.FirstItem.Rel_H;
//    oForm.Params.SiteGroups[0].Site2.Rel_H:=FRelProfile.LastItem.Rel_H;



//
    oForm.Params.SiteGroups[0].ReflectionPointArr:=dmLink.GetReflectionPointArray(Flinkdata.ID);

//  end;


  oForm.Params.Freq_MHz:= FLinkData.TX_FREQ_MHz;
  //qry_LinkEnd1.FieldByName(FLD_TX_FREQ_MHz).AsFloat;

///    (qry_LinkEnd1.FieldByName(FLD_TX_FREQ).AsFloat +
  ///   qry_LinkEnd1.FieldByName(FLD_RX_FREQ).AsFloat) / 2;


  oForm.ShowGraph;
  oForm.SaveToBmpFile (sImgFileName);
  FreeAndNil(oForm);


//  ShellExec(sImgFileName);



(*
  sPNGFileName:= ChangeFileExt(sImgFileName, '.png');

  img_BMP_to_PNG(sImgFileName, sPNGFileName);
*)


  FDataSet_Profile.Append;
  FDataSet_Profile['link_name']:=Flinkdata.Name;

//  FDataSet_img.Edit;
  TGraphicField(FDataSet_Profile.FieldByName(FLD_img)).LoadFromFile(sImgFileName);
  FDataSet_Profile.Post;
  
//  FImgFileList.Add(sImgFileName);



//

//////////////  FRelProfile.SetRefraction(dOldRefraction);


//  FDataSet_img.Post;

end;


// ---------------------------------------------------------------
procedure TdmLink_SDB_report.Profile_Open;
// ---------------------------------------------------------------
var
  sProfile_XML: string;
  s: string;
  b: Boolean;
  eLen_km: Double;
  i: Integer;
  k: Integer;
begin

  DoProgressMsg ('���������� �������...');

//  sProfile_XML:=ADOStoredProc_Link.FieldByName(FLD_profile_XML).AsString;
  sProfile_XML:=FLinkData.profile_XML;

  //Assert(Assigned(dmRel_Engine), 'Value not assigned');

  dmRel_Engine.AssignClutterModel (FLinkData.ClutterModel_ID, FRelProfile.Clutters);


  eLen_km:= geo_Distance_KM(FLinkData.BLVector);


  if (sProfile_XML='') or (not FRelProfile.LoadFromXml(sProfile_XML, eLen_km)) then
  begin
    dmRel_Engine.Open; //ByVector (FVector);
    b:=dmRel_Engine.BuildProfile1 (FRelProfile, FLinkData.BLVector, FLinkData.PROFILE_Step_M);//, FClutterModelID);


   // FRelProfile.Property1_id:=FLinkData.PropertyID1;
    //FRelProfile.Property2_id:=FLinkData.PropertyID2;

    sProfile_XML:=FRelProfile.GetXML();

//////    db_UpdateRecord(ADOStoredProc_Link, [db_Par(FLD_profile_XML, sProfile_XML)]);
  end ;

//  else
//    FRelProfile.isDirectionReversed := FLinkData.is_profile_reversed;

  // -------------------------
(*
  if VarIsNull(FLinkData.Property1_Ground_Height) then
    FLinkData.Property1_Ground_Height := FRelProfile.FirstItem().Rel_H;

  if VarIsNull(FLinkData.Property2_Ground_Height) then
    FLinkData.Property2_Ground_Height := FRelProfile.LastItem().Rel_H;
*)
  // -------------------------


 // oForm.Params.SiteGroups[0].Site1.Rel_H:=FRelProfile.FirstItem.Rel_H;
//  oForm.Params.SiteGroups[0].Site2.Rel_H:=FRelProfile.LastItem.Rel_H;

//  k:=ClientDataSet_Profile_items.RecordCount;

//  ClientDataSet_Profile_items.EmptyDataSet;

  //k:=ClientDataSet_Profile_items.RecordCount;


  FRelProfile.ApplyDefaultClutterHeights;

  

{
  FDataSet_Profile_items.Close;
  FDataSet_Profile_items.Open;

  while FDataSet_Profile_items.RecordCount>0 do
    FDataSet_Profile_items.delete;

}



  {
  with FRelProfile do
     for i:=0 to Count-1 do
     begin
       db_AddRecord_ (FDataSet_Profile_items,
         [//xml_ATT ('R',          TruncFloat(Data.Items[i].Distance,3) ),


           FLD_LINK_ID,  FLink_ID ,

           FLD_INDEX,  i+1 ,


           'distance_km',  TruncFloat(Items[i].Distance_KM,3) ,
           'ground_h',     Items[i].Rel_H ,
           'clutter_h',    Items[i].Clutter_H ,
           'earth_h',      TruncFloat(Items[i].Earth_H),

           'clutter_code',  Items[i].Clutter_Code,
           'clutter_color', GetClutterColorByCode(Items[i].Clutter_Code),
           'clutter_name',  GetClutterNameByCode(Items[i].Clutter_Code)
         ]);
                
     end;

   }
   
 // db_View(ClientDataSet_Profile);


// [Link."ELEVATION_angle_1"]



end;


// ---------------------------------------------------------------
procedure TdmLink_SDB_report.Add_Default;
// ---------------------------------------------------------------
const
  DEF_link = 'Link_SDB';


  DEF_NAME_SDB   ='FR - ����� �� SDB';

//  DEF_NAME_REPEATER   ='FR - ����� �� �� ��������� (��������� ������������)';

 // DEF_NAME_FOR_MTS_passiv ='FR - ����� �� �� ��������� (����������� ��� MTS-T) ��������� ������������';

 DEF_NAMES: array[0..0] of string =
      (
        DEF_NAME_SDB
      );

var
  I: Integer;
  k: Integer;
  rec: TdmReportAddRec;
  s: string;
  sTempDir: string;
//  sTempFileName: string;
begin
  for I := 0 to High(DEF_NAMES) do    // Iterate
  begin
    s:= Format('DELETE from %s where name=''%s''', [TBL_REPORT, DEF_NAMES[i]]); // 'select * from property where (id=:property2_id)');
    dmMain.ADOConnection1.Execute(s);

  end;    // for



  sTempDir:=GetTempFileDir;

//  sTempDir:='d:\report\';

//  ForceDirectories(sTempDir);



  // -------------------------

  FillChar(rec,SizeOf(rec),0);

  rec.Category:=DEF_link;


 // -------------------------
  rec.Name:=DEF_NAME_SDB;
  rec.FileName:= sTempDir +  rec.Name + '.fr3';

  frxReport_SDB.SaveToFile(rec.FileName);

  k:=dmReport.Add(rec);


end;

//--------------------------------------------------------------------
procedure TdmLink_SDB_report.ExecuteProc;
//--------------------------------------------------------------------
var
  i: integer;
//  iID: Integer;
 // k: Integer;
begin
  Assert (g_Link_Report_Param.IDList.Count>0);


 // CreateTempData( ChangeFileExt(g_Link_Report_Param.TemplateFileName, '.mdb') );


  //153522

  CursorHourGlass;

  Init_Datasets (frxReport1);


    if  g_Link_Report_Param.IsPreview  then
    begin
      Open_SDB (g_Link_Report_Param.IDList[0].ID);

      frxReport1.ShowReport;

//      dmMDB.Clear;
    end

    else
//  if not FDataReady then

    for i:=0 to g_Link_Report_Param.IDList.Count-1 do
    begin
      DoProgress (i, g_Link_Report_Param.IDList.Count);


   ///   iID:=;


     // Link_ExecuteItem (iID)  ;

      //DoProgressMsg ('���������� ������...');

   //   SELECT * FROM view_link WHERE id=@ID

     // k:=dmOnega_DB_data.Link_Select(ADOStoredProc_Link, iID);
      Open_SDB (g_Link_Report_Param.IDList[i].ID);

//      Assert (FDataSet_link.RecordCount=1);
//      Assert (frxDBDataset_link.DataSet.RecordCount=1);

      PrintReport();

//      dmMDB.Clear;

  //    Init_Datasets (FfrxReport);



      //and  Params.IsPrintReport
   //   if (not g_Link_Report_Param.IsPreview)   then
    //    PrintReport();

    end;
  ////////////////////////////

  {$IFDEF view_data}
//    Tdlg_Test.ExecDlg;
  {$ENDIF}

 //

  ///////////////////////////////



//  FDataReady:=True;


  CursorDefault;


{
  if (not g_Link_Report_Param.IsPreview)   then
     PrintReport();



  if g_Link_Report_Param.IsPreview then
    FfrxReport.ShowReport;
 }

end;


// ---------------------------------------------------------------
procedure TdmLink_SDB_report.PrintReport;
// ---------------------------------------------------------------
var
  k: Integer;
  sName, sTempFileName: string;
begin
  sName:=FDataSet_link.FieldByName(FLD_NAME).AsString;



//db_View (dxMemData_link);


//    sTempFileName := g_Link_Report_Param.FileDir + CorrectFileName(FLinkData.Name);

    sTempFileName := g_Link_Report_Param.FileDir + CorrectFileName(sName);


    frxReport1.PrepareReport;

    dmOnega_DB_data.Activity_Log('link_SDB_report', dmMain.ProjectID);


    case g_Link_Report_Param.FileType of
      ftFileExcel : ReportExportByFilter(frxXLSExport1, '.xls', sTempFileName);
      ftFilePDF   : ReportExportByFilter(frxPDFExport1, '.pdf', sTempFileName);

      ftFileWord  : ReportExportByFilter(frxRTFExport1, '.rtf', sTempFileName);

    end;

end;



begin

end.

(*


{


// ---------------------------------------------------------------
procedure TdmLink_SDB_report.DesignReport;
// ---------------------------------------------------------------
var
  sTemplateFileName: string;
begin
  inherited;

  sTemplateFileName := g_Link_Report_Param.FileDir + Format('report_%d.rep', [g_Link_Report_Param.Report_ID]);

//  sTemplateFileName := 'd:\11111.tmp';


 // Assert(Params.Report_ID>0);

 Assert (g_Link_Report_Param.Report_ID > 0);
  dmReport.SaveToFile(g_Link_Report_Param.Report_ID, sTemplateFileName);

//  bSave := False;

//  case g_Link_Report_Param.Category_Type of
   // ctLink_SDB:
    frxReport1.LoadFromFile(sTemplateFileName);
//    ctLinkLIne: FfrxReport.LoadFromFile(sTemplateFileName);

//  end;


  if ShowDesignReport then
  begin
    frxReport1.SaveToFile(sTemplateFileName);

    dmReport.LoadFromFile(g_Link_Report_Param.Report_ID, sTemplateFileName);

  end;

(*
  if bSave then
     dmReport.LoadFromFile(g_Link_Report_Param.Report_ID, sTemplateFileName);
*)

end;





// ---------------------------------------------------------------
function TdmLink_SDB_report.ShowDesignReport: Boolean;
// ---------------------------------------------------------------
var
  b: Boolean;
  oStream1: TMemoryStream;
  oStream2: TMemoryStream;
begin
 // dmOnega_DB_data.OpenQuery_(qry_Link, 'SELECT * FROM view_link_report WHERE id=:ID', ['id', 0]);

 // db_view (qry_Link);

//  frxDBDataset_UpdateAll(Self);

{
s:=Format('SELECT * FROM %s where id=0', [View_Link_freq_order]);

 // /'/  SELECT * FROM	view_Link  where id IN (@ID_STR)  order by name

  db_OpenQuery(qry_LInks, s);

  frxDBDataset_UpdateAll(Self);
 }

//  frxDBDataset_UpdateAll (Self);

  //--------------------------------

//  Init_Datasets (FfrxReport);

//  frxReport.PrepareReport(true);
//  frxReport.ShowReport;
  oStream1:=TMemoryStream.Create;
  oStream2:=TMemoryStream.Create;

  frxReport1.SaveToStream(oStream1);


  frxReport1.DesignReport(True);
//  frxReport.m
  b:=frxReport1.Modified;

  frxReport1.SaveToStream(oStream2);


  b:=not u_func.CompareMemoryStreams(oStream1,oStream2);

  Result := b;

  FreeAndNil(oStream1);
  FreeAndNil(oStream2);

end;





