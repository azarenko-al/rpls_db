unit I_Link_report;

interface

uses ADOInt,
  u_dll;

type

  ILink_report_X = interface(IInterface)
  ['{AB3197A4-5A57-4779-B999-446DBAAC4A5D}']

    procedure InitADO(aConnection: _Connection); stdcall;
  //  procedure InitAppHandle(aHandle: integer); stdcall;

    procedure Execute (aFileName: WideString);  stdcall;
  end;

var
  ILink_report: ILink_report_X;

  function Load_ILink_report: Boolean;
  procedure UnLoad_ILink_report;

implementation

const
//  DEF_BPL_FILENAME = 'bpl_LOS.bpl';
  DEF_FILENAME = 'dll_Link_report.dll';

var
  LHandle : Integer;


function Load_ILink_report: Boolean;
begin
  Result := GetInterface_DLL(DEF_FILENAME, LHandle,  ILink_report_X, ILink_report)=0;
end;


procedure UnLoad_ILink_report;
begin
  ILink_report := nil;
  UnloadPackage_dll(LHandle);
end;


end.



