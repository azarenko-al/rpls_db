unit u_ini_Link_report_params;

interface
uses
  IniFiles, SysUtils,

  u_func,
  u_files,

  
  u_classes
  ;

type
  // ---------------------------------------------------------------
  TIni_Link_Report_Param = class
  // ---------------------------------------------------------------
  private
 //   ID: integer;
 //   LinkLineID: integer;
  public
    ConnectionString : string;


 //   ObjName   : string;
    ProjectID : integer;

    Category  : string;
    Category_Type  : (ctLink,ctLinkLIne,ctLink_SDB);

//    LinkLineIDList: TIDList;
    IDList: TIDList;


  public
    WMS: record
      Checked: Boolean;
      LayerName :string;
      URL :string;
      EPSG: word;
      Z_max: byte;

      Z: integer;
      IsAutoScale : boolean;
    end;


    FileDir          : string;

    FileType : (ftFileExcel,ftFileWord,ftFilePDF);


    Report_ID         : integer;
   // ReportFileName   : string;
   // Action           : string;
    IsSaveToDB   : boolean;
    IsOpenReportAfterDone  : boolean;
    IsPreview    : Boolean;

    IsAutoScale : Boolean;
    IsMakeMap : Boolean;

  //  MapDesktopID     : integer;
    ImageWidth       : integer;
    ImageHeight      : integer;

    Scale            : integer;

    IsShowReflectionPoints : Boolean;

    Profile_IsShowFrenelZoneBottomLeftRight: boolean;


    IsTest:  Boolean;


    TemplateFileName : string;

    constructor Create;
    destructor Destroy; override;
    function GetTempReportFile: string;

    procedure LoadFromINI(aFileName: string);
    procedure SaveToINI(aFileName: string);

    function Validate: boolean;

  end;


var
  g_Link_Report_Param: TIni_Link_Report_Param;


implementation


constructor TIni_Link_Report_Param.Create;
begin
  inherited Create;
 // LinkLineIDList := TIDList.Create();
  IDList := TIDList.Create();
end;


destructor TIni_Link_Report_Param.Destroy;
begin
  FreeAndNil(IDList);
  //FreeAndNil(LinkLineIDList);
  inherited Destroy;
end;

// ---------------------------------------------------------------
function TIni_Link_Report_Param.GetTempReportFile: string;
// ---------------------------------------------------------------
begin
  Result := FileDir + Format('report_%d.rep', [Report_ID]);

  // TODO -cMM: TIni_Link_Report_Param.GetTempReportFile default body inserted
end;


//--------------------------------------------------------------
procedure TIni_Link_Report_Param.LoadFromINI(aFileName: string);
//--------------------------------------------------------------
var
  oIniFile: TIniFile;
 // sCategory: string;
  sScale: string;

begin
  if not FileExists(aFileName) then
    Exit;

  oIniFile := TIniFile.Create(aFileName);

  IDList.LoadIDsFromIniSection(oIniFile, 'items');
 // LinkLineIDList.LoadIDsFromIniSection (oIniFile, 'LinkLines');

  with oIniFile do
  begin
    IsTest   :=ReadBool   ('main', 'IsTest', False);

    ConnectionString :=ReadString ('main', 'ConnectionString', '');
Assert(ConnectionString <> '');

    Category  :=ReadString ('main', 'Category', 'link');

    Category_Type := ctLink;

    if Eq(Category,'link')     then Category_Type := ctLink else
    if Eq(Category,'link_SDB') then Category_Type := ctLink_SDB else
    if Eq(Category,'linkline') then Category_Type := ctLinkLIne;


  //  Category_Type  : (ctLink,ctLinkLIne);


    ProjectID        :=ReadInteger('main', 'ProjectID',   0);

  //  FileDir          :=ReadString ('main', 'FileDir', '');
 //   FileDir          :=IncludeTrailingBackslash(FileDir);

//    ID               :=ReadInteger('main', 'ID', 0);
  //  ObjName          :=ReadString ('main', 'ObjName', '');

    Report_ID         :=ReadInteger('main', 'ReportID',    0);
//    ReportFileName   :=ReadString ('main', 'ReportFileName', '');
  //  Action           :=ReadString ('main', 'Action',      '');
  //  LinkLineID       :=ReadInteger('main', 'LinkLineID',  0);



(*


    IsAutoScale :=ReadBool('main', 'IsAutoScale', False );
    IsShowReflectionPoints :=ReadBool('main', 'IsShowReflectionPoints', False );


    IsSaveToDB    :=ReadBool('main', 'IsSaveToDB', False );

   // IsSaveDocsToDB := True;

    IsOpenReportAfterDone   :=ReadBool('main', 'IsOpenReportAfterDone', False );

 //   IsShowReportDoc := True;


    MapDesktopID     :=ReadInteger('main', 'MapDesktopID', 0);
    Assert(MapDesktopID>0, 'Value <=0');


    ImageWidth       :=ReadInteger('main', 'ImageWidth',  10 );
    ImageHeight      :=ReadInteger('main', 'ImageHeight', 10 );
    Scale            :=ReadInteger('main', 'Scale',       0);

    Assert(Scale>0);


    Profile_IsShowFrenelZoneBottomLeftRight:=
      ReadBool('main', 'Profile_IsShowFrenelZoneBottomLeftRight', False );

*)

  end;

  FreeAndNil(oIniFile);
//  oIniFile.Free;

(*
  if Scale=0 then
  begin
    sScale:='1 000 000';
    if InputQuery('����� ��������', '�������:', sScale) then
      Scale:= AsInteger( ReplaceStr (sScale, ' ','') )
  end;
*)

end;

// ---------------------------------------------------------------
procedure TIni_Link_Report_Param.SaveToINI(aFileName: string);
// ---------------------------------------------------------------
var
  oInifile: TIniFile;
  sCategory: string;
begin
  //Assert(MapDesktopID>0, 'Value <=0');

  DeleteFile(aFileName);


  ForceDirByFileName(aFileName);

//  ForceDirectories (ExtractFileDir (aFileName));

  oInifile:=TIniFile.Create (aFileName);

  with oInifile do
  begin
    WriteBool   ('main', 'IsTest', IsTest);


    Assert (ConnectionString<>'');

    WriteString ('main', 'ConnectionString', ConnectionString);


//    case Category_Type of
//      ctLink     : sCategory:='link';
//      ctLinkLIne : sCategory:='linkline';
//    end;


    WriteString ('main', 'Category',      Category);

    WriteInteger('main', 'ProjectID',    ProjectID);

 //   WriteInteger('main', 'ID',           ID);
 //   WriteString ('main', 'ObjName',      ObjName);

 //   WriteString ('main', 'FileDir',      FileDir);
//    WriteInteger('main', 'ReportID',     Report_ID);

(*    WriteInteger('main', 'MapDesktopID', MapDesktopID);
    WriteInteger('main', 'ImageWidth',   ImageWidth);
    WriteInteger('main', 'ImageHeight',  ImageHeight);
    WriteInteger('main', 'Scale',        Scale);
    WriteBool   ('main', 'IsAutoScale',  IsAutoScale);
*)

  //  WriteString ('main', 'ReportFileName', ReportFileName);
 //   WriteString ('main', 'Action',         Action);

//    WriteInteger('main', 'LinkLineID',     LinkLineID);

(*    WriteBool   ('main', 'IsSaveToDB',     IsSaveToDB);
    WriteBool   ('main', 'IsOpenReportAfterDone',IsOpenReportAfterDone);

 //   WriteBool   ('main', 'ShowAfterDone',  ShowAfterDone);

    WriteBool   ('main', 'Profile_IsShowFrenelZoneBottomLeftRight',  Profile_IsShowFrenelZoneBottomLeftRight);

    WriteBool('main', 'IsShowReflectionPoints', IsShowReflectionPoints);
*)

//    UpdateFile;
  end;

  oInifile.Free;


  IDList.SaveIDsToIniFile (aFileName, 'items');

end;



function TIni_Link_Report_Param.Validate: boolean;
begin
  Result := //(FileDir<>'') and
            (ProjectID>0) ;//or
            //(MapDesktopID>0) or
           // (Report_ID>0)
end;


end.


