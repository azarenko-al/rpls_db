unit x_Link_report;

interface

uses  Sysutils, Dialogs, IniFiles, Windows, Messages,

  u_dll_with_dmMain,
  u_ini_Link_report_params,

  I_Options,

  u_func,

  //u_ini_Link_report_params,

  dm_Report,
  dm_Link_report,
  dm_LinkLine_report,
  dm_Link_report_xml,
  dm_Link_report_data,
  dm_Link_map,

  u_vars,


  dm_Main,

  i_Link_report;


type
  TLink_report_X = class(TDllCustomX, ILink_report_X)
  public
    procedure Execute(aFileName: WideString); stdcall;

  end;

  function DllGetInterface(const IID: TGUID; var Obj): HResult; stdcall;

exports
  DllGetInterface;

  
implementation


const
  WM_REFRESH_DATA = WM_USER + 600;



// ---------------------------------------------------------------
procedure TLink_report_X.Execute(aFileName: WideString);
// ---------------------------------------------------------------
const
  TEMP_INI_FILE = 'link_report.ini';

var
  b: Boolean;
  oIniFile: TIniFile;
  sReportFileName : string;

begin
  Assert(Assigned(dmMain), 'Assigned(dmMain) not assigned');


  if not FileExists(aFileName ) then
  begin
    ShowMessage(aFileName);
    Exit;
  end;



  g_Link_Report_Param:=TIni_Link_Report_Param.Create;

  g_Link_Report_Param.LoadFromINI(aFileName);

 //zzzzzzzz LoadFromINI(sIniFileName, rec);

  TdmLink_report_data.Init;
//  TdmRel_Engine.Init;
  TdmLink_report.Init;
  TdmLinkLine_report.Init;
  TdmLink_map.Init;
  TdmLink_report_xml.Init;


(*  gParams.IsSaveDocsToDB  :=rec.IsSaveDocsToDB;
  gParams.IsShowReportDoc :=rec.IsShowReportDoc;

*)

  if (g_Link_Report_Param.FileDir='') or
     (g_Link_Report_Param.ProjectID=0) or
     (g_Link_Report_Param.ReportID=0)
  then begin
    ShowMessage('(FileDir='''') or (ProjectID=0) or (ReportID=0) ');
    Exit;
  end;

  // -------------------------------------------------------------------

 ////// ShowMessage(IntToStr(rec.ProjectID));


  dmMain.ProjectID:=g_Link_Report_Param.ProjectID;

  IOptionsX_Load;

 // IOptions:=Get_IOptionsX();


 /////////// dmLink_Map.Params.MapDesktopID:=g_ReportParams.MapDesktopID;

 // rec.Scale:=0;
(*

  if g_ReportParams.Scale=0 then
  begin
    sScale:='1 000 000';
    if InputQuery('����� ��������', '�������:', sScale) then
      g_ReportParams.Scale:= AsInteger( ReplaceStr (sScale, ' ','') )
    else
      Exit;

  end;*)


(*  dmLink_Map.Params.Scale       :=g_ReportParams.Scale;


  dmLink_Map.Params.ImageWidth  :=g_ReportParams.ImageWidth;
  dmLink_Map.Params.ImageHeight :=g_ReportParams.ImageHeight;
*)
 // dmLink_report.Params.IniFileName:=sIniFileName;
//  dmLink_report.Params.IsShowReflectionPoints:=rec.IsShowReflectionPoints;
 // dmLink_report.Params.Profile_IsShowFrenelZoneBottomLeftRight:=g_ReportParams.Profile_IsShowFrenelZoneBottomLeftRight;

  dmLinkLine_report.Params.IniFileName:=aFileName;



  dmLink_report.LinkIDList.LoadIDsFromIniFile (aFileName, 'links');
  dmLink_report.LinkLineIDList.LoadIDsFromIniFile (aFileName, 'LinkLines');

//   UpdateReports;


//  if iScale>0 then
 //   dmLink_map.Scale:=iScale;


  sReportFileName:=g_Link_Report_Param.FileDir + 'temp\'+
              g_Link_Report_Param.ReportFileName;

  dmReport.SaveToFile (g_Link_Report_Param.ReportID, sReportFileName);

  dmLink_report_data.FileDir:=g_Link_Report_Param.FileDir;

  // ---------------------------------------------------------------

  CursorHourGlass;


  //-------------------------------------------------------------------
  if Eq(g_Link_Report_Param.Action,'LinkList_report') then
  //-------------------------------------------------------------------
  begin
   // dmLink_report.Params.ObjName_:=g_ReportParams.ObjName;
    dmLink_report.ReportTemplateFileName:=sReportFileName;
 //   dmLink_report.ExecuteProc;
    dmLink_report.ExecuteDlg();

//    dmLink_report.Execute_LinkList (); //sReportFileName)

  end  else

  //-------------------------------------------------------------------
  if Eq(g_Link_Report_Param.Action,'LinkLine_report') then
  //-------------------------------------------------------------------
  begin
    dmLinkLine_report.ReportTemplateFileName:=sReportFileName;

    if g_Link_Report_Param.LinkLineID>0 then
      dmLinkLine_report.Execute_LinkLine (g_Link_Report_Param.LinkLineID);         //,  sReportFileName

    if dmLink_report.LinkLineIDList.Count>0 then
      dmLinkLine_report.Execute_LinkLine (dmLink_report.LinkLineIDList[0].ID);         //,  sReportFileName
  end;


  //-------------------------------------------------------------------
  if Eq(g_Link_Report_Param.Action,'LinkNet_report') then
  //-------------------------------------------------------------------
   // dmLink_report.Execute_LinkNet (sReportFileName)
  else
    ;


  CursorDefault;


  IOptionsX_UnLoad();

  FreeAndNil(g_Link_Report_Param);



 // Tfrm_Documents.ExecDlg;

 // dmLink_report.ExecuteTest (iID);


end;


// ---------------------------------------------------------------
function DllGetInterface(const IID: TGUID; var Obj): HResult;
// ---------------------------------------------------------------
begin
  Result:=S_FALSE; Integer(Obj):=0;

  if IsEqualGUID(IID, ILink_report_X) then
  begin
    with TLink_report_X.Create() do
      if GetInterface(IID,OBJ) then
        Result:=S_OK
  end else
    raise Exception.Create('if IsEqualGUID(IID, ILink_graphX)');


end;



begin
 // RegisterClass(TdmMain_bpl);

end.


