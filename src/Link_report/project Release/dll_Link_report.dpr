program dll_Link_report;



uses
  d_MIF_export in '..\..\..\..\common7\Package_Common\Map\d_MIF_export.pas' {dlg_MIF_export1},
  d_Report_Select_new in '..\src\d_Report_Select_new.pas' {dlg_Main_Report},
  d_Wizard in '..\..\..\..\common7\Package_Common\d_Wizard.pas' {dlg_Wizard},
  dm_App_link_report in '..\src\dm_App_link_report.pas' {dmMain_app__report: TDataModule},
  dm_Document in '..\..\Documents\dm_Document.pas' {dmDocument: TDataModule},
  dm_Link_map in '..\src\dm_Link_map.pas' {dmLink_map_: TDataModule},
  dm_Link_report in '..\src\dm_Link_report.pas' {dmLink_report: TDataModule},
  dm_Link_SDB_report in '..\src\dm_Link_SDB_report.pas' {dmLink_SDB_report: TDataModule},
  dm_LinkLine_report in '..\src\dm_LinkLine_report.pas' {dmLinkLine_report: TDataModule},
  dm_MDB in '..\src\dm_MDB.pas' {dmMDB: TDataModule},
  dm_Progress in '..\..\..\..\common7\Package_Common\dm_Progress.pas' {dmProgress: TDataModule},
  Forms,
  i_Options_ in '..\..\ActiveX\i_Options_.pas',
  i_WMS in '..\..\ActiveX\i_WMS.pas',
  MidasLib,
  Options_TLB in '..\..\ActiveX\Options_TLB.pas',
  u_debug in '..\..\..\..\common7\Package_Common\u_debug.pas',
  u_dll_geo_convert in '..\..\..\..\DLL\Geo_Convert\shared\u_dll_geo_convert.pas',
  u_dll_img in '..\..\..\..\common dll 7\dll_Img\u_dll_img.pas',
  u_ini_Link_report_params in '..\src_shared\u_ini_Link_report_params.pas',
  u_Link_freq_report_classes in '..\..\Link_Freq_report\src\u_Link_freq_report_classes.pas',
  u_Link_report_class in '..\src\u_Link_report_class.pas',
  u_LinkCalcResult_new in '..\..\Link_Calc\src\u_LinkCalcResult_new.pas',
  u_math_reflection in '..\..\Profile\u_math_reflection.pas',
  u_report_classes in '..\src\u_report_classes.pas',
  u_ini_Link_report_params_WMS in '..\..\link_report_WMS\src_shared\u_ini_Link_report_params_WMS.pas',
  dm_Settings in '..\..\Settings\dm_Settings.pas' {dmSettings: TDataModule};

{$R *.RES}


begin
  Application.Initialize;
  Application.CreateForm(TdmMain_app__report, dmMain_app__report);
  Application.Run;
end.
