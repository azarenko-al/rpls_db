object Form7: TForm7
  Left = 455
  Top = 110
  Width = 1142
  Height = 656
  Caption = 'Form7'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object dxMemData_Groups: TdxMemData
    Indexes = <>
    Persistent.Data = {
      5665728FC2F5285C8FFE3F030000000400000003000300696400640000000100
      080043415054494F4E0032000000010005004E414D45000101000000010E0000
      003131313131313131313131313131000102000000010E000000323232323232
      323232323232323200}
    SortOptions = []
    Left = 152
    Top = 264
    object dxMemData_Groupsid: TIntegerField
      FieldName = 'id'
    end
    object dxMemData_GroupsCAPTION: TStringField
      FieldName = 'CAPTION'
      Size = 100
    end
    object dxMemData_GroupsNAME: TStringField
      FieldName = 'NAME'
      Size = 50
    end
  end
  object dxMemData_Items: TdxMemData
    Indexes = <>
    Persistent.Data = {
      5665728FC2F5285C8FFE3F050000000400000003000300494400040000000300
      0A00504152454E545F494400640000000100080043415054494F4E0008000000
      0600060056414C554500080000000600070056414C5545320001010000000101
      0000000103000000E0E0E0010000000000003640010000000000003640}
    SortOptions = []
    Left = 264
    Top = 184
    object dxMemData_ItemsID: TIntegerField
      FieldName = 'ID'
    end
    object dxMemData_ItemsPARENT_ID: TIntegerField
      FieldName = 'PARENT_ID'
    end
    object dxMemData_ItemsCAPTION: TStringField
      FieldName = 'CAPTION'
      Size = 100
    end
    object dxMemData_ItemsVALUE: TFloatField
      FieldName = 'VALUE'
    end
    object dxMemData_ItemsVALUE2: TFloatField
      FieldName = 'VALUE2'
    end
  end
end
