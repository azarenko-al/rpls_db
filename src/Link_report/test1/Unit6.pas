unit Unit6;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs,

  u_const_db,

  dm_Main,
  u_db, DB, ADODB, StdCtrls, Grids, DBGrids

  ;

type
  TForm6 = class(TForm)
    qry_Property1: TADOQuery;
    ds_Link: TDataSource;
    ADOConnection1: TADOConnection;
    DBGrid1: TDBGrid;
    Button1: TButton;
    qry_Property1name: TStringField;
    qry_Property1project_id: TIntegerField;
    qry_Property1folder_id: TIntegerField;
    qry_Property1georegion_id: TIntegerField;
    qry_Property1address: TStringField;
    qry_Property1region: TStringField;
    qry_Property2: TADOQuery;
    StringField1: TStringField;
    IntegerField1: TIntegerField;
    IntegerField2: TIntegerField;
    IntegerField3: TIntegerField;
    StringField2: TStringField;
    StringField3: TStringField;
    DataSource1: TDataSource;
    DBGrid2: TDBGrid;
    procedure Button1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure qry_Property1CalcFields(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form6: TForm6;

implementation

{$R *.dfm}

procedure TForm6.Button1Click(Sender: TObject);

var
  k: Integer;
begin
  k:=qry_Property1.FieldList.Count;
//  qry_Property1.FieldList.Clear;

//  qry_Property1.Close;



//  db_OpenQuery (qry_Property1, 'SELECT top 1 * FROM '+ TBL_PROPERTY + ' where id=0');
  db_OpenQuery (qry_Property1, 'SELECT * FROM '+ TBL_PROPERTY + ' where id=0');
  db_OpenQuery (qry_Property2, 'SELECT * FROM '+ TBL_PROPERTY + ' where id=0');

  db_CreateFieldsForDataset1(qry_Property1);


//  qry_Property1.FieldList.Update;
//  qry_Property1.FieldDefList.Update;

  db_CreateCalculatedField(qry_Property1, 'test', ftInteger);

  db_CreateFields(qry_Property1, qry_Property2);

  db_OpenQuery (qry_Property1, 'SELECT top 1 * FROM '+ TBL_PROPERTY + ' ');
  db_OpenQuery (qry_Property2, 'SELECT top 1 * FROM '+ TBL_PROPERTY + ' ');



//  qry_Property1.Open;
//  qry_Property2.Open;


end;

procedure TForm6.FormCreate(Sender: TObject);
begin
//  TdmMain.Init;
//  if not dmMain.OpenDB_reg then    // True
//    Exit;
//


//  db_SetComponentADOConnection (Self, dmMain.ADOConnection);

end;

procedure TForm6.qry_Property1CalcFields(DataSet: TDataSet);
begin
  DataSet['test'] := 1;
end;

end.
