object Form6: TForm6
  Left = 578
  Top = 291
  Width = 573
  Height = 432
  Caption = 'Form6'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object DBGrid1: TDBGrid
    Left = 0
    Top = 215
    Width = 565
    Height = 88
    Align = alBottom
    DataSource = ds_Link
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
  end
  object Button1: TButton
    Left = 416
    Top = 32
    Width = 75
    Height = 25
    Caption = 'Button1'
    TabOrder = 1
    OnClick = Button1Click
  end
  object DBGrid2: TDBGrid
    Left = 0
    Top = 303
    Width = 565
    Height = 102
    Align = alBottom
    DataSource = DataSource1
    TabOrder = 2
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
  end
  object qry_Property1: TADOQuery
    Connection = ADOConnection1
    CursorType = ctStatic
    OnCalcFields = qry_Property1CalcFields
    Parameters = <>
    Prepared = True
    SQL.Strings = (
      'SELECT    top 1   *'
      'FROM      Property'
      '')
    Left = 200
    Top = 36
    object qry_Property1name: TStringField
      FieldName = 'name'
      Size = 100
    end
    object qry_Property1project_id: TIntegerField
      FieldName = 'project_id'
    end
    object qry_Property1folder_id: TIntegerField
      FieldName = 'folder_id'
    end
    object qry_Property1georegion_id: TIntegerField
      FieldName = 'georegion_id'
    end
    object qry_Property1address: TStringField
      FieldName = 'address'
      Size = 250
    end
    object qry_Property1region: TStringField
      FieldName = 'region'
      Size = 250
    end
  end
  object ds_Link: TDataSource
    DataSet = qry_Property1
    Left = 200
    Top = 96
  end
  object ADOConnection1: TADOConnection
    ConnectionString = 
      'Provider=MSDASQL.1;Persist Security Info=False;User ID=sa;Data S' +
      'ource=server_onega_link1'
    LoginPrompt = False
    Provider = 'MSDASQL.1'
    Left = 32
    Top = 16
  end
  object qry_Property2: TADOQuery
    Connection = ADOConnection1
    CursorType = ctStatic
    OnCalcFields = qry_Property1CalcFields
    Parameters = <>
    Prepared = True
    SQL.Strings = (
      'SELECT    top 1   *'
      'FROM      Property'
      '')
    Left = 288
    Top = 36
    object StringField1: TStringField
      FieldName = 'name'
      Size = 100
    end
    object IntegerField1: TIntegerField
      FieldName = 'project_id'
    end
    object IntegerField2: TIntegerField
      FieldName = 'folder_id'
    end
    object IntegerField3: TIntegerField
      FieldName = 'georegion_id'
    end
    object StringField2: TStringField
      FieldName = 'address'
      Size = 250
    end
    object StringField3: TStringField
      FieldName = 'region'
      Size = 250
    end
  end
  object DataSource1: TDataSource
    DataSet = qry_Property2
    Left = 288
    Top = 96
  end
end
