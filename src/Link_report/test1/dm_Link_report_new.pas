unit dm_Link_report_new;

interface

// {$DEFINE gost}
//   {$IFDEF new}

uses
  SysUtils, Classes, Controls, Forms, Dialogs, Db, ADODB, Math, Variants,


  u_LinkCalcResult_new,

  //dm_FRep_short,

  dm_Onega_DB_data,

  //u_Link_report_vars,

  fr_Profile_Rel_Graph,

  dm_Main,

  dm_Rel_Engine,
  dm_Link,
  dm_Link_map,

  dm_Link_report_data,

  u_const_db,
  u_Link_const,

  dm_Progress,

  u_classes,

  u_Func_arrays,
  u_func,
  u_db,
  u_files,
  u_img,

  u_Geo,
  u_Geo_convert_new,

  u_rel_Profile,

  u_Link_report_class,

  dxmdaset, frxClass, frxExportRTF, frxExportPDF,
  frxExportXLS, frxDBSet, frxDesgn;

type
  TdmLink_report_new = class(TdmProgress)
    qry_LinkEnd1: TADOQuery;
    qry_Antennas1: TADOQuery;
    qry_LinkEnd2: TADOQuery;
    qry_Antennas2: TADOQuery;
    qry_LinkCalcResults_Item111: TADOQuery;
    qry_Property1: TADOQuery;
    qry_Property2: TADOQuery;
    ADOStoredProc_Link: TADOStoredProc;
    q_LInk_repeater: TADOQuery;
    q_LInk_repeater_ant: TADOQuery;
    dxMemData_Groups: TdxMemData;
    dxMemData_Items: TdxMemData;
    frXLS: TfrxXLSExport;
    frPDF: TfrxPDFExport;
    frRTF: TfrxRTFExport;
    frReport: TfrxReport;
    frxDBDataset_link: TfrxDBDataset;
    frxDBDataset_Property1: TfrxDBDataset;
    frxDBDataset_Property2: TfrxDBDataset;
    dxMemData_Link_img: TdxMemData;
    frxDBDataset_Link_img: TfrxDBDataset;
    dxMemData_Link_imglink_id: TIntegerField;
    dxMemData_Link_imgimg1: TBlobField;
    dxMemData_Link_imgimg2: TGraphicField;
    dxMemData_Link_imgimg3: TGraphicField;
    ADOConnection1: TADOConnection;
    ds_Link: TDataSource;
    frxDBDataset_LinkEnd1: TfrxDBDataset;
    frxDBDataset_LinkEnd2: TfrxDBDataset;
    frxDBDataset_group: TfrxDBDataset;
    frxDBDataset_Items: TfrxDBDataset;
    ds_Property2: TDataSource;
    ds_Property1: TDataSource;
    ds_LinkEnd1: TDataSource;
    ds_LinkEnd2: TDataSource;
    frxReport1: TfrxReport;
    dxMemData_Groupsid: TIntegerField;
    dxMemData_GroupsCAPTION: TStringField;
    dxMemData_GroupsNAME: TStringField;
    dxMemData_ItemsID: TIntegerField;
    dxMemData_ItemsPARENT_ID: TIntegerField;
    dxMemData_ItemsCAPTION: TStringField;
    dxMemData_ItemsVALUE: TFloatField;
    dxMemData_ItemsVALUE2: TFloatField;

    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  private
    FLinkCalcResult: TLinkCalcResult;

    FLink_ID: Integer;

    FVector: TBLVector;
    FClutterModelID: integer;

    function GetAntennaHeightArr (aQryAntenna: TADOQuery): u_Func.TDoubleArray ;

    procedure AddRRV;
    procedure AddCalcResults (aID: integer);

    procedure AddSiteInfo ( aSiteNumber: integer);
    procedure AddLinkInfo ;
    function Execute_LinkNet(aXmlFileName: string): boolean;


    procedure MakeProfileImage (aFileName: string);


    function GetCalcResult_Refraction(aParam: Integer; var aValue: Double): boolean;
    procedure InitFields;
    procedure MakeProfileImage_Repeater(aFileName: string);

    procedure Profile_Open;
    procedure Profile_Open_Repeater;

  private

    RelProfile1: TrelProfile;
    RelProfile2: TrelProfile;


  public
    FRefractionArr: array[1..3] of Double;// = (22,158,169);

//  const
//  PARAM_IDs: array[1..3] of integer = (22,158,169);


    FLinkData: TLinkData;
    RelProfile: TrelProfile;

//
//    Params: record
////      IniFileName: string;
////      ObjName_: string;
//
//   //   IsShowReflectionPoints: boolean;
//
//    //  Profile_IsShowFrenelZoneBottomLeftRight : boolean;
//    end;


    ReportTemplateFileName: string;
    DisableClearFileListForDocument: Boolean;

    LinkLineIDList: TIDList;
    LinkIDList:     TIDList;

    procedure ExecuteProc; override;
    function Link_ExecuteItem(aID: integer): Boolean;

//    function Execute_link (aID: integer; aReportTemplateFileName: string): boolean;

    function Execute_LinkList: Boolean;
    function OpenData(aID: integer): Boolean;

    class procedure Init;

  end;


//function dmLink_report: TdmLink_report;
var
  dmLink_report_new: TdmLink_report_new;

//==================================================================
// implementation
//==================================================================
implementation {$R *.dfm}

uses
  dm_Link_report_XML,

  u_ini_Link_report_params,

  u_link_calc_classes_main;




class procedure TdmLink_report_new.Init;
begin
  if not Assigned(dmLink_report_new) then
    dmLink_report_new := TdmLink_report_new.Create(Application);
end;


//--------------------------------------------------------------------
procedure TdmLink_report_new.InitFields;
//--------------------------------------------------------------------
var
  k: Integer;
begin
//  k:=qry_Property1.FieldList.Count;
//  qry_Property1.FieldList.Clear;

//  qry_Property1.Close;

  //////////////////////////////
  Exit;
  //////////////////////////////

  {

//  db_OpenQuery (qry_Property1, 'SELECT top 1 * FROM '+ TBL_PROPERTY + ' where id=0');
  db_OpenQuery (qry_Property1, 'SELECT * FROM '+ TBL_PROPERTY + ' where id=0');
  db_OpenQuery (qry_Property2, 'SELECT * FROM '+ TBL_PROPERTY + ' where id=0');

  db_CreateFieldsForDataset1(qry_Property1);

  db_CreateCalculatedField(qry_Property1, FLD_lat_WGS,  ftFloat);
  db_CreateCalculatedField(qry_Property1, FLD_lon_WGS,  ftFloat);
  db_CreateCalculatedField(qry_Property1, FLD_lat_CK95, ftFloat);
  db_CreateCalculatedField(qry_Property1, FLD_lon_CK95, ftFloat);

  db_CreateFields(qry_Property1, qry_Property2);
  }


//
//  db_OpenQuery (qry_Property1, 'SELECT top 1 * FROM '+ TBL_PROPERTY + ' ');
//  db_OpenQuery (qry_Property2, 'SELECT top 1 * FROM '+ TBL_PROPERTY + ' ');
//


//  qry_Property1.Open;
//  qry_Property2.Open;


end;


//
//var
//  k: Integer;
//begin
////  k:=qry_Property1.FieldList.Count;
////  qry_Property1.FieldList.Clear;
////
////  db_OpenQuery (qry_Property1, 'SELECT * FROM '+ TBL_PROPERTY + ' WHERE id=0');
////
////  qry_Property1.FieldDefList.Update;
//
//
//
//// SQL_SELECT_PMP_TERMINAL_ANT,
//  //                  [db_Par(FLD_PMP_TERMINAL_ID, iPmpTerminalID )]);
//
//
//end;


//--------------------------------------------------------------------
procedure TdmLink_report_new.DataModuleCreate(Sender: TObject);
//--------------------------------------------------------------------
begin
  inherited;

  RelProfile:=TrelProfile.Create;

  RelProfile1:=TrelProfile.Create;
  RelProfile2:=TrelProfile.Create;

  LinkLineIDList:=TIDList.Create;
  LinkIDList:=TIDList.Create;

  db_SetComponentADOConnection (Self, dmMain.ADOConnection);

  FLinkData := TLinkData.Create();

  FLinkCalcResult:=TLinkCalcResult.Create();

  FLinkCalcResult:=TLinkCalcResult.Create;


  {
  db_CreateField(dxMemData_Link_img,
      [
    //   db_Field(FLD_DXTREE_ID,        ftInteger),
    //   db_Field(FLD_DXTREE_PARENT_ID, ftInteger),
       db_Field(FLD_LINK_ID,          ftInteger),
       db_Field(FLD_PARENT_ID,        ftBlob),
      ]);
  }


end;

//--------------------------------------------------------------------
procedure TdmLink_report_new.DataModuleDestroy(Sender: TObject);
//--------------------------------------------------------------------
begin
  FreeAndNil(LinkIDList);
  FreeAndNil(LinkLineIDList);

  FreeAndNil(RelProfile);

  FreeAndNil(RelProfile1);
  FreeAndNil(RelProfile2);

  FreeAndNil(FLinkData);

  FreeAndNil(FLinkCalcResult);

  FreeAndNil(FLinkCalcResult);

  inherited;
end;


//--------------------------------------------------------------------
function TdmLink_report_new.OpenData(aID: integer): Boolean;
//--------------------------------------------------------------------
const

  // -------------------------
  // ANTENNAS
  // -------------------------
  SQL_SELECT_LINKEND_ANT =
     'SELECT *  FROM '+ view_LINKEND_ANTENNAS +
//     'SELECT *  FROM '+ TBL_LINKEND_ANTENNA +
     ' WHERE linkend_id=:linkend_id ORDER BY height desc';

  SQL_SELECT_PMP_SECTOR_ANT =
     'SELECT *  FROM '+view_PMP_SECTOR_ANTENNAS +
//     'SELECT *  FROM '+ TBL_LINKEND_ANTENNA +
     ' WHERE PMP_SECTOR_id=:PMP_SECTOR_id ORDER BY height desc';

  SQL_SELECT_PMP_TERMINAL_ANT =
//     'SELECT *  FROM '+ TBL_LINKEND_ANTENNA +
     'SELECT * FROM '+ view_PMP_TERMINAL_ANTENNAS +
     ' WHERE PMP_TERMINAL_id=:PMP_TERMINAL_id ORDER BY height desc';



  SQL_SELECT_LINKEND=
     'SELECT * FROM '+ VIEW_LINKEND +' WHERE id=:id ';


  SQL_SELECT_PMP_SECTOR=
//     'SELECT  * FROM ' + TBL_PMP_SECTOR + ' WHERE id=:id ';
//     'SELECT  * FROM ' + view_PMP_SECTORS + ' WHERE id=:id ';
     'SELECT  * FROM ' + view_PMP_SECTOR + ' WHERE id=:id ';

  SQL_SELECT_PMP_TERMINAL=
//     'SELECT *  FROM ' + TBL_PmpTerminal + ' WHERE id=:id ';
     'SELECT *  FROM ' + view_Pmp_Terminal_full + ' WHERE id=:id ';


  SQL_SELECT_reflection_POINTS =
    'SELECT * FROM '+ TBL_Link_reflection_points + ' WHERE (link_id=:link_id)';

//  TBL_LINK = 'LINK';


var

  iPmpSectorID,iPmpTerminalID,
  iLinkEndID1,iLinkEndID2: integer;

  iProperty1: Integer;
  iProperty2: Integer;
  i: integer;
  iRes: Integer;
  j: integer;
  oLinkCalcResult: TLinkCalcResult;
  sCalcResultsXML: string;

begin

  FLink_ID:=aID;

  Assert(FLink_ID>0, 'Value <=0');


  iRes:=dmOnega_DB_data.Link_Select(ADOStoredProc_Link, aID);

 // db_OpenQuery (qry_Link, Format('SELECT type FROM '+ TBL_LINK + ' WHERE id=%d', [aID]));
  i :=ADOStoredProc_Link.RecordCount;

  FLinkData.LoadFromDataset(ADOStoredProc_Link);


//  if not dmLink.GetInfoFromDataset (qry_Link, FLinkParamRec) then
  if (ADOStoredProc_Link.RecordCount=0) then
  //or
    // (not dmLink.GetInfoFromDataset (ADOStoredProc_Link, FLinkParamRec)) then
  begin
    ShowMessage('Link not found!');
    Exit;
  end;

  FVector        :=FLinkData.BLVector;

  FClutterModelID:=FLinkData.ClutterModel_ID;

//
//  db_OpenQuery(qry_Refl, 'SELECT * FROM ' + ,
//      [db_Par(FLD_LINK_ID, aID)]);
//
//
//  PARAM_IDs: array[1..3] of integer = (22,158,169);
//


(*
  db_OpenQuery(qry_Refl, SQL_SELECT_reflection_POINTS,
      [db_Par(FLD_LINK_ID, aID)]);

  FLinkData.LoadFromDataset_ReflectionPoints(qry_Refl);
*)



  case FLinkData.LinkType of    //
    // -------------------------------------------------------------------
    ltLink:  begin
    // -------------------------------------------------------------------

       iLinkEndID1:=FLinkData.LinkEndID1;//  qry_Link.FieldByName(FLD_LINKEND1_ID).AsInteger;
       iLinkEndID2:=FLinkData.LinkEndID2;//Left.LinkEndID.FieldByName(FLD_LINKEND2_ID).AsInteger;

       iProperty1:=FLinkData.PropertyID1; //   ADOStoredProc_Link.FieldByName(FLD_PROPERTY1_ID).AsInteger;
       iProperty2:=FLinkData.PropertyID2; //ADOStoredProc_Link.FieldByName(FLD_PROPERTY2_ID).AsInteger;


       Assert(iLinkEndID1>0, 'Value =0');
       Assert(iLinkEndID2>0, 'Value =0');


       db_OpenQuery (qry_LinkEnd1, SQL_SELECT_LINKEND, [db_Par(FLD_ID, iLinkEndID1)]);
       db_OpenQuery (qry_LinkEnd2, SQL_SELECT_LINKEND, [db_Par(FLD_ID, iLinkEndID2)]);

       if qry_LinkEnd1.IsEmpty or (qry_LinkEnd2.IsEmpty) then
         raise Exception.Create ('qry_LinkEnd1.IsEmpty or qry_LinkEnd2.IsEmpty');


       db_OpenQuery (qry_Antennas1, SQL_SELECT_LINKEND_ANT,
                    [db_Par(FLD_LINKEND_ID, iLinkEndID1)]);

       db_OpenQuery (qry_Antennas2, SQL_SELECT_LINKEND_ANT,
                    [db_Par(FLD_LINKEND_ID, iLinkEndID2)]);


     end;

    // -------------------------------------------------------------------
    ltPmpLink: begin
    // -------------------------------------------------------------------
       iProperty1:=ADOStoredProc_Link.FieldByName(FLD_PROPERTY1_ID).AsInteger;
       iProperty2:=ADOStoredProc_Link.FieldByName(FLD_PROPERTY2_ID).AsInteger;


       iPmpSectorID  :=ADOStoredProc_Link.FieldByName(FLD_PMP_SECTOR_ID).AsInteger;
       iPmpTerminalID:=ADOStoredProc_Link.FieldByName(FLD_PMP_TERMINAL_ID).AsInteger;

       Assert(iPmpSectorID>0, 'Value =0');
       Assert(iPmpTerminalID>0, 'Value =0');


       db_OpenQuery (qry_LinkEnd1, SQL_SELECT_PMP_SECTOR,   [db_Par(FLD_ID, iPmpSectorID)]);
//                db_OpenQueryByID (qry_LinkEnd2, TBL_PMP_TERMI, [db_Par(FLD_ID, rLinkParamRec.LinkEndID1)]);
       db_OpenQuery (qry_LinkEnd2, SQL_SELECT_PMP_TERMINAL, [db_Par(FLD_ID, iPmpTerminalID)]);

       Assert((not qry_LinkEnd1.IsEmpty) and (not qry_LinkEnd2.IsEmpty));

//                 db_OpenQueryByID (qry_LinkEnd1, TBL_PMP_SECTOR,   rLinkParamRec.PmpSectorID);
//                db_OpenQueryByID (qry_LinkEnd2, TBL_PMP_TERMINAL, rLinkParamRec.PmpTerminalID);

       db_OpenQuery (qry_Antennas1, SQL_SELECT_PMP_SECTOR_ANT,
                    [db_Par(FLD_PMP_SECTOR_ID, iPMPSECTORID)]);

       db_OpenQuery (qry_Antennas2, SQL_SELECT_PMP_TERMINAL_ANT,
                    [db_Par(FLD_PMP_TERMINAL_ID, iPmpTerminalID )]);


    end;
  end;



  Assert(iProperty1>0, 'Value =0');
  Assert(iProperty2>0, 'Value =0');


(*
  db_OpenQuery (qry_LinkCalcResults,
               'SELECT * FROM '+ TBL_LinkCalcResults +' WHERE (link_id=:link_id)',
                [db_Par(FLD_LINK_ID, aID) ]);


  db_OpenQuery (qry_Refractions,
     'SELECT * FROM '+ TBL_LinkCalcResults +' WHERE (link_id=:link_id) and (param in (22,158,169))',
      [db_Par(FLD_LINK_ID, aID) ]);
*)


  sCalcResultsXML:=ADOStoredProc_Link.FieldByName(FLD_CALC_RESULTS_XML).AsString;



  oLinkCalcResult := TLinkCalcResult.Create();

  oLinkCalcResult.LoadFromXmlString(sCalcResultsXML);
  oLinkCalcResult.SaveToDataset_2_Datasets(dxMemData_Groups, dxMemData_Items);

  FreeAndNil(oLinkCalcResult);


                //
//  PARAM_IDs: array[1..3] of integer = (22,158,169);
//


  //db_View (qry_Refractions);




  //������� ������ �� ���������
  db_OpenTableByID (qry_property1, TBL_property, iProperty1);
  db_OpenTableByID (qry_property2, TBL_property, iProperty2);



  if FLinkData.LinkRepeater.ID > 0 then
  begin

    dmOnega_DB_data.OpenQuery(q_LInk_repeater,
      Format('SELECT * FROM %s WHERE id=%d',
           [view_LINK_REPEATER, FLinkData.LinkRepeater.ID ]));

    Assert(q_LInk_repeater.RecordCount>0);

    FLinkData.LinkRepeater_LoadFromDataset(q_LInk_repeater);


    
//    Link.LinkRepeater_Restore;

   //  db_View(q_LInk_repeater);
(*
    dmOnega_DB_data.OpenQuery(q_LInk_repeater_ant,
      Format('SELECT * FROM %s WHERE Link_repeater_id=%d',
           [VIEW_LINK_REPEATER_ANTENNA, Link.LinkRepeater.ID]));


    Link.LinkRepeater.Antennas.LoadFromDataset(q_LInk_repeater_ant);
*)
  end;


(*
  FSite1_Rel_H:=qry_property1.FieldByName(FLD_ground_height).AsFloat;
  FSite2_Rel_H:=qry_property2.FieldByName(FLD_ground_height).AsFloat;
*)

  //u_LinkCalcResult_new
  FLinkCalcResult.LoadFromXmlString(FLinkData.CalcResultsXML);


  FLinkCalcResult.SaveToDataset_2_Datasets(dxMemData_Groups, dxMemData_Items);

  FRefractionArr[0]:=FLinkCalcResult.Groups.GetCalcParamValueByID_new('3',22);
  FRefractionArr[0]:=FLinkCalcResult.Groups.GetCalcParamValueByID_new('6',158);
  FRefractionArr[0]:=FLinkCalcResult.Groups.GetCalcParamValueByID_new('18',169);

(*
   <GROUP name="3" caption=" 3.���������� ������� �������� ��� a% ���������">
      <ITEM id="22" value="1.07685" caption=" ����������� ��������� (��� a% ���������)"/>

   <GROUP name="6" caption=" 6.���������� ������� ���������� � ���.������������ V���">
      <ITEM id="158" value="0.57333" caption=" ����������� ��������� � ���.������������"/>

    <GROUP name="18" caption=" 18.���.������� ���������������� ������������ ��������������">
      <ITEM id="169" value="0.2909" caption=" ����������� ��������� (��� ����.�����.)"/>


*)

(*
  const
  PARAM_IDs: array[1..3] of integer = (22,158,169);
  // 22  - 3. ���������� ������� �������� ��� a% ��������� - ����������� ���������
  // 158 - 6. ���������� ������� ���������� � ���.������������ V���
  // 169 - 18. ���.������� ���������������� ������������ ��������������

*)

(*
  db_View(dxMemData_Groups);
  db_View(dxMemData_Items);
*)

  Result:=True;
end;


//--------------------------------------------------------------------
procedure TdmLink_report_new.AddRRV;
//--------------------------------------------------------------------
var
  d: Double;
  sTerrainType: string;
  sPolarization: string;

  //, sModulation

  //vNode: IXMLNode;
begin
  case ADOStoredProc_Link.FieldByName(FLD_terrain_type).AsInteger of
    0: sTerrainType:= '������������������ ���������';
    1: sTerrainType:= '������ ���������';
    2: sTerrainType:= '������ ����������� ��� ���������� ���������';
  else
    sTerrainType:= '';
  end;

///////  vNode := xml_AddNode(aNode, 'RRV', [xml_Att(FLD_CAPTION, '������ �� �������� ��������������� ���������')]);

  with ADOStoredProc_Link, dmLink_report_data do
  begin
    mem_Link[FLD_FADE_MARGIN_DB] :=FieldValues[FLD_FADE_MARGIN_DB];//,   'RRV_1',  '1. ������� �������� ��������� ����.�������������, 10^-8 [1/�]');


    d:=FieldByName(FLD_FADE_MARGIN_DB).AsFloat;
    d:= RoundFloat(d,2);
    mem_Link[FLD_FADE_MARGIN_DB_ROUND] := d; //,   'RRV_1',  '1. ������� �������� ��������� ����.�������������, 10^-8 [1/�]');


    mem_Link[FLD_gradient_diel] :=FieldValues[FLD_gradient_diel];//,   'RRV_1',  '1. ������� �������� ��������� ����.�������������, 10^-8 [1/�]');

    {$IFDEF new}
    mem_Link[FLD_gradient_deviation]      :=FieldValues[FLD_gradient_deviation];//,        'RRV_2',  '2. ����������� ���������� ���������, 10^-8 [1/�]');
    {$ENDIF}

    mem_Link[FLD_TerrainType ]  :=sTerrainType;//,                     'RRV_3',  '3. ��� ������������ �����������');
    mem_Link[FLD_steam_wet]     :=FieldValues[FLD_steam_wet];//,       'RRV_4',  '4. ���������� ��������� �������� ���� [�/���.�]');
    mem_Link[FLD_Q_factor]      :=FieldValues[FLD_Q_factor];//,        'RRV_5',  '5. Q-������ ������ �����������');
    mem_Link[FLD_climate_factor]:=FieldValues[FLD_climate_factor];//,  'RRV_6',  '6. ������������� ������ K��, 10^-6');
    mem_Link[FLD_factor_B]      :=FieldValues[FLD_factor_B];//,        'RRV_7',  '7. �������� ������������ b ��� ������� ������');
    mem_Link[FLD_factor_C]      :=FieldValues[FLD_factor_C];//,        'RRV_8',  '8. �������� ������������ c ��� ������� ������');
    mem_Link[FLD_factor_D]      :=FieldValues[FLD_factor_D];//,        'RRV_9',  '9. �������� ������������ d ��� ������� ������');
    mem_Link[FLD_rain_intensity]  :=FieldValues[FLD_rain_intensity];//,       'RRV_10', '10. ������������� ����� � ������� 0.01% ������� [��/���]');
    mem_Link[FLD_refraction]    :=1.335;// {FieldValues[FLD_refraction_gradient_0]}, 'RRV_11', '11. ����������� ��������� (��� ��.���������=0, ����� �.�.=0)');
 // end;

 //AddDLT
// with ADOStoredProc_Link,dmLink_report_data do
//  begin
    mem_Link[FLD_profile_step]             :=FieldValues[FLD_profile_step];//,             'DLT_1', '1. ��� ��������� ������� [��] (0 - ��������� ����)');
    mem_Link[FLD_precision_V_diffraction]  :=1;// {FieldValues[FLD_precision_V_diffraction]},  'DLT_2', '2. �������� ������ ������� V����(g)=V���.� [��]');
    mem_Link[FLD_precision_g_V_diffraction]:=1 ;//{FieldValues[FLD_precision_g_V_diffraction]},'DLT_3', '3. �������� ������ ������� g(V����)=g(V���.�), 10^-8');
    mem_Link[FLD_max_gradient_for_subrefraction] :=80;// {FieldValues[FLD_max_gradient_for_subrefr]}, 'DLT_4', '4. ������������ �������� ��� ������� ������������, 10^-8');
 // end;

  //AddTRB
  //with qry_Link,dmLink_report_data do
 // begin
    mem_Link[FLD_GST_SESR]  :=FieldValues[FLD_GST_SESR];//.., '', '1. ����������� �������� SESR ��� ���� [%]');
    mem_Link[FLD_GST_Kng]   :=FieldValues[FLD_GST_Kng];//,  '', '2. ����������� �������� ���  ��� ���� [%]');
    mem_Link[FLD_GST_length]:=FieldValues[FLD_GST_length];//,'', '3. ����� ���� [��]');
    mem_Link[FLD_KNG_dop]   :=FieldValues[FLD_KNG_dop];//,   '', '4. ���������� ����� ���, ������������� �������������');
  end;
                     

  sPolarization:=qry_Antennas1.FieldByName(FLD_POLARIZATION_str).Asstring;

//  sPolarization:=dmAntenna.GetPolarizationStr (qry_Antennas1.FieldByName(FLD_POLARIZATION).AsInteger);

 // sModulation:= qry_LinkEnd1.FieldByName(FLD_Modulation_Type).AsString;

 // vNode := xml_AddNode(aNode, 'TTX', [xml_Att(FLD_CAPTION, 'TTX')]);

  with qry_LinkEnd1, dmLink_report_data do
  begin
    mem_Link[FLD_PolarizationType] :=sPolarization;//,                     'PolarizationType',    '1. ��� �����������');
    mem_Link[FLD_TX_FREQ_MHz]      :=FieldValues[FLD_TX_FREQ_MHz];//,          'FreqWork',            '2. P������ ������� [���]');
    mem_Link[FLD_BitRate_Mbps]     :=FieldValues[FLD_BitRate_Mbps];//,            FLD_Speed,             '3. �������� �������� [��/�]');

    mem_Link[FLD_Signature_Width]  :=FieldValues[FLD_Signature_Width];//,  FLD_Signature_Width,   '4. ������ ��������� [���]');
    mem_Link[FLD_Signature_Height] :=FieldValues[FLD_Signature_Height];//, FLD_Signature_Height,  '5. ������ ��������� [��]');
    mem_Link[FLD_Modulation_Type]  :=FieldByName(FLD_Modulation_Type).AsString;//sModulation;//,                       FLD_Modulation_Type_STR,   '6. ��� ������� ���������');

   // mem_Link[FLD_EQUALISER_PROFIT ]:=FieldValues[FLD_EQUALISER_PROFIT ];//, FLD_EQUALISER_PROFIT,  '7. �������, �������� ������������ [��]');

    mem_Link[FLD_KRATNOST_BY_FREQ ]:=FieldValues[FLD_KRATNOST_BY_FREQ ];//, FLD_KRATNOST_BY_FREQ,    '8. ��������� ���������� �� ������� [1...8]');
    mem_Link[FLD_KRATNOST_BY_SPACE]:=FieldValues[FLD_KRATNOST_BY_SPACE];//,FLD_KRATNOST_BY_SPACE,   '9. ��������� ���������� �� ������������ [1...2]');
    mem_Link[FLD_Freq_Spacing]     :=FieldValues[FLD_Freq_Spacing];//,     FLD_Freq_Spacing,      '10. ��������� ������ [���]');

  end;

end;


//--------------------------------------------------------------------
function TdmLink_report_new.GetAntennaHeightArr (aQryAntenna: TADOQuery): u_func.TDoubleArray ;
//--------------------------------------------------------------------
var
  i: integer;
begin
  Setlength(Result, aQryAntenna.RecordCount);

  with aQryAntenna do
  begin
    First; i:=0;

    while not Eof do
    begin
      Result[i]:=FieldByName(FLD_HEIGHT).AsFloat;
      Next;
      Inc(i);
    end;
  end;

  DoubleArraySort (Result);
end;


{
//--------------------------------------------------------------------
function TdmLink_report_new.GetCalcResult_Refraction(aParam: Integer; var aValue:
    Double): boolean;
//--------------------------------------------------------------------
begin
 // db_View (qry_LinkCalcResults);
    //            qry_Refractions
  Result := qry_Refractions.Locate(FLD_PARAM, aParam, []);

  if Result then
     aValue:=qry_Refractions.FieldByName(FLD_VALUE_).AsFloat
  else
     aValue:=0;

//
//  if qry_LinkCalcResults.Locate(FLD_PARAM, aParam, []) then
//    Result:= qry_LinkCalcResults.FieldByName(FLD_VALUE_).AsFloat
//  else
//    Result:=0;

end;

}

//--------------------------------------------------------------------
procedure TdmLink_report_new.MakeProfileImage (aFileName: string);
//--------------------------------------------------------------------
const
  PARAM_IDs: array[1..3] of integer = (22,158,169);
  // 22  - 3. ���������� ������� �������� ��� a% ��������� - ����������� ���������
  // 158 - 6. ���������� ������� ���������� � ���.������������ V���
  // 169 - 18. ���.������� ���������������� ������������ ��������������

var
  oForm: Tframe_Profile_Rel_Graph;
  sDir: string;
  i: Integer;
  dRefraction: double;
//  dRefraction, dOldRefraction: double;
  b: Boolean;
  d: Double;
//  iDirection: integer;
//  is_profile_reversed: Boolean;
begin
 Assert(FLink_ID>0);


  oForm:=Tframe_Profile_Rel_Graph.Create(nil);


 // oForm.Params. IsShowSites:=True;

 oForm.Params.IsShowReflectionPoints  := g_Link_Report_Param.IsShowReflectionPoints;

  oForm.Params.IsShowFrenelZoneTop:=True;
 // oForm.Params.IsShowFrenelZoneBottom:=True;

 // b:=Params.Profile_IsShowFrenelZoneBottomLeftRight;
  b :=True;
  oForm.Params.IsShowFrenelZoneBottomLeft:=b;
  oForm.Params.IsShowFrenelZoneBottomRight:=b;


 // oForm.Params.IsShowReflectionPoints:=True;

  oForm.Width :=550;
  oForm.Height:=400;

 // oForm.SetDefaultSize;
  oForm.Height:= Trunc(oForm.Height*1.2);
  oForm.Width := Trunc(oForm.Width *1.2);

///////  dOldRefraction:= RelProfile.Refraction;
  oForm.RelProfile_Ref:=RelProfile;

  d:=FLinkData.Length_KM;

(*

  // -------------------------
  if not VarIsNull(oDBLink.Link_repeater.Property_Ground_Height) then
    eRel1 :=oDBLink.Link_repeater.Property_Ground_Height
  else
    eRel1 :=FrelProfile2.FirstItem().Rel_H;

  // -------------------------
  if not VarIsNull(oDBLink.Property2_Ground_Height) then
    eRel2 :=oDBLink.Property2_Ground_Height
  else
    eRel2 :=FrelProfile2.LastItem().Rel_H;

  // -------------------------
  Fframe_Profile_Rel.Params.SiteGroups[1].Site1.Rel_H:=eRel1;
  Fframe_Profile_Rel.Params.SiteGroups[1].Site2.Rel_H:=eRel2;


*)

  is_profile_reversed:=ADOStoredProc_Link.FieldByName(FLD_is_profile_reversed).AsBoolean;

  // -------------------------
//  if FLinkData.LinkRepeater.ID =0 then
  // -------------------------
 // begin
    oForm.Params.SetSiteGroupCount(1);

    oForm.Params.SiteGroups[0].Distance_KM:=Flinkdata.Length_KM;
    //RelProfile.GeoDistance_KM();

  //  iDirection:=ADOStoredProc_Link.FieldByName(FLD_CALC_DIRECTION).AsInteger;

    oForm.Params.SiteGroups[0].Site1.AntennaHeights:=GetAntennaHeightArr(qry_Antennas1);
    oForm.Params.SiteGroups[0].Site2.AntennaHeights:=GetAntennaHeightArr(qry_Antennas2);


    oForm.Params.SiteGroups[0].Site1.Rel_H:=RelProfile.FirstItem.Rel_H;
    oForm.Params.SiteGroups[0].Site2.Rel_H:=RelProfile.LastItem.Rel_H;

//
    oForm.Params.SiteGroups[0].ReflectionPointArr:=dmLink.GetReflectionPointArray(FLink_ID);

//  end;


  oForm.Params.Freq_MHz:= FLinkData.TX_FREQ_MHz;
  //qry_LinkEnd1.FieldByName(FLD_TX_FREQ_MHz).AsFloat;

///    (qry_LinkEnd1.FieldByName(FLD_TX_FREQ).AsFloat +
  ///   qry_LinkEnd1.FieldByName(FLD_RX_FREQ).AsFloat) / 2;


  oForm.ShowGraph;
  oForm.SaveToBmpFile (aFileName);

(*const
  PARAM_IDs: array[1..3] of integer = (22,158,169);
*)

  for i:=1 to High(PARAM_IDs) do
  begin
    b:=GetCalcResult_Refraction(PARAM_IDs[i], dRefraction);

//    dRefraction:= GetCalcResult(PARAM_IDs[i], dRefraction);

    RelProfile.SetRefraction(dRefraction);
//    RelProfile.SetDirection(iDirection);
    RelProfile.IsDirectionReversed := is_profile_reversed;


    oForm.Params.IsShowReflectionPoints:= false;
    oForm.RelProfile_Ref:=RelProfile;

    oForm.ShowGraph;
    sDir:= ChangeFileExt(aFileName,Format('%d.bmp', [i]));
    oForm.SaveToBmpFile (sDir);
  end;

//////////////  RelProfile.SetRefraction(dOldRefraction);

  FreeAndNil(oForm);

//  oForm.Free;
end;


//--------------------------------------------------------------------
procedure TdmLink_report_new.MakeProfileImage_Repeater(aFileName: string);
//--------------------------------------------------------------------
const
  PARAM_IDs: array[1..3] of integer = (22,158,169);
  // 22  - 3. ���������� ������� �������� ��� a% ��������� - ����������� ���������
  // 158 - 6. ���������� ������� ���������� � ���.������������ V���
  // 169 - 18. ���.������� ���������������� ������������ ��������������

var
  oForm: Tframe_Profile_Rel_Graph;
  sDir: string;
  i: Integer;
//  dRefraction, dOldRefraction: double;
  dRefraction: double;
  b: Boolean;
  d: Double;
//  iDirection: integer;
  is_profile_reversed: Boolean;
begin
 Assert(FLink_ID>0);

  Assert(FLinkData.Refraction>0, 'FLinkData.Refraction>0');


  oForm:=Tframe_Profile_Rel_Graph.Create(nil);


 // oForm.Params. IsShowSites:=True;

 oForm.Params.IsShowReflectionPoints  := g_Link_Report_Param.IsShowReflectionPoints;

  oForm.Params.IsShowFrenelZoneTop:=True;
 // oForm.Params.IsShowFrenelZoneBottom:=True;

 // b:=Params.Profile_IsShowFrenelZoneBottomLeftRight;
  b :=True;
  oForm.Params.IsShowFrenelZoneBottomLeft:=b;
  oForm.Params.IsShowFrenelZoneBottomRight:=b;


 // oForm.Params.IsShowReflectionPoints:=True;

  oForm.Width :=550;
  oForm.Height:=400;

 // oForm.SetDefaultSize;
  oForm.Height:= Trunc(oForm.Height*1.2);
  oForm.Width := Trunc(oForm.Width *1.2);

  RelProfile1.SetRefraction(FLinkData.Refraction);
  RelProfile2.SetRefraction(FLinkData.Refraction);

  RelProfile.Assign(RelProfile1);
  RelProfile.Join  (RelProfile2);

//  RelProfile.in;


//  FLinkData.Refraction

 // dOldRefraction:= FLinkData.Refraction;
  oForm.RelProfile_Ref:=RelProfile;

  d:=FLinkData.Length_KM;

(*

  // -------------------------
  if not VarIsNull(oDBLink.Link_repeater.Property_Ground_Height) then
    eRel1 :=oDBLink.Link_repeater.Property_Ground_Height
  else
    eRel1 :=FrelProfile2.FirstItem().Rel_H;

  // -------------------------
  if not VarIsNull(oDBLink.Property2_Ground_Height) then
    eRel2 :=oDBLink.Property2_Ground_Height
  else
    eRel2 :=FrelProfile2.LastItem().Rel_H;

  // -------------------------
  Fframe_Profile_Rel.Params.SiteGroups[1].Site1.Rel_H:=eRel1;
  Fframe_Profile_Rel.Params.SiteGroups[1].Site2.Rel_H:=eRel2;


*)

  is_profile_reversed:=ADOStoredProc_Link.FieldByName(FLD_is_profile_reversed).AsBoolean;

  // -------------------------
//  if FLinkData.LinkRepeater.ID =0 then
  // -------------------------
  begin
    oForm.Params.SetSiteGroupCount(2);

    oForm.Params.SiteGroups[0].Distance_KM:=FLinkData.LinkRepeater.Length1_KM; // RelProfile1.GeoDistance_KM();
    oForm.Params.SiteGroups[1].Distance_KM:=FLinkData.LinkRepeater.Length2_KM; //RelProfile2.GeoDistance_KM();

  //  iDirection:=ADOStoredProc_Link.FieldByName(FLD_CALC_DIRECTION).AsInteger;

    // -------------------------
    // part 1
    // -------------------------
    oForm.Params.SiteGroups[0].Site1.AntennaHeights:=GetAntennaHeightArr(qry_Antennas1);

    oForm.Params.SiteGroups[0].Site2.AntennaHeights:=
        MakeDoubleArray( FLinkData.LinkRepeater.Antenna1_Height);

    oForm.Params.SiteGroups[0].Site1.Rel_H:= FLinkData.Property1_Ground_Height;
    oForm.Params.SiteGroups[0].Site2.Rel_H:= FLinkData.LinkRepeater.Property_Ground_Height;
//    RelProfile1.FirstItem().Rel_H;

    // -------------------------
    // part 2
    // -------------------------
//    GetAntennaHeightArr(qry_Antennas1);
    oForm.Params.SiteGroups[1].Site1.AntennaHeights:=
        MakeDoubleArray( FLinkData.LinkRepeater.Antenna2_Height);

//      GetAntennaHeightArr(qry_Antennas2);


 //   oForm.Params.SiteGroups[0].Site2.AntennaHeights:=GetAntennaHeightArr(qry_Antennas1);
    oForm.Params.SiteGroups[1].Site2.AntennaHeights:=GetAntennaHeightArr(qry_Antennas2);

    oForm.Params.SiteGroups[1].Site1.Rel_H:=FLinkData.LinkRepeater.Property_Ground_Height;
    oForm.Params.SiteGroups[1].Site2.Rel_H:=FLinkData.Property2_Ground_Height;
     //RelProfile2.LastItem().Rel_H;
                            

//
//    oForm.Params.SiteGroups[0].ReflectionPointArr:=dmLink.GetReflectionPointArray(FLink_ID);

  end;


  oForm.Params.Freq_MHz:= FLinkData.TX_FREQ_MHz;

//  qry_LinkEnd1.FieldByName(FLD_TX_FREQ_MHz).AsFloat;

///    (qry_LinkEnd1.FieldByName(FLD_TX_FREQ).AsFloat +
  ///   qry_LinkEnd1.FieldByName(FLD_RX_FREQ).AsFloat) / 2;


  oForm.ShowGraph;
  oForm.SaveToBmpFile (aFileName);

(*const
  PARAM_IDs: array[1..3] of integer = (22,158,169);
*)

  for i:=1 to High(PARAM_IDs) do
  begin
    b:=GetCalcResult_Refraction(PARAM_IDs[i], dRefraction);

//    dRefraction:= GetCalcResult(PARAM_IDs[i]);

  //  RelProfile.SetRefraction(dRefraction);


    RelProfile1.SetRefraction(dRefraction);
    RelProfile2.SetRefraction(dRefraction);

    RelProfile.Assign(RelProfile1);
    RelProfile.Join  (RelProfile2);


//    RelProfile.SetDirection(iDirection);

//////////    RelProfile.IsDirectionReversed := is_profile_reversed;


    oForm.Params.IsShowReflectionPoints:= false;
    oForm.RelProfile_Ref:=RelProfile;

    oForm.ShowGraph;
    sDir:= ChangeFileExt(aFileName,Format('%d.bmp', [i]));
    oForm.SaveToBmpFile (sDir);
  end;

///////////////  RelProfile.SetRefraction(dOldRefraction);

  FreeAndNil(oForm);

//  oForm.Free;
end;



//--------------------------------------------------------------------
procedure TdmLink_report_new.AddSiteInfo ( aSiteNumber: integer);
//--------------------------------------------------------------------
var
  sPrefix: string;
  sProtectionType,sANTENNA_HEIGHT: string;
  sANTENNA_DIAMETER: string;
  i: integer;
  h1,h2,dTilt,dAzimuth: double;
  oQryAntenna,oQryLinkEnd,oQryProperty: TADOQuery;
  sANTENNA_DIAMETER2,sManufacturer,sBeeDeviceName,s: string;

  blPoint1,blPoint2: TBLPoint;

  blPos_WGS,blPos,blPos1: TBLPoint;
  eDiameter: Double;
  site1_rel_h: integer;
  site2_rel_h: integer;
  eFreq_MHz: double;

begin

  sPrefix:='SITE'+AsString(aSiteNumber)+'_';

  case aSiteNumber of
    1: begin
         oQryAntenna :=qry_Antennas1;
         oQryLinkEnd :=qry_LinkEnd1;
         oQryProperty:=qry_Property1;

         qry_Antennas1.First;
         qry_Antennas2.First;
         h1:=qry_Antennas1.FieldBYName(FLD_HEIGHT).AsFloat;
         h2:=qry_Antennas2.FieldBYName(FLD_HEIGHT).AsFloat;


(*         h1:=qry_Antennas2.FieldBYName(FLD_HEIGHT).AsFloat;
         h2:=qry_Antennas1.FieldBYName(FLD_HEIGHT).AsFloat;
*)

         site1_rel_h :=  RelProfile.FirstItem().Rel_H;
         site2_rel_h :=  RelProfile.LastItem().Rel_H;


         blPoint1:=db_ExtractBLPoint (qry_Property1);
         blPoint2:=db_ExtractBLPoint (qry_Property2);

//         blPos:=db_ExtractBLPoint (oQryProperty);


         dAzimuth:=geo_Azimuth (blPoint1, blPoint2);

//         dAzimuth:=geo_Azimuth (FVector.Point1, FVector.Point2);

         dTilt   :=geo_TiltAngle (blPoint1, blPoint2, Site1_Rel_H+h1, Site2_Rel_H+h2);

//         dTilt   :=geo_TiltAngle (FVector.Point1, FVector.Point2, FSite1_Rel_H+h1, FSite2_Rel_H+h2);
         s:=geo_FormatAngleEx (dTilt);


       end;

    2: begin
         oQryAntenna :=qry_Antennas2;
         oQryLinkEnd :=qry_LinkEnd2;
         oQryProperty:=qry_Property2;

         qry_Antennas1.First;
         qry_Antennas2.First;
         h1:=qry_Antennas1.FieldBYName(FLD_HEIGHT).AsFloat;
         h2:=qry_Antennas2.FieldBYName(FLD_HEIGHT).AsFloat;


         site1_rel_h :=  RelProfile.FirstItem.Rel_H;
         site2_rel_h :=  RelProfile.LastItem.Rel_H;

         blPoint1:=db_ExtractBLPoint (qry_Property1);
         blPoint2:=db_ExtractBLPoint (qry_Property2);


         dAzimuth:=geo_Azimuth (blPoint2, blPoint1);
//         dAzimuth:=geo_Azimuth (FVector.Point2, FVector.Point1);
         dTilt   :=geo_TiltAngle (blPoint2, blPoint1, Site2_Rel_H+h2, Site1_Rel_H+h1);
//         dTilt   :=geo_TiltAngle (FVector.Point2, FVector.Point1, FSite2_Rel_H+h2, FSite1_Rel_H+h1);
         s:=geo_FormatAngleEx (dTilt);
//         blPos   :=FVector.Point2;

//         sBeeDeviceName:=qry_Property2.FieldBYName(FLD_BEE_BEENET).AsString +'<->'+
  //                       qry_Property1.FieldBYName(FLD_BEE_BEENET).AsString
       end;

  end;

//  DoAdd(aNode, sBeeDeviceName, TAG_DEVICE_NAME);

//  dmLink_report_data.mem_Link[sPrefix + FLD_DEVICE_NAME] := sBeeDeviceName;

    // �������� ��������
  with oQryProperty,dmLink_report_data do
  begin
    //mem_Link[FLD_SESR_NORM]:=

    mem_Link[sPrefix + FLD_NAME]      :=FieldValues[FLD_NAME];//, TAG_NAME );

    mem_Link[sPrefix + FLD_NAME_ERP]  :=FieldValues[FLD_NAME_ERP];//, TAG_NAME );
    mem_Link[sPrefix + FLD_NAME_SDB]  :=FieldValues[FLD_NAME_SDB];//, TAG_NAME );


   if Assigned(FindField(FLD_BEE_BEENET)) then
     mem_Link[sPrefix + FLD_BEE_BEENET] :=FieldValues[FLD_BEE_BEENET];//, FLD_RANGE );


  //  mem_Link[sPrefix + FLD_BEE_BEENET]:=FieldValues[FLD_BEE_BEENET];//, TAG_BEENET);
     mem_Link[sPrefix + FLD_ADDRESS]   :=FieldValues[FLD_ADDRESS];//, FLD_ADDRESS );

    // ---------------------------------------------------------------

     blPos:=db_ExtractBLPoint (oQryProperty);

     mem_Link[sPrefix + FLD_LAT_UNFORMATTED]:=  blPos.B;
     mem_Link[sPrefix + FLD_LON_UNFORMATTED]:=  blPos.L;

     mem_Link[sPrefix + FLD_LAT]:= geo_DegreeToStr (blPos.B);
     mem_Link[sPrefix + FLD_LON]:= geo_DegreeToStr (blPos.L);

     blPos_WGS:=geo_BL_to_BL_new (blPos, EK_CK_42, EK_WGS_84);

     mem_Link[sPrefix + FLD_LAT_WGS_UNFORMATTED]:=blPos_WGS.B;
     mem_Link[sPrefix + FLD_LON_WGS_UNFORMATTED]:=blPos_WGS.L;

     mem_Link[sPrefix + FLD_LAT_WGS]:=geo_DegreeToStr (blPos_WGS.B);
     mem_Link[sPrefix + FLD_LON_WGS]:=geo_DegreeToStr (blPos_WGS.L);

     // ---------------------------------------------------------------

  end;


  with oQryLinkEnd, dmLink_report_data do
  begin
     // ������� �������� [���]
    eFreq_MHz := FieldByName(FLD_TX_FREQ_MHz).AsFloat;


    mem_Link[sPrefix + FLD_TX_FREQ_MHz]:=eFreq_MHz;//, TAG_TX_FREQ );
    mem_Link[sPrefix + FLD_TX_FREQ_GHZ]:=eFreq_MHz / 1000;//, TAG_TX_FREQ );


 //   mem_Link[sPrefix + FLD_BAND]      :=FieldValues[FLD_BAND];//, FLD_RANGE );



   if Assigned(FindField(FLD_LINKID)) then
     mem_Link[sPrefix + FLD_LINKID]    :=FieldValues[FLD_LINKID];//, FLD_RANGE );


   if Assigned(FindField(FLD_BAND)) then
     mem_Link[sPrefix + FLD_BAND]      :=FieldValues[FLD_BAND];//, FLD_RANGE );


    mem_Link[sPrefix + FLD_LOSS]      :=FieldValues[FLD_LOSS];//, TAG_LOSS );

    // ��������
//    mem_Link[sPrefix + FLD_SPEED_E1]  :=FieldValues[FLD_SPEED_E1];//, TAG_SPEED_E1 , '���������� ����������� ���');
//    mem_Link[sPrefix + FLD_SPEED]     :=FieldValues[FLD_SPEED];//, TAG_SPEED );
    mem_Link[sPrefix + FLD_BItRate_Mbps] :=FieldValues[FLD_BitRate_Mbps];//, TAG_SPEED );



    //---------------------------------------------------------
    //���������� ��������������
    //---------------------------------------------------------
    if FieldBYName(FLD_redundancy).AsInteger = 1 then
    begin
      //��������� ���������� �� ������������
      if (FieldBYName(FLD_kratnost_By_Space).AsInteger=0 ) and
         ( FieldBYName(FLD_kratnost_By_Freq).AsInteger=0 )
      then sProtectionType:='HSBY';

      if (FieldBYName(FLD_kratnost_By_Space).AsInteger=0 ) and
         ( FieldBYName(FLD_kratnost_By_Freq).AsInteger>0 )
      then sProtectionType:='FD';

      if (FieldBYName(FLD_kratnost_By_Space).AsInteger=1 )
//         ( FieldBYName(kratnostByFreq).AsInteger>0 )
      then sProtectionType:='HSBY SD';

      if (FieldBYName(FLD_kratnost_By_Space).AsInteger=1 ) and
         ( FieldBYName(FLD_kratnost_By_Freq).AsInteger>0 )
      then sProtectionType:='FD SD';

    end else
      sProtectionType:='';

    //DoAdd(aNode, sProtectionType, TAG_Protection_Type );
    mem_Link[sPrefix + FLD_Protection_Type]   := sProtectionType;

    // ����� ��� BER=10-3, ����� ��� BER=10-6
  {  DoAdd (aNode, FieldBYName(FLD_THRESHOLD_BER_3).AsInteger, TAG_THRESHOLD_BER_3 );
    DoAdd (aNode, FieldBYName(FLD_THRESHOLD_BER_6).AsInteger, TAG_THRESHOLD_BER_6 );
}
    // ����� ��� BER=10-3, ����� ��� BER=10-6
    mem_Link[sPrefix + FLD_THRESHOLD_BER_3]   := FieldBYName(FLD_THRESHOLD_BER_3).AsFloat;
    mem_Link[sPrefix + FLD_THRESHOLD_BER_6]   := FieldBYName(FLD_THRESHOLD_BER_6).AsFloat;


    // ��������������
    case FieldByName(FLD_REDUNDANCY).AsInteger of
      0: s:= '1+0';
      1: s:= '1+1';
    end;
 //   DoAdd(aNode, s, TAG_REDUNDANCY );
    mem_Link[sPrefix + FLD_REDUNDANCY] :=s;

    // �������� [dBm]

    mem_Link[sPrefix + FLD_POWER_dBm] :=FieldValues[FLD_POWER_dBm];


    //������������� �� ������� ------------------------
    mem_Link[sPrefix + FLD_POWER]   :=FieldValues[FLD_POWER_dBm];
    mem_Link[sPrefix + FLD_SPEED] :=FieldValues[FLD_BitRate_Mbps];//, TAG_SPEED );

    //old
    mem_Link[sPrefix + FLD_TX_FREQ]:=eFreq_MHz / 1000;//, TAG_TX_FREQ );
    // ---------------------------------------------------------------

    mem_Link[sPrefix + FLD_EQUIPMENT_TYPE ] := FieldValues[FLD_LinkEndType_Name];


  end;


  // -------------------------------------------------------------------
  with oQryAntenna, dmLink_report_data do
    if not IsEmpty() then
  // -------------------------------------------------------------------
  begin

     mem_Link[sPrefix + FLD_AZIMUTH]               := geo_FormatAngleEx (dAzimuth);//                    FieldValues['SITE2_' + FLD_AZIMUTH];
     mem_Link[sPrefix + FLD_AZIMUTH_UNFORMATTED]   := dAzimuth;


     s:=geo_FormatAngleEx (dTilt);
    mem_Link[sPrefix + FLD_ANGLE] := geo_FormatAngleEx (dTilt);

    // ������� ������� [�]
  //  eDiameter:=TruncFloat(FieldBYName(FLD_DIAMETER).AsFloat);
    eDiameter:=RoundFloat(FieldBYName(FLD_DIAMETER).AsFloat);

    sANTENNA_DIAMETER:=AsString(eDiameter);
    sANTENNA_HEIGHT  :=FieldBYName(FLD_HEIGHT).AsString;

 // �������� �� ��������/����� [��]
    mem_Link[sPrefix + FLD_GAIN]             := FieldBYName(FLD_GAIN).AsFloat; // FieldValues['SITE2_' + FLD_AZIMUTH_UNFORMATTED];

    s:=FieldByName(FLD_POLARIZATION_str).Asstring;

    mem_Link[sPrefix + FLD_POLARIZATION]     := s;
    mem_Link[sPrefix + FLD_fronttobackratio] := FieldValues[FLD_fronttobackratio];


    if RecordCount>1 then
    begin
      Next;

//      eDiameter:=TruncFloat(FieldBYName(FLD_DIAMETER).AsFloat);
      eDiameter:=RoundFloat(FieldBYName(FLD_DIAMETER).AsFloat);

      sANTENNA_DIAMETER2:=AsString(eDiameter);

      sANTENNA_DIAMETER :=sANTENNA_DIAMETER+ '/'+ sANTENNA_DIAMETER2;
      sANTENNA_HEIGHT   :=sANTENNA_HEIGHT+ '/'+ FieldBYName(FLD_HEIGHT).AsString;

    end;

    mem_Link[sPrefix + FLD_DIAMETER] := sANTENNA_DIAMETER;
    mem_Link[sPrefix + FLD_ANTENNA_DIAMETER] := sANTENNA_DIAMETER;
    mem_Link[sPrefix + FLD_HEIGHT]   := sANTENNA_HEIGHT;//FieldBYName(FLD_HEIGHT).AsString;

  end;

end;


//--------------------------------------------------------------------
procedure TdmLink_report_new.AddLinkInfo;
//--------------------------------------------------------------------
var
  d: Double;
  dTx_Freq_MHz : double;
  e: Double;
  s: string;
begin
 with ADOStoredProc_Link, dmLink_report_data do
  begin
    mem_Link[FLD_ID]        :=FieldValues[FLD_ID];
    mem_Link[FLD_NAME]      :=FieldValues[FLD_NAME];

    //������������� �� ������� ---------------------------------------
    mem_Link[FLD_LENGTH] :=FieldByName(FLD_LENGTH).AsFloat;
    mem_Link[FLD_POWER]  :=FieldByName(FLD_RX_LEVEL_dBm).AsFloat;
    mem_Link[FLD_FREQ]   :=FieldByName(FLD_Tx_Freq_MHz).AsFloat / 1000;
    // ---------------------------------------------------------------


    // ����� ��������� [km]
    mem_Link[FLD_LENGTH_M]    :=FieldByName(FLD_LENGTH).AsFloat;
    mem_Link[FLD_LENGTH_KM]   :=TruncFloat(FieldByName(FLD_LENGTH_KM).AsFloat,2);
    mem_Link[FLD_RX_LEVEL_dBm] :=FieldByName(FLD_RX_LEVEL_dBm).AsFloat;



    d:=FieldByName(FLD_RX_LEVEL_dBm).AsFloat;;
    d:=RoundFloat(d,2);
    mem_Link[FLD_RX_LEVEL_dBm_Round]:=d;
   // mem_Link[FLD_RX_LEVEL_dBm]:=d;
//    FieldByName(FLD_RX_LEVEL_dBm).AsFloat;;

   // mem_Link[FLD_RX_LEVEL]  :=FieldValues[FLD_power];//, TAG_RX_LEVEL);

    mem_Link[FLD_SESR]      :=FieldByName(FLD_SESR).AsFloat;
    mem_Link[FLD_KNG]       :=FieldByName(FLD_KNG).AsFloat;

    mem_Link[FLD_STATUS]    :=FieldByName(FLD_STATUS_STR).AsString;


    dTx_Freq_MHz:=FieldByName(FLD_Tx_Freq_MHz).AsFloat;

    dTx_Freq_MHz:=TruncFloat(dTx_Freq_MHz / 1000, 2);

    mem_Link[FLD_TX_FREQ_MHz] :=FieldByName(FLD_Tx_Freq_MHz).AsFloat;
    mem_Link[FLD_TX_FREQ_GHz] :=FieldByName(FLD_Tx_Freq_MHz).AsFloat / 1000;

//    dTx_Freq_MHz;

  end;


 // db_View( ADOStoredProc_Link);


  with dmLink_report_data, ADOStoredProc_Link do
  begin
    mem_Link[FLD_SESR_NORM]     :=FieldValues[FLD_SESR_NORM];
    mem_Link[FLD_Kng_NORM]      :=FieldValues[FLD_Kng_NORM];

    mem_Link[FLD_ESR_NORM]      :=FieldValues[FLD_ESR_NORM];
    mem_Link[FLD_BBER_NORM]     :=FieldValues[FLD_BBER_NORM];

    mem_Link[FLD_ESR_REQUIRED]  :=FieldValues[FLD_ESR_REQUIRED];
    mem_Link[FLD_BBER_REQUIRED] :=FieldValues[FLD_BBER_REQUIRED];

    mem_Link[FLD_MARGIN_HEIGHT]      :=FieldValues[FLD_MARGIN_HEIGHT];    //, TAG_MARGIN_HEIGHT, '����������� ������������� ������� [m]');
    mem_Link[FLD_MARGIN_DISTANCE]    :=FieldValues[FLD_MARGIN_DISTANCE];  //, TAG_MARGIN_DISTANCE, '��������� ����� �������� [�m]');

    mem_Link[FLD_SESR_SUBREFRACTION] :=FieldValues[FLD_SESR_SUBREFRACTION];//, TAG_SESR_SUBREFRACTION, 'SESR �������������� ������������ [%]');
    mem_Link[FLD_SESR_RAIN]          :=FieldValues[FLD_SESR_RAIN];        //, TAG_SESR_RAIN, 'SESR �������� ������������ [%]');
    mem_Link[FLD_SESR_INTERFERENCE]  :=FieldValues[FLD_SESR_INTERFERENCE];//, TAG_SESR_INTERFERENCE, 'SESR ����������������� ������������ [%]');


   // e:=FieldValues[FLD_KNG_SUBREFRACTION];

   {$IFDEF gost}
    mem_Link[FLD_KNG_SUBREFRACTION] :=FieldValues[FLD_KNG_SUBREFRACTION];//, TAG_SESR_SUBREFRACTION, 'SESR �������������� ������������ [%]');
    mem_Link[FLD_KNG_RAIN]          :=FieldValues[FLD_KNG_RAIN];        //, TAG_SESR_RAIN, 'SESR �������� ������������ [%]');
    mem_Link[FLD_KNG_INTERFERENCE]  :=FieldValues[FLD_KNG_INTERFERENCE];//, TAG_SESR_INTERFERENCE, 'SESR ����������������� ������������ [%]');
   {$ENDIF}



    mem_Link[FLD_SPACE_LIMIT_PROBABILITY]  :=FieldValues[FLD_SPACE_LIMIT_PROBABILITY];//, TAG_SESR_INTERFERENCE, 'SESR ����������������� ������������ [%]');
  end;

end;



//--------------------------------------------------------------------
function TdmLink_report_new.Execute_LinkNet(aXmlFileName: string): boolean;
//--------------------------------------------------------------------
var
  i, j: integer;
  oIDList: TIDList;

  sDir,sShortDir,sImgFileName: string;
begin

  oIDList:=TIDList.Create;

  //XMLDoc.Clear;
{
  vNode:=xml_AddNode (oXMLDoc.DocumentElement, 'LINKNET');

  DoAdd(vNode, aNetName, TAG_NAME,  '��� �� ����');

//  xml_AddTextNode (vNode, '��� �� ����');
//  xml_AddTextNode (vNode, '�������� ����:');
//  xml_AddTextNode (vNode, '�����:');

  for i:=0 to aLinkLineIDList.Count-1 do
  begin
    dmLinkLine.GetLinkListByLinkLineID(aLinkLineIDList[i].ID, oIDList);

    db_OpenQueryByID(qry_LinkLine, TBL_LINKLINE, aLinkLineIDList[i].ID);

    vChildNode:=xml_AddNode (vNode, 'LINKLINE');


    dmLinkLine_report.AddLinkLineInfo(vChildNode);

    sDir     :=IncludeTrailingBackslash(ExtractFileDir(aXmlFileName)) + ExtractFileNameNoExt(aXmlFileName) + '\';
    sShortDir:=ExtractFileNameNoExt(aXmlFileName) + '\';

    sImgFileName:=sDir + Format('%d_map.bmp', [aLinkLineIDList[i].ID]);
    xml_AddNode(vChildNode, 'MAP_IMAGE', [xml_Att(ATT_FILENAME, sShortDir+ Format('%d_map.bmp', [aLinkLineIDList[i].ID]))]);

    dmLink_print.LinkLine_SaveToImage (aLinkLineIDList[i].ID, sImgFileName);


    for j:=0 to oIDList.Count-1 do
      Link_ExecuteItem (oIDList[j].ID, vChildNode, aXmlFileName);
  end;
}
 // vNode:=xml_AddNode (gl_XMLDoc.DocumentElement, 'RECORD', [xml_ATT (ATT_ID, FID)]);
 // DoAdd(vNode, ' ', TAG_NAME,  '���������, �� ������������� ������:');
{

  for i:=0 to LinkIDList.Count-1 do
    Link_ExecuteItem (LinkIDList[i].ID,  aXmlFileName);
}

////  oXMLDoc.SaveToFile(aXmlFileName);

  oIDList.Free;
  //oXMLDoc.Free;
end;


//--------------------------------------------------------------------
function TdmLink_report_new.Link_ExecuteItem(aID: integer): Boolean;
//--------------------------------------------------------------------

var
  sJpgFileName,sShortDir: string;
  sFileDir: string;
  b: boolean;
  sImgFileName,sMapImgFileName,sFile,sFile1,sFile2: string;
  i: Integer;

begin
 { TODO : !!!!!!!!! }
//  FID:=aID;

  //�� ������� ��� �����
  if not DisableClearFileListForDocument then
    dmLink_report_data.FileListForDocument_Clear;


  DoProgressMsg ('���������� ������...');
  if not OpenData (aID) then
  begin
    Result := False;
    Exit;
  end;

  CursorHourGlass;

  dmLink_report_data.mem_Link.Append;

  AddLinkInfo ();

  //'���������� �������...');

  if FLinkData.LinkRepeater.ID = 0 then
    Profile_Open()
  else
    Profile_Open_Repeater();


  AddSiteInfo (1);
  AddSiteInfo (2);

  AddRRV ();

  AddCalcResults (aID) ;

//  FStep_M:=ADOStoredProc_Link.FieldByName(FLD_PROFILE_STEP).AsInteger; // '1. ��� ��������� ������� [��] (0 - ��������� ����)');
//  FStep_M:=FLinkData.PROFILE_Step_M;
  // ADOStoredProc_Link.FieldByName(FLD_PROFILE_STEP).AsInteger; // '1. ��� ��������� ������� [��] (0 - ��������� ����)');



  // ---------------------------------------------------------------



//  sProfile:=

{  DoProgressMsg ('���������� �������...');
  dmRel_Engine.OpenByVector (FVector);
  dmRel_Engine.AssignClutterModel (FClutterModelID, RelProfile.Clutters);
  b:=dmRel_Engine.BuildProfile (RelProfile, FVector, FStep_M);//, FClutterModelID);

}

//  AddProfile ();


  sFileDir:=dmLink_report_data.FileDir;// + // 'temp\' +
//              CorrectFileName(qry_Link.FieldByName(FLD_NAME).AsString) + '\';


//  sImgFileName:=dmLink_report_data.FileDir + Format('temp\link%d_profile.bmp',[aID]);
  sImgFileName:=sFileDir + Format('link%d_profile.bmp',[aID]);
  sJpgFileName:=ChangeFileExt(sImgFileName,'.jpg');

  if FLinkData.LinkRepeater.ID =0 then
    MakeProfileImage(sImgFileName)
  else
    MakeProfileImage_Repeater(sImgFileName);



  img_BMP_moveto_JPEG (sImgFileName, sJpgFileName);
  dmLink_report_data.FileListForDocument_Add(sJpgFileName);

  // ---------------------------------------------------------------

//  sJpgFileName:=ExtractFileName(sJpgFileName);

  dmLink_report_data.mem_Link.FieldByName (FLD_IMG_PROFILE).AsString:=sJpgFileName;


  for I := 1 to 3 do
  begin
  //  sImgFileName:=dmLink_report_data.FileDir + Format('temp\link%d_profile%d.bmp', [aID,i]);
    sImgFileName:=sFileDir + Format('link%d_profile%d.bmp', [aID,i]);
    sJpgFileName:=ChangeFileExt(sImgFileName,'.jpg');

    img_BMP_moveto_JPEG (sImgFileName, sJpgFileName);

  //  sJpgFileName:=ExtractFileName(sJpgFileName);
    dmLink_report_data.mem_Link.FieldByName (Format('IMG_PROFILE%d', [i])).AsString:=sJpgFileName;


    dmLink_report_data.FileListForDocument_Add(sJpgFileName);
  end;

//  sImgFileName:=dmLink_report_data.FileDir + Format('temp\link%d_map.bmp', [aID]);
  sImgFileName:=sFileDir + Format('link%d_map.bmp', [aID]);
  sJpgFileName:=ChangeFileExt(sImgFileName,'.jpg');


  // ----------------Link_SaveToImage-----------------------------------------------

  DoProgressMsg ('���������� �����...');
 ///////////
  dmLink_map.Link_SaveToImage (aID, sImgFileName, nil);

//Exit;

  img_BMP_moveto_JPEG (sImgFileName, sJpgFileName);

//Exit;


 // sJpgFileName:=ExtractFileName(sJpgFileName);
  dmLink_report_data.FileListForDocument_Add(sJpgFileName);

// ---------------------------------------------------------------
  dmLink_report_data.mem_Link.FieldByName (FLD_IMG_MAP).AsString:=sJpgFileName;
  dmLink_report_data.mem_Link.Post;

  CursorDefault;

  Result := True;
end;


//--------------------------------------------------------------------
function TdmLink_report_new.Execute_LinkList: Boolean;
//--------------------------------------------------------------------
var i: integer;
  sReportTitle: string;
  s: string;
  sExt: string;
  sName: string;
  sDocFileName: string;
begin

  for i:=0 to LinkIDList.Count-1 do
  begin
    DoProgress (i, LinkIDList.Count);

    dmLink_report_data.mem_Link.Close;
    dmLink_report_data.mem_Link.Open;

  //  sName:=dmLink.GetNameByID (aLinkLineID);

    if Link_ExecuteItem (LinkIDList[i].ID)  then //,  aReportTemplateFileName );
    begin

   //test
   //   ReportTemplateFileName:='e:\HTML -�� ��������- ������ �����.xsl';

(*   /////////////
      ReportTemplateFileName:='s:\_Reports\HTML -�� ��������- ������ �����.xsl';
      ReportTemplateFileName:='s:\_Reports\HTML -�� ��������- ����������� �����.xsl';
      ReportTemplateFileName:='s:\_Reports\�� ��������- ������ �����.doc';
      ReportTemplateFileName:='s:\_Reports\�� ��������-������� �����.doc';

*)
      sExt:=GetFileExt (ReportTemplateFileName);
      sDocFileName := '';

      sName:=CorrectFileName(ADOStoredProc_Link.FieldByName(FLD_NAME).AsString);
     // sDocFileName  :=dmLink_report_data.FileDir + Format('%s.html', [sName]);

    //  dmLink_report_xml_Execute (aReportTemplateFileName,sDocFileName);

      sReportTitle:='�����:'+ sName;

//      dmLink_report_data.Document.Category   :='�� ���������';
 //     dmLink_report_data.Document.Object_Name:='LINK';
  //    dmLink_report_data.Document.Object_id  :=LinkIDList[i].ID;
      dmLink_report_data.Document.Link_ID     :=LinkIDList[i].ID;
      dmLink_report_data.Document.LinkLine_ID :=0;



      
//
//        ShortReport_4 (
//    'w:\Tasks\FR_Short4\FRep004\Temp\�������� - �������� 001.xml', // �������� ���� (*.xml)
//    'w:\Tasks\FR_Short4\FRep004'          // ��� ����� � �������������
//    );


  //////////////////////////

(*
  ShortReport_4 (
    'w:\Tasks\FR_Short4\FRep004\Temp\�������� - �������� 001.xml', // �������� ���� (*.xml)
    'w:\Tasks\FR_Short4\FRep004'          // ��� ����� � �������������
    );
*)

(*
   if (Pos('�����������', LowerCase(ReportTemplateFileName))>0) or
      (Pos('�������', LowerCase(ReportTemplateFileName))>0)
   then
   begin
     dmLink_report_xml.Execute_Link_stas (sReportTitle );

     Exit;
   end;
*)



      if Eq(sExt,'.xsl') then
      begin
        sDocFileName  :=dmLink_report_data.FileDir + Format('%s.html', [sName]);
        dmLink_report_xml.Execute_Link (sReportTitle, ReportTemplateFileName, sDocFileName);

      end else

      if Eq(sExt,'.doc') then
      begin
        sDocFileName  :=dmLink_report_data.FileDir + Format('%s.doc', [sName]);
        dmLink_report_data.SaveReportToDocFile (sReportTitle, ReportTemplateFileName, sDocFileName);

      end else
        raise Exception.Create('');

    end;

    //show report !
 //////////   ShellExec (sDocFileName,'');
  end;


  if g_Link_Report_Param.IsShowReportDoc then
    ShellExec (sDocFileName,'');


end;
            


procedure TdmLink_report_new.ExecuteProc;
begin
  Execute_LinkList ();
end;



// ---------------------------------------------------------------
procedure TdmLink_report_new.Profile_Open;
// ---------------------------------------------------------------
var
  sProfile_XML: string;
  s: string;
  b: Boolean;
  eLen_km: Double;
begin

  DoProgressMsg ('���������� �������...');

//  sProfile_XML:=ADOStoredProc_Link.FieldByName(FLD_profile_XML).AsString;
  sProfile_XML:=FLinkData.profile_XML;

  //Assert(Assigned(dmRel_Engine), 'Value not assigned');

  dmRel_Engine.AssignClutterModel (FClutterModelID, RelProfile.Clutters);

  eLen_km:= geo_Distance_KM(FVector);


  if (sProfile_XML='') or (not RelProfile.LoadFromXml(sProfile_XML, eLen_km)) then
  begin
    dmRel_Engine.OpenByVector (FVector);
    b:=dmRel_Engine.BuildProfile1 (RelProfile, FVector, FLinkData.PROFILE_Step_M);//, FClutterModelID);


   // RelProfile.Property1_id:=FLinkData.PropertyID1;
    //RelProfile.Property2_id:=FLinkData.PropertyID2;

    sProfile_XML:=RelProfile.GetXML();

//////    db_UpdateRecord(ADOStoredProc_Link, [db_Par(FLD_profile_XML, sProfile_XML)]);
  end else
    RelProfile.isDirectionReversed := FLinkData.is_profile_reversed;

  // -------------------------
(*
  if VarIsNull(FLinkData.Property1_Ground_Height) then
    FLinkData.Property1_Ground_Height := RelProfile.FirstItem().Rel_H;

  if VarIsNull(FLinkData.Property2_Ground_Height) then
    FLinkData.Property2_Ground_Height := RelProfile.LastItem().Rel_H;
*)
  // -------------------------


 // oForm.Params.SiteGroups[0].Site1.Rel_H:=RelProfile.FirstItem.Rel_H;
//  oForm.Params.SiteGroups[0].Site2.Rel_H:=RelProfile.LastItem.Rel_H;


end;



// ---------------------------------------------------------------
procedure TdmLink_report_new.Profile_Open_Repeater;
// ---------------------------------------------------------------
var
  sProfile_XML1: string;
  sProfile_XML2: string;
  s: string;
  b: Boolean;

  blVector: TBLVector;
  eLen_km: Double;
begin

  DoProgressMsg ('���������� �������...');

//  sProfile_XML:=ADOStoredProc_Link.FieldByName(FLD_profile_XML).AsString;
  sProfile_XML1:=FLinkData.LinkRepeater.Profile1_XML;
  sProfile_XML2:=FLinkData.LinkRepeater.Profile2_XML;

  //Assert(Assigned(dmRel_Engine), 'Value not assigned');

  dmRel_Engine.OpenByVector (FLinkData.blVector);


  dmRel_Engine.AssignClutterModel (FClutterModelID, RelProfile1.Clutters);
  dmRel_Engine.AssignClutterModel (FClutterModelID, RelProfile2.Clutters);

  eLen_km:= geo_Distance_KM(FLinkData.LinkRepeater.BLVector1);

  // -------------------------
  if (sProfile_XML1='') or (not RelProfile1.LoadFromXml(sProfile_XML1, eLen_km)) then
  begin
    blVector := FLinkData.LinkRepeater.BLVector1;

//    dmRel_Engine.OpenByVector (blVector);
    b:=dmRel_Engine.BuildProfile1 (RelProfile1, blVector, FLinkData.PROFILE_Step_M);//, FClutterModelID);


   // RelProfile.Property1_id:=FLinkData.PropertyID1;
    //RelProfile.Property2_id:=FLinkData.PropertyID2;

    sProfile_XML1:=RelProfile2.GetXML();

//////    db_UpdateRecord(ADOStoredProc_Link, [db_Par(FLD_profile_XML, sProfile_XML)]);
  end;
 //  else
  //  RelProfile.isDirectionReversed := FLinkData.is_profile_reversed;

  eLen_km:= geo_Distance_KM(FLinkData.LinkRepeater.BLVector2);

  // -------------------------
  if (sProfile_XML2='') or (not RelProfile2.LoadFromXml(sProfile_XML2, eLen_km)) then
  begin
    blVector := FLinkData.LinkRepeater.BLVector2;

  //  dmRel_Engine.OpenByVector (blVector);
    b:=dmRel_Engine.BuildProfile1 (RelProfile2, blVector, FLinkData.PROFILE_Step_M);//, FClutterModelID);


   // RelProfile.Property1_id:=FLinkData.PropertyID1;
    //RelProfile.Property2_id:=FLinkData.PropertyID2;

    sProfile_XML2:=RelProfile2.GetXML();

//////    db_UpdateRecord(ADOStoredProc_Link, [db_Par(FLD_profile_XML, sProfile_XML)]);
  end;


 // -------------------------
  if VarIsNull(FLinkData.Property1_Ground_Height) then
    FLinkData.Property1_Ground_Height := RelProfile1.FirstItem().Rel_H;

  if VarIsNull(FLinkData.Property2_Ground_Height) then
    FLinkData.Property2_Ground_Height := RelProfile2.LastItem().Rel_H;

  if VarIsNull(FLinkData.LinkRepeater.Property_Ground_Height) then
    FLinkData.LinkRepeater.Property_Ground_Height := RelProfile1.LastItem().Rel_H;



end;


begin

end.


    {


//--------------------------------------------------------------------
procedure TdmLink_report_new.AddCalcResults (aID: integer);
//--------------------------------------------------------------------


      procedure DoAddItems1;//(aQuery: TADOQuery);
      begin
        with qry_LinkCalcResults do
        begin
          First;

          while not Eof do
          begin
            db_AddRecord (dmLink_report_data.mem_Calc_Results,
                      [db_Par(FLD_NAME,  '    '+FieldValues[FLD_CAPTION]),
                       db_Par(FLD_VALUE, FieldValues[FLD_VALUE_]) ]);

            Next;
          end;
        end;
      end;


(*
      procedure DoAddItems;//(aQuery: TADOQuery);
      begin
        with qry_LinkCalcResults_Item do
          while not Eof do
          begin
            db_AddRecord (dmLink_report_data.mem_Calc_Results,
                      [db_Par(FLD_NAME,  '    '+FieldValues[FLD_CAPTION]),
                       db_Par(FLD_VALUE, FieldValues[FLD_VALUE_]) ]);

            Next;
          end;
      end;
*)
const

  SQL_SELECT_CalcResults =
    'SELECT * FROM '+ TBL_LinkCalcResults +
    ' WHERE (link_id=:link_id) order by ID';  //  order by CAPTION


  SQL_SELECT_CALC_RES_GROUP =
    'SELECT * FROM '+ TBL_LinkCalcResults +
    ' WHERE (link_id=:link_id) and (is_folder=:is_folder)  order by ID';

  SQL_SELECT_CALC_RES_ITEM =
    'SELECT * FROM '+ TBL_LinkCalcResults +
    ' WHERE (parent_id=:parent_id) order by ID';
           //  (link_id=:link_id) and

var
  iID: integer;
begin
(* db_OpenQuery (qry_LinkCalcResults_Group, SQL_SELECT_CALC_RES_GROUP,
                [db_Par(FLD_LINK_ID, aID),
                 db_Par(FLD_IS_FOLDER, 1)
                ]);
*)
  db_OpenQuery (qry_LinkCalcResults, SQL_SELECT_CalcResults,
                [db_Par(FLD_LINK_ID, aID)
                ]);

  qry_LinkCalcResults_Group.Clone(qry_LinkCalcResults);
  qry_LinkCalcResults_Group.Filtered := True;
  qry_LinkCalcResults_Group.Filter  := 'is_folder=1';

//  qry_LinkCalcResults1.Clone(qry_LinkCalcResults1);
  qry_LinkCalcResults.Filtered := True;
//  qry_LinkCalcResults1.Filter  := 'is_folder<>1';

(*
  db_ViewDataSet(qry_LinkCalcResults_Group);
*)

  dmLink_report_data.mem_Calc_Results.Close;
  dmLink_report_data.mem_Calc_Results.Open;



  with qry_LinkCalcResults_Group do
    while not Eof do
  begin
    if Eq(qry_LinkCalcResults_Group[FLD_CAPTION], '�������� ������ (XML)') then
    begin
      Next;
      Continue;
    end;

    db_AddRecord (dmLink_report_data.mem_Calc_Results,
            [
             db_Par(FLD_IS_GROUP,   True ),
             db_Par(FLD_NAME,       FieldValues[FLD_CAPTION] ),
             db_Par(FLD_VALUE,      '.')
            ]);

    iID :=qry_LinkCalcResults_Group[FLD_ID];
    qry_LinkCalcResults.Filter  := 'PARENT_ID='+ IntToStr(iID);

(*
    db_OpenQuery (qry_LinkCalcResults_Item, SQL_SELECT_CALC_RES_ITEM,
          [db_Par(FLD_LINK_ID,   qry_LinkCalcResults_Group[FLD_LINK_ID]),
           db_Par(FLD_PARENT_ID, qry_LinkCalcResults_Group[FLD_ID])]);
*)

//    DoAddItems;
    DoAddItems1;

    db_AddRecord (dmLink_report_data.mem_Calc_Results,
              [db_Par(FLD_IS_GROUP,     True ),
               db_Par(FLD_NAME,         '.'),
               db_Par(FLD_VALUE,        '.') ]);

    Next;
  end;

end;


