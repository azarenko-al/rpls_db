library dll_link_report;

{ Important note about DLL memory management: ShareMem must be the
  first unit in your library's USES clause AND your project's (select
  Project-View Source) USES clause if your DLL exports any procedures or
  functions that pass strings as parameters or function results. This
  applies to all strings passed to and from your DLL--even those that
  are nested in records and classes. ShareMem is the interface unit to
  the BORLNDMM.DLL shared memory manager, which must be deployed along
  with your DLL. To avoid using BORLNDMM.DLL, pass string information
  using PChar or ShortString parameters. }

uses
  dm_App_link_report in '..\src\dm_App_link_report.pas' {dmMain_app__report: TDataModule},
  Classes,
  dm_Link_map in '..\src\dm_Link_map.pas' {dmLink_map: TDataModule},
  dm_Link_report in '..\src\dm_Link_report.pas' {dmLink_report: TDataModule},
  dm_Link_report_data in '..\src\dm_Link_report_data.pas' {dmLink_report_data: TDataModule},
  dm_Link_report_XML in '..\src\dm_Link_report_XML.pas' {dmLink_report_XML: TDataModule},
  dm_LinkLine_report in '..\src\dm_LinkLine_report.pas' {dmLinkLine_report: TDataModule},
  I_Link_report in '..\src_shared\I_Link_report.pas',
  SysUtils,
  u_Link_report_class in '..\src\u_Link_report_class.pas',
  x_Link_report in '..\src_dll\x_Link_report .pas',
  u_ini_Link_report_params in '..\src_shared\u_ini_Link_report_params.pas';

{$R *.res}

begin
end.
