unit u_func_msg;

interface
uses
   Classes,SysUtils,Forms, Variants,

   u_classes,
   u_func
  // u_geo
   ;


type

  TEventParam = class (TCollectionItem)
  public
    Name: string;
    Value: Variant;

    function AsInteger: integer;
  end;

  TEventParamList = class (TCollection)
  private
    function GetItem (Index: integer): TEventParam;
    function GetItemByName(Index: string): TEventParam;
  public
    ObjName: string;
    ID : Integer;

    Lat,Lon: double;

    Files: TStrings;

    IDList: TIDList;
    Checked: Boolean;

    constructor Create;
    
    procedure AddItem_ (aParam: string; aValue: Variant);


    function FindItem (aName: string): TEventParam;
    function ValueByName (aName: string): Variant;


    property ItemByName[Index: string]: TEventParam read GetItemByName; // default;
    property Items_[Index: integer]: TEventParam read GetItem; default;
  end;
      


type
  TEventType =
     (et_RefreshData,

      et_Refresh_Modes,

//      et_MAP_CLOSED,

      et_MAP_UPDATE_STOP,
      et_MAP_UPDATE_START,


      et_PROJECT_CLEAR,
      et_PROJECT_CHANGED,
//      WE_PROJECT_CHANGED_

      et_PROJECT_CLOSED,
      et_PROJECT_UPDATE_MATRIX_LIST,

      // ------------------------------
      WE_PROPERTY_REFRESH,

      et_LINK_REFRESH,
      WE_LINKEND_ANTENNAS_REFRESH,

      WM_REFRESH_SITE,
      WE_LINKNET_LINKS_REFRESH_,
      WM_PMPSECTOR_REFRESH,


      WE_OBJECT_GET_POPUP_MENU,
      WE_OBJECT_GET_GRID_MENU,

      // ------------------------------
      et_MAP_EVENT_ON_CURSOR,
      et_MAP_EVENT_ON_SELECT,
      et_MAP_EVENT_ON_Free,

      et_MAP_SAVE_AND_CLOSE_WORKSPACE,
      et_MAP_RESTORE_WORKSPACE,
      et_MAP_CLEAR,
      et_MAP_REFRESH,
      et_MAP_CLOSED,
      et_MAP_CLOSE,
      et_MAP_REFRESH_FROM_DESKTOP,
      et_MAP_REFRESH_WMS,
      WE_MAP_REFRESH_OBJECT_Themas,

      et_DEL_MAP_BY_MASK,

      et_INTERFACE_NORMAL,
      et_INTERFACE_SIMPLE,

      et_ADD_MAP,
      et_DEL_MAP,

      WM_DATA_CHANGED,

      et_CalcMap_REFRESH,

      et_Explorer_REFRESH_REL_MATRIX,
      et_Explorer_REFRESH_MAP_FILES,
      et_EXPLORER_ReLoad_LIB,

      et_REL_MATRIX_LIST_CHECKED,
      et_REL_MATRIX_ENABLED



     );


type
  // �������� ����������, ������� ������ ������������� �����, �����������
  // � ���������� �� ���������� ������� ������

//  IMessageHandler = interface
//    ['{DB6CC915-A762-4C09-B12A-053D3D2E765C}']
//    procedure GetMessage (aMsg: integer; aParams: TEventParamList; var aHandled: Boolean);
//  end;

  IMessageHandler = interface
  ['{DB6CC915-A762-4C09-B12A-111D3D2E765C}']
    procedure GetMessage(aMsg: TEventType; aParams: TEventParamList; var aHandled:     boolean);
  end;



type
//  TOnActionExecuteEvent  = procedure (aMsg: integer;  aParams: TEventParamList; var aHandled: Boolean) of object;

  TEventManager = class(TStringList)
  public
    procedure PostEvent_(aMsg: TEventType; aParams: array of Variant);
    procedure PostEvent_ID(aMsg: TEventType; aID: integer);
    procedure PostEvent_LatLon(aMsg: TEventType; aLat, aLon: Double);

  end;




var
  g_EventManager: TEventManager;




//===================================================================
implementation
//===================================================================

 
//==================================================================
// TEventParamList
//==================================================================

function TEventParamList.FindItem(aName: string): TEventParam;
var i:integer;
begin
  for i:=0 to Count-1 do
    if Eq(Items_[i].Name, aName) then begin Result:=Items_[i]; Exit; end;

  Result:=nil;
  raise Exception.Create('Result:=Null; param:'+aName);
end;



function TEventParamList.ValueByName (aName: string): Variant;
var obj: TEventParam;
begin
  obj:=FindItem(aName);
  if Assigned(obj) then Result:=obj.Value
                   else
                     raise Exception.Create('Result:=Null; param:'+aName);
//                   Result:=Null;
end;

{
procedure TEventParamList.AddItem (aRec: TEventParamRec);
var obj: TEventParam;
begin
  obj:=TEventParam.Create (Self);
  obj.Name:=aRec.Name;
  obj.Value:=aRec.Value;
 // obj.Pointer:=aRec.Pointer;

end;
 }

procedure TEventParamList.AddItem_ (aParam: string; aValue: Variant);
var
  obj: TEventParam;
begin
  if Eq(aParam, 'objname') then objname:=aValue else
  if Eq(aParam, 'ID')      then ID:=aValue  else
  if Eq(aParam, 'Checked') then Checked:=aValue  else

  if Eq(aParam, 'IDList')  then IDList:=TIDList(Integer(aValue))   else
  if Eq(aParam, 'Files')   then Files :=TStrings(Integer(aValue)) else

   //   StrList: TStrings;

  if Eq(aParam, 'Lat') then Lat:=aValue  else
  if Eq(aParam, 'Lon') then Lon:=aValue;


  //  ObjName: string;
   // ID : Integer;


  obj:=TEventParam.Create (Self);
  obj.Name:=aParam;
  obj.Value:=aValue;

end;



function TEventParamList.GetItem (Index: integer): TEventParam;
begin
  Result:=TEventParam (inherited Items[Index])
end;

function TEventParamList.GetItemByName(Index: string): TEventParam;
begin
  Result:=FindItem(Index);
end;




constructor TEventParamList.Create;
begin
  inherited Create (TEventParam);

  IDList:=TIDList.Create;
end;


//-------------------------------------------------------------------
procedure TEventManager.PostEvent_ (aMsg: TEventType; aParams: array of Variant);
//-------------------------------------------------------------------
var
  oEventParams: TEventParamList;

   procedure DoComponentCheck (aComponent: TComponent);
   var
     bHandled: Boolean;
     i: Integer;
     intf: IMessageHandler;
     s: string;

   begin
    bHandled:=false;

    for i:=aComponent.ComponentCount-1 downto 0 do
    begin
      if aComponent.Components[i].GetInterface(IMessageHandler, intf) then
      begin
        s:=aComponent.Components[i].Name;

        intf.GetMessage (aMsg, oEventParams, bHandled);
        if bHandled then
          Exit;

      end;

      DoComponentCheck (aComponent.Components[i]);
    end;

   end;

var
  i: Integer;

begin
  oEventParams:=TEventParamList.Create;

  for i:=0 to (Length(aParams) div 2)-1 do
    oEventParams.AddItem_ (aParams[i*2], aParams[i*2+1]);

  DoComponentCheck (Application);

  oEventParams.Free;

end;

procedure TEventManager.PostEvent_ID(aMsg: TEventType; aID: integer);
begin
  PostEvent_(aMsg, ['id', aID]);
end;


procedure TEventManager.PostEvent_LatLon(aMsg: TEventType; aLat, aLon: Double);
begin
  PostEvent_(aMsg, ['lat', aLat,
                    'lon', aLon ]);
end;
           

function TEventParam.AsInteger: integer;
begin
  Result := Value;
end;



initialization
  g_EventManager:=TEventManager.Create;

finalization
  FreeAndNil(g_EventManager);
end.



