unit Unit2;

interface

uses
  SysUtils, Classes, Dialogs, Forms,

  I_LOS_,

  u_vars,

  dm_Main
  ;

type
  TdmMain_app = class(TDataModule)
    procedure DataModuleCreate(Sender: TObject);
  end;


var
  dmMain_app: TdmMain_app;

//===================================================================
// implementation
//===================================================================
implementation
 {$R *.DFM}

//--------------------------------------------------------------
procedure TdmMain_app.DataModuleCreate(Sender: TObject);
//--------------------------------------------------------------
const
  DEF_TEMP_FILENAME = 'los.ini';

var

//  iProjectID: integer;
  sIniFileName: string;

 // oInifile: TIniFile;
//  blPoint: TBLPoint;

//  BLPoints: TBLPointArrayF;

 // rec: TdmCalcMapAddRec;

 // iMode: (mtLos, mtLosM);

// d: double;
// BLPoint1,BLPoint2,newBLPoint: TBLPoint;

begin

//  sIniFileName:=GetCommonApplicationDataDir(DEF_SOFTWARE_NAME)+
 //                 DEF_TEMP_FILENAME;
                                    

 // sIniFileName:=ParamStr(1);
//  if sIniFileName='' then
    sIniFileName:=g_ApplicationDataDir+ DEF_TEMP_FILENAME;


  if not FileExists(sIniFileName) then
  begin
    ShowMessage(sIniFileName);
    Exit;
  end;

  //---------------------------------------------------------------------

(*
  g_Ini_LOS_Params:=TIni_LOS_Params.Create;
  if not g_Ini_LOS_Params.LoadFromFile(sIniFileName) then
    Exit;
*)


  TdmMain.Init;
  if not dmMain.OpenDB_reg then
//  if not dmMain.OpenFromReg then
    Exit;




  if Load_ILos() then
  begin
    ILos.Init(Application.Handle, dmMain.ADOConnection1.ConnectionObject);
 //   ILos.Init( dmMain.ADOConnection1.ConnectionObject);
    ILos.LoadFromFile(sIniFileName);
    ILos.Execute;
    ILos.Execute;
    ILos.Execute;
    ILos.Execute;

    UnLoad_ILos();
  end;




 // FreeAndNil(dmMain);
  





end;



end.

