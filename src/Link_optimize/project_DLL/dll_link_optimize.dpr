library dll_link_optimize;

{ Important note about DLL memory management: ShareMem must be the
  first unit in your library's USES clause AND your project's (select
  Project-View Source) USES clause if your DLL exports any procedures or
  functions that pass strings as parameters or function results. This
  applies to all strings passed to and from your DLL--even those that
  are nested in records and classes. ShareMem is the interface unit to
  the BORLNDMM.DLL shared memory manager, which must be deployed along
  with your DLL. To avoid using BORLNDMM.DLL, pass string information
  using PChar or ShortString parameters. }

uses
  Classes,
  d_Link_antennas_optimize in '..\Src\d_Link_antennas_optimize.pas' {dlg_Link_antennas_optimize},
  d_Link_Optimize_freq_diversity in '..\Src\d_Link_Optimize_freq_diversity.pas' {dlg_Link_Optimize_freq_diversity},
  dm_Main_app in '..\Src\dm_Main_app.pas' {dmMain_app: TDataModule},
  I_link_optimize in '..\src shared\I_link_optimize.pas',
  SysUtils,
  x_Link_optimize in '..\src_dll\x_Link_optimize.pas',
  u_ini_Link_Optimize_params in '..\src shared\u_ini_Link_Optimize_params.pas',
  u_dll_with_dmMain in '..\..\DLL_common\u_dll_with_dmMain.pas';

{$R *.res}

begin
end.
