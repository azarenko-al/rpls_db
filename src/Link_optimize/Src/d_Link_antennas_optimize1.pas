unit d_Link_antennas_optimize1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs,  StdCtrls, ExtCtrls, rxPlacemnt,
  ActnList, cxGridLevel, cxGridCustomTableView, cxGridTableView,
  cxGridBandedTableView, cxClasses, cxControls, cxGridCustomView, cxGrid,
  Registry,

//  u_Ini_rpls_rrl_params,


  u_Link_run,

  dm_Link_Calc_SESR,

  u_link_model_layout,


  u_link_calc_classes_export,

  u_link_calc_classes_main,

  dm_Main,

  u_ini_LinkCalcParams,


  u_LinkCalcResult_new,


  u_vars,

  u_const,
  u_const_db,

  u_db,
  u_func,

  u_run,

  u_rrl_param_rec_new,

  u_ini_Link_optimize_params,


  dm_Link_calc, DB, dxmdaset

  ;

type
  Tdlg_Link_antennas_optimize1 = class(TForm)
    cxGrid1: TcxGrid;
    cxGrid1BandedTableView1: TcxGridBandedTableView;
    col_Name: TcxGridBandedColumn;
    col_Check: TcxGridBandedColumn;
    col_H_max: TcxGridBandedColumn;
    col_H_new: TcxGridBandedColumn;
    col_H_old: TcxGridBandedColumn;
    col_ID: TcxGridBandedColumn;
    cxGrid1Level1: TcxGridLevel;
    rg_Criteria: TRadioGroup;
    rg_Dop_ant: TRadioGroup;
    ActionList1: TActionList;
    act_Calc: TAction;
    act_Save: TAction;
    FormStorage1: TFormStorage;
    pn_Buttons: TPanel;
    Bevel1: TBevel;
    Button2: TButton;
    Button1: TButton;
    Panel1: TPanel;
    btn_Cancel: TButton;
    dxMemData1: TdxMemData;
    DataSource1: TDataSource;
    dxMemData1name: TStringField;
    dxMemData1height_max: TIntegerField;
    dxMemData1checked: TBooleanField;
    procedure FormDestroy(Sender: TObject);
    procedure act_CalcExecute(Sender: TObject);
    procedure cxGrid1Exit(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
//    FblVector: TblVector;
    FIsChanged: Boolean;
    FParams: TIni_Link_Optimize_Params;
    FRegPath: string;

    FrrlCalcParam: TrrlCalcParam;


    FDBLink: TDBLink;


    procedure ClearRow(aRowIndex: Integer);

    function Execute: Integer;
    function GetGridValue(aRow, aColIndex: Integer): Variant;
    function GetWorkingTypeStr(aStatus: integer): string;
    procedure Init_grid;
    procedure Load;
    function Run_rpls_rrl(aFile_XML: string): Boolean;
    procedure SaveHeights;

    procedure SetGridValue(aRow, aColIndex: Integer; aValue: Variant);
//    procedure SetGridValue_res(aRow, aColIndex: Integer; aValue: Variant);



  public
    class function ExecDlg(aID: Integer; aParams: TIni_Link_Optimize_Params):
        Boolean;

  end;

var
  dlg_Link_antennas_optimize1: Tdlg_Link_antennas_optimize1;

implementation

{$R *.dfm}

const
  REGISTRY_COMMON_FORMS  = 'Software\Onega\Common\Forms\';

  TAG_ROW_TX_ANTENNA    = 0;
  TAG_ROW_RX_ANTENNA    = 1;

  TAG_ROW_TX_ANTENNA_DOP= 2;
  TAG_ROW_RX_ANTENNA_DOP= 3;

  TAG_ROW_capability    = 5;   //�����������
  TAG_ROW_SESR          = 6;   //SESR
  TAG_ROW_KNG           = 7;   //��� �



//-------------------------------------------------------------------
class function Tdlg_Link_antennas_optimize1.ExecDlg(aID: Integer; aParams:
    TIni_Link_Optimize_Params): Boolean;
//-------------------------------------------------------------------

begin
  with Tdlg_Link_antennas_optimize1.Create(Application) do
  begin
    FParams:=aParams;

    TdmLink_calc.Init;

    FDBLink:=dmLink_calc.LinkList.AddItem;

  //  dmLink_calc.Params.Repeater.Active := False;
//    dmLink_calc.Params.IsUsePassiveElements := FParams.UsePassiveElements;
    dmLink_calc.OpenData_new(aID, FDBLink);

    Load();


    Result := ShowModal = mrOk;

//    Result := FIsChanged;

    Free;
  end;
end;



// ---------------------------------------------------------------
procedure Tdlg_Link_antennas_optimize1.FormDestroy(Sender: TObject);
// ---------------------------------------------------------------
begin
  FreeAndNil(FrrlCalcParam);

  with TRegIniFile.Create(FRegPath) do
  begin

    WriteInteger('1','H_max',  AsInteger(GetGridValue(0, col_H_max.Index)));
    WriteInteger('2','H_max',  AsInteger(GetGridValue(1, col_H_max.Index)));
    WriteInteger('3','H_max',  AsInteger(GetGridValue(2, col_H_max.Index)));
    WriteInteger('4','H_max',  AsInteger(GetGridValue(3, col_H_max.Index)));

    WriteBool('1','check', AsBoolean(GetGridValue(0, col_Check.Index)));
    WriteBool('2','check', AsBoolean(GetGridValue(1, col_Check.Index)));
    WriteBool('3','check', AsBoolean(GetGridValue(2, col_Check.Index)));
    WriteBool('4','check', AsBoolean(GetGridValue(3, col_Check.Index)));

    WriteInteger('', rg_Criteria.Name, rg_Criteria.ItemIndex);

    Free;

  end;

  FreeAndNil(dmLink_calc);
end;



// ---------------------------------------------------------------
procedure Tdlg_Link_antennas_optimize1.FormCreate(Sender: TObject);
// ---------------------------------------------------------------
begin
 inherited;

  FRegPath:= REGISTRY_COMMON_FORMS + ClassName +'\';

  Caption:='����������� ����� ������';


{
  Assert(not cxPropertiesStore.Active);

  cxPropertiesStore.StorageName:=FRegPath+'cxPropertiesStore';
  cxPropertiesStore.Active := True;}

  FrrlCalcParam := TrrlCalcParam.Create();




  //�������������� �������� ������� (������)
end;


// ---------------------------------------------------------------
procedure Tdlg_Link_antennas_optimize1.Init_grid;
// ---------------------------------------------------------------
begin
  with TRegIniFile.Create(FRegPath) do
  begin

    //���������� �������
    SetGridValue(0, col_H_max.Index,  ReadInteger('1','H_max',30));
    SetGridValue(1, col_H_max.Index,  ReadInteger('2','H_max',30));
    SetGridValue(2, col_H_max.Index,  ReadInteger('3','H_max',30));
    SetGridValue(3, col_H_max.Index,  ReadInteger('4','H_max',30));

    SetGridValue(0, col_Check.Index,  ReadBool('1','check',False));
    SetGridValue(1, col_Check.Index,  ReadBool('2','check',False));
    SetGridValue(2, col_Check.Index,  ReadBool('3','check',False));
    SetGridValue(3, col_Check.Index,  ReadBool('4','check',False));

    rg_Criteria.ItemIndex:=ReadInteger('',rg_Criteria.Name,0);

    Free;
  end;

  cxGrid1BandedTableView1.ViewData.Rows[2].Values[0]:='�������������� ������� (�����)';
  cxGrid1BandedTableView1.ViewData.Rows[3].Values[0]:='�������������� ������� (������)';

end;


// ---------------------------------------------------------------
procedure Tdlg_Link_antennas_optimize1.act_CalcExecute(Sender: TObject);
// ---------------------------------------------------------------
begin
  if Sender=act_Save then
    SaveHeights else

  if Sender=act_Calc then
    Execute;

end;

procedure Tdlg_Link_antennas_optimize1.ClearRow(aRowIndex: Integer);
var I: integer;
begin
  for I := 0 to cxGrid1BandedTableView1.ColumnCount - 1 do
    cxGrid1BandedTableView1.ViewData.Rows[aRowIndex].Values[i]:=null;
end;

procedure Tdlg_Link_antennas_optimize1.cxGrid1Exit(Sender: TObject);
begin
  cxGrid1BandedTableView1.DataController.PostEditingData;
end;

// ---------------------------------------------------------------
function Tdlg_Link_antennas_optimize1.Run_rpls_rrl(aFile_XML: string): Boolean;

//--function TdmLink_calc.Run_rpls_rrl(aFile_XML, aFile_result, aFile_layout: string):  Boolean;

// ---------------------------------------------------------------
var
  sRunPath: string;

//  oIni_rpls_rrl_params: TIni_rpls_rrl_params;
 // sFile: string;

begin
  Assert(aFile_XML<>'');
//  Assert(aFile_result<>'');
//  Assert(aFile_layout<>'');

//  oIni_rpls_rrl_params:=TIni_rpls_rrl_params.Create;


  sRunPath := GetApplicationDir() + EXE_RPLS_RRL;

  Result := True;


//  oIni_rpls_rrl_params.In_FileName := aFile_XML;
//  oIni_rpls_rrl_params.result_FileName := aFile_result;
//  oIni_rpls_rrl_params.layout_FileName := aFile_layout;


//  sFile :=g_ApplicationDataDir + 'rrl.ini';

//  oIni_rpls_rrl_params.SaveToFile(sFile);

  Result := RunAppEx (sRunPath,  [aFile_XML]);



 // FreeAndNil(oIni_rpls_rrl_params);

end;



//--------------------------------------------------------------------
function Tdlg_Link_antennas_optimize1.Execute: Integer;
//--------------------------------------------------------------------
var sFile1,sFile2: string;
  I: integer;
  sStatus: string;
  iStatus: integer;
  eSESR: double;
  eKNG: double;
  sErrStr: string;

  
  rec: TGet_Equivalent_Length_KM_rec;
  sFile_layout: string;


//

const
  TEMP_IN_FILENAME = 'rrl.xml';
  TEMP_OUT_FILENAME = 'rrl_result.xml';

  TEMP_IN_layout  = 'layout.xml';


const
  DEF_POWER    = 101; //��������� ������� ������� �� ����� ��������� (���)
  DEF_SESR     = 17;
  DEF_KNG      = 151;

  DEF_STATUS   = 7;  //����������� ���������

begin
  Assert(Assigned(dmLink_calc), 'Value not assigned');


//  sFile1:=GetTempFileName_('xml');

  sFile1:=g_ApplicationDataDir + TEMP_IN_FILENAME;
  sFile2:=g_ApplicationDataDir + TEMP_OUT_FILENAME;
  sFile_layout :=g_ApplicationDataDir + TEMP_IN_layout;



//  FrrlCalcParam.Result_FileName:=sFile2;

//  dmLink_calc.Params.IsUsePassiveElements:=FParams.UsePassiveElements;
//  dmLink_calc.Params.IsCalcWithAdditionalRain:=True;


// ..  dmLink_calc. SaveToclass(FrrlCalcParam);

  TDBLink_export.SaveToClass(FDBLink, FDBLink.LinkEnd1, FDBLink.LinkEnd2,
      FrrlCalcParam, FDBLink.relProfile, FDBLink.IsProfileExists);



//  dmLink_calc.
//  SaveToclass(FrrlCalcParam);


  if AsBoolean(GetGridValue(TAG_ROW_TX_ANTENNA, col_Check.Index)) then
    FrrlCalcParam.TTX.Site1_HEIGHT:=- AsInteger(GetGridValue(TAG_ROW_TX_ANTENNA, col_H_max.Index));

  if AsBoolean(GetGridValue(TAG_ROW_RX_ANTENNA, col_Check.Index)) then
    FrrlCalcParam.TTX.Site2_HEIGHT:=- AsInteger(GetGridValue(TAG_ROW_RX_ANTENNA, col_H_max.Index));

  if AsBoolean(GetGridValue(TAG_ROW_RX_ANTENNA_DOP, col_Check.Index)) then
    FrrlCalcParam.TTX.Dop_Site2.HEIGHT:=- AsInteger(GetGridValue(TAG_ROW_RX_ANTENNA_DOP, col_H_max.Index));

  if AsBoolean(GetGridValue(TAG_ROW_TX_ANTENNA_DOP, col_Check.Index)) then
    FrrlCalcParam.TTX.Dop_Site1.HEIGHT:=- AsInteger(GetGridValue(TAG_ROW_TX_ANTENNA_DOP, col_H_max.Index));



  //�������������� ����� //���������� ����� ��������� �� ������
  FrrlCalcParam.TTX.AntennaOffsetHor_m:=0;

//  FrrlCalcParam.TTX.AntennaOffsetHor:=geo_Distance(FblVector);

  FrrlCalcParam.Optimization.OptimizeEnabled :=True;

  case rg_Criteria.ItemIndex of
    0:  FrrlCalcParam.Optimization.OptimizeKind:=otMinMax_H1_H2;
    1:  FrrlCalcParam.Optimization.OptimizeKind:=otMin_H1_plus_H2;
  end;


  case rg_Dop_ant.ItemIndex of
    0:  FrrlCalcParam.Optimization.Criteria_opti_dop_antenna:=otMin_SESR;
    1:  FrrlCalcParam.Optimization.Criteria_opti_dop_antenna:=otMin_KNG;
  end;


  FillChar (rec, SizeOf(rec), 0);

  rec.Length_Km     := FDBLink.GetLength_km;
  rec.GST_length_km := FDBLink.Trb.GST_LENGTH_km_;
  rec.GST_Type      := FDBLink.Trb.GST_TYPE;

//  rec.Length_Km     :=ADOStoredProc_Link.FieldByName(FLD_LENGTH).AsFloat/1000;
//  rec.GST_length_km :=ADOStoredProc_Link.FieldByName(FLD_GST_LENGTH).AsFloat;
//  rec.GST_Type      :=ADOStoredProc_Link.FieldByName(FLD_GST_TYPE).AsInteger;


  FDBLink.Trb.GST_LENGTH_km_  :=  dmLink_Calc_SESR.Get_Equivalent_Length_KM_rec(rec);

  FrrlCalcParam.TRB.GST_Length_KM_3_:= FDBLink.Trb.GST_LENGTH_km_;

//  FrrlCalcParam.TTX.Criteria_opti_dop_antenna




{
  if not FrrlCalcParam.Validate then
  begin
    ShowMessage(FrrlCalcParam.ErrorMsg);
    exit;
  end else
    FrrlCalcParam.SaveToXML (sFile1, True);
}

//  dmLink_calc.Run_rpls_rrl(aFile1, aFile2: string);

  Screen.Cursor:=crHourGlass;

  CalcMethod_LoadFromXMLFile(
      GetApplicationDir + 'link_calc_model_setup\RRL_ParamSys.xml',
      FrrlCalcParam.Calc_method,
      sFile_layout
      );


  FrrlCalcParam.Result_FileName:=sFile2;
  FrrlCalcParam.Layout_FileName:=sFile_layout;


  if FrrlCalcParam.ValidateDlg then
    FrrlCalcParam.SaveToXML (sFile1, True)
  else
    Exit;


  Run_rpls_rrl (sFile1);

{
  if RunApp (GetApplicationDir() + EXE_RPLS_RRL,
             DoubleQuotedStr(sFile1) +' '+
             DoubleQuotedStr(sFile2))
              then
 }

  begin
    dmLink_calc.LinkCalcResults.Clear;

    dmLink_calc.LinkCalcResults.LoadFromXmlFile_RRL_Result(sFile2);
    // LoadCalcResultsFromXML_ToRec (sFile2);
//    dmLink_calc.LoadCalcResultsFromXML_ToRec (sFile2);
//    LoadCalcResultsFromXML (FLinkID, sFile2);

    for I := 0 to cxGrid1BandedTableView1.DataController.RowCount - 1 do
    begin
      SetGridValue(i, col_H_new.Index, '');
    end;

    if AsBoolean(GetGridValue(TAG_ROW_TX_ANTENNA, col_Check.Index)) then
      SetGridValue(TAG_ROW_TX_ANTENNA, col_H_new.Index, dmLink_calc.LinkCalcResults.Data.Optimize.TX_ANTENNA_H);
//      SetGridValue(TAG_ROW_TX_ANTENNA, col_H_new.Index, dmLink_calc.GetCalcParamValue(3));

    if AsBoolean(GetGridValue(TAG_ROW_RX_ANTENNA, col_Check.Index)) then
      SetGridValue(TAG_ROW_RX_ANTENNA, col_H_new.Index, dmLink_calc.LinkCalcResults.Data.Optimize.RX_ANTENNA_H);
//      SetGridValue(TAG_ROW_RX_ANTENNA, col_H_new.Index, dmLink_calc.GetCalcParamValue(4));

    if AsBoolean(GetGridValue(TAG_ROW_RX_ANTENNA_DOP, col_Check.Index)) then
      SetGridValue(TAG_ROW_RX_ANTENNA_DOP, col_H_new.Index, dmLink_calc.LinkCalcResults.Data.Optimize.RX_ANTENNA_DOP_H);

    if AsBoolean(GetGridValue(TAG_ROW_TX_ANTENNA_DOP, col_Check.Index)) then
      SetGridValue(TAG_ROW_TX_ANTENNA_DOP, col_H_new.Index, dmLink_calc.LinkCalcResults.Data.Optimize.TX_ANTENNA_DOP_H);

//      SetGridValue(TAG_ROW_RX_ANTENNA_DOP, col_H_new.Index, dmLink_calc.GetCalcParamValue(6));


    //DEF_SESR := 17;
    eSESR := dmLink_calc.LinkCalcResults.Data.SESR;
    // GetCalcParamValue(DEF_SESR);
//    eSESR := dmLink_calc.GetCalcParamValue(DEF_SESR);
    SetGridValue(TAG_ROW_SESR, col_H_new.Index, eSESR);

  //   cxGridBandedTableView_Res.ViewData.Rows[0].Values[1]:=eSESR;

    iStatus:=dmLink_calc.LinkCalcResults.Data.STATUS;
//    iStatus:=AsInteger(dmLink_calc.GetCalcParamValue(DEF_STATUS));
    sStatus:=IIF(iStatus=1, '�����', '�� �����');

    SetGridValue(TAG_ROW_capability, col_H_new.Index, sStatus);

  //  cxGridBandedTableView_Res.ViewData.Rows[1].Values[1]:=sStatus;


    //DEF_KNG := 151;
    eKNG := dmLink_calc.LinkCalcResults.Data.Kng;
//    eKNG := dmLink_calc.GetCalcParamValue(DEF_KNG);
    SetGridValue(TAG_ROW_KNG, col_H_new.Index, eKNG);

//    cxGridBandedTableView_Res.ViewData.Rows[2].Values[1]:=eKNG;
  //

  end;

   Screen.Cursor:=crDefault;


end;



procedure Tdlg_Link_antennas_optimize1.FormShow(Sender: TObject);
begin
  Init_grid;

  
//  cxGrid1BandedTableView1.ViewData.Rows[2].Values[0]:='�������������� �������� ������� (������)';
//  cxGrid1BandedTableView1.ViewData.Rows[3].Values[0]:='�������������� ���������� ������� (�����)';

end;



function Tdlg_Link_antennas_optimize1.GetGridValue(aRow, aColIndex: Integer):
    Variant;
begin
  Result:=cxGrid1BandedTableView1.ViewData.Rows[aRow].Values[aColIndex];
end;

//--------------------------------------------------------------------
function Tdlg_Link_antennas_optimize1.GetWorkingTypeStr(aStatus: integer):
    string;
//--------------------------------------------------------------------
begin
  case aStatus of
    1: Result:='�����';
    2: Result:='�� �����';
    else
      Result:='?';
  end;
end;

//-------------------------------------------------------------------
procedure Tdlg_Link_antennas_optimize1.Load;
//-------------------------------------------------------------------

var
  sStatus: string;
  iStatus: integer;
begin


 // dmMain.ProjectID:=470;
 // dmLink_calc.OpenData(46401);

// db_ViewDataSet(dmLink_calc.qry_Link);


//  db_ViewDataSet(dmLink_calc.qry_Antennas1);
  SetGridValue (TAG_ROW_SESR, col_H_old.Index, dmLink_calc.ADOStoredProc_Link.FieldByName(FLD_SESR).AsFloat);
  SetGridValue (TAG_ROW_KNG,  col_H_old.Index, dmLink_calc.ADOStoredProc_Link.FieldByName(FLD_KNG).AsFloat);

  iStatus:=dmLink_calc.ADOStoredProc_Link.FieldByName(FLD_STATUS).AsInteger;
  sStatus:=GetWorkingTypeStr(iStatus); //dmLink.

  SetGridValue (TAG_ROW_capability, col_H_old.Index, sStatus);

  with dmLink_calc.qry_Antennas1 do
  begin
    First;

    SetGridValue (0, col_H_old.Index, FieldByName(FLD_HEIGHT).AsFloat);
    SetGridValue (0, col_ID.Index,    FieldByName(FLD_ID).AsInteger);

    if RecordCount>1 then
    begin
      Next;

//      FblVector.Point2:=db_ExtractBLPoint(dmLink_calc.qry_Antennas2);

      SetGridValue (TAG_ROW_TX_ANTENNA_DOP, col_H_old.Index, FieldByName(FLD_HEIGHT).AsFloat);
      SetGridValue (TAG_ROW_TX_ANTENNA_DOP, col_ID.Index,    FieldByName(FLD_ID).AsInteger);

    end;

   {
      TAG_ROW_TX_ANTENNA    = 0;
  TAG_ROW_RX_ANTENNA    = 1;

  TAG_ROW_TX_ANTENNA_DOP= 2;
  TAG_ROW_RX_ANTENNA_DOP= 3;

  TAG_ROW_capability    = 5;   //�����������
  TAG_ROW_SESR          = 6;   //SESR
  TAG_ROW_KNG           = 7;   //��� �
   }

//    SetGridValue (0, col_H_old.Index);

  end;


//  FillChar(FblVector, SizeOf(FblVector), 0);


  with dmLink_calc.qry_Antennas2 do
  begin
    First;

//    FblVector.Point1:=db_ExtractBLPoint(dmLink_calc.qry_Antennas2);

    SetGridValue (1, col_H_old.Index, FieldByName(FLD_HEIGHT).AsFloat);
    SetGridValue (1, col_ID.Index,    FieldByName(FLD_ID).AsInteger);

    if RecordCount>1 then
    begin
      Next;

//      FblVector.Point2:=db_ExtractBLPoint(dmLink_calc.qry_Antennas2);

      SetGridValue (TAG_ROW_RX_ANTENNA_DOP, col_H_old.Index, FieldByName(FLD_HEIGHT).AsFloat);
      SetGridValue (TAG_ROW_RX_ANTENNA_DOP, col_ID.Index,    FieldByName(FLD_ID).AsInteger);

    end else
      ClearRow(2);

  end;


end;

// ---------------------------------------------------------------
procedure Tdlg_Link_antennas_optimize1.SaveHeights;
// ---------------------------------------------------------------
var
  b: Boolean;
  iID,i: integer;
  eH: double;
begin
  for I := 0 to 3 do
//  for I := 0 to 2 do
  begin
    eH :=AsFloat  (GetGridValue(i, col_H_new.Index));
    iID:=AsInteger(GetGridValue(i, col_ID.Index));

    b:=AsBoolean(GetGridValue(i, col_Check.Index));


    if b and (eH>0) then
      gl_DB.UpdateRecord(TBL_LINKEND_ANTENNA, iID,
                        [db_Par(FLD_HEIGHT, eH)]);
  end;


  FIsChanged:=True;
end;


procedure Tdlg_Link_antennas_optimize1.SetGridValue(aRow, aColIndex: Integer;
    aValue: Variant);
begin
  Assert (aColIndex <>0 );

  cxGrid1BandedTableView1.ViewData.Rows[aRow].Values[aColIndex]:=aValue;
end;



end.

{



procedure Tdlg_Link_antennas_optimize1.SetGridValue_res(aRow, aColIndex:  Integer; aValue: Variant);
begin
  cxGrid1BandedTableView1.ViewData.Rows[aRow].Values[aColIndex]:=aValue;
end;

