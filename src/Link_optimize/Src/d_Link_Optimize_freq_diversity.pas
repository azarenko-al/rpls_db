unit d_Link_Optimize_freq_diversity;

interface

uses
  SysUtils, Variants, Classes, Controls, Forms,  Dialogs, d_Wizard, ActnList, StdCtrls,
  ExtCtrls, cxGridCustomTableView, cxGridTableView, Math,
  cxClasses, cxControls, cxGridCustomView, cxPropertiesStore,
  cxGridLevel, cxGrid, Registry,


  u_link_model_layout,

  //u_link_calc_classes_export,


  u_link_calc_classes_export,


  
  u_link_calc_classes_main,

  u_Log,


  u_DB_Manager,

  u_ini_LinkCalcParams,

  u_LinkCalcResult_new,

  dm_Main,
  u_vars,

  u_const,

  u_const_db,

  u_db,

  u_func,

  u_dlg,

  u_run,

  u_rrl_param_rec_new,

  dm_Link_calc,
  dm_LinkEnd,
  dm_LinkEndType,
  dm_Link  ,

  u_ini_Link_Optimize_params,

  rxPlacemnt, cxLookAndFeels, cxGraphics, cxLookAndFeelPainters, cxStyles,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit;

type
  Tdlg_Link_Optimize_freq_diversity = class(Tdlg_Wizard)
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    cxGrid1TableView1: TcxGridTableView;
    cxGrid1TableView1Column1: TcxGridColumn;
    col_Max: TcxGridColumn;
    col_Old: TcxGridColumn;
    col_New: TcxGridColumn;
    Panel1: TPanel;
    ed_ChannelWidth1: TEdit;
    Label1: TLabel;
    lb_ChannelWidth: TLabel;
    Label3: TLabel;
    lb_ChannelCount: TLabel;
    ed_ChannelCount1: TEdit;
    ActList: TActionList;
    act_Optimize: TAction;
    act_Save: TAction;
    Button1: TButton;
    Button2: TButton;
    Memo1: TMemo;
    procedure act_OptimizeExecute(Sender: TObject);
    procedure act_SaveExecute(Sender: TObject);

    procedure cxGrid1Exit(Sender: TObject);
    procedure cxGrid1TableView1Editing(Sender: TcxCustomGridTableView; AItem: TcxCustomGridTableItem;
        var AAllow: Boolean);
    procedure FormDestroy(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    FParams: TIni_Link_Optimize_Params;


    FDBManager: TDBManager;



    FDBLink: TDBLink;



//    FIsLoaded : boBoolean;
    FrrlCalcParam: TrrlCalcParam;

    FChannelCount: Integer;
    FChannelWidth: double;
    FIsChanged: boolean;

 //   FIsLoaded: Boolean;

    FLinkInfoRec: TdmLinkInfoRec;
    FLinkEndInfoRec: TdmLinkEndAddRec;


    FLinkID: integer;

    function GetGridValue(aRow, aColIndex: Integer): Variant;
    function GetWorkingTypeStr(aValue: integer): string;
    function OpenData(aLInkID: Integer): Boolean;
    procedure Optimize;
    procedure SetGridValue(aRow, aColIndex: Integer; aValue: Variant);

  public
    class function ExecDlg(aLinkID: Integer; aParams: TIni_Link_Optimize_Params):
        Boolean;

    procedure Save;
  end;


implementation

(*uses
  u_db_manager;
*)
{$R *.dfm}


const

  TAG_ROW_MHz = 0;
  TAG_ROW_nF  = 1;

  TAG_ROW_capability = 3;   //�����������
  TAG_ROW_SESR       = 4;



//-------------------------------------------------------------------
class function Tdlg_Link_Optimize_freq_diversity.ExecDlg(aLinkID: Integer;
    aParams: TIni_Link_Optimize_Params): Boolean;
//-------------------------------------------------------------------
//var
 // oLink: TDBLink;

begin
  with Tdlg_Link_Optimize_freq_diversity.Create(Application) do
  begin
    FParams:= aParams;

    FLinkID:=aLinkID;


    TdmLink_calc.Init;

    FDBLink:=dmLink_calc.LinkList.AddItem;

  //  dmLink_calc.Params.Repeater.Active := False;
//    dmLink_calc.Params.IsUsePassiveElements := FParams.UsePassiveElements;
    dmLink_calc.OpenData_new(aLinkID, FDBLink);


//    TdmLink_calc.Init;

//    oLink:=dmLink_calc.LinkList.AddItem;

//    dmLink_calc.OpenData_new(aID, oLink);


    if OpenData(aLinkID) then
      ShowModal;

    Result := FIsChanged;

    Free;
  end;
end;

//-------------------------------------------------------------------
function Tdlg_Link_Optimize_freq_diversity.OpenData(aLInkID: Integer): Boolean;
//-------------------------------------------------------------------
var
  sStatus: string;
  iBandID: Integer;
begin
  dmLink.GetInfo1(aLInkID, FLinkInfoRec);

  dmLinkEnd.GetInfoRec(FLinkInfoRec.LinkEnd1_ID, FLinkEndInfoRec);
//  GetChannelWidth();

  if FLinkEndInfoRec.Kratnost_by_Freq=0 then
  begin
    ShowMessage('��������� ���������� �� ������� = N+0.');
    Result := False;
    Exit;
  end;


  case FLinkEndInfoRec.EquipmentType of  //������������: 0-�������, 1-���������

     DEF_NON_TYPE_EQUIPMENT :
       SetGridValue (TAG_ROW_MHz, col_Max.index, 100);


     DEF_TYPE_EQUIPMENT :
      begin
        //---------------------------------------------------------
        iBandID := dmLinkEndType.GetBandID_by_Name(FLinkEndInfoRec.SubBand,
                                        FLinkEndInfoRec.LinkEndType_ID);

        if iBandID > 0 then
        begin


          FChannelWidth:= FDBManager.GetDoubleFieldValue(TBL_LINKENDTYPE_BAND, FLD_CHANNEL_WIDTH,
                                                    [db_par(FLD_ID, iBandID)]);

          FChannelCount:= FDBManager.GetIntFieldValue(TBL_LINKENDTYPE_BAND, FLD_CHANNEL_COUNT,
                                                    [db_par(FLD_ID, iBandID)]);


          ed_ChannelWidth1.Text:=FloatToStr(FChannelWidth);
          ed_ChannelCount1.Text:=FloatToStr(FChannelCount);

        end;
        //---------------------------------------------------------


        if FChannelWidth=0 then
        begin
          ErrorDlg('�� ����� �����������.');

          g_Log.Error ('ChannelWidth=0');

          Result := False;
          Exit;
        end;



        SetGridValue (TAG_ROW_nF,  col_Max.index, (FChannelCount-1));
        SetGridValue (TAG_ROW_MHz, col_Max.index, (FChannelCount-1)*FChannelWidth);
      end;

  end;

  SetGridValue (TAG_ROW_MHz, col_Old.index, FLinkEndInfoRec.Freq_Spacing_MHz);
  SetGridValue (TAG_ROW_nF,  col_Old.index, FLinkEndInfoRec.CHANNEL_SPACING);


//  iStatus:=AsInteger(dmLink_calc.GetCalcParamValue(DEF_STATUS));
  sStatus:=IIF(FLinkInfoRec.Status=1, '�����', '�� �����');

  SetGridValue (TAG_ROW_capability, col_Old.index, sStatus);
  SetGridValue (TAG_ROW_SESR,       col_Old.index, FLinkInfoRec.SESR);


//  dmLink_calc.OpenData(aLinkID);
  Result := True;
end;


//-------------------------------------------------------------------
procedure Tdlg_Link_Optimize_freq_diversity.FormCreate(Sender: TObject);
//-------------------------------------------------------------------
begin
  inherited;

  ed_ChannelWidth1.Text := '';
  ed_ChannelCount1.Text := '';

  Caption:='����������� ���������� �������';

  SetActionName('����������� ���������� �������');

  btn_Ok.Visible:=False;

  cxGrid1.Align:=alClient;

  with TRegIniFile.Create(FRegPath) do
  begin
    //���������� �������
    SetGridValue(0, col_max.Index,  ReadInteger('1','MHz',100));
    SetGridValue(1, col_max.Index,  ReadInteger('2','nF', 0));

    Free;
  end;

  lb_ChannelCount.Caption:='���-�� �������';
  lb_ChannelWidth.Caption:='������ ������';

  FrrlCalcParam := TrrlCalcParam.Create();


  FDBManager:=TDBManager.Create (dmmain.ADOConnection);

end;


// ---------------------------------------------------------------
procedure Tdlg_Link_Optimize_freq_diversity.FormDestroy(Sender: TObject);
// ---------------------------------------------------------------
begin
  FreeAndNil(FrrlCalcParam);

  FreeAndNil(FDBManager);


  with TRegIniFile.Create(FRegPath) do
  begin
    WriteInteger('1','MHz',  GetGridValue(0, col_max.Index));
    WriteInteger('2','nF',   GetGridValue(1, col_max.Index));

    Free;
  end;
  inherited;
end;


procedure Tdlg_Link_Optimize_freq_diversity.cxGrid1Exit(Sender: TObject);
begin
  cxGrid1TableView1.DataController.PostEditingData;
end;


procedure Tdlg_Link_Optimize_freq_diversity.cxGrid1TableView1Editing(Sender:
    TcxCustomGridTableView; AItem: TcxCustomGridTableItem; var AAllow: Boolean);
begin
//  AAllow := Sender.DataController.FocusedRecordIndex in [0..2];

end;



procedure Tdlg_Link_Optimize_freq_diversity.act_OptimizeExecute(Sender:  TObject);
begin
  Optimize();
end;



function Tdlg_Link_Optimize_freq_diversity.GetGridValue(aRow, aColIndex:
    Integer): Variant;
begin
  Result:=cxGrid1TableView1.ViewData.Rows[aRow].Values[aColIndex];
end;

//--------------------------------------------------------------------
procedure Tdlg_Link_Optimize_freq_diversity.Optimize;
//--------------------------------------------------------------------
var sFile1,sFile2: string;
  d: double;
  eNew_nF: double;
  I: integer;
  sStatus: string;
  iStatus: integer;
//  rec: TrrlParamRec;
   eNew_StepF: Double;
  sErrName: string;

  oParams: TIni_Link_Calc_Params;
  sFile_layout: string;

const
  TEMP_IN_FILENAME = 'rrl.xml';
  TEMP_OUT_FILENAME = 'rrl_result.xml';

  TEMP_IN_layout  = 'layout.xml';


const
  DEF_POWER    = 101; //��������� ������� ������� �� ����� ��������� (���)
  DEF_SESR     = 17;

  DEF_STATUS   = 7;  //����������� ���������



{
begin
  with Tdlg_Link_Optimize_freq_diversity.Create(Application) do
  begin
    FParams:= aParams;

    FLinkID:=aLinkID;

//    TdmLink_calc.Init;

//    oLink:=dmLink_calc.LinkList.AddItem;

//    dmLink_calc.OpenData_new(aID, oLink);}



begin
//  sFile1:=GetTempFileName_('xml');

  sFile1:=g_ApplicationDataDir + TEMP_IN_FILENAME;
  sFile2:=g_ApplicationDataDir + TEMP_OUT_FILENAME;

  sFile_layout :=g_ApplicationDataDir + TEMP_IN_layout;


 {
  if not FIsLoaded then
  begin
   // dmLink_calc.Params.IsUsePassiveElements := FParams.UsePassiveElements;


    TdmLink_calc.init;
    oLink:=dmLink_calc.LinkList.AddItem;

    dmLink_calc.OpenData_new(FLinkID, oLink);

    dmLink_calc.

 //   dmLink_calc.OpenData(FLinkID);
  //  dmLink_calc.SaveToclass1(FrrlCalcParam);

    FIsLoaded := True;
  end;
  }

  TDBLink_export.SaveToClass(FDBLink, FDBLink.LinkEnd1, FDBLink.LinkEnd2,
      FrrlCalcParam, FDBLink.relProfile, FDBLink.IsProfileExists);




  FrrlCalcParam.TTX.Freq_Spacing_MHz :=  - Abs (AsFloat(GetGridValue(TAG_ROW_MHz, col_Max.Index)) );


  CalcMethod_LoadFromXMLFile(
      GetApplicationDir + 'link_calc_model_setup\RRL_ParamSys.xml',
      FrrlCalcParam.Calc_method,
      sFile_layout
      );


  FrrlCalcParam.Result_FileName:=sFile2;
  FrrlCalcParam.Layout_FileName:=sFile_layout;

  if FrrlCalcParam.ValidateDlg then
    FrrlCalcParam.SaveToXML (sFile1, True)
  else
    Exit;


  

{
  if not FrrlCalcParam.Validate then
  begin
    ShowMessage(FrrlCalcParam.ErrorMsg);
    Exit;
  end else
    FrrlCalcParam.SaveToXML (sFile1, True);
}

  //---------------------------------------------------------

   //dmLink_calc.Run_rpls_rrl(aFile1, aFile2: string);

  if RunApp (GetApplicationDir() + EXE_RPLS_RRL,
             DoubleQuotedStr(sFile1) +' '+
             DoubleQuotedStr(sFile2) +' '+
             DoubleQuotedStr(sFile_layout)
             ) then

  begin

{
   ShellExec_Notepad1( sFile1 );
    ShellExec_Notepad1( sFile2 );
}

//    dmLink_calc.LinkCalcResults.LoadCalcResultsFromXML (sFile2);
    dmLink_calc.LinkCalcResults.LoadFromXmlFile_RRL_Result (sFile2);
//    dmLink_calc.LoadCalcResultsFromXML_ToRec (sFile2);

//ind:= Groups.AddItem('19','19. ���.������� ����������������� ������������ ��������-���');

    eNew_StepF:=dmLink_calc.LinkCalcResults.Groups.GetCalcParamValueByID(175);

//    eNew_StepF:=;


    Assert(FChannelWidth>0, 'ChannelWidth <=0');

    eNew_nF :=Ceil(eNew_StepF/FChannelWidth);   //FChannelWidth - ��� ����� ������

 ///   eNew_StepF:=eNew_nF * FChannelWidth;

//      FStepF:=FChannelWidth;  //��� ����� ������


    SetGridValue (TAG_ROW_MHz, col_New.index, eNew_StepF);
    SetGridValue (TAG_ROW_nF,  col_New.index, eNew_nF);

//    if FLinkEndInfoRec.EquipmentType=1 then    //������������: 0-�������, 1-���������
 //     SetGridValue (TAG_ROW_MHz, col_New.index, d);

    if FLinkEndInfoRec.EquipmentType=0 then    //������������: 0-�������, 1-���������
    begin
//      SetGridValue (TAG_ROW_nF,  col_Max.index, FChannelCount-1);
 //     SetGridValue (TAG_ROW_MHz, col_Max.index, (FChannelCount-1)*FChannelWidth);
    end;


    d:=dmLink_calc.LinkCalcResults.Data.SESR;

    SetGridValue(TAG_ROW_SESR, col_New.Index, d);
//    SetGridValue(TAG_ROW_SESR, col_New.Index, dmLink_calc.GetCalcParamValue(DEF_SESR));

    iStatus:=dmLink_calc.LinkCalcResults.Data.STATUS;
//    iStatus:=AsInteger(dmLink_calc.GetCalcParamValue(DEF_STATUS));
//    sStatus:=IIF(iStatus=1, '�����', '�� �����');

    sStatus:=GetWorkingTypeStr(iStatus);

    SetGridValue(TAG_ROW_capability, col_New.Index, sStatus);
  end;
end;


//--------------------------------------------------------------------
function Tdlg_Link_Optimize_freq_diversity.GetWorkingTypeStr(aValue: integer):
    string;
//--------------------------------------------------------------------
begin
  case aValue of
    1: Result:='�����';
    2: Result:='�� �����';
    else
      Result:='?';
  end;
end;

//-------------------------------------------------------------------
procedure Tdlg_Link_Optimize_freq_diversity.Save;
//-------------------------------------------------------------------
var
  eFreqSpacing,nF: Double;
begin

  nF:=GetGridValue (TAG_ROW_nF,  col_New.index);
  eFreqSpacing:=nF * FChannelWidth;

  dmLinkEnd.Update_(FLinkInfoRec.LinkEnd1_ID,
                  [db_Par(FLD_FREQ_SPACING,    eFreqSpacing),
                   db_Par(FLD_CHANNEL_SPACING, nF) ]);

  dmLinkEnd.Update_(FLinkInfoRec.LinkEnd2_ID,
                  [db_Par(FLD_FREQ_SPACING,    eFreqSpacing),
                   db_Par(FLD_CHANNEL_SPACING, nF) ]);

end;


procedure Tdlg_Link_Optimize_freq_diversity.act_SaveExecute(Sender: TObject);
begin
  Save();
end;


procedure Tdlg_Link_Optimize_freq_diversity.SetGridValue(aRow, aColIndex:
    Integer; aValue: Variant);
begin
  cxGrid1TableView1.ViewData.Rows[aRow].Values[aColIndex]:=aValue;
end;




end.




(*

  FDBManager:=TDBManager.Create (dmmain.ADOConnection);

  FreeAndNil(FDBManager);
*)

