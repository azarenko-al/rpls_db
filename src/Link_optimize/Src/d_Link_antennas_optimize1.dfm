object dlg_Link_antennas_optimize1: Tdlg_Link_antennas_optimize1
  Left = 1149
  Top = 354
  BorderStyle = bsDialog
  Caption = 'dlg_Link_antennas_optimize1'
  ClientHeight = 418
  ClientWidth = 654
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object cxGrid1: TcxGrid
    Left = 0
    Top = 0
    Width = 654
    Height = 201
    Align = alTop
    TabOrder = 0
    OnExit = cxGrid1Exit
    LookAndFeel.Kind = lfFlat
    object cxGrid1BandedTableView1: TcxGridBandedTableView
      NavigatorButtons.ConfirmDelete = False
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      DataController.Data = {
        160200000F00000044617461436F6E74726F6C6C657231060000001200000054
        6378537472696E6756616C75655479706512000000546378537472696E675661
        6C75655479706513000000546378496E746567657256616C7565547970651200
        0000546378537472696E6756616C75655479706512000000546378537472696E
        6756616C75655479706512000000546378537472696E6756616C756554797065
        08000000445841464D5400001A000000CFE5F0E5E4E0FEF9E0FF20E0EDF2E5ED
        EDE02028F1EBE5E2E029000400000054727565002C000000010101445841464D
        54000022000000CEF1EDEEE2EDE0FF20EFF0E8E5ECEDE0FF20E0EDF2E5EDEDE0
        2028F1EFF0E0E2E0290004000000547275650021000000010101445841464D54
        000028000000C4EEEFEEEBEDE8F2E5EBFCEDE0FF20EFF0E8E5ECEDE0FF20E0ED
        F2E5EDEDE02028F1EFF0E0E2E029000400000054727565004200000001010144
        5841464D54000029000000C4EEEFEEEBEDE8F2E5EBFCEDE0FF20EFE5F0E5E4E0
        FEF9E0FF20E0EDF2E5EDEDE02028F1EBE5E2E029010042000000010101445841
        464D5400000100000020000500000046616C736501010101445841464D540000
        0B000000CFF0E8E3EEE4EDEEF1F2FC000500000046616C736501010101445841
        464D5400000400000053455352000500000046616C736501010101445841464D
        54000005000000CAEDE32EE8000500000046616C736501010101}
      OptionsCustomize.ColumnFiltering = False
      OptionsCustomize.ColumnMoving = False
      OptionsData.Deleting = False
      OptionsView.CellAutoHeight = True
      OptionsView.GroupByBox = False
      OptionsView.HeaderAutoHeight = True
      OptionsView.Indicator = True
      OptionsView.BandHeaders = False
      Bands = <
        item
          Width = 571
        end>
      object col_Name: TcxGridBandedColumn
        Caption = #1040#1085#1090#1077#1085#1085#1072
        Options.Editing = False
        Width = 271
        Position.BandIndex = 0
        Position.ColIndex = 0
        Position.RowIndex = 0
      end
      object col_Check: TcxGridBandedColumn
        Caption = #1054#1087#1090#1080#1084#1080#1079#1080#1088#1086#1074#1072#1090#1100
        PropertiesClassName = 'TcxCheckBoxProperties'
        Width = 96
        Position.BandIndex = 0
        Position.ColIndex = 2
        Position.RowIndex = 0
      end
      object col_H_max: TcxGridBandedColumn
        Caption = #1042#1099#1089#1086#1090#1072' max [m]'
        DataBinding.ValueType = 'Integer'
        PropertiesClassName = 'TcxSpinEditProperties'
        Width = 53
        Position.BandIndex = 0
        Position.ColIndex = 1
        Position.RowIndex = 0
      end
      object col_H_new: TcxGridBandedColumn
        Caption = #1042#1099#1089#1086#1090#1072' '#1085#1086#1074#1072#1103' [m]'
        Width = 70
        Position.BandIndex = 0
        Position.ColIndex = 4
        Position.RowIndex = 0
      end
      object col_H_old: TcxGridBandedColumn
        Caption = #1042#1099#1089#1086#1090#1072' '#1087#1088#1077#1078#1085#1103#1103' [m]'
        Options.Editing = False
        Width = 81
        Position.BandIndex = 0
        Position.ColIndex = 3
        Position.RowIndex = 0
      end
      object col_ID: TcxGridBandedColumn
        Caption = 'id'
        Visible = False
        Options.Editing = False
        Width = 43
        Position.BandIndex = 0
        Position.ColIndex = 5
        Position.RowIndex = 0
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1BandedTableView1
    end
  end
  object rg_Criteria: TRadioGroup
    Left = 5
    Top = 209
    Width = 305
    Height = 60
    Caption = #1050#1088#1080#1090#1077#1088#1080#1081' '#1086#1087#1090#1080#1084#1080#1079#1072#1094#1080#1080' '#1074#1099#1089#1086#1090' 2-'#1093' '#1072#1085#1090#1077#1085#1085
    ItemIndex = 0
    Items.Strings = (
      'min max (H1, H2)'
      'min (H1 + H2)')
    TabOrder = 1
  end
  object rg_Dop_ant: TRadioGroup
    Left = 5
    Top = 277
    Width = 305
    Height = 60
    Caption = #1050#1088#1080#1090#1077#1088#1080#1081' '#1086#1087#1090#1080#1084#1080#1079#1072#1094#1080#1080' '#1074#1099#1089#1086#1090#1099' '#1076#1086#1087'.'#1072#1085#1090#1077#1085#1085#1099' ('#1089#1087#1088#1072#1074#1072')'
    ItemIndex = 0
    Items.Strings = (
      'min SESR'
      'min '#1050#1085#1075'.'#1080)
    TabOrder = 2
  end
  object pn_Buttons: TPanel
    Left = 0
    Top = 376
    Width = 654
    Height = 42
    Align = alBottom
    BevelOuter = bvNone
    Constraints.MaxHeight = 42
    Constraints.MinHeight = 42
    TabOrder = 3
    object Bevel1: TBevel
      Left = 0
      Top = 0
      Width = 654
      Height = 2
      Align = alTop
      Shape = bsTopLine
    end
    object Button2: TButton
      Left = 92
      Top = 8
      Width = 193
      Height = 23
      Action = act_Save
      ModalResult = 1
      TabOrder = 0
    end
    object Button1: TButton
      Left = 4
      Top = 8
      Width = 75
      Height = 23
      Action = act_Calc
      TabOrder = 1
    end
    object Panel1: TPanel
      Left = 568
      Top = 2
      Width = 86
      Height = 40
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 2
      object btn_Cancel: TButton
        Left = 6
        Top = 6
        Width = 75
        Height = 23
        Cancel = True
        Caption = #1047#1072#1082#1088#1099#1090#1100
        ModalResult = 2
        TabOrder = 0
      end
    end
  end
  object ActionList1: TActionList
    Left = 374
    Top = 225
    object act_Calc: TAction
      Caption = #1056#1072#1089#1095#1077#1090
      OnExecute = act_CalcExecute
    end
    object act_Save: TAction
      Caption = #1059#1089#1090#1072#1085#1086#1074#1080#1090#1100' '#1085#1086#1074#1099#1077' '#1074#1099#1089#1086#1090#1099
      OnExecute = act_CalcExecute
    end
  end
  object FormStorage1: TFormStorage
    IniFileName = 'Software\Onega\'
    StoredProps.Strings = (
      'rg_Criteria.ItemIndex'
      'rg_Dop_ant.ItemIndex')
    StoredValues = <>
    Left = 373
    Top = 280
  end
  object dxMemData1: TdxMemData
    Active = True
    Indexes = <>
    Persistent.Data = {
      5665728FC2F5285C8FFE3F0300000032000000010005006E616D650004000000
      03000B006865696768745F6D6178000200000005000800636865636B65640001
      1A000000CFE5F0E5E4E0FEF9E0FF20E0EDF2E5EDEDE02028F1EBE5E2E0290000
      0000000000000122000000CEF1EDEEE2EDE0FF20EFF0E8E5ECEDE0FF20E0EDF2
      E5EDEDE02028F1EFF0E0E2E0290000000000000000011F000000C4EEEFEEEBED
      E8F2E5EBFCEDE0FF20E0EDF2E5EDEDE02028F1EFF0E0E2E02900000000000000
      00011E000000C4EEEFEEEBEDE8F2E5EBFCEDE0FF20E0EDF2E5EDEDE02028F1EB
      E5E2E0290000000000000000010200000020200000000000000000010B000000
      CFF0E8E3EEE4EDEEF1F2FC000000000000000001040000005345535200000000
      000000000105000000CAEDE32EE80000000000000000}
    SortOptions = []
    Left = 520
    Top = 224
    object dxMemData1name: TStringField
      FieldName = 'name'
      Size = 50
    end
    object dxMemData1height_max: TIntegerField
      FieldName = 'height_max'
    end
    object dxMemData1checked: TBooleanField
      FieldName = 'checked'
    end
  end
  object DataSource1: TDataSource
    DataSet = dxMemData1
    Left = 520
    Top = 288
  end
end
