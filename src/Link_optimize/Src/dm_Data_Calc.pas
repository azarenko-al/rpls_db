unit dm_Data_Calc;

interface

uses
  SysUtils, Classes, DB, ADODB, Forms,


 u_LinkFreqPlan_classes,
 u_const_db,

//  u_const_msg,
  u_LinkFreqPlan_const,

  dm_Main,

  u_Link_const,

  u_db,

  u_func,


  dm_Onega_DB_data


  ;

type
  TdmData_Calc = class(TDataModule)
    ADOConnection1: TADOConnection;
  private
    { Private declarations }
  public

    class procedure Init;

    procedure OpenData;

  end;

var
  dmData_Calc: TdmData_Calc;

implementation

{$R *.dfm}


// ---------------------------------------------------------------
class procedure TdmData_Calc.Init;
// ---------------------------------------------------------------
begin
  if not Assigned(dmData_Calc) then
    dmData_Calc := TdmData_Calc.Create(Application);

end;


procedure TdmData_Calc.OpenData;
begin

end;

end.
