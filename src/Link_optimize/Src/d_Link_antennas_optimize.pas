unit d_Link_antennas_optimize;

interface

uses
  SysUtils, Variants, Classes, Controls, Forms,
  cxGridCustomTableView, cxGridTableView, ActnList, rxPlacemnt,
  cxGridLevel, cxGrid, cxGridBandedTableView, StdCtrls, ExtCtrls, Registry,

  dm_Main,

  u_LinkCalcResult,


  u_vars,

  u_const,
  u_const_db,

  u_db,
  u_func,
  
  u_run,

  u_rrl_param_rec_new,

  u_ini_Link_optimize_params,


  dm_Link_calc,


   cxClasses, cxControls, cxGridCustomView
  ;

type
  Tdlg_Link_antennas_optimize1111111111 = class(TForm)
    ActionList1: TActionList;
    act_Calc: TAction;
    act_Save: TAction;
    Bevel1: TBevel;
    Button1: TButton;
    Button2: TButton;
    col_Check: TcxGridBandedColumn;
    col_H_max: TcxGridBandedColumn;
    col_H_new: TcxGridBandedColumn;
    col_H_old: TcxGridBandedColumn;
    col_ID: TcxGridBandedColumn;
    col_Name: TcxGridBandedColumn;
    cxGrid1: TcxGrid;
    cxGrid1BandedTableView1: TcxGridBandedTableView;
    cxGrid1Level1: TcxGridLevel;
    FormStorage1: TFormStorage;
    pn_Buttons: TPanel;
    rg_Criteria: TRadioGroup;
    rg_Dop_ant: TRadioGroup;
    Panel1: TPanel;
    btn_Cancel: TButton;
    procedure act_CalcExecute(Sender: TObject);
    procedure cxGrid1BandedTableView1Editing(Sender: TcxCustomGridTableView; AItem: TcxCustomGridTableItem; var AAllow:
        Boolean);
    procedure cxGrid1Exit(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
//    FblVector: TblVector;
    FIsChanged: Boolean;
    FParams: TIni_Link_Optimize_Params;
    FRegPath: string;
    FrrlCalcParamRec: TrrlCalcParam;

    procedure ClearRow(aRowIndex: Integer);
    function Execute: Integer;
    function GetGridValue(aRow, aColIndex: Integer): Variant;
    function GetWorkingTypeStr(aStatus: integer): string;
    procedure Load;
    procedure SaveHeights;
    procedure SetGridValue(aRow, aColIndex: Integer; aValue: Variant);
  public
    class function ExecDlg(aID: Integer; aParams: TIni_Link_Optimize_Params):
        Boolean;
  end;


implementation

 {$R *.dfm}


const
  REGISTRY_COMMON_FORMS  = 'Software\Onega\Common\Forms\';

  TAG_ROW_TX_ANTENNA    = 0;
  TAG_ROW_RX_ANTENNA    = 1;
  TAG_ROW_RX_ANTENNA_DOP= 2;

  TAG_ROW_capability    = 4;   //�����������
  TAG_ROW_SESR          = 5;   //SESR
  TAG_ROW_KNG           = 6;   //��� �



//-------------------------------------------------------------------
class function Tdlg_Link_antennas_optimize1111111111.ExecDlg(aID: Integer; aParams:
    TIni_Link_Optimize_Params): Boolean;
//-------------------------------------------------------------------
begin
  with Tdlg_Link_antennas_optimize.Create(Application) do
  begin
    FParams:=aParams;

    TdmLink_calc.Init;

  //  dmLink_calc.Params.Repeater.Active := False;
    dmLink_calc.Params.IsUsePassiveElements := FParams.UsePassiveElements;
    dmLink_calc.OpenData(aID);

    Load();

    Result := ShowModal = mrOk;

//    Result := FIsChanged;

    Free;
  end;
end;

//-------------------------------------------------------------------
procedure Tdlg_Link_antennas_optimize1111111111.FormCreate(Sender: TObject);
//-------------------------------------------------------------------
begin
  inherited;

  FRegPath:= REGISTRY_COMMON_FORMS + ClassName +'\';

  Caption:='����������� ����� ������';


  with TRegIniFile.Create(FRegPath) do
  begin

    //���������� �������
    SetGridValue(0, col_H_max.Index,  ReadInteger('1','H_max',30));
    SetGridValue(1, col_H_max.Index,  ReadInteger('2','H_max',30));
    SetGridValue(2, col_H_max.Index,  ReadInteger('3','H_max',30));

    SetGridValue(0, col_Check.Index,  ReadBool('1','check',False));
    SetGridValue(1, col_Check.Index,  ReadBool('2','check',False));
    SetGridValue(2, col_Check.Index,  ReadBool('3','check',False));

    rg_Criteria.ItemIndex:=ReadInteger('',rg_Criteria.Name,0);

    Free;
  end;

{
  Assert(not cxPropertiesStore.Active);

  cxPropertiesStore.StorageName:=FRegPath+'cxPropertiesStore';
  cxPropertiesStore.Active := True;}

  FrrlCalcParamRec := TrrlCalcParam.Create();

end;

//-------------------------------------------------------------------
procedure Tdlg_Link_antennas_optimize1111111111.FormDestroy(Sender: TObject);
//-------------------------------------------------------------------
begin
  FreeAndNil(FrrlCalcParamRec);

  with TRegIniFile.Create(FRegPath) do
  begin

    WriteInteger('1','H_max',  AsInteger(GetGridValue(0, col_H_max.Index)));
    WriteInteger('2','H_max',  AsInteger(GetGridValue(1, col_H_max.Index)));
    WriteInteger('3','H_max',  AsInteger(GetGridValue(2, col_H_max.Index)));

    WriteBool('1','check', AsBoolean(GetGridValue(0, col_Check.Index)));
    WriteBool('2','check', AsBoolean(GetGridValue(1, col_Check.Index)));
    WriteBool('3','check', AsBoolean(GetGridValue(2, col_Check.Index)));

    WriteInteger('', rg_Criteria.Name, rg_Criteria.ItemIndex);

    Free;

  end;

  FreeAndNil(dmLink_calc);

end;



// ---------------------------------------------------------------
procedure Tdlg_Link_antennas_optimize1111111111.act_CalcExecute(Sender: TObject);
// ---------------------------------------------------------------
begin
  if Sender=act_Save then
    SaveHeights;

  if Sender=act_Calc then
    Execute;

end;


procedure Tdlg_Link_antennas_optimize1111111111.ClearRow(aRowIndex: Integer);
var I: integer;
begin
  for I := 0 to cxGrid1BandedTableView1.ColumnCount - 1 do
    cxGrid1BandedTableView1.ViewData.Rows[aRowIndex].Values[i]:=null;
end;


procedure Tdlg_Link_antennas_optimize1111111111.cxGrid1BandedTableView1Editing(Sender: TcxCustomGridTableView; AItem:
    TcxCustomGridTableItem; var AAllow: Boolean);
begin
  AAllow := Sender.DataController.FocusedRecordIndex in [0..2];

{
  with Sender.DataController.FocusedRecordIndex do
    if (Values[FocusedRecordIndex,col_ID.Index] = false) and ((AItem.Index = <AMyColumn>.Index) then
      AAllow := False
    else
      AAllow := True;
}

{  cxGrid1BandedTableView1.OptionsSelection


  AAllow:=AItem.
}
//  cxGrid1BandedTableView1.
//  AItem.FocusedCellViewInfo.
//  cxGrid1BandedTableView1.
end;

procedure Tdlg_Link_antennas_optimize1111111111.cxGrid1Exit(Sender: TObject);
begin
  cxGrid1BandedTableView1.DataController.PostEditingData;
end;


//--------------------------------------------------------------------
function Tdlg_Link_antennas_optimize1111111111.Execute: Integer;
//--------------------------------------------------------------------
var sFile1,sFile2: string;
  I: integer;
  sStatus: string;
  iStatus: integer;
  eSESR: double;
  eKNG: double;
  sErrStr: string;


//

const
  TEMP_IN_FILENAME = 'rrl.xml';
  TEMP_OUT_FILENAME = 'rrl_result.xml';


const
  DEF_POWER    = 101; //��������� ������� ������� �� ����� ��������� (���)
  DEF_SESR     = 17;
  DEF_KNG      = 151;

  DEF_STATUS   = 7;  //����������� ���������

begin
//  sFile1:=GetTempFileName_('xml');

  sFile1:=g_ApplicationDataDir + TEMP_IN_FILENAME;
  sFile2:=g_ApplicationDataDir + TEMP_OUT_FILENAME;


  dmLink_calc.Params.IsUsePassiveElements:=FParams.UsePassiveElements;
  dmLink_calc.Params.IsCalcWithAdditionalRain:=True;

  dmLink_calc.SaveToclass(FrrlCalcParamRec);


  if AsBoolean(GetGridValue(TAG_ROW_TX_ANTENNA, col_Check.Index)) then
    FrrlCalcParamRec.TTX.Site1_HEIGHT:=- AsInteger(GetGridValue(TAG_ROW_TX_ANTENNA, col_H_max.Index));

  if AsBoolean(GetGridValue(TAG_ROW_RX_ANTENNA, col_Check.Index)) then
    FrrlCalcParamRec.TTX.Site2_HEIGHT:=- AsInteger(GetGridValue(TAG_ROW_RX_ANTENNA, col_H_max.Index));

  if AsBoolean(GetGridValue(TAG_ROW_RX_ANTENNA_DOP, col_Check.Index)) then
    FrrlCalcParamRec.TTX.Dop_Site2.HEIGHT:=- AsInteger(GetGridValue(TAG_ROW_RX_ANTENNA_DOP, col_H_max.Index));



  //�������������� ����� //���������� ����� ��������� �� ������
  FrrlCalcParamRec.TTX.AntennaOffsetHor:=0;

//  FrrlCalcParamRec.TTX.AntennaOffsetHor:=geo_Distance(FblVector);

  FrrlCalcParamRec.Optimization.OptimizeEnabled :=True;

  case rg_Criteria.ItemIndex of
    0:  FrrlCalcParamRec.Optimization.OptimizeKind:=otMinMax_H1_H2;
    1:  FrrlCalcParamRec.Optimization.OptimizeKind:=otMin_H1_plus_H2;
  end;


  case rg_Dop_ant.ItemIndex of
    0:  FrrlCalcParamRec.Optimization.Criteria_opti_dop_antenna:=otMin_SESR;
    1:  FrrlCalcParamRec.Optimization.Criteria_opti_dop_antenna:=otMin_KNG;
  end;


//  FrrlCalcParamRec.TTX.Criteria_opti_dop_antenna


  if FrrlCalcParamRec.ValidateDlg then
    FrrlCalcParamRec.SaveToXML (sFile1, True)
  else
    Exit;

{
  if not FrrlCalcParamRec.Validate then
  begin
    ShowMessage(FrrlCalcParamRec.ErrorMsg);
    exit;
  end else
    FrrlCalcParamRec.SaveToXML (sFile1, True);
}

  if RunApp (GetApplicationDir() + EXE_RPLS_RRL,
             DoubleQuotedStr(sFile1) +' '+
             DoubleQuotedStr(sFile2)) then

  begin
    dmLink_calc.LinkCalcResults.Clear;

    dmLink_calc.LinkCalcResults.LoadFromXmlFile_RRL_Result(sFile2);
    // LoadCalcResultsFromXML_ToRec (sFile2);
//    dmLink_calc.LoadCalcResultsFromXML_ToRec (sFile2);
//    LoadCalcResultsFromXML (FLinkID, sFile2);

    for I := 0 to cxGrid1BandedTableView1.DataController.RowCount - 1 do
    begin
      SetGridValue(i, col_H_new.Index, '');
    end;

    if AsBoolean(GetGridValue(TAG_ROW_TX_ANTENNA, col_Check.Index)) then
      SetGridValue(TAG_ROW_TX_ANTENNA, col_H_new.Index, dmLink_calc.LinkCalcResults.Data.Optimize.TX_ANTENNA_H);
//      SetGridValue(TAG_ROW_TX_ANTENNA, col_H_new.Index, dmLink_calc.GetCalcParamValue(3));

    if AsBoolean(GetGridValue(TAG_ROW_RX_ANTENNA, col_Check.Index)) then
      SetGridValue(TAG_ROW_RX_ANTENNA, col_H_new.Index, dmLink_calc.LinkCalcResults.Data.Optimize.RX_ANTENNA_H);
//      SetGridValue(TAG_ROW_RX_ANTENNA, col_H_new.Index, dmLink_calc.GetCalcParamValue(4));

    if AsBoolean(GetGridValue(TAG_ROW_RX_ANTENNA_DOP, col_Check.Index)) then
      SetGridValue(TAG_ROW_RX_ANTENNA_DOP, col_H_new.Index, dmLink_calc.LinkCalcResults.Data.Optimize.RX_ANTENNA_DOP_H);
//      SetGridValue(TAG_ROW_RX_ANTENNA_DOP, col_H_new.Index, dmLink_calc.GetCalcParamValue(6));


    //DEF_SESR := 17;
    eSESR := dmLink_calc.LinkCalcResults.Data.SESR;
    // GetCalcParamValue(DEF_SESR);
//    eSESR := dmLink_calc.GetCalcParamValue(DEF_SESR);
    SetGridValue(TAG_ROW_SESR, col_H_new.Index, eSESR);

    iStatus:=dmLink_calc.LinkCalcResults.Data.STATUS;
//    iStatus:=AsInteger(dmLink_calc.GetCalcParamValue(DEF_STATUS));
    sStatus:=IIF(iStatus=1, '�����', '�� �����');

    SetGridValue(TAG_ROW_capability, col_H_new.Index, sStatus);

    //DEF_KNG := 151;
    eKNG := dmLink_calc.LinkCalcResults.Data.Kng;
//    eKNG := dmLink_calc.GetCalcParamValue(DEF_KNG);
    SetGridValue(TAG_ROW_KNG, col_H_new.Index, eKNG);

  end;
end;


function Tdlg_Link_antennas_optimize1111111111.GetGridValue(aRow, aColIndex: Integer): Variant;
begin
  Result:=cxGrid1BandedTableView1.ViewData.Rows[aRow].Values[aColIndex];
end;

//--------------------------------------------------------------------
function Tdlg_Link_antennas_optimize1111111111.GetWorkingTypeStr(aStatus: integer):
    string;
//--------------------------------------------------------------------
begin
  case aStatus of
    1: Result:='�����';
    2: Result:='�� �����';
    else
      Result:='?';
  end;
end;

//-------------------------------------------------------------------
procedure Tdlg_Link_antennas_optimize1111111111.Load;
//-------------------------------------------------------------------

var
  sStatus: string;
  iStatus: integer;
begin


 // dmMain.ProjectID:=470;
 // dmLink_calc.OpenData(46401);

// db_ViewDataSet(dmLink_calc.qry_Link);


//  db_ViewDataSet(dmLink_calc.qry_Antennas1);
  SetGridValue (TAG_ROW_SESR, col_H_old.Index, dmLink_calc.ADOStoredProc_Link.FieldByName(FLD_SESR).AsFloat);
  SetGridValue (TAG_ROW_KNG, col_H_old.Index, dmLink_calc.ADOStoredProc_Link.FieldByName(FLD_KNG).AsFloat);

  iStatus:=dmLink_calc.ADOStoredProc_Link.FieldByName(FLD_STATUS).AsInteger;
  sStatus:=GetWorkingTypeStr(iStatus); //dmLink.

  SetGridValue (TAG_ROW_capability, col_H_old.Index, sStatus);

  with dmLink_calc.qry_Antennas1 do
  begin
    First;

    SetGridValue (0, col_H_old.Index, FieldByName(FLD_HEIGHT).AsFloat);
    SetGridValue (0, col_ID.Index,    FieldByName(FLD_ID).AsInteger);

//    SetGridValue (0, col_H_old.Index);

  end;


//  FillChar(FblVector, SizeOf(FblVector), 0);


  with dmLink_calc.qry_Antennas2 do
  begin
    First;

//    FblVector.Point1:=db_ExtractBLPoint(dmLink_calc.qry_Antennas2);

    SetGridValue (1, col_H_old.Index, FieldByName(FLD_HEIGHT).AsFloat);
    SetGridValue (1, col_ID.Index,    FieldByName(FLD_ID).AsInteger);

    if RecordCount>1 then
    begin
      Next;

//      FblVector.Point2:=db_ExtractBLPoint(dmLink_calc.qry_Antennas2);

      SetGridValue (2, col_H_old.Index, FieldByName(FLD_HEIGHT).AsFloat);
      SetGridValue (2, col_ID.Index,    FieldByName(FLD_ID).AsInteger);

    end else
      ClearRow(2);

  end;


end;

// ---------------------------------------------------------------
procedure Tdlg_Link_antennas_optimize1111111111.SaveHeights;
// ---------------------------------------------------------------
var
  b: Boolean;
  iID,i: integer;
  eH: double;
begin
  for I := 0 to 2 do
  begin
    eH :=AsFloat(GetGridValue(i, col_H_new.Index));
    iID:=AsInteger(GetGridValue(i, col_ID.Index));

    b:=AsBoolean(GetGridValue(i, col_Check.Index));


    if b and (eH>0) then
      gl_DB.UpdateRecord(TBL_LINKEND_ANTENNA, iID,
                        [db_Par(FLD_HEIGHT, eH)]);
  end;


  FIsChanged:=True;
end;

procedure Tdlg_Link_antennas_optimize1111111111.SetGridValue(aRow, aColIndex: Integer; aValue: Variant);
begin
  cxGrid1BandedTableView1.ViewData.Rows[aRow].Values[aColIndex]:=aValue;
end;









end.


{
TcxCustomGridTableItem; var AAllow: Boolean);
begin
  with Sender.DataController do
  if (Values[FocusedRecordIndex,<ACheckBoxColumn>.Index] = false) and ((AItem.Index = <AMyColumn>.Index) then
    AAllow := False
  else
    AAllow := True;
end;


