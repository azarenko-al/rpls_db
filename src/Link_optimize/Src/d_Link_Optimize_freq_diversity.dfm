inherited dlg_Link_Optimize_freq_diversity: Tdlg_Link_Optimize_freq_diversity
  Left = 1107
  Top = 468
  VertScrollBar.Range = 0
  BorderIcons = [biSystemMenu, biMaximize]
  BorderStyle = bsDialog
  Caption = 'dlg_Link_Optimize_freq_diversity'
  ClientHeight = 450
  ClientWidth = 509
  OldCreateOrder = True
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object Label3: TLabel [0]
    Left = 8
    Top = 8
    Width = 67
    Height = 13
    Caption = 'ChannelWidth'
  end
  inherited pn_Buttons: TPanel
    Top = 415
    Width = 509
    inherited Bevel1: TBevel
      Width = 509
    end
    inherited Panel3: TPanel
      Left = 330
      inherited btn_Ok: TButton
        Left = 399
        Top = 9
        Width = 19
        Action = act_Optimize
        ModalResult = 0
        Visible = False
      end
      inherited btn_Cancel: TButton
        Left = 430
      end
    end
    object Button1: TButton
      Left = 174
      Top = 8
      Width = 75
      Height = 23
      Action = act_Save
      Anchors = [akTop, akRight]
      Cancel = True
      ModalResult = 2
      TabOrder = 2
    end
    object Button2: TButton
      Left = 11
      Top = 7
      Width = 152
      Height = 23
      Action = act_Optimize
      Anchors = [akTop, akRight]
      Default = True
      TabOrder = 1
    end
  end
  inherited pn_Top_: TPanel
    Width = 509
    inherited Bevel2: TBevel
      Width = 509
    end
    inherited pn_Header: TPanel
      Width = 509
    end
  end
  object cxGrid1: TcxGrid [3]
    Left = 0
    Top = 60
    Width = 509
    Height = 209
    Align = alTop
    TabOrder = 2
    OnExit = cxGrid1Exit
    LookAndFeel.Kind = lfFlat
    object cxGrid1TableView1: TcxGridTableView
      NavigatorButtons.ConfirmDelete = False
      OnEditing = cxGrid1TableView1Editing
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      DataController.Data = {
        DA0000000F00000044617461436F6E74726F6C6C657231040000001200000054
        6378537472696E6756616C75655479706512000000546378537472696E675661
        6C75655479706512000000546378537472696E6756616C756554797065120000
        00546378537472696E6756616C75655479706505000000445841464D54000003
        0000004D687A00030000003130300101445841464D540000020000006E460101
        01445841464D5400000100000020010101445841464D5400000B000000CFF0E8
        E3EEE4EDEEF1F2FC010101445841464D5400000400000053455352010101}
      OptionsCustomize.ColumnFiltering = False
      OptionsView.GroupByBox = False
      object cxGrid1TableView1Column1: TcxGridColumn
        Caption = 'Min '#1088#1072#1079#1085#1086#1089' '#1095#1072#1089#1090#1086#1090
        Options.Editing = False
        Options.Sorting = False
        Width = 115
      end
      object col_Max: TcxGridColumn
        Caption = 'Max '#1074#1086#1079#1084#1086#1078#1085#1099#1081
        Options.Sorting = False
        Width = 97
      end
      object col_Old: TcxGridColumn
        Caption = #1055#1088#1077#1078#1085#1080#1081
        Options.Editing = False
        Options.Sorting = False
        Width = 110
      end
      object col_New: TcxGridColumn
        Caption = #1053#1086#1074#1099#1081
        Options.Sorting = False
        Width = 104
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1TableView1
    end
  end
  object Panel1: TPanel [4]
    Left = 0
    Top = 306
    Width = 509
    Height = 55
    Align = alBottom
    BevelOuter = bvLowered
    TabOrder = 3
    object Label1: TLabel
      Left = 16
      Top = 8
      Width = 3
      Height = 13
    end
    object lb_ChannelWidth: TLabel
      Left = 8
      Top = 8
      Width = 67
      Height = 13
      Caption = 'ChannelWidth'
    end
    object lb_ChannelCount: TLabel
      Left = 8
      Top = 32
      Width = 67
      Height = 13
      Caption = 'ChannelCount'
    end
    object ed_ChannelWidth1: TEdit
      Left = 112
      Top = 8
      Width = 121
      Height = 21
      ParentColor = True
      ReadOnly = True
      TabOrder = 0
      Text = 'ed_ChannelWidth1'
    end
    object ed_ChannelCount1: TEdit
      Left = 112
      Top = 32
      Width = 121
      Height = 21
      ParentColor = True
      ReadOnly = True
      TabOrder = 1
      Text = 'ed_ChannelCount1'
    end
  end
  object Memo1: TMemo [5]
    Left = 0
    Top = 361
    Width = 509
    Height = 54
    Align = alBottom
    Lines.Strings = (
      #1050#1088#1080#1090#1077#1088#1080#1081' '#1086#1087#1090#1080#1084#1080#1079#1072#1094#1080#1080': '
      '  '#1085#1072#1081#1090#1080'  MIN '#1088#1072#1079#1085#1086#1089' '#1095#1072#1089#1090#1086#1090',  '#1087#1088#1080' '#1082#1086#1090#1086#1088#1086#1084' SESR <= SESR '#1090#1088#1077#1073'.')
    ParentColor = True
    TabOrder = 4
  end
  object ActList: TActionList
    Left = 133
    Top = 5
    object act_Optimize: TAction
      Caption = #1054#1087#1090#1080#1084#1080#1079#1080#1088#1086#1074#1072#1090#1100
      OnExecute = act_OptimizeExecute
    end
    object act_Save: TAction
      Caption = #1057#1086#1093#1088#1072#1085#1080#1090#1100
      OnExecute = act_SaveExecute
    end
  end
end
