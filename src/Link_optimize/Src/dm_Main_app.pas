unit dm_Main_app;

interface


uses
  SysUtils, Classes, IniFiles, Dialogs,Messages, Windows,

  u_vars,


  I_Link_optimize,

  u_Ini_Link_Optimize_params,

  d_Link_Optimize_freq_diversity,
 // d_Link_antennas_optimize,
  d_Link_antennas_optimize1,

  dm_Main

  ;

type
  TdmMain_app = class(TDataModule)

    procedure DataModuleCreate(Sender: TObject);

  private
    procedure PostMsg;
  public
    FParams: record
               Handle: Integer;
             end;

  end;

var
  dmMain_app: TdmMain_app;

implementation
{$R *.dfm}


{
const
  WM_REFRESH_DATA = WM_USER + 600;

}


procedure TdmMain_app.PostMsg;

begin


//  h:=FindWindow (PChar('Tfrm_Main_MDI'), nil);
//  if FParams.Handle>0 then
//    SendMessage (FParams.Handle, WM_EXTERNAL_PROJECT_IMPORT_DONE,0,0);


end;


//--------------------------------------------------------------
procedure TdmMain_app.DataModuleCreate(Sender: TObject);
//--------------------------------------------------------------
const
  TEMP_INI_FILE = 'link_optimize.ini';
//TEMP_INI_FILE = 'link_calc.ini';



//const
 // DEF_TEMP_FILENAME = 'Link_Optimize.ini';

var
  b: Boolean;

//  iHandle: Integer;
////  iProjectID,
//  iLinkID: integer;
//  iProjectID: Integer;
//  sMode: string;

  sIniFileName: string;

  oIniFile: TIniFile;


 // bUsePassiveElements: boolean;
 // bIsCalcWithAdditionalRain: Boolean;
 // bSaveReportToDB: boolean;

  oParams: TIni_Link_Optimize_Params;

begin
  sIniFileName:=ParamStr(1);
  if sIniFileName='' then
  //  sIniFileName:=TEMP_INI_FILE;
    sIniFileName:=g_ApplicationDataDir+  TEMP_INI_FILE;


  if not FileExists(sIniFileName) then
  begin
    ShowMessage('not exists: ' + sIniFileName);
    Exit;
  end;


  oParams:=TIni_Link_Optimize_Params.Create;

  oParams.LoadFromFile(sIniFileName);
  if not oParams.Validate then
    Exit;


//  oIniFile := TIniFile.Create(sIniFileName);
//
//  with oIniFile do
//  begin
//    iProjectID                :=ReadInteger ('main', 'ProjectID',  0);
//    iHandle                   :=ReadInteger ('main', 'Handle',  0);
//    iLinkID                   :=ReadInteger ('main', 'ID',  0);
//    sMode                     :=LowerCase(ReadString ('main', 'mode',  ''));
//
//   // bSaveReportToDB           :=ReadBool    ('main', 'IsSaveReportToDB',  True);
//  //  bUsePassiveElements       :=ReadBool    ('main', 'UsePassiveElements',  false);
//  //  bIsCalcWithAdditionalRain :=ReadBool    ('main', 'IsCalcWithAdditionalRain',  false);
//
//   // Free;
//  end;
//
//  FreeAndNil(oIniFile);


{
  iProjectID :=1;
  iLinkID    :=1;
}

 (* if (iLinkID=0) or
     (iProjectID=0)
  then begin
    ShowMessage('(iLinkID=0)');
    Exit;
  end;
*)

  TdmMain.Init;


//  dmMain.ADOConnection1.ConnectionString:=oParams.ConnectionString;
//  dmMain.ADOConnection1.Open;



  if not dmMain.OpenDB_reg then
    Exit;

  dmMain.ProjectID:=oParams.ProjectID;



  //  b:=Tdlg_Link_antennas_optimize1.ExecDlg (oParams.LinkID, oParams);




  if oParams.Mode=LowerCase('Optimize_freq_diversity') then
    b:=Tdlg_Link_Optimize_freq_diversity.ExecDlg(oParams.LinkID, oParams)
  else

  if oParams.Mode=LowerCase('Optimize_antennas') then
    b:=Tdlg_Link_antennas_optimize1.ExecDlg (oParams.LinkID, oParams)
  else
    ShowMessage('Mode=?');


  if b then
  begin
  {
    ShowMessage('WM_REFRESH_DATA: ' + IntToStr(oParams.Handle) +'- WM_REFRESH_DATA: '+ IntToStr(WM_REFRESH_DATA));


    if oParams.Handle>0 then
      SendMessage (oParams.Handle, WM_REFRESH_DATA, 0,0);

      }
  end;


  FreeAndNil(oParams);


  if b then
    ExitProcess(1)
  else
    ExitProcess(0);


  //  PostMsg;
 
end;




end.

(*


  b:=Tfrm_Main_TN_import.ExecDlg ();
//  Application.CreateForm(Tframe_Options, frame_Options);
 

  ini_WriteBool (sIniFileName, 'main', 'result', b);


  PostMsg();


{
  if Tdlg_Property_And_Link_Import.ExecDlg (iProjectID) then
    ExitProcess(1)
  else
    ExitProcess(0);
}



const
  // debug info
  //---------------------------------------------------
  // messages
  //---------------------------------------------------
//  WM_FORM_CREATED = WM_USER + 1;

  WM_EXTERNAL_PROJECT_IMPORT_DONE   = WM_USER + 500;
  WM_EXTERNAL_LIB_IMPORT_DONE       = WM_USER + 501;

*)
