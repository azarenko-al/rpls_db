program Link_optimize;

uses
  d_Link_antennas_optimize1 in '..\Src\d_Link_antennas_optimize1.pas' {dlg_Link_antennas_optimize1},
  d_Link_Optimize_freq_diversity in '..\Src\d_Link_Optimize_freq_diversity.pas' {dlg_Link_Optimize_freq_diversity},
  dm_Link_calc in '..\..\Link_Calc\src\dm_Link_calc.pas' {dmLink_calc: TDataModule},
  dm_Main_app in '..\Src\dm_Main_app.pas' {dmMain_app: TDataModule},
  Forms,
  u_ini_Link_Optimize_params in '..\src shared\u_ini_Link_optimize_params.pas',
  u_link_classes in '..\..\Common classes\u_link_classes.pas',
  u_radio in '..\..\..\..\common7\Package_Common\u_radio.pas',
  u_run in '..\..\..\..\common7\Package_Common\u_run.pas',
  Windows,
  u_opti_class_new in '..\..\Link_Calc\src\opti\u_opti_class_new.pas',
  f_Main_opti in '..\..\Link_Calc\src\opti\f_Main_opti.pas' {frm_Main_opti},
  u_Opti_classes in '..\..\Link_Calc\src\opti\u_Opti_classes.pas';

{$R *.res}

begin
  SetThreadLocale(1049);


  Application.Initialize;
  
  Application.CreateForm(TdmMain_app, dmMain_app);
  Application.Run;
end.
