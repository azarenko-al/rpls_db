unit u_ini_Link_Optimize_params;


interface
uses
   IniFiles, SysUtils;

type
  TIni_Link_Optimize_Params = class(TObject)
  public
    ConnectionString  : string;

 //  UsePassiveElements       : Boolean;


//    Handle: Integer;
  //  iProjectID,
    LinkID: integer;
    ProjectID: Integer;

    Mode: string;

//    ModeType: string;



    procedure LoadFromFile(aFileName: string);
    procedure SaveToFile(aFileName: string);

// TODO: Validate
  function Validate: boolean;
  end;


implementation


// ---------------------------------------------------------------
procedure TIni_Link_Optimize_Params.LoadFromFile(aFileName: string);
// ---------------------------------------------------------------
//const
 // DEF_Repeater = 'Repeater';
 // DEF_BuildGraph = 'BuildGraph';

begin
  Assert(FileExists(aFileName), aFileName);


  with TIniFile.Create(aFileName) do
  begin
//    Handle             :=ReadInteger ('main', 'Handle',  0);

    ProjectID          :=ReadInteger ('main', 'ProjectID',  0);
    LinkID             :=ReadInteger ('main', 'ID',  0);

    Mode               :=LowerCase(ReadString ('main', 'mode',  ''));

    ConnectionString :=ReadString ('main', 'ConnectionString',  '');

//    UsePassiveElements := ReadBool ('main', 'UsePassiveElements',  False);

    Free;
  end;


end;

// ---------------------------------------------------------------
procedure TIni_Link_Optimize_Params.SaveToFile(aFileName: string);
// ---------------------------------------------------------------

var
  oIniFile: TIniFile;

begin
  oIniFile := TIniFile.Create(aFileName);

  with oIniFile do
  begin
 //   WriteInteger ('main', 'Handle',  Handle);

    WriteInteger ('main', 'ProjectID', ProjectID);
    WriteInteger ('main', 'ID',    LinkID);
   // WriteBool    ('main', 'IsSaveReportToDB',  IsSaveReportToDB);
//    WriteBool    ('main', 'UsePassiveElements',  UsePassiveElements);

    WriteString ('main', 'mode', Mode);

    WriteString ('main', 'ConnectionString',  ConnectionString);

  end;


  FreeAndNil(oIniFile);
end;




function TIni_Link_Optimize_Params.Validate: boolean;
begin
  Result := (LinkID>0) and (ProjectID>0);

//  if not Result then
 //   ShowMessage('(iLinkID=0) or (iProjectID=0)');

end;


end.



(*


  if sMode=LowerCase('Optimize_freq_diversity') then
    b:=Tdlg_Link_Optimize_freq_diversity.ExecDlg(iLinkID)
  else

  if sMode=LowerCase('Optimize_antennas') then
    b:=Tdlg_Link_antennas_optimize.ExecDlg (iLinkID)
  else
    ShowMessage('Mode=?');




*)
