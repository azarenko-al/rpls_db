unit I_link_optimize;

interface

uses ADOInt,
  u_dll;

type

  Ilink_optimize_X = interface(IInterface)
  ['{73DB7EE4-1606-4247-B469-ED8C2CD8524E}']

    procedure InitADO(aConnection: _Connection); stdcall;
    procedure InitAppHandle(aHandle: integer); stdcall;

    procedure Execute (aFileName: WideString);  stdcall;
  end;

var
  ILink_optimize: Ilink_optimize_X;

  function Load_ILink_optimize: Boolean;
  procedure UnLoad_ILink_optimize;

implementation

const
//  DEF_BPL_FILENAME = 'bpl_LOS.bpl';
  DEF_FILENAME = 'dll_Link_optimize.dll';

var
  LHandle : Integer;


function Load_ILink_optimize: Boolean;
begin
  Result:=GetInterface_DLL(DEF_FILENAME, LHandle, Ilink_optimize_X, ILink_optimize)=0;
end;


procedure UnLoad_ILink_optimize;
begin
  ILink_optimize := nil;
  UnloadPackage_dll(LHandle);
end;


end.



