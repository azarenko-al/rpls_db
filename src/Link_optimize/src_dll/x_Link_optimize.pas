unit x_Link_optimize;

interface

uses  Sysutils, Dialogs, IniFiles, Windows, Messages,

  u_dll_with_dmMain,

  dm_Main,         

  d_Link_Optimize_freq_diversity,
  d_Link_antennas_optimize1,

  u_ini_Link_Optimize_params,

//  u_Ini_Link_Graph_Params,


 I_link_optimize;

type
  TLink_optimize_X = class(TDllCustomX, ILink_optimize_X)
  public
    procedure Execute(aFileName: WideString); stdcall;

  end;

  function DllGetInterface(const IID: TGUID; var Obj): HResult; stdcall;

exports
  DllGetInterface;

  
implementation


const
  WM_REFRESH_DATA = WM_USER + 600;



// ---------------------------------------------------------------
procedure TLink_optimize_X.Execute(aFileName: WideString);
// ---------------------------------------------------------------
const
  TEMP_INI_FILE = 'link_calc.ini';

var
  b: Boolean;
  oIniFile: TIniFile;

  oParams: TIni_Link_Optimize_Params;


begin
  Assert(Assigned(dmMain), 'Assigned(dmMain) not assigned');


  if not FileExists(aFileName ) then
  begin
    ShowMessage(aFileName);
    Exit;
  end;


  oParams:= TIni_Link_Optimize_Params.Create;
  oParams.LoadFromFile(aFileName);


  dmMain.ProjectID:=oParams.ProjectID;


  if oParams.Mode=LowerCase('Optimize_freq_diversity') then
    b:=Tdlg_Link_Optimize_freq_diversity.ExecDlg(oParams.LinkID, oParams)
  else

  if oParams.Mode=LowerCase('Optimize_antennas') then
    b:=Tdlg_Link_antennas_optimize1.ExecDlg (oParams.LinkID, oParams)
  else
    ShowMessage('Mode=?');


  if b then
  begin
  //  ShowMessage('WM_REFRESH_DATA: ' + IntToStr(iHandle));

    if oParams.Handle>0 then
      SendMessage (oParams.Handle, WM_REFRESH_DATA, 0,0);

  end;


  FreeAndNil(oParams);




(*

  TdmMain.Init;
  if not dmMain.OpenDB_reg then
    Exit;
*)
(*

  Tfrm_Main_link_graph.ExecDlg (oParams.LinkID, oParams);


  FreeAndNil(oParams);


  *)

end;


// ---------------------------------------------------------------
function DllGetInterface(const IID: TGUID; var Obj): HResult;
// ---------------------------------------------------------------
begin
  Result:=S_FALSE; Integer(Obj):=0;

  if IsEqualGUID(IID, ILink_optimize_X) then
  begin
    with TLink_optimize_X.Create() do
      if GetInterface(IID,OBJ) then
        Result:=S_OK
  end else
    raise Exception.Create('if IsEqualGUID(IID, ILink_graphX)');


end;



begin
 // RegisterClass(TdmMain_bpl);

end.


