unit dm_Object_Manager;

interface

uses
  SysUtils, Classes, Forms, Dialogs, Variants, Menus,

  I_Object,

  u_func_msg,
  u_const_msg,

  u_geo,
  u_log,
  u_func,
  u_classes
  ;

type
  TdmObject_Manager1 = class(TDataModule)
//    procedure DataModuleDestroy(Sender: TObject);
  //  procedure DataModuleCreate(Sender: TObject);
  private
  public
    function GetViewForm(aObjName: string; aOwnerForm: TForm): TForm;

//    procedure ViewInProjectTree (aID: integer; aGUID: string);

    procedure OBJECT_FOCUS_IN_PROJECT_TREE(aID: integer; aObjName: string);

  //  property Objects: Integer read FObjects;

    class procedure Init;
  end;


var
  dmObject_Manager1: TdmObject_Manager1;


// function dmObject_Manager: TdmObject_Manager;



//====================================================================
// implementation
//====================================================================

implementation {$R *.DFM}



class procedure TdmObject_Manager1.Init;
begin
  if not Assigned(dmObject_Manager1) then
    Application.CreateForm(TdmObject_Manager1, dmObject_Manager1);


end;



// -------------------------------------------------------------------
procedure TdmObject_Manager1.OBJECT_FOCUS_IN_PROJECT_TREE(aID: integer;
    aObjName: string);
// -------------------------------------------------------------------
var
  i: Integer;
  intf: IObjectHandlerX;
begin
  PostEvent(WE_OBJECT_FOCUS_IN_PROJECT_TREE,
            [app_Par(PAR_ID, aID),
             app_Par(PAR_OBJECT_NAME, aObjName)
        //     app_Par(PAR_GUID, aGUID)
             ]);

{  for i:=0 to Application.ComponentCount-1  do
    if Application.Components[i].GetInterface(IObjectHandler, intf) then
    begin
      if Eq(aObjName, intf.ObjectName) then begin
        result := intf.GetViewForm (aOwnerForm);
        Exit;
      end;
    end;

  Result := nil;
}

end;


// -------------------------------------------------------------------
function TdmObject_Manager1.GetViewForm(aObjName: string; aOwnerForm: TForm): TForm;
// -------------------------------------------------------------------
var
  i: Integer;
  intf: IObjectHandlerX;
begin
  for i:=0 to Application.ComponentCount-1  do
    if Application.Components[i].GetInterface(IObjectHandlerX, intf) then
    begin
      if intf.IsObjectSupported(aObjName) then

    //  if Eq(aObjName, intf.ObjectName) then
      begin
        result := intf.GetViewForm (aOwnerForm);
        Exit;
      end;
    end;

  Result := nil;
end;

  

begin

end.


