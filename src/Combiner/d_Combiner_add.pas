unit d_Combiner_add;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
   rxPlacemnt, ActnList,    StdCtrls, ExtCtrls,
  cxControls, cxInplaceContainer, cxPropertiesStore,cxVGrid,

  d_Wizard_add_with_Inspector,
  u_const_str,

  u_const_db,

  u_cx_vgrid,


//  u_const_msg,

  u_func,
//  u_reg,
//  u_func_msg,

//  dm_Main,
  dm_Combiner, ComCtrls

  ;

type
  Tdlg_Combiner_add = class(Tdlg_Wizard_add_with_Inspector)
    cxVerticalGrid1: TcxVerticalGrid;
    row_Loss: TcxEditorRow;
    procedure FormCreate(Sender: TObject);
    procedure act_OkExecute(Sender: TObject);
  private
    FID,FFolderID: integer;
    procedure Append;

  public
    class function ExecDlg (aFolderID: integer =0): integer;
  end;


//====================================================================
implementation {$R *.DFM}
//====================================================================

//--------------------------------------------------------------------
class function Tdlg_Combiner_add.ExecDlg (aFolderID: integer =0): integer;
//--------------------------------------------------------------------
begin
  with Tdlg_Combiner_add.Create(Application) do
  begin
    FFolderID:=aFolderID;                                  
    ed_Name_.Text:=dmCombiner.GetNewName;

    ShowModal;
    Result:=FID;

    Free;
  end;
end;

//--------------------------------------------------------------------
procedure Tdlg_Combiner_add.FormCreate(Sender: TObject);
//--------------------------------------------------------------------
begin
  inherited;

  SetActionName(STR_DLG_ADD_COMBINER_TYPE);

  lb_Name.Caption:=STR_NAME;

  row_Loss.Properties.Caption := STR_LOSS;
 // row_Channels.Caption:= '������';


  cx_InitVerticalGrid (cxVerticalGrid1);


//  FAntTypeID:=RegIni.ReadInteger(Name, FLD_ANTENNA_TYPE_ID, 0);
//  edr_AntType.Text:=dmMain.GetNameByID(TBL_ANTENNA_TYPE, FAntTypeID);
end;


//-------------------------------------------------------------------
procedure Tdlg_Combiner_add.Append;
//-------------------------------------------------------------------
var rec: TdmCombinerAddRec;
begin
  inherited;

  FillChar(rec, SizeOf(rec), 0);

  rec.Name:=ed_Name_.Text;
  rec.FolderID:= FFolderID;
  ////////// rec.Channels:= row_Channels.Text;
  rec.Loss_dB := AsFloat(row_Loss.Properties.Value);

  FID:=dmCombiner.Add (rec);
end;


procedure Tdlg_Combiner_add.act_OkExecute(Sender: TObject);
begin
  inherited;
  Append();
end;


end.