unit dm_Act_Combiner;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
  Dialogs,  Menus, ActnList,

 i_Audit,
   
  dm_User_Security,

  I_Object,

  u_DataExport_run,

  dm_act_Base,

  dm_Combiner,
  d_Combiner_add,

  u_func,
  u_func_msg,
  u_dlg,

  u_const,
  u_const_msg,
 u_const_str,  
  u_const_db,
  u_types
  ;

type
  TdmAct_Combiner = class(TdmAct_Base)
    ActionList2: TActionList;
    act_Export_MDB: TAction;
    act_Audit: TAction;
    procedure DataModuleCreate(Sender: TObject);
  private
    function CheckActionsEnable: Boolean;

  protected
    procedure GetPopupMenu (aPopupMenu: TPopupMenu; aMenuType: TMenuType); override;
    procedure DoAction (Sender: TObject);

    function ItemDel(aID: integer; aName: string): boolean; override;
    function ItemAdd (aFolderID: integer): integer; override;

  public

    class procedure Init;

  end;

var
  dmAct_Combiner: TdmAct_Combiner;

//==================================================================
implementation

uses dm_Main; {$R *.dfm}
//==================================================================

//--------------------------------------------------------------------
class procedure TdmAct_Combiner.Init;
//--------------------------------------------------------------------
begin
  if not Assigned(dmAct_Combiner) then
    Application.CreateForm(TdmAct_Combiner, dmAct_Combiner);

  //  dmAct_Combiner:=TdmAct_Combiner.Create(Application);
end;


//--------------------------------------------------------------------
procedure TdmAct_Combiner.DataModuleCreate(Sender: TObject);
//--------------------------------------------------------------------
begin
  inherited;

  ObjectName:=OBJ_COMBINER;

  act_Export_MDB.Caption:=DEF_STR_Export_MDB;

  act_Audit.Caption:=STR_Audit;

  SetActionsExecuteProc ([
                         act_Export_MDB,
                         act_Audit

                          ], DoAction);


end;

//--------------------------------------------------------------------
procedure TdmAct_Combiner.DoAction (Sender: TObject);
//--------------------------------------------------------------------
begin
  if Sender=act_Audit then
     Init_IAuditX.ExecDlg( dmMain.GetConnectionString, TBL_PASSIVE_COMPONENT, FFocusedID) else

  if Sender=act_Export_MDB then
//    TDataExport_run.ExportToRplsDbGuides1 (OBJ_ANTENNA_TYPE);
    TDataExport_run.ExportToRplsDb_selected (TBL_PASSIVE_COMPONENT, FSelectedIDList);


end;

//--------------------------------------------------------------------
function TdmAct_Combiner.ItemAdd (aFolderID: integer): integer;
//--------------------------------------------------------------------
begin
  Result:=Tdlg_Combiner_add.ExecDlg (aFolderID);
end;

//--------------------------------------------------------------------
function TdmAct_Combiner.ItemDel(aID: integer; aName: string): boolean;
//--------------------------------------------------------------------
begin
  Result:=dmCombiner.Del (aID);
end;



// ---------------------------------------------------------------
function TdmAct_Combiner.CheckActionsEnable: Boolean;
// ---------------------------------------------------------------
begin
  Result:= dmUser_Security.Modify_lib in [mtAllow, mtAllowMy];

  SetActionsEnabled1( [
          act_Del_,
          act_Del_list

      ],

   Result );

  //  TModify_lib = (mtAllow=1, mtAllowMy=2, mtForbidden=0 );

end;


//--------------------------------------------------------------------
procedure TdmAct_Combiner.GetPopupMenu;
//--------------------------------------------------------------------
var
  bEnable: Boolean;
begin
  bEnable:=CheckActionsEnable();


  case aMenuType of
    mtFolder:  begin
                 AddFolderMenu_Create (aPopupMenu, bEnable);
                 AddFolderPopupMenu(aPopupMenu, bEnable);
//                 AddMenuItem (aPopupMenu, act_Export_MDB);
               end;
    mtItem: begin
               AddMenuItem (aPopupMenu, act_Del_);
              AddMenuItem (aPopupMenu, nil);
              AddFolderMenu_Tools (aPopupMenu);
              AddMenuItem (aPopupMenu, act_Export_MDB);

              AddMenuItem (aPopupMenu, act_Audit);
            end;
    mtList: begin
              AddMenuItem (aPopupMenu, act_Del_list);
              AddMenuItem (aPopupMenu, act_Export_MDB);              
            end;
  end;

end;


begin

end.
