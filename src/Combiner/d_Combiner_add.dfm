inherited dlg_Combiner_add: Tdlg_Combiner_add
  Left = 757
  Top = 322
  Caption = 'dlg_Combiner_add'
  ParentFont = True
  PixelsPerInch = 120
  TextHeight = 13
  inherited Panel2: TPanel
    Height = 209
    inherited PageControl1: TPageControl
      Height = 199
      inherited TabSheet1: TTabSheet
        object cxVerticalGrid1: TcxVerticalGrid
          Left = 0
          Top = 0
          Width = 542
          Height = 168
          Align = alClient
          LookAndFeel.Kind = lfStandard
          OptionsView.CellTextMaxLineCount = 3
          OptionsView.AutoScaleBands = False
          OptionsView.PaintStyle = psDelphi
          OptionsView.GridLineColor = clBtnShadow
          OptionsView.RowHeaderMinWidth = 30
          OptionsView.RowHeaderWidth = 141
          OptionsView.ValueWidth = 59
          TabOrder = 0
          Version = 1
          object row_Loss: TcxEditorRow
            Expanded = False
            Properties.Caption = 'loss'
            Properties.EditPropertiesClassName = 'TcxTextEditProperties'
            Properties.EditProperties.Alignment.Horz = taLeftJustify
            Properties.EditProperties.Alignment.Vert = taVCenter
            Properties.EditProperties.MaxLength = 0
            Properties.EditProperties.ReadOnly = False
            Properties.DataBinding.ValueType = 'String'
            Properties.Value = Null
            ID = 0
            ParentID = -1
            Index = 0
            Version = 1
          end
        end
      end
    end
  end
end
