unit dm_Combiner;

interface

uses
  Windows, Db, ADODB, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, Variants,

  dm_Main,

  dm_Object_base,

  u_func,
  u_db,
  
  u_types,

  u_const_str,
  u_const_db

  ;

type
  //-------------------------------------------------------------------
  TdmCombinerAddRec = record
  //-------------------------------------------------------------------
    Name: string;
    FolderID : integer;

    Loss_dB  : double;
  //  Channels: string;

  end;


  TdmCombiner = class(TdmObject_base)
    procedure DataModuleCreate(Sender: TObject);

  public
    function Add (aRec: TdmCombinerAddRec): integer;
    function GetLoss (aID: integer): double;

  end;


function dmCombiner: TdmCombiner;

//==========================================================
implementation {$R *.dfm}
//==========================================================

var
  FdmCombiner: TdmCombiner;


// ---------------------------------------------------------------
function dmCombiner: TdmCombiner;
// ---------------------------------------------------------------
begin
  if not Assigned(FdmCombiner) then
      FdmCombiner := TdmCombiner.Create(Application);

  Result := FdmCombiner;
end;



procedure TdmCombiner.DataModuleCreate(Sender: TObject);
begin
  inherited;
  TableName:=TBL_PASSIVE_COMPONENT;
//  DisplayName:=STR_PASSIVE_ELEMENT;
  ObjectName:=OBJ_COMBINER;// PASSIVE_COMPONENT;
end;

//--------------------------------------------------------------------
function TdmCombiner.GetLoss (aID: integer): double;
//--------------------------------------------------------------------
begin
  Result:= GetDoubleFieldValue(aID, FLD_LOSS);
end;

//--------------------------------------------------------------------
function TdmCombiner.Add (aRec: TdmCombinerAddRec): integer;
//--------------------------------------------------------------------
begin
  with aRec do
  Result:=gl_DB.AddRecordID (TableName,

                  [db_Par(FLD_NAME, Name),
                   db_Par(FLD_FOLDER_ID, IIF(FolderID>0,FolderID,NULL)),

                   db_Par(FLD_LOSS,     Loss_dB)
                 //  db_Par(FLD_CHANNELS, Channels),
                   //db_Par(FLD_FREQ_MAX, FreqMax)

                  ] );
//   ;
end;


begin
  
end.

