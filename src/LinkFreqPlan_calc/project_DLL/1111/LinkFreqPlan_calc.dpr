library LinkFreqPlan_calc;

uses
  ComServ,
  dm_Data_Calc in '..\src\dm_Data_Calc.pas' {dmData_Calc: TDataModule},
  dm_LinkFreqPlan_calc in '..\src\dm_LinkFreqPlan_calc.pas' {dmLinkFreqPlan_calc: TDataModule},
  LinkFreqPlan_calc_TLB in 'LinkFreqPlan_calc_TLB.pas',
  u_ini_LinkFreqPlan_calc_params in '..\src_shared\u_ini_LinkFreqPlan_calc_params.pas',
  u_LinkFreqPlan_calc_export in '..\src\u_LinkFreqPlan_calc_export.pas',
  u_LinkFreqPlan_classes in '..\..\LinkFreqPlan_common_classes\u_LinkFreqPlan_classes.pas',
  x_main in 'x_main.pas' {LinkFreqPlan_calc: CoClass};

exports
  DllGetClassObject,
  DllCanUnloadNow,
  DllRegisterServer,
  DllUnregisterServer;

{$R *.TLB}

{$R *.RES}

begin
end.
