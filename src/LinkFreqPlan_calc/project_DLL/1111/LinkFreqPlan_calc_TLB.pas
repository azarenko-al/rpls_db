unit LinkFreqPlan_calc_TLB;

// ************************************************************************ //
// WARNING                                                                    
// -------                                                                    
// The types declared in this file were generated from data read from a       
// Type Library. If this type library is explicitly or indirectly (via        
// another type library referring to this type library) re-imported, or the   
// 'Refresh' command of the Type Library Editor activated while editing the   
// Type Library, the contents of this file will be regenerated and all        
// manual modifications will be lost.                                         
// ************************************************************************ //

// PASTLWTR : 1.2
// File generated on 07.09.2015 18:04:34 from Type Library described below.

// ************************************************************************  //
// Type Lib: W:\RPLS_DB\src\LinkFreqPlan_calc\project_DLL\LinkFreqPlan_calc.tlb (1)
// LIBID: {AE49E385-4BD7-40F5-9C11-15DF96D644EF}
// LCID: 0
// Helpfile: 
// HelpString: LinkFreqPlan_calc Library
// DepndLst: 
//   (1) v2.0 stdole, (C:\Windows\system32\stdole2.tlb)
// ************************************************************************ //
{$TYPEDADDRESS OFF} // Unit must be compiled without type-checked pointers. 
{$WARN SYMBOL_PLATFORM OFF}
{$WRITEABLECONST ON}
{$VARPROPSETTER ON}
interface

uses Windows, ActiveX, Classes, Graphics, StdVCL, Variants;
  

// *********************************************************************//
// GUIDS declared in the TypeLibrary. Following prefixes are used:        
//   Type Libraries     : LIBID_xxxx                                      
//   CoClasses          : CLASS_xxxx                                      
//   DISPInterfaces     : DIID_xxxx                                       
//   Non-DISP interfaces: IID_xxxx                                        
// *********************************************************************//
const
  // TypeLibrary Major and minor versions
  LinkFreqPlan_calcMajorVersion = 1;
  LinkFreqPlan_calcMinorVersion = 0;

  LIBID_LinkFreqPlan_calc: TGUID = '{AE49E385-4BD7-40F5-9C11-15DF96D644EF}';


implementation

uses ComObj;

end.
