nit Unit5;

{$WARN SYMBOL_PLATFORM OFF}

interface

uses
  Windows, ActiveX, Classes, ComObj,
  Dialogs,

   LinkFreqPlan_calc_TLB, StdVcl;

type
  TLinkFreqPlan_calc_ = class(TTypedComObject, ILinkFreqPlan_calc, ILinkFreqPlan_calc)
  protected
    function Exec(const aFileName: WideString): HResult; stdcall;
    function ILinkFreqPlan_calc.Exec = ILinkFreqPlan_calc_Exec;
    function ILinkFreqPlan_calc_Exec(const aFileName: WideString): HResult;
      stdcall;
    function Method2: HResult; stdcall;
    {Declare ITest methods here}
  public
    constructor Create;
    destructor Destroy; override;
  end;

implementation

uses ComServ;

constructor TLinkFreqPlan_calc_.Create;
begin
  inherited;

  ShowMessage('constructor TTest.Create;');

  // TODO -cMM: TTest.Create default body inserted
end;

destructor TLinkFreqPlan_calc_.Destroy;
begin
  inherited;

   ShowMessage('constructor TTest.Destroy;');

  // TODO -cMM: TTest.Destroy default body inserted
end;

function TLinkFreqPlan_calc_.Exec(const aFileName: WideString): HResult;
begin

end;

function TLinkFreqPlan_calc_.ILinkFreqPlan_calc_Exec(
  const aFileName: WideString): HResult;
begin

end;

function TLinkFreqPlan_calc_.Method2: HResult;
begin

end;

initialization
  TTypedComObjectFactory.Create(ComServer, TLinkFreqPlan_calc_, Class_LinkFreqPlan_calc_,
    ciMultiInstance, tmApartment);
end.
