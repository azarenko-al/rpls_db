unit x_main;

{$WARN SYMBOL_PLATFORM OFF}

interface

uses
  Windows, ActiveX, Classes, ComObj, LinkFreqPlan_calc_TLB, Dialogs, StdVcl;

type
  TLinkFreqPlan_calc_ = class(TTypedComObject, ILinkFreqPlan_calc)
  protected
    function Exec(const aFileName: WideString): HResult; stdcall;
    {Declare ITest methods here}

    procedure AfterConstruction;  override;
    procedure BeforeDestruction; override;



  public
    constructor Create;
  end;

implementation

uses ComServ;

constructor TLinkFreqPlan_calc_.Create;
begin
  inherited;


  ShowMessage('constructor TLinkFreqPlan_calc_.Create;');

end;

procedure TLinkFreqPlan_calc_.AfterConstruction;
begin
   ShowMessage(' AfterConstruction.;');
end;

procedure TLinkFreqPlan_calc_.BeforeDestruction;
begin
  ShowMessage(' BeforeDestruction');
end;



function TLinkFreqPlan_calc_.Exec(const aFileName: WideString): HResult;
begin
  ShowMessage('function TLinkFreqPlan_calc_.Exec');

end;

initialization
  TTypedComObjectFactory.Create(ComServer, TLinkFreqPlan_calc_, Class_LinkFreqPlan_calc_,
    ciMultiInstance, tmApartment);
end.
