unit u_LinkFreqPlan_calc_classes;

interface

uses
  Classes, DB, SysUtils,

  u_LinkFreqPlan_const,
  u_const_db ;

type
  TLinkFreqPlan_Neighbor_List = class;

  // ---------------------------------------------------------------
  TcLinkFreqPlan_Linkend = class(TCollectionItem)
  // ---------------------------------------------------------------
  public
    Linkend_id  : Integer;
    PROPERTY_ID : Integer;

    CHANNEL_TYPE_FIXED: string;
    FREQ_FIXED: string;
    FREQ_REQ_COUNT: Integer;

    ChannelType: string;
    ChSpacing: Double;

//    LinkEndType_ID: Integer;

    // -------------------------
    // MODE
    // -------------------------
    Bandwidth : Double;

    ChannelWidth : double;
    TxRx_Shift   : Double;
    Freq_Min_low_MHz: Double;
    ChannelCount : Integer;

(*    THRESHOLD_DEGRADATION : Double;

    Next_Linkend_id : Integer;

    Link_id : Integer;
    Link_Name : string;
    ClutterModel_ID : Integer;


    LINKENDTYPE_ID: Integer;

    THRESHOLD_BER_6    : double;
    THRESHOLD_BER_3    : double;
    Power_dBm          : double;
//    Rx_level_dBm        : double;


    TX_FREQ_MHz        : Double;

    BitRate_Mbps       : double;  //BitRate_Mbps

    BAND               : string;

    LOSS_dBm           : Double;
    KNG                : Double;

    KRATNOST_BY_SPACE  : Integer;

    KRATNOST_BY_FREQ   : Integer;
    FREQ_CHANNEL_COUNT : Integer;

    Freq_Spacing_MHz   : Double;

    SIGNATURE_WIDTH : double;// '������ ���������, ���');
    SIGNATURE_HEIGHT: double;// '������ ���������, ��');
    MODULATION_COUNT: double;// '����� ������� ���������');
    EQUALISER_PROFIT: double;// '�������, �������� ������������, ��');

    // -------------------------
    // band
    // -------------------------
    FREQ_MAX_HIGH : double;
    FREQ_MIN_LOW  : double;

    // -------------------------
    // LinkEndType
    // -------------------------
    level_tx_a  : Integer;
    level_rx_a  : Integer;

    // -------------------------
    // Property
    // -------------------------
 //   GROUND_HEIGHT : Variant;

    // -------------------------
    // MODE
    // -------------------------
    Bandwidth : Double;

    BANDWIDTH_TX_30 : Double;
    BANDWIDTH_TX_3  : Double;
    BANDWIDTH_TX_A  : Double;

    BANDWIDTH_RX_30 : Double;
    BANDWIDTH_RX_3  : Double;
    BANDWIDTH_RX_A  : Double;

*)
  public

    constructor Create(Collection: TCollection); override;
    destructor Destroy; override;

    procedure LoadFromDataset(aDataset: TDataset);
  end;


  TcLinkFreqPlan_Linkend_List = class(TCollection)
  private
    function Add: TcLinkFreqPlan_Linkend;
    function GetItems(Index: Integer): TcLinkFreqPlan_Linkend;
  public
    constructor Create;

    function FindByID(aID: Integer): TcLinkFreqPlan_Linkend;
    procedure LoadFromDataset(aDataset: TDataset);

    property Items[Index: Integer]: TcLinkFreqPlan_Linkend read GetItems; default;
  end;


  // ---------------------------------------------------------------
  TLinkFreqPlan_calc_params = class(TObject)
  // ---------------------------------------------------------------
  public
    LinkEnds: TcLinkFreqPlan_Linkend_List;
    Neighbors: TLinkFreqPlan_Neighbor_List;

    Band_Resources: array of record
                          Band : string;
                          FREQ_ALLOW : String;
                          FREQ_FORBIDDEN: string;

                       //   StrArrAllow     : TStrArray;
                       //   StrArrForbidden : TStrArray;

                        end;

    // -------------------------
  public

  //  Params: record
    SectorFreqSpacing   : double;  // ������ ������ � �������
    SiteFreqSpacing   : double;  // ������ ������ �������� � ������� �������
//      SitesFreqSpacing    : double;  // ������ ������ �������� ������ ������� �������
    StartSectorNum  : integer; // ����� �������� ����� �������������
    PereborQueueType : integer;
    FreqDistribType : integer; // 0-dtOneFreqForSector, 1-dtAllFreqsForSector);
    MaxIterCount : integer;
    PereborOrderType    : integer; //0-opForward, 1-opBackward, 2-opRandom);
    MaxFreqRepeat       : integer; //max �����.�������� ������
 // end;


    constructor Create;
    destructor Destroy; override;

    procedure LoadFromDataset_LinkFreqPlan(aDataset: TDataset);
    procedure LoadFromDataset_BandResources(aDataset: TDataset);
    procedure LoadFromDataset_Neighbors(aDataset: TDataset);

  end;


  // ---------------------------------------------------------------
  TcLinkFreqPlan_Neighbor = class(TCollectionItem)
  // ---------------------------------------------------------------
  public
    Linkend_id  : Integer;
    PROPERTY_ID : Integer;

    CHANNEL_TYPE_FIXED: string;
    FREQ_FIXED: string;
    FREQ_REQ_COUNT: Integer;

    ChannelType: string;
    ChSpacing: Double;

//    LinkEndType_ID: Integer;

    // -------------------------
    // MODE
    // -------------------------
    Bandwidth : Double;

    ChannelWidth : double;
    TxRx_Shift   : Double;
    Freq_Min_low_MHz: Double;
    ChannelCount : Integer;

(*    THRESHOLD_DEGRADATION : Double;

    Next_Linkend_id : Integer;

    Link_id : Integer;
    Link_Name : string;
    ClutterModel_ID : Integer;


    LINKENDTYPE_ID: Integer;

    THRESHOLD_BER_6    : double;
    THRESHOLD_BER_3    : double;
    Power_dBm          : double;
//    Rx_level_dBm        : double;


    TX_FREQ_MHz        : Double;

    BitRate_Mbps       : double;  //BitRate_Mbps

    BAND               : string;

    LOSS_dBm           : Double;
    KNG                : Double;

    KRATNOST_BY_SPACE  : Integer;

    KRATNOST_BY_FREQ   : Integer;
    FREQ_CHANNEL_COUNT : Integer;

    Freq_Spacing_MHz   : Double;

    SIGNATURE_WIDTH : double;// '������ ���������, ���');
    SIGNATURE_HEIGHT: double;// '������ ���������, ��');
    MODULATION_COUNT: double;// '����� ������� ���������');
    EQUALISER_PROFIT: double;// '�������, �������� ������������, ��');

    // -------------------------
    // band
    // -------------------------
    FREQ_MAX_HIGH : double;
    FREQ_MIN_LOW  : double;

    // -------------------------
    // LinkEndType
    // -------------------------
    level_tx_a  : Integer;
    level_rx_a  : Integer;

    // -------------------------
    // Property
    // -------------------------
 //   GROUND_HEIGHT : Variant;

    // -------------------------
    // MODE
    // -------------------------
    Bandwidth : Double;

    BANDWIDTH_TX_30 : Double;
    BANDWIDTH_TX_3  : Double;
    BANDWIDTH_TX_A  : Double;

    BANDWIDTH_RX_30 : Double;
    BANDWIDTH_RX_3  : Double;
    BANDWIDTH_RX_A  : Double;

*)
  public

    constructor Create(Collection: TCollection); override;
    destructor Destroy; override;

    procedure LoadFromDataset(aDataset: TDataset);
  end;


  TLinkFreqPlan_Neighbor_List = class(TCollection)
  private
    function AddItem: TcLinkFreqPlan_Neighbor;
    function GetItems(Index: Integer): TcLinkFreqPlan_Neighbor;
  public
    constructor Create;

    procedure LoadFromDataset(aDataset: TDataset);

    property Items[Index: Integer]: TcLinkFreqPlan_Neighbor read GetItems; default;
  end;

  
implementation

constructor TcLinkFreqPlan_Linkend.Create(Collection: TCollection);
begin
  inherited Create(Collection);

end;

destructor TcLinkFreqPlan_Linkend.Destroy;
begin

  inherited Destroy;
end;

 // ---------------------------------------------------------------
procedure TcLinkFreqPlan_Linkend.LoadFromDataset(aDataset: TDataset);
// ---------------------------------------------------------------

var
  FREQ_ALLOW: string;
  FREQ_FORBIDDEN: string;
  LinkEndType_Mode_ID: Integer;
begin
  with aDataset do
  begin
    Linkend_id :=FieldByName(FLD_Linkend_id).AsInteger;

    PROPERTY_ID       :=FieldByName(FLD_PROPERTY_ID).AsInteger;

    // -------------------------
    // fixed
    // -------------------------
    FREQ_FIXED:=FieldByName(FLD_FREQ_FIXED).AsString;
    CHANNEL_TYPE_FIXED:=FieldByName(FLD_CHANNEL_TYPE_FIXED).AsString;

    FREQ_REQ_COUNT    :=FieldByName(FLD_FREQ_REQ_COUNT).AsInteger;

    FREQ_ALLOW     := FieldByName(FLD_FREQ_ALLOW).AsString;
    FREQ_FORBIDDEN := FieldByName(FLD_FREQ_FORBIDDEN).AsString;

    ChannelType         := Lowercase( FieldByName(FLD_CHANNEL_TYPE).AsString);
  //  iChType       := FieldByName(FLD_CHANNEL_NUMBER_TYPE).AsInteger;

  //  LinkEndType_ID:= FieldByName(FLD_LINKENDType_ID).AsInteger;
    LinkEndType_Mode_ID:= FieldByName(FLD_LINKENDType_Mode_ID).AsInteger;


    // -------------------------
    // MODE
    // -------------------------

    ChannelWidth      := FieldByName(FLD_CHANNEL_WIDTH).AsFloat;
    TxRx_Shift        := FieldByName(FLD_TXRX_SHIFT).AsFloat;
    Freq_Min_low_MHz  := FieldByName(FLD_FREQ_MIN_LOW).AsFloat;
    ChannelCount      := FieldByName(FLD_CHANNEL_COUNT).AsInteger;


(*    ChSpacing:= FieldByName(FLD_ch_spacing).AsFloat;

    Bandwidth:= FieldByName(FLD_bandwidth).AsFloat;
*)


(*    arrStrAllow     := StringToStrArray();
    arrStrForbidden := StringToStrArray();
*)


(*    Next_Linkend_id:=FieldByName(FLD_NEXT_Linkend_id).AsInteger;

    LINKENDTYPE_ID :=FieldByName(FLD_LINKENDTYPE_ID).AsInteger;



    BAND              :=FieldByName(FLD_BAND).AsString;
   // TX_FREQ_MHz       :=FieldByName(FLD_TX_FREQ_MHz).AsFloat;

    LINKENDTYPE_ID   :=FieldByName(FLD_LINKENDTYPE_ID).AsInteger;

//    Rx_level_dBm      :=FieldByName(FLD_Rx_level_dBm).AsFloat;
    Power_dBm         :=FieldByName(FLD_POWER_dBm).AsFloat;

    Kng               :=FieldByName(FLD_Kng).AsFloat;
    LOSS_dBm          :=FieldByName(FLD_LOSS).AsFloat;

    FREQ_CHANNEL_COUNT:=FieldByName(FLD_FREQ_CHANNEL_COUNT).AsInteger;

    Freq_Spacing_MHz  :=FieldByName(FLD_Freq_Spacing).AsFloat;

    KRATNOST_BY_SPACE:=FieldByName(FLD_KRATNOST_BY_SPACE).AsInteger;
    KRATNOST_BY_FREQ :=FieldByName(FLD_KRATNOST_BY_FREQ).AsInteger;

    SIGNATURE_WIDTH := FieldByName(FLD_SIGNATURE_WIDTH).AsFloat;//,  'double; //  '������ ���������, ���');
    SIGNATURE_HEIGHT:= FieldByName(FLD_SIGNATURE_HEIGHT).AsFloat;//, 'double; // '������ ���������, ��');
    MODULATION_COUNT:= FieldByName(FLD_MODULATION_COUNT).AsFloat;//,double;// '����� ������� ���������');
    EQUALISER_PROFIT:= FieldByName(FLD_EQUALISER_PROFIT).AsFloat;//, 'double; // '�������, �������� ������������, ��');


    GROUND_HEIGHT  :=FieldByName(FLD_GROUND_HEIGHT).Value;

    // -------------------------
    // band
    // -------------------------
    FREQ_MAX_HIGH := FieldByName(FLD_FREQ_MAX_HIGH).AsFloat;
    FREQ_MIN_LOW  := FieldByName(FLD_FREQ_MIN_LOW).AsFloat;


    // -------------------------
    // Link
    // -------------------------
    Link_id    :=FieldByName(FLD_Link_id).AsInteger;
    Link_Name  :=FieldByName(FLD_Link_Name).AsString;
    ClutterModel_ID:= FieldByName(FLD_CLUTTER_MODEL_ID).AsInteger;

    // -------------------------
    // LinkEndType
    // -------------------------
    level_tx_a:=FieldByName(FLD_level_tx_a).AsInteger;
    level_rx_a:=FieldByName(FLD_level_rx_a).AsInteger;

    // -------------------------
    // MODE
    // -------------------------
    Bandwidth :=FieldByName(FLD_BANDWIDTH).AsFloat;

    BANDWIDTH_TX_30 :=FieldByName(FLD_BANDWIDTH_TX_30).AsFloat;
    BANDWIDTH_TX_3  :=FieldByName(FLD_BANDWIDTH_TX_3).AsFloat;
    BANDWIDTH_TX_A  :=FieldByName(FLD_BANDWIDTH_TX_A).AsFloat;


    BANDWIDTH_RX_30 :=FieldByName(FLD_BANDWIDTH_RX_30).AsFloat;
    BANDWIDTH_RX_3  :=FieldByName(FLD_BANDWIDTH_RX_3).AsFloat;
    BANDWIDTH_RX_A  :=FieldByName(FLD_BANDWIDTH_RX_A).AsFloat;

    THRESHOLD_BER_6   :=FieldByName(FLD_THRESHOLD_BER_6).asFloat;
    THRESHOLD_BER_3   :=FieldByName(FLD_THRESHOLD_BER_3).asFloat;

*)
  end;
end;

constructor TcLinkFreqPlan_Linkend_List.Create;
begin
  inherited Create(TcLinkFreqPlan_Linkend);
end;


function TcLinkFreqPlan_Linkend_List.Add: TcLinkFreqPlan_Linkend;
begin
  Result := TcLinkFreqPlan_Linkend (inherited Add);
end;

// ---------------------------------------------------------------
function TcLinkFreqPlan_Linkend_List.FindByID(aID: Integer): TcLinkFreqPlan_Linkend;
// ---------------------------------------------------------------
var
  I: integer;
begin
  for I := 0 to Count - 1 do
    if Items[i].Linkend_ID= aID then
    begin
      Result := Items[i];
      Exit;
    end;

  Result := nil;
end;


function TcLinkFreqPlan_Linkend_List.GetItems(Index: Integer):
    TcLinkFreqPlan_Linkend;
begin
  Result := TcLinkFreqPlan_Linkend(inherited Items[Index]);
end;


procedure TcLinkFreqPlan_Linkend_List.LoadFromDataset(aDataset: TDataset);
begin
  Clear;
  aDataset.First;

  while not aDataset.EOF do
  begin
    Add.LoadFromDataset(aDataset);
    aDataset.Next;
  end;
end;

constructor TLinkFreqPlan_calc_params.Create;
begin
  inherited Create;
  LinkEnds := TcLinkFreqPlan_Linkend_List.Create();
end;

destructor TLinkFreqPlan_calc_params.Destroy;
begin
  FreeAndNil(LinkEnds);
  inherited Destroy;
end;

 // ---------------------------------------------------------------
procedure TLinkFreqPlan_calc_params.LoadFromDataset_LinkFreqPlan(aDataset:
    TDataset);
// ---------------------------------------------------------------

(*var
  FREQ_ALLOW: string;
  FREQ_FORBIDDEN: string;
  LinkEndType_Mode_ID: Integer;*)
begin
  with aDataset do
  begin
   (* Linkend_id :=FieldByName(FLD_Linkend_id).AsInteger;

    PROPERTY_ID       :=FieldByName(FLD_PROPERTY_ID).AsInteger;

    // -------------------------
    // fixed
    // -------------------------
    FREQ_FIXED:=FieldByName(FLD_FREQ_FIXED).AsString;
    CHANNEL_TYPE_FIXED:=FieldByName(FLD_CHANNEL_TYPE_FIXED).AsString;

    FREQ_REQ_COUNT    :=FieldByName(FLD_FREQ_REQ_COUNT).AsInteger;

    FREQ_ALLOW     := FieldByName(FLD_FREQ_ALLOW).AsString;
    FREQ_FORBIDDEN := FieldByName(FLD_FREQ_FORBIDDEN).AsString;

    ChannelType         := Lowercase( FieldByName(FLD_CHANNEL_TYPE).AsString);
  //  iChType       := FieldByName(FLD_CHANNEL_NUMBER_TYPE).AsInteger;

  //  LinkEndType_ID:= FieldByName(FLD_LINKENDType_ID).AsInteger;
    LinkEndType_Mode_ID:= FieldByName(FLD_LINKENDType_Mode_ID).AsInteger;


    // -------------------------
    // MODE
    // -------------------------

    ChannelWidth      := FieldByName(FLD_CHANNEL_WIDTH).AsFloat;
    TxRx_Shift        := FieldByName(FLD_TXRX_SHIFT).AsFloat;
    Freq_Min_low_MHz  := FieldByName(FLD_FREQ_MIN_LOW).AsFloat;
    ChannelCount      := FieldByName(FLD_CHANNEL_COUNT).AsInteger;

*)
(*    ChSpacing:= FieldByName(FLD_ch_spacing).AsFloat;

    Bandwidth:= FieldByName(FLD_bandwidth).AsFloat;
*)


(*    arrStrAllow     := StringToStrArray();
    arrStrForbidden := StringToStrArray();
*)


(*    Next_Linkend_id:=FieldByName(FLD_NEXT_Linkend_id).AsInteger;

    LINKENDTYPE_ID :=FieldByName(FLD_LINKENDTYPE_ID).AsInteger;



    BAND              :=FieldByName(FLD_BAND).AsString;
   // TX_FREQ_MHz       :=FieldByName(FLD_TX_FREQ_MHz).AsFloat;

    LINKENDTYPE_ID   :=FieldByName(FLD_LINKENDTYPE_ID).AsInteger;

//    Rx_level_dBm      :=FieldByName(FLD_Rx_level_dBm).AsFloat;
    Power_dBm         :=FieldByName(FLD_POWER_dBm).AsFloat;

    Kng               :=FieldByName(FLD_Kng).AsFloat;
    LOSS_dBm          :=FieldByName(FLD_LOSS).AsFloat;

    FREQ_CHANNEL_COUNT:=FieldByName(FLD_FREQ_CHANNEL_COUNT).AsInteger;

    Freq_Spacing_MHz  :=FieldByName(FLD_Freq_Spacing).AsFloat;

    KRATNOST_BY_SPACE:=FieldByName(FLD_KRATNOST_BY_SPACE).AsInteger;
    KRATNOST_BY_FREQ :=FieldByName(FLD_KRATNOST_BY_FREQ).AsInteger;

    SIGNATURE_WIDTH := FieldByName(FLD_SIGNATURE_WIDTH).AsFloat;//,  'double; //  '������ ���������, ���');
    SIGNATURE_HEIGHT:= FieldByName(FLD_SIGNATURE_HEIGHT).AsFloat;//, 'double; // '������ ���������, ��');
    MODULATION_COUNT:= FieldByName(FLD_MODULATION_COUNT).AsFloat;//,double;// '����� ������� ���������');
    EQUALISER_PROFIT:= FieldByName(FLD_EQUALISER_PROFIT).AsFloat;//, 'double; // '�������, �������� ������������, ��');


    GROUND_HEIGHT  :=FieldByName(FLD_GROUND_HEIGHT).Value;

    // -------------------------
    // band
    // -------------------------
    FREQ_MAX_HIGH := FieldByName(FLD_FREQ_MAX_HIGH).AsFloat;
    FREQ_MIN_LOW  := FieldByName(FLD_FREQ_MIN_LOW).AsFloat;


    // -------------------------
    // Link
    // -------------------------
    Link_id    :=FieldByName(FLD_Link_id).AsInteger;
    Link_Name  :=FieldByName(FLD_Link_Name).AsString;
    ClutterModel_ID:= FieldByName(FLD_CLUTTER_MODEL_ID).AsInteger;

    // -------------------------
    // LinkEndType
    // -------------------------
    level_tx_a:=FieldByName(FLD_level_tx_a).AsInteger;
    level_rx_a:=FieldByName(FLD_level_rx_a).AsInteger;

    // -------------------------
    // MODE
    // -------------------------
    Bandwidth :=FieldByName(FLD_BANDWIDTH).AsFloat;

    BANDWIDTH_TX_30 :=FieldByName(FLD_BANDWIDTH_TX_30).AsFloat;
    BANDWIDTH_TX_3  :=FieldByName(FLD_BANDWIDTH_TX_3).AsFloat;
    BANDWIDTH_TX_A  :=FieldByName(FLD_BANDWIDTH_TX_A).AsFloat;


    BANDWIDTH_RX_30 :=FieldByName(FLD_BANDWIDTH_RX_30).AsFloat;
    BANDWIDTH_RX_3  :=FieldByName(FLD_BANDWIDTH_RX_3).AsFloat;
    BANDWIDTH_RX_A  :=FieldByName(FLD_BANDWIDTH_RX_A).AsFloat;

    THRESHOLD_BER_6   :=FieldByName(FLD_THRESHOLD_BER_6).asFloat;
    THRESHOLD_BER_3   :=FieldByName(FLD_THRESHOLD_BER_3).asFloat;

*)
  end;
end;

 // ---------------------------------------------------------------
procedure TLinkFreqPlan_calc_params.LoadFromDataset_BandResources(
    aDataset: TDataset);
// ---------------------------------------------------------------
var i: Integer;
begin
  SetLength(Band_Resources, aDataset.RecordCount);

  aDataset.First;

  with aDataset do
    while not EOF do
    begin
      i:=RecNo-1;
      Band_Resources[i].Band           := FieldByName(FLD_band).AsString;
      Band_Resources[i].FREQ_ALLOW     := FieldByName(FLD_FREQ_ALLOW).AsString;
      Band_Resources[i].FREQ_FORBIDDEN := FieldByName(FLD_FREQ_FORBIDDEN).AsString;

      Next;
    end;
end;


procedure TLinkFreqPlan_calc_params.LoadFromDataset_Neighbors(aDataset:
    TDataset);
begin
  with aDataset do
  begin
  //  Linkend_id :=FieldByName(FLD_Linkend_id).AsInteger;

  //  PROPERTY_ID       :=FieldByName(FLD_PROPERTY_ID).AsInteger;

  end;
end;

constructor TcLinkFreqPlan_Neighbor.Create(Collection: TCollection);
begin
  inherited Create(Collection);

end;

destructor TcLinkFreqPlan_Neighbor.Destroy;
begin

  inherited Destroy;
end;

 // ---------------------------------------------------------------
procedure TcLinkFreqPlan_Neighbor.LoadFromDataset(aDataset: TDataset);
// ---------------------------------------------------------------

var
  FREQ_ALLOW: string;
  FREQ_FORBIDDEN: string;
  LinkEndType_Mode_ID: Integer;
begin
  with aDataset do
  begin
    Linkend_id :=FieldByName(FLD_Linkend_id).AsInteger;

    PROPERTY_ID       :=FieldByName(FLD_PROPERTY_ID).AsInteger;

    // -------------------------
    // fixed
    // -------------------------
    FREQ_FIXED:=FieldByName(FLD_FREQ_FIXED).AsString;
    CHANNEL_TYPE_FIXED:=FieldByName(FLD_CHANNEL_TYPE_FIXED).AsString;

    FREQ_REQ_COUNT    :=FieldByName(FLD_FREQ_REQ_COUNT).AsInteger;

    FREQ_ALLOW     := FieldByName(FLD_FREQ_ALLOW).AsString;
    FREQ_FORBIDDEN := FieldByName(FLD_FREQ_FORBIDDEN).AsString;

    ChannelType         := Lowercase( FieldByName(FLD_CHANNEL_TYPE).AsString);
  //  iChType       := FieldByName(FLD_CHANNEL_NUMBER_TYPE).AsInteger;

  //  LinkEndType_ID:= FieldByName(FLD_LINKENDType_ID).AsInteger;
    LinkEndType_Mode_ID:= FieldByName(FLD_LINKENDType_Mode_ID).AsInteger;


    // -------------------------
    // MODE
    // -------------------------

    ChannelWidth      := FieldByName(FLD_CHANNEL_WIDTH).AsFloat;
    TxRx_Shift        := FieldByName(FLD_TXRX_SHIFT).AsFloat;
    Freq_Min_low_MHz  := FieldByName(FLD_FREQ_MIN_LOW).AsFloat;
    ChannelCount      := FieldByName(FLD_CHANNEL_COUNT).AsInteger;


(*    ChSpacing:= FieldByName(FLD_ch_spacing).AsFloat;

    Bandwidth:= FieldByName(FLD_bandwidth).AsFloat;
*)


(*    arrStrAllow     := StringToStrArray();
    arrStrForbidden := StringToStrArray();
*)


(*    Next_Linkend_id:=FieldByName(FLD_NEXT_Linkend_id).AsInteger;

    LINKENDTYPE_ID :=FieldByName(FLD_LINKENDTYPE_ID).AsInteger;



    BAND              :=FieldByName(FLD_BAND).AsString;
   // TX_FREQ_MHz       :=FieldByName(FLD_TX_FREQ_MHz).AsFloat;

    LINKENDTYPE_ID   :=FieldByName(FLD_LINKENDTYPE_ID).AsInteger;

//    Rx_level_dBm      :=FieldByName(FLD_Rx_level_dBm).AsFloat;
    Power_dBm         :=FieldByName(FLD_POWER_dBm).AsFloat;

    Kng               :=FieldByName(FLD_Kng).AsFloat;
    LOSS_dBm          :=FieldByName(FLD_LOSS).AsFloat;

    FREQ_CHANNEL_COUNT:=FieldByName(FLD_FREQ_CHANNEL_COUNT).AsInteger;

    Freq_Spacing_MHz  :=FieldByName(FLD_Freq_Spacing).AsFloat;

    KRATNOST_BY_SPACE:=FieldByName(FLD_KRATNOST_BY_SPACE).AsInteger;
    KRATNOST_BY_FREQ :=FieldByName(FLD_KRATNOST_BY_FREQ).AsInteger;

    SIGNATURE_WIDTH := FieldByName(FLD_SIGNATURE_WIDTH).AsFloat;//,  'double; //  '������ ���������, ���');
    SIGNATURE_HEIGHT:= FieldByName(FLD_SIGNATURE_HEIGHT).AsFloat;//, 'double; // '������ ���������, ��');
    MODULATION_COUNT:= FieldByName(FLD_MODULATION_COUNT).AsFloat;//,double;// '����� ������� ���������');
    EQUALISER_PROFIT:= FieldByName(FLD_EQUALISER_PROFIT).AsFloat;//, 'double; // '�������, �������� ������������, ��');


    GROUND_HEIGHT  :=FieldByName(FLD_GROUND_HEIGHT).Value;

    // -------------------------
    // band
    // -------------------------
    FREQ_MAX_HIGH := FieldByName(FLD_FREQ_MAX_HIGH).AsFloat;
    FREQ_MIN_LOW  := FieldByName(FLD_FREQ_MIN_LOW).AsFloat;


    // -------------------------
    // Link
    // -------------------------
    Link_id    :=FieldByName(FLD_Link_id).AsInteger;
    Link_Name  :=FieldByName(FLD_Link_Name).AsString;
    ClutterModel_ID:= FieldByName(FLD_CLUTTER_MODEL_ID).AsInteger;

    // -------------------------
    // LinkEndType
    // -------------------------
    level_tx_a:=FieldByName(FLD_level_tx_a).AsInteger;
    level_rx_a:=FieldByName(FLD_level_rx_a).AsInteger;

    // -------------------------
    // MODE
    // -------------------------
    Bandwidth :=FieldByName(FLD_BANDWIDTH).AsFloat;

    BANDWIDTH_TX_30 :=FieldByName(FLD_BANDWIDTH_TX_30).AsFloat;
    BANDWIDTH_TX_3  :=FieldByName(FLD_BANDWIDTH_TX_3).AsFloat;
    BANDWIDTH_TX_A  :=FieldByName(FLD_BANDWIDTH_TX_A).AsFloat;


    BANDWIDTH_RX_30 :=FieldByName(FLD_BANDWIDTH_RX_30).AsFloat;
    BANDWIDTH_RX_3  :=FieldByName(FLD_BANDWIDTH_RX_3).AsFloat;
    BANDWIDTH_RX_A  :=FieldByName(FLD_BANDWIDTH_RX_A).AsFloat;

    THRESHOLD_BER_6   :=FieldByName(FLD_THRESHOLD_BER_6).asFloat;
    THRESHOLD_BER_3   :=FieldByName(FLD_THRESHOLD_BER_3).asFloat;

*)
  end;
end;

constructor TLinkFreqPlan_Neighbor_List.Create;
begin
  inherited Create(TcLinkFreqPlan_Neighbor);
end;

function TLinkFreqPlan_Neighbor_List.AddItem: TcLinkFreqPlan_Neighbor;
begin
  Result := TcLinkFreqPlan_Neighbor (inherited Add);
end;

function TLinkFreqPlan_Neighbor_List.GetItems(Index: Integer):
    TcLinkFreqPlan_Neighbor;
begin
  Result := TcLinkFreqPlan_Neighbor(inherited Items[Index]);
end;

procedure TLinkFreqPlan_Neighbor_List.LoadFromDataset(aDataset: TDataset);
begin
  Clear;
  aDataset.First;

  while not aDataset.EOF do
  begin
    AddItem.LoadFromDataset(aDataset);
    aDataset.Next;
  end;
end;


end.
