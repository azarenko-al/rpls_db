object dmLinkFreqPlan_calc: TdmLinkFreqPlan_calc
  OldCreateOrder = True
  OnCreate = DataModuleCreate
  OnDestroy = DataModuleDestroy
  Left = 668
  Top = 303
  Height = 361
  Width = 539
  object qry_RRL_Neighbors: TADOQuery
    Parameters = <>
    Left = 52
    Top = 8
  end
  object mem_linkend: TdxMemData
    Indexes = <>
    SortOptions = []
    Left = 260
    Top = 28
  end
  object mem_MatrixCI: TdxMemData
    Indexes = <>
    SortOptions = []
    Left = 168
    Top = 28
  end
  object mem_MatrixTrf: TdxMemData
    Indexes = <>
    SortOptions = []
    Left = 168
    Top = 80
  end
  object qry_LinkFreqPlan: TADOQuery
    Parameters = <>
    Left = 51
    Top = 74
  end
  object qry_LinkFreqPlan_LinkEnd: TADOQuery
    Parameters = <>
    Left = 51
    Top = 142
  end
  object mem_MatrixCA: TdxMemData
    Indexes = <>
    SortOptions = []
    Left = 168
    Top = 140
  end
  object qry_LinkEnd: TADOQuery
    Parameters = <>
    Left = 48
    Top = 204
  end
  object qry_Temp: TADOQuery
    LockType = ltReadOnly
    Parameters = <>
    Left = 265
    Top = 187
  end
  object sp_FreqPlan_LinkEnd: TADOStoredProc
    Parameters = <>
    Left = 400
    Top = 16
  end
  object qry_LinkEnd1: TADOQuery
    Parameters = <>
    Left = 416
    Top = 228
  end
end
