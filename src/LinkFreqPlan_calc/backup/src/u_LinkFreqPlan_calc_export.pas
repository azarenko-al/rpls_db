unit u_LinkFreqPlan_calc_export;

interface

uses
  Classes, SysUtils,

  u_func,
//  u_xml   ,
   XMLDoc, XMLIntf,u_xml_document,
  DB;

type
  TLinkFreqPlan_LinkEnd_List = class;


  // ---------------------------------------------------------------
  TLinkFreqPlan_LinkEnd = class(TObject)
  // ---------------------------------------------------------------
  public
    LinkEnd_ID      : Integer;
    FreqFixed       : string;
    ChannelsAllowed : string;

    DuplexFreqDelta : double;
    ChannelWidth    : Double;
    FreqMin         : Double;

    Range           : string;



// xml_AddNode (aNode, 'SECTOR',
//          [xml_Att('id',                aLinkEndID),
//
//            //� 1-�� ������ �������� ������, �� 2-�� - ������ "����. ����"
//           xml_Att('SiteID',  IIF(FParams.SiteFreqSpacing>0, iPropID, aCount)),
//
//           xml_Att('FreqRequiredCount', iFreqRecCount),
//           xml_Att('FreqFixed',         sFixed),
//           xml_Att('FreqForbidden',     ''),
//           xml_Att('total_cells',       1),
//           xml_Att('ChannelsAllowed',   sChAllowed),
//
//// ��������� 3 ��������� ��������� �� 100 ��� �������� � ������� ���
//           xml_Att('DuplexFreqDelta',   dTxRx_Shift*100),
//           xml_Att('ChannelWidth',      dChWidth*100),
//           xml_Att('FreqMin',           dFreqMin*100),
//           xml_Att('Range',             sBand),
//
//           // Added by alex 09.05.2010 7:52:37
//           //new
//           xml_Att('Band',              sBand)
//          ]);

  end;



  // ---------------------------------------------------------------
  TLinkFreqPlan_LinkEnd_List = class(TList)
  // ---------------------------------------------------------------
  private
    function GetItems(Index: Integer): TLinkFreqPlan_LinkEnd;
  public
    function AddItem: TLinkFreqPlan_LinkEnd;

    property Items[Index: Integer]: TLinkFreqPlan_LinkEnd read GetItems; default;
  end;

  // ---------------------------------------------------------------
  TLinkFreqPlan_calc_export_params = class(TObject)
  // ---------------------------------------------------------------
  public
    LinkEnds: TLinkFreqPlan_LinkEnd_List;

    // -------------------------

    LinkFreqPlan: record
      SectorFreqSpacing   : double;  // ������ ������ � �������
      SiteFreqSpacing   : double;  // ������ ������ �������� � ������� �������
//      SitesFreqSpacing    : double;  // ������ ������ �������� ������ ������� �������
      StartSectorNum  : integer; // ����� �������� ����� �������������
      PereborQueueType : integer;
      FreqDistribType : integer; // 0-dtOneFreqForSector, 1-dtAllFreqsForSector);
      MaxIterCount : integer;
      PereborOrderType    : integer; //0-opForward, 1-opBackward, 2-opRandom);
      MaxFreqRepeat       : integer; //max �����.�������� ������
    end;


    constructor Create;
    destructor Destroy; override;

    procedure LoadFromDataset_LinkFreqPlan(aDataset: TDataset);

    procedure SaveToXML(aFileName, aMatrixFileDir: string);
  end;



implementation
const
  MATRIX_CI_FILENAME  = 'freq_params.ByCI';
  MATRIX_CA_FILENAME  = 'freq_params.ByCA';
  MATRIX_TRF_FILENAME = 'freq_params.ByTrfCI';



constructor TLinkFreqPlan_calc_export_params.Create;
begin
  inherited Create;
  LinkEnds := TLinkFreqPlan_LinkEnd_List.Create();
end;

destructor TLinkFreqPlan_calc_export_params.Destroy;
begin
  FreeAndNil(LinkEnds);
  inherited Destroy;
end;

 // ---------------------------------------------------------------
procedure TLinkFreqPlan_calc_export_params.LoadFromDataset_LinkFreqPlan(
    aDataset: TDataset);
// ---------------------------------------------------------------

var
  FREQ_ALLOW: string;
  FREQ_FORBIDDEN: string;
  LinkEndType_Mode_ID: Integer;
begin
  with aDataset do
  begin
   (* Linkend_id :=FieldByName(FLD_Linkend_id).AsInteger;

    PROPERTY_ID       :=FieldByName(FLD_PROPERTY_ID).AsInteger;

    // -------------------------
    // fixed
    // -------------------------
    FREQ_FIXED:=FieldByName(FLD_FREQ_FIXED).AsString;
    CHANNEL_TYPE_FIXED:=FieldByName(FLD_CHANNEL_TYPE_FIXED).AsString;

    FREQ_REQ_COUNT    :=FieldByName(FLD_FREQ_REQ_COUNT).AsInteger;

    FREQ_ALLOW     := FieldByName(FLD_FREQ_ALLOW).AsString;
    FREQ_FORBIDDEN := FieldByName(FLD_FREQ_FORBIDDEN).AsString;

    ChannelType         := Lowercase( FieldByName(FLD_CHANNEL_TYPE).AsString);
  //  iChType       := FieldByName(FLD_CHANNEL_NUMBER_TYPE).AsInteger;

  //  LinkEndType_ID:= FieldByName(FLD_LINKENDType_ID).AsInteger;
    LinkEndType_Mode_ID:= FieldByName(FLD_LINKENDType_Mode_ID).AsInteger;


    // -------------------------
    // MODE
    // -------------------------

    ChannelWidth      := FieldByName(FLD_CHANNEL_WIDTH).AsFloat;
    TxRx_Shift        := FieldByName(FLD_TXRX_SHIFT).AsFloat;
    Freq_Min_low_MHz  := FieldByName(FLD_FREQ_MIN_LOW).AsFloat;
    ChannelCount      := FieldByName(FLD_CHANNEL_COUNT).AsInteger;

*)
(*    ChSpacing:= FieldByName(FLD_ch_spacing).AsFloat;

    Bandwidth:= FieldByName(FLD_bandwidth).AsFloat;
*)


(*    arrStrAllow     := StringToStrArray();
    arrStrForbidden := StringToStrArray();
*)


(*    Next_Linkend_id:=FieldByName(FLD_NEXT_Linkend_id).AsInteger;

    LINKENDTYPE_ID :=FieldByName(FLD_LINKENDTYPE_ID).AsInteger;



    BAND              :=FieldByName(FLD_BAND).AsString;
   // TX_FREQ_MHz       :=FieldByName(FLD_TX_FREQ_MHz).AsFloat;

    LINKENDTYPE_ID   :=FieldByName(FLD_LINKENDTYPE_ID).AsInteger;

//    Rx_level_dBm      :=FieldByName(FLD_Rx_level_dBm).AsFloat;
    Power_dBm         :=FieldByName(FLD_POWER_dBm).AsFloat;

    Kng               :=FieldByName(FLD_Kng).AsFloat;
    LOSS_dBm          :=FieldByName(FLD_LOSS).AsFloat;

    FREQ_CHANNEL_COUNT:=FieldByName(FLD_FREQ_CHANNEL_COUNT).AsInteger;

    Freq_Spacing_MHz  :=FieldByName(FLD_Freq_Spacing).AsFloat;

    KRATNOST_BY_SPACE:=FieldByName(FLD_KRATNOST_BY_SPACE).AsInteger;
    KRATNOST_BY_FREQ :=FieldByName(FLD_KRATNOST_BY_FREQ).AsInteger;

    SIGNATURE_WIDTH := FieldByName(FLD_SIGNATURE_WIDTH).AsFloat;//,  'double; //  '������ ���������, ���');
    SIGNATURE_HEIGHT:= FieldByName(FLD_SIGNATURE_HEIGHT).AsFloat;//, 'double; // '������ ���������, ��');
    MODULATION_COUNT:= FieldByName(FLD_MODULATION_COUNT).AsFloat;//,double;// '����� ������� ���������');
    EQUALISER_PROFIT:= FieldByName(FLD_EQUALISER_PROFIT).AsFloat;//, 'double; // '�������, �������� ������������, ��');


    GROUND_HEIGHT  :=FieldByName(FLD_GROUND_HEIGHT).Value;

    // -------------------------
    // band
    // -------------------------
    FREQ_MAX_HIGH := FieldByName(FLD_FREQ_MAX_HIGH).AsFloat;
    FREQ_MIN_LOW  := FieldByName(FLD_FREQ_MIN_LOW).AsFloat;


    // -------------------------
    // Link
    // -------------------------
    Link_id    :=FieldByName(FLD_Link_id).AsInteger;
    Link_Name  :=FieldByName(FLD_Link_Name).AsString;
    ClutterModel_ID:= FieldByName(FLD_CLUTTER_MODEL_ID).AsInteger;

    // -------------------------
    // LinkEndType
    // -------------------------
    level_tx_a:=FieldByName(FLD_level_tx_a).AsInteger;
    level_rx_a:=FieldByName(FLD_level_rx_a).AsInteger;

    // -------------------------
    // MODE
    // -------------------------
    Bandwidth :=FieldByName(FLD_BANDWIDTH).AsFloat;

    BANDWIDTH_TX_30 :=FieldByName(FLD_BANDWIDTH_TX_30).AsFloat;
    BANDWIDTH_TX_3  :=FieldByName(FLD_BANDWIDTH_TX_3).AsFloat;
    BANDWIDTH_TX_A  :=FieldByName(FLD_BANDWIDTH_TX_A).AsFloat;


    BANDWIDTH_RX_30 :=FieldByName(FLD_BANDWIDTH_RX_30).AsFloat;
    BANDWIDTH_RX_3  :=FieldByName(FLD_BANDWIDTH_RX_3).AsFloat;
    BANDWIDTH_RX_A  :=FieldByName(FLD_BANDWIDTH_RX_A).AsFloat;

    THRESHOLD_BER_6   :=FieldByName(FLD_THRESHOLD_BER_6).asFloat;
    THRESHOLD_BER_3   :=FieldByName(FLD_THRESHOLD_BER_3).asFloat;

*)
  end;
end;


procedure TLinkFreqPlan_calc_export_params.SaveToXML(aFileName, aMatrixFileDir: string);
var
  vRoot,vParams,vSectors: IXMLNode;
  i: Integer;

  oXMLDoc: TXMLDoc;
begin
  oXMLDoc:=TXMLDoc.Create;

  vRoot:=oXMLDoc.DocumentElement.AddChild( 'FREQ_PLAN');
  vParams:=vRoot.AddChild('params');

    xml_AddNode (vParams, 'ChannelsAllowed', [xml_Att(ATT_value, '1-124')]);
    xml_AddNode (vParams, 'DuplexFreqDelta', [xml_Att(ATT_value, 45)]);
    xml_AddNode (vParams, 'ChannelWidth',    [xml_Att(ATT_value, 200)]);

    xml_AddNode (vParams, 'MaxCI',             [xml_Att(ATT_value, 12)]);
    xml_AddNode (vParams, 'MaxCA',             [xml_Att(ATT_value, 3)]);
    xml_AddNode (vParams, 'NeighborMatrixType',[xml_Att(ATT_value, 2)]);  //2!!!
    xml_AddNode (vParams, 'CI',                [xml_Att(ATT_value, 12)]);
    xml_AddNode (vParams, 'CA',                [xml_Att(ATT_value, 9)]);
    xml_AddNode (vParams, 'SitesFreqSpacing',  [xml_Att(ATT_value, 0)]);//  Params.SitesFreqSpacing)]);
    xml_AddNode (vParams, 'SectorFreqSpacing', [xml_Att(ATT_value, LinkFreqPlan.SectorFreqSpacing)]);
    xml_AddNode (vParams, 'SiteFreqSpacing',   [xml_Att(ATT_value,
          IIF(LinkFreqPlan.SiteFreqSpacing>0, LinkFreqPlan.SiteFreqSpacing-1, 0))]);

    xml_AddNode (vParams, 'IsUseGarmonic_BS_PO', [xml_Att(ATT_value, 0)]);
    xml_AddNode (vParams, 'IsUseGarmonic_BS_BS', [xml_Att(ATT_value, 0)]);

    xml_AddNode (vParams, 'PercentCI',          [xml_Att(ATT_value, 0)]);
    xml_AddNode (vParams, 'PercentTrf',         [xml_Att(ATT_value, 0)]);

    xml_AddNode (vParams, 'MaxPorogPercent',        [xml_Att(ATT_value, 100)]);

    xml_AddNode (vParams, 'SectorInterferenceType', [xml_Att(ATT_value, 0)]);
    xml_AddNode (vParams, 'IsLossQualityByAllFreq', [xml_Att(ATT_value, 0)]);
    xml_AddNode (vParams, 'IsLossQualityByHandoff', [xml_Att(ATT_value, 0)]);
    xml_AddNode (vParams, 'BestSecPereborType',     [xml_Att(ATT_value, 0)]);

    xml_AddNode (vParams, 'StartSectorNum',     [xml_Att(ATT_value, LinkFreqPlan.StartSectorNum)]);
    xml_AddNode (vParams, 'PereborQueueType',   [xml_Att(ATT_value, LinkFreqPlan.PereborQueueType)]);
    xml_AddNode (vParams, 'FreqDistribType',    [xml_Att(ATT_value, LinkFreqPlan.FreqDistribType)]);
    xml_AddNode (vParams, 'MaxIterCount',       [xml_Att(ATT_value, LinkFreqPlan.MaxIterCount)]);
    xml_AddNode (vParams, 'PereborOrderType',   [xml_Att(ATT_value, LinkFreqPlan.PereborOrderType)]);
    xml_AddNode (vParams, 'IsUniformFreqRepeat',[xml_Att(ATT_value, 0)]);
    xml_AddNode (vParams, 'MaxFreqRepeat',      [xml_Att(ATT_value, LinkFreqPlan.MaxFreqRepeat)]);

    xml_AddNode (vParams, 'BestFreqPereborType',[xml_Att(ATT_value, 0)]);
    xml_AddNode (vParams, 'NotAllowFreqRepeat', [xml_Att(ATT_value, 0)]);


(*
  vSectors:=xml_AddNode (vRoot, 'SECTORS', [xml_Att('count', LinkEnds.Count)]);
  for i := 0 to aIDList.Count-1 do
    AddLinkEnd(aIDList[i].ID, vSectors, i + 1);

*)

  xml_AddNode (vRoot, 'NeighborsByCI',
                  [xml_Att('FILENAME', aMatrixFileDir + MATRIX_CI_FILENAME)]);
  xml_AddNode (vRoot, 'NeighborsByCA',
                  [xml_Att('FILENAME', aMatrixFileDir + MATRIX_CA_FILENAME)]);
  xml_AddNode (vRoot, 'NeighborsByTrfCI',
                  [xml_Att('FILENAME', aMatrixFileDir + MATRIX_TRF_FILENAME)]);

  oXMLDoc.SaveToFile(aFileName);

  oXMLDoc.Free;

end;


function TLinkFreqPlan_LinkEnd_List.AddItem: TLinkFreqPlan_LinkEnd;
begin
  Result := TLinkFreqPlan_LinkEnd.Create;
  Add(Result);
end;

function TLinkFreqPlan_LinkEnd_List.GetItems(Index: Integer):
    TLinkFreqPlan_LinkEnd;
begin
  Result := TLinkFreqPlan_LinkEnd(inherited Items[Index]);
end;

end.

{


//--------------------------------------------------------------------
procedure TdmLinkFreqPlan_calc.SaveToXML(aFileName: string; aIDList: TIDList;
    aMatrixFileDir: string);
//--------------------------------------------------------------------
var vRoot,vParams,vSectors: IXMLNode;
    i: Integer;
var
  oXMLDoc: TXMLDoc;
begin
  oXMLDoc:=TXMLDoc.Create('');

  vRoot:=xml_AddNodeTag (oXMLDoc.DocumentElement, 'FREQ_PLAN');
  vParams:=xml_AddNodeTag (vRoot, 'params');

    xml_AddNode (vParams, 'ChannelsAllowed', [xml_Att(ATT_value, '1-124')]);
    xml_AddNode (vParams, 'DuplexFreqDelta', [xml_Att(ATT_value, 45)]);
    xml_AddNode (vParams, 'ChannelWidth',    [xml_Att(ATT_value, 200)]);

    xml_AddNode (vParams, 'MaxCI',             [xml_Att(ATT_value, 12)]);
    xml_AddNode (vParams, 'MaxCA',             [xml_Att(ATT_value, 3)]);
    xml_AddNode (vParams, 'NeighborMatrixType',[xml_Att(ATT_value, 2)]);  //2!!!
    xml_AddNode (vParams, 'CI',                [xml_Att(ATT_value, 12)]);
    xml_AddNode (vParams, 'CA',                [xml_Att(ATT_value, 9)]);
    xml_AddNode (vParams, 'SitesFreqSpacing',  [xml_Att(ATT_value, 0)]);//  Params.SitesFreqSpacing)]);
    xml_AddNode (vParams, 'SectorFreqSpacing', [xml_Att(ATT_value, Params.SectorFreqSpacing)]);
    xml_AddNode (vParams, 'SiteFreqSpacing',   [xml_Att(ATT_value,
          IIF(Params.SiteFreqSpacing>0, Params.SiteFreqSpacing-1, 0))]);

    xml_AddNode (vParams, 'IsUseGarmonic_BS_PO', [xml_Att(ATT_value, 0)]);
    xml_AddNode (vParams, 'IsUseGarmonic_BS_BS', [xml_Att(ATT_value, 0)]);

    xml_AddNode (vParams, 'PercentCI',          [xml_Att(ATT_value, 0)]);
    xml_AddNode (vParams, 'PercentTrf',         [xml_Att(ATT_value, 0)]);

    xml_AddNode (vParams, 'MaxPorogPercent',        [xml_Att(ATT_value, 100)]);
    xml_AddNode (vParams, 'SectorInterferenceType', [xml_Att(ATT_value, 0)]);
    xml_AddNode (vParams, 'IsLossQualityByAllFreq', [xml_Att(ATT_value, 0)]);
    xml_AddNode (vParams, 'IsLossQualityByHandoff', [xml_Att(ATT_value, 0)]);
    xml_AddNode (vParams, 'BestSecPereborType',     [xml_Att(ATT_value, 0)]);

    xml_AddNode (vParams, 'StartSectorNum',     [xml_Att(ATT_value, Params.StartSectorNum)]);
    xml_AddNode (vParams, 'PereborQueueType',   [xml_Att(ATT_value, Params.PereborQueueType)]);
    xml_AddNode (vParams, 'FreqDistribType',    [xml_Att(ATT_value, Params.FreqDistribType)]);
    xml_AddNode (vParams, 'MaxIterCount',       [xml_Att(ATT_value, Params.MaxIterCount)]);
    xml_AddNode (vParams, 'PereborOrderType',   [xml_Att(ATT_value, Params.PereborOrderType)]);
    xml_AddNode (vParams, 'IsUniformFreqRepeat',[xml_Att(ATT_value, 0)]);
    xml_AddNode (vParams, 'MaxFreqRepeat',      [xml_Att(ATT_value, Params.MaxFreqRepeat)]);

    xml_AddNode (vParams, 'BestFreqPereborType',[xml_Att(ATT_value, 0)]);
    xml_AddNode (vParams, 'NotAllowFreqRepeat', [xml_Att(ATT_value, 0)]);

  vSectors:=xml_AddNode (vRoot, 'SECTORS', [xml_Att('count', aIDList.Count)]);
  for i := 0 to aIDList.Count-1 do
    AddLinkEnd(aIDList[i].ID, vSectors, i + 1);

  xml_AddNode (vRoot, 'NeighborsByCI',
                  [xml_Att('FILENAME', aMatrixFileDir + MATRIX_CI_FILENAME)]);
  xml_AddNode (vRoot, 'NeighborsByCA',
                  [xml_Att('FILENAME', aMatrixFileDir + MATRIX_CA_FILENAME)]);
  xml_AddNode (vRoot, 'NeighborsByTrfCI',
                  [xml_Att('FILENAME', aMatrixFileDir + MATRIX_TRF_FILENAME)]);

  oXMLDoc.SaveToFile(aFileName);

  oXMLDoc.Free;

end;


end;


