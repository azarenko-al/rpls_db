unit dm_LinkFreqPlan_calc;

interface

uses
  SysUtils, Classes, Forms, Db, ADODB, dxmdaset, Math, Variants,
  XMLDoc, XMLIntf,

  u_xml_document,
  u_FreqPlan_calc_export,


  u_LinkFreqPlan_calc_export,

  dm_LinkEnd_ex,

  dm_Main,

  u_vars,

  u_const,
  u_const_db,

  u_Link_const,
  u_LinkFreqPlan_const,

  u_Log,
  u_db,

  u_files,
//  u_XML,

  u_Func_arrays,  
  u_func,
  u_dlg,

  u_types,

//  dm_LinkEnd,

  u_LinkFreqPlan_calc_classes;

type
  TdmLinkFreqPlan_calc = class(TDataModule)
    qry_RRL_Neighbors: TADOQuery;
    mem_linkend: TdxMemData;
    mem_MatrixCI: TdxMemData;
    mem_MatrixTrf: TdxMemData;
    qry_LinkFreqPlan: TADOQuery;
    qry_LinkFreqPlan_LinkEnd: TADOQuery;
    mem_MatrixCA: TdxMemData;
    qry_LinkEnd: TADOQuery;
    qry_Temp: TADOQuery;
    sp_FreqPlan_LinkEnd: TADOStoredProc;
    qry_LinkEnd1: TADOQuery;
    procedure DataModuleDestroy(Sender: TObject);
    procedure DataModuleCreate(Sender: TObject);

  private
 //   FLinkends: TcLinkFreqPlan_Linkend_List;

    FFreqPlan_calc_export: TFreqPlan_calc_export_params;

    FExportParams: TLinkFreqPlan_calc_params;


    FParams_: record
      SectorFreqSpacing   : double;  // ������ ������ � �������
      SiteFreqSpacing   : double;  // ������ ������ �������� � ������� �������
//      SitesFreqSpacing    : double;  // ������ ������ �������� ������ ������� �������
      StartSectorNum  : integer; // ����� �������� ����� �������������
      PereborQueueType : integer;
      FreqDistribType : integer; // 0-dtOneFreqForSector, 1-dtAllFreqsForSector);
      MaxIterCount : integer;
      PereborOrderType    : integer; //0-opForward, 1-opBackward, 2-opRandom);
      MaxFreqRepeat       : integer; //max �����.�������� ������
    end;


    function  GetForbidden(aLinkEndID : integer; aFirstFreq, aChannelWidth : double; aChannelArray: TIntArray): TIntArray;

    function GetFreq_Resource(aLinkEndID: integer; var aTxRx_Shift, aChannelWidth,
        aFreqMin: double; var aChannelsAllowed: string): boolean;

    procedure AddLinkEnd(aLinkEndID: integer; aNode: IXMLNode; aCount: Integer);

    procedure LoadParams ();
    procedure OpenData ();

    procedure SaveToXML(aFileName, aMatrixFileDir: string);
    procedure Load_XMLToDB (aFileName: string);

    procedure SaveToXML_new(aFileName, aMatrixFileDir: string);

  public
    Fcalc_params: TLinkFreqPlan_calc_export_params;


    Params: record
      LinkFreqPlan_ID: integer;
    end;

    function Execute: boolean;

    procedure Execute_all;


    class procedure Init;

  end;

var
  dmLinkFreqPlan_calc: TdmLinkFreqPlan_calc;


//function dmLinkFreqPlan_calc: TdmLinkFreqPlan_calc;

//====================================================================
//====================================================================
implementation
 {$R *.DFM}

const
  view_LINKFREQPLAN_linkend_for_calc = 'view_LINKFREQPLAN_linkend_for_calc';

const

  TEMP_IN_FILENAME  = 'LinkFreqPlan_calc_params.xml';

  MATRIX_CI_FILENAME  = 'freq_params.ByCI';
  MATRIX_CA_FILENAME  = 'freq_params.ByCA';
  MATRIX_TRF_FILENAME = 'freq_params.ByTrfCI';



//--------------------------------------------------------------------
procedure TdmLinkFreqPlan_calc.DataModuleCreate(Sender: TObject);
//--------------------------------------------------------------------
begin
  inherited;

  db_SetComponentADOConnection(Self, dmMain.ADOConnection);

  Fcalc_params:=TLinkFreqPlan_calc_export_params.Create;


  db_CreateField (mem_MatrixCI,  [db_Field('CELL1_ID', ftInteger),
                                  db_Field('CELL2_ID', ftInteger),
                                  db_Field('cell_count', ftInteger)
                                 ]);
  db_CreateField (mem_MatrixCA,  [db_Field('CELL1_ID', ftInteger),
                                  db_Field('CELL2_ID', ftInteger),
                                  db_Field('cell_count', ftInteger)
                                 ]);
  db_CreateField (mem_MatrixTrf, [db_Field('CELL1_ID', ftInteger),
                                  db_Field('CELL2_ID', ftInteger),
                                  db_Field('cell_count', ftInteger)
                                 ]);

  FFreqPlan_calc_export := TFreqPlan_calc_export_params.Create();

  FExportParams := TLinkFreqPlan_calc_params.Create();
end;


procedure TdmLinkFreqPlan_calc.DataModuleDestroy(Sender: TObject);
begin
  FreeAndNil(FExportParams);
  FreeAndNil(FFreqPlan_calc_export);

  FreeAndNil(Fcalc_params);
end;


//--------------------------------------------------------------------
procedure TdmLinkFreqPlan_calc.LoadParams ();
//--------------------------------------------------------------------
const
  SQL_SELECT_FREQPLAN_RRL_PARAMS =
     'SELECT * FROM '+ TBL_LINKFREQPLAN +' WHERE id=:id';

begin
  Assert(Params.LinkFreqPlan_ID>0);

  db_OpenQuery (qry_LinkFreqPlan, SQL_SELECT_FREQPLAN_RRL_PARAMS,
                   [db_par(FLD_ID, Params.LinkFreqPlan_ID)]);


  with qry_LinkFreqPlan do
    if not EOF then
  begin
//    FParams.SitesFreqSpacing := FieldByName(FLD_SitesFreqSpacing).AsInteger;
    FParams_.SectorFreqSpacing:= FieldByName(FLD_SectorFreqSpacing).AsInteger;
    FParams_.SiteFreqSpacing  := FieldByName(FLD_SiteFreqSpacing).AsInteger;
    FParams_.StartSectorNum   := FieldByName(FLD_StartSectorNum).AsInteger;
    FParams_.PereborQueueType := FieldByName(FLD_PereborQueueType).AsInteger;
    FParams_.FreqDistribType  := FieldByName(FLD_FreqDistribType).AsInteger;
    FParams_.MaxIterCount     := FieldByName(FLD_MaxIterCount).AsInteger;
    FParams_.PereborOrderType := FieldByName(FLD_PereborOrderType).AsInteger;
    FParams_.MaxFreqRepeat    := FieldByName(FLD_MaxFreqRepeat).AsInteger;
  end;


  FExportParams.LoadFromDataset_LinkFreqPlan(qry_LinkFreqPlan);

//  Fcalc_params.


(*
 with qry_LinkFreqPlan, Fcalc_params do
    if not EOF then
  begin
//    FParams.SitesFreqSpacing := FieldByName(FLD_SitesFreqSpacing).AsInteger;
   SectorFreqSpacing:= FieldByName(FLD_SectorFreqSpacing).AsInteger;
   SiteFreqSpacing  := FieldByName(FLD_SiteFreqSpacing).AsInteger;
   StartSectorNum   := FieldByName(FLD_StartSectorNum).AsInteger;
   PereborQueueType := FieldByName(FLD_PereborQueueType).AsInteger;
   FreqDistribType  := FieldByName(FLD_FreqDistribType).AsInteger;
   MaxIterCount     := FieldByName(FLD_MaxIterCount).AsInteger;
   PereborOrderType := FieldByName(FLD_PereborOrderType).AsInteger;
   MaxFreqRepeat    := FieldByName(FLD_MaxFreqRepeat).AsInteger;
  end;

*)


end;

//--------------------------------------------------------------------
procedure TdmLinkFreqPlan_calc.OpenData ();
//--------------------------------------------------------------------
const
  SQL_SELECT_RRL_NEIGHBORS =

     'SELECT * FROM '+ TBL_LINKFREQPLAN_NEIGHBORS +
     ' WHERE LinkFreqPlan_id=:LinkFreqPlan_id';

var iFreqSpacing : integer;
begin

  mem_MatrixCI.Close;
  mem_MatrixCA.Close;
  mem_MatrixTrf.Close;

  mem_MatrixCI.Open;
  mem_MatrixCA.Open;
  mem_MatrixTrf.Open;


  db_OpenQuery (qry_RRL_Neighbors, SQL_SELECT_RRL_NEIGHBORS,
                [db_par(FLD_LINKFREQPLAN_ID, Params.LinkFreqPlan_ID)]);


  if qry_RRL_Neighbors.RecordCount=0 then
    g_Log.Add('���-�� �������=0');


//  qry_RRL_Neighbors.First;
  with qry_RRL_Neighbors do
    while not Eof do
  begin
    if not FieldByName(FLD_IsLink).AsBoolean and
       not (FieldByName(FLD_IsPropertyOne).AsBoolean and (FParams_.SiteFreqSpacing>0))
    then
      begin
        FFreqPlan_calc_export.NeighborsByCI.AddItem
                (FieldValues[FLD_TX_LINKEND_ID],
                 FieldValues[FLD_TX_LINK_ID],
                 FieldValues[FLD_IsNeigh]);


        db_AddRecord(mem_MatrixCI, [db_par('CELL1_ID',  FieldValues[FLD_TX_LINKEND_ID]),
                                    db_par('CELL2_ID',  FieldValues[FLD_TX_LINK_ID]),
                                    db_par('cell_count',FieldValues[FLD_IsNeigh])
                                    ]);

      end;




    db_AddRecord(mem_MatrixTrf,[db_par('CELL1_ID',  FieldValues[FLD_TX_LINKEND_ID]),
                                db_par('CELL2_ID',  FieldValues[FLD_TX_LINK_ID]),
                                db_par('cell_count',FieldValues[FLD_IsLink])
                                ]);

    iFreqSpacing:= Ceil(FieldByName(FLD_FREQ_SPACING).AsFloat*100);

    FFreqPlan_calc_export.NeighborsByCA.AddItem
            (FieldValues[FLD_TX_LINKEND_ID],
             FieldValues[FLD_TX_LINK_ID],
             iFreqSpacing);

    db_AddRecord(mem_MatrixCA, [db_par('CELL1_ID',  FieldValues[FLD_TX_LINKEND_ID]),
                                db_par('CELL2_ID',  FieldValues[FLD_TX_LINK_ID]),
                                db_par('cell_count', iFreqSpacing)
                                ]);
    Next;
  end;
end;



//--------------------------------------------------------------------
procedure TdmLinkFreqPlan_calc.SaveToXML_new(aFileName, aMatrixFileDir: string);
//--------------------------------------------------------------------
begin
  with FFreqPlan_calc_export do
  begin
    ChannelsAllowed    := '1-124';
    DuplexFreqDelta    := 45;
    ChannelWidth       := 200;

    MaxCI              := 12;
    MaxCA              := 3;
    NeighborMatrixType := 2;
    CI                 := 12;
    CA                 := 9;
    SitesFreqSpacing   := 0;
    SectorFreqSpacing  := SectorFreqSpacing;
    SiteFreqSpacing    := IIF(SiteFreqSpacing>0, SiteFreqSpacing-1, 0);

    IsUseGarmonic_BS_PO    := False;
    IsUseGarmonic_BS_BS    := False;

    PercentCI              := 0;
    PercentTrf             := 0;

    MaxPorogPercent        := 100;
    SectorInterferenceType := 0;
    IsLossQualityByAllFreq := 0;
    IsLossQualityByHandoff := 0;
    BestSecPereborType     := 0;

    StartSectorNum:=         FParams_.StartSectorNum;
    PereborQueueType:=       FParams_.PereborQueueType;
    FreqDistribType:=        FParams_.FreqDistribType;
    MaxIterCount:=           FParams_.MaxIterCount;
    PereborOrderType:=       FParams_.PereborOrderType;
    IsUniformFreqRepeat:=    0;
    MaxFreqRepeat :=         FParams_.MaxFreqRepeat;

    BestFreqPereborType :=   0;
    NotAllowFreqRepeat :=    0;

    NeighborsByCI_FILENAME   := aMatrixFileDir + MATRIX_CI_FILENAME;
    NeighborsByCA_FILENAME   := aMatrixFileDir + MATRIX_CA_FILENAME;
    NeighborsByTrf_FILENAME:= aMatrixFileDir + MATRIX_TRF_FILENAME;

  end;


end;



//--------------------------------------------------------------------
procedure TdmLinkFreqPlan_calc.SaveToXML(aFileName, aMatrixFileDir: string);
//--------------------------------------------------------------------
var vRoot,vParams,vSectors: IXMLNode;
    i: Integer;
var
  oXMLDoc: TXMLDoc;
begin
  oXMLDoc:=TXMLDoc.Create;

  vRoot:=oXMLDoc.DocumentElement.AddChild('FREQ_PLAN');
  vParams:=vRoot.AddChild('params');

    xml_AddNode (vParams, 'ChannelsAllowed', [xml_Att(ATT_value, '1-124')]);
    xml_AddNode (vParams, 'DuplexFreqDelta', [xml_Att(ATT_value, 45)]);
    xml_AddNode (vParams, 'ChannelWidth',    [xml_Att(ATT_value, 200)]);

    xml_AddNode (vParams, 'MaxCI',             [xml_Att(ATT_value, 12)]);
    xml_AddNode (vParams, 'MaxCA',             [xml_Att(ATT_value, 3)]);
    xml_AddNode (vParams, 'NeighborMatrixType',[xml_Att(ATT_value, 2)]);  //2!!!
    xml_AddNode (vParams, 'CI',                [xml_Att(ATT_value, 12)]);
    xml_AddNode (vParams, 'CA',                [xml_Att(ATT_value, 9)]);
    xml_AddNode (vParams, 'SitesFreqSpacing',  [xml_Att(ATT_value, 0)]);//  FParams.SitesFreqSpacing)]);
    xml_AddNode (vParams, 'SectorFreqSpacing', [xml_Att(ATT_value, FParams_.SectorFreqSpacing)]);
    xml_AddNode (vParams, 'SiteFreqSpacing',   [xml_Att(ATT_value,
          IIF(FParams_.SiteFreqSpacing>0, FParams_.SiteFreqSpacing-1, 0))]);

    xml_AddNode (vParams, 'IsUseGarmonic_BS_PO',    [xml_Att(ATT_value, 0)]);
    xml_AddNode (vParams, 'IsUseGarmonic_BS_BS',    [xml_Att(ATT_value, 0)]);

    xml_AddNode (vParams, 'PercentCI',              [xml_Att(ATT_value, 0)]);
    xml_AddNode (vParams, 'PercentTrf',             [xml_Att(ATT_value, 0)]);

    xml_AddNode (vParams, 'MaxPorogPercent',        [xml_Att(ATT_value, 100)]);
    xml_AddNode (vParams, 'SectorInterferenceType', [xml_Att(ATT_value, 0)]);
    xml_AddNode (vParams, 'IsLossQualityByAllFreq', [xml_Att(ATT_value, 0)]);
    xml_AddNode (vParams, 'IsLossQualityByHandoff', [xml_Att(ATT_value, 0)]);
    xml_AddNode (vParams, 'BestSecPereborType',     [xml_Att(ATT_value, 0)]);

    xml_AddNode (vParams, 'StartSectorNum',         [xml_Att(ATT_value, FParams_.StartSectorNum)]);
    xml_AddNode (vParams, 'PereborQueueType',       [xml_Att(ATT_value, FParams_.PereborQueueType)]);
    xml_AddNode (vParams, 'FreqDistribType',        [xml_Att(ATT_value, FParams_.FreqDistribType)]);
    xml_AddNode (vParams, 'MaxIterCount',           [xml_Att(ATT_value, FParams_.MaxIterCount)]);
    xml_AddNode (vParams, 'PereborOrderType',       [xml_Att(ATT_value, FParams_.PereborOrderType)]);
    xml_AddNode (vParams, 'IsUniformFreqRepeat',    [xml_Att(ATT_value, 0)]);
    xml_AddNode (vParams, 'MaxFreqRepeat',          [xml_Att(ATT_value, FParams_.MaxFreqRepeat)]);

    xml_AddNode (vParams, 'BestFreqPereborType',    [xml_Att(ATT_value, 0)]);
    xml_AddNode (vParams, 'NotAllowFreqRepeat',     [xml_Att(ATT_value, 0)]);




  vSectors:=xml_AddNode (vRoot, 'SECTORS', [xml_Att('count', FExportParams.Linkends.Count)]);
  for i := 0 to FExportParams.Linkends.Count-1 do
    AddLinkEnd(FExportParams.Linkends[i].Linkend_ID, vSectors, i + 1);

  xml_AddNode (vRoot, 'NeighborsByCI',
                  [xml_Att('FILENAME', aMatrixFileDir + MATRIX_CI_FILENAME)]);
  xml_AddNode (vRoot, 'NeighborsByCA',
                  [xml_Att('FILENAME', aMatrixFileDir + MATRIX_CA_FILENAME)]);
  xml_AddNode (vRoot, 'NeighborsByTrfCI',
                  [xml_Att('FILENAME', aMatrixFileDir + MATRIX_TRF_FILENAME)]);

  oXMLDoc.SaveToFile(aFileName);

  oXMLDoc.Free;
end;

//--------------------------------------------------------------------
procedure TdmLinkFreqPlan_calc.AddLinkEnd(aLinkEndID: integer;
          aNode: IXMLNode;  aCount: Integer);
//--------------------------------------------------------------------
const
  SQL_SELECT_LINKFREQPLAN_LINKEND =
     'SELECT * FROM '+ TBL_LINKFREQPLAN_LINKEND +
     ' WHERE (LinkFreqPlan_id=:LinkFreqPlan_id) and ' +
     '       (LinkEnd_id=:LinkEnd_id)';

var
  sFixed, sBand, sChAllowed: string;
  iPropID, iFreqRecCount,
   // iChTypeFixed,
  iChShift, i: integer;
  dTxRx_Shift, dChannelWidth, dFreqMin_MHz: Double;
  arrFixed, arrFixed_All: TIntArray;
  b: boolean;
  bIsLow: Boolean;
  sChTypeFixed: string;
begin
  db_OpenQuery(qry_LinkFreqPlan_LinkEnd, SQL_SELECT_LINKFREQPLAN_LINKEND,
                      [db_par(FLD_LINKEND_ID,      aLinkEndID),
                       db_par(FLD_LINKFREQPLAN_ID, Params.LinkFreqPlan_ID)]);

//  if qry_LinkFreqPlan_LinkEnd.FieldByName(FLD_ENABLED).AsInteger = 0 then
//    Exit;

  dTxRx_Shift:=0;
  dChannelWidth:=0;
  dFreqMin_MHz:=0;
  sChAllowed:='';

  b:= GetFreq_Resource(aLinkEndID, dTxRx_Shift, dChannelWidth, dFreqMin_MHz, sChAllowed);

  if (dTxRx_Shift=0) or (dChannelWidth=0) or (dFreqMin_MHz=0) then
  begin
    ErrorDlg('��� ��� "'+gl_DB.GetNameByID(TBL_LINKEND, aLinkEndID)+'" '+
             #13#10 + '�� ������ �����������!');
    Exit;
  end;

  dTxRx_Shift:= TruncFloat(dTxRx_Shift, 3);
  dChannelWidth   := TruncFloat(dChannelWidth, 3);
  dFreqMin_MHz   := TruncFloat(dFreqMin_MHz, 3);

//  db_OpenQuery(qry_LinkEnd, 'SELECT property_id, RANGE FROM '+ TBL_LINKEND + ' WHERE (id=:id) ',
  dmOnega_DB_data.OpenQuery(qry_LinkEnd,
              'SELECT property_id, band FROM '+ TBL_LINKEND + ' WHERE (id=:id) ',
              [FLD_ID, aLinkEndID]);

//  db_OpenTableByID(qry_LinkEnd, TBL_LINKEND, aLinkEndID);
  iPropID:= qry_LinkEnd.FieldByName(FLD_PROPERTY_ID).AsInteger;
  sBand  := qry_LinkEnd.FieldByName(FLD_BAND).AsString;
//  iRange := qry_LinkEnd.FieldByName(FLD_RANGE).AsInteger;

  with qry_LinkFreqPlan_LinkEnd do
  begin
    sChTypeFixed := FieldByName(FLD_CHANNEL_TYPE_FIXED).AsString;
//    iChTypeFixed := FieldByName(FLD_CH_TYPE_FIXED).AsInteger;
    arrFixed     := StringToIntArray(FieldByName(FLD_FREQ_FIXED).AsString);
    iFreqRecCount:= FieldByName(FLD_FREQ_REQ_COUNT).AsInteger;
  end;

  { TODO : �����������������!!! }
  iChShift:= Round(dTxRx_Shift/dChannelWidth);
  for i := 0 to Length(arrFixed)-1 do
    if (arrFixed[i]<>0) and
       ((i = 0) and (arrFixed[i]   < iChShift) or
        (i > 0) and (arrFixed[i]-1 < iChShift)) then
    begin
      bIsLow:=Eq(sChTypeFixed,'low');

      IntArrayAddValue(arrFixed_All, IIF(Eq(sChTypeFixed,'low'), arrFixed[i], arrFixed[i] + iChShift));
      IntArrayAddValue(arrFixed_All, IIF(Eq(sChTypeFixed,'Low'), arrFixed[i] + iChShift, arrFixed[i]));

(*      IntArrayAddValue(arrFixed_All, IIF(iChTypeFixed=0, arrFixed[i], arrFixed[i] + iChShift));
      IntArrayAddValue(arrFixed_All, IIF(iChTypeFixed=0, arrFixed[i] + iChShift, arrFixed[i]));
*)
   end;
  sFixed:= IntArrayToString(arrFixed_All);

  xml_AddNode (aNode, 'SECTOR',
      [xml_Att('id',                aLinkEndID),

        //� 1-�� ������ �������� ������, �� 2-�� - ������ "����. ����"
       xml_Att('SiteID',  IIF(FParams_.SiteFreqSpacing>0, iPropID, aCount)),

       xml_Att('FreqRequiredCount', iFreqRecCount),
       xml_Att('FreqFixed',         sFixed),
       xml_Att('FreqForbidden',     ''),
       xml_Att('total_cells',       1),
       xml_Att('ChannelsAllowed',   sChAllowed),

// ��������� 3 ��������� ��������� �� 100 ��� �������� � ������� ���
       xml_Att('DuplexFreqDelta',   dTxRx_Shift*100),
       xml_Att('ChannelWidth',      dChannelWidth*100),
       xml_Att('FreqMin',           dFreqMin_MHz*100),
       xml_Att('Range',             sBand),

       // Added by alex 09.05.2010 7:52:37
       //new
       xml_Att('Band',              sBand)
      ]);

end;

//--------------------------------------------------------------------
function TdmLinkFreqPlan_calc.GetForbidden(aLinkEndID : integer;
                                           aFirstFreq, aChannelWidth : double;
                                           aChannelArray: TIntArray): TIntArray;
//--------------------------------------------------------------------
var
    arrDouble: TDoubleArray;

    //------------------------------------------------------
    procedure DoGet(aArrAllow, aArrForbidden: TStrArray);
    //------------------------------------------------------
    var i, j: integer;
    begin
      if Length(aChannelArray) > 0 then
        for i:=0 to Length(aChannelArray)-1 do
        begin
          if Length(aArrAllow) > 0 then  //������ ������ ����������
            for j:=0 to Length(aArrAllow)-1 do
            begin
              arrDouble:= StringToDoubleArray (aArrAllow[j], '-');

              if Length(arrDouble) = 1 then  //��� ����� �������
                if aFirstFreq+((aChannelArray[i]-1)*aChannelWidth) <> arrDouble[0] then
                begin
                  IntArrayAddValue(Result, aChannelArray[i]);
                  Break;
                end;

              if Length(arrDouble) = 2 then //��� ������ ������
                if (aFirstFreq+((aChannelArray[i]-1)*aChannelWidth) < arrDouble[0]) or
                   (aFirstFreq+((aChannelArray[i]-1)*aChannelWidth) > arrDouble[1]) then
                begin
                  IntArrayAddValue(Result, aChannelArray[i]);
                  Break;
                end;
            end;

        if Length(aArrForbidden) > 0 then  //������ ������ ����������
          for j:=0 to Length(aArrForbidden)-1 do
          begin
            arrDouble:= StringToDoubleArray (aArrForbidden[j], '-');
            if Length(arrDouble) = 1 then  //��� ����� �������
              if aFirstFreq+((aChannelArray[i]-1)*aChannelWidth) = arrDouble[0] then
              begin
                IntArrayAddValue(Result, aChannelArray[i]);
                Break;
              end;

            if Length(arrDouble) = 2 then //��� ������ ������
              if (aFirstFreq+((aChannelArray[i]-1)*aChannelWidth) >= arrDouble[0]) and
                 (aFirstFreq+((aChannelArray[i]-1)*aChannelWidth) <= arrDouble[1]) then
              begin
                IntArrayAddValue(Result, aChannelArray[i]);
                Break;
              end;
          end;
      end;
    end;    //


var
   arrStrAllow, arrStrForbidden: TStrArray;
  sBand: string;
begin
//  iRange:= dmLinkEnd.GetIntFieldValue(aLinkEndID, FLD_RANGE);
//  sBand:= dmLinkEnd.GetStringFieldValue(aLinkEndID, FLD_BAND_NAME);
//  sBand:= dmLinkEnd.GetStringFieldValue(aLinkEndID, FLD_BAND);

  sBand:= gl_DB.GetStringFieldValueByID(tbl_LinkEnd, FLD_BAND, aLinkEndID);

  dmOnega_DB_data.OpenQuery(qry_Temp,
              'SELECT * FROM ' + TBL_LINKFREQPLAN_RESOURCE +
              ' WHERE (band=:band) and (linkfreqplan_id=:linkfreqplan_id)',

              [FLD_band,            sBand,
               FLD_LINKFREQPLAN_ID, Params.LinkFreqPlan_ID
               ]);

//          ' WHERE (range=:range) and (link_freqplan_id=:link_freqplan_id)',

  FExportParams.LoadFromDataset_BandResources(qry_Temp);


  qry_Temp.First;

  with qry_Temp do
  begin
    arrStrAllow     := StringToStrArray(FieldByName(FLD_FREQ_ALLOW).AsString);
    arrStrForbidden := StringToStrArray(FieldByName(FLD_FREQ_FORBIDDEN).AsString);
  end;

  DoGet(arrStrAllow, arrStrForbidden); //����� ����������-����������

  dmOnega_DB_data.OpenQuery(qry_Temp,
               'SELECT * FROM ' + TBL_LINKFREQPLAN_LINKEND  +
               ' WHERE (linkend_id=:linkend_id) and '+
               '       (linkfreqplan_id=:linkfreqplan_id)',
            [FLD_LINKEND_ID,       aLinkEndID,
             FLD_LINKFREQPLAN_ID, Params.LinkFreqPlan_ID
             ]);

  with qry_Temp do
  begin
    arrStrAllow     := StringToStrArray(FieldByName(FLD_FREQ_ALLOW).AsString);
    arrStrForbidden := StringToStrArray(FieldByName(FLD_FREQ_FORBIDDEN).AsString);
  end;

        { TODO : ����� ���������! }
  DoGet(arrStrAllow, arrStrForbidden); //��������� ����������-����������

end;

//--------------------------------------------------------------------
function TdmLinkFreqPlan_calc.GetFreq_Resource(aLinkEndID: integer; var
    aTxRx_Shift, aChannelWidth, aFreqMin: double; var aChannelsAllowed:
    string): boolean;
//--------------------------------------------------------------------
var // iMode,
 // iChType,
  iChCount, iChSkip, iChShift, iChNum, i, iLinkEndTypeID: integer;
  dChannelSpacing: double;
  arrForbidden, arrChannel_all, arrChannel_low, arrChannel_high: TIntArray;
  b: boolean;
  iLinkEndType_Mode_ID: integer;
  sChannelType: string;
begin
  Result:=False;

  ClearArray(arrChannel_low);
  ClearArray(arrChannel_high);
  ClearArray(arrChannel_all);

  dmOnega_DB_data.OpenQuery(qry_LinkEnd,
    'SELECT mode, channel_type, linkendtype_mode_id, linkendtype_id FROM '+ TBL_LINKEND
//    'SELECT mode, channel_number_type, linkendtype_mode_id, linkendtype_id FROM '+ TBL_LINKEND
      + ' WHERE (id=:id) ',
    [FLD_ID, aLinkEndID]);


//  db_OpenTableByID(qry_LinkEnd, TBL_LINKEND, aLinkEndID);
  with qry_LinkEnd do
  begin
//    iMode         := FieldByName(FLD_MODE).AsInteger;
    sChannelType       := Lowercase( FieldByName(FLD_CHANNEL_TYPE).AsString);
  //  iChType       := FieldByName(FLD_CHANNEL_NUMBER_TYPE).AsInteger;

    iLinkEndTypeID:= FieldByName(FLD_LINKENDType_ID).AsInteger;
    iLinkEndType_Mode_ID:= FieldByName(FLD_LINKENDType_Mode_ID).AsInteger;

    if iLinkEndTypeID = 0 then
      Exit;
  end;


  b:=dmLinkEnd_ex.GetBand_Params(aLinkEndID, aTxRx_Shift, aChannelWidth, aFreqMin, iChCount, OBJ_LINKEND);
//  b:=dmLinkEnd_ex.GetBand_Params1(aLinkEndID, aTxRx_Shift, aChannelWidth, aFreqMin, iChCount, OBJ_LINKEND);

  if aChannelWidth = 0 then
    Exit;

  dChannelSpacing:= gl_DB.GetDoubleFieldValue(TBL_LINKENDTYPE_MODE, FLD_ch_spacing,
                        [
                         db_par(FLD_ID, iLinkEndType_Mode_ID)
                        // ,
                        // /db_par(FLD_LinkEndType_id, iLinkEndTypeID),
                        // db_par(FLD_MODE,            iMode)
                         ]);

  if dChannelSpacing = 0 then
    dChannelSpacing:= gl_DB.GetDoubleFieldValue(TBL_LINKENDTYPE_MODE, FLD_bandwidth,
                        [
                         db_par(FLD_ID, iLinkEndType_Mode_ID)
                        // db_par(FLD_LinkEndType_id, iLinkEndTypeID),
                        // db_par(FLD_MODE,            iMode)
                         ]);

  if (dChannelSpacing = aChannelWidth) or (dChannelSpacing = 0) then
    for i := 0 to iChCount-1 do
      IntArrayAddValue(arrChannel_low, i+1)
  else
  begin
    if (dChannelSpacing < 0) or (aChannelWidth < 0) then
      Exit;

    iChNum:=1;
    while iChNum < iChCount do
    begin
      IntArrayAddValue(arrChannel_low, iChNum);

      iChSkip:= Ceil(dChannelSpacing/aChannelWidth);
      Inc(iChNum, iChSkip);
    end;    // while
  end;


  arrForbidden := GetForbidden(aLinkEndID, aFreqMin, aChannelWidth, arrChannel_low);

  IntArrayRemoveValues(arrChannel_low,  arrForbidden);
//             GetForbidden(aLinkEndID, aFreqMin, aChannelWidth, arrChannel_low));

  iChShift:= Round(aTxRx_Shift/aChannelWidth);
  i:= 0;
  while (i<Length(arrChannel_low)) and (arrChannel_low[i] <= iChCount) do
  begin
    IntArrayAddValue(arrChannel_high, arrChannel_low[i]+iChShift);

    Inc(i);
  end;    // while

  IntArrayRemoveValues(arrChannel_high,
            GetForbidden(aLinkEndID, aFreqMin, aChannelWidth, arrChannel_high));

  if sChannelType = 'high' then
//  if iChType = 1 then
    arrChannel_all:= MakeIntArray(arrChannel_high)
  else // iChType = 0
    for i := 0 to Length(arrChannel_high)-1 do
      IntArrayAddValue(arrChannel_all, arrChannel_high[i]-iChShift);

  IntArraySort(arrChannel_all);
  aChannelsAllowed:= IntArrayToString(arrChannel_all);

  Result:=True;
end;

//--------------------------------------------------------------------
procedure TdmLinkFreqPlan_calc.Load_XMLToDB (aFileName: string);
//--------------------------------------------------------------------
const

  SQL_UPDATE_LINK_FREQPLAN_LINKEND =
       'UPDATE '+ TBL_LINKFREQPLAN_LINKEND +
       ' SET freq_distributed=:freq_distributed,   '+
       '     Calc_Order=:Calc_Order,               '+
       '     channel_type_distributed=:channel_type_distributed, ' +
       '     freq_distributed_tx=:freq_distributed_tx, ' +
       '     freq_distributed_rx=:freq_distributed_rx ' +
       ' WHERE (LinkFreqPlan_id=:LinkFreqPlan_id) and '+
       '       (linkend_id=:linkend_id)';

(*
  SQL_UPDATE_LINK_FREQPLAN_LINKEND1 =
       'UPDATE '+ TBL_LINKFREQPLAN_LINKEND +
       ' SET freq_distributed=:freq_distributed,   '+
       '     ch_type_distributed=:ch_type_distributed ' +
       ' WHERE (LinkFreqPlan_id=:LinkFreqPlan_id) and '+
       '       (linkend_id=:linkend_id)';
*)

var vNode,vGroup,vRoot: IXMLNode;
    i, iLinkEndID, iDistributedCh, iCalcOrder: Integer;
    sChType : string;
    //iChType : string;
    iChannelCount, iChShift: Integer;
    dTxRx_Shift, dChannelWidth, dFreqMin, dTxFreq, dRxFreq: double;

  oXMLDoc: TXMLDoc;
  b: boolean;

  oLinkend: TcLinkFreqPlan_Linkend;
begin
  oXMLDoc:=TXMLDoc.Create;

  oXMLDoc.LoadFromFile(aFileName);
  if Assigned(oXMLDoc.DocumentElement) then
    if oXMLDoc.DocumentElement.ChildNodes.Count>0 then
  begin

    vRoot:=oXMLDoc.DocumentElement;
    vGroup:= vRoot.ChildNodes.FindNode('SECTORS');
//    xml_GetNodeByPath(vRoot, ['SECTORS']);

    for i:=0 to vGroup.ChildNodes.Count-1 do
    begin
      vNode:=vGroup.ChildNodes[i];

      iLinkEndID    := xml_GetIntAttr(vNode, FLD_ID);
      iDistributedCh:= xml_GetIntAttr(vNode, 'FreqDistributed');
      iCalcOrder    := xml_GetIntAttr(vNode, 'CalcOrder');

      if iDistributedCh = 0 then
        sChType:= ''
//        iChType:= -1
      else
      begin
        oLinkend := FexportParams.Linkends.FindByID(iLinkEndID);

        b:=dmLinkEnd_ex.GetBand_Params (iLinkEndID, dTxRx_Shift, dChannelWidth, dFreqMin, iChannelCount, OBJ_LINKEND);
//        b:=dmLinkEnd.GetBand_Params1 (iLinkEndID, dTxRx_Shift, dChannelWidth, dFreqMin, iChannelCount, OBJ_LINKEND);
        iChShift:= Round(dTxRx_Shift/dChannelWidth);

        if iDistributedCh > iChannelCount then
        begin
          iDistributedCh:= iDistributedCh - iChShift;
          sChType:= 'high';
   //       iChType:= 1;
        end
        else
          sChType:= 'low';
//          iChType:= 0;

        if sChType='low' then
//        case iChType of
  //          0:
              begin  //low
                 dTxFreq:= dFreqMin + dChannelWidth*(iDistributedCh - 1);
                 dRxFreq:= dTxFreq + dTxRx_Shift;
               end;

        if sChType='high' then
           // 1:
              begin  //high
                 dRxFreq:= dFreqMin + dChannelWidth*(iDistributedCh - 1);
                 dTxFreq:= dRxFreq + dTxRx_Shift;
               end;
//        else
  //        raise Exception.Create('');
    //    end;
      end;

//      dmOnega_DB_data.

(*      gl_DB.ExecCommand(SQL_UPDATE_LINK_FREQPLAN_LINKEND1,
          [
           db_par(FLD_LINKFREQPLAN_ID,    Params.LinkFreqPlan_ID),
           db_par(FLD_LINKEND_ID,          iLinkEndID),
           db_par(FLD_FREQ_DISTRIBUTED,    IIF(iDistributedCh>0, iDistributedCh, NULL)),

           db_par(FLD_CH_TYPE_DISTRIBUTED, sChType)


//           db_par(FLD_CH_TYPE_DISTRIBUTED, IIF(iChType=-1, NULL, iChType))
           ]);

*)

      gl_DB.ExecCommand(SQL_UPDATE_LINK_FREQPLAN_LINKEND,
          [
           db_par(FLD_LINKFREQPLAN_ID,    Params.LinkFreqPlan_ID),

           db_par(FLD_LINKEND_ID,          iLinkEndID),
           db_par(FLD_Calc_order,          iCalcOrder),
           db_par(FLD_FREQ_DISTRIBUTED,    IIF(iDistributedCh>0, iDistributedCh, NULL)),
           db_par('freq_distributed_tx',   IIF(iDistributedCh>0, dTxFreq, NULL)),
           db_par('freq_distributed_rx',   IIF(iDistributedCh>0, dRxFreq, NULL)),
           db_par(FLD_CHANNEL_TYPE_DISTRIBUTED, sChType)

//           db_par(FLD_CH_TYPE_DISTRIBUTED, IIF(iChType=-1, NULL, iChType))
          ]);

    end;

  end;

  oXMLDoc.Free;
end;

//--------------------------------------------------------------------
function TdmLinkFreqPlan_calc.Execute: boolean;
//--------------------------------------------------------------------
var sFile1,sFile2,sTempFileDir: string;
begin
  LoadParams();
  OpenData();

  sFile1:=g_ApplicationDataDir + TEMP_IN_FILENAME;
  sFile2:=ChangeFileExt(sFile1,'_res.xml');

  sTempFileDir:= IncludeTrailingBackslash (ExtractFileDir(sFile1));

{
  sTempFileDir:= GetTempFileDir;
  sFile1:=GetTempXmlFileName ();
  sFile2:=ChangeFileExt(sFile1,'_res.xml');
}

  SaveToXML (sFile1, sTempFileDir);
 // SaveToXML (TEMP_IN_FILENAME, aLinkend_IDList);

//  i:= mem_MatrixCI.RecordCount;
  mem_MatrixCI.SaveToTextFile (sTempFileDir + MATRIX_CI_FILENAME);
  mem_MatrixCA.SaveToTextFile (sTempFileDir + MATRIX_CA_FILENAME);
  mem_MatrixTrf.SaveToTextFile(sTempFileDir + MATRIX_TRF_FILENAME);

  Result:=RunApp (GetApplicationDir() + EXE_rpls_freq,
                   DoubleQuotedStr(sFile1)+' '+
                   DoubleQuotedStr(sFile2));

  Load_XMLToDB(sFile2);
  Result:=True;
end;


// ---------------------------------------------------------------
procedure TdmLinkFreqPlan_calc.Execute_all;
// ---------------------------------------------------------------

var
  I: Integer;

begin
  dmOnega_DB_data.OpenQuery(qry_LinkEnd1,
    'SELECT * FROM ' + view_LINKFREQPLAN_linkend_for_calc + ' WHERE (linkfreqplan_id=:id)',
    [FLD_ID, Params.LinkFreqPlan_ID]);

 // dmOnega_DB_data.LinkFreqPlan_SELECT (sp_FreqPlan_LinkEnd, Params.LinkFreqPlan_ID);

  FExportParams.Linkends.LoadFromDataset(qry_LinkEnd1);


  //--------------
  // RUN program
  //--------------

 // Params.LinkFreqPlan_ID:=aLinkFreqPlan_ID;
  Execute();


 // oLink_IDList_full.Free;
 // oLinkEnd_IDList.Free;


end;


class procedure TdmLinkFreqPlan_calc.Init;
begin
  if not Assigned(dmLinkFreqPlan_calc) then
    dmLinkFreqPlan_calc := TdmLinkFreqPlan_calc.Create(Application);


end;



begin


end.