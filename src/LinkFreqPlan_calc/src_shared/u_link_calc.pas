unit u_link_calc;

interface

implementation

end.

   (*


//--------------------------------------------------------------------
function Tdlg_Link_add_from_Profile.CalcReserve: double;
//--------------------------------------------------------------------
var
  L, dFreq_MHZ, dGain1, dGain2, dSense, dSpeed, dPower_dBm, Pout: double;
  sBand: string;

  rec: TCalcReserveRec;

begin

//  Result:=dmLink.CalcReserve(rec, iLinkEnd1,iLinkEnd2, oVector, ePout, eFade_margin_dB);


  dGain1:=gl_DB.GetFieldValueByID(TBL_ANTENNATYPE, FLD_GAIN, Fframe_Link_add1.AntennaTypeID, '');
  dGain2:=gl_DB.GetFieldValueByID(TBL_ANTENNATYPE, FLD_GAIN, Fframe_Link_add2.AntennaTypeID, '');

  //-------------------------------------------------------------------
  if Fframe_Link_add1.Equipment_Type = 0 then
  begin
   // raise Exception.Create('');
//    ShowMessage('function Tdlg_Link_add_from_Profile.CalcReserve: double;');

//    dFreq_MHZ:= gl_DB.GetFieldValueByID(TBL_LINKENDTYPE, FLD_RANGE,     Fframe_Link_add1.LinkEndTypeID, '');// * 1000;
    sBand:= gl_DB.GetFieldValueByID(TBL_LINKENDTYPE, FLD_BAND,  Fframe_Link_add1.LinkEndTypeID, '');// * 1000;

    dFreq_MHZ :=dmLibrary.GetBandAveFreq_MHZ (sBAnd);
    dPower_dBm:=gl_DB.GetFieldValueByID(TBL_LINKENDTYPE, FLD_POWER_MAX, Fframe_Link_add1.LinkEndTypeID, '');
  end else
  begin

    dFreq_MHZ:= AsFloat(Fframe_Link_add1.row_Freq.Properties.Value);
    dPower_dBm:=AsFloat(Fframe_Link_add1.row_Power_dBm.Properties.Value);

{    dFreq_MHZ:= AsFloat(Fframe_Link_add1.row_Freq.Text);
    dPower_dBm:=AsFloat(Fframe_Link_add1.row_Power.Text);
}  end;

  //-------------------------------------------------------------------
  if Fframe_Link_add2.Equipment_Type = 0 then
    dSense:= gl_DB.GetDoubleFieldValue(TBL_LINKENDTYPE_MODE, FLD_THRESHOLD_BER_6,
                    [db_Par(FLD_LINKENDTYPE_ID, Fframe_Link_add2.LinkEndTypeID)])
  else
    dSense:=Fframe_Link_add2.row_THRESHOLD_BER_6.Properties.Value;

  rec.Freq_GHz := dFreq_MHZ;
  rec.Power_dBm := dPower_dBm;
  rec.Gain1 :=  dGain1;
  rec.Gain1 :=  dGain2;
  rec.Sense :=  dSense;
  rec.Distance_km  :=  geo_Distance_km(FVector);


  Result:= radio_CalcReserve(rec,
                  dFreq_MHZ, dPower_dBm, dGain1, dGain2,
                  dSense, geo_Distance_km(FVector),  POut, FVector);


  if Result<0 then
    StatusBar2.Color:=clRed
  else
    StatusBar2.Color:=clLime;

  StatusBar1.Panels[0].Text:=
    '������ ��� ��������� ���������:';
  StatusBar1.Panels[1].Text:=
    '����� ��������� (km): '+AsString(geo_Distance_km(FVector)); ///1000

  StatusBar2.Panels[0].Text:=
    '�������� �� ����� �������� (dBm): '+AsString(TruncFloat(POut));
  StatusBar2.Panels[1].Text:=
    '����� �� ��������� (dB): '+AsString(TruncFloat(Result));

end;    //
