program dll_LinkFreqPlan_calc;

uses
  Forms,
  app_dm_Main in '..\src\app_dm_Main.pas' {app_dmMain: TDataModule},
  dm_LinkFreqPlan_calc in '..\src\dm_LinkFreqPlan_calc.pas' {dmLinkFreqPlan_calc: TDataModule},
  u_LinkFreqPlan_calc_export in '..\src\u_LinkFreqPlan_calc_export.pas',
  u_FreqPlan_calc_export in '..\src\u_FreqPlan_calc_export.pas',
  u_LinkFreqPlan_classes in '..\..\LinkFreqPlan_common_classes\u_LinkFreqPlan_classes.pas',
  u_ini_LinkFreqPlan_calc_params in '..\src_shared\u_ini_LinkFreqPlan_calc_params.pas';

{$R *.RES}




begin
  Application.Initialize;
  Application.CreateForm(Tapp_dmMain, app_dmMain);
  Application.Run;
end.
