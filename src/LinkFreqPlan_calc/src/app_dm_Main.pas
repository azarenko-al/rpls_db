unit app_dm_Main;

interface

uses
  SysUtils, Classes, Dialogs, IniFiles,

  u_vars,
 
  dm_LinkFreqPlan_Calc,

  dm_Main
  ;

type
  Tapp_dmMain = class(TDataModule)
    procedure DataModuleCreate(Sender: TObject);

  end;


var
  app_dmMain: Tapp_dmMain;

//===================================================================
implementation {$R *.DFM}
//===================================================================


//--------------------------------------------------------------
procedure Tapp_dmMain.DataModuleCreate(Sender: TObject);
//--------------------------------------------------------------
const
  TEMP_INI_FILE = 'LinkFreqPlan_calc.ini';

var
//  iID: Integer;
  iLinkFreqPlanID: Integer;
 // iProjectID: Integer;
  sIniFileName: string;
//  iLinkFreqPlanID: Integer;

  oIniFile:   TIniFile;
begin
  sIniFileName:=ParamStr(1);
  if sIniFileName='' then
    sIniFileName:=g_ApplicationDataDir+  TEMP_INI_FILE;


  oIniFile:=TIniFile.Create (sIniFileName);

  with oIniFile do
  begin
  //  iID :=ReadInteger('main', 'ID', 0);
    iLinkFreqPlanID :=ReadInteger('main', 'LinkFreqPlanID',  0);
//    iProjectID      :=ReadInteger('main', 'ProjectID',       0);
  //  Free;
  end;

  FreeAndNil(oIniFile);


 // iLinkFreqPlanID:=405;


  if (iLinkFreqPlanID = 0)
    // (iLinkFreqPlanID=0)
  then begin
    ShowMessage('(LinkFreqPlanID=0) ');
    Exit;
  end;


  TdmMain.Init;
  if not dmMain.OpenDB_reg then
    Exit;

(*  TdmMain.Init;

  if not dmMain.OpenFromReg then
    Exit;
*)

 // dmMain.ProjectID:=iProjectID;

  TdmLinkFreqPlan_calc.Init;

  with dmLinkFreqPlan_calc do
  begin
    Params.LinkFreqPlan_ID:= iLinkFreqPlanID;

//    Params.ChannelsKind := IIF (sChannelType = bUseFixedChannels, opUseDistrChannels, opUseFixedChannels);

    Execute;

//    ExecuteDlg ('������');
  end;

end;


end.
