unit u_FreqPlan_calc_export;

interface

uses SysUtils, Classes, XMLIntf,

  u_xml_document;


type
  TFreqPlan_NB_List = class;
  TFreqPlan_calc_export_params= class;

  // ---------------------------------------------------------------
  TFreqPlan_Cell_export = class(TCollectionItem)
  // ---------------------------------------------------------------
  public
    ID                : Integer;

    SiteID            : Integer;

    FreqRequiredCount : Integer;
    FreqFixed         : string;
    FreqForbidden     : string;
    total_cells       : Integer;
    ChannelsAllowed   : string;

    DuplexFreqDelta   : double;
    ChannelWidth      : double;
    FreqMin           : double;
    Range             : string;

    CalcResult: record
              DistributedCh : Integer;
              CalcOrder     : Integer;
            end;


   
   LinkEnd_name  : string;
   PROPERTY_name : string;

//   oCell.FreqRequiredCount := iFreqRecCount;
//   oCell.FreqFixed:=         sFixed),
//
//   oCell.FreqForbidden:=      sFORBIDDEN;
//   oCell.total_cells:=         1;
//   oCell.ChannelsAllowed:=   sChannelsAllowed;
//
//


  end;


  TFreqPlan_export_NB = class(TCollectionItem)
  public
    CELL1_ID   : Integer;
    CELL2_ID   : Integer;
    Count      : Integer;

    Tx_Property_ID   : Integer;
    Rx_Property_ID   : Integer;

    Tx_Property_name   : string;
    Rx_Property_name   : string;

    LinkEnd1_name   : string;
    LinkEnd2_name   : string;

  end;

  // ---------------------------------------------------------------
  TFreqPlan_cell_List = class(TCollection)
  // ---------------------------------------------------------------
  private
    function GetItems(Index: Integer): TFreqPlan_Cell_export;
  public
    constructor Create;
    function AddItem: TFreqPlan_Cell_export;

    function FindByID(aID: Integer): TFreqPlan_Cell_export;

//    function SaveToXMLNode(aXMLNode: IXMLNode): Integer;


    property Items[Index: Integer]: TFreqPlan_Cell_export read GetItems; default;
  end;


  // ---------------------------------------------------------------
  TFreqPlan_calc_export_params = class(TObject)
  // ---------------------------------------------------------------
  private
    NeighborMatrixType1: Integer;
  public
    Cells: TFreqPlan_cell_List;

    NeighborsByCI           : TFreqPlan_NB_List;
    NeighborsByCA           : TFreqPlan_NB_List;
    NeighborsByTrf          : TFreqPlan_NB_List;

    NeighborsByCI_FILENAME  : string;
    NeighborsByCA_FILENAME  : string;
    NeighborsByTrf_FILENAME : string;

    //-----------------------

    ChannelsAllowed         : string;
    DuplexFreqDelta         : Integer;
    ChannelWidth            : Integer;

    MaxCI                   : Integer;
    MaxCA                   : Integer;
    CI                      : Integer;
    CA                      : Integer;
    SitesFreqSpacing        : Integer;
    SectorFreqSpacing       : Integer;
    SiteFreqSpacing         : Integer;

    IsUseGarmonic_BS_PO     : Boolean;
    IsUseGarmonic_BS_BS     : Boolean;

    PercentCI               : Integer;
    PercentTrf              : Integer;

    MaxPorogPercent         : Integer;
    SectorInterferenceType  : Integer;
    IsLossQualityByAllFreq  : Integer;
    IsLossQualityByHandoff  : Integer;
    BestSecPereborType      : Integer;

    StartSectorNum          : Integer;
    PereborQueueType        : Integer;
    FreqDistribType         : Integer;
    MaxIterCount            : Integer;
    PereborOrderType        : Integer;
    IsUniformFreqRepeat     : Integer;
    MaxFreqRepeat           : Integer;

    BestFreqPereborType     : Integer;
    NotAllowFreqRepeat      : Integer;

  public
    constructor Create;
    destructor Destroy; override;

    procedure LoadFromXML(aFileName: string);
    procedure SaveToXML(aFileName, aMatrixFileDir: string);
  end;

  // ---------------------------------------------------------------
  TFreqPlan_NB_List = class(TCollection)
  // ---------------------------------------------------------------
  private
    function GetItems(Index: Integer): TFreqPlan_export_NB;
  public
    constructor Create;
    function AddItem(aCELL1_ID,aCELL2_ID,aCount: Integer): TFreqPlan_export_NB;

    procedure SaveToFile(aFileName: string);
    function SaveToXMLNode(aXMLNode: IXMLNode): Integer;


    property Items[Index: Integer]: TFreqPlan_export_NB read GetItems; default;
  end;




implementation

const

  MATRIX_CI_FILENAME  = 'freq_params.ByCI';
  MATRIX_CA_FILENAME  = 'freq_params.ByCA';
  MATRIX_TRF_FILENAME = 'freq_params.ByTrfCI';



constructor TFreqPlan_calc_export_params.Create;
begin
  inherited Create;
  Cells := TFreqPlan_cell_List.Create();
  NeighborsByCI := TFreqPlan_NB_List.Create();
  NeighborsByCA := TFreqPlan_NB_List.Create();
  NeighborsByTrf := TFreqPlan_NB_List.Create();
end;


destructor TFreqPlan_calc_export_params.Destroy;
begin
  FreeAndNil(NeighborsByTrf);
  FreeAndNil(NeighborsByCA);
  FreeAndNil(NeighborsByCI);
  FreeAndNil(Cells);
  inherited Destroy;
end;

//--------------------------------------------------------------------
procedure TFreqPlan_calc_export_params.LoadFromXML(aFileName: string);
//--------------------------------------------------------------------
var
  oXMLDoc: TXMLDoc;

  vNode,vGroup,vRoot: IXMLNode;
  i, iID, iDistributedCh, iCalcOrder: Integer;
  b: boolean;

  oCell: TFreqPlan_Cell_export;

begin
  oXMLDoc:=TXMLDoc.Create;

  oXMLDoc.LoadFromFile(aFileName);

  vRoot:=oXMLDoc.DocumentElement;

  if vRoot.ChildNodes.Count>0 then
  begin

//    vGroup:= xml_GetNodeByPath(vRoot, ['SECTORS']);
    vGroup:= xml_FindNode(vRoot, 'SECTORS');

    for i:=0 to vGroup.ChildNodes.Count-1 do
    begin
      vNode:=vGroup.ChildNodes[i];

      iID           := xml_GetIntAttr(vNode, 'ID');
      iDistributedCh:= xml_GetIntAttr(vNode, 'FreqDistributed');
      iCalcOrder    := xml_GetIntAttr(vNode, 'CalcOrder');

      oCell:=Cells.FindByID(iID);

      if Assigned(oCell) then
      begin
        oCell.CalcResult.DistributedCh := iDistributedCh;
        oCell.CalcResult.CalcOrder     := iCalcOrder;
      end;
    end;
  end;

  FreeAndNil(oXMLDoc);

end;

//--------------------------------------------------------------------
procedure TFreqPlan_calc_export_params.SaveToXML(aFileName, aMatrixFileDir: string);
//--------------------------------------------------------------------
var
  oXMLDoc: TXMLDoc;
  vRoot,vParams,vSectors: IXMLNode;
  i: Integer;

begin
  oXMLDoc:=TXMLDoc.Create;

  vRoot:=oXMLDoc.DocumentElement.AddChild('FREQ_PLAN');
  vParams:=vRoot.AddChild('params');

  vParams.AddChild('ChannelsAllowed').Attributes[ATT_value]    := ChannelsAllowed;
  vParams.AddChild('DuplexFreqDelta').Attributes[ATT_value]    := DuplexFreqDelta;
  vParams.AddChild('ChannelWidth').Attributes[ATT_value]       := ChannelWidth;

  vParams.AddChild('MaxCI').Attributes[ATT_value]              := MaxCI;
  vParams.AddChild('MaxCA').Attributes[ATT_value]              := MaxCA;
/////  vParams.AddChild('NeighborMatrixType').Attributes[ATT_value] := NeighborMatrixType1 ;  //2!!!
  vParams.AddChild('CI').Attributes[ATT_value]                 := CI;
  vParams.AddChild('CA').Attributes[ATT_value]                 := CA;
  vParams.AddChild('SitesFreqSpacing').Attributes[ATT_value]   := SitesFreqSpacing;//  FParams.SitesFreqSpacing;
  vParams.AddChild('SectorFreqSpacing').Attributes[ATT_value]  := SectorFreqSpacing;
  vParams.AddChild('SiteFreqSpacing').Attributes[ATT_value]    := SiteFreqSpacing;

  vParams.AddChild('IsUseGarmonic_BS_PO').Attributes[ATT_value]:= 0;
  vParams.AddChild('IsUseGarmonic_BS_BS').Attributes[ATT_value]:= 0;

  vParams.AddChild('PercentCI').Attributes[ATT_value]          := 0;
  vParams.AddChild('PercentTrf').Attributes[ATT_value]         := 0;

  vParams.AddChild('MaxPorogPercent').Attributes[ATT_value]        := MaxPorogPercent;
  vParams.AddChild('SectorInterferenceType').Attributes[ATT_value] := SectorInterferenceType;
  vParams.AddChild('IsLossQualityByAllFreq').Attributes[ATT_value] := IsLossQualityByAllFreq;
  vParams.AddChild('IsLossQualityByHandoff').Attributes[ATT_value] := IsLossQualityByHandoff;
  vParams.AddChild('BestSecPereborType').Attributes[ATT_value]     := BestSecPereborType;

  vParams.AddChild('StartSectorNum').Attributes[ATT_value]     := StartSectorNum;
  vParams.AddChild('PereborQueueType').Attributes[ATT_value]   := PereborQueueType;
  vParams.AddChild('FreqDistribType').Attributes[ATT_value]    := FreqDistribType;
  vParams.AddChild('MaxIterCount').Attributes[ATT_value]       := MaxIterCount;
  vParams.AddChild('PereborOrderType').Attributes[ATT_value]   := PereborOrderType;
  vParams.AddChild('IsUniformFreqRepeat').Attributes[ATT_value]:= IsUniformFreqRepeat;
  vParams.AddChild('MaxFreqRepeat').Attributes[ATT_value]      := MaxFreqRepeat;

  vParams.AddChild('BestFreqPereborType').Attributes[ATT_value]:= BestFreqPereborType;
  vParams.AddChild('NotAllowFreqRepeat').Attributes[ATT_value] := NotAllowFreqRepeat;


(*
    xml_AddNode (vParams, 'ChannelsAllowed',   [xml_Att(ATT_value, ChannelsAllowed)]);
    xml_AddNode (vParams, 'DuplexFreqDelta',   [xml_Att(ATT_value, DuplexFreqDelta)]);
    xml_AddNode (vParams, 'ChannelWidth',      [xml_Att(ATT_value, ChannelWidth)]);

    xml_AddNode (vParams, 'MaxCI',             [xml_Att(ATT_value, MaxCI)]);
    xml_AddNode (vParams, 'MaxCA',             [xml_Att(ATT_value, MaxCA)]);
    xml_AddNode (vParams, 'NeighborMatrixType',[xml_Att(ATT_value, NeighborMatrixType )]);  //2!!!
    xml_AddNode (vParams, 'CI',                [xml_Att(ATT_value, CI)]);
    xml_AddNode (vParams, 'CA',                [xml_Att(ATT_value, CA)]);
    xml_AddNode (vParams, 'SitesFreqSpacing',  [xml_Att(ATT_value, SitesFreqSpacing)]);//  FParams.SitesFreqSpacing)]);
    xml_AddNode (vParams, 'SectorFreqSpacing', [xml_Att(ATT_value, SectorFreqSpacing)]);
    xml_AddNode (vParams, 'SiteFreqSpacing',   [xml_Att(ATT_value, SiteFreqSpacing)]);

    xml_AddNode (vParams, 'IsUseGarmonic_BS_PO', [xml_Att(ATT_value, 0)]);
    xml_AddNode (vParams, 'IsUseGarmonic_BS_BS', [xml_Att(ATT_value, 0)]);

    xml_AddNode (vParams, 'PercentCI',          [xml_Att(ATT_value, 0)]);
    xml_AddNode (vParams, 'PercentTrf',         [xml_Att(ATT_value, 0)]);

    xml_AddNode (vParams, 'MaxPorogPercent',        [xml_Att(ATT_value, MaxPorogPercent)]);
    xml_AddNode (vParams, 'SectorInterferenceType', [xml_Att(ATT_value, SectorInterferenceType)]);
    xml_AddNode (vParams, 'IsLossQualityByAllFreq', [xml_Att(ATT_value, IsLossQualityByAllFreq)]);
    xml_AddNode (vParams, 'IsLossQualityByHandoff', [xml_Att(ATT_value, IsLossQualityByHandoff)]);
    xml_AddNode (vParams, 'BestSecPereborType',     [xml_Att(ATT_value, BestSecPereborType)]);

    xml_AddNode (vParams, 'StartSectorNum',     [xml_Att(ATT_value, StartSectorNum)]);
    xml_AddNode (vParams, 'PereborQueueType',   [xml_Att(ATT_value, PereborQueueType)]);
    xml_AddNode (vParams, 'FreqDistribType',    [xml_Att(ATT_value, FreqDistribType)]);
    xml_AddNode (vParams, 'MaxIterCount',       [xml_Att(ATT_value, MaxIterCount)]);
    xml_AddNode (vParams, 'PereborOrderType',   [xml_Att(ATT_value, PereborOrderType)]);
    xml_AddNode (vParams, 'IsUniformFreqRepeat',[xml_Att(ATT_value, IsUniformFreqRepeat)]);
    xml_AddNode (vParams, 'MaxFreqRepeat',      [xml_Att(ATT_value, MaxFreqRepeat)]);

    xml_AddNode (vParams, 'BestFreqPereborType',[xml_Att(ATT_value, BestFreqPereborType)]);
    xml_AddNode (vParams, 'NotAllowFreqRepeat', [xml_Att(ATT_value, NotAllowFreqRepeat)]);
*)


(*
  vSectors:=xml_AddNode (vRoot, 'SECTORS', [xml_Att('count', FLinkends.Count)]);
  for i := 0 to FLinkends.Count-1 do
    AddLinkEnd(FLinkends[i].Linkend_ID, vSectors, i + 1);
*)
  xml_AddNode (vRoot, 'NeighborsByCI',
                  [xml_Att('FILENAME', aMatrixFileDir + MATRIX_CI_FILENAME),
                   xml_Att('comment', '')
                  ]);

  xml_AddNode (vRoot, 'NeighborsByCA',
                  [xml_Att('FILENAME', aMatrixFileDir + MATRIX_CA_FILENAME),
                   xml_Att('comment', '')
                  ]);

  xml_AddNode (vRoot, 'NeighborsByTrf',
                  [xml_Att('FILENAME', aMatrixFileDir + MATRIX_TRF_FILENAME),
                   xml_Att('comment', 'by link')
                  ]);

  oXMLDoc.SaveToFile(aFileName);

  oXMLDoc.Free;
end;

constructor TFreqPlan_cell_List.Create;
begin
  inherited Create(TFreqPlan_Cell_export);
end;

function TFreqPlan_cell_List.AddItem: TFreqPlan_Cell_export;
begin
  Result := TFreqPlan_Cell_export (inherited Add);
end;

// ---------------------------------------------------------------
function TFreqPlan_cell_List.FindByID(aID: Integer): TFreqPlan_Cell_export;
// ---------------------------------------------------------------
var
  I: integer;
begin
  for I := 0 to Count - 1 do
    if Items[i].ID= aID then
    begin
      Result := Items[i];
      Exit;
    end;

  Result := nil;
end;





function TFreqPlan_cell_List.GetItems(Index: Integer): TFreqPlan_Cell_export;
begin
  Result := TFreqPlan_Cell_export(inherited Items[Index]);
end;




// ---------------------------------------------------------------
function TFreqPlan_NB_List.SaveToXMLNode(aXMLNode: IXMLNode): Integer;
// ---------------------------------------------------------------
var
  I: integer;
begin
  for i := 0 to Count-1 do
    xml_AddNode (aXMLNode, 'item',
       [
        xml_Att('CELL1_ID', Items[i].CELL1_ID),
        xml_Att('CELL2_ID', Items[i].CELL2_ID),
        xml_Att('Count',    Items[i].Count),

        xml_Att('LinkEnd1_name', Items[i].LinkEnd1_name),
        xml_Att('LinkEnd2_name', Items[i].LinkEnd2_name)

       ]);

end;



// ---------------------------------------------------------------
procedure TFreqPlan_NB_List.SaveToFile(aFileName: string);
// ---------------------------------------------------------------
var
  i: Integer;
  oSList: TStringList;
begin
  oSList:=TStringList.Create;

  oSList.Add('CELL1_ID'+ Chr(9) +'CELL2_ID'+ Chr(9) +	'cell_count');
      

  for i:=0 to Count-1 do
    oSList.Add( Format('%d'+ Chr(9) +'%d'+ Chr(9) +	'%d',
        [Items[i].CELL1_ID, Items[i].CELL2_ID, Items[i].Count]) );

  oSList.SaveToFile(aFileName);


  FreeAndNil(oSList);
end;

constructor TFreqPlan_NB_List.Create;
begin
  inherited Create(TFreqPlan_export_NB);
end;


function TFreqPlan_NB_List.AddItem(aCELL1_ID,aCELL2_ID,aCount: Integer):
    TFreqPlan_export_NB;
begin
  Result := TFreqPlan_export_NB (inherited Add);
  Result.CELL1_ID:=aceLL1_ID;
  Result.CELL2_ID:=aceLL2_ID;
  Result.Count:=aCount;

end;

function TFreqPlan_NB_List.GetItems(Index: Integer): TFreqPlan_export_NB;
begin
  Result := TFreqPlan_export_NB(inherited Items[Index]);
end;

end.


(*

 xml_AddNode (aNode, 'SECTOR',
      [xml_Att('id',                aLinkEndID),

        //� 1-�� ������ �������� ������, �� 2-�� - ������ "����. ����"
       xml_Att('SiteID',            IIF(FParams.SiteFreqSpacing>0, iPropID, aCount)),

       xml_Att('FreqRequiredCount', iFreqRecCount),
       xml_Att('FreqFixed',         sFixed),
       xml_Att('FreqForbidden',     ''),
       xml_Att('total_cells',       1),
       xml_Att('ChannelsAllowed',   sChAllowed),

// ��������� 3 ��������� ��������� �� 100 ��� �������� � ������� ���
       xml_Att('DuplexFreqDelta',   dTxRx_Shift*100),
       xml_Att('ChannelWidth',      dChannelWidth*100),
       xml_Att('FreqMin',           dFreqMin_MHz*100),
       xml_Att('Range',             sBand),

       // Added by alex 09.05.2010 7:52:37
       //new
       xml_Att('Band',              sBand)
      ]);*);
