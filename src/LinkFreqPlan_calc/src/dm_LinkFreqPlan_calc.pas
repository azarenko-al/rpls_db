unit dm_LinkFreqPlan_calc;

interface

uses
  SysUtils, Classes, Forms, Db, ADODB, Math, Variants, Dialogs,
  XMLIntf,

  u_LinkFreqPlan_classes,

  u_xml_document,
  u_FreqPlan_calc_export,

  

  dm_Main,

  u_vars,

  u_const,
  u_const_db,

  
  u_LinkFreqPlan_const,

  
  u_db,

  u_files,
//  u_XML,

  u_Func_arrays,
  u_func ;


type
  TdmLinkFreqPlan_calc = class(TDataModule)
    qry_RRL_Neighbors: TADOQuery;
    qry_LinkFreqPlan: TADOQuery;
    qry_LinkFreqPlan_LinkEnd: TADOQuery;
    ADOQuery1: TADOQuery;
    procedure DataModuleDestroy(Sender: TObject);
    procedure DataModuleCreate(Sender: TObject);

  private
 //   FLinkends: TcLinkFreqPlan_Linkend_List;

    FFreqPlan_calc_export: TFreqPlan_calc_export_params;

  //  FExportParams: TLinkFreqPlan_calc_params;

//    FLinkFreqPlan: TLinkFreqPlan;

    FData: TnbData;

    {
    FParams_: record
      SectorFreqSpacing   : double;  // ������ ������ � �������
      SiteFreqSpacing   : double;  // ������ ������ �������� � ������� �������
//      SitesFreqSpacing    : double;  // ������ ������ �������� ������ ������� �������
      StartSectorNum  : integer; // ����� �������� ����� �������������
      PereborQueueType : integer;
      FreqDistribType : integer; // 0-dtOneFreqForSector, 1-dtAllFreqsForSector);
      MaxIterCount : integer;
      PereborOrderType    : integer; //0-opForward, 1-opBackward, 2-opRandom);
      MaxFreqRepeat       : integer; //max �����.�������� ������

    end;
   }

    function GetForbidden(aLinkend: TnbLinkFreqPlan_Linkend;
    //aLinkEndID : integer;
        //aFreqMinLow, aChannelWidth : double;
        aChannelArray: TIntArray):
        TIntArray;

    function GetFreq_Resource(aLinkend: TnbLinkFreqPlan_Linkend;
    //aLinkEndID: integer; var aTxRx_Shift, aChannelWidth,
//        aFreqMinLow: double;

        var aChannelsAllowed: string): boolean;

    procedure AddLinkEnd(aLinkend: TnbLinkFreqPlan_Linkend; aLinkEndID: integer;
        aNode: IXMLNode; aCount: Integer);

// TODO: LoadParams
//  procedure LoadParams ();
    function OpenData: Boolean;

    procedure SaveToXML(aFileName, aMatrixFileDir: string);
    procedure Load_XMLToDB (aFileName: string);

//    procedure SaveToXML_new(aFileName, aMatrixFileDir: string);

  public
    Fcalc_params: TFreqPlan_calc_export_params;



    Params: record
      LinkFreqPlan_ID: integer;
    end;

    function Execute: boolean;

// TODO: Execute_all11111111
//  procedure Execute_all11111111;


    class procedure Init;

  end;

var
  dmLinkFreqPlan_calc: TdmLinkFreqPlan_calc;


//function dmLinkFreqPlan_calc: TdmLinkFreqPlan_calc;

//====================================================================
//====================================================================
implementation
 {$R *.DFM}

const
  view_LINKFREQPLAN_linkend_for_calc = 'view_LINKFREQPLAN_linkend_for_calc';

const

  TEMP_IN_FILENAME  = 'LinkFreqPlan_calc_params.xml';

  MATRIX_CI_FILENAME  = 'freq_params.ByCI';
  MATRIX_CA_FILENAME  = 'freq_params.ByCA';
  MATRIX_TRF_FILENAME = 'freq_params.ByTrfCI';




class procedure TdmLinkFreqPlan_calc.Init;
begin
  if not Assigned(dmLinkFreqPlan_calc) then
    dmLinkFreqPlan_calc := TdmLinkFreqPlan_calc.Create(Application);


end;


//--------------------------------------------------------------------
procedure TdmLinkFreqPlan_calc.DataModuleCreate(Sender: TObject);
//--------------------------------------------------------------------
begin
  inherited;

  db_SetComponentADOConnection(Self, dmMain.ADOConnection);


  Fcalc_params:=TFreqPlan_calc_export_params.Create;


  {
  db_CreateField (mem_MatrixCI,  [db_Field('CELL1_ID', ftInteger),
                                  db_Field('CELL2_ID', ftInteger),
                                  db_Field('cell_count', ftInteger)
                                 ]);
  db_CreateField (mem_MatrixCA,  [db_Field('CELL1_ID', ftInteger),
                                  db_Field('CELL2_ID', ftInteger),
                                  db_Field('cell_count', ftInteger)
                                 ]);
  db_CreateField (mem_MatrixTrf, [db_Field('CELL1_ID', ftInteger),
                                  db_Field('CELL2_ID', ftInteger),
                                  db_Field('cell_count', ftInteger)
                                 ]);
  }
  FFreqPlan_calc_export := TFreqPlan_calc_export_params.Create();

//  FExportParams := TLinkFreqPlan_calc_params.Create();
//  FLinkFreqPlan := TLinkFreqPlan.Create();

  FData:=TnbData.Create;

end;


procedure TdmLinkFreqPlan_calc.DataModuleDestroy(Sender: TObject);
begin

  FreeAndNil(FData);

//  FreeAndNil(FLinkFreqPlan);

//  FreeAndNil(FExportParams);
  FreeAndNil(FFreqPlan_calc_export);

  FreeAndNil(Fcalc_params);
end;


//--------------------------------------------------------------------
function TdmLinkFreqPlan_calc.OpenData: Boolean;
//--------------------------------------------------------------------
const
  SQL_SELECT_LINKFREQPLAN =
     'SELECT * FROM '+ TBL_LINKFREQPLAN +' WHERE id=:id';

  SQL_SELECT_LINKFREQPLAN_linkend =
    'SELECT * FROM ' + view_LINKFREQPLAN_linkend + ' WHERE (linkfreqplan_id=:id)';



//  SQL_SELECT_LINKFREQPLAN_NEIGHBORS11 =
//     'SELECT * FROM '+ view_LINKFREQPLAN_NEIGHBORS +
////     'SELECT * FROM '+ TBL_LINKFREQPLAN_NEIGHBORS +
//     ' WHERE LinkFreqPlan_id=:LinkFreqPlan_id';

  {
  SQL_SELECT_LINKFREQPLAN_NEIGHBORS1111 =
'SELECT     dbo.LinkFreqPlan_Neighbors.*, dbo.LinkEndType_Mode.bandwidth, dbo.LinkEndType.coeff_noise '+
'FROM         dbo.LinkEnd LEFT OUTER JOIN '+
'             dbo.LinkEndType_Mode ON dbo.LinkEnd.LinkEndType_mode_id = dbo.LinkEndType_Mode.id LEFT OUTER JOIN '+
'             dbo.LinkEndType ON dbo.LinkEnd.linkendtype_id = dbo.LinkEndType.id RIGHT OUTER JOIN '+
'             dbo.LinkFreqPlan_Neighbors ON dbo.LinkEnd.id = dbo.LinkFreqPlan_Neighbors.rx_linkend_id '+
' WHERE LinkFreqPlan_id=:LinkFreqPlan_id                                 ';
  }

  view_LINKFREQPLAN_Calc_NEIGHBORS = 'view_LINKFREQPLAN_Calc_NEIGHBORS';

  SQL_SELECT_LINKFREQPLAN_NEIGHBORS =
      'SELECT * FROM  '+  view_LINKFREQPLAN_Calc_NEIGHBORS +
      ' WHERE LinkFreqPlan_id=:LinkFreqPlan_id';



var
  b: Boolean;
  bIsLink: Boolean;
//  bIsNeigh: Boolean;
  i: Integer;
  iFreqSpacing_MHz : integer;
  iID: Integer;
  iIsLink: Integer;
  iIsNeigh: Integer;
  iRX_LINKEND_ID: Integer;
  iTX_LINKEND_ID: Integer;
  oErrorList: TStringList;
  oItem: TFreqPlan_export_NB;

  oNeighbor: TnbNeighbor;

begin
  Result := False;


  Assert(Params.LinkFreqPlan_ID>0);

  dmOnega_DB_data.OpenQuery (qry_LinkFreqPlan, SQL_SELECT_LINKFREQPLAN,
                   [FLD_ID, Params.LinkFreqPlan_ID]);


//  FLinkFreqPlan.LoadFromDataset(qry_LinkFreqPlan);

  Assert(qry_LinkFreqPlan.RecordCount>0, 'qry_LinkFreqPlan.RecordCount>0');


  FData.LinkFreqPlan.LoadFromDataset(qry_LinkFreqPlan);

  {
  with qry_LinkFreqPlan do
    if not EOF then
  begin
//    FParams.SitesFreqSpacing := FieldByName(FLD_SitesFreqSpacing).AsInteger;
    FParams_.SectorFreqSpacing:= FieldByName(FLD_SectorFreqSpacing).AsInteger;
    FParams_.SiteFreqSpacing  := FieldByName(FLD_SiteFreqSpacing).AsInteger;
    FParams_.StartSectorNum   := FieldByName(FLD_StartSectorNum).AsInteger;
    FParams_.PereborQueueType := FieldByName(FLD_PereborQueueType).AsInteger;
    FParams_.FreqDistribType  := FieldByName(FLD_FreqDistribType).AsInteger;
    FParams_.MaxIterCount     := FieldByName(FLD_MaxIterCount).AsInteger;
    FParams_.PereborOrderType := FieldByName(FLD_PereborOrderType).AsInteger;
    FParams_.MaxFreqRepeat    := FieldByName(FLD_MaxFreqRepeat).AsInteger;
  end;
  }

  // -------------------------

  dmOnega_db_data.OpenQuery(qry_LinkFreqPlan_LinkEnd,
//    'SELECT * FROM ' + view_LINKFREQPLAN_linkend_for_calc + ' WHERE (linkfreqplan_id=:id)',
    'SELECT * FROM ' + view_LINKFREQPLAN_linkend + ' WHERE (linkfreqplan_id=:id)',
    [FLD_ID, Params.LinkFreqPlan_ID]);

 // dmOnega_DB_data.LinkFreqPlan_SELECT (sp_FreqPlan_LinkEnd, Params.LinkFreqPlan_ID);

//db_View (qry_LinkFreqPlan_LinkEnd);


  FData.Linkends.LoadFromDataset(qry_LinkFreqPlan_LinkEnd);

 // Assert(FData.Linkends.Count>0, 'FData.Linkends.Count>0');

  if qry_LinkFreqPlan_LinkEnd.RecordCount = 0 then
  begin
    ShowMessage('������. ��� ������.');
    exit;
  end;



  if not FData.Linkends.Validate then
    Exit;


//  for i := 0 to FData.Linkends.Count-1 do
//    AddLinkEnd(FData.Linkends[i], FData.Linkends[i].Linkend_ID, vSectors, i + 1);



//  FExportParams.Linkends.LoadFromDataset(qry_view_LINKFREQPLAN_linkend);


  db_OpenQuery(ADOQuery1,
              'SELECT * FROM ' + TBL_LINKFREQPLAN_RESOURCE +
              ' WHERE (linkfreqplan_id=:linkfreqplan_id)',
              [db_par(FLD_LINKFREQPLAN_ID, Params.LinkFreqPlan_ID)  ]);

//          ' WHERE (range=:range) and (link_freqplan_id=:link_freqplan_id)',

  FData.Resources.LoadFromDataset(ADOQuery1);

  Assert(FData.Linkends.Count>0, 'FData.Linkends.Count>0');


//  FExportParams.Linkends.Validate();

  // -------------------------
  {
  oErrorList:=TStringList.Create;

  FExportParams.Linkends.Validate(oErrorList);

  if oErrorList.Count>0 then
  begin
    if ConfirmDlg('���������� ������ ������. �������� ������?') then
      ShellExec_Notepad_temp(oErrorList.Text);

    if not ConfirmDlg('���������� ������?') then
      Exit;

  end;

  FreeAndNil(oErrorList);
  // -------------------------
  }


//  FLinkFreqPlan.Linkends.LoadFromDataset(qry_view_LINKFREQPLAN_linkend);

//  if FExportParams.Linkends.Count=0 then
//    ShowMessage('FExportParams.Linkends.Count=0');

(*
  if FLinkFreqPlan.Linkends.Count=0 then
  begin
    ShowMessage('FLinkFreqPlan.Linkends.Count=0');

    Exit;
  end;
*)



//  view_LINKFREQPLAN_Calc_NEIGHBORS = 'view_LINKFREQPLAN_Calc_NEIGHBORS';
//
//  SQL_SELECT_LINKFREQPLAN_NEIGHBORS =
//'SELECT * FROM  '+  view_LINKFREQPLAN_Calc_NEIGHBORS +
//' WHERE LinkFreqPlan_id=:LinkFreqPlan_id';
//



  db_OpenQuery (qry_RRL_Neighbors, SQL_SELECT_LINKFREQPLAN_NEIGHBORS,
                [db_par(FLD_LINKFREQPLAN_ID, Params.LinkFreqPlan_ID)]);

  if qry_RRL_Neighbors.RecordCount = 0 then
  begin
    ShowMessage('������. ������ �� ����������.');
    exit;
  end;





  Assert(qry_RRL_Neighbors.RecordCount>0, 'qry_RRL_Neighbors.RecordCount>0');

  FData.Neighbors.LoadFromDataset(qry_RRL_Neighbors);

//  FLinkFreqPlan.Neighbors.LoadFromDataset(qry_RRL_Neighbors);

  {
  mem_MatrixCI.Close;
  mem_MatrixCA.Close;
  mem_MatrixTrf.Close;

  mem_MatrixCI.Open;
  mem_MatrixCA.Open;
  mem_MatrixTrf.Open;
  }


//  if qry_RRL_Neighbors.RecordCount=0 then
 //   g_Log.Add('���-�� �������=0');


//  qry_RRL_Neighbors.First; //!!!!!!

  for i:=0 to FData.Neighbors.Count-1 do

//  with qry_RRL_Neighbors do
//    while not Eof do
  begin
  //  iID:=FieldValues[FLD_ID];

 //   oNeighbor:=FData.Neighbors.FindIByID(iID);
    oNeighbor:=FData.Neighbors[i];

   iTX_LINKEND_ID:=oNeighbor.LINKEND1_ID;
   iRX_LINKEND_ID:=oNeighbor.LINKEND2_ID;



//    iTX_LINKEND_ID:=FieldValues[FLD_TX_LINKEND_ID];
//    iRX_LINKEND_ID:=FieldValues[FLD_RX_LINKEND_ID];


//    iRX_LINKEND_ID:=FieldValues[FLD_TX_LINK_ID];

//    b:= FieldByName(FLD_IsLink).AsBoolean;
//    b:= FieldByName(FLD_IsPropertyOne).AsBoolean;



//    if not FieldByName(FLD_IsLink).AsBoolean and
//       not ( FieldByName(FLD_IsPropertyOne).AsBoolean and

    if not oNeighbor.IsLink then
       if not ( oNeighbor.IsPropertyOne and
//             (FParams_.SiteFreqSpacing>0)
             (FData.LinkFreqPlan.SiteFreqSpacing>0)
           )
    then
      begin
     //   bIsNeigh:=oNeighbor.IsNeigh1;//   FieldByName(FLD_IsNeigh).AsBoolean;
        iIsNeigh := IIF(oNeighbor.IsNeigh1, 1, 0);

      //  iIsNeigh :=1;

      //  if bIsNeigh then
         oItem:= FFreqPlan_calc_export.NeighborsByCI.AddItem
                (iTX_LINKEND_ID,
                 iRX_LINKEND_ID,
                 //FieldValues[FLD_TX_LINK_ID],
                 iIsNeigh //FieldValues[FLD_IsNeigh]
                 );


        oItem.LinkEnd1_name:= oNeighbor.LINKEND1_name;
        oItem.LinkEnd2_name:= oNeighbor.LINKEND2_name;

 {
        oItem.tx_Property_ID:= oNeighbor.PROPERTY1_ID;
        oItem.rx_Property_ID:= oNeighbor.PROPERTY2_ID;

        Assert (oItem.tx_Property_ID>0);


        oItem.tx_Property_name:= oNeighbor.PROPERTY1_name;
        oItem.rx_Property_name:= oNeighbor.PROPERTY2_name;
}

       {
        db_AddRecord(mem_MatrixCI,
           [db_par('CELL1_ID',  iTX_LINKEND_ID),
            db_par('CELL2_ID',  iRX_LINKEND_ID),
           // db_par('CELL2_ID',  FieldValues[FLD_TX_LINK_ID]),
            db_par('cell_count', iIsNeigh ) //FieldValues[FLD_IsNeigh]
            ]);
        }
      end;
    //--------------------------------------------

   //  bIsLink:= oNeighbor.IsLink;//  FieldByName(FLD_IsNeigh).AsBoolean;
    iIsLink := IIF(oNeighbor.IsLink, 1, 0);

    oItem:= FFreqPlan_calc_export.NeighborsByTrf.AddItem
            (iTX_LINKEND_ID,
             iRX_LINKEND_ID,
             iIsLink);


    oItem.LinkEnd1_name:= oNeighbor.LINKEND1_name;
    oItem.LinkEnd2_name:= oNeighbor.LINKEND2_name;

 {
    oItem.tx_Property_ID:= oNeighbor.PROPERTY1_ID;
    oItem.rx_Property_ID:= oNeighbor.PROPERTY2_ID;

    oItem.tx_Property_name:= oNeighbor.PROPERTY1_name;
    oItem.rx_Property_name:= oNeighbor.PROPERTY2_name;
 }

   {
    db_AddRecord(mem_MatrixTrf,[db_par('CELL1_ID',  iTX_LINKEND_ID),
                                db_par('CELL2_ID',  iRX_LINKEND_ID),
                               // db_par('CELL2_ID',  FieldValues[FLD_TX_LINK_ID]),
                                db_par('cell_count', iIsLink)
                                ]);
     }


 //   Assert(oNeighbor.FREQ_SPACING_MHz>0);

    iFreqSpacing_MHz:= Ceil(oNeighbor.FREQ_SPACING_MHz * 100);

 //   oNeighbor.Fr
//    iFreqSpacing_MHz:= Ceil( oNeighbor.Fr FieldByName(FLD_Freq_Spacing_MHz).AsFloat*100);

//    iFreqSpacing_MHz:= Ceil(FieldByName(FLD_Freq_Spacing_MHz).AsFloat*100);

    oItem:= FFreqPlan_calc_export.NeighborsByCA.AddItem
            (iTX_LINKEND_ID,
             iRX_LINKEND_ID,
             iFreqSpacing_MHz);

    oItem.LinkEnd1_name:= oNeighbor.LINKEND1_name;
    oItem.LinkEnd2_name:= oNeighbor.LINKEND2_name;


     {
    db_AddRecord(mem_MatrixCA, [db_par('CELL1_ID',  iTX_LINKEND_ID),
                                db_par('CELL2_ID',  iRX_LINKEND_ID),
                                db_par('cell_count', iFreqSpacing_MHz)
                            ]);    } 
//    Next;
  end;

  Result := True;

end;




//--------------------------------------------------------------------
procedure TdmLinkFreqPlan_calc.SaveToXML(aFileName, aMatrixFileDir: string);
//--------------------------------------------------------------------
var vRoot,vParams,vSectors: IXMLNode;
    i: Integer;
var
  oXMLDoc: TXMLDoc;
  vNode: IXMLNode;
begin
  oXMLDoc:=TXMLDoc.Create;

  vRoot:=oXMLDoc.DocumentElement.AddChild('FREQ_PLAN');
  vParams:=vRoot.AddChild('params');

    xml_AddNode (vParams, 'ChannelsAllowed', [xml_Att(ATT_value, '1-124')]);
    xml_AddNode (vParams, 'DuplexFreqDelta', [xml_Att(ATT_value, 45)]);
    xml_AddNode (vParams, 'ChannelWidth',    [xml_Att(ATT_value, 200)]);

    xml_AddNode (vParams, 'MaxCI',             [xml_Att(ATT_value, 12)]);
    xml_AddNode (vParams, 'MaxCA',             [xml_Att(ATT_value, 3)]);
    xml_AddNode (vParams, 'NeighborMatrixType',[xml_Att(ATT_value, 2)]);  //2!!!
    xml_AddNode (vParams, 'CI',                [xml_Att(ATT_value, 12)]);
    xml_AddNode (vParams, 'CA',                [xml_Att(ATT_value, 9)]);
    xml_AddNode (vParams, 'SitesFreqSpacing',  [xml_Att(ATT_value, 0)]);//  FParams.SitesFreqSpacing)]);
    xml_AddNode (vParams, 'SectorFreqSpacing', [xml_Att(ATT_value, FData.LinkFreqPlan.SectorFreqSpacing)]);
    xml_AddNode (vParams, 'SiteFreqSpacing',   [xml_Att(ATT_value,
          IIF(FData.LinkFreqPlan.SiteFreqSpacing>0, FData.LinkFreqPlan.SiteFreqSpacing-1, 0))]);

    xml_AddNode (vParams, 'IsUseGarmonic_BS_PO',    [xml_Att(ATT_value, 0)]);
    xml_AddNode (vParams, 'IsUseGarmonic_BS_BS',    [xml_Att(ATT_value, 0)]);

    xml_AddNode (vParams, 'PercentCI',              [xml_Att(ATT_value, 0)]);
    xml_AddNode (vParams, 'PercentTrf',             [xml_Att(ATT_value, 0)]);

    xml_AddNode (vParams, 'MaxPorogPercent',        [xml_Att(ATT_value, 100)]);
    xml_AddNode (vParams, 'SectorInterferenceType', [xml_Att(ATT_value, 0)]);
    xml_AddNode (vParams, 'IsLossQualityByAllFreq', [xml_Att(ATT_value, 0)]);
    xml_AddNode (vParams, 'IsLossQualityByHandoff', [xml_Att(ATT_value, 0)]);
    xml_AddNode (vParams, 'BestSecPereborType',     [xml_Att(ATT_value, 0)]);

    xml_AddNode (vParams, 'StartSectorNum',         [xml_Att(ATT_value, FData.LinkFreqPlan.StartSectorNum)]);
    xml_AddNode (vParams, 'PereborQueueType',       [xml_Att(ATT_value, FData.LinkFreqPlan.PereborQueueType)]);
    xml_AddNode (vParams, 'FreqDistribType',        [xml_Att(ATT_value, FData.LinkFreqPlan.FreqDistribType)]);
    xml_AddNode (vParams, 'MaxIterCount',           [xml_Att(ATT_value, FData.LinkFreqPlan.MaxIterCount)]);
    xml_AddNode (vParams, 'PereborOrderType',       [xml_Att(ATT_value, FData.LinkFreqPlan.PereborOrderType)]);
    xml_AddNode (vParams, 'IsUniformFreqRepeat',    [xml_Att(ATT_value, 0)]);
    xml_AddNode (vParams, 'MaxFreqRepeat',          [xml_Att(ATT_value, FData.LinkFreqPlan.MaxFreqRepeat)]);

    xml_AddNode (vParams, 'BestFreqPereborType',    [xml_Att(ATT_value, 0)]);
    xml_AddNode (vParams, 'NotAllowFreqRepeat',     [xml_Att(ATT_value, 0)]);



  //----------------------------------------

  vSectors:=xml_AddNode (vRoot, 'SECTORS', [xml_Att('count', FData.Linkends.Count)]);
  for i := 0 to FData.Linkends.Count-1 do
    AddLinkEnd(FData.Linkends[i], FData.Linkends[i].Linkend_ID, vSectors, i + 1);

  vNode:=xml_AddNode (vRoot, 'NeighborsByCI',
           [
            xml_Att('comment', ''),
            xml_Att('FILENAME', aMatrixFileDir + MATRIX_CI_FILENAME),
            xml_Att('count', FFreqPlan_calc_export.NeighborsByCI.Count),

            xml_Att('comment', 'iIsNeigh')

           ]);
  FFreqPlan_calc_export.NeighborsByCI.SaveToXMLNode(vNode);

  //----------------------------------------

  vNode:=xml_AddNode (vRoot, 'NeighborsByCA',
          [
           xml_Att('comment', ''),
           xml_Att('FILENAME', aMatrixFileDir + MATRIX_CA_FILENAME),
           xml_Att('count', FFreqPlan_calc_export.NeighborsByCA.Count),

           xml_Att('comment', 'iFreqSpacing_MHz')

          ]);
  FFreqPlan_calc_export.NeighborsByCA.SaveToXMLNode(vNode);

  //----------------------------------------

  vNode:=xml_AddNode (vRoot, 'NeighborsByTrfCI',
         [
          xml_Att('comment', 'Link'),
          xml_Att('FILENAME', aMatrixFileDir + MATRIX_TRF_FILENAME),
          xml_Att('count', FFreqPlan_calc_export.NeighborsByTrf.Count),

          xml_Att('comment', 'by link')

         ]);

  FFreqPlan_calc_export.NeighborsByTrf.SaveToXMLNode(vNode);

  oXMLDoc.SaveToFile(aFileName);

  oXMLDoc.Free;
end;

//--------------------------------------------------------------------
procedure TdmLinkFreqPlan_calc.AddLinkEnd(aLinkend: TnbLinkFreqPlan_Linkend;
    aLinkEndID: integer; aNode: IXMLNode; aCount: Integer);
//--------------------------------------------------------------------
{
const
  SQL_SELECT_LINKFREQPLAN_LINKEND =
     'SELECT * FROM '+ TBL_LINKFREQPLAN_LINKEND +
     ' WHERE (LinkFreqPlan_id=:LinkFreqPlan_id) and ' +
     '       (LinkEnd_id=:LinkEnd_id)';
}


var
  b: boolean;
  sFixed, sBand, sChannelsAllowed: string;
  iPropID, iFreqRecCount,
   // iChTypeFixed,
  iChShift, i: integer;
  dTxRx_Shift, dChannelWidth, dFreqMinLow_MHz: Double;
  arrFixed, arrFixed_All, arrFORBIDDEN: u_func.TIntArray;
//  b: boolean;
  bIsLow: Boolean;
//  sChannelTypeFixed: string;
  sFORBIDDEN: string;

//  channelTypeFixed : TLinkendChannelType;

  oCell: TFreqPlan_Cell_export;


 // oLinkend: TnbLinkFreqPlan_Linkend;
  s: string;
begin
//  oLinkend:=FExportParams.LinkEnds.FindByID(aLinkEndID);
//  oLinkend:=FData.LinkEnds.FindByID(aLinkEndID);

  Assert(Assigned(aLinkend), 'Value not assigned');

  {

  db_OpenQuery(qry_LinkFreqPlan_LinkEnd, SQL_SELECT_LINKFREQPLAN_LINKEND,
                      [db_par(FLD_LINKEND_ID,      aLinkEndID),
                       db_par(FLD_LINKFREQPLAN_ID, Params.LinkFreqPlan_ID)]);
}

//  if qry_LinkFreqPlan_LinkEnd.FieldByName(FLD_ENABLED).AsInteger = 0 then
//    Exit;

  dTxRx_Shift:=0;
  dChannelWidth:=0;
  dFreqMinLow_MHz:=0;
  sChannelsAllowed:='';

 // if aLinkEndID=203838 then
 //   s:=s;

  dTxRx_Shift:=aLinkend.TxRx_Shift;
  dChannelWidth:=aLinkend.ChannelWidth;
  dFreqMinLow_MHz:=aLinkend.FreqMinLow_MHz;

 // sChAllowed:=oLinkend.ChannelType;

/// /dTxRx_Shift, dChannelWidth, dFreqMin_MHz,


  b:= GetFreq_Resource(aLinkend, // aLinkEndID, dTxRx_Shift, dChannelWidth, dFreqMinLow_MHz,
  sChannelsAllowed);

  {
  if (dTxRx_Shift=0) or (dChannelWidth=0) or (dFreqMinLow_MHz=0) then
  begin
    s:=gl_DB.GetNameByID(TBL_LINKEND, aLinkEndID);

    ErrorDlg( Format('��� ��� "%s" �� ������ �����������!', [s]));
    Exit;
  end;
  }

  dTxRx_Shift    := aLinkEnd.TxRx_Shift;
  dChannelWidth  := aLinkEnd.ChannelWidth;
  dFreqMinLow_MHz:= aLinkEnd.FreqMinLow_MHz;



//  dTxRx_Shift     := TruncFloat(dTxRx_Shift, 3);
//  dChannelWidth   := TruncFloat(dChannelWidth, 3);
//  dFreqMinLow_MHz := TruncFloat(dFreqMinLow_MHz, 3);

//  db_OpenQuery(qry_LinkEnd, 'SELECT property_id, RANGE FROM '+ TBL_LINKEND + ' WHERE (id=:id) ',

{
  db_OpenQuery(qry_LinkEnd,
              'SELECT property_id, band FROM '+ TBL_LINKEND + ' WHERE (id=:id) ',
              [db_par(FLD_ID, aLinkEndID)]);
 }

//  db_OpenTableByID(qry_LinkEnd, TBL_LINKEND, aLinkEndID);

  iPropID:= aLinkEnd.PROPERTY_ID;
  sBand  := aLinkEnd.BAND;





//  iPropID:= qry_LinkEnd.FieldByName(FLD_PROPERTY_ID).AsInteger;
//  sBand  := qry_LinkEnd.FieldByName(FLD_BAND).AsString;
//  iRange := qry_LinkEnd.FieldByName(FLD_RANGE).AsInteger;

  //  channelTypeFixed:= aLinkEnd.ChannelFixedType;

    arrFixed     := StringToIntArray(aLinkEnd.FREQ_FIXED);
    arrFORBIDDEN := StringToIntArray(aLinkEnd.FREQ_FORBIDDEN);
    iFreqRecCount:= aLinkEnd.FreqReqCount;

{
  with qry_LinkFreqPlan_LinkEnd do
  begin

   // sChannelTypeFixed := aLinkEnd.CHANNEL_TYPE_FIXED;

//    sChannelTypeFixed := FieldByName(FLD_CHANNEL_TYPE_FIXED).AsString;
//    iChTypeFixed := FieldByName(FLD_CH_TYPE_FIXED).AsInteger;
    arrFixed     := StringToIntArray(FieldByName(FLD_FREQ_FIXED).AsString);

    sFORBIDDEN := FieldByName(FLD_FREQ_FORBIDDEN).AsString;

    arrFORBIDDEN := StringToIntArray(sFORBIDDEN);

    iFreqRecCount:= FieldByName(FLD_FREQ_REQ_COUNT).AsInteger;
  end;
  }


  { TODO : �����������������!!! }
  iChShift:= Round(dTxRx_Shift/dChannelWidth);

  for i := 0 to High(arrFixed) do
    if (arrFixed[i]<>0) and
       ((i = 0) and (arrFixed[i]   < iChShift) or
        (i > 0) and (arrFixed[i]-1 < iChShift)) then
    begin
   //   bIsLow:=Eq(sChannelTypeFixed,'low');
    //  bIsLow:=Eq(sChannelTypeFixed,'low');

   ///   b:=aLinkEnd.ChannelFixedType,'low');
   //  b:=Eq(sChannelTypeFixed,'low');

      IntArrayAddValue(arrFixed_All, IIF(aLinkEnd.ChannelType = ctLow, arrFixed[i],            arrFixed[i] + iChShift));
      IntArrayAddValue(arrFixed_All, IIF(aLinkEnd.ChannelType = ctLow, arrFixed[i] + iChShift, arrFixed[i]));


//      IntArrayAddValue(arrFixed_All, IIF(aLinkEnd.ChannelFixedType = ctLow, arrFixed[i],            arrFixed[i] + iChShift));
//      IntArrayAddValue(arrFixed_All, IIF(aLinkEnd.ChannelFixedType = ctLow, arrFixed[i] + iChShift, arrFixed[i]));

//      IntArrayAddValue(arrFixed_All, IIF(Eq(sChannelTypeFixed,'low'), arrFixed[i], arrFixed[i] + iChShift));
//      IntArrayAddValue(arrFixed_All, IIF(Eq(sChannelTypeFixed,'Low'), arrFixed[i] + iChShift, arrFixed[i]));


(*      IntArrayAddValue(arrFixed_All, IIF(iChTypeFixed=0, arrFixed[i], arrFixed[i] + iChShift));
      IntArrayAddValue(arrFixed_All, IIF(iChTypeFixed=0, arrFixed[i] + iChShift, arrFixed[i]));
*)
   end;
  sFixed:= IntArrayToString(arrFixed_All);


  Assert(aLinkEnd.Linkend_id = aLinkEndID);



   oCell:=Fcalc_params.Cells.AddItem();
                      
   oCell.ID:= aLinkEndID;

    //� 1-�� ������ �������� ������, �� 2-�� - ������ "����. ����"
   oCell.SiteID             :=  iPropID;

   oCell.DuplexFreqDelta   := Round(dTxRx_Shift*100);
   oCell.ChannelWidth      := Round(dChannelWidth*100);
   oCell.FreqMin           := Round(dFreqMinLow_MHz*100);

   oCell.Range             := sBand;

   oCell.LinkEnd_name      := aLinkEnd.Name;
   oCell.PROPERTY_name     := aLinkEnd.PROPERTY_name;

   oCell.FreqRequiredCount := iFreqRecCount;
   oCell.FreqFixed         := sFixed;

   oCell.FreqForbidden     := sFORBIDDEN;
   oCell.total_cells       := 1;
   oCell.ChannelsAllowed   := sChannelsAllowed;



  xml_AddNode (aNode, 'SECTOR',
      [xml_Att('id',                aLinkEndID),

        //� 1-�� ������ �������� ������, �� 2-�� - ������ "����. ����"
       xml_Att('SiteID',  IIF(FData.LinkFreqPlan.SiteFreqSpacing>0, iPropID, aCount)),


// ��������� 3 ��������� ��������� �� 100 ��� �������� � ������� ���
//       xml_Att('DuplexFreqDelta111',   Round(dTxRx_Shift*100)),
       xml_Att('DuplexFreqDelta',   Round(dTxRx_Shift*100)),
//       xml_Att('DuplexFreqDelta',   Round(dTxRx_Shift*100)),
       xml_Att('ChannelWidth',      Round(dChannelWidth*100)),
       xml_Att('FreqMin',           Round(dFreqMinLow_MHz*100)),

       xml_Att('Range',             sBand),



       xml_Att('LinkEnd_name', aLinkEnd.Name),
       xml_Att('PROPERTY_name', aLinkEnd.PROPERTY_name),


       xml_Att('FreqRequiredCount', iFreqRecCount),
       xml_Att('FreqFixed',         sFixed),
      // xml_Att('FreqForbidden',     ''),
       xml_Att('FreqForbidden',     sFORBIDDEN),
       xml_Att('total_cells',       1),
       xml_Att('ChannelsAllowed',   sChannelsAllowed),


       // Added by alex 09.05.2010 7:52:37
       //new
       xml_Att('Band',              sBand)
      ]);

end;

//--------------------------------------------------------------------
function TdmLinkFreqPlan_calc.GetForbidden(aLinkend: TnbLinkFreqPlan_Linkend;
    //aLinkEndID : integer;
    //aFreqMinLow, aChannelWidth : double;

    aChannelArray:
    TIntArray): TIntArray;
//--------------------------------------------------------------------
//var
//    arrDouble: u_func.TDoubleArray;

    //------------------------------------------------------
    procedure DoGet(aArrAllow, aArrForbidden: u_func.TStrArray);
    //------------------------------------------------------
    var i, j: integer;
        eFreqMinLow, eChannelWidth : double;

     arrDouble: u_func.TDoubleArray;

    begin
       eFreqMinLow  :=aLinkend.FreqMinLow_MHz;
       eChannelWidth:=aLinkend.ChannelWidth;


        if (Length(aArrAllow) = 0) and (Length(aArrForbidden) = 0 ) then
          Exit;


     // if Length(aChannelArray) > 0 then
        for i:=0 to High(aChannelArray) do
        begin
         // if Length(aArrAllow) > 0 then  //������ ������ ����������
            for j:=0 to High(aArrAllow) do
            begin
              arrDouble:= StringToDoubleArray (aArrAllow[j], '-');

              if Length(arrDouble) = 1 then  //��� ����� �������
                if eFreqMinLow + ((aChannelArray[i]-1)*eChannelWidth) <> arrDouble[0] then
                begin
                  IntArrayAddValue(Result, aChannelArray[i]);
                  Break;
                end;

              if Length(arrDouble) = 2 then //��� ������ ������
                if (eFreqMinLow+((aChannelArray[i]-1)*eChannelWidth) < arrDouble[0]) or
                   (eFreqMinLow+((aChannelArray[i]-1)*eChannelWidth) > arrDouble[1]) then
                begin
                  IntArrayAddValue(Result, aChannelArray[i]);
                  Break;
                end;
            end;

    //    if Length(aArrForbidden) > 0 then  //������ ������ ����������
          for j:=0 to High(aArrForbidden) do
          begin
            arrDouble:= StringToDoubleArray (aArrForbidden[j], '-');

            if Length(arrDouble) = 1 then  //��� ����� �������
              if eFreqMinLow + ((aChannelArray[i]-1)*eChannelWidth) = arrDouble[0] then
              begin
                IntArrayAddValue(Result, aChannelArray[i]);
                Break;
              end;

            if Length(arrDouble) = 2 then //��� ������ ������
              if (eFreqMinLow + ((aChannelArray[i]-1)*eChannelWidth) >= arrDouble[0]) and
                 (eFreqMinLow + ((aChannelArray[i]-1)*eChannelWidth) <= arrDouble[1]) then
              begin
                IntArrayAddValue(Result, aChannelArray[i]);
                Break;
              end;
          end;
      end;
    end;    //


var
  arrStrAllow, arrStrForbidden: TStrArray;
  sBand: string;


  oResource: TnbResource;

//  oLinkend: TcLinkFreqPlan_Linkend;
begin
//  oLinkend:=FExportParams.LinkEnds.FindByID(aLinkEndID);

  Assert(Assigned(aLinkend), 'Value not assigned');



//  iRange:= dmLinkEnd.GetIntFieldValue(aLinkEndID, FLD_RANGE);
//  sBand:= dmLinkEnd.GetStringFieldValue(aLinkEndID, FLD_BAND_NAME);
//  sBand:= dmLinkEnd.GetStringFieldValue(aLinkEndID, FLD_BAND);

  sBand:=aLinkend.BAND;
 // sBand:= gl_DB.GetStringFieldValueByID(tbl_LinkEnd, FLD_BAND, aLinkEndID);

 {
  db_OpenQ uery(qry_Temp,
              'SELECT * FROM ' + TBL_LINKFREQPLAN_RESOURCE +
              ' WHERE (band=:band) and (linkfreqplan_id=:linkfreqplan_id)',

              [db_par(FLD_band,             sBand),
               db_par(FLD_LINKFREQPLAN_ID, Params.LinkFreqPlan_ID)
               ]);

               }

//          ' WHERE (range=:range) and (link_freqplan_id=:link_freqplan_id)',

//  FData.Resources.LoadFromDataset(qry_Temp);


 // FExportParams.LoadFromDataset_BandResources(qry_Temp);

  Assert(FData.Resources.Count>0, 'FData.Resources.Count>0');

  oResource:=FData.Resources.FindByBand(aLinkend.BAND);
  Assert(Assigned(oResource));

  arrStrAllow     := StringToStrArray(oResource.FREQ_BAND_ALLOW_Mhz);
  arrStrForbidden := StringToStrArray(oResource.FREQ_BAND_FORBIDDEN_Mhz);

  {
  qry_Temp.First;

  with qry_Temp do
  begin
    arrStrAllow     := StringToStrArray(FieldByName(FLD_FREQ_ALLOW).AsString);
    arrStrForbidden := StringToStrArray(FieldByName(FLD_FREQ_FORBIDDEN).AsString);
  end;
   }

  DoGet(arrStrAllow, arrStrForbidden); //����� ����������-����������

  {
  db_Open Query(qry_Temp,
               'SELECT * FROM ' + TBL_LINKFREQPLAN_LINKEND  +
               ' WHERE (linkend_id=:linkend_id) and '+
               '       (linkfreqplan_id=:linkfreqplan_id)',
            [db_par(FLD_LINKEND_ID,       aLinkEndID),
             db_par(FLD_LINKFREQPLAN_ID, Params.LinkFreqPlan_ID)
             ]);
  }
  arrStrAllow     := StringToStrArray(aLinkend.FREQ_ALLOW);
  arrStrForbidden := StringToStrArray(aLinkend.FREQ_FORBIDDEN);

  {
  with qry_Temp do
  begin
    arrStrAllow     := StringToStrArray(FieldByName(FLD_FREQ_ALLOW).AsString);
    arrStrForbidden := StringToStrArray(FieldByName(FLD_FREQ_FORBIDDEN).AsString);
  end;
  }

        { TODO : ����� ���������! }
  DoGet(arrStrAllow, arrStrForbidden); //��������� ����������-����������

end;

//--------------------------------------------------------------------
function TdmLinkFreqPlan_calc.GetFreq_Resource( aLinkend: TnbLinkFreqPlan_Linkend;
//aLinkEndID: integer; var
//    aTxRx_Shift, aChannelWidth, aFreqMinLow: double;

    var aChannelsAllowed:
    string): boolean;
//--------------------------------------------------------------------
var // iMode,
 // iChType,
  iChCount, iChSkip, iChShift, iChNum, i: integer; //, iLinkEndTypeID
 // dChannelSpacing: double;
  arrForbidden, arrChannel_all, arrChannel_low, arrChannel_high: u_func.TIntArray;
  b: boolean;
 // eChannelSpacing: Double;
//  iLinkEndType_Mode_ID: integer;
  sChannelType: string;

 // oLinkend: TnbLinkFreqPlan_Linkend;
begin
//  oLinkend:=FData.LinkEnds.FindByID(aLinkEndID);

 // Assert(Assigned(oLinkend), 'Value not assigned');


  Result:=False;

  ClearArray(arrChannel_low);
  ClearArray(arrChannel_high);
  ClearArray(arrChannel_all);

   {

  db_OpenQuery(qry_LinkEnd,
    'SELECT mode, channel_type, linkendtype_mode_id, linkendtype_id FROM '+ TBL_LINKEND
//    'SELECT mode, channel_number_type, linkendtype_mode_id, linkendtype_id FROM '+ TBL_LINKEND
      + ' WHERE (id=:id) ',
    [db_par(FLD_ID, aLinkEndID)]);

   }{
 eChannelSpacing:= oLinkend.ch_spacing;


//  db_OpenTableByID(qry_LinkEnd, TBL_LINKEND, aLinkEndID);
  with qry_LinkEnd do
  begin
//    iMode         := FieldByName(FLD_MODE).AsInteger;
    sChannelType       := Lowercase( FieldByName(FLD_CHANNEL_TYPE).AsString);
  //  iChType       := FieldByName(FLD_CHANNEL_NUMBER_TYPE).AsInteger;

    iLinkEndTypeID      := FieldByName(FLD_LINKENDType_ID).AsInteger;
    iLinkEndType_Mode_ID:= FieldByName(FLD_LINKENDType_Mode_ID).AsInteger;

    if iLinkEndTypeID = 0 then
      Exit;
  end;
   }

 // b:=dmLinkEnd_ex.GetBand_Params(aLinkEndID, aTxRx_Shift, aChannelWidth, aFreqMinLow, iChCount, OBJ_LINKEND);
//  b:=dmLinkEnd_ex.GetBand_Params1(aLinkEndID, aTxRx_Shift, aChannelWidth, aFreqMin, iChCount, OBJ_LINKEND);
 {
  if aChannelWidth = 0 then
    Exit;

  dChannelSpacing:= gl_DB.GetDoubleFieldValue(TBL_LINKENDTYPE_MODE, FLD_ch_spacing,
                        [
                         db_par(FLD_ID, iLinkEndType_Mode_ID)
                        // ,
                        // /db_par(FLD_LinkEndType_id, iLinkEndTypeID),
                        // db_par(FLD_MODE,            iMode)
                         ]);

  if dChannelSpacing = 0 then
    dChannelSpacing:= gl_DB.GetDoubleFieldValue(TBL_LINKENDTYPE_MODE, FLD_bandwidth,
                        [
                         db_par(FLD_ID, iLinkEndType_Mode_ID)
                        // db_par(FLD_LinkEndType_id, iLinkEndTypeID),
                        // db_par(FLD_MODE,            iMode)
                         ]);
  }


//  if (dChannelSpacing = aChannelWidth) or (dChannelSpacing = 0) then
//    for i := 0 to iChCount-1 do
//      IntArrayAddValue(arrChannel_low, i+1)
//  else
  begin
//    if (dChannelSpacing < 0) or (aChannelWidth < 0) then
 //     Exit;

   iChCount:=aLinkend.ChannelCount;

    iChNum:=1;
    while iChNum <= iChCount do
    begin
      IntArrayAddValue(arrChannel_low, iChNum);

      // dChannelSpacing:= gl_DB.GetDoubleFieldValue(TBL_LINKENDTYPE_MODE, FLD_bandwidth,

      Assert (aLinkend.ChannelWidth>0);

      iChSkip:= Ceil(aLinkend.BANDWIDTH/aLinkend.ChannelWidth);

//      iChSkip:= Ceil(dChannelSpacing/aChannelWidth);
      Inc(iChNum, iChSkip);
    end;    // while
  end;


  arrForbidden := GetForbidden(aLinkend, //aLinkEndID, aFreqMinLow, aChannelWidth,
  arrChannel_low);

  if Length(arrForbidden)>0 then
    IntArrayRemoveValues(arrChannel_low,  arrForbidden);
//             GetForbidden(aLinkEndID, aFreqMin, aChannelWidth, arrChannel_low));

//  iChShift:=aLinkend.ChShift;

   Assert (aLinkend.ChannelWidth>0, 'aLinkend.ChannelWidth>0)');


  iChShift:= Round(aLinkend.TxRx_Shift/aLinkend.ChannelWidth);
//  iChShift:= Round(aTxRx_Shift/aChannelWidth);

  i:= 0;
  while (i<Length(arrChannel_low)) and (arrChannel_low[i] <= iChCount) do
  begin
    IntArrayAddValue(arrChannel_high, arrChannel_low[i]+iChShift);

    Inc(i);
  end;    // while

  arrForbidden :=GetForbidden(aLinkend, //aLinkEndID, aFreqMinLow, aChannelWidth,
           arrChannel_high);

  if Length(arrForbidden)>0 then
    IntArrayRemoveValues(arrChannel_high, arrForbidden);

  if aLinkend.ChannelType = ctHigh then

//  if Eq(sChannelType, 'high') then
//  if iChType = 1 then
    arrChannel_all:= MakeIntArray(arrChannel_high)
  else // iChType = 0
    for i := 0 to Length(arrChannel_high)-1 do
      IntArrayAddValue(arrChannel_all, arrChannel_high[i]-iChShift);

  IntArraySort(arrChannel_all);
  aChannelsAllowed:= IntArrayToString(arrChannel_all);

  Result:=True;
end;

//--------------------------------------------------------------------
procedure TdmLinkFreqPlan_calc.Load_XMLToDB (aFileName: string);
//--------------------------------------------------------------------
const

  SQL_UPDATE_LINK_FREQPLAN_LINKEND =
       'UPDATE '+ TBL_LINKFREQPLAN_LINKEND +
       ' SET freq_distributed =:freq_distributed,   '+
       '     Calc_Order       =:Calc_Order,               '+
    //   '     channel_type_distributed=:channel_type_distributed ' +
       '     freq_distributed_tx=:freq_distributed_tx, ' +
       '     freq_distributed_rx=:freq_distributed_rx ' +
//       ' WHERE (LinkFreqPlan_id=:LinkFreqPlan_id) and '+
 //      '       (linkend_id=:linkend_id)';

       ' WHERE (id=:id)';


(*
  SQL_UPDATE_LINK_FREQPLAN_LINKEND1 =
       'UPDATE '+ TBL_LINKFREQPLAN_LINKEND +
       ' SET freq_distributed=:freq_distributed,   '+
       '     ch_type_distributed=:ch_type_distributed ' +
       ' WHERE (LinkFreqPlan_id=:LinkFreqPlan_id) and '+
       '       (linkend_id=:linkend_id)';
*)

var vNode,vGroup,vRoot: IXMLNode;
    i, iLinkEndID, //iDistributedCh,
    iCalcOrder: Integer;
    sChType : string;
    //iChType : string;
   // iChannelCount,
   //  iChShift: Integer;
  //  dTxRx_Shift, dChannelWidth, dFreqMin, dTxFreq, dRxFreq: double;

  oXMLDoc: TXMLDoc;
  b: boolean;

  oLinkend: TnbLinkFreqPlan_Linkend;
  s: string;
  sDistributedCh: string;


  arrChannel: u_func.TIntArray;
  dRxFreq: Double;
  dTxFreq: Double;
  j: Integer;


  rInfo: TnbLinkFreqPlan_Linkend_Get_Info;


begin
 // ShellExec_Notepad(aFileName);

  /////////////////////


  oXMLDoc:=TXMLDoc.Create;

  oXMLDoc.LoadFromFile(aFileName);

  vRoot:=oXMLDoc.DocumentElement;

  if Assigned(vRoot) then
    if vRoot.ChildNodes.Count>0 then
  begin

//    vRoot:=oXMLDoc.DocumentElement;
//    vGroup:= vRoot.ChildNodes.FindNode('SECTORS');
    vGroup:= xml_FindNode(vRoot, 'SECTORS');
//    xml_GetNodeByPath(vRoot, ['SECTORS']);

    for i:=0 to vGroup.ChildNodes.Count-1 do
    begin
      vNode:=vGroup.ChildNodes[i];

      s:= xml_GetStrAttr(vNode, 'FreqDistributed');


      iLinkEndID    := xml_GetIntAttr(vNode, FLD_ID);

      sDistributedCh:= xml_GetStrAttr(vNode, 'FreqDistributed');

      arrChannel:= StringToIntArray (sDistributedCh);

    // iDistributedCh:= xml_GetIntAttr(vNode, 'FreqDistributed');
      iCalcOrder    := xml_GetIntAttr(vNode, 'CalcOrder');


      oLinkend := FData.Linkends.FindByLinkendID(iLinkEndID);

      Assert(Assigned(oLinkend), 'Value not assigned');


      if oLinkend.ChannelType=ctHigh then
      begin
        for j:=0 to High(arrChannel) do
          arrChannel[j]:=arrChannel[j] - oLinkend.ChShift1;

          //      ChShift1:=Round( TxRx_Shift /ChannelWidth)  ;


        sDistributedCh := IntArrayToString(arrChannel);



       // arrChannel:= StringToIntArray (sDistributedCh);
      end;


    oLinkend.freq_distributed:= sDistributedCh;




   rInfo:=oLinkend.Get();

   if Length(rInfo.Distr_TX_arr)>0 then dTxFreq:=AsFloat(rInfo.Distr_TX_arr[0]) else dTxFreq:=0;
   if Length(rInfo.Distr_RX_arr)>0 then dRxFreq:=AsFloat(rInfo.Distr_RX_arr[0]) else dRxFreq:=0;


{
    FieldByName(FLD_FREQ_DISTRIBUTED_TX_STR).Value := StrArrayToString(rInfo.Distr_TX_arr);
    FieldByName(FLD_FREQ_DISTRIBUTED_RX_STR).Value := StrArrayToString(rInfo.Distr_RX_arr);


    FieldByName(FLD_FREQ_FIXED_TX_STR).Value := StrArrayToString(rInfo.Fixed_TX_arr);
    FieldByName(FLD_FREQ_FIXED_RX_STR).Value := StrArrayToString(rInfo.Fixed_RX_arr);
}

 //   exit;


      {
      if iDistributedCh = 0 then
        sChType:= ''
//        iChType:= -1
      else
      begin
    //    iChannelCount:=oLinkend.ChannelCount;
//        iChShift:=Round( oLinkend.TxRx_Shift /  oLinkend.ChannelWidth)  ;
       }

       // b:=dmLinkEnd_ex.GetBand_Params (iLinkEndID, dTxRx_Shift, dChannelWidth, dFreqMin, iChannelCount, OBJ_LINKEND);

//        b:=dmLinkEnd.GetBand_Params1 (iLinkEndID, dTxRx_Shift, dChannelWidth, dFreqMin, iChannelCount, OBJ_LINKEND);
     //   iChShift:= oLinkend.ChShift;
    //    iChShift:= Round(dTxRx_Shift/dChannelWidth);



//        if iDistributedCh > iChannelCount then

      {
        if iDistributedCh > oLinkend.ChannelCount then
        begin
          iDistributedCh:= iDistributedCh - iChShift;
          sChType:= 'high';
   //       iChType:= 1;
        end
        else
          sChType:= 'low';
//          iChType:= 0;
          }
{
        if sChType='low' then
//        case iChType of
  //          0:
              begin  //low
                 dTxFreq:= dFreqMin + dChannelWidth * (iDistributedCh - 1);
                 dRxFreq:= dTxFreq  + dTxRx_Shift;
               end;

        if sChType='high' then
           // 1:
              begin  //high
                 dRxFreq:= dFreqMin + dChannelWidth * (iDistributedCh - 1);
                 dTxFreq:= dRxFreq  + dTxRx_Shift;
               end;
//        else
  //        raise Exception.Create('');
    //    end;

    }
   //   end;

//      dmOnega_DB_data.

(*      gl_DB.ExecCommand(SQL_UPDATE_LINK_FREQPLAN_LINKEND1,
          [
           db_par(FLD_LINKFREQPLAN_ID,    Params.LinkFreqPlan_ID),
           db_par(FLD_LINKEND_ID,          iLinkEndID),
           db_par(FLD_FREQ_DISTRIBUTED,    IIF(iDistributedCh>0, iDistributedCh, NULL)),

           db_par(FLD_CH_TYPE_DISTRIBUTED, sChType)


//           db_par(FLD_CH_TYPE_DISTRIBUTED, IIF(iChType=-1, NULL, iChType))
           ]);

*)

      gl_DB.ExecCommand(SQL_UPDATE_LINK_FREQPLAN_LINKEND,
          [
           db_par(FLD_ID,    oLinkend.ID),

//           db_par(FLD_LINKFREQPLAN_ID,    Params.LinkFreqPlan_ID),

  //         db_par(FLD_LINKEND_ID,           iLinkEndID),

           db_par(FLD_Calc_order,           iCalcOrder),

//           db_par(FLD_CHANNEL_TYPE_DISTRIBUTED, sChType),
           db_par(FLD_FREQ_DISTRIBUTED,     sDistributedCh),

///           db_par(FLD_FREQ_DISTRIBUTED,     IIF(iDistributedCh>0, iDistributedCh, NULL))

           db_par(FLD_freq_distributed_tx,  IIF(dTxFreq>0, dTxFreq, NULL)),
           db_par(FLD_freq_distributed_rx,  IIF(dRxFreq>0, dRxFreq, NULL))


//           db_par(FLD_CH_TYPE_DISTRIBUTED, IIF(iChType=-1, NULL, iChType))
          ]);

    end;

  end;

  oXMLDoc.Free;
end;

//--------------------------------------------------------------------
function TdmLinkFreqPlan_calc.Execute: boolean;
//--------------------------------------------------------------------
var sFile1,sFile2,sTempFileDir: string;
  iCode: Integer;
begin
  //LoadParams();
  if not OpenData() then
    Exit;

  sFile1:=g_ApplicationDataDir + TEMP_IN_FILENAME;
  sFile2:=ChangeFileExt(sFile1,'_res.xml');

  sTempFileDir:= IncludeTrailingBackslash (ExtractFileDir(sFile1));

{
  sTempFileDir:= GetTempFileDir;
  sFile1:=GetTempXmlFileName ();
  sFile2:=ChangeFileExt(sFile1,'_res.xml');
}

{

  FFreqPlan_calc_export.NeighborsByCI.Clear;
  FFreqPlan_calc_export.NeighborsByCA.Clear;
  FFreqPlan_calc_export.NeighborsByTrf.Clear;

}


  SaveToXML (sFile1, sTempFileDir);
 // SaveToXML (TEMP_IN_FILENAME, aLinkend_IDList);

//  i:= mem_MatrixCI.RecordCount;

  FFreqPlan_calc_export.NeighborsByCI.SaveToFile (sTempFileDir  + MATRIX_CI_FILENAME);
  FFreqPlan_calc_export.NeighborsByCA.SaveToFile (sTempFileDir  + MATRIX_CA_FILENAME );
  FFreqPlan_calc_export.NeighborsByTrf.SaveToFile (sTempFileDir + MATRIX_TRF_FILENAME );


//  mem_MatrixCI.SaveToTextFile (sTempFileDir + MATRIX_CI_FILENAME);
//  mem_MatrixCA.SaveToTextFile (sTempFileDir + MATRIX_CA_FILENAME);
//  mem_MatrixTrf.SaveToTextFile(sTempFileDir + MATRIX_TRF_FILENAME);


  Result:=RunApp (GetApplicationDir() + EXE_rpls_freq,
                   DoubleQuotedStr(sFile1)+' '+
                   DoubleQuotedStr(sFile2), iCode);

  Load_XMLToDB(sFile2);


  {
  ShellExec_Notepad(sFile1);
  ShellExec_Notepad(sFile2);
  }


  //
 //
 //ShellExec_Notepad(aFileName);

  Result:=True;
end;



begin


end.





