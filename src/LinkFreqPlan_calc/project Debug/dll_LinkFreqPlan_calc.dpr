program dll_LinkFreqPlan_calc;

uses
  Forms,
  app_dm_Main in '..\src\app_dm_Main.pas' {app_dmMain: TDataModule},
  dm_LinkFreqPlan_calc in '..\src\dm_LinkFreqPlan_calc.pas' {dmLinkFreqPlan_calc: TDataModule},
  u_LinkFreqPlan_calc_export in '..\src\u_LinkFreqPlan_calc_export.pas',
  u_FreqPlan_calc_export in '..\src\u_FreqPlan_calc_export.pas',
  d_LinkFreqPlan_calc_data_test in '..\src\d_LinkFreqPlan_calc_data_test.pas' {dlg_LinkFreqPlan_calc_data_test},
  u_LinkFreqPlan_calc_classes in '..\src\u_LinkFreqPlan_calc_classes.pas';

{$R *.RES}




begin
  Application.Initialize;
  Application.CreateForm(Tapp_dmMain, app_dmMain);
  // Application.CreateForm(TdmLinkEnd_ex, dmLinkEnd_ex);
  Application.Run;
end.
