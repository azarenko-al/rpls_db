unit i_link_graph;


interface

//x_Link_graph

uses ADOInt,
  u_dll;

type


  ILink_graphX = interface(IInterface)
  ['{D11A3364-2321-464F-A139-456F6EECD6C6}']

    procedure InitADO(aConnection: _Connection); stdcall;
    procedure InitAppHandle(aHandle: integer); stdcall;

    procedure Execute (aFileName: WideString);  stdcall;
  end;

var
  ILink_graph: ILink_graphX;

  function Load_ILink_graph: Boolean;
  procedure UnLoad_ILink_graph;

implementation

const
//  DEF_BPL_FILENAME = 'bpl_LOS.bpl';
  DEF_FILENAME = 'dll_Link_graph.dll';

var
  LHandle : Integer;


function Load_ILink_graph: boolean;
begin
  Result := GetInterface_DLL(DEF_FILENAME, LHandle,
       ILink_graphX, ILink_graph)=0;
//       ILink_graphX, ILink_graph, 'DllGetInterface_link_graph')=0;

  Assert(Result);

end;


procedure UnLoad_ILink_graph;
begin
  ILink_graph := nil;
  UnloadPackage_dll(LHandle);
end;


end.




