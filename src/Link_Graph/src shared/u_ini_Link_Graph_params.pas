unit u_ini_Link_Graph_params;

interface
uses
   IniFiles, SysUtils, Dialogs;

type
  // ---------------------------------------------------------------
  TIni_Link_Graph_Params = class
  // ---------------------------------------------------------------
  private
  public
    ConnectionString: string;

    LinkID: integer;
 //   UsePassiveElements       : Boolean;

    UseAM  : Boolean;

    IsTest : boolean;
    ProjectID: Integer;

   // IsCalcWithAdditionalRain : Boolean;


    procedure LoadFromFile(aFileName: string);
    procedure SaveToFile(aFileName: string);

    function Validate: boolean;
  end;



implementation


// ---------------------------------------------------------------
procedure TIni_Link_Graph_Params.LoadFromFile(aFileName: string);
// ---------------------------------------------------------------
begin
  Assert(FileExists(aFileName), aFileName);


  with TIniFile.Create(aFileName) do
  begin
    ProjectID  :=ReadInteger ('main', 'ProjectID',  0);
    LinkID     :=ReadInteger ('main', 'ID',  0);


    IsTest   :=ReadBool   ('main', 'IsTest', False);

    LinkID:=  ReadInteger ('main', 'LinkID',  0  );



//    UsePassiveElements   := ReadBool ('main', 'UsePassiveElements',  False);

    Free;
  end;

  Assert(LinkID>0);
end;

// ---------------------------------------------------------------
procedure TIni_Link_Graph_Params.SaveToFile(aFileName: string);
// ---------------------------------------------------------------
begin
  Assert(LinkID>0);

  with TIniFile.Create(aFileName) do
  begin
    WriteInteger ('main', 'ProjectID', ProjectID);
    WriteInteger ('main', 'ID',    LinkID);


    WriteBool ('main', 'IsTest', IsTest);

 //   WriteBool ('main', 'UsePassiveElements',    UsePassiveElements);
    WriteInteger ('main', 'LinkID',    LinkID);


  //  UsePassiveElements       : Boolean;
   // IsCalcWithAdditionalRain : Boolean;

    Free;
  end;
end;




function TIni_Link_Graph_Params.Validate: boolean;
begin
  Result := (LinkID>0) and (ProjectID>0);

//  if not Result then
 //   ShowMessage('(iLinkID=0) or (iProjectID=0)');

end;


end.

