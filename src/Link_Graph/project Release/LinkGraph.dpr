program LinkGraph;

uses
  Windows,
  dm_Main_app in '..\Src\dm_Main_app.pas' {dmMain_app: TDataModule},
  f_Main_link_graph in '..\Src\f_Main_link_graph.pas' {frm_Main_link_graph},
  Forms,
  u_rrl_param_rec_new in '..\..\Link_Calc\src_shared\u_rrl_param_rec_new.pas',
  u_ini_Link_Graph_params in '..\src shared\u_ini_Link_Graph_params.pas',
  u_Link_run in '..\..\Link\u_Link_run.pas',
  i_link_graph in '..\src shared\i_link_graph.pas',
  u_opti_class_new in '..\..\Link_Calc\src\opti\u_opti_class_new.pas',
  u_Opti_classes in '..\..\Link_Calc\src\opti\u_Opti_classes.pas',
  f_Main_opti in '..\..\Link_Calc\src\opti\f_Main_opti.pas' {frm_Main_opti};

{$R *.res}

begin
  SetThreadLocale(1049);


  Application.Initialize;
  
  Application.CreateForm(TdmMain_app, dmMain_app);
  Application.Run;
end.
