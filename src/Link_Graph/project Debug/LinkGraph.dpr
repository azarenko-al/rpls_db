program LinkGraph;

uses
  dm_Main_app in '..\Src\dm_Main_app.pas' {dmMain_app: TDataModule},
  f_Main_link_graph in '..\Src\f_Main_link_graph.pas' {frm_Main_link_graph},
  Forms,
  dm_Link_graph in '..\Src\dm_Link_graph.pas' {dmLink_graph: TDataModule};

{$R *.res}

begin
  Application.Initialize;
  
  Application.CreateForm(TdmMain_app, dmMain_app);
  Application.Run;
end.
