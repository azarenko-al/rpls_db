library dll_link_graph;

{ Important note about DLL memory management: ShareMem must be the
  first unit in your library's USES clause AND your project's (select
  Project-View Source) USES clause if your DLL exports any procedures or
  functions that pass strings as parameters or function results. This
  applies to all strings passed to and from your DLL--even those that
  are nested in records and classes. ShareMem is the interface unit to
  the BORLNDMM.DLL shared memory manager, which must be deployed along
  with your DLL. To avoid using BORLNDMM.DLL, pass string information
  using PChar or ShortString parameters. }

uses
  SysUtils,
  Classes,
  i_link_graph in '..\src shared\I_Link_Graph.pas',
  x_link_graph in '..\src_dll\x_link_graph.pas',
  dm_Main_app in '..\Src\dm_Main_app.pas' {dmMain_app: TDataModule},
  f_Main_link_graph in '..\Src\f_Main_link_graph.pas' {frm_Main_link_graph},
  u_ini_Link_Graph_params in '..\src shared\u_ini_Link_Graph_params.pas',
  u_dll_with_dmMain in '..\..\DLL_common\u_dll_with_dmMain.pas';

{$R *.res}

begin
end.
