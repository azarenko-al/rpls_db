object Form8: TForm8
  Left = 404
  Top = 119
  Width = 598
  Height = 511
  Caption = 'Form8'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Button1: TButton
    Left = 168
    Top = 72
    Width = 75
    Height = 25
    Caption = 'ILink_graph'
    TabOrder = 0
    OnClick = Button1Click
  end
  object cxGrid1: TcxGrid
    Left = 32
    Top = 168
    Width = 265
    Height = 209
    TabOrder = 1
    object cxGrid1DBTableView1: TcxGridDBTableView
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      NavigatorButtons.ConfirmDelete = False
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1DBTableView1
    end
  end
  object Button2: TButton
    Left = 272
    Top = 72
    Width = 75
    Height = 25
    Caption = 'ILink_calc'
    TabOrder = 2
    OnClick = Button2Click
  end
end
