program test_link_graph;

uses
  Forms,
  Unit8 in 'Unit8.pas' {Form8},
  i_link_graph in '..\src shared\i_link_graph.pas',
  i_link_calc in '..\..\Link_Calc\src_shared\I_Link_calc.pas',
  dm_Onega_db_data in '..\..\DataModules\dm_Onega_db_data.pas' {dmOnega_DB_data: TDataModule};

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TForm8, Form8);
  Application.Run;
end.
