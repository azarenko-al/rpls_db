unit Unit8;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs,

  I_Link_graph,
   I_Link_calc,
    
  u_vars,

  dm_Main,


    u_files,


   StdCtrls, cxGridLevel, cxClasses, cxControls, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid
  ;

type
  TForm8 = class(TForm)
    Button1: TButton;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    Button2: TButton;
    procedure FormCreate(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);

  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form8: TForm8;

implementation

{$R *.dfm}

procedure TForm8.FormCreate(Sender: TObject);
begin

  TdmMain.Init;
  if not dmMain.OpenDB_dlg then
//  if not dmMain.OpenFromReg then
    Exit;


end;

procedure TForm8.Button1Click(Sender: TObject);

const
  DEF_TEMP_FILENAME = 'link_calc.ini';

var

//  iProjectID: integer;
  sIniFileName: string;

 // oInifile: TIniFile;
//  blPoint: TBLPoint;

//  BLPoints: TBLPointArrayF;

 // rec: TdmCalcMapAddRec;

 // iMode: (mtLos, mtLosM);

// d: double;
// BLPoint1,BLPoint2,newBLPoint: TBLPoint;

begin

//  sIniFileName:=GetCommonApplicationDataDir(DEF_SOFTWARE_NAME)+
 //                 DEF_TEMP_FILENAME;


 // sIniFileName:=ParamStr(1);
//  if sIniFileName='' then
    sIniFileName:=g_ApplicationDataDir+ DEF_TEMP_FILENAME;


  if not FileExists(sIniFileName) then
  begin
    ShowMessage(sIniFileName);
    Exit;
  end;

  //---------------------------------------------------------------------

(*
  g_Ini_LOS_Params:=TIni_LOS_Params.Create;
  if not g_Ini_LOS_Params.LoadFromFile(sIniFileName) then
    Exit;
*)

(*
  TdmMain.Init;
  if not dmMain.OpenDB_reg then
//  if not dmMain.OpenFromReg then
    Exit;

*)


  if Load_ILink_graph() then
  begin
    ILink_graph.InitADO(dmMain.ADOConnection1.ConnectionObject);
  //  ILink_graph.InitAppHandle(Application.Handle);

 //   ILos.Init( dmMain.ADOConnection1.ConnectionObject);
//    ILos.LoadFromFile(sIniFileName);

    ILink_graph.Execute(sIniFileName);

(*    ILos.Execute;
    ILos.Execute;
    ILos.Execute;
*)
    UnLoad_ILink_graph();
  end;




 // FreeAndNil(dmMain);




end;

procedure TForm8.Button2Click(Sender: TObject);
const
  DEF_SOFTWARE_NAME= 'RPLS_DB_LINK';
  DEF_TEMP_FILENAME = 'link_calc.ini';

var
  sIniFileName: string;
//  oParams: TIni_Link_Calc_Params;

begin
  sIniFileName:=ParamStr(1);
  if sIniFileName='' then
//    sIniFileName:=TEMP_INI_FILE;
    sIniFileName:=GetCommonApplicationDataDir(DEF_SOFTWARE_NAME)+
                  DEF_TEMP_FILENAME;


//  sIniFileName:=GetCommonApplicationDataDir(DEF_SOFTWARE_NAME)+
 //                 DEF_TEMP_FILENAME;


 // sIniFileName:=ParamStr(1);
//  if sIniFileName='' then
    sIniFileName:=g_ApplicationDataDir+ DEF_TEMP_FILENAME;


  if not FileExists(sIniFileName) then
  begin
    ShowMessage(sIniFileName);
    Exit;
  end;

  //---------------------------------------------------------------------

(*
  g_Ini_LOS_Params:=TIni_LOS_Params.Create;
  if not g_Ini_LOS_Params.LoadFromFile(sIniFileName) then
    Exit;
*)

(*
  TdmMain.Init;
  if not dmMain.OpenDB_reg then
//  if not dmMain.OpenFromReg then
    Exit;

*)


  if Load_ILink_calc() then
  begin
    Assert(Assigned(dmMain), 'Value not assigned');

    ILink_calc.InitADO( dmMain.ADOConnection1.ConnectionObject);

    ILink_calc.Execute(sIniFileName);


    UnLoad_ILink_calc();
  end;




//  FreeAndNil(dmMain);




end;




end.

