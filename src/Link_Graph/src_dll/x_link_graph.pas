unit x_link_graph;

interface


uses  ADOInt, Forms,Sysutils, Dialogs, IniFiles,

  u_dll_with_dmMain,

  dm_Main,
//  dm_Onega_DB_data,

  u_func,

  u_Ini_Link_Graph_Params,

  I_link_graph,

  f_Main_link_graph

  ;

type
  Tlink_graphX = class(TDllCustomX, ILink_graphX)
  public
    procedure Execute(aFileName: WideString); stdcall;
  end;

  function DllGetInterface(const IID: TGUID; var Obj): HResult; stdcall;

exports
  DllGetInterface;

implementation


// ---------------------------------------------------------------
procedure Tlink_graphX.Execute(aFileName: WideString);
// ---------------------------------------------------------------
const
  TEMP_INI_FILE = 'link_calc.ini';

var
  oIniFile: TIniFile;

  oParams: TIni_Link_Graph_Params;


begin
  Assert(Assigned(dmMain), 'Assigned(dmMain) not assigned');


  if not FileExists(aFileName ) then
  begin
    ShowMessage(aFileName);
    Exit;
  end;

  oParams:=TIni_Link_Graph_Params.Create;
  oParams.LoadFromFile(aFileName);
  oParams.Validate;


  dmMain.ProjectID:=oParams.ProjectID;


(*

  TdmMain.Init;
  if not dmMain.OpenDB_reg then
    Exit;
*)


  Tfrm_Main_link_graph.ExecDlg (oParams.LinkID, oParams);


  FreeAndNil(oParams);

end;


// ---------------------------------------------------------------
function DllGetInterface(const IID: TGUID; var Obj): HResult;
// ---------------------------------------------------------------
begin
  Result:=S_FALSE; Integer(Obj):=0;

  if IsEqualGUID(IID, ILink_graphX) then
  begin
    with Tlink_graphX.Create() do
      if GetInterface(IID,OBJ) then
        Result:=S_OK
  end else
    raise Exception.Create('if IsEqualGUID(IID, ILink_graphX)');


end;



begin
 // RegisterClass(TdmMain_bpl);

end.

