unit f_Main_link_graph;

interface

 {$DEFINE CompactDatabase}

uses
  SysUtils, Variants, Classes, Controls, Forms, DB, ADODB, DBGrids,ComCtrls, cxVGrid,
  ExtCtrls, cxInplaceContainer, cxGridLevel, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxControls, cxGridCustomView, cxGrid,

  cxDBLookupComboBox,  cxTextEdit, Dialogs,
  TeEngine, dxBar, cxClasses, dxmdaset, ActnList,  cxPropertiesStore, rxPlacemnt, StdCtrls,


  dm_Link_Calc_SESR,

  u_link_calc_classes_export,
  u_link_calc_classes_main,

  u_link_model_layout,

  //u_Ini_rpls_rrl_params,


  cxSpinEdit,

  u_ini_Link_Graph_params,

  dm_Link_calc,

  u_vars,

  u_const,

  u_cx_vgrid_export,


  u_cx_vgrid,
  u_cx,
  u_db,
  u_db_mdb,
  u_files,

  u_Func_arrays,
  u_func,
  u_run,

  u_rrl_param_rec_new,

  Grids, Series, TeeProcs, Chart, DBChart, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, cxEdit, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxDBData
  ;


type
  Tfrm_Main_link_graph = class(TForm)
    Panel1: TPanel;
    ADOConnection_MDB: TADOConnection;
    ds_Functions: TDataSource;
    tb_Functions: TADOTable;
    tb_Arguments: TADOTable;
    ds_Arguments: TDataSource;
    FormStorage1: TFormStorage;
    cxPropertiesStore1: TcxPropertiesStore;
    ActionList1: TActionList;
    act_Start: TAction;
    Panel2: TPanel;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    cxVerticalGrid1: TcxVerticalGrid;
    row_Func1: TcxEditorRow;
    row_Func2: TcxEditorRow;
    row_Argument: TcxEditorRow;
    cxVerticalGrid1CategoryRow1: TcxCategoryRow;
    row_Min: TcxEditorRow;
    row_Max: TcxEditorRow;
    row_Step: TcxEditorRow;
    mem_Items: TdxMemData;
    DataSource1: TDataSource;
    mem_ItemsX: TFloatField;
    mem_ItemsY: TFloatField;
    mem_ItemsY2: TFloatField;
    PageControl_Graph: TPageControl;
    ts_Main: TTabSheet;
    DBChart1: TDBChart;
    Series1: TLineSeries;
    TabSheet6: TTabSheet;
    DBGrid3: TDBGrid;
    ts_Func: TTabSheet;
    cx_Functions: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    cx_FunctionsDBColumn1: TcxGridDBColumn;
    cx_Functionsgroup_name: TcxGridDBColumn;
    cx_Functionsid: TcxGridDBColumn;
    cx_Functionsgroup_id: TcxGridDBColumn;
    cx_Functionscode: TcxGridDBColumn;
    cx_Functionsname: TcxGridDBColumn;
    cx_Functionsvalue: TcxGridDBColumn;
    cx_Functionstemp: TcxGridDBColumn;
    TabSheet3: TTabSheet;
    cxGrid2: TcxGrid;
    cx_Arguments: TcxGridDBTableView;
    cxGridLevel1: TcxGridLevel;
    cx_Argumentsid: TcxGridDBColumn;
    cx_Argumentsgroup_id: TcxGridDBColumn;
    cx_Argumentscode: TcxGridDBColumn;
    cx_Argumentscategory: TcxGridDBColumn;
    cx_Argumentsname: TcxGridDBColumn;
    dxBarManager1: TdxBarManager;
    dxBarButton1: TdxBarButton;
    Bevel1: TBevel;
    cxVerticalGrid1CategoryRow2: TcxCategoryRow;
    DBChart_bottom: TDBChart;
    Series2: TLineSeries;
    Splitter1: TSplitter;
    procedure act_StartExecute(Sender: TObject);

    procedure FormDestroy(Sender: TObject);
    procedure FormCreate(Sender: TObject);

    procedure row_ArgumentEditPropertiesEditValueChanged(Sender: TObject);
    procedure row_MinEditPropertiesEditValueChanged(Sender: TObject);
  private
    FIsLoaded: Boolean;
    FLinkID : Integer;
    FRegPath: string;

    FrrlCalcParam: TrrlCalcParam;


    FParams: TIni_Link_Graph_Params;


    FLink: TDBLink;


    procedure UpdateChart;
    function Execute: boolean;
    procedure LoadChartFromFile(aFileName: string);
    
    function Run_rpls_rrl(aFile_XML: string): Boolean;

  public

    class procedure ExecDlg(aLinkID: Integer; aParams: TIni_Link_Graph_Params);

  end;

{var
  frm_Main_link_graph: Tfrm_Main_link_graph;
}

implementation

{$R *.dfm}

const
  REGISTRY_COMMON_FORMS  = 'Software\Onega\LinkGraph\Forms\';



//------------------------------------------------------
class procedure Tfrm_Main_link_graph.ExecDlg(aLinkID: Integer; aParams: TIni_Link_Graph_Params);
//------------------------------------------------------
begin
  with Tfrm_Main_link_graph.Create(Application) do
  begin
    FLinkID:=aLinkID;

    FParams :=aParams;

    ShowModal;

    Free;
  end;
end;


//------------------------------------------------------
procedure Tfrm_Main_link_graph.FormCreate(Sender: TObject);
//------------------------------------------------------
var
  sMdbFileName: string;
begin
  Caption :='����������� ����������� ������� �� �������� ������ | '+
            GetAppVersionStr();


  FRegPath:= REGISTRY_COMMON_FORMS + ClassName +'\';

  cx_VerticalGrid_LoadFromReg(cxVerticalGrid1, FRegPath + cxVerticalGrid1.Name);

  cx_Arguments.RestoreFromRegistry(FRegPath + cx_Arguments.Name);
  cx_Functions.RestoreFromRegistry(FRegPath + cx_Functions.Name);


  Panel1.Align:=alClient;

  PageControl1.ActivePageIndex:=0;
  PageControl_Graph.Align:=alClient;
  PageControl_Graph.ActivePageIndex:=0;

  //------------------------------------------------------
  DBChart1.BottomAxis.Automatic:=False;
  DBChart_bottom.BottomAxis.Automatic:=False;

  DBChart1.BottomAxis.Title.Caption:='';
  DBChart_bottom.BottomAxis.Title.Caption:='';

  Series1.Title:='';
  Series2.Title:='';
  DBChart1.BottomAxis.Increment:=0;
  DBChart1.BottomAxis.Minimum:=0;
  DBChart1.BottomAxis.Maximum:=0;


  Series1.Clear;
  Series2.Clear;

  DBChart1.Align:=alClient;

  //------------------------------------------------------

 // cxVerticalGrid1.Align:=alClient;
 // Chart1.BottomAxis.Automatic:=False;

  cx_InitVerticalGrid (cxVerticalGrid1);


  CursorHourGlass;

  sMdbFileName :=GetApplicationDir() + 'LinkGraph.mdb';

  {$IFDEF CompactDatabase}
//  mdb_CompactDatabase(sMdbFileName);
  {$ENDIF}

  mdb_OpenConnection (ADOConnection_MDB,   sMdbFileName);
  Assert (ADOConnection_MDB.Mode = cmRead);

//    GetApplicationDir() + 'LinkGraph\LinkGraph.mdb');

  tb_Functions.Open;
  tb_Arguments.Open;

  CursorDefault;

  UpdateChart();

  FrrlCalcParam := TrrlCalcParam.Create();


  TdmLink_calc.Init;
end;


procedure Tfrm_Main_link_graph.FormDestroy(Sender: TObject);
begin
  FreeAndNil(dmLink_calc);

  FreeAndNil(FrrlCalcParam);
  cx_VerticalGrid_SaveToReg(cxVerticalGrid1, FRegPath + cxVerticalGrid1.Name);

  cx_Arguments.StoreToRegistry(FRegPath + cx_Arguments.Name);
  cx_Functions.StoreToRegistry(FRegPath + cx_Functions.Name);

end;


procedure Tfrm_Main_link_graph.act_StartExecute(Sender: TObject);
begin
  Execute;
end;

procedure Tfrm_Main_link_graph.LoadChartFromFile(aFileName: string);
var
  I: Integer;
  oStrList: TStringList;
  eArr: TDoubleArray;
  x: double;
  y1: double;
  y2: double;
begin
  assert(FileExists(aFileName));

  oStrList := TStringList.Create();
  oStrList.LoadFromFile(aFileName);

  db_Clear(mem_Items);

  Series1.Clear;
  Series2.Clear;

  Series1.Visible:=FrrlCalcParam.Link_Graph.NFunc1>0;
  Series2.Visible:=FrrlCalcParam.Link_Graph.NFunc2>0;


  for I := 0 to oStrList.Count - 1 do
  begin

    eArr:=StringToDoubleArray(oStrList[i], ' ');

    x:=eArr[0];
    y1:=eArr[1];
    y2:=eArr[2];


    db_AddRecord(mem_Items, [db_Par('x',  x),
                             db_Par('y1', y1),
                             db_Par('y2', y2)]);

    Series1.AddXY(x, y1);
    Series2.AddXY(x, y2);

//    if FrrlCalcParam.DLT.NFunc1>0 then   Series1.AddXY(x, y1);
 //   if FrrlCalcParam.DLT.NFunc2>0 then   Series2.AddXY(x, y2);

  end;


  oStrList.Free;


end;



//--------------------------------------------------------------------
function Tfrm_Main_link_graph.Execute: boolean;
//--------------------------------------------------------------------
var
  b: Boolean;
  I: integer;
  sStatus: string;
  iStatus: integer;
  sCategory: string;

  sFile_in: string;
  sFile_result: string;
  sFile_out_graph: string;
  sErr: string;
  sFile_layout: string;

  rec: TGet_Equivalent_Length_KM_rec;



//  sTempDir : string;
begin
  i := AsINteger(row_Argument.Properties.Value);

  if AsINteger(row_Argument.Properties.Value)=0 then
  begin
    ShowMessage('�������� �� �����');
    Exit;
  end;

  if AsINteger(row_Func1.Properties.Value)=0 then
  begin
    ShowMessage('����� 1-� ������� �� �����');
    Exit;
  end;

  FrrlCalcParam.Link_Graph.Arg_max :=row_Max.Properties.Value;
  FrrlCalcParam.Link_Graph.Arg_step:=row_Step.Properties.Value;



  if not FIsLoaded then
  begin
     dmLink_calc.LinkList.Clear;

     FLink:=dmLink_calc.LinkList.AddItem;

//    TdmLink_calc.Init;
  //  dmLink_calc.Params.IsUsePassiveElements := FParams.UsePassiveElements;

    dmLink_calc.OpenData_new(FLinkID, FLink);

//    if not dmLink_calc.OpenData(FLinkID)  then
//      Exit;

  //  dmLink_calc.SaveToClass11(FrrlCalcParam);


    TDBLink_export.SaveToClass(FLink, FLink.LinkEnd1, FLink.LinkEnd2,
      FrrlCalcParam, FLink.RelProfile, FLink.IsProfileExists);



//     FreeAndNil(dmLink_calc);

    FIsLoaded := True;
  end;




  FillChar (rec, SizeOf(rec), 0);

  rec.Length_Km     := FLink.GetLength_km;
  rec.GST_length_km := FLink.Trb.GST_LENGTH_km_;
  rec.GST_Type      := FLink.Trb.GST_TYPE;

//  rec.Length_Km     :=ADOStoredProc_Link.FieldByName(FLD_LENGTH).AsFloat/1000;
//  rec.GST_length_km :=ADOStoredProc_Link.FieldByName(FLD_GST_LENGTH).AsFloat;
//  rec.GST_Type      :=ADOStoredProc_Link.FieldByName(FLD_GST_TYPE).AsInteger;


  FLink.Trb.GST_LENGTH_km_  :=  dmLink_Calc_SESR.Get_Equivalent_Length_KM_rec(rec);

  FrrlCalcParam.TRB.GST_Length_KM_3_:= FLink.Trb.GST_LENGTH_km_;



  if tb_Arguments.Locate(FLD_ID, row_Argument.Properties.Value, []) then
  begin
    sCategory := LowerCase(tb_Arguments.FieldByName(FLD_category).AsString);

    FrrlCalcParam.Link_Graph.ArgGroup:= IIF(sCategory='ttx', 1, 2);

    FrrlCalcParam.Link_Graph.NArg:=tb_Arguments.FieldByName(FLD_CODE).AsInteger;
  end else
    FrrlCalcParam.Link_Graph.NArg:=0;


  if tb_Functions.Locate(FLD_ID, row_Func1.Properties.Value ,[]) then
    FrrlCalcParam.Link_Graph.NFunc1:=tb_Functions.FieldByName(FLD_CODE).AsInteger
  else
    FrrlCalcParam.Link_Graph.NFunc1:=0;


  if tb_Functions.Locate(FLD_ID, row_Func2.Properties.Value, []) then
    FrrlCalcParam.Link_Graph.NFunc2:=tb_Functions.FieldByName(FLD_CODE).AsInteger
  else
    FrrlCalcParam.Link_Graph.NFunc2:=0;


  FrrlCalcParam.Link_Graph.Arg_min:=row_Min.Properties.Value;
  FrrlCalcParam.Link_Graph.Arg_max:=row_Max.Properties.Value;
  FrrlCalcParam.Link_Graph.Arg_step:=row_Step.Properties.Value;


  sFile_in       :=g_ApplicationDataDir + 'rrl.xml';
  sFile_result   :=g_ApplicationDataDir + 'rrl_result.xml';
  sFile_out_graph:=g_ApplicationDataDir + 'rrl_graph.xml';
  sFile_layout   :=g_ApplicationDataDir + 'rrl_layout.xml';


  if FrrlCalcParam.ValidateDlg then
{  begin
    ShowMessage(FrrlCalcParam.ErrorMsg);
    exit;
  end else
}

  b:=FLink.IsProfileExists;

//    FrrlCalcParam.SaveToXML (sFile_in, oLink.FIsProfileExists1);



  CalcMethod_LoadFromXMLFile(
      '',
      FLink.Calc_method,
//      dmLink_calc.Link.Calc_method,
      sFile_layout
      );


// .. sFile_in       :=g_ApplicationDataDir + 'rrl.xml';
  FrrlCalcParam.Result_FileName :=g_ApplicationDataDir + 'rrl_result.xml';
  FrrlCalcParam.Graph_FileName  :=g_ApplicationDataDir + 'rrl_graph.xml';
  FrrlCalcParam.Layout_FileName :=g_ApplicationDataDir + 'rrl_layout.xml';


  FrrlCalcParam.SaveToXML (sFile_in, True);


  Run_rpls_rrl(sFile_in); //, sFile_result, sFile_out_graph, sFile_layout);


(*
  RunApp (GetApplicationDir() + EXE_RPLS_RRL,
     DoubleQuotedStr(sFile_in) +' '+
     DoubleQuotedStr(sFile_result)+' '+
     DoubleQuotedStr(sFile_out_graph));

*)
  assert(FileExists(sFile_out_graph));


  LoadChartFromFile (sFile_out_graph);


end;


// ---------------------------------------------------------------
function Tfrm_Main_link_graph.Run_rpls_rrl(aFile_XML: string): Boolean;
// ---------------------------------------------------------------
var
  sRunPath: string;

//  oIni_rpls_rrl_params: TIni_rpls_rrl_params;
//  sFile: string;

begin
  Assert(aFile_XML<>'');
//  Assert(aFile_result<>'');
//  Assert(aFile_layout<>'');

//  oIni_rpls_rrl_params:=TIni_rpls_rrl_params.Create;


  sRunPath := GetApplicationDir() + EXE_RPLS_RRL;

  Result := True;

  {$DEFINE use_dll111}

  {$IFDEF use_dll}
   {
    if Load_Irpls_rrl() then
    begin
    //  Irpls_rrl.InitADO(dmMain.ADOConnection1.ConnectionObject);
   //   ILink_graph.InitAppHandle(Application.Handle);
      Irpls_rrl.Execute(sFile);

      UnLoad_Irpls_rrl();
    end;
    }
  //  Exit;

  {$ELSE}

//    oIni_rpls_rrl_params.In_FileName := aFile_XML;

//    oIni_rpls_rrl_params.out_FileName := aFile_result;
//    oIni_rpls_rrl_params.Graph_FileName  := aFile_out_graph;
//    oIni_rpls_rrl_params.OUTREZ_FileName := aFile_layout;


//    sFile :=g_ApplicationDataDir + 'rrl.ini';

//    oIni_rpls_rrl_params.SaveToFile(sFile);

    Result := RunAppEx (sRunPath,  [aFile_XML]);

 //   Result := RunAppEx (sRunPath,  [aFile_XML, aFile_result, '', aFile_layout]);

//                      DoubleQuotedStr(aFile_XML) +' '+
  //                    DoubleQuotedStr(aFile_result)) ;

//    Result := RunApp (sRunPath,
//                      DoubleQuotedStr(aFile_XML) +' '+
//                      DoubleQuotedStr(aFile_result)) ;


  //  if RunApp (GetApplicationDir() + EXE_RPLS_RRL, sFile1 +' '+ aFile_result) then


  {$ENDIF}

//  FreeAndNil(oIni_rpls_rrl_params);

end;




//--------------------------------------------------------------
procedure Tfrm_Main_link_graph.UpdateChart;
//--------------------------------------------------------------
var
  sArgument: string;
begin

  with row_Argument do
    sArgument:=TcxLookupComboBoxProperties(Properties.EditProperties).GetDisplayText(Properties.Value);

  DBChart1.BottomAxis.Title.Caption:=sArgument;
  DBChart_bottom.BottomAxis.Title.Caption:=sArgument;


  with row_Func1 do
    Series1.Title:=TcxLookupComboBoxProperties(Properties.EditProperties).GetDisplayText(Properties.Value);

  with row_Func2 do
    Series2.Title:=TcxLookupComboBoxProperties(Properties.EditProperties).GetDisplayText(Properties.Value);

  if Series1.Title='' then Series1.Title:='�������� 1';
  if Series2.Title='' then Series2.Title:='�������� 2';


  DBChart1.BottomAxis.Increment      :=row_Step.Properties.Value;
  DBChart_bottom.BottomAxis.Increment:=row_Step.Properties.Value;


  DBChart1.BottomAxis.Minimum:=-100000;
  DBChart1.BottomAxis.Maximum:=100000;
  DBChart1.BottomAxis.Minimum:=row_Min.Properties.Value;
  DBChart1.BottomAxis.Maximum:=row_Max.Properties.Value;

  DBChart_bottom.BottomAxis.Minimum:=-100000;
  DBChart_bottom.BottomAxis.Maximum:=100000;
  DBChart_bottom.BottomAxis.Minimum:=row_Min.Properties.Value;
  DBChart_bottom.BottomAxis.Maximum:=row_Max.Properties.Value;

end;


procedure Tfrm_Main_link_graph.row_ArgumentEditPropertiesEditValueChanged(
  Sender: TObject);
begin

  if Sender is TcxTextEdit then
    TcxTextEdit(Sender).PostEditValue
  else
    cxVerticalGrid1.HideEdit;

  UpdateChart;
end;


procedure Tfrm_Main_link_graph.row_MinEditPropertiesEditValueChanged(
  Sender: TObject);
begin
  if Sender is TcxSpinEdit then
    TcxSpinEdit(Sender).PostEditValue;

//  ShowMessage(Sender.className);
 // cxVerticalGrid1.HideEdit;

  if cxVerticalGrid1.FocusedRow = row_Min then
  begin
    DBChart1.BottomAxis.Minimum:=row_Min.Properties.Value;
    DBChart_bottom.BottomAxis.Minimum:=row_Min.Properties.Value;
  end;

  if cxVerticalGrid1.FocusedRow = row_Max then
  begin
    DBChart1.BottomAxis.Maximum:=row_Max.Properties.Value;
    DBChart_bottom.BottomAxis.Maximum:=row_Max.Properties.Value;
  end;


  if cxVerticalGrid1.FocusedRow = row_Step then
  begin
    DBChart1.BottomAxis.Increment      :=row_Step.Properties.Value;
    DBChart_bottom.BottomAxis.Increment:=row_Step.Properties.Value;
  end;


//  DBChart1.BottomAxis.Minimum:=row_Min.Properties.Value;
 // DBChart1.BottomAxis.Maximum:=row_Max.Properties.Value;

end;

end.


