object frm_Main_link_graph: Tfrm_Main_link_graph
  Left = 1141
  Top = 243
  Width = 857
  Height = 690
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = 'frm_Main_link_graph'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 136
    Width = 841
    Height = 448
    Align = alTop
    Anchors = [akLeft, akTop, akRight, akBottom]
    BevelOuter = bvNone
    BorderWidth = 5
    TabOrder = 0
    object PageControl1: TPageControl
      Left = 5
      Top = 5
      Width = 831
      Height = 438
      ActivePage = TabSheet1
      Align = alClient
      TabOrder = 0
      object TabSheet1: TTabSheet
        HelpType = htKeyword
        HelpKeyword = 'v'
        Caption = #1055#1072#1088#1072#1084#1077#1090#1088#1099
        object Bevel1: TBevel
          Left = 0
          Top = 161
          Width = 823
          Height = 9
          Align = alTop
          Shape = bsTopLine
        end
        object cxVerticalGrid1: TcxVerticalGrid
          Left = 0
          Top = 0
          Width = 823
          Height = 161
          Align = alTop
          LookAndFeel.Kind = lfFlat
          OptionsView.PaintStyle = psDelphi
          OptionsView.RowHeaderMinWidth = 50
          OptionsView.RowHeaderWidth = 208
          TabOrder = 0
          Version = 1
          object row_Argument: TcxEditorRow
            Properties.Caption = #1040#1088#1075#1091#1084#1077#1085#1090
            Properties.EditPropertiesClassName = 'TcxLookupComboBoxProperties'
            Properties.EditProperties.DropDownListStyle = lsFixedList
            Properties.EditProperties.DropDownRows = 40
            Properties.EditProperties.KeyFieldNames = 'id'
            Properties.EditProperties.ListColumns = <
              item
                FieldName = 'name'
              end
              item
                FieldName = 'id'
              end
              item
                FieldName = 'code'
              end
              item
                FieldName = 'category'
              end>
            Properties.EditProperties.ListSource = ds_Arguments
            Properties.EditProperties.OnEditValueChanged = row_ArgumentEditPropertiesEditValueChanged
            Properties.DataBinding.ValueType = 'String'
            Properties.Value = Null
            ID = 0
            ParentID = -1
            Index = 0
            Version = 1
          end
          object row_Func1: TcxEditorRow
            Properties.Caption = #1053#1086#1084#1077#1088' 1-'#1081' '#1092#1091#1085#1082#1094#1080#1080
            Properties.EditPropertiesClassName = 'TcxLookupComboBoxProperties'
            Properties.EditProperties.DropDownListStyle = lsFixedList
            Properties.EditProperties.DropDownRows = 40
            Properties.EditProperties.KeyFieldNames = 'id'
            Properties.EditProperties.ListColumns = <
              item
                FieldName = 'name'
              end
              item
                FieldName = 'code'
              end
              item
                FieldName = 'group_name'
              end>
            Properties.EditProperties.ListSource = ds_Functions
            Properties.EditProperties.OnEditValueChanged = row_ArgumentEditPropertiesEditValueChanged
            Properties.DataBinding.ValueType = 'String'
            Properties.Value = Null
            ID = 1
            ParentID = -1
            Index = 1
            Version = 1
          end
          object row_Func2: TcxEditorRow
            Properties.Caption = #1053#1086#1084#1077#1088' 2-'#1081' '#1092#1091#1085#1082#1094#1080#1080
            Properties.EditPropertiesClassName = 'TcxLookupComboBoxProperties'
            Properties.EditProperties.DropDownListStyle = lsFixedList
            Properties.EditProperties.DropDownRows = 40
            Properties.EditProperties.KeyFieldNames = 'id'
            Properties.EditProperties.ListColumns = <
              item
                FieldName = 'name'
              end
              item
                FieldName = 'code'
              end
              item
                FieldName = 'group_name'
              end>
            Properties.EditProperties.ListSource = ds_Functions
            Properties.EditProperties.OnEditValueChanged = row_ArgumentEditPropertiesEditValueChanged
            Properties.DataBinding.ValueType = 'String'
            Properties.Value = Null
            ID = 2
            ParentID = -1
            Index = 2
            Version = 1
          end
          object cxVerticalGrid1CategoryRow1: TcxCategoryRow
            ID = 3
            ParentID = -1
            Index = 3
            Version = 1
          end
          object row_Min: TcxEditorRow
            Properties.Caption = 'Min '#1079#1085#1072#1095#1077#1085#1080#1077' '#1072#1088#1075#1091#1084#1077#1085#1090#1072
            Properties.EditPropertiesClassName = 'TcxSpinEditProperties'
            Properties.EditProperties.OnEditValueChanged = row_MinEditPropertiesEditValueChanged
            Properties.DataBinding.ValueType = 'Float'
            Properties.Value = 3.000000000000000000
            ID = 4
            ParentID = -1
            Index = 4
            Version = 1
          end
          object row_Max: TcxEditorRow
            Properties.Caption = 'Max '#1079#1085#1072#1095#1077#1085#1080#1077' '#1072#1088#1075#1091#1084#1077#1085#1090#1072
            Properties.EditPropertiesClassName = 'TcxSpinEditProperties'
            Properties.EditProperties.OnEditValueChanged = row_MinEditPropertiesEditValueChanged
            Properties.DataBinding.ValueType = 'Float'
            Properties.Value = 34.000000000000000000
            ID = 5
            ParentID = -1
            Index = 5
            Version = 1
          end
          object cxVerticalGrid1CategoryRow2: TcxCategoryRow
            ID = 6
            ParentID = -1
            Index = 6
            Version = 1
          end
          object row_Step: TcxEditorRow
            Properties.Caption = #1064#1072#1075' '#1087#1077#1088#1077#1073#1086#1088#1072' '#1079#1085#1072#1095#1077#1085#1080#1081' '#1072#1088#1075#1091#1084#1077#1085#1090#1072
            Properties.Hint = #1064#1072#1075' '#1087#1077#1088#1077#1073#1086#1088#1072' '#1079#1085#1072#1095#1077#1085#1080#1081' '#1072#1088#1075#1091#1084#1077#1085#1090#1072
            Properties.EditPropertiesClassName = 'TcxTextEditProperties'
            Properties.EditProperties.OnEditValueChanged = row_ArgumentEditPropertiesEditValueChanged
            Properties.DataBinding.ValueType = 'String'
            Properties.Value = '1'
            ID = 7
            ParentID = -1
            Index = 7
            Version = 1
          end
        end
        object PageControl_Graph: TPageControl
          Left = 0
          Top = 170
          Width = 823
          Height = 240
          ActivePage = ts_Main
          Align = alClient
          MultiLine = True
          Style = tsFlatButtons
          TabOrder = 1
          object ts_Main: TTabSheet
            Caption = #1043#1088#1072#1092#1080#1082
            object Splitter1: TSplitter
              Left = 0
              Top = 117
              Width = 815
              Height = 3
              Cursor = crVSplit
              Align = alBottom
              Beveled = True
            end
            object DBChart1: TDBChart
              Left = 0
              Top = 0
              Width = 815
              Height = 105
              Title.Bevel = bvLowered
              Title.Shadow.Color = clLime
              Title.Shadow.Transparency = 6
              Title.Text.Strings = (
                'TDBChart')
              Title.Visible = False
              LeftAxis.AxisValuesFormat = '#,##0.######'
              Legend.Alignment = laTop
              Legend.LegendStyle = lsSeries
              RightAxis.Grid.Color = clGreen
              RightAxis.Grid.SmallDots = True
              View3D = False
              Align = alTop
              BevelOuter = bvLowered
              TabOrder = 0
              PrintMargins = (
                15
                40
                15
                40)
              object Series1: TLineSeries
                Marks.Arrow.Visible = True
                Marks.Callout.Brush.Color = clBlack
                Marks.Callout.Arrow.Visible = True
                Marks.Visible = False
                Title = #1055#1088#1080#1079#1085#1072#1082' '#1087#1088#1080#1075#1086#1076#1085#1086#1089#1090#1080' '#1087#1086' '#1074#1089#1077#1084' '#1082#1088#1080#1090#1077#1088#1080#1103#1084','
                Pointer.InflateMargins = True
                Pointer.Style = psCross
                Pointer.Visible = True
                XValues.Name = 'X'
                XValues.Order = loAscending
                YValues.Name = 'Y'
                YValues.Order = loNone
              end
            end
            object DBChart_bottom: TDBChart
              Left = 0
              Top = 120
              Width = 815
              Height = 89
              Title.Bevel = bvLowered
              Title.Shadow.Color = clLime
              Title.Shadow.Transparency = 6
              Title.Text.Strings = (
                'TDBChart')
              Title.Visible = False
              BottomAxis.Title.Caption = 'dffffffffffffffffffffffffffffddddddfgsdfg'
              LeftAxis.AxisValuesFormat = '#,##0.######'
              Legend.Alignment = laTop
              Legend.LegendStyle = lsSeries
              RightAxis.Grid.Color = clGreen
              RightAxis.Grid.SmallDots = True
              View3D = False
              Align = alBottom
              BevelOuter = bvLowered
              TabOrder = 1
              object Series2: TLineSeries
                Marks.Arrow.Visible = True
                Marks.Callout.Brush.Color = clBlack
                Marks.Callout.Arrow.Visible = True
                Marks.Visible = False
                Pointer.InflateMargins = True
                Pointer.Style = psCross
                Pointer.Visible = True
                XValues.Name = 'X'
                XValues.Order = loAscending
                YValues.Name = 'Y'
                YValues.Order = loNone
                Data = {
                  0019000000000000000000204000000000000056400000000000C05840000000
                  0000288440000000000058864000000000005084400000000000208740000000
                  0000805340000000000060884000000000000000000000000000E08A40000000
                  0000988C400000000000004C400000000000004C400000000000004C40000000
                  00008050400000000000288E400000000000208C400000000000002240000000
                  00009085400000000000BC864000000000002889400000000000A08940000000
                  00002C8A400000000000E88740}
              end
            end
          end
          object TabSheet6: TTabSheet
            Caption = #1058#1072#1073#1083#1080#1094#1072
            ImageIndex = 1
            object DBGrid3: TDBGrid
              Left = 0
              Top = 0
              Width = 394
              Height = 241
              Align = alLeft
              DataSource = DataSource1
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
            end
          end
        end
      end
      object TabSheet3: TTabSheet
        Caption = #1040#1088#1075#1091#1084#1077#1085#1090#1099
        ImageIndex = 4
        object cxGrid2: TcxGrid
          Left = 0
          Top = 0
          Width = 831
          Height = 410
          Align = alClient
          TabOrder = 0
          LookAndFeel.Kind = lfFlat
          object cx_Arguments: TcxGridDBTableView
            NavigatorButtons.ConfirmDelete = False
            DataController.DataSource = ds_Arguments
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <>
            DataController.Summary.SummaryGroups = <>
            OptionsCustomize.ColumnFiltering = False
            object cx_Argumentsid: TcxGridDBColumn
              DataBinding.FieldName = 'id'
              Visible = False
            end
            object cx_Argumentsgroup_id: TcxGridDBColumn
              DataBinding.FieldName = 'group_id'
              Visible = False
            end
            object cx_Argumentscategory: TcxGridDBColumn
              Caption = #1082#1072#1090#1077#1075#1086#1088#1080#1103
              DataBinding.FieldName = 'category'
              Visible = False
              GroupIndex = 0
              Width = 69
            end
            object cx_Argumentscode: TcxGridDBColumn
              Caption = #1082#1086#1076
              DataBinding.FieldName = 'code'
              Width = 51
            end
            object cx_Argumentsname: TcxGridDBColumn
              Caption = #1053#1072#1079#1074#1072#1085#1080#1077
              DataBinding.FieldName = 'name'
              Width = 396
            end
          end
          object cxGridLevel1: TcxGridLevel
            GridView = cx_Arguments
          end
        end
      end
      object ts_Func: TTabSheet
        Caption = #1060#1091#1085#1082#1094#1080#1080
        ImageIndex = 3
        object cxGrid1: TcxGrid
          Left = 0
          Top = 0
          Width = 831
          Height = 410
          Align = alClient
          TabOrder = 0
          LookAndFeel.Kind = lfFlat
          object cx_Functions: TcxGridDBTableView
            NavigatorButtons.ConfirmDelete = False
            DataController.DataSource = ds_Functions
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <>
            DataController.Summary.SummaryGroups = <>
            OptionsCustomize.ColumnFiltering = False
            object cx_FunctionsDBColumn1: TcxGridDBColumn
              Visible = False
            end
            object cx_Functionsid: TcxGridDBColumn
              DataBinding.FieldName = 'id'
              Visible = False
            end
            object cx_Functionsgroup_id: TcxGridDBColumn
              DataBinding.FieldName = 'group_id'
              Visible = False
            end
            object cx_Functionsgroup_name: TcxGridDBColumn
              Caption = #1050#1072#1090#1077#1075#1086#1088#1080#1103
              DataBinding.FieldName = 'group_name'
              Width = 257
            end
            object cx_Functionscode: TcxGridDBColumn
              Caption = #1050#1086#1076
              DataBinding.FieldName = 'code'
            end
            object cx_Functionsname: TcxGridDBColumn
              Caption = #1053#1072#1079#1074#1072#1085#1080#1077
              DataBinding.FieldName = 'name'
              Width = 366
            end
            object cx_Functionsvalue: TcxGridDBColumn
              DataBinding.FieldName = 'value'
              Visible = False
            end
            object cx_Functionstemp: TcxGridDBColumn
              DataBinding.FieldName = 'temp'
              Visible = False
            end
          end
          object cxGrid1Level1: TcxGridLevel
            GridView = cx_Functions
          end
        end
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 0
    Width = 841
    Height = 108
    Align = alTop
    Caption = 'Panel2'
    Color = clGray
    TabOrder = 1
    Visible = False
  end
  object ADOConnection_MDB: TADOConnection
    ConnectionString = 
      'Provider=Microsoft.Jet.OLEDB.4.0;User ID=Admin;Data Source=C:\ON' +
      'EGA\RPLS_DB\bin\LinkGraph\LinkGraph.mdb;Mode=Read;Extended Prope' +
      'rties="";Persist Security Info=False;Jet OLEDB:System database="' +
      '";Jet OLEDB:Registry Path="";Jet OLEDB:Database Password="";Jet ' +
      'OLEDB:Engine Type=5;Jet OLEDB:Database Locking Mode=0;Jet OLEDB:' +
      'Global Partial Bulk Ops=2;Jet OLEDB:Global Bulk Transactions=1;J' +
      'et OLEDB:New Database Password="";Jet OLEDB:Create System Databa' +
      'se=False;Jet OLEDB:Encrypt Database=False;Jet OLEDB:Don'#39't Copy L' +
      'ocale on Compact=False;Jet OLEDB:Compact Without Replica Repair=' +
      'False;Jet OLEDB:SFP=False'
    LoginPrompt = False
    Mode = cmRead
    Provider = 'Microsoft.Jet.OLEDB.4.0'
    Left = 336
    Top = 5
  end
  object ds_Functions: TDataSource
    DataSet = tb_Functions
    Left = 159
    Top = 54
  end
  object tb_Functions: TADOTable
    Connection = ADOConnection_MDB
    CursorType = ctStatic
    Filter = 'checked=true'
    Filtered = True
    TableName = 'view_Functions'
    Left = 159
    Top = 6
  end
  object tb_Arguments: TADOTable
    Connection = ADOConnection_MDB
    CursorType = ctStatic
    Filter = 'checked=true'
    Filtered = True
    TableName = 'Arguments'
    Left = 231
    Top = 6
  end
  object ds_Arguments: TDataSource
    DataSet = tb_Arguments
    Left = 231
    Top = 54
  end
  object FormStorage1: TFormStorage
    IniFileName = 'Software\Onega\'
    StoredProps.Strings = (
      'DBChart_bottom.Height')
    StoredValues = <>
    Left = 50
    Top = 10
  end
  object cxPropertiesStore1: TcxPropertiesStore
    Active = False
    Components = <>
    StorageName = 'cxPropertiesStore1'
    StorageType = stRegistry
    Left = 19
    Top = 9
  end
  object ActionList1: TActionList
    Left = 80
    Top = 10
    object act_Start: TAction
      Caption = '&'#1042#1099#1087#1086#1083#1085#1080#1090#1100' '#1088#1072#1089#1095#1077#1090
      OnExecute = act_StartExecute
    end
  end
  object mem_Items: TdxMemData
    Active = True
    Indexes = <>
    Persistent.Data = {
      566572CDCCCCCCCCCCFC3F030000000800000006000200580008000000060003
      005931000800000006000300593200}
    SortOptions = []
    Left = 672
    Top = 9
    object mem_ItemsX: TFloatField
      DisplayWidth = 14
      FieldName = 'X'
    end
    object mem_ItemsY: TFloatField
      DisplayWidth = 16
      FieldName = 'Y1'
    end
    object mem_ItemsY2: TFloatField
      DisplayWidth = 17
      FieldName = 'Y2'
    end
  end
  object DataSource1: TDataSource
    DataSet = mem_Items
    Left = 672
    Top = 60
  end
  object dxBarManager1: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = []
    Categories.Strings = (
      'Default')
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    NotDocking = [dsNone, dsLeft, dsTop, dsRight, dsBottom]
    PopupMenuLinks = <>
    SunkenBorder = True
    UseSystemFont = True
    Left = 455
    Top = 7
    DockControlHeights = (
      0
      0
      28
      0)
    object dxBarManager1Bar1: TdxBar
      Caption = 'Custom 1'
      CaptionButtons = <>
      DockedDockingStyle = dsTop
      DockedLeft = 0
      DockedTop = 0
      DockingStyle = dsTop
      FloatLeft = 604
      FloatTop = 357
      FloatClientWidth = 23
      FloatClientHeight = 22
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarButton1'
        end>
      OldName = 'Custom 1'
      OneOnRow = True
      Row = 0
      ShowMark = False
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarButton1: TdxBarButton
      Action = act_Start
      Category = 0
    end
  end
end
