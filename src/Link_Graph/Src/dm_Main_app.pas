unit dm_Main_app;

interface



uses
  SysUtils, Classes, IniFiles, Dialogs,Forms,

  i_link_graph,


  u_Ini_Link_Graph_Params,

  u_vars,

 // dm_Link_Calc,
  f_Main_link_graph,

  dm_Main

  ;

type
  TdmMain_app = class(TDataModule)

    procedure DataModuleCreate(Sender: TObject);
    { Private declarations }
  public
    { Public declarations }
  end;

var
  dmMain_app: TdmMain_app;

implementation
{$R *.dfm}


//--------------------------------------------------------------
procedure TdmMain_app.DataModuleCreate(Sender: TObject);
//--------------------------------------------------------------
const
  TEMP_INI_FILE = 'link_calc.ini';

var

  sIniFileName: string;   
  oIniFile: TIniFile;

  oParams: TIni_Link_Graph_Params;

begin
  sIniFileName:=ParamStr(1);
  if sIniFileName='' then
  //  sIniFileName:=TEMP_INI_FILE;
    sIniFileName:=g_ApplicationDataDir+  TEMP_INI_FILE;


  if not FileExists(sIniFileName) then
  begin
    ShowMessage(sIniFileName);
    Exit;
  end;



  TdmMain.Init;
  if not dmMain.OpenDB_reg then
    Exit;

 {$DEFINE use_dll111}

 {$IFDEF use_dll}
  if Load_ILink_Graph() then
  begin
    ILink_Graph.InitADO(dmMain.ADOConnection1.ConnectionObject);
    ILink_graph.InitAppHandle(Application.Handle);
    ILink_Graph.Execute(sFile);

    UnLoad_ILink_Graph();
  end;

  Exit;
  {$ENDIF}


  oParams:=TIni_Link_Graph_Params.Create;
  oParams.LoadFromFile(sIniFileName);
  oParams.Validate;                                 


  dmMain.ProjectID:=oParams.ProjectID;

  Tfrm_Main_link_graph.ExecDlg (oParams.LinkID, oParams);


  FreeAndNil(oParams);
        
end;


end.




//
//  oIniFile := TIniFile.Create(sIniFileName);
//
//  with oIniFile do
//  begin
//    iProjectID                :=ReadInteger ('main', 'ProjectID',  0);
//    iLinkID                   :=ReadInteger ('main', 'LinkID',  0);
//   // bSaveReportToDB           :=ReadBool    ('main', 'IsSaveReportToDB',  True);
//  //  bUsePassiveElements       :=ReadBool    ('main', 'UsePassiveElements',  false);
//  //  bIsCalcWithAdditionalRain :=ReadBool    ('main', 'IsCalcWithAdditionalRain',  false);
//
//   // Free;
//  end;
//
//  FreeAndNil(oIniFile);


{
  iProjectID :=1;
  iLinkID    :=1;
}

 (* if (iLinkID=0) or
     (iProjectID=0)
  then begin
    ShowMessage('(iLinkID=0) or (iProjectID=0) ');
    Exit;
  end;
*)

 // Application.CreateForm(Tfrm_Main, frm_Main);


{  dmMain.ProjectID:=iProjectID;
  dmLink_Calc.IsUsePassiveElements:=bUsePassiveElements;
  dmLink_Calc.IsCalcWithAdditionalRain:=bIsCalcWithAdditionalRain;
  dmLink_Calc.Params.IsSaveReportToDB:=bSaveReportToDB;

  dmLink_Calc.Execute (iLinkID);
}

