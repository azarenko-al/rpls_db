unit dm_Objects;

interface

uses
  SysUtils, Classes, DB, ADODB, Forms, dxmdaset, Dialogs, Variants,
  dm_Custom,

  u_const_db,

  u_db,
  u_types

  ;



type
  TdmObjects1 = class(TdmCustom)
    qry_Objects_: TADOQuery;
    qry_ObjectFields_: TADOQuery;
    mem_Objects: TdxMemData;
    mem_Objects_Fields: TdxMemData;
    qry_Temp: TADOQuery;
    procedure DataModuleCreate(Sender: TObject);
  private
    function  ObjectIDByType (aObjType: TrpObjectType): integer;
//    function  FieldTypeByFieldCaption (ot: TrpObjectType; aFieldCaption: string): string;
//    function  VerifyStrLstItemByFieldName (ot: TrpObjectType; aFieldName: string; aItem: string): boolean;
    function  LstIndexByFieldName (ot: TrpObjectType; aFieldName, aFieldValue: string): integer;

  public
    function  ItemsByFieldName (ot: TrpObjectType; aFieldName: string; oItems: TStrings): boolean;
    function  FieldCaptionByFieldName(aObjID: integer; aFieldName: string): string;
    function  ObjectIDByName (aObjectName: string): integer;

    function FieldTypeByFieldName(aObjID: integer; aFieldName: string): string;

    function  StrLstItemByFieldName (ot: TrpObjectType; aFieldName: string; aItemIndex: integer): string;

    procedure Open1;
  end;

function dmObjects1: TdmObjects1;


//===================================================================
implementation    {$R *.DFM}
//===================================================================

var
  FdmObjects: TdmObjects1;


// ---------------------------------------------------------------
function dmObjects1: TdmObjects1;
// ---------------------------------------------------------------
begin
  if not Assigned(FdmObjects) then
      FdmObjects := TdmObjects1.Create(Application);

  Result := FdmObjects;
end;


//-------------------------------------------------------------------
procedure TdmObjects1.DataModuleCreate (Sender: TObject);
//-------------------------------------------------------------------
{var
  oft: TFieldType;}
begin
  inherited;
// db_SetComponentsADOConn([qry_Objects_, qry_ObjectFields_], dmMain.AdoConnection);

  db_CreateField (mem_Objects,
                    [db_Field(FLD_ID,   ftInteger),
                     db_Field(FLD_NAME, ftString, 200)  ]);

  db_CreateField (mem_Objects_Fields,
                    [db_Field(FLD_ID,              ftInteger),
                     db_Field(FLD_OBJECT_ID,       ftInteger),
                     db_Field(FLD_IS_FOLDER,       ftBoolean),
                     db_Field(FLD_PARENT_ID,       ftInteger),
                     db_Field(FLD_NAME,            ftString, 50),
                     db_Field(FLD_TYPE,            ftString, 20),
                     db_Field(FLD_CAPTION,         ftString, 200),
                     db_Field(FLD_IS_READONLY,     ftBoolean),
                     db_Field(FLD_XREF_ObjectName, ftString, 50),

                     db_Field(FLD_STATUS_NAME,     ftString, 50),


                     db_Field(FLD_ENABLED,         ftBoolean),
                     db_Field(FLD_ITEMS,           ftMemo)
                     ]);

 ///// Open1;


{2	id	int	4	0
0	object_id	int	4	0
0	is_folder	bit	1	1
0	parent_id	int	4	1
0	index_	int	4	1
0	name	TName (varchar)	50	1
0	type	varchar	20	1
0	caption	varchar	200	1
0	size_	int	4	1
0	DefaultValue	varchar	200	1
0	SubQuery111	varchar	4000	1
0	is_readonly	bit	1	1
0	required	int	4	1
0	hidden	int	4	1
0	sorted	int	4	1
0	enabled	bit	1	1
0	hint	varchar	100	1
0	SubQuery_field111	varchar	50	1
0	items	text	16	1
0	values_	text	16	1
0	xref_ObjectName	varchar	50	1
0	xref_TableName	varchar	255	1
0	xref_FieldName	varchar	255	1
0	is_in_list	bit	1	1
0	is_in_view	bit	1	1
0	column_caption	varchar	255	1
0	column_index	int	4	1
0	show_button	bit	1	1
0	IsGroupAssignmentEnabled	bit	1	1
1	IsMapCriteria	bit	1	1                  }

//  qry_ObjectFields.Filtered:= false;
end;


//-------------------------------------------------------------------
procedure TdmObjects1.Open1;
//-------------------------------------------------------------------
begin

  db_OpenTableAll (qry_Objects_,      TBL_OBJECTS );
  dmOnega_DB_data.OpenQuery   (qry_ObjectFields_, 'SELECT * FROM '+ TBL_OBJECT_FIELDS +' ORDER BY object_id');

  db_Clear(mem_Objects);
  db_Clear(mem_Objects_Fields);

  db_CopyRecords(qry_Objects_,      mem_Objects);
  db_CopyRecords(qry_ObjectFields_, mem_Objects_Fields);

//  u_db_lib1.db_CopyRecords(qry_Objects_,      mem_Objects);
 // u_db_lib1.db_CopyRecords(qry_ObjectFields_, mem_Objects_Fields);
end;

//-------------------------------------------------------------------
function  TdmObjects1.ObjectIDByType (aObjType: TrpObjectType): integer;
//-------------------------------------------------------------------
begin
  if mem_Objects.Locate(FLD_NAME, g_Obj.ItemByType[aObjType].Name, [loCaseInsensitive]) then
    Result:= mem_Objects[FLD_ID]
  else

  if mem_Objects.Locate(FLD_NAME, g_Obj.ItemByType[aObjType].TableName, [loCaseInsensitive]) then
    Result:= mem_Objects[FLD_ID]

  else
    Result:= -1;
end;

//-------------------------------------------------------------------
function  TdmObjects1.ObjectIDByName (aObjectName: string): integer;
//-------------------------------------------------------------------
begin
  if g_Obj.IsObjectNameExist(aObjectName) then
    Result:= ObjectIDByType(g_Obj.ItemByName[aObjectName].Type_)
  else
    Result:= -1;
end;

//-------------------------------------------------------------------
function  TdmObjects1.ItemsByFieldName (ot: TrpObjectType; aFieldName: string; oItems: TStrings): boolean;
//-------------------------------------------------------------------
var
  iObjID: integer;
///  S: TStrArray;
begin
  Result:= true;

  try
    iObjID:= ObjectIDByType(ot);

    if mem_Objects_Fields.Locate(FLD_NAME+';'+FLD_OBJECT_ID, VarArrayOf([aFieldName, iObjID]), [loCaseInsensitive]) then
      oItems.Text:=mem_Objects_Fields.FieldByName(FLD_ITEMS).AsString;

  except
    Result:= false;
  end;
end;

//-------------------------------------------------------------------
function  TdmObjects1.StrLstItemByFieldName (ot: TrpObjectType; aFieldName: string; aItemIndex: integer): string;
//-------------------------------------------------------------------
var
  oList: TStringList;
begin
  oList:= TStringList.Create;

  Result:= '';

  ItemsByFieldName(ot, aFieldName, oList);

  if (oList.Count > 0) AND (aItemIndex < oList.Count) then
    Result:= oList[aItemIndex];

  oList.Free;
end;

//-------------------------------------------------------------------
function  TdmObjects1.LstIndexByFieldName (ot: TrpObjectType; aFieldName, aFieldValue: string): integer;
//-------------------------------------------------------------------
var
  oList: TStringList;
begin
  oList:= TStringList.Create;

  Result:= -1;

  ItemsByFieldName(ot, aFieldName, oList);

  Result:= oList.IndexOf(aFieldValue);

  oList.Free;
end;


//-------------------------------------------------------------------
function TdmObjects1.FieldCaptionByFieldName(aObjID: integer; aFieldName: string): string;
//-------------------------------------------------------------------
begin
  if mem_Objects_Fields.Locate(FLD_NAME+';'+FLD_OBJECT_ID, VarArrayOf([aFieldName, aObjID]), [loCaseInsensitive]) then
    Result:= mem_Objects_Fields.FieldByName(FLD_CAPTION).AsString;
end;

//-------------------------------------------------------------------
function TdmObjects1.FieldTypeByFieldName(aObjID: integer; aFieldName: string):
    string;
//-------------------------------------------------------------------
(*var
  iObjID: integer;
*)
begin
//  iObjID:= ObjectIDByType(ot);

  if mem_Objects_Fields.Locate(FLD_NAME+';'+FLD_OBJECT_ID, VarArrayOf([aFieldName, aObjID]), [loCaseInsensitive]) then
    Result:= mem_Objects_Fields.FieldByName(FLD_TYPE).AsString;

(*
  if mem_Objects_Fields.Locate(FLD_NAME+';'+FLD_OBJECT_ID, VarArrayOf([aFieldName, iObjID]), [loCaseInsensitive]) then
    Result:= mem_Objects_Fields.FieldByName(FLD_TYPE).AsString;
*)
end;


begin

end.


{
//-------------------------------------------------------------------
function  TdmObjects.FieldTypeByFieldCaption (ot: TrpObjectType; aFieldCaption: string): string;
//-------------------------------------------------------------------
var
  iObjID: integer;
begin
  iObjID:= ObjectIDByType(ot);

  if mem_Objects_Fields.Locate(FLD_CAPTION+';'+FLD_OBJECT_ID, VarArrayOf([aFieldCaption, iObjID]), [loCaseInsensitive]) then
    Result:= mem_Objects_Fields.FieldByName(FLD_TYPE).AsString;
end;


//-------------------------------------------------------------------
function  TdmObjects.VerifyStrLstItemByFieldName (ot: TrpObjectType; aFieldName: string; aItem: string): boolean;
//-------------------------------------------------------------------
var
  oList: TStringList;
begin
  oList:= TStringList.Create;

  Result:= false;

  ItemsByFieldName(ot, aFieldName, oList);

  if (oList.IndexOf(aItem) > -1) then
    Result:= true;

  oList.Free;
end;

}