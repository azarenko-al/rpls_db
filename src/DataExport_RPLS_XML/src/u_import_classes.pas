unit u_import_classes;

interface

uses
  Classes, 

  u_geo
  ;

type
  TSectorList = class;
  TAntennaList= class;

  //---------------------------------------------------------
  TProperty = class(TCollectionItem)
  //---------------------------------------------------------
  public
    Name: string;
    Lat: Double;
    Lon: double;

    BLPoint_wgs: TBLPoint;
    BLPoint_pulkovo: TBLPoint;

    Status : Integer;

    LocalHeight : integer;


    Address: string;
    Description: string;

    SectorList: TSectorList;

    constructor Create(Collection: TCollection); override;

    function GetMaxHeight: double;

   // Comments: string;
  end;

//---------------------------------------------------------
  TSector = class(TCollectionItem)
  //---------------------------------------------------------
  public
    Name: string;
    Cell_ID: string;

    AntennaList: TAntennaList;

    constructor Create(Collection: TCollection); override;
  end;


//---------------------------------------------------------
  TAntenna = class(TCollectionItem)
  //---------------------------------------------------------
  public
    Name: string;
    Azimuth: double;
    Height: double;
    
    Model: string;
  end;


//  <ANTENNA Name="������� 2" Azimuth="120" Height="27" Model="HBXX6517DSVTM" Slope="-1" TiltElectrical="1" >


  //-------------------------------------------------------------------
  TPropertyList = class(TCollection)
  //-------------------------------------------------------------------
  private
    function GetItems(Index: Integer): TProperty;
  public
    constructor Create;
    function AddItem: TProperty;

    property Items[Index: Integer]: TProperty read GetItems; default;

  end;


//  <ANTENNA Name="������� 2" Azimuth="120" Height="27" Model="HBXX6517DSVTM" Slope="-1" TiltElectrical="1" >


  //-------------------------------------------------------------------
  TAntennaList = class(TCollection)
  //-------------------------------------------------------------------
  private
    function GetItems(Index: Integer): TAntenna;
  public
    constructor Create;
    function AddItem: TAntenna;

    property Items[Index: Integer]: TAntenna read GetItems; default;

  end;


//  <ANTENNA Name="������� 2" Azimuth="120" Height="27" Model="HBXX6517DSVTM" Slope="-1" TiltElectrical="1" >


  //-------------------------------------------------------------------
  TSectorList = class(TCollection)
  //-------------------------------------------------------------------
  private
    function GetItems(Index: Integer): TSector;
  public
    constructor Create;
    function AddItem: TSector;

    property Items[Index: Integer]: TSector read GetItems; default;

  end;



implementation

constructor TPropertyList.Create;
begin
  inherited Create(TProperty);
end;

function TPropertyList.AddItem: TProperty;
begin
  Result := TProperty (Add);
end;

function TPropertyList.GetItems(Index: Integer): TProperty;
begin
  Result := TProperty(inherited Items[Index]);
end;

constructor TAntennaList.Create;
begin
  inherited Create(TAntenna);
end;

function TAntennaList.AddItem: TAntenna;
begin
  Result := TAntenna (Add);
end;

function TAntennaList.GetItems(Index: Integer): TAntenna;
begin
  Result := TAntenna(inherited Items[Index]);
end;

constructor TSectorList.Create;
begin
  inherited Create(TSector);
end;

function TSectorList.AddItem: TSector;
begin
  Result := TSector ( Add);
end;

function TSectorList.GetItems(Index: Integer): TSector;
begin
  Result := TSector(inherited Items[Index]);
end;


constructor TSector.Create(Collection: TCollection);
begin
  inherited;

  AntennaList:=TAntennaList.Create;
end;


constructor TProperty.Create;
begin
  inherited;

  SectorList:=TSectorList.Create;
end;

// ---------------------------------------------------------------
function TProperty.GetMaxHeight: double;
// ---------------------------------------------------------------
var
  h: Double;
  I: Integer;
  j: Integer;
begin
  Result := 0;

  for I := 0 to SectorList.Count - 1 do
    for j := 0 to SectorList[i].AntennaList.Count - 1 do
    begin
      h:=SectorList[i].AntennaList[j].Height;

      if h > Result then
        Result:=h;
    end;

end;



end.
