
{*****************************************}
{                                         }
{            XML Data Binding             }
{                                         }
{         Generated on: 16.05.2011 19:02:10 }
{       Generated from: D:\Sites.xml      }
{   Settings stored in: D:\Sites.xdb      }
{                                         }
{*****************************************}

unit u_xml_Sites;

interface

uses XMLDoc, XMLIntf;

type

{ Forward Decls }

  IXMLDocumentType = interface;
  IXMLSITESType = interface;
  IXMLSiteType = interface;

{ IXMLDocumentType }

  IXMLDocumentType = interface(IXMLNode)
    ['{F90BACB7-BBC0-4A81-9718-8C40FB4DF273}']
    { Property Accessors }
    function Get_SITES: IXMLSITESType;
    { Methods & Properties }
    property SITES: IXMLSITESType read Get_SITES;
  end;

{ IXMLSITESType }

  IXMLSITESType = interface(IXMLNodeCollection)
    ['{C3480D2D-9376-4BEA-B5CA-B52726118DEA}']
    { Property Accessors }
    function Get_Site(Index: Integer): IXMLSiteType;
    { Methods & Properties }
    function Add: IXMLSiteType;
    function Insert(const Index: Integer): IXMLSiteType;
    property Site[Index: Integer]: IXMLSiteType read Get_Site; default;
  end;

{ IXMLSiteType }

  IXMLSiteType = interface(IXMLNode)
    ['{A088E530-20E7-43C4-8941-12CC9A67B923}']
    { Property Accessors }
    function Get_Id: WideString;
    function Get_Name: WideString;
    function Get_Address: WideString;
    function Get_B: WideString;
    function Get_L: WideString;
    { Methods & Properties }
    property Id: WideString read Get_Id;
    property Name: WideString read Get_Name;
    property Address: WideString read Get_Address;
    property B: WideString read Get_B;
    property L: WideString read Get_L;
  end;

{ Forward Decls }

  TXMLDocumentType = class;
  TXMLSITESType = class;
  TXMLSiteType = class;

{ TXMLDocumentType }

  TXMLDocumentType = class(TXMLNode, IXMLDocumentType)
  protected
    { IXMLDocumentType }
    function Get_SITES: IXMLSITESType;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLSITESType }

  TXMLSITESType = class(TXMLNodeCollection, IXMLSITESType)
  protected
    { IXMLSITESType }
    function Get_Site(Index: Integer): IXMLSiteType;
    function Add: IXMLSiteType;
    function Insert(const Index: Integer): IXMLSiteType;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLSiteType }

  TXMLSiteType = class(TXMLNode, IXMLSiteType)
  protected
    { IXMLSiteType }
    function Get_Id: WideString;
    function Get_Name: WideString;
    function Get_Address: WideString;
    function Get_B: WideString;
    function Get_L: WideString;
  end;

{ Global Functions }

function GetDocument(Doc: IXMLDocument): IXMLDocumentType;
function LoadDocument(const FileName: WideString): IXMLDocumentType;
function NewDocument: IXMLDocumentType;

const
  TargetNamespace = '';

implementation

{ Global Functions }

function GetDocument(Doc: IXMLDocument): IXMLDocumentType;
begin
  Result := Doc.GetDocBinding('Document', TXMLDocumentType, TargetNamespace) as IXMLDocumentType;
end;

function LoadDocument(const FileName: WideString): IXMLDocumentType;
begin
  Result := LoadXMLDocument(FileName).GetDocBinding('Document', TXMLDocumentType, TargetNamespace) as IXMLDocumentType;
end;

function NewDocument: IXMLDocumentType;
begin
  Result := NewXMLDocument.GetDocBinding('Document', TXMLDocumentType, TargetNamespace) as IXMLDocumentType;
end;

{ TXMLDocumentType }

procedure TXMLDocumentType.AfterConstruction;
begin
  RegisterChildNode('SITES', TXMLSITESType);
  inherited;
end;

function TXMLDocumentType.Get_SITES: IXMLSITESType;
begin
  Result := ChildNodes['SITES'] as IXMLSITESType;
end;

{ TXMLSITESType }

procedure TXMLSITESType.AfterConstruction;
begin
  RegisterChildNode('site', TXMLSiteType);
  ItemTag := 'site';
  ItemInterface := IXMLSiteType;
  inherited;
end;

function TXMLSITESType.Get_Site(Index: Integer): IXMLSiteType;
begin
  Result := List[Index] as IXMLSiteType;
end;

function TXMLSITESType.Add: IXMLSiteType;
begin
  Result := AddItem(-1) as IXMLSiteType;
end;

function TXMLSITESType.Insert(const Index: Integer): IXMLSiteType;
begin
  Result := AddItem(Index) as IXMLSiteType;
end;

{ TXMLSiteType }

function TXMLSiteType.Get_Id: WideString;
begin
  Result := AttributeNodes['id'].Text;
end;

function TXMLSiteType.Get_Name: WideString;
begin
  Result := AttributeNodes['name'].Text;
end;

function TXMLSiteType.Get_Address: WideString;
begin
  Result := AttributeNodes['address'].Text;
end;

function TXMLSiteType.Get_B: WideString;
begin
  Result := AttributeNodes['b'].Text;
end;

function TXMLSiteType.Get_L: WideString;
begin
  Result := AttributeNodes['l'].Text;
end;

end.