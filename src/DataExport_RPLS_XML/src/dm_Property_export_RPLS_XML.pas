unit dm_Property_export_RPLS_XML;

interface
{$I ver.inc}

uses
  SysUtils, Classes, DB, ADODB, Forms, Variants, Dialogs,
  XMLIntf,

  u_import_classes,

  dm_Onega_DB_data,

  u_xml_document,

  
  u_const_filters,

 // dm_Custom,
  dm_Main,

//  u_xml_Sites,

  u_Property_Add,

  u_classes,
  u_Geo,
  u_Geo_convert_new,
  u_func,

  u_db,
  


  u_const_db
   ;

type

  TdmProperty_export_RPLS_XML = class(TDataModule)
    qry_Properties: TADOQuery;
    SaveDialog1: TSaveDialog;
    OpenDialog1: TOpenDialog;
    procedure DataModuleCreate(Sender: TObject);
  private
//    procedure Dlg_ImportFrom_RPLS_SITES_XML_;
    procedure ExportListToRPLS_XML(aIDList: TIDList; aFileName: string);
    procedure OpenData(aProjectID,aFolderID: Integer);
//    procedure CreateFile;
  public
    procedure Dlg_ImportFrom_RPLS_SITES_XML;

    procedure Dlg_ExportToRPLS_XML(aIDList: TIDList; aFileName: string=''); overload;

    procedure Dlg_ExportToRPLS_XML_Folder(aFolderID: Integer; aFileName: string =
        ''); overload;

    function ExportSiteToXMLNode(aDataset: TDataset; aSites_XMLNode: IXMLNode):
        IXMLNode;
  end;


function dmProperty_export_RPLS_XML: TdmProperty_export_RPLS_XML;

//====================================================================
//
//====================================================================
implementation {$R *.dfm}


var
  FdmProperty_export: TdmProperty_export_RPLS_XML;


// -------------------------------------------------------------------
function dmProperty_export_RPLS_XML: TdmProperty_export_RPLS_XML;
// -------------------------------------------------------------------
begin
 if not Assigned(FdmProperty_export) then
     FdmProperty_export := TdmProperty_export_RPLS_XML.Create(Application);

  Result := FdmProperty_export;
end;


procedure TdmProperty_export_RPLS_XML.DataModuleCreate(Sender: TObject);
begin
  inherited;
  db_SetComponentADOConnection (Self, dmMain.ADOConnection);

//  db_SetADOConnection(dmMain.ADOConnection);

end;




//--------------------------------------------------------------------
procedure TdmProperty_export_RPLS_XML.Dlg_ExportToRPLS_XML_Folder(aFolderID:
    Integer; aFileName: string = '');
//--------------------------------------------------------------------
var
  oIDList: TIDList;

begin
  SaveDialog1.FileName:='Sites.xml';
  SaveDialog1.Filter:=FILTER_XML;

  if not SaveDialog1.Execute then
    Exit;

  OpenData (dmMain.ProjectID, aFolderID);

  // -------------------------
  oIDList:= TIDList.Create;
  oIDList.LoadIDsFromDataSet(qry_Properties, FLD_ID);

  ExportListToRPLS_XML (oIDList, SaveDialog1.FileName );

  oIDList.Free;

  ShowMessage('������� ��������.');

end;


//--------------------------------------------------------------------
procedure TdmProperty_export_RPLS_XML.OpenData(aProjectID,aFolderID: Integer);
//--------------------------------------------------------------------
var
  sSQL: string;
begin
  if aFolderID>0 then
    sSQL:=Format('SELECT * from %s WHERE project_id=%d AND folder_id=%d  ORDER BY name',
                [TBL_PROPERTY, aProjectID, aFolderID])
  else
    sSQL:=Format('SELECT * from %s WHERE project_id=%d ORDER BY name ',  // AND folder_id IS NULL
                [TBL_PROPERTY, aProjectID]);

//////  db_OpenQuery(qry_Properties, sSQL);

  dmOnega_DB_data.OpenQuery(qry_Properties, sSQL);

end;



//-------------------------------------------------------------------
procedure TdmProperty_export_RPLS_XML.Dlg_ExportToRPLS_XML(aIDList: TIDList; aFileName: string='');
//-------------------------------------------------------------------
begin
  SaveDialog1.Filter:=FILTER_XML; //'������� RPLS XML (*.xml)|*.xml)';

  if not SaveDialog1.Execute then
    Exit;

  OpenData (dmMain.ProjectID, 0);


  ExportListToRPLS_XML (aIDList, SaveDialog1.FileName );

  ShowMessage('������� ��������.');
  
end;



// ---------------------------------------------------------------
procedure TdmProperty_export_RPLS_XML.Dlg_ImportFrom_RPLS_SITES_XML;
// ---------------------------------------------------------------


  // ---------------------------------------------------------------
  procedure DoInsertAntennas(aParentNode: IXMLNode; aSector: TSector);
  // ---------------------------------------------------------------
  var
    i: Integer;
    oAntenna: TAntenna;
    vNode: IXMLNode;

  begin
    for i:=0 to aParentNode.ChildNodes.Count-1 do
    begin
      vNode:=aParentNode.ChildNodes[i];

      assert(Assigned(aSector.AntennaList));

      oAntenna:=aSector.AntennaList.AddItem;

      oAntenna.Name    := xml_GetStrAttr(vNode, 'Name');
      oAntenna.Azimuth := xml_GetFloatAttr(vNode, 'Azimuth');
      oAntenna.Height  := xml_GetFloatAttr(vNode, 'Height');

//      oAntenna.Tilt  := xml_GetFloatAttr(vNode, 'Slope');

    end;

  end;    //

  // ---------------------------------------------------------------
  procedure DoInsertSectors(aParentNode: IXMLNode; aProperty: TProperty);
  // ---------------------------------------------------------------
  var
    i: Integer;
    oSector: TSector;
    vNode: IXMLNode;

  begin
    for i:=0 to aParentNode.ChildNodes.Count-1 do
    begin
      vNode:=aParentNode.ChildNodes[i];

      assert(Assigned(aProperty.SectorList));

      oSector:=aProperty.SectorList.AddItem;

      oSector.Name    := xml_GetStrAttr(vNode, 'Name');
      oSector.Cell_ID := xml_GetStrAttr(vNode, 'Cell_ID');

      DoInsertAntennas (vNode, oSector);

    end;

  end;    //



var
  bIsWGS: Boolean;
  oXMLDoc: TXMLDoc;
  vNode,vSites,vDocument: IXMLNode;

  bl: TBLPoint;
  bl_WGS: TBLPoint;

  I: Integer;
  oPropertyAdd: TPropertyAddToDB;
  sCoordSys: string;

  oPropertyList: TPropertyList;
  oProperty: TProperty;

begin
  if not OpenDialog1.Execute then
    Exit;

  oPropertyList:=TPropertyList.Create;



 // oPropertyAdd:=TPropertyAdd.Create;


  oXMLDoc:=TXMLDoc.Create;
  oXMLDoc.LoadFromFile (OpenDialog1.FileName);

  vDocument:=oXMLDoc.DocumentElement;

//  vDocument := oXMLDoc.DocumentElement;
   vSites   := xml_FindNode(vDocument, 'SITES');

//     <SITES CoordSys="WGS84">



  if Assigned(vSites) then
  begin
    sCoordSys:=xml_GetStrAttr(vSites, 'CoordSys');

    bIsWGS:= Eq(sCoordSys,'WGS84');


    for i:=0 to vSites.ChildNodes.Count-1 do
    begin
      vNode:=vSites.ChildNodes[i];

      oProperty:=oPropertylist.AddItem;

      oProperty.Name    := xml_GetStrAttr(vNode, 'Name');
      oProperty.Address := xml_GetStrAttr(vNode, 'Address');

      oProperty.LocalHeight := xml_GetIntAttr(vNode, 'LocalHeight');


      bl.B  := xml_GetFloatAttr (vNode, 'B');
      bl.L  := xml_GetFloatAttr (vNode, 'L');

      bl_WGS.B  := xml_GetFloatAttr (vNode, 'B_WGS');
      bl_WGS.L  := xml_GetFloatAttr (vNode, 'L_WGS');

      
      if bIsWGS then
      begin
        oProperty.BLPoint_wgs := bl;

        bl:=geo_WGS84_to_Pulkovo42(bl) ;
    //    bl1:=geo_Pulkovo42_to_WGS84(bl) ;

        oProperty.BLPoint_pulkovo:=bl;

      end else
        oProperty.BLPoint_pulkovo := bl;



      if bl_WGS.B<>0 then
        oProperty.BLPoint_wgs :=bl_WGS;


     DoInsertSectors (vNode, oProperty);

    end;
  end;


  // ---------------------------------------------------------------

  oPropertyAdd:=TPropertyAddToDB.Create;

  for I := 0 to oPropertyList.Count - 1 do
  begin
    oProperty:=oPropertyList[i];

    oPropertyAdd.PROJECT_ID := dmMain.ProjectID;
    oPropertyAdd.NewName    := oProperty.Name;
    oPropertyAdd.Address    := oProperty.Address;

//    if oProperty.LocalHeight>0 then
    oPropertyAdd.Tower_height:=oProperty.LocalHeight;

  //  oPropertyAdd.Tower_height:=oProperty.GetMaxHeight();

  //  oPropertyAdd.BLPoint_Pulkovo  := oProperty.BLPoint_pulkovo;
    oPropertyAdd.BLPoint_Pulkovo  := oProperty.BLPoint_pulkovo;
    oPropertyAdd.BLPoint_WGS      := oProperty.BLPoint_wgs;

    oPropertyAdd.Add_DB; //(dmMain.ADOConnection);

  end;


  FreeAndNil(oPropertyAdd);

 // ShowMessage('�������� ������ ��������: ' + IntToStr(oPropertyList.Count));


  FreeAndNil(oPropertyList);


  ShowMessage('������ ��������.');
end;




//--------------------------------------------------------------
function TdmProperty_export_RPLS_XML.ExportSiteToXMLNode(aDataset: TDataset;
    aSites_XMLNode: IXMLNode): IXMLNode;
//--------------------------------------------------------------
begin
  with aDataset do
    Result:=xml_AddNode(aSites_XMLNode, 'site',
                [xml_Par('id',      FieldByName(FLD_ID).AsInteger),
                 xml_Par('name',    FieldByName(FLD_NAME).AsString),
              //   xml_Par('number',  iNumber),
                 xml_Par('address', FieldByName(FLD_address).AsString),
                 xml_Par('b',       FieldByName(FLD_LAT).AsFloat),
                 xml_Par('l',       FieldByName(FLD_LON).AsFloat)
                ]);

end;


//--------------------------------------------------------------
procedure TdmProperty_export_RPLS_XML.ExportListToRPLS_XML(aIDList: TIDList; aFileName:  string);
//--------------------------------------------------------------
var
  i: integer;
  iNumber: integer;
  iID: integer;

  vSite,vSites: IXMLNode;
  iSiteID: Integer;
  oXMLDoc: TXMLDoc;

label
  LABEL_while_next;

begin
  assert(qry_Properties.Active, 'qry_Properties.Active');
 // assert(qry_Properties.IsEmpty, 'qry_Properties.IsEmpty');
  assert(qry_Properties.RecordCount>0, 'qry_Properties.IsEmpty');


  oXMLDoc:=TXMLDoc.Create;

  vSites:=oXMLDoc.DocumentElement.AddChild('sites');


  with qry_Properties do
    while not EOF do
  begin
    iID:=FieldByName(FLD_ID).AsInteger;

    if Assigned(aIDList) then
      if aIDList.Count>0 then
        if (not aIDList.IDFound(iID)) then
          goto LABEL_while_next;


    vSite:=ExportSiteToXMLNode(qry_Properties, vSites);

  LABEL_while_next:

    Next;
  end;

  oXMLDoc.SaveToFile (ChangeFileExt(aFileName, '.xml'));

  FreeAndNil(oXMLDoc);
end;


begin
end.


  {

<?xml version="1.0" encoding="windows-1251"?>
<Document>
  <SITES CoordSys="WGS84">
    <site Name="LI0345.2" CurrStatus="2" Address="�. ������, ��. �����������, 23�" B="52,620203" L="39,623795" Comment="#RO DocNumber: APUN-9KL83Q, Exported: 02.06.2014 10:05:23">
      <sector Name="LI0345_1" Cell_ID="3451" >
        <ANTENNA Name="������� 1" Azimuth="0" Height="27" Model="HBXX6517DSVTM" Slope="0" TiltElectrical="1" >
        </ANTENNA>
      </sector>
      <sector Name="LI0345_2" Cell_ID="3452" >
        <ANTENNA Name="������� 2" Azimuth="120" Height="27" Model="HBXX6517DSVTM" Slope="-1" TiltElectrical="1" >
        </ANTENNA>
      </sector>
      <sector Name="LI0345_3" Cell_ID="3453" >
        <ANTENNA Name="������� 3" Azimuth="230" Height="27" Model="HBXX6517DSVTM" Slope="0" TiltElectrical="1" >
        </ANTENNA>
      </sector>
    </site>
  </SITES>
</Document>






procedure TdmProperty_export_RPLS_XML.CreateFile;
begin
  mdb_CreateFileAndOpen ('d:\test.mdb', ADOConnection_MDB);


  mdb_CreateTableFromArr(ADOConnection_MDB, TBL_PROPERTY,
    [

    ]);





//  db_SetADOConnection(dmMain.ADOConnection);

end;




