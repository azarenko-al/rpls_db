object dmLink_export_RPLS_XML: TdmLink_export_RPLS_XML
  OldCreateOrder = True
  OnCreate = DataModuleCreate
  OnDestroy = DataModuleDestroy
  Left = 1157
  Top = 528
  Height = 624
  Width = 754
  object qry_Links: TADOQuery
    Parameters = <>
    Left = 44
    Top = 164
  end
  object SaveDialog1: TSaveDialog
    FileName = 'links.xml'
    Filter = 'XML (*.xml)|*.xml'
    Left = 64
    Top = 80
  end
  object qry_Temp: TADOQuery
    LockType = ltBatchOptimistic
    Parameters = <>
    Left = 205
    Top = 70
  end
  object OpenDialog1: TOpenDialog
    Filter = 'XML (*.xml)|*.xml'
    Left = 584
    Top = 112
  end
  object ActionList1: TActionList
    Left = 576
    Top = 20
    object act_Link_profile_Save_XML: TAction
      Category = 'Link_export'
      Caption = #1057#1086#1093#1088#1072#1085#1080#1090#1100' '#1087#1088#1086#1092#1080#1083#1100' '#1074' XML '#1092#1072#1081#1083
    end
    object act_Link_profile_Load_XML: TAction
      Category = 'Link_export'
      Caption = 'act_Link_profile_Load_XML'
    end
  end
  object qry_prop: TADOQuery
    Parameters = <>
    Left = 308
    Top = 76
  end
  object ds_Links: TDataSource
    DataSet = qry_Links
    Left = 40
    Top = 216
  end
  object qry_LinkEnd1: TADOQuery
    CursorType = ctStatic
    DataSource = ds_Links
    Parameters = <
      item
        Name = 'linkend1_id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Value = 2354
      end>
    SQL.Strings = (
      'select * from linkend where id=:linkend1_id')
    Left = 204
    Top = 244
  end
  object qry_LinkEnd2: TADOQuery
    CursorType = ctStatic
    DataSource = ds_Links
    Parameters = <
      item
        Name = 'linkend2_id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Value = 2355
      end>
    SQL.Strings = (
      'select * from linkend where id=:linkend2_id')
    Left = 204
    Top = 388
  end
  object qry_Antenna1: TADOQuery
    CursorType = ctStatic
    DataSource = ds_LinkEnd1
    Parameters = <
      item
        Name = 'id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Value = 2354
      end>
    SQL.Strings = (
      'select * from linkend_antenna where linkend_id=:id')
    Left = 340
    Top = 188
  end
  object qry_Prop1: TADOQuery
    CursorType = ctStatic
    DataSource = ds_LinkEnd1
    Parameters = <
      item
        Name = 'property_id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Value = 2914
      end>
    SQL.Strings = (
      'select * from property '
      'where id=:property_id')
    Left = 492
    Top = 228
  end
  object ds_LinkEnd2: TDataSource
    DataSet = qry_LinkEnd2
    Left = 208
    Top = 440
  end
  object ds_LinkEnd1: TDataSource
    DataSet = qry_LinkEnd1
    Left = 200
    Top = 304
  end
  object ds_Prop1: TDataSource
    DataSet = qry_Prop1
    Left = 496
    Top = 288
  end
  object ds_Prop2: TDataSource
    DataSet = qry_Prop2
    Left = 504
    Top = 440
  end
  object qry_Prop2: TADOQuery
    CursorType = ctStatic
    DataSource = ds_LinkEnd2
    Parameters = <
      item
        Name = 'property_id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Value = 2915
      end>
    SQL.Strings = (
      'select * from property '
      'where id=:property_id')
    Left = 492
    Top = 372
  end
  object qry_Antenna2: TADOQuery
    CursorType = ctStatic
    DataSource = ds_LinkEnd2
    Parameters = <
      item
        Name = 'id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Value = 2355
      end>
    SQL.Strings = (
      'select * from linkend_antenna where linkend_id=:id')
    Left = 324
    Top = 340
  end
  object ds_Antenna1: TDataSource
    DataSet = qry_Antenna1
    Left = 336
    Top = 240
  end
  object ds_Antenna2: TDataSource
    DataSet = qry_Antenna2
    Left = 320
    Top = 392
  end
end
