object Form1: TForm1
  Left = 655
  Top = 641
  Width = 1195
  Height = 655
  Caption = 'Form1'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object DBGrid1: TDBGrid
    Left = 40
    Top = 376
    Width = 249
    Height = 81
    DataSource = ds_Links
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
  end
  object DBGrid2: TDBGrid
    Left = 344
    Top = 376
    Width = 329
    Height = 81
    DataSource = ds_LinkEnd1
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
  end
  object DBGrid3: TDBGrid
    Left = 344
    Top = 464
    Width = 329
    Height = 81
    DataSource = ds_LinkEnd2
    TabOrder = 2
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
  end
  object DBGrid4: TDBGrid
    Left = 704
    Top = 384
    Width = 329
    Height = 81
    DataSource = ds_Prop1
    TabOrder = 3
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
  end
  object DBGrid5: TDBGrid
    Left = 704
    Top = 472
    Width = 329
    Height = 81
    DataSource = ds_Prop2
    TabOrder = 4
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
  end
  object DBGrid6: TDBGrid
    Left = 688
    Top = 96
    Width = 329
    Height = 81
    DataSource = ds_Antenna1
    TabOrder = 5
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
  end
  object DBGrid7: TDBGrid
    Left = 688
    Top = 184
    Width = 329
    Height = 81
    DataSource = ds_Antenna2
    TabOrder = 6
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
  end
  object qry_Links: TADOQuery
    Active = True
    Connection = ADOConnection_
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select  top 1  * from link')
    Left = 44
    Top = 100
  end
  object qry_LinkEnd1: TADOQuery
    Active = True
    Connection = ADOConnection_
    CursorType = ctStatic
    DataSource = ds_Links
    Parameters = <
      item
        Name = 'linkend1_id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Value = 2354
      end>
    SQL.Strings = (
      'select * from linkend where id=:linkend1_id')
    Left = 204
    Top = 100
  end
  object qry_LinkEnd2: TADOQuery
    Active = True
    Connection = ADOConnection_
    CursorType = ctStatic
    DataSource = ds_Links
    Parameters = <
      item
        Name = 'linkend2_id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Value = 2355
      end>
    SQL.Strings = (
      'select * from linkend where id=:linkend2_id')
    Left = 204
    Top = 236
  end
  object qry_Antenna1: TADOQuery
    Active = True
    Connection = ADOConnection_
    CursorType = ctStatic
    DataSource = ds_LinkEnd1
    Parameters = <
      item
        Name = 'id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Value = 2354
      end>
    SQL.Strings = (
      'select * from linkend_antenna where linkend_id=:id')
    Left = 324
    Top = 52
  end
  object qry_Prop1: TADOQuery
    Active = True
    Connection = ADOConnection_
    CursorType = ctStatic
    DataSource = ds_LinkEnd1
    Parameters = <
      item
        Name = 'property_id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Value = 2914
      end>
    SQL.Strings = (
      'select * from property '
      'where id=:property_id')
    Left = 460
    Top = 36
  end
  object ds_Links: TDataSource
    DataSet = qry_Links
    Left = 40
    Top = 152
  end
  object ds_LinkEnd2: TDataSource
    DataSet = qry_LinkEnd2
    Left = 208
    Top = 288
  end
  object ds_LinkEnd1: TDataSource
    DataSet = qry_LinkEnd1
    Left = 200
    Top = 152
  end
  object ADOConnection_: TADOConnection
    Connected = True
    ConnectionString = 
      'Provider=SQLOLEDB.1;Password=sa;Persist Security Info=True;User ' +
      'ID=sa;Initial Catalog=onega_link;Data Source=SERVER1'
    LoginPrompt = False
    Mode = cmShareDenyNone
    Provider = 'SQLOLEDB.1'
    Left = 56
    Top = 32
  end
  object ds_Prop1: TDataSource
    DataSet = qry_Prop1
    Left = 464
    Top = 96
  end
  object ds_Prop2: TDataSource
    DataSet = qry_Prop2
    Left = 472
    Top = 248
  end
  object qry_Prop2: TADOQuery
    Active = True
    Connection = ADOConnection_
    CursorType = ctStatic
    DataSource = ds_LinkEnd2
    Parameters = <
      item
        Name = 'property_id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Value = 2915
      end>
    SQL.Strings = (
      'select * from property '
      'where id=:property_id')
    Left = 460
    Top = 180
  end
  object qry_Antenna2: TADOQuery
    Active = True
    Connection = ADOConnection_
    CursorType = ctStatic
    DataSource = ds_LinkEnd2
    Parameters = <
      item
        Name = 'id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Value = 2355
      end>
    SQL.Strings = (
      'select * from linkend_antenna where linkend_id=:id')
    Left = 324
    Top = 188
  end
  object ds_Antenna1: TDataSource
    DataSet = qry_Antenna1
    Left = 328
    Top = 104
  end
  object ds_Antenna2: TDataSource
    DataSet = qry_Antenna2
    Left = 320
    Top = 240
  end
end
