unit dm_Link_export_RPLS_XML;

interface
//{$I ver.inc}

uses
  SysUtils, Classes, DB, ADODB, Forms, Variants, Dialogs,


  dm_Onega_DB_data,

//  dm_Property_export_RPLS_XML,

  dm_Main,


  XMLIntf,

  u_xml_document,

  u_classes,

  u_db,


  u_const_db,
  u_Link_const,

  ActnList
  ;

type

  TdmLink_export_RPLS_XML = class(TDataModule)
    qry_Links: TADOQuery;
    SaveDialog1: TSaveDialog;
    qry_Temp: TADOQuery;
    OpenDialog1: TOpenDialog;
    ActionList1: TActionList;
    act_Link_profile_Save_XML: TAction;
    act_Link_profile_Load_XML: TAction;
    qry_prop: TADOQuery;
    ds_Links: TDataSource;
    qry_LinkEnd1: TADOQuery;
    qry_LinkEnd2: TADOQuery;
    qry_Antenna1: TADOQuery;
    qry_Prop1: TADOQuery;
    ds_LinkEnd2: TDataSource;
    ds_LinkEnd1: TDataSource;
    ds_Prop1: TDataSource;
    ds_Prop2: TDataSource;
    qry_Prop2: TADOQuery;
    qry_Antenna2: TADOQuery;
    ds_Antenna1: TDataSource;
    ds_Antenna2: TDataSource;
    procedure DataModuleDestroy(Sender: TObject);
    procedure DataModuleCreate(Sender: TObject);
  private
    FIDList: TIDList;

    procedure ExportListToRPLS_XML(aIDList: TIDList; aFileName: string);

    function ExportSiteToXMLNode(aDataset: TDataset; aSites_XMLNode: IXMLNode):
        IXMLNode;
    procedure Export_Property(aParent_XMLNode: IXMLNode);
    procedure OpenData;
  public
    procedure Dlg_ExportToRPLS_XML(aIDList: TIDList; aFileName: string=''); overload;

    procedure Dlg_ExportToRPLS_XML_Folder(aFolderID: Integer; aFileName: string =
        ''); overload;
    function ExportLinkToXMLNode(aDataset: TDataset; aParent_XMLNode: IXMLNode):
        IXMLNode;

  end;


function dmLink_export_RPLS_XML: TdmLink_export_RPLS_XML;


//====================================================================
//
//====================================================================
implementation
  {$R *.dfm}

var
  FdmLink_export: TdmLink_export_RPLS_XML;


// -------------------------------------------------------------------
function dmLink_export_RPLS_XML: TdmLink_export_RPLS_XML;
// -------------------------------------------------------------------
begin
 if not Assigned(FdmLink_export) then
   FdmLink_export := TdmLink_export_RPLS_XML.Create(Application);

  Result := FdmLink_export;
end;


procedure TdmLink_export_RPLS_XML.DataModuleCreate(Sender: TObject);
begin
  inherited;
  db_SetComponentADOConnection(Self, dmMain.AdoConnection);

  FIDList := TIDList.Create();
end;


procedure TdmLink_export_RPLS_XML.DataModuleDestroy(Sender: TObject);
begin
  FreeAndNil(FIDList);
end;

//--------------------------------------------------------------------
procedure TdmLink_export_RPLS_XML.Dlg_ExportToRPLS_XML_Folder(aFolderID:
    Integer; aFileName: string = '');
//--------------------------------------------------------------------
var
  oBSList: TIDList;
  sSQL: string;
begin
  SaveDialog1.FileName:='Links.xml';
  SaveDialog1.DefaultExt := '.csv';
  SaveDialog1.Filter:='������� RPLS XML (*.xml)|*.xml)';

  if not SaveDialog1.Execute then
    Exit;

(*  sSQL:=Format('SELECT * from %s WHERE project_id=%d', [TBL_PROPERTY, dmMain.ProjectID]);
  db_OpenQuery(qry_prop, sSQL);

  sSQL:=Format('SELECT * from %s WHERE project_id=%d', [TBL_LInkEND, dmMain.ProjectID]);
  db_OpenQuery(qry_LinkEnd, sSQL);

*)

  if aFolderID>0 then
    sSQL:=Format('SELECT id from %s WHERE project_id=%d AND folder_id=%d ORDER BY name',
                [VIEW_LINK, dmMain.ProjectID, aFolderID])
  else
    sSQL:=Format('SELECT id from %s WHERE project_id=%d ORDER BY name ',  // AND folder_id IS NULL
                [VIEW_LINK, dmMain.ProjectID]);

  dmOnega_DB_data.OpenQuery(qry_Temp, sSQL);

//  db_OpenQuery(qry_Temp, sSQL);


  oBSList:= TIDList.Create;
  oBSList.LoadIDsFromDataSet(qry_Temp, FLD_ID);

  ExportListToRPLS_XML (oBSList, SaveDialog1.FileName );

  FreeAndNil(oBSList);



end;


//-------------------------------------------------------------------
procedure TdmLink_export_RPLS_XML.Dlg_ExportToRPLS_XML(aIDList: TIDList; aFileName: string='');
//-------------------------------------------------------------------
begin

  SaveDialog1.DefaultExt := '.xml';
  SaveDialog1.Filter:='������� RPLS XML (*.xml)|*.xml)';

  if not SaveDialog1.Execute then
    Exit;

 // SaveDialog1.FileName := 's:\1\_link.xml';


  ExportListToRPLS_XML (aIDList, SaveDialog1.FileName );
end;


//--------------------------------------------------------------
function TdmLink_export_RPLS_XML.ExportSiteToXMLNode(aDataset: TDataset;
    aSites_XMLNode: IXMLNode): IXMLNode;
//--------------------------------------------------------------

(*

  vSites:=oXMLDoc.DocumentElement.AddChild('sites');

  with qry_Properties do
    while not EOF do
  begin
    iID:=FieldByName(FLD_ID).AsInteger;

    vSite:=xml_AddNode(vSites, 'site',
                [xml_Par('id',      FieldByName(FLD_ID).AsInteger),
                 xml_Par('name',    FieldByName(FLD_NAME).AsString),
              //   xml_Par('number',  iNumber),
                 xml_Par('address', FieldByName(FLD_address).AsString),
                 xml_Par('b',       FieldByName(FLD_LAT).AsFloat),
                 xml_Par('l',       FieldByName(FLD_LON).AsFloat)
                ]);
  *)

begin
  with aDataset do
    Result:=xml_AddNode(aSites_XMLNode, 'site',
                [xml_Par('id',      FieldByName(FLD_ID).AsInteger),
                 xml_Par('name',    FieldByName(FLD_NAME).AsString),
              //   xml_Par('number',  iNumber),
                 xml_Par('address', FieldByName(FLD_address).AsString),
                 xml_Par('b',       FieldByName(FLD_LAT).AsFloat),
                 xml_Par('l',       FieldByName(FLD_LON).AsFloat)
                ]);

end;


//--------------------------------------------------------------
procedure TdmLink_export_RPLS_XML.ExportListToRPLS_XML(aIDList: TIDList; aFileName:  string);
//--------------------------------------------------------------
var
  i: integer;
  iNumber: integer;
  iID: integer;
  iLinkEnd1_ID: Integer;
  iLinkEnd2_ID: Integer;
  iPROPERTY1_ID: Integer;
  iPROPERTY2_ID: Integer;

  vLink,vLinks: IXMLNode;
  iSiteID: Integer;
  oXMLDoc: TXMLDoc;
  vProject: IXMLNode;
  vScenario: IXMLNode;
  vStations: IXMLNode;
  Y: Double;

label
  LABEL_while_next;

begin
 // TdmAntType.Init;

//  sParamFileName:=SaveDialog1.FileName;// 'e:\1.xml';

  OpenData();

(*  gl_DB.ExecCommand('CREATE TABLE #temp (id INT)');

  for i := 0 to aIDList.Count - 1 do
    gl_DB.ExecCommand(
       Format('INSERT INTO #temp (id) VALUES (%d)',[aIDList[i].ID]));

  db_OpenQuery (qry_Links, 'SELECT * FROM ' +  VIEW_LINK +
//  db_OpenQuery (qry_Links, 'SELECT * FROM ' +  TBL_LINK +
                         ' WHERE id IN (SELECT id FROM #temp)',
                [db_Par(FLD_project_id,   dmMain.ProjectID)
                ]);

  gl_DB.ADOConnection.Execute('drop TABLE #temp ');

*)

  oXMLDoc:=TXMLDoc.Create;

  vProject :=oXMLDoc.DocumentElement.AddChild('project');
  vScenario:=vProject.AddChild('scenario');
    vScenario.Attributes['id']   := 1;
    vScenario.Attributes['Name'] := '��������';


  vLinks:=vProject.AddChild('RRLS');


//  if not VarIsNull(vLinks) then
  with qry_Links do
    while not EOF do
  begin
    iID:=FieldByName(FLD_ID).AsInteger;


    if Assigned(aIDList) then
      if aIDList.Count>0 then
        if (not aIDList.IDFound(iID)) then
          goto LABEL_while_next;


    //iPROPERTY1_ID:=FieldByName(FLD_PROPERTY1_ID).AsInteger;
//    iPROPERTY2_ID:=FieldByName(FLD_PROPERTY2_ID).AsInteger;

    iPROPERTY1_ID:=qry_Prop1.FieldByName(FLD_ID).AsInteger;
    iPROPERTY2_ID:=qry_Prop2.FieldByName(FLD_ID).AsInteger;


    if not FIDList.IDFound(iPROPERTY1_ID) then
      FIDList.AddID(iPROPERTY1_ID);

    if not FIDList.IDFound(iPROPERTY2_ID) then
      FIDList.AddID(iPROPERTY2_ID);

    iLinkEnd1_ID:=FieldByName(FLD_LinkEnd1_ID).AsInteger;
    iLinkEnd2_ID:=FieldByName(FLD_LinkEnd2_ID).AsInteger;

    Assert(iLinkEnd1_ID>0, 'Value <=0');
    Assert(iLinkEnd2_ID>0, 'Value <=0');

    Assert(qry_LinkEnd1.RecordCount>0);

  //  db_View(qry_LinkEnd1);

//    qry_Prop1.Locate(FLD_Id, iPROPERTY1_ID, []);
//    qry_Prop2.Locate(FLD_Id, iPROPERTY2_ID, []);


{
    if qry_LinkEnd1.Locate(FLD_Id, iLinkEnd1_ID, []) then
     if qry_LinkEnd2.Locate(FLD_Id, iLinkEnd2_ID, []) then
      if qry_Antenna1.Locate(FLD_LinkEnd_Id, iLinkEnd1_ID, []) then
       if qry_Antenna2.Locate(FLD_LinkEnd_Id, iLinkEnd2_ID, []) then
 }

          ExportLinkToXMLNode(qry_Links, vLinks);

   // if qry_LinkEnd.Locate(FLD_Id, iLinkEnd2_ID, []) then;


   // iNumber:= gl_db.GetIntFieldValue(TBL_SITE, FLD_NUMBER,
                         //           [db_par(FLD_PROPERTY_ID, iID)]);
//    if aIDList.IDFound(iID) then

  LABEL_while_next:

    Next;
  end;

  vStations:=vScenario.AddChild('stations');
  Export_Property (vStations);


  oXMLDoc.SaveToFile (ChangeFileExt(aFileName, '.xml'));

  FreeAndNil(oXMLDoc);
end;

//--------------------------------------------------------------
function TdmLink_export_RPLS_XML.ExportLinkToXMLNode(aDataset: TDataset;
    aParent_XMLNode: IXMLNode): IXMLNode;
//--------------------------------------------------------------
var
  iPolarization: Integer;
  s: string;
begin
  s:=LowerCase(qry_Antenna1.FieldByName(FLD_POLARIZATION_STR).AsString);
  if s='h' then iPolarization:=0 else
  if s='v' then iPolarization:=1
           else iPolarization := 2;


  with aDataset do
    Result:=xml_AddNode(aParent_XMLNode, 'RRL',
      [
       xml_Par('id',      FieldByName(FLD_ID).AsInteger),
       xml_Par('parent_id', 1),

       xml_Par('name',    FieldByName(FLD_NAME).AsString),
    //   xml_Par('number',  iNumber),


       xml_Par('b1',       qry_Prop1.FieldByName(FLD_LAT).AsFloat),
       xml_Par('l1',       qry_Prop1.FieldByName(FLD_LON).AsFloat),

       xml_Par('b2',       qry_Prop2.FieldByName(FLD_LAT).AsFloat),
       xml_Par('l2',       qry_Prop2.FieldByName(FLD_LON).AsFloat),



(*
     xml_Par('b1',       FieldByName(FLD_LAT1).AsFloat),
       xml_Par('l1',       FieldByName(FLD_LON1).AsFloat),

       xml_Par('b2',       FieldByName(FLD_LAT2).AsFloat),
       xml_Par('l2',       FieldByName(FLD_LON2).AsFloat),
*)


(*
    oXLink.LinkEnd1.FREQ_GHz     :=xml_GetFloatAttr (vNode, 'FreqWork');//AsFloat(vRRL.FreqWork);
    oXLink.LinkEnd2.FREQ_GHz     :=xml_GetFloatAttr (vNode, 'FreqWork');//AsFloat(vRRL.FreqWork);

    oXLink.LinkEnd1.Bitrate_Mbps :=xml_GetIntAttr (vNode, 'Speed');//AsInteger(vRRL.Speed);
    oXLink.LinkEnd2.Bitrate_Mbps :=xml_GetIntAttr (vNode, 'Speed');//AsInteger(vRRL.Speed);
*)


       xml_Par('EmpLevel', FieldByName(FLD_Rx_Level_dBm).AsFloat),
       xml_Par('NFrenel',  FieldByName(FLD_NFrenel).AsInteger),

       xml_Par('Refraction', FieldByName(FLD_Refraction).AsFloat),

       xml_Par('Speed',           qry_LinkEnd1.FieldByName(FLD_Bitrate_Mbps).AsFloat),
       xml_Par('FreqWork',        qry_LinkEnd1.FieldByName(FLD_TX_FREQ_MHz).AsFloat / 1000),


       xml_Par('KratnostByFreq',  qry_LinkEnd1.FieldByName(FLD_Kratnost_By_Freq).AsInteger),
       xml_Par('kratnostBySpace', qry_LinkEnd1.FieldByName(FLD_kratnost_By_Space).AsInteger),

       xml_Par('Site1_Power',       qry_LinkEnd1.FieldByName(FLD_Power_dBm).AsFloat),
       xml_Par('Site2_Sensitivity', qry_LinkEnd1.FieldByName(FLD_THRESHOLD_BER_3).AsFloat),

       // -------------------------
       xml_Par('TRB_1', FieldByName(FLD_GST_SESR).AsFloat),                  // '����������� �������� SESR ��� ����, %');
       xml_Par('TRB_2', FieldByName(FLD_GST_Kng).AsFloat),                   // '����������� �������� ���  ��� ����, %');
       xml_Par('TRB_3', FieldByName(FLD_GST_Length).AsFloat),                // '����� ����, ��');
       xml_Par('TRB_4', FieldByName(FLD_KNG_dop).AsFloat),                   // '���������� ����� ���, ������������� �������������}');
       xml_Par('TRB_5', FieldByName(FLD_SPACE_LIMIT).AsFloat),               // '���������� ������������� ������� p(a%)');
       xml_Par('TRB_6', FieldByName(FLD_SPACE_LIMIT_PROBABILITY).AsFloat),   // '���������� ����������� a% ������������� ������������ �������� p(a%) [%]');

       // -------------------------  

       xml_Par('DLT_1', FieldByName(FLD_Profile_Step).AsFloat/1000),                  //'��� ��������� �������, �� ( 0 - ��������� ����)');
//       xml_Par('DLT_2', FieldByName(FLD_precision_V_diffraction).AsFloat),     //'�������� ������ ������� V����(g)=V���.�, ��');
//       xml_Par('DLT_3', FieldByName(FLD_precision_g_V_diffraction).AsFloat),   //'�������� ������ ������� g(V����)=g(V���.�), 10^-8');
//       xml_Par('DLT_4', FieldByName(FLD_max_gradient_for_subrefr).AsFloat),    //'������������ �������� ��� ������� ������������, 10^-8');

       // -------------------------
       //������� ��������������� ��������� (���)
       // -------------------------
       xml_Par('RRV_1', FieldByName(FLD_gradient_diel).AsFloat),     // '������� �������� ��������� ����.�������������,10^-8 1/�' );
       xml_Par('RRV_2', FieldByName(FLD_gradient_deviation).AsFloat),// '����������� ���������� ���������,10^-8 1/�' );
       xml_Par('RRV_3', FieldByName(FLD_underlying_terrain_type).AsInteger),

       xml_Par('RRV_4', FieldByName(FLD_steam_wet).AsFloat),
       xml_Par('RRV_5', FieldByName(FLD_Q_factor).AsFloat),
       xml_Par('RRV_6', FieldByName(FLD_climate_factor).AsFloat),
       xml_Par('RRV_7', FieldByName(FLD_factor_B).AsFloat),
       xml_Par('RRV_8', FieldByName(FLD_factor_C).AsFloat),
       xml_Par('RRV_9', FieldByName(FLD_factor_D).AsFloat),
       xml_Par('RRV_10', FieldByName(FLD_rain_intensity).AsFloat),
       xml_Par('RRV_11', FieldByName(FLD_REFRACTION).AsFloat),
       xml_Par('RRV_12', FieldByName(FLD_terrain_type).AsFloat),

(*

      gradient_diel_1 := AsFloat(vRRL.RRV_1);   // '������� �������� ��������� ����.�������������,10^-8 1/�' );
      gradient_2      := AsFloat(vRRL.RRV_2);   // '����������� ���������� ���������,10^-8 1/�' );

      underlying_terrain_type_3:= AsFloat(vRRL.RRV_3); //'��� ������������ �������.(1...3,0->����.�����=����.���.)' );

      steam_wet_4     := AsFloat(vRRL.RRV_4);   // '���������� ��������� �������� ����, �/���.�');
      Q_factor_5      := AsFloat(vRRL.RRV_5);   // 'Q-������ ������ �����������');
      climate_factor_6:= AsFloat(vRRL.RRV_6);   //'������������� ������ K��, 10^-6');
      factor_B_7      := AsFloat(vRRL.RRV_7);   // '�������� ������������ b ��� ������� ������');
      factor_C_8      := AsFloat(vRRL.RRV_8);   // '�������� ������������ c ��� ������� ������');
      FACTOR_D_9      := AsFloat(vRRL.RRV_9);   // '�������� ������������ d ��� ������� ������');
      RAIN_RATE_10    := AsFloat(vRRL.RRV_10);   // '������������� ����� � ������� 0.01% �������, ��/���');
      REFRACTION_11   := AsFloat(vRRL.RRV_11);       //'�����. ��������� (��� ��.���������=0, ����� �.�.=0)');

      terrain_type_14 := AsFloat(vRRL.RRV_12);   //'��� ���������.(1...3,0->����.�����=����.���.)' );
*)

       xml_Par('Site1_Antenna_Height',   qry_Antenna1.FieldByName(FLD_Height).AsFloat),
       xml_Par('Site1_Antenna_DIAMETER', qry_Antenna1.FieldByName(FLD_DIAMETER).AsFloat),
       xml_Par('Site1_Antenna_Gain',     qry_Antenna1.FieldByName(FLD_GAIN).AsFloat),
       xml_Par('Site1_Antenna_Width',    qry_Antenna1.FieldByName(FLD_VERT_WIDTH).AsFloat),

       xml_Par('Site2_Antenna_Height',   qry_Antenna2.FieldByName(FLD_Height).AsFloat),
       xml_Par('Site2_Antenna_DIAMETER', qry_Antenna2.FieldByName(FLD_DIAMETER).AsFloat),
       xml_Par('Site2_Antenna_Gain',     qry_Antenna2.FieldByName(FLD_GAIN).AsFloat),
       xml_Par('Site2_Antenna_Width',    qry_Antenna2.FieldByName(FLD_VERT_WIDTH).AsFloat),

       xml_Par('PolarizationType',     iPolarization)



(*
    oAntenna:=oXLink.LinkEnd1.AntennaList.AddItem;
    oAntenna.   := AsFloat(vRRL.);
    oAntenna. := AsFloat(vRRL.);
    oAntenna.  := AsFloat(vRRL.);
    oAntenna.Polarization  := sPolarization;
*)



(*
   //-------------------------------------------------------------------
    with oXLink.RRV do begin  //������� ��������������� ��������� (���)
    //-------------------------------------------------------------------

      _1 := AsFloat(vRRL.RRV_1);   // '������� �������� ��������� ����.�������������,10^-8 1/�' );
      _2      := AsFloat(vRRL.RRV_2);   // '����������� ���������� ���������,10^-8 1/�' );

      underlying_terrain_type_3:= AsFloat(vRRL.RRV_3); //'��� ������������ �������.(1...3,0->����.�����=����.���.)' );

      _4     := AsFloat(vRRL.RRV_4);   // '���������� ��������� �������� ����, �/���.�');
      _5      := AsFloat(vRRL.RRV_5);   // 'Q-������ ������ �����������');
      _6:= AsFloat(vRRL.RRV_6);   //'������������� ������ K��, 10^-6');
      _7      := AsFloat(vRRL.RRV_7);   // '�������� ������������ b ��� ������� ������');
      _8      := AsFloat(vRRL.RRV_8);   // '�������� ������������ c ��� ������� ������');
      FACTOR_D_9      := AsFloat(vRRL.RRV_9);   // '�������� ������������ d ��� ������� ������');
      _10    := AsFloat(vRRL.RRV_10);   // '������������� ����� � ������� 0.01% �������, ��/���');
      _11   := AsFloat(vRRL.RRV_11);       //'�����. ��������� (��� ��.���������=0, ����� �.�.=0)');

      _14 := AsFloat(vRRL.RRV_12);   //'��� ���������.(1...3,0->����.�����=����.���.)' );
    end;

*)
(*

    //---------------------------
    with oXLink.DLT do begin
    //---------------------------
//      Calc_Method : Integer; // 0 "����, ITU-R � 16 ����� ��" (�� ���������)
	//			                     // 1 "���� � 53363 - 2009"

          := AsFloat(vRRL.DLT_1); //'��� ��������� �������, �� ( 0 - ��������� ����)');
         := AsFloat(vRRL.DLT_2);  //'�������� ������ ������� V����(g)=V���.�, ��');
       := AsFloat(vRRL.DLT_3);    //'�������� ������ ������� g(V����)=g(V���.�), 10^-8');
        := AsFloat(vRRL.DLT_4);   //'������������ �������� ��� ������� ������������, 10^-8');
    end;

    //---------------------------
    with oXLink.TRB do begin
    //---------------------------
      GST_SESR_1               := AsFloat(vRRL.TRB_1); // '����������� �������� SESR ��� ����, %');
      GST_Kng_2                := AsFloat(vRRL.TRB_2); // '����������� �������� ���  ��� ����, %');
      GST_Length_KM_3          := AsFloat(vRRL.TRB_3); // '����� ����, ��');
      KNG_dop_4                := AsFloat(vRRL.TRB_4); // '���������� ����� ���, ������������� �������������}');
      SPACE_LIMIT_5            := AsFloat(vRRL.TRB_5); // '���������� ������������� ������� p(a%)');
      SPACE_LIMIT_PROBABILITY_6:= AsFloat(vRRL.TRB_6); // '���������� ����������� a% ������������� ������������ �������� p(a%) [%]');
    end;

    //---------------------------
    with oXLink.TRB do begin
    //---------------------------
      GST_SESR_1               := AsFloat(vRRL.); // '����������� �������� SESR ��� ����, %');
      _2                := AsFloat(vRRL.); // '����������� �������� ���  ��� ����, %');
      GST__3          := AsFloat(vRRL.TRB_3); // '����� ����, ��');
      KNG_dop_4                := AsFloat(vRRL.TRB_4); // '���������� ����� ���, ������������� �������������}');
      _5            := AsFloat(vRRL.TRB_5); // '���������� ������������� ������� p(a%)');
      := AsFloat(vRRL.TRB_6); // '���������� ����������� a% ������������� ������������ �������� p(a%) [%]');
    end;
*)

  //  oXLink.LinkEnd1. :=AsFloat(vRRL.);
  //  oXLink.LinkEnd1. :=AsFloat(vRRL.Site2_Sensitivity);



      ]);

(*

    if AsInteger(vRRL.PolarizationType)=0 then
      sPolarization :='h'
    else
      sPolarization :='v';

    oXLink.Refraction := AsFloat( vRRL.Refraction);


    oXLink.LinkEnd1.   := AsInteger( vRRL.);
    oXLink.LinkEnd1. := AsInteger( vRRL.);
    oXLink.LinkEnd1.:= AsInteger( vRRL.);


    oXLink.LinkEnd2.Bitrate_Mbps   := oXLink.LinkEnd1.Bitrate_Mbps;
    oXLink.LinkEnd2.KratnostByFreq := oXLink.LinkEnd1.KratnostByFreq ;
    oXLink.LinkEnd2.kratnostBySpace:= oXLink.LinkEnd1.kratnostBySpace;


    //---------------------------
    with oXLink.TRB do begin
    //---------------------------
      GST_SESR_1               := AsFloat(vRRL.TRB_1); // '����������� �������� SESR ��� ����, %');
      GST_Kng_2                := AsFloat(vRRL.TRB_2); // '����������� �������� ���  ��� ����, %');
      GST_Length_KM_3          := AsFloat(vRRL.TRB_3); // '����� ����, ��');
      KNG_dop_4                := AsFloat(vRRL.TRB_4); // '���������� ����� ���, ������������� �������������}');
      SPACE_LIMIT_5            := AsFloat(vRRL.TRB_5); // '���������� ������������� ������� p(a%)');
      SPACE_LIMIT_PROBABILITY_6:= AsFloat(vRRL.TRB_6); // '���������� ����������� a% ������������� ������������ �������� p(a%) [%]');
    end;

    //---------------------------
    with oXLink.DLT do begin
    //---------------------------
//      Calc_Method : Integer; // 0 "����, ITU-R � 16 ����� ��" (�� ���������)
	//			                     // 1 "���� � 53363 - 2009"

      Profile_Step_KM_1           := AsFloat(vRRL.DLT_1);//'��� ��������� �������, �� ( 0 - ��������� ����)');
      precision_V_diffraction_2   := AsFloat(vRRL.DLT_2);//'�������� ������ ������� V����(g)=V���.�, ��');
      precision_g_V_diffraction_3 := AsFloat(vRRL.DLT_3);//'�������� ������ ������� g(V����)=g(V���.�), 10^-8');
      max_gradient_for_subrefr_4  := AsFloat(vRRL.DLT_4);//'������������ �������� ��� ������� ������������, 10^-8');
    end;

    //-------------------------------------------------------------------
    with oXLink.RRV do begin  //������� ��������������� ��������� (���)
    //-------------------------------------------------------------------

      gradient_diel_1 := AsFloat(vRRL.RRV_1);   // '������� �������� ��������� ����.�������������,10^-8 1/�' );
      gradient_2      := AsFloat(vRRL.RRV_2);   // '����������� ���������� ���������,10^-8 1/�' );

      underlying_terrain_type_3:= AsFloat(vRRL.RRV_3); //'��� ������������ �������.(1...3,0->����.�����=����.���.)' );

      steam_wet_4     := AsFloat(vRRL.RRV_4);   // '���������� ��������� �������� ����, �/���.�');
      Q_factor_5      := AsFloat(vRRL.RRV_5);   // 'Q-������ ������ �����������');
      climate_factor_6:= AsFloat(vRRL.RRV_6);   //'������������� ������ K��, 10^-6');
      factor_B_7      := AsFloat(vRRL.RRV_7);   // '�������� ������������ b ��� ������� ������');
      factor_C_8      := AsFloat(vRRL.RRV_8);   // '�������� ������������ c ��� ������� ������');
      FACTOR_D_9      := AsFloat(vRRL.RRV_9);   // '�������� ������������ d ��� ������� ������');
      RAIN_RATE_10    := AsFloat(vRRL.RRV_10);   // '������������� ����� � ������� 0.01% �������, ��/���');
      REFRACTION_11   := AsFloat(vRRL.RRV_11);       //'�����. ��������� (��� ��.���������=0, ����� �.�.=0)');

      terrain_type_14 := AsFloat(vRRL.RRV_12);   //'��� ���������.(1...3,0->����.�����=����.���.)' );
    end;



    oXLink.LinkEnd1.Rx_Level_dBm :=oXLink.Rx_Level_dBm;
    oXLink.LinkEnd2.Rx_Level_dBm :=oXLink.Rx_Level_dBm;

//aIXMLRRLsType[i].NFrenel



    oXLink.LinkEnd1.Power_dBm :=AsFloat(vRRL.Site1_Power);
    oXLink.LinkEnd1.THRESHOLD_dBm :=AsFloat(vRRL.Site2_Sensitivity);

    oXLink.LinkEnd2.Power_dBm :=oXLink.LinkEnd1.Power_dBm;
    oXLink.LinkEnd2.THRESHOLD_dBm :=oXLink.LinkEnd1.THRESHOLD_dBm;


*)
end;


//--------------------------------------------------------------
procedure TdmLink_export_RPLS_XML.Export_Property(aParent_XMLNode: IXMLNode);
//--------------------------------------------------------------
var
  I: Integer;
  sSQL: string;
begin
  for I := 0 to FIDList.Count - 1 do
  begin
    sSQL:=Format('SELECT * from %s WHERE id=%d', [TBL_PROPERTY, FIDList[i].ID]);
    db_OpenQuery(qry_Prop, sSQL);

    if not qry_Prop.IsEmpty then
      ExportSiteToXMLNode (qry_Prop, aParent_XMLNode);

  end;
end;

// ---------------------------------------------------------------
procedure TdmLink_export_RPLS_XML.OpenData;
// ---------------------------------------------------------------
var
  sSQL: string;
begin
  Assert(dmMain.ProjectID>0, 'Value <=0');
  // -------------------------

//  sSQL:=Format('SELECT * from %s WHERE project_id=%d', [VIEW_LINK, dmMain.ProjectID]);
  sSQL:=Format('SELECT * from %s WHERE project_id=%d', [TBL_LINK, dmMain.ProjectID]);
  db_OpenQuery(qry_Links, sSQL);

//  db_View(qry_Links);

  {
  sSQL:=Format('SELECT * from %s WHERE id=:LINKEND1_id', [TBL_LINKEND]);
  db_OpenQuery(qry_LinkEnd1, sSQL);

  sSQL:=Format('SELECT * from %s WHERE id=:LINKEND2_id', [TBL_LINKEND]);
  db_OpenQuery(qry_LinkEnd2, sSQL);
//  qry_prop2.Clone(qry_prop1);


  sSQL:=Format('SELECT * from %s WHERE id=:PROPERTY_id', [TBL_PROPERTY]);
  db_OpenQuery(qry_Prop1, sSQL);

  sSQL:=Format('SELECT * from %s WHERE id=:PROPERTY_id', [TBL_PROPERTY]);
  db_OpenQuery(qry_Prop2, sSQL);


  sSQL:=Format('SELECT * from %s WHERE LINKEND_id=:id', [TBL_LINKEND_ANTENNA]);
  db_OpenQuery(qry_Antenna1, sSQL);

  sSQL:=Format('SELECT * from %s WHERE LINKEND_id=:id', [TBL_LINKEND_ANTENNA]);
  db_OpenQuery(qry_Antenna2, sSQL);


  db_View(qry_Antenna1);
  db_View(qry_Antenna2);
  }

  qry_LinkEnd1.Open;
  qry_LinkEnd2.Open;

  qry_Prop1.Open;
  qry_Prop2.Open;

  qry_Antenna1.Open;
  qry_Antenna2.Open;


{
  db_View(qry_Prop1);
  db_View(qry_Prop2);


  db_View(qry_Antenna1);
  db_View(qry_Antenna2);
 }

//  sSQL:=Format('SELECT * from %s WHERE project_id=%d ORDER BY height ', [VIEW_LINKEND_ANTENNA_with_PROJECT_ID, dmMain.ProjectID]);
//  db_OpenQuery(qry_Antenna1, sSQL);

//  qry_Antenna2.Clone(qry_Antenna1);


end;     


begin
end.




