unit dm_MDB;

interface

uses
  Classes, Forms,


  u_const_db,

  u_db,
  u_db_mdb, DB, ADODB

  ;

type
  TdmMDB = class(TDataModule)
    ds_PROPERTY: TDataSource;
    ADOConnection_MDB: TADOConnection;
    t_PROPERTY: TADOTable;
    procedure DataModuleCreate(Sender: TObject);
  private
    procedure AddRecord(aDataset: TDataset; aParams: array of Variant);
    procedure CreateFile(aFileName: string);

  public
    procedure Init;
 
  end;

var
  dmMDB: TdmMDB;

implementation

{$R *.dfm}


// -------------------------------------------------------------------
procedure TdmMDB.Init;
// -------------------------------------------------------------------
begin
 if not Assigned(dmMDB) then
   dmMDB := TdmMDB.Create(Application);

//  Result := FdmLink_export;
end;


procedure TdmMDB.DataModuleCreate(Sender: TObject);
begin
//   CreateFile;
end;



procedure TdmMDB.AddRecord(aDataset: TDataset; aParams: array of Variant);
begin
  db_AddRecord_(aDataset, aParams);
end;


// ---------------------------------------------------------------
procedure TdmMDB.CreateFile(aFileName: string);
// ---------------------------------------------------------------
begin
  mdb_CreateFileAndOpen ('f:\test.mdb', ADOConnection_MDB);


  mdb_CreateTableFromArr(ADOConnection_MDB, TBL_PROPERTY,
    [
      db_Field(FLD_id, ftInteger),

      db_Field(FLD_name,    ftString, 200),
      db_Field(FLD_address, ftString, 200),

      db_Field(FLD_LAT,ftFloat),
      db_Field(FLD_LON,ftFloat) ,

      db_Field(FLD_Tower_height, ftFloat)
    ]);

{

    Result:=xml_AddNode(aSites_XMLNode, 'site',
                [xml_Par('id',      FieldByName(FLD_ID).AsInteger),
                 xml_Par('name',    FieldByName(FLD_NAME).AsString),
              //   xml_Par('number',  iNumber),
                 xml_Par('address', FieldByName(FLD_address).AsString),
                 xml_Par('b',       FieldByName(FLD_LAT).AsFloat),
                 xml_Par('l',       FieldByName(FLD_LON).AsFloat)
                ]);


  mdb_CreateTableFromArr(ADOConnection_MDB, 'Profile_items',
    [
      db_Field(FLD_link_id, ftInteger),

      db_Field(FLD_INDEX, ftInteger),

      db_Field(FLD_distance_km, ftFloat),

      db_Field(FLD_ground_h, ftFloat),
      db_Field(FLD_clutter_h, ftInteger),
      db_Field(FLD_clutter_code, ftInteger),

      db_Field(FLD_clutter_name, ftString, 200),
      db_Field(FLD_clutter_color, ftInteger),

      db_Field(FLD_earth_h, ftFloat)

    ]);
}

//  db_SetADOConnection(dmMain.ADOConnection);

end;


end.

