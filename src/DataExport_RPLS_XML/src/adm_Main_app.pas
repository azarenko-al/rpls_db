unit adm_Main_app;

interface

uses
  Classes, Dialogs,  SysUtils,

  u_ini_Data_Export_RPLS_XML_params,

  dm_Property_export_RPLS_XML,
  dm_Link_export_RPLS_XML,

 

  u_vars,
 
  u_func,

  dm_Main

  ;
  //dm_LinkLine_Complex_Report11,

type
  TdmMain_app = class(TDataModule)
    procedure DataModuleDestroy(Sender: TObject);
    procedure DataModuleCreate(Sender: TObject);
  public
    FParams: TIni_Data_Export_RPLS_XML_Param;
  //  procedure UpdateReports;
  end;

var
  dmMain_app: TdmMain_app;


implementation
 {$R *.DFM}



procedure TdmMain_app.DataModuleDestroy(Sender: TObject);
begin
  FreeAndNil(FParams);
end;

//--------------------------------------------------------------
procedure TdmMain_app.DataModuleCreate(Sender: TObject);
//--------------------------------------------------------------
const
  DEF_TEMP_FILENAME = 'project_export_RPLS_XML.ini';

//  REGISTRY_ROOT = 'Software\Onega\RPLS_DB\';



var
 // rec: TReportParamRec;

  sIniFileName: string;
  sMDB_FileName: string;
  sReportFileName: string;
 // sReportFileName: string;
  //sScale: string;
//  iID: integer;

begin
  sIniFileName:=ParamStr(1);
  if sIniFileName='' then
    sIniFileName:=g_ApplicationDataDir+ DEF_TEMP_FILENAME;

  if not FileExists(sIniFileName) then
  begin
    ShowMessage(sIniFileName);
    Exit;
  end;


 // ShellExec_Notepad(sIniFileName);


 //    sIniFileName:=GetCommonApplicationDataDir_Local_AppData(DEF_SOFTWARE_NAME)+
  sMDB_FileName:=g_ApplicationDataDir + 'import.mdb';

//  TdmMDB.Init;



  FParams := TIni_Data_Export_RPLS_XML_Param.Create();
  FParams.LoadFromINI(sIniFileName);

  // -------------------------------------------------------------------

  TdmMain.Init;
  if not dmMain.OpenDB_reg then    // True
    Exit;

  dmMain.ProjectID:=FParams.ProjectID;

  // -------------------------
  if Eq(FParams.ObjName, 'property') then
  // -------------------------
  begin
    if Eq(FParams.Action,'import') then
      dmProperty_export_RPLS_XML.Dlg_ImportFrom_RPLS_SITES_XML else //Dlg_ExportToRPLS_XML_Folder(FParams.FolderID)

    if FParams.FolderID>0 then
      dmProperty_export_RPLS_XML.Dlg_ExportToRPLS_XML_Folder(FParams.FolderID)
    else
      dmProperty_export_RPLS_XML.Dlg_ExportToRPLS_XML(FParams.IDList);
  end else

  // -------------------------
  if Eq(FParams.ObjName, 'link') then
  // -------------------------
  begin
   if FParams.FolderID>0 then
      dmLink_export_RPLS_XML.Dlg_ExportToRPLS_XML_Folder(FParams.FolderID)
    else
      dmLink_export_RPLS_XML.Dlg_ExportToRPLS_XML(FParams.IDList);

 end;

end;


end.



