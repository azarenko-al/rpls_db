unit u_ini_Data_Export_RPLS_XML_params;

interface
uses
  IniFiles, SysUtils,Dialogs,

  u_func,
  u_classes
  ;

type
  // ---------------------------------------------------------------
  TIni_Data_Export_RPLS_XML_Param = class
  // ---------------------------------------------------------------
  public
    ProjectID  : Integer;
    FolderID  : integer;
  //  FileDir          : string;

  //  ID        : integer;
    ObjName   : string;
    Action   : string;

    IDList: TIDList;


    constructor Create;
    destructor Destroy; override;

    procedure LoadFromINI(aFileName: string);
    procedure SaveToINI(aFileName: string);


  end;


implementation


constructor TIni_Data_Export_RPLS_XML_Param.Create;
begin
  inherited Create;
  IDList := TIDList.Create();

end;

destructor TIni_Data_Export_RPLS_XML_Param.Destroy;
begin
  FreeAndNil(IDList);

  inherited Destroy;
end;

//--------------------------------------------------------------
procedure TIni_Data_Export_RPLS_XML_Param.LoadFromINI(aFileName: string);
//--------------------------------------------------------------
var
  oIniFile: TIniFile;
  sScale: string;

begin
  if not FileExists(aFileName) then
    Exit;

  oIniFile := TIniFile.Create(aFileName);

  IDList.LoadIDsFromIniSection(oIniFile, 'items');

  with oIniFile do
  begin
    ProjectID        :=ReadInteger('main', 'ProjectID',   0);
    ObjName          :=ReadString ('main', 'ObjName', '');
    Action           :=ReadString ('main', 'Action', '');

    FolderID         :=ReadInteger('main', 'FolderID',   0);

  end;

  FreeAndNil(oIniFile);

end;

// ---------------------------------------------------------------
procedure TIni_Data_Export_RPLS_XML_Param.SaveToINI(aFileName: string);
// ---------------------------------------------------------------
var
  oInifile: TIniFile;
begin
  oInifile:=TIniFile.Create (aFileName);

  with oInifile do
  begin
    WriteInteger('main', 'ProjectID',    ProjectID);

    WriteInteger('main', 'FolderID',    FolderID);

    WriteString ('main', 'ObjName',      ObjName);
    WriteString ('main', 'Action',      Action);

         //      :=ReadString ('main', 'Action', '');

  end;

  FreeAndNil(oInifile);

  IDList.SaveIDsToIniFile(aFileName, 'items');

//  oInifile.Free;
end;


end.



(*
var
  gParams: record
    //------------------------------
    //map
    //------------------------------
    MapDesktopID: integer;
    Scale: integer;

    ImageWidth: integer;
    ImageHeight: integer;

    IsSaveDocsToDB: Boolean;
    IsShowReportDoc: Boolean;

    Profile: record
      IsShowFrenelZoneBottomLeftRight: boolean;
    end;
  end;*)

