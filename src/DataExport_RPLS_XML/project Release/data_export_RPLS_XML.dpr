program data_export_RPLS_XML;



uses
  adm_Main_app in '..\src\adm_Main_app.pas' {dmMain_app: TDataModule},
  dm_Link_export_RPLS_XML in '..\src\dm_Link_export_RPLS_XML.pas' {dmLink_export_RPLS_XML: TDataModule},
  dm_Property_export_RPLS_XML in '..\src\dm_Property_export_RPLS_XML.pas' {dmProperty_export_RPLS_XML: TDataModule},
  Forms,
  u_DataExport_RPLS_XML_run in '..\..\DataExport\u_DataExport_RPLS_XML_run.pas',
  u_ini_data_export_import_params in '..\..\..\..\RPLS_DB EXPORT-IMPORT\DataExportImport\src shared\u_ini_data_export_import_params.pas',
  u_import_classes in '..\src\u_import_classes.pas',
  dm_MDB in '..\src\dm_MDB.pas' {dmMDB: TDataModule},
  Unit1 in '..\src\Unit1.pas' {Form1};

{$R *.RES}


begin
  Application.Initialize;
  Application.CreateForm(TdmMain_app, dmMain_app);
  Application.Run;
end.
