program dll_Link_report;

{%File '..\..\..\mapx.inc'}

uses
  adm_Main_report_app in '..\src\adm_Main_report_app.pas' {dmMain_app__report: TDataModule},
  dm_Link_map in '..\src\dm_Link_map.pas' {dmLink_map: TDataModule},
  dm_Link_report in '..\src\dm_Link_report.pas' {dmLink_report: TDataModule},
  dm_Link_report_data in '..\src\dm_Link_report_data.pas' {dmLink_report_data: TDataModule},
  dm_Link_report_XML in '..\src\dm_Link_report_XML.pas' {dmLink_report_XML: TDataModule},
  dm_LinkLine_report in '..\src\dm_LinkLine_report.pas' {dmLinkLine_report: TDataModule},
  Forms,
  u_Link_report_params in '..\src\u_Link_report_params.pas',
  u_Map_engine in '..\..\Map\u_Map_engine.pas';

{$R *.RES}


begin
  Application.Initialize;
  Application.CreateForm(TdmMain_app__report, dmMain_app__report);
  Application.Run;
end.
