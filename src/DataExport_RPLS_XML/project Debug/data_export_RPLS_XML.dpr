program data_export_RPLS_XML;



uses
  adm_Main_app in '..\src\adm_Main_app.pas' {dmMain_app: TDataModule},
  dm_Link_export_RPLS_XML in '..\src\dm_Link_export_RPLS_XML.pas' {dmLink_export_RPLS_XML: TDataModule},
  dm_Property_export_RPLS_XML in '..\src\dm_Property_export_RPLS_XML.pas' {dmProperty_export_RPLS_XML: TDataModule},
  Forms,
  u_ini_Data_Export_RPLS_XML_params in '..\src_shared\u_ini_Data_Export_RPLS_XML_params.pas',
  u_DataExport_RPLS_XML_run in '..\..\DataExport\u_DataExport_RPLS_XML_run.pas',
  u_xml_Sites in '..\src\u_xml_Sites.pas';

{$R *.RES}


begin
  Application.Initialize;
  Application.CreateForm(TdmMain_app, dmMain_app);
  Application.Run;
end.
