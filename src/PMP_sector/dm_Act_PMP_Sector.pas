unit dm_Act_PMP_Sector;

interface

uses
  Classes, Forms,  Dialogs,  Menus,

 // I_Act_PMP_Sector,
 // I_Act_Antenna,


  dm_MapEngine_store,

  I_Object,


  dm_act_Base,


  dm_PMP_Sector,


  fr_PMP_Sector_view,

  d_LinkEnd_inspector,

  d_LinkEnd_Add,

  u_func_msg,
  
  u_func,
  

  u_classes,
  u_Types,
  
  
  

  ActnList
  ;


type

  TdmAct_PMP_Sector = class(TdmAct_Base)
    act_Antenna_Add: TAction;
    ColorDialog1: TColorDialog;
    act_Change_color: TAction;
  //  procedure DataModuleDestroy(Sender: TObject);
//    procedure DataModuleDestroy(Sender: TObject);
    procedure DataModuleCreate(Sender: TObject);
  //  procedure ApplicationEvents1Message(var Msg: tagMSG; var Handled: Boolean);
  private
    FPmpSiteID: integer;

    procedure AntennaAdd(aID: integer);
    procedure DoAction (Sender: TObject);
    function ItemDel(aID: integer ; aName: string = ''): boolean; override;


  protected
    procedure GetPopupMenu (aPopupMenu: TPopupMenu; aMenuType: TMenuType); override;
    function  GetViewForm (aOwnerForm: TForm): TForm; override;

  public
// TODO: Dlg_AddPmpTerminal
//  procedure Dlg_AddPmpTerminal(aID: integer);

    function Dlg_Add(aSiteID: integer): integer; overload;
    function Dlg_Edit(aID: integer): Boolean;

    function Dlg_Del(aID: integer): boolean;

//    function Validate: Boolean;

    class procedure Init;

  end;


var
  dmAct_PMP_Sector: TdmAct_PMP_Sector;

//====================================================================
// implementation
//====================================================================
implementation
 {$R *.dfm}

uses 
     dm_act_Antenna;


//
//procedure TdmAct_PMP_Sector.DataModuleDestroy(Sender: TObject);
//begin
// // IAct_PMP_Sector := nil;
//
//  inherited;
//end;


//--------------------------------------------------------------------
class procedure TdmAct_PMP_Sector.Init;
//--------------------------------------------------------------------
begin
  if not Assigned(dmAct_PMP_Sector) then
  begin
    dmAct_PMP_Sector:=TdmAct_PMP_Sector.Create(Application);

   // dmAct_PMP_Sector.GetInterface(IAct_PMP_Sector_X, IAct_PMP_Sector);
   // Assert(Assigned(IAct_PMP_Sector));

  end;

end;


//-------------------------------------------------------------------
procedure TdmAct_PMP_Sector.DataModuleCreate(Sender: TObject);
//-------------------------------------------------------------------
begin
  inherited;

  ObjectName:=OBJ_PMP_Sector;

  act_Antenna_Add.Caption :='�������� �������';
//  act_Change_color.Caption      :='�������� ����';

  SetActionsExecuteProc ([act_Antenna_Add
//                          act_Change_color,
                          ],
                          DoAction);

  SetCheckedActionsArr([
     act_Del_,
     act_Del_list,
     act_Antenna_Add
    ]);


end;


//-------------------------------------------------------------------
procedure TdmAct_PMP_Sector.DoAction (Sender: TObject);
//-------------------------------------------------------------------
begin
//  if Sender=act_Del_ then Del (FFocusedID) else
  if Sender=act_Antenna_Add  then
    AntennaAdd (FFocusedID) else

end;


//--------------------------------------------------------------------
function TdmAct_PMP_Sector.Dlg_Edit(aID: integer): boolean;
//--------------------------------------------------------------------
begin
  Result:= Tdlg_LinkEnd_inspector.ExecDlg1 (aID, OBJ_PMP_SECTOR);
//  Result:= Tframe_LinkEnd_inspector.ExecDlg1 (aID, OBJ_PMP_SECTOR);
end;



//--------------------------------------------------------------------
procedure TdmAct_PMP_Sector.AntennaAdd(aID: integer);
//--------------------------------------------------------------------
begin
 // Assert(Assigned(IAntenna));

  if dmAct_Antenna.Dlg_Add_LinkEnd_Antenna (aID)>0 then
//  if dmAct_Antenna.Dlg_AddPmpAntenna (aID)>0 then
//  PostWMsg (WM_PMPSECTOR_ANTENNA_ADD,     aID);
//  PostEvent (WE_PMPSECTOR_ANTENNA_ADD, PAR_NAME, AsString(aID));

    g_EventManager.postEvent_ (WM_PMPSECTOR_REFRESH, ['ID', aID]);

 //   PostWMsg (WM_PMPSECTOR_REFRESH,     aID);
end;

//--------------------------------------------------------------------
function TdmAct_PMP_Sector.ItemDel(aID: integer ; aName: string = ''): boolean;
//--------------------------------------------------------------------
var
  oList: TIDList;
  i: integer;
begin
//  FIDList

 // oList:=TIDList.Create;
  { TODO : !!!!!!!!! }

{
  dmPmp_Sector.GetTerminalIDList(aID, oList);
  for i:=0 to oList.Count-1 do
  begin
    dmMapEngine1.DelObject(otPmpTerminal, oList[i].ID);
  //  dmMapEngine1.Sync (otPmpTerminal);
  end;}

//  dmMapEngine1.DelObject(otPmpSector, aID);
//  dmMapEngine1.Sync (otPmpSectorAnt);

//  PostEvent(WE_MAP_ENGINE_SYNC_OBJECT, Integer(otCellAnt));

{  Result:=dmPMP_Sector.Del (aID);

  dmAct_Map.MapRefresh;

  oList.Free;

}


//  FIDList.Clear;
 ///// dmPMP_Sector.GetAntennaIDList(aID, FDeletedIDList);

///  dmAntenna.GetAntennaIDList (aID, OBJ_PMP_SECTOR, FIDList);
 // oAntArray:= dmCell.GetAnt IDArray(aID);

  Result:=dmPMP_Sector.Del (aID);

  if Result then
  begin
    dmMapEngine_store.Feature_Del (OBJ_Pmp_Sector, aID);

///    PostEvent (WE_MAP_ENGINE_DEL_OBJECT, Integer(otCalcRegion), aID);

  //  dmAct_Map.MapRefresh;
  end;

{
  if Result then
    dmMapEngine1.DelList (FIDList);

  //  for i := 0 to FIDList.Count-1 do
    //  dmMapEngine1.DelObject (otPmpSectorAnt, FIDList[i].ID);

  dmAct_Map.MapRefresh;
}


end;


//--------------------------------------------------------------------
function TdmAct_PMP_Sector.Dlg_Add(aSiteID: integer): integer;
//--------------------------------------------------------------------
begin
  Result:=Tdlg_LinkEnd_add.ExecDlg_PmpSector (aSiteID);

 { 
  if Result>0 then
  begin
   // dmMapEngine1.CreateObject (otPmpSector, Result);
   // dmMapEngine1.Sync (otPmpSectorAnt);

    g_ShellEvents.Shell_UpdateNodeChildren_ByGUID (dmPmpSite.GetGUIDByID(aSiteID));
                
    PostMessage(Application.Handle, WM_REFRESH_SITE, 0, 0);
  //  dmAct_Map.MapRefresh;
  end;
  }


end;


//--------------------------------------------------------------------
function TdmAct_PMP_Sector.Dlg_Del(aID: integer): boolean;
//--------------------------------------------------------------------
begin
  Result:=False;

  if ConfirmDlg('������� ?') then
  begin
    Result:=dmPMP_Sector.Del (aID);

  end;

end;



//--------------------------------------------------------------------
function TdmAct_PMP_Sector.GetViewForm (aOwnerForm: TForm): TForm;
//--------------------------------------------------------------------
begin
  Result:=Tframe_PMP_Sector_View.Create(aOwnerForm);
end;


//--------------------------------------------------------------------
procedure TdmAct_PMP_Sector.GetPopupMenu;
//--------------------------------------------------------------------
begin
  case aMenuType of
    mtItem: begin
              AddMenuItem(aPopupMenu, act_Antenna_Add);
           //   AddMenuItem(aPopupMenu, act_Add_PmpTerminal);
           //   AddMenuItem(aPopupMenu, act_Change_color);
              AddMenuItem(aPopupMenu, nil);
              AddMenuItem(aPopupMenu, act_Del_);
            end;
    mtList: AddMenuItem(aPopupMenu, act_Del_list);
  end;
end;

 
end.

