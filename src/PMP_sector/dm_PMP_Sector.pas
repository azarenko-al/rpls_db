unit dm_PMP_Sector;

interface
{$I ver.inc}

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,

  dm_Onega_DB_data,

  I_CalcModel,
  dm_Main,

  u_const_str,
  u_const_db,

   u_types,

  u_GEO,
  u_func,
  u_db,
  u_img,
  u_classes,

  dm_LinkEnd,
  dm_LinkEndType,

  dm_PmpTerminal,

  dm_Object_base,
  dm_Antenna,

//  dm_Antenna_tools,


  Db, ADODB
  ;

type

  TdmPMP_Sector = class(TdmObject_base)
    procedure DataModuleCreate(Sender: TObject);
  private
  public
    function Del (aID: integer): boolean; override;
    function GetNewName(aPmpSiteID: integer): string;


  end;



function dmPMP_Sector: TdmPMP_Sector;


//==================================================================
implementation {$R *.dfm}
//==================================================================

var
  FdmPMP_Sector: TdmPMP_Sector;


// ---------------------------------------------------------------
function dmPMP_Sector: TdmPMP_Sector;
// ---------------------------------------------------------------
begin
  if not Assigned(FdmPMP_Sector) then
      FdmPMP_Sector := TdmPMP_Sector.Create(Application);

  Result := FdmPMP_Sector;
end;


procedure TdmPMP_Sector.DataModuleCreate(Sender: TObject);
begin
  inherited;
//  TableName:=TBL_PMP_Sector;
  DisplayName:='PMP ������';
end;



function TdmPMP_Sector.GetNewName(aPmpSiteID: integer): string;
var
  rec: TObject_GetNewName_Params;
begin
  FillChar(rec,SizeOf(rec),0);
  rec.ObjName := OBJ_PMP_SECTOR;
  rec.Project_ID := dmMain.ProjectID;
  rec.Parent_ID  := aPmpSiteID;       

  Result := dmOnega_DB_data.Object_GetNewName_new
     (rec, OBJ_PMP_SECTOR, dmMain.ProjectID, 0, aPmpSiteID);
end;


  //  ed_Name_.Text:=dmPMP_Sector.GetNewName_For_Cell1(FRec.PmpSector.PmpSite_ID);



//-------------------------------------------------------------------
function TdmPMP_Sector.Del (aID: integer): boolean;
//-------------------------------------------------------------------
//var
  //iRes: Integer;
begin
{ TODO : !!!!!!!!!!! }

//  dmAntenna_tools.DelByOwnerID (aID, OBJ_PMP_SECTOR);

  Result:=dmOnega_DB_data.Pmp_Sector_del(aID)>0;

//  Result:=True;

end;

begin

end.
