unit fr_PMP_Sector_view;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
  Dialogs, ActnList, Menus,  ToolWin, ComCtrls, ExtCtrls, RXSplit,
  DBCtrls, StdCtrls,   ImgList,  cxControls, cxSplitter,

  dm_User_Security,

  fr_View_base,
  
  fr_LinkEnd_inspector,
  fr_LinkEnd_antennas,


  u_func,
  u_reg,
  u_const_db,

  u_types, cxPropertiesStore, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, dxSkinsCore, dxSkinsDefaultPainters, dxBar,
  cxBarEditItem, cxClasses
  ;

type
  Tframe_PMP_Sector_View = class(Tframe_View_Base)
    pn_Inspector: TPanel;
    cxSplitter1: TcxSplitter;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    procedure FormCreate(Sender: TObject);
  private
    Ffrm_LinkEnd_inspector: Tframe_LinkEnd_inspector;
    Ffrm_antennas: Tframe_LinkEnd_antennas;

  public
    procedure View(aID: integer; aGUID: string); override;
  end;



//==================================================================
//==================================================================
implementation
{$R *.DFM}


//--------------------------------------------------------------------
procedure Tframe_PMP_Sector_View.FormCreate(Sender: TObject);
//--------------------------------------------------------------------
begin
  inherited;

  ObjectName:=OBJ_PMP_SECTOR;

//  TableName:=TBL_PMP_SECTOR;


  pn_Inspector.Align:=alClient;

  CreateChildForm(Tframe_LinkEnd_inspector,  Ffrm_LinkEnd_inspector, pn_Inspector);
  CreateChildForm(Tframe_LinkEnd_antennas,   Ffrm_antennas,  TabSheet1);

{  AddComponentProps(GSPages1, [PROP_HEIGHT,PROP_ACTIVE_PAGE]);
  AddComponentProp(GSPages1, PROP_ACTIVE_PAGE_INDEX);
  cxPropertiesStore.RestoreFrom;
}
end;


//-------------------------------------------------------------------
procedure Tframe_PMP_Sector_View.View(aID: integer; aGUID: string);
//-------------------------------------------------------------------
//var
//  bReadOnly: Boolean;
 // iGeoRegion_ID: Integer;
begin
  inherited;

//  if not RecordExists (aID) then
//    Exit;



  Ffrm_LinkEnd_inspector.View (aID, OBJ_PMP_SECTOR);

  // -------------------------
//  FGeoRegion_ID := Ffrm_LinkEnd_inspector.GetDataset().FieldByName(FLD_GeoRegion_ID).AsInteger;// GetIntFieldValue(FLD_GeoRegion_ID);
//  bReadOnly:=dmUser_Security.GeoRegion_is_ReadOnly(FGeoRegion_ID);

//  Ffrm_LinkEnd_inspector.SetReadOnly(bReadOnly, IntToStr(FGeoRegion_ID));
//  Ffrm_antennas.SetReadOnly(bReadOnly);
  // -------------------------


  Ffrm_antennas.View (aID, OBJ_PMP_SECTOR);
end;


end.