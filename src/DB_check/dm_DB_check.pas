unit dm_DB_check;

interface

uses
  SysUtils, Classes, Forms, dialogs, DB, ADODB, IniFiles,

  u_Ini_Data_export_import_params,

  

  u_reg,

  u_log,

  u_vars,
  u_files,

  u_const_db,

  u_ini_install_db,

  u_func,
  u_db,

  dm_Main
  ;

type
  TdmDB_check1 = class(TDataModule)
    ADOQuery1: TADOQuery;
    procedure DataModuleCreate(Sender: TObject);
  private
    function Run_install_db: Boolean;
    procedure Update_Library;

  public
    function Exec: Boolean;

    class procedure Init;  
  end;

var
  dmDB_check1: TdmDB_check1;

implementation

uses dm_Onega_db_data;
{$R *.dfm}


class procedure TdmDB_check1.Init;
begin
  if not Assigned(dmDB_check1) then
    dmDB_check1 := TdmDB_check1.Create(Application);

end;


procedure TdmDB_check1.DataModuleCreate(Sender: TObject);
begin
  db_SetComponentADOConnection(Self, dmMain.ADOConnection);

end;

// ---------------------------------------------------------------
function TdmDB_check1.Exec: Boolean;
// ---------------------------------------------------------------
const
  MSG_VERSION_ERROR =
     '������ ����������� ������ ���� ������. �� �� ����� ������� ����������?';

  MSG_VERSION_OLD =
     '������ ���� ������ ��������. ��������� ������� ����������?';

//  SQL_SEL_VER =
 //    'SELECT datetime_ FROM _db_version WHERE [type]=''db_version'' ';
  MSG_DB_UPDATE_ERROR =
    '��������� ����������� ������ ��� ���������� ���� ������. '+
    '��������� ����� �������� �� ���������. '+
    '��� ��������� ���������� �� ���������� ������ ���������� � �������������';

var

  i: Integer;

  sIniFileName_Ver: string;
  oSL: TStringList;
  s: string;

  oIni: TMemIniFile;

  sDB_VERSION: string;
  sIni_VERSION: string;
  sVERSION: string;


//  FLD_Checksum = 'Checksum';

begin
//  Update_Library();
/////////////////////////



  Result := False;

  sIniFileName_Ver:= GetApplicationDir + 'db_version.ini';

  if not FileExists(sIniFileName_Ver) then
  begin
    ShowMessage('���� ��� ����������� ������ ���� ������ �� ������: '+ sIniFileName_Ver);
    Exit;
  end;

  // -------------------------

  oIni:=TMemIniFile.Create (sIniFileName_Ver);
  sIni_VERSION := oIni.ReadString ('MAIN', 'VERSION', '');
  FreeAndNil(oIni);


  Assert(sIni_VERSION<>'', 'oIni:=TMemIniFile.Create (sIniFileName_Ver);');


  s:= Format('if object_id(''%s'') is not null '+
      ' select top 1 * from %s order by version desc ' + //DESC !!!!
      ' else   select null where 1<>1 ',
      [TBL__db_version_, TBL__db_version_]);


  dmOnega_DB_data.OpenQuery(ADOQuery1, s) ;


  sDB_VERSION :='';

  if ADOQuery1.RecordCount>0 then
    if Assigned(ADOQuery1.FindField(FLD_VERSION)) then
      sDB_VERSION :=ADOQuery1.FieldByName(FLD_VERSION).AsString;


  s:='' ;

  if (ADOQuery1.RecordCount=1) AND (sIni_VERSION<=sDB_VERSION) then
  begin
    g_log.Msg(Format('������ DB: %s', [sDB_VERSION]));

    Result := True;
    Exit;
  end;

  s:=
      Format('������: %s',   [dmMain.LoginRec.Server]) + CRLF+
      Format('���� ������: %s', [dmMain.LoginRec.Database]) + CRLF+
      '---------------------'+ CRLF;


  if ADOQuery1.RecordCount>0 then
  begin
    sDB_VERSION   :=Trim(ADOQuery1.FieldByName(FLD_VERSION).AsString);


    s:= s+
        Format('������ DB: %s',   [sDB_VERSION]) + CRLF+
        Format('������ ini: %s',  [sIni_VERSION]) + CRLF+
        '---------------------'+ CRLF+
        MSG_VERSION_OLD;

  end else
    s:= s+
        '������ �� ������ ������ ���.'+ CRLF+
        '---------------------'+ CRLF+
        MSG_VERSION_OLD;


//  MyMessageDlg('Hello World!'; mtInformation; [mbYes, mbNo];
//      ['Yessss','Noooo']; 'New MessageDlg Box'):


    if ConfirmDlg(s) then
    begin
      Result := Run_install_db();


   //   Update_Library();


     // if not Result then
      //  Exit;

  //    db_View(ADOQuery1);

      //Result:=
  //    ConfirmDlg(MSG_VERSION_ERROR);

    end else
      Result := True;

 // end;




(*
  if not db_OpenQuery(ADOQuery1, 'select object_id(''db_version'')') then
  begin
    db_View(ADOQuery1);

    //Result:=
    ConfirmDlg(MSG_VERSION_ERROR);
    Exit;
  end;
*)

end;


// ---------------------------------------------------------------
function TdmDB_check1.Run_install_db: Boolean;
// ---------------------------------------------------------------

const
  TEMP_INI_FILE = 'install_db.ini';
var
  b: Boolean;
  iCode: Integer;
  sFile: string;
  obj: TIni_Install_db;
  
begin
  ShowMessage ('1');


  sFile := g_ApplicationDataDir + TEMP_INI_FILE;


  obj := TIni_Install_db.Create;

  obj.Server   := dmMain.LoginRec.Server;
  obj.DataBase := dmMain.LoginRec.DataBase;
  obj.Login    := dmMain.LoginRec.Login;
  obj.Password := dmMain.LoginRec.Password;
  obj.IsUseWinAuth := dmMain.LoginRec.IsUseWinAuth;

  obj.Project_name := 'link';

  obj.IsAuto := True;

  obj.SaveToIni(sFile);
                                              
  FreeAndNil(obj);

  ShowMessage ('2');


 // CursorHourGlass;

//  s:='W:\RPLS_DB tools\_bin\install_db.exe'


//  RunApp ('W:\RPLS_DB tools\_bin\install_db.exe',  DoubleQuotedStr(sFile) );
//  RunApp ('W:\RPLS_DB tools\_bin\install_db.exe',  DoubleQuotedStr(sFile) );

//  Assert(FileExists(GetApplicationDir() + DEF_INSTALL_DB_EXE));

  AssertFileExists(GetApplicationDir() + DEF_INSTALL_DB_EXE);


  RunApp (GetApplicationDir() + DEF_INSTALL_DB_EXE,  DoubleQuotedStr(sFile), iCode );


  Result:=ini_ReadBool(sFile, 'result', 'value', False);


//  RunApp (GetApplicationDir() + DEF_INSTALL_DB_EXE,  DoubleQuotedStr(sFile) );

//  Result := True;

 // CursorDefault;

end;

// ---------------------------------------------------------------
procedure TdmDB_check1.Update_Library;
// ---------------------------------------------------------------
const
  DEF_library  = '_library';

var
  I: Integer;
  iCode: Integer;
  k: Integer;
  sDir: string;

  oIni: TIni_Data_export_import_params;
  sFile: string;


  oSList: TStringList;
begin
  oSList:=TStringList.Create;


  sDir := GetApplicationDir + DEF_library;


  k:= ScanDir1(sDir, '*.mdb', oSList);

  if oSList.Count>0 then
    if ConfirmDlg( '������� �����������: ' + CRLF +CRLF +
                   oSList.Text + CRLF +
                   '�������� ?') then
//  if oSList.Count>0 then
  begin
    sFile := GetTempFileNameWithExt('.ini');

 //   sFile := 'd:\1.ini';



    oIni:=TIni_Data_export_import_params.Create;

    //!!!!!!!!!!!!!!!
    oIni.IsAuto:=True;
    oIni.Mode:=mtImport_GUIDES;


    for I := 0 to oSList.Count - 1 do
    begin
      oIni.FileName_MDB := oSList[i];

      oIni.SaveToIni(sFile);

      RunApp (GetApplicationDir()+ PROG_data_export_import_exe, DoubleQuotedStr(sFile), iCode );

    end;


 //   CheckListBox1.Checked[i]:=True;

    FreeAndNil(oIni);

  end;

  DeleteFile(sFile);

end;



end.


(*
  with CreateMessageDialog('Is it DNA or Protein?', mtConfirmation,
    mbYesNoCancel) do
  try
    TButton(FindComponent('Yes')).Caption := 'DNA';
    TButton(FindComponent('No')).Caption := 'Protein';
    Position := poScreenCenter;
   // Result :=
    ShowModal;
  finally
    Free;
  end;

*)


(*



//------------------------------------------------------------------------------
function TdmMain.CheckDBVersion(): boolean;
//------------------------------------------------------------------------------
const
  MSG_VERSION_ERROR =
     '������ ����������� ������ ���� ������. �� �� ����� ������� ����������?';
  MSG_VERSION_OLD =
     '������ ���� ������ ��������. ��������� ������� ����������?';
  SQL_SEL_VER =
     'SELECT datetime_ FROM _db_version WHERE [type]=''db_version'' ';
  MSG_DB_UPDATE_ERROR =
    '��������� ����������� ������ ��� ���������� ���� ������. '+
    '��������� ����� �������� �� ���������. '+
    '��� ��������� ���������� �� ���������� ������ ���������� � �������������';
var
  sFileName_Ver: string;
  dMdbVer, dSqlVer: double;
begin
  sFileName_Ver:= GetApplicationDir + 'db_version.ini';
  Result:= True;

  if not FileExists(sFileName_Ver) then
  begin
    Result:= ConfirmDlg(MSG_VERSION_ERROR);
    exit;
  end else begin
    if not db_OpenQuery(qry_Temp, SQL_SEL_VER) then
    begin
      Result:= ConfirmDlg(MSG_VERSION_ERROR);
      exit;
    end else begin
      with TIniFile.Create(sFileName_Ver) do begin
        try
          dMdbVer:= AsFloat( ReadString('main', 'version', '0') );
        except
          Result:= ConfirmDlg(MSG_VERSION_ERROR);
//          dmMain.ErrorMsg:= '���������� ���������� ������ ����������';
//          Result:= false;
          Free;
          exit;
        end;
        Free;
      end;

      if not qry_Temp.IsEmpty then
      begin
        dSqlVer:= AsFloat ( qry_Temp['datetime_'] );
        if dSqlVer < dMdbVer then begin
          if ConfirmDlg(MSG_VERSION_OLD) then begin
            Result:= RunApp(ReplaceStr( GetApplicationDir, 'BIN', 'UTILS') + 'install_db.exe', 'AUTO');
            if not Result then begin
              ErrDlg(MSG_DB_UPDATE_ERROR);
              exit;
            end;
          end
          else
            Result:= true;

          exit;
        end else
          Result:= true;
      end
      else
        Result:= true;
    end;
  end;
end;
