unit Unit16;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs,

  u_ini_data_export_import_params, 

  u_files,


  StdCtrls, Mask, rxToolEdit, CheckLst
  ;

type
  TForm16 = class(TForm)
    CheckListBox1: TCheckListBox;
    Button1: TButton;
    DirectoryEdit1: TDirectoryEdit;
    GroupBox1: TGroupBox;
    Button2: TButton;
    Memo1: TMemo;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
  private
    { Private declarations }
  public
    class function ExecDlg: boolean;

  end;

var
  Form16: TForm16;

implementation

{$R *.dfm}

procedure TForm16.Button1Click(Sender: TObject);
var
  I: Integer;
  k: Integer;
  sDir: string;
begin
  sDir:= IncludeTrailingBackslash (  Trim(DirectoryEdit1.Text));

 // CheckListBox1.
   LockWindowUpdate(CheckListBox1.Handle);

  k:= ScanDir1(sDir, '*.mdb', CheckListBox1.Items);

  for I := 0 to CheckListBox1.Count - 1 do
    CheckListBox1.Checked[i]:=True;

   LockWindowUpdate(0);

(*

     LockWindowUpdate(PopupWindow.Handle);
  with ActiveProperties.Items do
  begin
    for I := Count - 1 downto 0 do
      if Pos(ATextToFind, Items[I].Description) = 0 then
         Delete(I);
  end;
  LockWindowUpdate(0);


*)
//

end;


procedure TForm16.Button2Click(Sender: TObject);
var
  I: Integer;
  oIni: TIni_Data_export_import_params;
  sFile: string;
begin
  sFile := GetTempFileNameWithExt('.mdb');
  // -------------------------


  oIni:=TIni_Data_export_import_params.Create;

//  oIni.IsAuto:=True;
  oIni.Mode:=mtImport_GUIDES;



  for I := 0 to CheckListBox1.Count - 1 do
  begin
    oIni.FileName_MDB := CheckListBox1.Items[i];

    oIni.SaveToIni(sFile);

  end;


 //   CheckListBox1.Checked[i]:=True;

  FreeAndNil(oIni);

  DeleteFile(sFile);

end;

// ---------------------------------------------------------------
class function TForm16.ExecDlg: boolean;
// ---------------------------------------------------------------
begin
  with TForm16.Create(Application) do
  begin

    ShowModal;

    Free;
  end;


end;


end.
