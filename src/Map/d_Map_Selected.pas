unit d_Map_Selected;

interface


uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
//  Db, dxmdaset, Grids, DBGrids, ActnList, ToolWin, ComCtrls, rxPlacemnt, Registry,

  cxGridLevel, cxClasses, cxGridCustomView,
//  cxControls, StdCtrls, ExtCtrls,  
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,

//  I_Act_Explorer, //dm_Act_Explorer;

  u_Shell_new,

  u_Storage,

  I_Shell,
  dm_Main,

 // u_debug,

//  dm_Object_Manager,
  u_Object_Manager,
  I_Object_list,

  u_const,
  u_const_db,

  u_types,

  u_explorer,
  u_classes,

 //   
  u_db,
  u_reg,
  u_func,

  fr_Browser,

  DBCtrls,

  u_Map_Classes,


    DB, dxmdaset, ActnList, rxPlacemnt, cxControls, StdCtrls,
  ExtCtrls, ToolWin, ComCtrls;

type
  Tdlg_Map_Selected = class(TForm)
    DataSource1: TDataSource;
    FormPlacement1: TFormPlacement;
    ToolBar1: TToolBar;
    ActionList1: TActionList;
    act_ShowInExplorer: TAction;
    Panel1: TPanel;
    btn_Show: TButton;
    act_Close: TAction;
    Button2: TButton;
    pn_Browser: TPanel;
    lb_Action: TLabel;
    mem_Selection: TdxMemData;
    cxGrid1: TcxGrid;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1DBTableView1ObjCaption: TcxGridDBColumn;
    col__name: TcxGridDBColumn;
    cxGrid1DBTableView1Caption: TcxGridDBColumn;
    col__id: TcxGridDBColumn;
    col__ObjName: TcxGridDBColumn;
    col__GUID: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure act_CloseExecute(Sender: TObject);
    procedure dx_ObjectsDblClick(Sender: TObject);
    procedure ActionList1Update(Action: TBasicAction; var Handled: Boolean);
    procedure cxGrid1DBTableView1DblClick(Sender: TObject);
    procedure cxGrid1DBTableView1FocusedRecordChanged(Sender: TcxCustomGridTableView;
        APrevFocusedRecord, AFocusedRecord: TcxCustomGridRecord; ANewItemRecordFocusingChanged:
        Boolean);
    procedure FormDestroy(Sender: TObject);
   // procedure dx_ObjectsChangeNode(Sender: TObject; OldNode,
   //   Node: TdxTreeListNode);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    Fframe_Browser: Tframe_Browser;

    FRegPath: string;
    procedure FocusNode;

  public
    class procedure ExecDlg (aDataset: TDataset);

    class procedure ExecDlg1(aSelectedItemList: TMapSelectedItemList);


    procedure UpdateCaptions;
  end;



//====================================================================
implementation {$R *.DFM}
//====================================================================
//
uses
  dm_Act_Explorer;

// ---------------------------------------------------------------
class procedure Tdlg_Map_Selected.ExecDlg1(aSelectedItemList:
    TMapSelectedItemList);
// ---------------------------------------------------------------
begin
  with Tdlg_Map_Selected.Create(Application) do
  begin
    aSelectedItemList.InitDataset(mem_Selection);
    aSelectedItemList.SaveToDataset(mem_Selection);

  (*  mem_Selection.DisableControls;
    mem_Selection.Close;
    mem_Selection.LoadFromDataSet(aDataset);

    UpdateCaptions;

    mem_Selection.EnableControls;

    dx_Objects.FullExpand;

    cxGrid1DBTableView1.DataController.Groups.FullExpand;

   // mem_Selection.First;
   // FocusNode();


*)
    ShowModal;

    Free;

//    aDataset.First;
  end;

end;




// -------------------------------------------------------------------
class procedure Tdlg_Map_Selected.ExecDlg (aDataset: TDataset);
// -------------------------------------------------------------------
{
var
  oForm: TForm;
  }
begin
(*
  if not Assigned(IShell) then
  begin
    ShowMessage('if not Assigned(IShell) then');
    Exit;
  end;
*)

{
  oForm:= FindForm ( Tdlg_Map_Selected.ClassName);

  if not Assigned(oForm) then
    oForm:=Tdlg_Map_Selected.Create(Application);
}

  with Tdlg_Map_Selected.Create(Application) do
//  with Tdlg_Map_Selected(oForm) do
  begin
    mem_Selection.DisableControls;
    mem_Selection.Close;
    mem_Selection.LoadFromDataSet(aDataset);

    UpdateCaptions;

    mem_Selection.EnableControls;

  /////  dx_Objects.FullExpand;

    cxGrid1DBTableView1.DataController.Groups.FullExpand;

   // mem_Selection.First;
   // FocusNode();


    ShowModal;

    Free;
//    aDataset.First;
  end;

//  oForm.
end;

//-------------------------------------------------------------------
procedure Tdlg_Map_Selected.FormCreate(Sender: TObject);
//-------------------------------------------------------------------
begin
 // Assert(Assigned(IShell));


  Caption:= '�������� ������� ��������';
  lb_Action.Caption:= '���� �� �������� �� ����� ��������� ������ ��������';
  Toolbar1.Color:= clInfoBk;

  pn_Browser.Align:=alClient;

  cxGrid1.Align:=alClient;


  if not g_IsDebug then
  begin


  //  col_ID.Visible:=False;
  //  col_ObjName.Visible:=False;
  end;

//  g_Storage.


  FRegPath:=REGISTRY_COMMON_FORMS+ClassName+'\1\';

  FormPlacement1.IniFileName:=FRegPath;
  FormPlacement1.Active := True;

  CreateChildForm(Tframe_Browser, Fframe_Browser, pn_Browser);

//  dx_Objects.Align:=alClient;


{
  with TRegIniFile.Create(FRegPath) do
  begin
    dx_Objects.Width:= reg_ReadInteger(FRegPath, dx_Objects.Name, 500);

    Free;
  end;
}

//  with gl_Reg.RegIni do begin
//  dx_Objects.Width:= reg_ReadInteger(FRegPath, dx_Objects.Name, 500);

 // dx_Objects.LoadFromRegistry(FRegPath + dx_Objects.Name);

 // dx_Objects.ShowGroupPanel:=false;

//  cxGrid1.Width:= reg_ReadInteger(FRegPath, cxGrid1.Name, 500);
 // end;

// FormStorage1.



 // col_ID.Visible:=g_IsDEBUG;
 // col_ObjName.Visible:=g_IsDEBUG;


////  act_Close.Caption:='�������';
end;

//-------------------------------------------------------------------
procedure Tdlg_Map_Selected.FormDestroy(Sender: TObject);
//-------------------------------------------------------------------
begin
{  with TRegIniFile.Create(FRegPath) do
  begin
    reg_WriteInteger(FRegPath, cxGrid1.Name, cxGrid1.Width);

    Free;
  end;

}
//  dx_Objects.SaveToRegistry(FRegPath + dx_Objects.Name);

  //
  //with gl_Reg.RegIni do begin
 // end;

//  Fframe_Browser.Free;

  inherited;
end;


//-------------------------------------------------------------------
procedure Tdlg_Map_Selected.act_CloseExecute(Sender: TObject);
//-------------------------------------------------------------------
var
  sGUID: string;
//  I: Integer;
  sObjName, sName: string;
  iID: Integer;
//  b, IsFound: boolean;
begin
  if Sender=act_Close then
  begin
    DataSource1.DataSet:=nil; // turn off
    Close;
  end else


  if Sender=act_ShowInExplorer then
  begin
    iID     := mem_Selection.FieldBYName(FLD_ID).AsInteger;
    sName   := mem_Selection.FieldBYName(FLD_NAME).AsString;
    sObjName:= mem_Selection.FieldBYName(FLD_OBJNAME).AsString;

   // sGUID   := mem_Selection.FieldBYName(FLD_GUID).AsString;


//    IsFound:= false;

//    if not g_Obj.ItemExist[sObjName] then
//      exit;

{    if (sObjName <> '') and (sName <> '') then
      for I := 0 to High(PROJECT_TYPES)-1 do
         if (g_Obj.ItemByName[sObjName].Type_ = PROJECT_TYPES[i]) then
           IsFound:= true;
}
//    if (iID > 0) and (IsFound) then
//    begin

    dmAct_Explorer.Browser_ViewObjectByID(sObjName, iID);
 //   dmAct_Explorer.Browser_ViewObjectByID(sObjName, iID);

//    Tfrm_Browser.ViewObjectByID (sObjName, iID);

//procedure TdmAct_Base.GetMessage (aMsg: integer; aParams: TEventParamList; var aHandled: Boolean);

//    IMainDBExplorer.Fo

    //to  TdmAct_Base
   // dmObject_Manager.OBJECT_FOCUS_IN_PROJECT_TREE (iID,sObjName);//,sGUID);
   // g_Object_Manager.OBJECT_FOCUS_IN_PROJECT_TREE (iID,sObjName);//,sGUID);

    g_Shell.OBJECT_FOCUS_IN_PROJECT_TREE(sObjName, iID);
    

//    g_ObjectInterfaceList.OBJECT_FOCUS_IN_PROJECT_TREE

    

  {  PostEvent(WE_OBJECT_FOCUS_IN_PROJECT_TREE,
              [app_Par(PAR_ID, iID),
               app_Par(PAR_OBJECT_NAME, sObjName),
               app_Par(PAR_GUID, sGUID)
               ]);
}

    act_Close.Execute;
//    end;
  end;
end;


//-------------------------------------------------------------------
procedure Tdlg_Map_Selected.dx_ObjectsDblClick(Sender: TObject);
//-------------------------------------------------------------------
begin
  btn_Show.Click;
end;

//-------------------------------------------------------------------
procedure Tdlg_Map_Selected.ActionList1Update(Action: TBasicAction;  var Handled: Boolean);
//-------------------------------------------------------------------
var
  sObjName: string;
  iID: integer;
  v: variant;
begin
  if (not assigned(cxGrid1DBTableView1.Controller.FocusedRow)) or
     (cxGrid1DBTableView1.Controller.FocusedRow.Level = 0)
  then
    act_ShowInExplorer.Enabled:=False
  else
  begin
    v:=cxGrid1DBTableView1.Controller.FocusedRow.Values[col__id.Index];
    v:=cxGrid1DBTableView1.Controller.FocusedRow.Values[col__ObjName.Index];


    iID     :=cxGrid1DBTableView1.Controller.FocusedRow.Values[col__id.Index];
    sObjName:=cxGrid1DBTableView1.Controller.FocusedRow.Values[col__ObjName.Index];


    act_ShowInExplorer.Enabled:= (iID>0) and
                      g_Obj.IsObjectNameExist(sObjName) and
                      FoundInProjectTypes(g_Obj.ItemByName[sObjName].Type_)
                     ;



  //sObjName:= mem_Selection.FieldBYName(FLD_OBJNAME).AsString;
   //     mem_Selection.FieldBYName(FLD_ID).AsInteger;

  end;




{  sObjName:= mem_Selection.FieldBYName(FLD_OBJNAME).AsString;
  iID:=      mem_Selection.FieldBYName(FLD_ID).AsInteger;

  act_ShowInExplorer.Enabled:= (iID>0) and
                      g_Obj.ItemExist[sObjName] and
                      FoundInProjectTypes(g_Obj.ItemByName[sObjName].Type_)
                     ;
}
end;

procedure Tdlg_Map_Selected.cxGrid1DBTableView1DblClick(Sender: TObject);
begin
  btn_Show.Click
end;

procedure Tdlg_Map_Selected.cxGrid1DBTableView1FocusedRecordChanged(Sender: TcxCustomGridTableView;
    APrevFocusedRecord, AFocusedRecord: TcxCustomGridRecord; ANewItemRecordFocusingChanged:
    Boolean);
var
  iID: Integer;
begin
{  iID:=AsInteger(AFocusedRecord.Values[col__id.Index]);

//////  iID:=AsInteger(Node.Values[col_ID.INdex]);

  if iID=0 then
//    mem_Selection.Locate(FLD_ID, iID, [])
 // else
    Fframe_Browser.ViewObjectByPIDL(nil);


  FocusNode ();}
end;




//-------------------------------------------------------------------
procedure Tdlg_Map_Selected.FocusNode;
//-------------------------------------------------------------------
var
  sObjectName: string;
  oPIDL: TrpPIDL;
  sTblName, sObjName, sName: string;
  iID: Integer;
  ot: TrpObjectType;
  sGUID: string;
begin
{  if Node.Level = 0 then
    exit;
}
//  Assert(Assigned(FDataSet));

{

  iID:=AsInteger(Node.Values[col_ID.INdex]);
  if iID>0 then
    mem_Selection.Locate(FLD_ID, iID, [])
  else
    Fframe_Browser.ViewObjectByPIDL(nil);
}

  if (not assigned(cxGrid1DBTableView1.Controller.FocusedRow)) or
     (cxGrid1DBTableView1.Controller.FocusedRow.Level = 0)
  then
//  if not assigned(cxGrid1DBTableView1.Controller.FocusedRow) then
    exit;


  iID     :=cxGrid1DBTableView1.Controller.FocusedRow.Values[col__id.Index];
  sObjName:=cxGrid1DBTableView1.Controller.FocusedRow.Values[col__ObjName.Index];
  sGUID   :=cxGrid1DBTableView1.Controller.FocusedRow.Values[col__GUID.Index];
  sName   :=cxGrid1DBTableView1.Controller.FocusedRow.Values[col__Name.Index];


{  sObjectName:=mem_Selection.FieldByName(FLD_OBJNAME).AsString;
  iID        :=mem_Selection.FieldByName(FLD_ID).AsInteger;
}

  if (iID=0) or (not g_Obj.IsObjectNameExist(sObjectName))  then
    Exit;

  oPIDL:= TrpPIDL.Create;

  oPIDL.ObjectName:= sObjName;//mem_Selection.FieldByName(FLD_OBJNAME).AsString;
  oPIDL.ID        := iID;   //mem_Selection.FieldByName(FLD_ID).AsInteger;
  oPIDL.Name      := sName; //mem_Selection.FieldByName(FLD_NAME).AsString;
  oPIDL.GUID      := sGUID; //mem_Selection.FieldByName(FLD_GUID).AsString;

  ot:= g_Obj.ItemByName[oPIDL.ObjectName].Type_;

  case ot of
    otCellAnt,
    otPmpSectorAnt,
    otPmpTerminalAnt,
    otLinkEnd_Antenna:     oPIDL.ObjectName:= OBJ_LINKEND_ANTENNA;
  end;

  if FoundInProjectTypes(ot) or
   //  FoundInDictTypes(ot) or
     ( oPIDL.GUID <> '' )
  then
    Fframe_Browser.ViewObjectByPIDL(oPIDL);

  oPIDL.Free;


end;




procedure Tdlg_Map_Selected.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action:=caFree;
end;

// ---------------------------------------------------------------
procedure Tdlg_Map_Selected.UpdateCaptions;
// ---------------------------------------------------------------
var
  sObjName: string;
  sObjCaption: string;
begin
 // db_ViewDataSet(mem_Selection);

  mem_Selection.First;

  with mem_Selection do
    while not EOF do
  begin
     sObjName:= FieldByName(FLD_OBJNAME).AsString;
     sObjCaption := '';


     if (sObjName <> '') then //and (sName <> '') then
     begin
       if Assigned(g_Obj.ItemByName[sObjName]) then
         sObjCaption:= g_Obj.ItemByName[sObjName].RootFolderName
       else
         sObjCaption:='';
     end
     else

{       if sObjName <> '' then  begin
         sObjCaption:= sObjName;
         sCaption:= '���: '+ sObjCode+' - '+sCaption;
       end
       else
}
         sObjCaption:= '������ �������';

    db_UpdateRecord(mem_Selection,
                    [db_Par(FLD_OBJCAPTION, sObjCaption)]);



    Next;
  end;



{
          // IsFound:= false;
           if (sObjName <> '') and (sName <> '') then
             sObjCaption:= g_Obj.ItemByName[sObjName].RootFolderName

           else
             if sObjName <> '' then  begin
               sObjCaption:= sObjName;
               sCaption:= '���: '+ sObjCode+' - '+sCaption;
             end
             else
               sObjCaption:= '������ �������';

      //////     if sName <> '' then
           db_AddRecord (mem_Selection,
               [db_Par(FLD_ID,         iID),
                db_Par(FLD_NAME,       IIF(sName='', sCaption, sName)),
                db_Par(FLD_CAPTION,    sCaption),
                db_Par(FLD_GUID,       sGUID),
                db_Par(FLD_OBJNAME,    sObjName),
                db_Par(FLD_OBJCAPTION, sObjCaption),
                db_Par(FLD_GEOREGION_ID, iGeoRegion),
                db_Par(FLD_FILENAME,   vLayer.Filespec)   ]);
         end;
}
end;




end.
