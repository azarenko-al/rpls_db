unit dm_act_LOS;

interface

uses
  SysUtils, Classes, Forms, IniFiles, Windows, Messages,

  u_func_msg,

  dm_Main,

  //I_act_LOS,
 // I_LOS_,
  u_ini_LOS_params,

  I_MapAct,

  u_vars,

  u_func,
  u_reg,

  u_run,

  u_const,
  u_const_msg,

  dm_GeoRegion,

  
  u_dlg,
  u_geo, ExtCtrls
  ;

type
  Tdmact_LOS = class(TDataModule) //, IAct_LOSX)
    Timer1: TTimer;
  //  procedure DataModuleCreate(Sender: TObject);
 //   procedure DataModuleDestroy(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
  private
    FMode: (mtLos, mtLosM);

    FBLPointsF: TBLPointArrayF;
    FBLPoint: TBLPoint;

  //  FMapDesktopID : Integer;

    procedure Add;

    procedure Dlg_CalcLOS ();
    procedure SetMapDesktopID(const Value: Integer);
    procedure StartTimer;

  public

    procedure AddByGeoRegionID(aGeoRegionName: WideString);
    procedure AddByRing(aCenter: TBLPoint; aRadius: Double);
    procedure AddByPoly(var aBLPoints: TBLPointArrayF);
    procedure AddByRect(aBLRect: TBLRect);

    procedure AddByPoint(aBLPoint: TBLPoint);

//    property MapDesktopID: Integer write SetMapDesktopID;


    class procedure Init;


  //  class procedure dmact_LOS;
  end;


var
  dmact_LOS: Tdmact_LOS;


implementation
{$R *.dfm}


class procedure Tdmact_LOS.Init;
begin
  Assert(not Assigned(dmact_LOS));

  dmact_LOS:=Tdmact_LOS.Create(Application);

 // dmact_LOS.GetInterface(IAct_LOSX, IAct_LOS);
  //Assert(Assigned(IAct_LOS));
end;


//procedure Tdmact_LOS.DataModuleDestroy(Sender: TObject);
//begin
//  IAct_LOS := nil;
//  dmact_LOS:= nil;
//
//  inherited;
//end;



procedure Tdmact_LOS.Add;
begin
  Dlg_CalcLOS ();
end;

//--------------------------------------------------------------------
procedure Tdmact_LOS.AddByGeoRegionID(aGeoRegionName: WideString);
//--------------------------------------------------------------------
begin
  if dmGeoRegion.GetPolyPointsFromMap(aGeoRegionName, FBLPointsF) then //geo_BLRectToBLPoints(aBLRect);
    StartTimer()
//    Add()
  else
    ErrorDlg('�������� ������ �� ����� ���������� ����������');

end;

//--------------------------------------------------------------------
procedure Tdmact_LOS.AddByRing(aCenter: TBLPoint; aRadius: double);
//--------------------------------------------------------------------
const POINT_COUNT=20;
begin
  geo_MakeCirclePointsF(aCenter, aRadius, POINT_COUNT, FBLPointsF);
  FMode:=mtLosM;

  StartTimer();

///  Add ();
end;


//--------------------------------------------------------------------
procedure Tdmact_LOS.AddByPoly(var aBLPoints: TBLPointArrayF);
//--------------------------------------------------------------------
begin
  FMode:=mtLosM;
  FBLPointsF:=aBLPoints;

  StartTimer();
//  Add ();
end;

//--------------------------------------------------------------------
procedure Tdmact_LOS.AddByRect(aBLRect: TBLRect);
//--------------------------------------------------------------------
begin
  FMode:=mtLosM;
  geo_BLRectToBLPointsF(aBLRect, FBLPointsF);

  StartTimer();

//  Add ();
end;

//--------------------------------------------------------------------
procedure Tdmact_LOS.AddByPoint(aBLPoint: TBLPoint);
//--------------------------------------------------------------------
begin
  FMode:=mtLos;
  FBLPoint:=aBLPoint;

  StartTimer();

//
//  Dlg_CalcLOS ();
end;


//--------------------------------------------------------------------
procedure Tdmact_LOS.Dlg_CalcLOS ();
//--------------------------------------------------------------------
const
  DEF_TEMP_FILENAME = 'los.ini';


var
  I: integer;
  iID: integer;
  b: Boolean;
  sIniFile,sTabFileName: string;
  oIni: TIniFile;

  oParams: TIni_LOS_Params;
begin
//  assert( FMapDesktopID<>0);

  case FMode of
    mtLos:   sTabFileName:=dmMain.Dirs.ProjectCalcDir + 'LOS\los.Tab';
    mtLosM:  sTabFileName:=dmMain.Dirs.ProjectCalcDir + 'LOS\los_m.Tab'
  end;


  sIniFile:=g_ApplicationDataDir + DEF_TEMP_FILENAME;


//  sIniFile:=TEMP_INI_FILE;

//  DeleteFile(sIniFile);

  oParams:=TIni_LOS_Params.Create;

    oParams.ProjectID    := dmMain.ProjectID;
    oParams.TabFileName  := sTabFileName;
//    oParams.MapDesktopID := FMapDesktopID;


    case FMode of
      mtLos:   begin
                 sTabFileName:=dmMain.Dirs.ProjectCalcDir + 'LOS\los.Tab';

                 oParams.Mode :=mtLos_;
                 oParams.blPoint := FBLPoint;
               end;

      mtLosM:  begin
                 sTabFileName:=dmMain.Dirs.ProjectCalcDir + 'LOS\los_m.Tab';

                 oParams.Mode :=mtLosM_;
                 oParams.BLPoints := FBLPointsF;
               end;
    end;


(*    if FMode=mtLos then
    begin
    end else

    if FMode=mtLosM then
    begin
    end;
*)

  oParams.SaveToFile(sIniFile);

  FreeAndNil(oParams);

(*  // ---------------------------------------------------------------
  oIni:=TIniFile.Create (sIniFile);

  with oIni do
  begin
    WriteInteger ('main', 'Project_ID',   dmMain.ProjectID);
    WriteString  ('main', 'TabFileName',  sTabFileName);
    WriteInteger ('main', 'MapDesktopID', MapDesktopID);

    WriteInteger ('main', 'Mode', IIF(FMode=mtLos,0,1));

    if FMode=mtLos then
    begin
      WriteFloat ('main', 'lat', FBLPoint.B);
      WriteFloat ('main', 'lon', FBLPoint.L);
    end else

    if FMode=mtLosM then
    begin
      WriteInteger ('points', 'count', FBLPointsF.Count);

      for I := 0 to FBLPointsF.Count - 1 do
      begin
        WriteFloat   ('points', Format('lat%d',[i]),  FBLPointsF.Items[I].B);
        WriteFloat   ('points', Format('lon%d',[i]),  FBLPointsF.Items[I].L);
      end;
    end;
  end;

  oIni.Free;
  // ---------------------------------------------------------------
*)

//  if Assigned(IActiveMapView) then


//
  if Assigned(IActiveMapView) then
    IActiveMapView.HideMap1(sTabFileName);


//  Ffrm_MapView.HideMap (sTabFileName);

  RunApp (GetApplicationDir() + EXE_LOS,  DoubleQuotedStr(sIniFile) );

(*
  if Load_ILos() then
  begin
    ILos.InitADOConnection(dmMain.ADOConnection1.ConnectionObject);
    ILos.LoadFromFile(sIniFile);
    ILos.Execute;

    UnLoad_ILos();
  end;

*)

//  if
  b:=ini_ReadBool (sIniFile, 'main', 'result', False);
  //  dmCalcMap.RegisterCalcMap (0,  '����� LOS', FTabFileName, 'los', False, True);


  if Assigned(IActiveMapView) then
    IActiveMapView.RestoreMap1(sTabFileName);

//  Ffrm_MapView.RestoreMap (sTabFileName);

  if not b then
    Exit;


//  IActiveMapView.


  if Assigned(IActiveMapView) then
  begin
    IActiveMapView.MapAddSimple (sTabFileName);

 //   PostMessage(Application.Handle, WM_REFRESH_CALC_MAP_FILES,0,0);

    g_EventManager.PostEvent_(et_CalcMap_REFRESH, []);
//    PostUserMessage(WM__CalcMap_REFRESH);

  //  PostMessage (Application.Handle, WM_USER + Integer(WM__CalcMap_REFRESH) ,0,0);


  end;



{
  if Ffrm_MapView.GetMapLayerIndexByFileName(sTabFileName)<0 then
    Ffrm_MapView.MapAddSimple (sTabFileName);
}



{  iID:=dmCalcMap.RegisterCalcMap (0,0, '����� LOS', sTabFileName, 'los', False, True);

  dmCalcMap_View.SetChecked(FMapDesktopID, iID, True);

}
end;


procedure Tdmact_LOS.SetMapDesktopID(const Value: Integer);
begin
//  FMapDesktopID :=Value;
end;

procedure Tdmact_LOS.StartTimer;
begin
  Timer1.Enabled:=True;
end;


procedure Tdmact_LOS.Timer1Timer(Sender: TObject);
begin
  Timer1.Enabled:=False;

  Dlg_CalcLOS ();
end;



end.

