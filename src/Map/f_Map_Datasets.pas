unit f_Map_Datasets;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, RxMemDS, MapXLib_TLB, Grids, DBGrids,

  u_Mapx
  ;

type
  Tfrm_Map_Datasets = class(TForm)
    RxMemoryData1: TRxMemoryData;
    DataSource1: TDataSource;
    DBGrid1: TDBGrid;
  private
    { Private declarations }
  public
    class function ExecDlg(aMap: TMap): boolean;


  end;

var
  frm_Map_Datasets: Tfrm_Map_Datasets;


implementation

{$R *.dfm}


class function Tfrm_Map_Datasets.ExecDlg(aMap: TMap): boolean;
begin
  with Tfrm_Map_Datasets.Create(Application) do
  begin
 
    ShowModal;

    Free;
  end;
  
end;

end.
