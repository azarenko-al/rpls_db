inherited dlg_Map_Links: Tdlg_Map_Links
  Left = 1267
  Top = 449
  Width = 521
  Height = 361
  BorderIcons = [biSystemMenu]
  Caption = 'dlg_Map_Links'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited ToolBar1: TToolBar
    Width = 513
  end
  inherited StatusBar1: TStatusBar
    Top = 264
    Width = 513
    Visible = False
  end
  object Panel1: TPanel [2]
    Left = 0
    Top = 302
    Width = 513
    Height = 31
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    DesignSize = (
      513
      31)
    object Button1: TButton
      Left = 1
      Top = 5
      Width = 96
      Height = 23
      Action = act_Make_LinkLine
      TabOrder = 0
    end
    object Button2: TButton
      Left = 434
      Top = 5
      Width = 75
      Height = 23
      Action = act_Close
      Anchors = [akTop, akRight]
      TabOrder = 1
    end
    object Button3: TButton
      Left = 105
      Top = 5
      Width = 72
      Height = 23
      Action = act_Clear
      TabOrder = 2
    end
  end
  object ListBox1: TListBox [3]
    Left = 0
    Top = 65
    Width = 513
    Height = 104
    Align = alTop
    ItemHeight = 13
    TabOrder = 3
  end
  object StatusBar_Count: TStatusBar [4]
    Left = 0
    Top = 283
    Width = 513
    Height = 19
    Panels = <>
    SimplePanel = True
  end
  inherited FormStorage1: TFormStorage
    Left = 54
    Top = 9
  end
  object ActionList1: TActionList
    OnUpdate = ActionList1Update
    Left = 24
    Top = 10
    object act_Make_LinkLine: TAction
      Caption = #1057#1086#1079#1076#1072#1090#1100' '#1083#1080#1085#1080#1102
      OnExecute = act_Make_LinkLineExecute
    end
    object act_Close: TAction
      Caption = 'Close'
      OnExecute = act_Make_LinkLineExecute
    end
    object act_Clear: TAction
      Caption = #1054#1095#1080#1089#1090#1080#1090#1100
      OnExecute = act_Make_LinkLineExecute
    end
  end
end
