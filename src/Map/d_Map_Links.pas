unit d_Map_Links;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, dxmdaset, Grids, DBGrids, ActnList, ToolWin, ComCtrls, rxPlacemnt, ExtCtrls,
  cxGridCustomTableView, cxGridTableView,    StdCtrls,
  cxGridDBTableView, cxControls, cxGridCustomView, cxClasses, cxGridLevel,

  dm_User_Security,

  f_Map_Tool_base,

  u_Map_Classes,

//  dm_Main,
  I_LinkLine,
  dm_Act_LinkLine,

  I_MapAct,
  I_MapView,

  u_types,

  u_const_db,

  u_classes,
  u_func,
  u_db,

  cxGrid
  , u_func_msg;

type
  Tdlg_Map_Links = class(Tfrm_Map_Tool_base)
    ActionList1: TActionList;
    act_Make_LinkLine: TAction;
    Panel1: TPanel;
    Button1: TButton;
    act_Close: TAction;
    Button2: TButton;
    ListBox1: TListBox;
    StatusBar_Count: TStatusBar;
    act_Clear: TAction;
    Button3: TButton;
    procedure FormCreate(Sender: TObject);
    procedure act_Make_LinkLineExecute(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ActionList1Update(Action: TBasicAction;
      var Handled: Boolean);
  private
    FIDList: TIDList;

  protected
//    procedure DoOnMapEvent(Sender: TObject; aEventIndex: integer); override;

    procedure GetMessage(aMsg: TEventType; aParams: TEventParamList ; var aHandled:
        boolean); override;

  public
    class procedure ShowWindow();

  end;



//==================================================================
implementation {$R *.DFM}
//==================================================================


//-------------------------------------------------------------------
class procedure Tdlg_Map_Links.ShowWindow;
//-------------------------------------------------------------------
var
  oForm: TForm;
  vIMapView: IMapViewX;

begin
{
  // ---------------------------------------------------------------
  if dmUser_Security.ProjectIsReadOnly then
  begin
    ShowMessage('���������� �������� ������. ������ ������ ��� ������.');
  //  Result:=0;
    Exit;
  end;
}


  if not Assigned(IActiveMapView) then
    Exit;

  oForm:= IsFormExists(Application, Tdlg_Map_Links.ClassName);

  if not Assigned(oForm) then
  begin
    vIMapView:=IActiveMapView;
    oForm:= Tdlg_Map_Links.Create(Application);
    Tdlg_Map_Links(oForm).SetIMapView(vIMapView);
  end;


  oForm.Show;

end;

//-------------------------------------------------------------------
procedure Tdlg_Map_Links.FormCreate(Sender: TObject);
//-------------------------------------------------------------------
begin
  inherited;

  Caption:='�������� �� �����';

  act_Close.Caption:='�������';
  act_Make_LinkLine.Caption:='�������';


  listBox1.Align:=alClient;


  FIDList:=TIDList.Create;

end;


procedure Tdlg_Map_Links.FormDestroy(Sender: TObject);
begin

  FreeAndNil(FIDList);

  inherited;
end;

//-------------------------------------------------------------------
procedure Tdlg_Map_Links.act_Make_LinkLineExecute(Sender: TObject);
//-------------------------------------------------------------------
var
  iID: integer;
begin
  if Sender=act_Clear then
    ListBox1.Items.Clear
  else

  if Sender=act_Close then
    Close
  else

  if Sender=act_Make_LinkLine then
  begin
//    FIDList.LoadFromDataSet (mem_Items);
    FIDList.LoadFromStrings(listBox1.items);

    if dmAct_LinkLine.Dlg_Add (FIDList) > 0 then
      Close;

  end;
  ;

end;



procedure Tdlg_Map_Links.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action:=caFree;
end;


procedure Tdlg_Map_Links.ActionList1Update(Action: TBasicAction; var Handled: Boolean);
begin
  act_Make_LinkLine.Enabled:=ListBox1.Items.Count>1;
end;



// ---------------------------------------------------------------
procedure Tdlg_Map_Links.GetMessage(aMsg: TEventType; aParams: TEventParamList
    ; var aHandled: boolean);
// ---------------------------------------------------------------
var

  oList:  TMapSelectedItemList;
  oItem: TMapSelectedItem;
begin
  inherited;

  case aMsg of
    et_MAP_EVENT_ON_SELECT:
          begin
            oList :=FIMapView.GetSelectedItemList;

   //         oDataset:=FIMapView.GetSelectedDataset;

       //     db_ViewDataset(oDataset);

            oItem:=oList.findByObjName(OBJ_LINK);


            if Assigned(oItem) then
            begin
              if ListBox1.Items.IndexOfObject(Pointer(oItem.ID))<0 then
              begin
                ListBox1.Items.AddObject(oItem.Name, Pointer(oItem.ID));

                StatusBar_Count.SimpleText := Format('����� ����������: %d', [ListBox1.Items.Count]);
              end;

           end;

          end;

  end;

end;


end.


{

procedure Tdlg_Map_Links.DoOnMapEvent(Sender: TObject; aEventIndex: integer);
var
//  oDataset: TDataSet;

  oList:  TMapSelectedItemList;
  oItem: TMapSelectedItem;
begin
  inherited;

  case aEventIndex of
    MAP_EVENT_ON_SELECT_:
          begin
            oList :=FIMapView.GetSelectedItemList;

   //         oDataset:=FIMapView.GetSelectedDataset;

       //     db_ViewDataset(oDataset);

            oItem:=oList.findByObjName(OBJ_LINK);


            if Assigned(oItem) then
            begin
              if ListBox1.Items.IndexOfObject(Pointer(oItem.ID))<0 then
              begin
                ListBox1.Items.AddObject(oItem.Name, Pointer(oItem.ID));

                StatusBar_Count.SimpleText := Format('����� ����������: %d', [ListBox1.Items.Count]);
              end;

           end;

          end;

  end;

end;


