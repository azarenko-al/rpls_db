object dlg_Map_Selected: Tdlg_Map_Selected
  Left = 895
  Top = 549
  Width = 525
  Height = 429
  Caption = 'dlg_Map_Selected'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object ToolBar1: TToolBar
    Left = 0
    Top = 0
    Width = 517
    Height = 65
    BorderWidth = 5
    ButtonHeight = 13
    Caption = 'ToolBar1'
    Color = clAppWorkSpace
    ParentColor = False
    TabOrder = 0
    object lb_Action: TLabel
      Left = 0
      Top = 2
      Width = 44
      Height = 13
      Caption = 'lb_Action'
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 364
    Width = 517
    Height = 37
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    DesignSize = (
      517
      37)
    object btn_Show: TButton
      Left = 3
      Top = 6
      Width = 182
      Height = 23
      Action = act_ShowInExplorer
      TabOrder = 0
    end
    object Button2: TButton
      Left = 438
      Top = 6
      Width = 75
      Height = 23
      Action = act_Close
      Anchors = [akTop, akRight]
      Cancel = True
      ModalResult = 2
      TabOrder = 1
    end
  end
  object pn_Browser: TPanel
    Left = 473
    Top = 65
    Width = 44
    Height = 299
    Align = alRight
    BevelOuter = bvNone
    TabOrder = 2
    Visible = False
  end
  object cxGrid1: TcxGrid
    Left = 0
    Top = 65
    Width = 233
    Height = 299
    Align = alLeft
    TabOrder = 3
    LookAndFeel.Kind = lfFlat
    object cxGrid1DBTableView1: TcxGridDBTableView
      OnDblClick = cxGrid1DBTableView1DblClick
      NavigatorButtons.ConfirmDelete = False
      DataController.DataSource = DataSource1
      DataController.Filter.MaxValueListCount = 1000
      DataController.KeyFieldNames = 'id'
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      Filtering.ColumnPopup.MaxDropDownItemCount = 12
      OptionsBehavior.ImmediateEditor = False
      OptionsCustomize.ColumnFiltering = False
      OptionsSelection.CellSelect = False
      OptionsSelection.HideFocusRectOnExit = False
      OptionsSelection.InvertSelect = False
      OptionsView.ColumnAutoWidth = True
      OptionsView.GroupByBox = False
      Preview.AutoHeight = False
      Preview.MaxLineCount = 2
      object cxGrid1DBTableView1ObjCaption: TcxGridDBColumn
        Caption = #1058#1080#1087
        DataBinding.FieldName = 'ObjCaption'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = True
        Visible = False
        GroupIndex = 0
        Options.Filtering = False
        SortIndex = 0
        SortOrder = soAscending
        Width = 43
      end
      object col__name: TcxGridDBColumn
        Caption = #1053#1072#1079#1074#1072#1085#1080#1077
        DataBinding.FieldName = 'name'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = True
        Options.Filtering = False
        Width = 62
      end
      object cxGrid1DBTableView1Caption: TcxGridDBColumn
        DataBinding.FieldName = 'Caption'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = True
        Visible = False
        Options.Filtering = False
        Width = 271
      end
      object col__id: TcxGridDBColumn
        DataBinding.FieldName = 'id'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = True
        Visible = False
        Options.Filtering = False
        Width = 28
      end
      object col__ObjName: TcxGridDBColumn
        DataBinding.FieldName = 'ObjName'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.MaxLength = 0
        Properties.ReadOnly = True
        Visible = False
        Options.Filtering = False
        Width = 28
      end
      object col__GUID: TcxGridDBColumn
        DataBinding.FieldName = 'GUID'
        Visible = False
        Options.Filtering = False
        Width = 71
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1DBTableView1
    end
  end
  object DataSource1: TDataSource
    DataSet = mem_Selection
    Left = 307
    Top = 53
  end
  object FormPlacement1: TFormPlacement
    Active = False
    IniFileName = 'Software\Onega\'
    Left = 132
    Top = 4
  end
  object ActionList1: TActionList
    OnUpdate = ActionList1Update
    Left = 130
    Top = 51
    object act_ShowInExplorer: TAction
      Caption = #1055#1086#1082#1072#1079#1072#1090#1100' '#1074' '#1075#1083#1072#1074#1085#1086#1084' '#1080#1085#1089#1087#1077#1082#1090#1086#1088#1077
      OnExecute = act_CloseExecute
    end
    object act_Close: TAction
      Caption = #1047#1072#1082#1088#1099#1090#1100
      OnExecute = act_CloseExecute
    end
  end
  object mem_Selection: TdxMemData
    Indexes = <>
    SortOptions = []
    Left = 309
    Top = 5
  end
end
