unit u_MapViewClasses;

interface
uses Classes,Menus,Dialogs,Math,SysUtils,Controls, Graphics, Types, Windows,

     u_Geo,
     u_func;

type

  TMapUserShape       = class;
  TMapUserShapeList   = class;

  //----------------------------------------
  // ������� ��� ����������� �� ����� (����������������)
  //----------------------------------------
  TMapUserShape = class
  private
  //  constructor CreateCross(aColor:integer; aBLPoint: TBLPoint; aWidth: integer);
  protected
  public
    ShapeType   : (stLine,stRect,ustCircle,stCross,stPoly,
                   stCaption,stMouseCaption
                   );
    ShapeColor  : integer;
   // Bounds      : record b1,l1, b2,l2: single; end;
    BLRect      : TBLRect;
    BLVector    : TBLVector;
    BLPoint     : TBLPoint;
    BLPointArr  : TBLPointArrayF;
   // XYRect      : TXYRect;
    Center      : TBLPoint;

    Width: integer;

    Caption_    : string;
    TextAlign   : TAlignment; // = (taLeftJustify, taRightJustify, taCenter);
    VAlign      : (vaCenter,vaBottom,vaTop);

    constructor CreateLine     (aColor: integer; aVector: TBLVector; aWidth: integer );
    constructor CreateRect     (aColor: integer; aRect  : TBLRect);
    constructor CreatePoly     (aColor: integer; aBLPoints : TBLPointArrayF);
    constructor CreateCircle   (aColor: integer; aPoint: TBLPoint; aRadius: double);
    constructor CreateCross    (aColor: integer; aPoint: TBLPoint);
    constructor CreateCaption  (aColor: integer; aPoint: TBLPoint; aPCaption: PChar);
    constructor CreateCaption1 (aColor: integer; aPoint: TBLPoint; aCaption: string; aAlign: TAlignment);
  end;

  
  
  TOnMapToScreenEvent =  function (aBLPoint: TBLPoint): TPoint of object;


  TMapUserShapeList = class(TList)
  private
    FOnMapToScreen: TOnMapToScreenEvent;
    procedure DoDrawHint (aCanvas: TCanvas; aX,aY:integer; aText: string; aAlign:TAlignment);
  public

    procedure DrawArrow(aColor, aWidth: Integer; aBLVector: TBLVector);

    function GetItem(Index: integer): TMapUserShape;
    procedure Clear; override;
    property Items  [Index: integer]: TMapUserShape read GetItem; default;
    property OnMapToScreen: TOnMapToScreenEvent read FOnMapToScreen write FOnMapToScreen;

    procedure DrawShapeList(aCanvas: TCanvas);

  end;



//=================================================================
implementation
//=================================================================


//-----------------------------------------------------------------
function TMapUserShapeList.GetItem(Index: integer): TMapUserShape;
//-----------------------------------------------------------------
begin
  Result:=TMapUserShape(inherited Items[Index]);
end;

//-----------------------------------------------------------------
procedure TMapUserShapeList.Clear;
//-----------------------------------------------------------------
var i: integer;
begin
  for i:=0 to Count-1 do Items[i].Destroy;
  inherited;
end;

//-----------------------------------------------------------------
constructor TMapUserShape.CreateLine (aColor:integer; aVector: TBLVector; aWidth: integer);
//-----------------------------------------------------------------
begin
  inherited Create;
  ShapeColor:=aColor;
  ShapeType:=stLine;
  blVector:=aVector;
  Width:=aWidth;
//  with Bounds do begin  b1:=ab1;  l1:=al1;  b2:=ab2;  l2:=al2; end;
end;


{
//-----------------------------------------------------------------
constructor TMapUserShape.CreateCross(aColor:integer; aBLPoint: TBLPoint;
    aWidth: integer);
//-----------------------------------------------------------------
begin
  inherited Create;
  ShapeColor:=aColor;
  ShapeType:=stCross;
  BLPoint:=aBLPoint;
  Width:=aWidth;
//  with Bounds do begin  b1:=ab1;  l1:=al1;  b2:=ab2;  l2:=al2; end;
end;
}

//-----------------------------------------------------------------
constructor TMapUserShape.CreateRect (aColor:integer; aRect: TBLRect);
//-----------------------------------------------------------------
begin
  inherited Create;
  ShapeColor:=aColor;  ShapeType:=stRect;
  blRect:=aRect;
end;

//-----------------------------------------------------------------
constructor TMapUserShape.CreatePoly (aColor:integer; aBLPoints: TBLPointArrayF);
//-----------------------------------------------------------------
begin
  inherited Create;
  ShapeColor:=aColor;  ShapeType:=stPoly;
  BLPointArr:=aBLPoints;
//  blRect:=aRect;
end;

//-----------------------------------------------------------------
constructor TMapUserShape.CreateCircle (aColor:integer; aPoint: TBLPoint; aRadius: double);
//-----------------------------------------------------------------
begin
  inherited Create;
  ShapeColor:=aColor;
  ShapeType:=ustCircle;
  Center:=aPoint;
//////  XYRect:=geo_XYRectByCircle (aPoint, aRadius);
end;

//-----------------------------------------------------------------
constructor TMapUserShape.CreateCross (aColor:integer; aPoint: TBLPoint);
//-----------------------------------------------------------------
begin
  inherited Create;
  ShapeColor:=aColor;
  ShapeType:=stCross;
  blPoint:=aPoint;
end;

//-----------------------------------------------------------------
constructor TMapUserShape.CreateCaption (aColor:integer; aPoint: TBLPoint; aPCaption:PChar);
//-----------------------------------------------------------------
var PCaption: PChar;
begin
  inherited Create;
  ShapeColor:=aColor;  ShapeType:=stMouseCaption;
  PCaption:=aPCaption;
  Caption_:=StrPas (aPCaption);
  StrDispose (aPCaption);

  blPoint:=aPoint;
end;

//-----------------------------------------------------------------
constructor TMapUserShape.CreateCaption1 (aColor:integer; aPoint:TBLPoint;
                             aCaption:string; aAlign:TAlignment);
//-----------------------------------------------------------------
begin
  inherited Create;
  ShapeColor:=aColor;  ShapeType:=stCaption;
  Caption_:=aCaption;
  blPoint:=aPoint;
//  with Bounds do begin  b1:=aBLPoint.B;  l1:=aBLPoint.L;  b2:=aBLPoint.B; l2:=aBLPoint.L;  end;
  TextAlign:=aAlign;
end;



procedure TMapUserShapeList.DrawArrow(aColor, aWidth: Integer; aBLVector: TBLVector);
var
  dAzimuth,dLength: Double;
begin
  Add(TMapUserShape.CreateLine(aColor, ablVector, aWidth));

  dLength:=geo_Distance_m(aBLVector)/2;
  dAzimuth:=geo_Azimuth_Minus(geo_Azimuth(aBLVector.Point1,aBLVector.Point2), 1.1464);
  aBLVector.Point1:=geo_RotateByAzimuth (aBLVector.Point1 ,dLength,dAzimuth);

  aBLVector.Point2:=geo_RotateByAzimuth (aBLVector.Point1,30,geo_Azimuth_Plus(dAzimuth,150));
  Add(TMapUserShape.CreateLine(aColor, aBLVector, aWidth));

  aBLVector.Point2:=geo_RotateByAzimuth (aBLVector.Point1,30,geo_Azimuth_Plus(dAzimuth,210));
  Add(TMapUserShape.CreateLine(aColor, aBLVector, aWidth));

end;




//----------------------------------------------------------------------
procedure TMapUserShapeList.DoDrawHint (aCanvas: TCanvas; aX,aY:integer; aText: string; aAlign:TAlignment);
//----------------------------------------------------------------------
var iW,iH: integer;  r: TRect;
begin
 if aText <> '' then
 begin
   iW := aCanvas.TextWidth (aText) + 6;
   iH := aCanvas.TextHeight(aText) + 4;

   case aAlign of
     taLeftJustify  : Inc(aX, 15);   //  15 - �������� �� ������
     taRightJustify : Dec(aX, iW);
     taCenter       : begin
                        Dec(aX, iW div 2);  Dec(aY, iH div 2);
                      end;
   end;

   r:=Rect(aX,aY, aX+iW, aY+iH);

   aCanvas.Font.Color := clInfoText;
   DrawEdge (aCanvas.Handle, r, BDR_RAISEDOUTER, BF_RECT);
   InflateRect (r,-2,-2);

   aCanvas.TextRect (r, aX+2, aY+2, aText);
 end;
end;




//---------------------------------------------------
procedure TMapUserShapeList.DrawShapeList(aCanvas: TCanvas);
//---------------------------------------------------

    function MapToScreen(aBLPoint: TBLPoint): TPoint;
    begin
      if Assigned(FOnMapToScreen) then
        Result :=  FOnMapToScreen(aBLPoint)
      else
        raise Exception.Create('');
    end;


    //----------------------------------------------------------------------
    procedure DoDrawHint (aX,aY:integer; aText: string; aAlign:TAlignment);
    //----------------------------------------------------------------------
    var iW,iH: integer;  r: TRect;
    begin
     if aText <> '' then
     begin
       iW := aCanvas.TextWidth (aText) + 6;
       iH := aCanvas.TextHeight(aText) + 4;

       case aAlign of
         taLeftJustify  : Inc(aX, 15);   //  15 - �������� �� ������
         taRightJustify : Dec(aX, iW);
         taCenter       : begin
                            Dec(aX, iW div 2);  Dec(aY, iH div 2);
                          end;
       end;

       r:=Rect(aX,aY, aX+iW, aY+iH);

       aCanvas.Font.Color := clInfoText;
       DrawEdge (aCanvas.Handle, r, BDR_RAISEDOUTER, BF_RECT);
       InflateRect (r,-2,-2);

       aCanvas.TextRect (r, aX+2, aY+2, aText);
     end;
    end;


    //---------------------------------------------------
    procedure DoDrawItems();
    //---------------------------------------------------
    var i,j,iW,iH: integer;
        hPen: hdc;
        _a,_b,pt: TPoint;
       str:string;
       R: TRect;
       blPoints: TBLPointArrayF;
       oUserShape: TMapUserShape;
    begin
      for i:=0 to Count-1 do
      begin
        oUserShape:=Items[i];

        case oUserShape.ShapeType of
          stLine:    begin

                      blPoints.Count:=2;
                      blPoints.Items[0]:=oUserShape.blVector.Point1;
                      blPoints.Items[1]:=oUserShape.blVector.Point2;

                      aCanvas.Pen.Color:=oUserShape.ShapeColor;
                      aCanvas.Pen.Width:=oUserShape.Width;

                      _a:=MapToScreen (blPoints.Items[0]);
                      _b:=MapToScreen (blPoints.Items[1]);

                      aCanvas.MoveTo (_a.x, _a.y);
                      aCanvas.LineTo (_b.x, _b.y);
                    end;

          stCaption:begin
                      pt:=MapToScreen (oUserShape.blPoint);
                      DoDrawHint (pt.x, pt.y+5, oUserShape.Caption_, oUserShape.TextAlign);
                    end;

         else
          raise Exception.Create('');
        end;
      end;

    end;


begin
  DoDrawItems();


end;




end.

{

    //----------------------------------------------------------------------
    procedure DoDrawHint (aCanvas: TCanvas; aX,aY:integer; aText: string; aAlign:TAlignment);
    //----------------------------------------------------------------------
    var iW,iH: integer;  r: TRect;
    begin
     if aText <> '' then
     begin
       iW := aCanvas.TextWidth (aText) + 6;
       iH := aCanvas.TextHeight(aText) + 4;

       case aAlign of
         taLeftJustify  : Inc(aX, 15);   //  15 - �������� �� ������
         taRightJustify : Dec(aX, iW);
         taCenter       : begin
                            Dec(aX, iW div 2);  Dec(aY, iH div 2);
                          end;
       end;

       r:=Rect(aX,aY, aX+iW, aY+iH);

       aCanvas.Font.Color := clInfoText;
       DrawEdge (aCanvas.Handle, r, BDR_RAISEDOUTER, BF_RECT);
       InflateRect (r,-2,-2);

       aCanvas.TextRect (r, aX+2, aY+2, aText);
     end;
    end;


    //---------------------------------------------------
    procedure DrawShapeList(aCanvas: TCanvas; aMapUserShapeList: TMapUserShapeList);
    //---------------------------------------------------
    var i,j,iW,iH: integer;
        hPen: hdc;
        _a,_b,pt: TPoint;
       str:string;
       R: TRect;
       blPoints: TBLPointArrayF;
       oUserShape: TMapUserShape;
    begin
      for i:=0 to aMapUserShapeList.Count-1 do
      begin
        oUserShape:=aMapUserShapeList.Items[i];

        case oUserShape.ShapeType of
          stLine:    begin

                      blPoints.Count:=2;
                      blPoints.Items[0]:=oUserShape.blVector.Point1;
                      blPoints.Items[1]:=oUserShape.blVector.Point2;

                      aCanvas.Pen.Color:=oUserShape.ShapeColor;
                      aCanvas.Pen.Width:=oUserShape.Width;

                      _a:=MapToScreen (blPoints.Items[0]);
                      _b:=MapToScreen (blPoints.Items[1]);

                      aCanvas.MoveTo (_a.x, _a.y);
                      aCanvas.LineTo (_b.x, _b.y);
                    end;

          stCaption:begin
                      pt:=MapToScreen (oUserShape.blPoint);
                      DoDrawHint (aCanvas, pt.x, pt.y+5, oUserShape.Caption_, oUserShape.TextAlign);
                    end;

         else
          raise Exception.Create('');
        end;
      end;

    end;




Var
   screenX1, screenY1, screenX2, screenY2 : Single;
   mapX1, mapX2, mapY1, mapY2 : Double;
   pText : PChar;
   size : integer;
   oCanvas: TCanvas;
begin
  if (FUserShapeList.Count>0) {or (CustomShapeList.Count>0)} then
  begin
    oCanvas:=TCanvas.Create;
    oCanvas.Handle:=aOutputDC;

   // FUserShapeList.
    DrawShapeList (oCanvas, FUserShapeList);

    FreeAndNil(oCanvas);

    FUserShapeList.Clear;
  end;
end;


