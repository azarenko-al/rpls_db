unit dm_act_Map;

interface

uses
  Classes, Forms,
  I_MapAct,

  u_func_msg,


  f_Map
  ;

type

  TdmAct_Map = class(TDataModule, IAct_Map_X)
    procedure DataModuleDestroy(Sender: TObject);
//    procedure ApplicationEvents1Message(var Msg: tagMSG; var Handled: Boolean);

  public
    procedure MAP_RESTORE_WORKSPACE;
    procedure MAP_SAVE_AND_CLOSE_WORKSPACE;
    procedure MapRefresh();

//    procedure CLEAR111;
   // procedure UnRegisterMap(Sender: tobject; aIMapView: IMapViewX);

  public
    procedure CreateNewForm;
    procedure ShowForm();


    class procedure Init;


  end;

var
  dmAct_Map: TdmAct_Map;

//====================================================================
// implementation
//====================================================================
implementation {$R *.DFM}
                        

//--------------------------------------------------------------------
class procedure TdmAct_Map.Init;
//--------------------------------------------------------------------
begin
  Assert(not Assigned(dmAct_Map));

  Application.CreateForm(TdmAct_Map, dmAct_Map);
  dmAct_Map.GetInterface(IAct_Map_X, IAct_Map);

  Assert(Assigned(IAct_Map));
end;


procedure TdmAct_Map.DataModuleDestroy(Sender: TObject);
begin
  IAct_Map := nil;
  dmAct_Map :=nil;

  inherited;
end;


procedure TdmAct_Map.CreateNewForm;
begin
  Tfrm_Map.CreateNewForm();
end;



//--------------------------------------------------------------------
procedure TdmAct_Map.ShowForm ();
//--------------------------------------------------------------------
begin
  Tfrm_Map.ShowForm;
end;


//--------------------------------------------------------------------
procedure TdmAct_Map.MapRefresh ();
//--------------------------------------------------------------------
begin
  g_EventManager.PostEvent_(et_MAP_REFRESH, []);

 // PostUserMessage_(MSG_MAP_REFRESH);
 // _PostMessage11111 (MSG_MAP_REFRESH);

end;


procedure TdmAct_Map.MAP_RESTORE_WORKSPACE;
begin
  g_EventManager.PostEvent_(et_MAP_RESTORE_WORKSPACE, []);

 // PostUserMessage_ (MSG_MAP_RESTORE_WORKSPACE);
  
//  _PostMessage11111 (MSG_MAP_RESTORE_WORKSPACE);

end;


procedure TdmAct_Map.MAP_SAVE_AND_CLOSE_WORKSPACE;
begin
 // _PostMessage11111 (MSG_MAP_SAVE_AND_CLOSE_WORKSPACE);
 // PostUserMessage_ (MSG_MAP_SAVE_AND_CLOSE_WORKSPACE);

  g_EventManager.PostEvent_(et_MAP_SAVE_AND_CLOSE_WORKSPACE, []);

end;

(*
procedure TdmAct_Map.CLEAR111;
begin
  _PostMessage11111 (MSG_MAP_CLEAR);


//  if Assigned(IMainMapView) then
end;
*)



end.


{



procedure TdmAct_Map.UnRegisterMap(Sender: tobject; aIMapView: IMapViewX);
begin
  if not Assigned(aIMapView) then
    Exit;


  Assert(Assigned(aIMapView));

  if aIMapView=IActiveMapView then
    IActiveMapView:=nil;

end;





// TODO: RemoveCalcRegionMaps
//procedure TdmAct_Map.RemoveCalcRegionMaps(aCalcRegionID: Integer);
//var
//i: integer;
//v: IMapViewX;
//begin
//
//(*  for i := 0 to FMapInterfaceList.Count - 1 do
//begin
//  v:=FMapInterfaceList[i] as IMapViewX;
///////////  v.RemoveCalcRegionMaps(aCalcRegionID);
//end;
//*)
////  if Assigned(IMainMapView) then
//end;



//
//
//    dmAct_MSC.GetInterface(IAct_MSC_X, IAct_MSC);
//    Assert(Assigned(IAct_MSC));
//
// // end;
//
////    dmAct_MSC:=TdmAct_MSC.Create(nil);
//
//end;
//
//
//procedure TdmAct_MSC.DataModuleDestroy(Sender: TObject);
//begin
//  IAct_MSC := nil;
//  dmAct_MSC:= nil;





procedure TdmAct_Map.ApplicationEvents1Message(var Msg: tagMSG; var Handled:
    Boolean);
begin
  case Msg.message of

  // MSG_MAP_REFRESH_test:
   //  ShowMessage('procedure TdmAct_Map.ApplicationEvents1Message.MSG_MAP_REFRESH_test');


    WE_PROJECT_CHANGED_:
      g_log.SysMsg('TdmAct_Map.ApplicationEvents1Message -- WE_PROJECT_CHANGED_' );

  end;

end;

