unit dm_MapView_tools;

interface

{$I Mapx.Inc}

uses
  Forms, Classes, MapXLib_TLB, Variants,StrUtils, Dialogs,

  u_MapX_lib,
  u_MapX,

  u_func,

  I_Options_
//  X_Options
  ;

type
  TdmMapView_tools111111111 = class(TDataModule)
  private
    procedure Exec(aMap: TMap);


  public
    class procedure Init;

 //   procedure RefreshObjectStyles(FMap: TMap; FObjectMapList_new: TObjectMapList);

  end;

var
  dmMapView_tools111111111: TdmMapView_tools111111111;

implementation


{$R *.dfm}

procedure TdmMapView_tools111111111.Exec(aMap: TMap);
var
  I: Integer;
  iParamCount: Integer;
  j: Integer;
  k: Integer;
  n: Integer;
  vLayer: CMapXLayer;
  vFeature: CMapXFeature;
  x: Double;
  y: Double;

  vDataset: CMapxDataset;
  vRowValues: CMapXRowValues;

  rStyle: TOptionsObjectStyleRec;
  s: string;
  sObjName: string;
  sValue: string;

begin
  CursorHourGlass();

  for j := 1 to aMap.Layers.Count do
  begin
    {$IFDEF Mapx5}
    vLayer:=aMap.Layers.Item[j];
    {$ELSE}
    vLayer:=aMap.Layers.Item(j);
    {$ENDIF}

//    vLayer:=aMap.Layers.Item[j];

    s:=vLayer.Name;


    if LeftStr(vLayer.Name,1)<>'~' then
      Continue;


    sObjName := Copy(vLayer.Name,2,100);

    if not (Eq(sObjName,'link') or Eq(sObjName,'property')) then
      Continue;

//    if aObjectMaps.IndexOf(sObjName)<0 then
  //    Continue;

//    if  then

    rStyle:=IOptions.GetDefaultStyle(sObjName);

    vLayer.BeginAccess(miAccessReadWrite);

    if vLayer.Datasets.Count=0 then
      ShowMessage('vLayer.Datasets.Count=0');

(*
    if vLayer.Datasets.Count=0 then
      aMap.Datasets.Add (miDataSetLayer, vLayer, EmptyParam, EmptyParam,
                        EmptyParam, EmptyParam, EmptyParam, EmptyParam) ;
*)


//    {$IFDEF Mapx5}
//    vDataset := vLayer.Datasets.Item[1];
//    {$ELSE}
//    vDataset := vLayer.Datasets.Item(1);

//    vDataset := vLayer.Datasets.Item(sObjName+'-Data');
    vDataset := vLayer.Datasets.Item[sObjName+'-Data'];

  //  mapx_GetDatasetByName(vLayer, sObjName+'-Data');

    k:=vLayer.Datasets.Count;

   // {$ENDIF}

//    vDataset := vLayer.Datasets.Item[1];

    for I := 1 to vLayer.AllFeatures.Count do
    begin
//      vFeature :=vLayer.AllFeatures.Item[i];


      vFeature :=vLayer.AllFeatures.Item[i];

     // vFeature.Style.SymbolFont.Size  := 24;

      case vFeature.type_ of

         miFeatureTypeSymbol:
         begin
//           vLayer.Style.SymbolFontColor:= rStyle.MIStyle.

//           vLayer.Style.SymbolFontColor:=miColorGreen;
           vLayer.Style.SymbolFontColor:=rStyle.MIStyle.FontColor;
//           vLayer.Style.SymbolFontColor:=rStyle.MIStyle.FontColor;

  //         j:=vLayer.Style.SymbolFontColor;

            //vFeature.Style.SymbolFontColor := miColorRed;

            OleVariant(vLayer.Style.SymbolFont).Name  := rStyle.MIStyle.FontName;// 'Mapinfo Symbols';
            OleVariant(vLayer.Style.SymbolFont).Size  := rStyle.MIStyle.FontSize; //  20;
          //  OleVariant(vFeature.Style.SymbolFont).Color  := clRed;


        //    vFeature.Style.SymbolFont := 41;
            vLayer.Style.SymbolCharacter := rStyle.MIStyle.Character;// FontColor; //;;//41;
         //   vFeature.Style.SymbolFontBackColor := miColorRed;

            x:=vFeature.LabelPoint.X;
            y:=vFeature.LabelPoint.y;

        //   vFeature.LabelPoint.Offset(x, y);

        //    vFeature.Style.TextFontColor  := miColorRed;

         //   vLayer.Style.SymbolFontColor := mapx_MakeColor(clRed);

  (*          vFeature.Style.SymbolFontShadow := True;
        //    vFeature.Style. SymbolFontShadow := True;
            x:=vFeature.LabelPoint.X;
            y:=vFeature.LabelPoint.y;
  *)
         end;


      miFeatureTypeLine:
         begin
           if not Eq(sObjName,'link')  then
              Continue;

           if not mapx_Dataset_FieldExists (vDataset, 'status_name') then
              Continue;


           vRowValues:=vDataset.RowValues[vFeature];

(*

           for k := 1 to vDataset.Fields.Count do
           begin
             s:=vDataset.Fields.Item(k).Name;

           end;
*)

//           mapx_getfi

           sValue:=AsString(mapx_RowValues_GetItemValue(vRowValues, 'status_name'));

//           sValue:=AsString(vRowValues.Item('status_name').Value);


            rStyle:=IOptions.GetDefaultStyle(sObjName);
            if rStyle.MIStyle.LineStyle=1 then
              rStyle.MIStyle.LineStyle:=miPenNone;

            mapx_SetMapXLayertStyle(vLayer, miFeatureTypeLine, rStyle.MIStyle);

(*            vLayer.Style.LineColor:=rStyle.MIStyle.LineColor;
            vLayer.Style.LineWidth:=rStyle.MIStyle.LineWidth;
            vLayer.Style.LineStyle:=rStyle.MIStyle.LineStyle;
*)

           iParamCount:=IOptions.GetParamCount(sObjName);

//           rStyle:=IOptions. GetParamByIndex(sObjName, j);


            for k := 0 to iParamCount - 1 do
            begin
              rStyle:=IOptions.GetParamByIndex(sObjName, k);


              if not Eq(rStyle.FieldValue, sValue) then
                Continue;

              if rStyle.MIStyle.LineStyle=1 then
                rStyle.MIStyle.LineStyle:=miPenNone;

            mapx_SetMapXLayertStyle(vLayer, miFeatureTypeLine, rStyle.MIStyle);

(*              vLayer.Style.LineColor:=rStyle.MIStyle.LineColor;
              vLayer.Style.LineWidth:=rStyle.MIStyle.LineWidth;
              vLayer.Style.LineStyle:=rStyle.MIStyle.LineStyle;
*)
            end;


//           vLayer.Style.LineColor:= miColorRed;


         //   j:=vFeature.Style.SymbolFontColor;

         end;


      end;

      vFeature.Style:=vLayer.Style;
      vFeature.Update(EmptyParam,EmptyParam);
    end;

    vLayer.EndAccess(miAccessEnd);

  end;


  CursorDefault();

  //vLayer.LabelProperties.Visible := True;

end;



class procedure TdmMapView_tools111111111.Init;
begin
  if not Assigned(dmMapView_tools) then
    dmMapView_tools := TdmMapView_tools.Create(Application);

end;

end.

