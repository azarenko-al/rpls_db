object frm_Map_tool_Liner: Tfrm_Map_tool_Liner
  Left = 1093
  Top = 400
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = #1051#1080#1085#1077#1081#1082#1072
  ClientHeight = 245
  ClientWidth = 699
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = True
  Position = poDefault
  Visible = True
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnKeyDown = FormKeyDown
  PixelsPerInch = 120
  TextHeight = 13
  object Panel2: TPanel
    Left = 0
    Top = 0
    Width = 321
    Height = 245
    Align = alLeft
    BevelOuter = bvNone
    TabOrder = 0
    object Panel3: TPanel
      Left = 0
      Top = 40
      Width = 321
      Height = 40
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox_LatLon: TGroupBox
        Left = 0
        Top = 0
        Width = 200
        Height = 40
        Align = alLeft
        Caption = #1064#1080#1088#1086#1090#1072', '#1044#1086#1083#1075#1086#1090#1072
        PopupMenu = PopupMenu_StatusBar
        TabOrder = 0
        object lb_LatLon: TLabel
          Left = 16
          Top = 17
          Width = 47
          Height = 13
          Caption = 'lb_LatLon'
        end
      end
      object GroupBox3: TGroupBox
        Left = 200
        Top = 0
        Width = 69
        Height = 40
        Align = alLeft
        Caption = #1042#1099#1089#1086#1090#1072
        TabOrder = 1
        object ed_Height: TComboEdit
          Left = 2
          Top = 17
          Width = 65
          Height = 21
          Align = alBottom
          GlyphKind = gkEllipsis
          ButtonWidth = 17
          NumGlyphs = 1
          TabOrder = 0
          OnClick = ed_HeightClick
        end
      end
    end
    object btnDelLat: TButton
      Left = 4
      Top = 88
      Width = 157
      Height = 25
      Caption = #1059#1076#1072#1083#1080#1090#1100' '#1087#1086#1089#1083#1077#1076#1085#1102#1102' (Esc)'
      TabOrder = 1
      OnClick = btnDelLatClick
      OnKeyDown = FormKeyDown
    end
    object btnNew: TButton
      Left = 168
      Top = 88
      Width = 97
      Height = 25
      Caption = #1053#1086#1074#1099#1081' '#1079#1072#1084#1077#1088
      TabOrder = 2
      OnClick = btnNewClick
      OnKeyDown = FormKeyDown
    end
    object Panel1: TPanel
      Left = 0
      Top = 0
      Width = 321
      Height = 40
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 3
      object GroupBox2: TGroupBox
        Left = 200
        Top = 0
        Width = 69
        Height = 40
        Align = alLeft
        Caption = #1040#1079#1080#1084#1091#1090
        TabOrder = 0
        object lb_Azimuth: TLabel
          Left = 9
          Top = 16
          Width = 51
          Height = 13
          Caption = 'lb_Azimuth'
        end
      end
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 200
        Height = 40
        Align = alLeft
        Caption = #1056#1072#1089#1089#1090#1086#1103#1085#1080#1077
        TabOrder = 1
        object lb_DistAll: TLabel
          Left = 96
          Top = 16
          Width = 43
          Height = 13
          Caption = 'lb_DistAll'
        end
        object lb_Dist: TLabel
          Left = 16
          Top = 16
          Width = 32
          Height = 13
          Caption = 'lb_Dist'
        end
      end
    end
  end
  object PopupMenu_StatusBar: TPopupMenu
    Left = 400
    Top = 16
    object btn_CK42: TMenuItem
      Caption = 'CK 42'
      Checked = True
      RadioItem = True
      OnClick = btn_CK42Click
    end
    object btn_CK95: TMenuItem
      Caption = 'CK 95'
      RadioItem = True
      OnClick = btn_CK42Click
    end
    object btn_WGS: TMenuItem
      Caption = 'WGS 84'
      RadioItem = True
      OnClick = btn_CK42Click
    end
    object btn_GCK: TMenuItem
      Caption = #1043#1057#1050' 2011'
      RadioItem = True
      OnClick = btn_CK42Click
    end
  end
  object FormStorage11: TFormStorage
    IniFileName = 'Software\Onega\MapLinear'
    Options = [fpPosition]
    StoredProps.Strings = (
      'btn_CK42.Checked'
      'btn_CK95.Checked'
      'btn_GCK.Checked'
      'btn_WGS.Checked')
    StoredValues = <
      item
        Name = 'qqq'
      end
      item
      end
      item
      end>
    Left = 400
    Top = 72
  end
end
