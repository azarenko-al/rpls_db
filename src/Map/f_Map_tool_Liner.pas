unit f_Map_tool_Liner;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, ComCtrls, rxPlacemnt,  ToolWin, StdCtrls,   

  I_MapAct,
  I_MapView,

  f_map_Tool_base,

  dm_Rel_Engine,

  u_Geo,
  u_const_msg,

  I_rel_Matrix1,

  u_func,
  u_func_msg,
  u_dlg, Mask, rxToolEdit, Menus

  ;

type

  Tfrm_Map_tool_Liner = class(Tfrm_Map_Tool_base)
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox_LatLon: TGroupBox;
    lb_LatLon: TLabel;
    GroupBox3: TGroupBox;
    btnDelLat: TButton;
    btnNew: TButton;
    Panel1: TPanel;
    GroupBox2: TGroupBox;
    lb_Azimuth: TLabel;
    GroupBox1: TGroupBox;
    lb_DistAll: TLabel;
    lb_Dist: TLabel;
    ed_Height: TComboEdit;
    PopupMenu_StatusBar: TPopupMenu;
    btn_CK42: TMenuItem;
    btn_CK95: TMenuItem;
    btn_WGS: TMenuItem;
    btn_GCK: TMenuItem;
    FormStorage11: TFormStorage;

    procedure FormDestroy (Sender: TObject);

    procedure FormCreate  (Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormKeyDown(Sender: TObject; var Key: Word;  Shift: TShiftState);
    procedure btnDelLatClick(Sender: TObject);
    procedure btnNewClick(Sender: TObject);
    procedure btn_CK42Click(Sender: TObject);
    procedure ed_HeightClick(Sender: TObject);

  private
    FCounter: integer;
    FIsProcessRelief: boolean;
    
    FBLPoints: TBLPointArrayF;
    FBLRect: TBLRect;
    FNextPoint, FBLCursor: TBLPoint;
    FDistance_All_m: Double;
    FDistance_m: double;

    procedure Init;
    procedure DelLastPoint;
    procedure Update_GroupBox_LatLon;

  protected
    procedure SetMapCursor (aBLCursor: TBLPoint); override;
  public
  //  IMapView: IMapViewX;

//    class function CreateSingleForm (aIMapView: IMapViewX): boolean; //(var aObjName, aFeatureName: string): boolean;

    class procedure ShowWindow (//aIMapView: IMapViewX;
                                aNextPoint: TBLPoint);//  aBLRect: TBLRect);
  end;


//=========================================================================
implementation {$R *.DFM}
//=========================================================================


//-------------------------------------------------------------------------
class procedure Tfrm_Map_tool_Liner.ShowWindow (//aIMapView: IMapViewX;
                             aNextPoint: TBLPoint);//  aBLRect: TBLRect);
//-------------------------------------------------------------------------
var
  oForm: TForm;
  vIMapView: IMapViewX;
begin
  if not Assigned(IActiveMapView) then
    Exit;



  oForm:= IsFormExists(Application, Tfrm_Map_tool_Liner.ClassName);

  if not Assigned(oForm) then
  begin
    vIMapView:=IActiveMapView;
    oForm:= Tfrm_Map_tool_Liner.Create(Application);
    Tfrm_Map_tool_Liner(oForm).SetIMapView(vIMapView);
  end;


  with Tfrm_Map_tool_Liner(oForm) do
  begin
//    SetIMapView(IActiveMapView);
 //   IMapView:=aIMapView;

    FNextPoint:= aNextPoint;
    FBLRect   := FIMapView.GetBounds;//aBLRect;

    if not geo_Eq(FNextPoint, NULL_BLPOINT) then
    begin
      Inc(FCounter);
//      SetLength(FBLPoints, FCounter);

      FBLPoints.Count:= FCounter;

      FBLPoints.Items[FCounter-1]:= aNextPoint;

      if not geo_Eq(FBLCursor, NULL_BLPOINT) then
//        FDistance_All_m:= FDistance_All_m + FDistance;
        FDistance_All_m:= FDistance_All_m + FDistance_m;

    Show;
  end;
  end;

end;

//------------------------------------------------------------------------------
procedure Tfrm_Map_tool_Liner.FormCreate(Sender: TObject);
//------------------------------------------------------------------------------
begin
  inherited;

(*  lb_Dist   .Caption:='';
  lb_DistAll.Caption:='';
  lb_Azimuth.Caption:='';
  lb_LatLon .Caption:='';

*)
  Height :=160;
  Width:= 366;


  Init;

end;

//------------------------------------------------------------------------------
procedure TFrm_Map_tool_Liner.Init;
//------------------------------------------------------------------------------
begin
  FCounter        :=0;
  FBLPoints.Count :=0;
  FDistance_All_m   :=0;
//  FDistance       :=0;
  FDistance_m       :=0;
  FNextPoint      := NULL_BLPOINT;

  lb_Dist   .Caption:=Format('%1.0f �', [0.0]);
  lb_DistAll.Caption:=Format('%1.0f �', [0.0]);
  lb_Azimuth.Caption:=Format('%1.0f �', [0.0]);

  lb_LatLon .Caption:='';//Format('%s',[geo_FormatBLPoint(NULL_BLPOINT)]);


  Update_GroupBox_LatLon();
end;

//------------------------------------------------------------------------------
procedure Tfrm_Map_tool_Liner.FormDestroy(Sender: TObject);
//------------------------------------------------------------------------------
begin
//  PostEvent(WE_MAP_DESTROY_USER_LAYER);
//  PostEvent(WE_MAP_SET_DEFAULT_TOOL);

  FIMapView.UserLayerDestroy;
  FIMapView.SetDefaultTool;// MAP_SET_DEFAULT_TOOL;

  inherited;
end;

//------------------------------------------------------------------------------
procedure TFrm_Map_tool_Liner.FormKeyDown(Sender: TObject; var Key: Word;  Shift: TShiftState);
//------------------------------------------------------------------------------
begin
  if Key=VK_ESCAPE then
    DelLastPoint;

  if Key=VK_SPACE then
    Key:= Key;
end;

//------------------------------------------------------------------------------
procedure TFrm_Map_tool_Liner.Update_GroupBox_LatLon;
//------------------------------------------------------------------------------
var
  s: string;
begin
  if btn_CK42.Checked  then s:='CK-42' else
  if btn_CK95.Checked  then s:='CK-95' else
  if btn_WGS.Checked then s:='WGS-84' else
  if btn_GCK.Checked   then s:='�CK-2011' else s:='';


  GroupBox_LatLon.Caption:= Format('������,������� (%s)', [s]);

end;


//------------------------------------------------------------------------------
procedure TFrm_Map_tool_Liner.DelLastPoint;
//------------------------------------------------------------------------------
begin
  if FCounter>0 then
  begin
    dec(FCounter);

    if FCounter>0 then
    begin
      FNextPoint:= FBLPoints.Items[FCounter-1];

      FDistance_m:= geo_Distance_m(FNextPoint, FBLPoints.Items[FCounter]);
     // FDistance_m:= geo_Distance_m(FNextPoint, FBLPoints.Items[FCounter]);
      FDistance_All_m:= FDistance_All_m-FDistance_m;

      FBLPoints.Count:= FCounter;


    end else
      Init;

    SetMapCursor(FBLCursor);//FNextPoint);
/////    ReUserLayerDraw;
  end;

  FIMapView.UserLayerDraw;
//  PostEvent(WE_MAP_DRAW_USER_LAYER);
end;

//------------------------------------------------------------------------------
procedure TFrm_Map_tool_Liner.btnDelLatClick(Sender: TObject);
//------------------------------------------------------------------------------
begin
  DelLastPoint;
end;

//------------------------------------------------------------------------------
procedure TFrm_Map_tool_Liner.btnNewClick(Sender: TObject);
//------------------------------------------------------------------------------
begin
  Init;
//  FNextPoint:= NULL_POINT;
  SetMapCursor(FBLCursor); //ReUserLayerDraw;

//  IMapView.UserLayerDestroy;
//  IMapView.Map_dr Draw
{ TODO : ! }
  FIMapView.UserLayerDraw;
//  PostEvent(WE_MAP_DRAW_USER_LAYER);
end;

procedure Tfrm_Map_tool_Liner.btn_CK42Click(Sender: TObject);
begin
  TMenuItem(Sender).Checked:=True;

  Update_GroupBox_LatLon();
end;

procedure Tfrm_Map_tool_Liner.ed_HeightClick(Sender: TObject);
begin
 if (FBLRect.TopLeft.B <> 0) and (FBLRect.BottomRight.B <> 0) and
     (FBLRect.TopLeft.L <> 0) and (FBLRect.BottomRight.L <> 0) then
  begin
    dmRel_Engine.Open();
//    dmRel_Engine.OpenByRect(FBLRect);
    FIsProcessRelief:= true;
  end else
    FIsProcessRelief:= false;

end;

//------------------------------------------------------------------------------
procedure Tfrm_Map_tool_Liner.SetMapCursor (aBLCursor: TBLPoint);
//------------------------------------------------------------------------------
  //------------------------------------
  procedure DoDrawLine (aBLPoint1, aBLPoint2: TBLPoint);
  //------------------------------------
  begin
    if Assigned(FIMapView) then
      FIMapView.MAP_ADD_USER_LINE(clRed, 2, aBLPoint1, aBLPoint2);

 {   PostEvent(WE_MAP_ADD_USER_LINE,
      [app_Par(PAR_COLOR, clRed),
       app_Par(PAR_WIDTH, 3),
       app_Par(PAR_LAT1, aBLPoint1.B),
       app_Par(PAR_LON1, aBLPoint1.L),
       app_Par(PAR_LAT2, aBLPoint2.B),
       app_Par(PAR_LON2, aBLPoint2.L)  ]);
       }

  end;

var
  i: integer;
  dDistance_All_m, dAzimuth: double;
  oRec: TRel_Record;
begin
  FBLCursor:= aBLCursor;

  lb_LatLon.Caption:=  Format('%s', [geo_FormatBLPoint(FBLCursor)]);

  if not geo_Eq(FNextPoint, NULL_BLPOINT) then
  begin
  //  FDistance:=     geo_Distance (FNextPoint, aBLCursor);
    FDistance_m:=     geo_Distance_m (FNextPoint, aBLCursor);
    dDistance_All_m:= FDistance_All_m + FDistance_m;

    dAzimuth     :=geo_Azimuth  (FNextPoint, FBLCursor);



//    lb_Dist.Caption:=    Format('%1.0f �', [FDistance]);
    lb_Dist.Caption:=    Format('%1.0f �', [FDistance_m]);
    lb_DistAll.Caption:= Format('%1.0f �', [dDistance_All_m]);
    lb_Azimuth.Caption:= Format('%1.0f �', [dAzimuth]);

    if FIsProcessRelief then
    begin
      if dmRel_Engine.FindPointBL (FBLCursor, oRec) then
        ed_Height.Text:=  Format('%d',       [oRec.Rel_H]);
    end;

    if FCounter > 1 then
      for i:= 1 to FCounter-1 do
        DoDrawLine(FBLPoints.Items[i-1], FBLPoints.Items[i]);

    DoDrawLine(FNextPoint, FBLCursor);

    FIMapView.UserLayerDraw;
///    PostEvent(WE_MAP_DRAW_USER_LAYER);

  end else
    lb_LatLon.Caption:=  Format('%s', [geo_FormatBLPoint(FBLCursor)]);
end;



//------------------------------------------------------------------------------
procedure Tfrm_Map_tool_Liner.FormKeyPress(Sender: TObject; var Key: Char);
//------------------------------------------------------------------------------
begin
  if Ord(Key) = VK_ESCAPE then
  begin
    FIMapView.UserLayerDestroy;
    FIMapView.SetDefaultTool;


//    PostEvent(WE_MAP_DESTROY_USER_LAYER);
  //  PostEvent(WE_MAP_SET_DEFAULT_TOOL);

    // alex  08.11.2007 17:59:34
    //FormDestroy(Self);
    Close;
  end;
end;

//------------------------------------------------------------------------------
end.






(*//------------------------------------------------------------------------------
procedure TFrm_Map_tool_Liner.ReUserLayerDraw;
//------------------------------------------------------------------------------
var
  I: Integer;
  dDistance_All, dAzimuth: Double;
begin
  lb_LatLon.Caption:=Format('%s',[geo_FormatBLPoint(FBLCursor)]);

  if FCounter>0 then
  begin
    FDistance    :=geo_Distance (FNextPoint, FBLCursor);
    dDistance_All:=FDistance_All+FDistance;
    dAzimuth     :=geo_Azimuth  (FNextPoint, FBLCursor);

    lb_Dist   .Caption:=Format('%1.0f �', [FDistance]);
    lb_DistAll.Caption:=Format('%1.0f �', [dDistance_All]);
    lb_Azimuth.Caption:=Format('%1.0f �', [dAzimuth]);
  end;

  if FCounter>0 then
    DrawLine(FNextPoint, FBLCursor);

  if FCounter > 1 then
    for i:= 1 to FCounter-1 do
      DrawLine(FBLPoints[i-1], FBLPoints[i]);

  PostEvent(WE_MAP_DRAW_USER_LAYER);
end;*)



{



//-------------------------------------------------------------------
procedure Tframe_MapView_base.RefreshStatusBar_new(aBLPoint: TBLPoint);
//-------------------------------------------------------------------
var
  iScale: integer;
//  dMapLengthInCm, dMapLengthInKm, dZoom, dMapX, dMapY: Double;

  bl: TblPoint;
  k: Integer;
  S: string;

 // xyPoint: TxyPoint;

const
  DEF_DELIMITER = '   ';

begin
  s:='';

  if btn_CK42.Checked then
  begin
    s:=s + 'CK-42: ' + geo_FormatBLPoint(aBLPoint) + DEF_DELIMITER;
  end;

  if btn_CK95.Checked then
  begin
    bl:= geo_Pulkovo42_to_CK_95 (aBLPoint);//dmOptions.GetGeoCoord);

    s:=s + 'CK-95: ' + geo_FormatBLPoint(bl) + DEF_DELIMITER;
  end;


  if btn_WGS.Checked then
  begin
    bl:= geo_Pulkovo42_to_WGS84 (aBLPoint);//dmOptions.GetGeoCoord);

    s:=s + 'WGS-84: ' + geo_FormatBLPoint(bl) + DEF_DELIMITER;
  end;

  if btn_GCK.Checked then
  begin
    CK42_to_GSK_2011 (aBLPoint.B, aBLPoint.L, bl.B, bl.L  );

   // bl:= geo_Pulkovo42_to_WGS84 (aBLPoint);//dmOptions.GetGeoCoord);

    s:=s + '�CK-2011: ' + geo_FormatBLPoint(bl) + DEF_DELIMITER;
  end;




  StatusBar1.SimpleText:=s;


  k:=StatusBar1.Canvas.TextWidth(s);

  StatusBar1.Panels[0].Width:=k;
  StatusBar1.Panels[0].Text:=s;


  dxStatusBar1.Panels[0].Width:=k;
  dxStatusBar1.Panels[0].Text:=s;


  UpdateZoom();


  StatusBar_Update_HeightInfo (aBLPoint);

end;
