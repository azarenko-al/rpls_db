unit fr_Map;

interface
//zzz {$I ver.inc}

{$DEFINE test11}


//{$DEFINE WMS}


uses
  Math,

  Windows,  ExtCtrls, ComCtrls, SysUtils,  Classes, Controls,   Variants,
  Forms,  StdCtrls, ActnList, Menus, TB2Item, TB2Dock, TB2Toolbar,
  dxBar, ImgList,   cxPropertiesStore, Dialogs,

  MapXLib_TLB,
  dm_Settings,

  u_cx,

  u_dxbar,
  u_run,
  u_types,

  i_WMS,

  u_Shell_new,

  u_Storage,

  u_Map_Classes,

  d_Map_desktops,

 // Clipbrd,
  dm_Map_Desktop,

  dm_Main,

  u_Object_Manager,

  f_Custom,

 // u_map_vars,

  d_Map_Selected,

  fr_MapView_base,
  fr_MapView_MIF,


  u_log,
  u_func,

  u_mapx_lib,
  u_mapx,

  u_mapx_func,
  u_db,
  u_Geo,
  u_reg,

  u_const_msg,
  u_func_msg,


  //d_Map_Show_Attributes,
 // f_Map_Tool_liner,


  ToolWin,  Mask, rxToolEdit, cxClasses, cxControls, cxContainer, cxEdit,
  cxTextEdit, cxMaskEdit, cxButtonEdit, cxLookAndFeels, rxPlacemnt,
  cxBarEditItem, dxSkinsCore, dxSkinsdxBarPainter, dxSkinsDefaultPainters,
  cxCheckBox, cxGraphics, cxLookAndFeelPainters, cxDropDownEdit,
  cxLookupEdit, cxDBLookupEdit, cxDBLookupComboBox  ;



type

  Tframe_Map = class(TForm, IMessageHandler)
    ActionList_map: TActionList;
    MapImages: TImageList;
    act_Zoom_in: TAction;
    act_Zoom_out: TAction;
    act_Zoom_out_2: TAction;
    act_Zoom_out_4: TAction;
    act_Setup_Layers: TAction;
    act_Map_Open: TAction;
    act_Map_Escape: TAction;
    TBDock1: TTBDock;
    TBItem1: TTBItem;
    TBItem2: TTBItem;
    TBSubmenuItem1000: TTBSubmenuItem;
    TBItem4000: TTBItem;
    TBItem5000: TTBItem;
    TBSeparatorItem1000: TTBSeparatorItem;
    TBItem6000: TTBItem;
    TBSeparatorItem2: TTBSeparatorItem;
    TBSeparatorItem5: TTBSeparatorItem;
    TBToolbar_map: TTBToolbar;
    TBToolbar_WMS: TTBToolbar;
    act_Desktop_Setup: TAction;
    TBSubmenuItem4000: TTBSubmenuItem;
    TBSubmenuItem5000: TTBSubmenuItem;
    btn_Zoom_in000: TTBItem;
    TBSeparatorItem7000: TTBSeparatorItem;
    TBItem20000: TTBItem;
    TBItem21000: TTBItem;
    TBSeparatorItem9000: TTBSeparatorItem;
    btn_Setup_Layers000: TTBItem;
    TBSeparatorItem3: TTBSeparatorItem;
    pn_Main: TPanel;
    act_Liner: TAction;
    btn_Liner000: TTBItem;
    TBItem18000: TTBItem;
    TBSeparatorItem12000: TTBSeparatorItem;
    act_Zoom_Pan_move: TAction;
    act_Zoom_Plan: TAction;
    TBItem13: TTBItem;
    act_Map_Refresh: TAction;
    TBItem17000: TTBItem;
    TBItem119: TTBItem;
    act_PrintToFile: TAction;
    TBItem34000: TTBItem;
    TBSeparatorItem8000: TTBSeparatorItem;
    TBItem44000: TTBItem;
    act_Map_ObjectInfo: TAction;
    act_Map_Show_Feature: TAction;
    act_Setup_Layers_System: TAction;
    TBItem111000: TTBItem;
    TBItem45000: TTBItem;
    act_Map_Select_Group: TAction;
    PopupMenu1: TPopupMenu;
    TBSubmenuItem21000: TTBSubmenuItem;
    TBItem9000: TTBItem;
    act_Map_Unselect: TAction;
    TBItem4: TTBItem;
    TBSeparatorItem1: TTBSeparatorItem;
    TBItem3: TTBItem;
    act_Map_RunMapinfoWorkspace: TAction;
    act_Map_Ground: TAction;
    TBItem6: TTBItem;
    TBItem7: TTBItem;
    act_MapX_Version: TAction;
    TBSeparatorItem4: TTBSeparatorItem;
    TBItem5: TTBItem;
    dxBarManager1: TdxBarManager;
    dxBarButton1: TdxBarButton;
    dxBarSubItem2: TdxBarSubItem;
    dxBarButton2: TdxBarButton;
    dxBarButton3: TdxBarButton;
    dxBarButton4: TdxBarButton;
    dxBarButton7: TdxBarButton;
    dxBarButton16: TdxBarButton;
    dxBarButton17: TdxBarButton;
    dxBarButton18: TdxBarButton;
    TBItem18: TTBItem;
    TBSeparatorItem7: TTBSeparatorItem;
    TBItem20: TTBItem;

    act_Map_Select_Group_by_circle: TAction;
    act_Map_Select_Group_by_points: TAction;
    act_Map_Select_Group_by_region: TAction;


    act_Zoom_500000: TAction;
    act_Zoom_1000000: TAction;
    act_Zoom_Custom: TAction;
    act_Zoom_10000: TAction;
    act_Zoom_100000: TAction;
    act_Zoom_200000: TAction;
    TBItem8: TTBItem;
    TBSeparatorItem8: TTBSeparatorItem;
    TBItem21: TTBItem;
    TBSeparatorItem9: TTBSeparatorItem;
    TBItem34: TTBItem;
    TBItem36: TTBItem;
    TBItem44: TTBItem;
    TBItem45: TTBItem;
    TBItem46: TTBItem;
    TBItem47: TTBItem;
    TBSeparatorItem11: TTBSeparatorItem;
    TBItem48: TTBItem;
    TBItem49: TTBItem;
    TBItem50: TTBItem;
    TBItem51: TTBItem;
    TBItem52: TTBItem;
    TBSeparatorItem12: TTBSeparatorItem;
    act_Zoom_50000: TAction;
    TBToolbar1: TTBToolbar;
    TBItem10: TTBItem;
    TBItem11: TTBItem;
    cb_WMS: TCheckBox;
    TBControlItem1: TTBControlItem;
    FormStorage_Map_frame: TFormStorage;
    TBSubmenuItem_Z: TTBSubmenuItem;
    act_WMS_log: TAction;
    TBItem9111: TTBItem;
    dxBarManager1Bar2222: TdxBar;
    dxBarManager1Bar_WMS: TdxBar;
    TBItem_PropertyPage: TTBItem;
    act_map_PropertyPage: TAction;
    cb_WMS_: TcxBarEditItem;
    dxBarButton19: TdxBarButton;
    dxBarSubItem_WMS_Z: TdxBarSubItem;
    dxBarButton_WMS_log: TdxBarButton;
    dxBarButton2011: TdxBarButton;
    dxBarButton2111: TdxBarButton;
    dxBarButton2211: TdxBarButton;
    dxBarButton2311: TdxBarButton;
    dxBarButton2411: TdxBarButton;
    dxBarButton5: TdxBarButton;
    dxBarButton25: TdxBarButton;
    dxBarSubItem3: TdxBarSubItem;
    dxBarButton26: TdxBarButton;
    dxBarButton27: TdxBarButton;
    dxBarButton28: TdxBarButton;
    dxBarButton29: TdxBarButton;
    dxBarButton31: TdxBarButton;
    dxBarButton32: TdxBarButton;
    dxBarButton33: TdxBarButton;
    dxBarButton34: TdxBarButton;
    dxBarButton351_z1: TdxBarButton;
    dxBarButton362_z2: TdxBarButton;
    dxBarButton373_z3: TdxBarButton;
    dxBarButton384_z4: TdxBarButton;
    dxBarManager1Bar1_map1: TdxBar;
    dxBarButton6_z5: TdxBarButton;
    dxBarButton9_z6: TdxBarButton;
    dxBarButton10_z7: TdxBarButton;
    dxBarButton11_z8: TdxBarButton;
    dxBarButton12_z9: TdxBarButton;
    dxBarButton13_z10: TdxBarButton;
    dxBarButton6_z11: TdxBarButton;
    dxBarButton8_z12: TdxBarButton;
    dxBarButton9_z13: TdxBarButton;
    dxBarButton10_z14: TdxBarButton;
    dxBarButton11_z15: TdxBarButton;
    dxBarButton6_z16: TdxBarButton;
    dxBarButton6_z17: TdxBarButton;
    dxBarButton8_z18: TdxBarButton;
    dxBarButton6_z19: TdxBarButton;
    cxLookAndFeelController1: TcxLookAndFeelController;
    dxBarButton6: TdxBarButton;
    Panel1: TPanel;
    Button1: TButton;
    Button2: TButton;
    dxBarButton8: TdxBarButton;
    dxBarButton9: TdxBarButton;
    dxBarButton10: TdxBarButton;
    TBControlItem3: TTBControlItem;
    cxLookupComboBox_WMS: TcxLookupComboBox;
    cxBarEditItem_WMS: TcxBarEditItem;


procedure  TBItemClick_Z(Sender: TObject);


    procedure ActionList_mapUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure Button2Click(Sender: TObject);
    procedure cb_WMSClick(Sender: TObject);

    procedure cxLookupComboBox_WMSClick(Sender: TObject);
    procedure dxBarButton351_z1Click(Sender: TObject);
//    procedure dxBarCombo_WMSChange(Sender: TObject);
//    procedure dxBarCombo_WMSClick(Sender: TObject);
//    procedure ed_WMSButtonClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure _Actions(Sender: TObject);
 //   procedure cb_WorkspaceCloseUp(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure ed_DesktopButtonClick(Sender: TObject; AbsoluteIndex: Integer);
//    procedure cb_DesktopClick(Sender: TObject);
    procedure ed_DesktopClick(Sender: TObject);
//    procedure TBItem55Click(Sender: TObject);
//    procedure ed_WMSButtonClick(Sender: TObject);
//    procedure ed_WMSChange(Sender: TObject);
//    procedure TBItemClick_Z(Sender: TObject);
//    procedure cb_ShowHintsClick(Sender: TObject);
//    procedure FormActivate(Sender: TObject);
 //   procedure TBDock1ContextPopup(Sender: TObject; MousePos: TPoint; var Handled:
    //    Boolean);
//    procedure Button1Click(Sender: TObject);
(*    procedure mnu_ActionsPopup(Sender: TTBCustomItem; FromLink: Boolean);
    procedure mnu_ActionsClick(Sender: TObject);*)
  private
//    FProcIndex: integer;

//    FMapViewName: string;


    FWorkspaceFileName: string;
    procedure refresh_WMS;
    procedure WMS_change;
//    procedure DoOnWmsSelected(Sender: TObject);
  //  function Count1: integer;
  protected
    FRegPath: string;


//    FOnEscape           : TNotifyEvent;
    FOnCursor           : TOnMapCursorEvent;
    FOnVector           : TOnMapViewVectorEvent;
    FOnDraftVector      : TOnMapViewVectorEvent;
    FOnFixedPoint       : TOnMapViewVectorEvent;
    FOnRect             : TOnMapViewRectEvent;
    FOnPoly             : TOnMapViewPolyEvent;
    FOnMapViewPoly      : TOnMapViewPolyEvent;
    FOnRing             : TOnMapViewRingEvent;
    FOnPoint            : TOnMapViewPointEvent;
    FOnMouseDown        : TOnMapViewMouseDownEvent;

//    procedure GetMessage(aMsg: integer; aParams: TEventParamList; var aHandled:  Boolean); virtual;
    procedure GetMessage(aMsg: TEventType; aParams: TEventParamList ; var aHandled:
        boolean); virtual;


    procedure SetWindowTool (Value: TMapWindowTool); virtual;
//    procedure ChangeDesktop (aMapDesktopID: integer);

    procedure DoOnPoint  (aBLPoint: TBLPoint); virtual;
    procedure DoOnRect   (aBLRect: TBLRect); virtual;
    procedure DoOnVector (aBLVector: TBLVector); virtual;
    procedure DoOnFixedPoint (aBLVector: TBLVector); virtual;
    procedure DoOnCursor (aBLPoint: TBLPoint);
    procedure DoOnEscape (Sender: TObject); virtual;
    procedure DoOnPoly   (var aBLPoints: TBLPointArrayF); virtual;
    procedure DoOnRing   (aCenter: TBLPoint; aRadius: double); virtual;
    procedure DoOnLog    (aMsg: string);

    procedure DoOnMapDblClick(Sender: tobject);
    procedure DoOnMapObjectClick(Sender: tobject);

    procedure DoOnMapViewChanged(Sender: tobject);

    function GetMapViewRegistryRoot: string;

//    procedure MapDelByID(aID: Integer);

  protected
    Ffrm_MapView: Tframe_MapView_MIF;
//    FMapDesktopID: integer;

  public
    WorkspaceName: string;

    //; aDefaultBLRect: TBLRect
    procedure SetMapViewName2;
    function GetProjectMapDir: string;

    procedure SetBounds (aBLRect: TBLRect);

    procedure SetCenter (aBLPoint: TBLPoint);

    procedure AlignBoundsForBLVector (aBLVector: TBLVector);
    procedure SetBoundsWithOffset (aBLRect: TBLRect);

    procedure Log (aMsg: string);

    procedure ZoomInBLRect(aRect: TBLRect);
    procedure ZoomOut (aTimes: integer);
    procedure ZoomPlan;

    procedure MapRefresh;

    //dialogs
    procedure DlgMapAdd ();
    procedure DlgLayers;

    procedure MapAdd(aFileName: string);
    procedure Clear;
    procedure CloseData;
    function Count: Integer;

    procedure MapDelByFileName(aFileName: string);
    procedure MapDelByMask(aMask: string);

    procedure SetDefaultTool;

    procedure SaveWorkspaceFile ();

// TODO: RefreshObjectStyles
//  procedure RefreshObjectStyles;

  //  class function CreateForm: Tframe_Map;


//    procedure LoadMapsFromXML (aXML: string);
    function  MapExists (aFileName: string): boolean;
//    procedure MapDel(aFileName: string);

    property  WindowTool : TMapWindowTool write SetWindowTool;
    property  OnCursor   : TOnMapCursorEvent        read FOnCursor  write FOnCursor;
    property  OnRect     : TOnMapViewRectEvent     read FOnRect           write FOnRect;
    property  OnPoly     : TOnMapViewPolyEvent     read FOnPoly    write FOnPoly;
    property  OnRing     : TOnMapViewRingEvent     read FOnRing           write FOnRing;
    property  OnPoint    : TOnMapViewPointEvent read FOnPoint write FOnPoint;
    property  OnVector   : TOnMapViewVectorEvent   read FOnVector         write FOnVector;
    property  OnDraftVector: TOnMapViewVectorEvent  read FOnDraftVector write FOnFixedPoint;//FOnDraftVector;
    property  OnFixedPoint:  TOnMapViewVectorEvent   read FOnFixedPoint write FOnFixedPoint;
    property  OnMouseDown: TOnMapViewMouseDownEvent read FOnMouseDown write FOnMouseDown;
//    property  OnEscape   : TNotifyEvent read FOnEscape write FOnEscape;
  end;


//==================================================================
implementation

uses dm_MapEngine_store, DB;  {$R *.dfm}
//==================================================================

const

  STR_MAP_Zoom_Pan   = '�����������';
  STR_MAP_Zoom_In    = '����������';
  STR_MAP_Zoom_Out   = '��������';
  STR_MAP_Zoom_Out_2 = '�������� x 2';
  STR_MAP_Zoom_Out_4 = '�������� x 4';
//  STR_MAP_Zoom_Out_8 = '�������� x 8';
  STR_MAP_Zoom_Plan  = '����';
  STR_MAP_LINER      = '�������';

  MODULE_NAME = 'Map: ';


procedure Tframe_Map.ActionList_mapUpdate(Action: TBasicAction; var Handled:
    Boolean);
begin
//  ComboBox_WMS.Enabled:=cb_WMS.Checked;
  TBSubmenuItem_Z.Enabled:=cb_WMS.Checked;
  dxBarSubItem_WMS_Z.Enabled:=cb_WMS.Checked;

//  lb_Z.Visible:=cb_WMS.Checked;

end;
 

//--------------------------------------------------------------------
procedure Tframe_Map.FormCreate(Sender: TObject);
//--------------------------------------------------------------------
var
 oBItem: TTBItem;


  I: Integer;
  s: WideString;

begin
  inherited;

  TdmMap_Desktop.Init;

  bar_Init (dxBarManager1);


//  TBSubmenuItem_Z.Add();


  pn_Main.Align:=alClient;
  pn_Main.BorderStyle:=bsNone;

//  Ffrm_MapView:= Tframe_MapView_MIF.CreateChildForm (pn_Main);

 // Application.ProcessMessages;

  CreateChildForm(Tframe_MapView_MIF, Ffrm_MapView, pn_Main);

 // CopyControls (Ffrm_MapView, pn_Main);

 // IMapView:=Ffrm_MapView;

  Ffrm_MapView.OnCursor     :=DoOnCursor;
  Ffrm_MapView.OnPoint      :=DoOnPoint;

  Ffrm_MapView.OnVector     :=DoOnVector;
//  Ffrm_MapView.OnDraftVector:=OnDraftVector;
  Ffrm_MapView.OnDraftVector:=DoOnFixedPoint;//////OnDraftVector;

  Ffrm_MapView.OnRect       :=DoOnRect;
  Ffrm_MapView.OnRing       :=OnRing;
  Ffrm_MapView.OnPoly       :=DoOnPoly;
  Ffrm_MapView.OnMouseDown  :=OnMouseDown;
 // Ffrm_MapView.OnEscape     :=DoOnEscape;
  Ffrm_MapView.OnRing       :=DoOnRing;

  Ffrm_MapView.OnLog        :=DoOnLog;

  Ffrm_MapView.OnDblClick   := DoOnMapDblClick;
  Ffrm_MapView.OnObjectClick:= DoOnMapObjectClick;

  Ffrm_MapView.OnMapViewChanged:=DoOnMapViewChanged;

//  DoOnMapViewChanged


  // const str --------------------------
  act_Zoom_Pan_move.Caption:=STR_MAP_Zoom_Pan;
  act_Zoom_In.Caption     :=STR_MAP_Zoom_IN;
  act_Zoom_Out.Caption    :=STR_MAP_Zoom_Out;
  act_Zoom_Out_2.Caption  :=STR_MAP_Zoom_Out_2;
  act_Zoom_Out_4.Caption  :=STR_MAP_Zoom_Out_4;
//  act_Zoom_out_8.Caption:=STR_MAP_Zoom_Out_8+'-------';

  act_Zoom_Plan.Caption   :=STR_MAP_Zoom_Plan;

  act_Liner.Caption       :=STR_MAP_LINER;
  act_Map_Open.Caption    :='�������...';

  act_PrintToFile.Caption :='������ ����� � ����';


  act_Map_ObjectInfo.Caption :='���������� �� �������';


//  act_Desktop_Setup.Caption :='���������';
  act_Desktop_Setup.Caption :='����';


//  act_Map_set_projection.Caption :='���������� �������� ...';

{
  with StatusBar11 do begin
    Panels[0].Width:=85;
    Panels[1].Width:=85;
  end;
}

//  ed_Desktop.Text:=gl_DB.GetNameByID(TBL_Map_Desktops, FMapDesktopID);

//  ed_Desktop.Text:= dmMap_Desktop.GetNameByID (FMapDesktopID);


 // FRegPath:=REGISTRY_ROOT+ClassName+'\';


  FRegPath:= g_Storage.GetPathByClass(ClassName);



 // cb_ShowHints.Checked:= gl_Reg.RegIni.ReadBool(Name, cb_ShowHints.Name, true);
 // cb_ShowHints.Checked:= reg_ReadBool(FRegPath, cb_ShowHints.Name, true);

 // Ffrm_MapView.MapHintingEnabled:= cb_ShowHints.Checked;


  act_Map_Select_Group_by_region.Caption:='��������� �� ��������������';
  act_Map_Select_Group_by_circle.Caption:='��������� �� ����������';
  act_Map_Select_Group_by_points.Caption:='��������� �� ��������';


//  FProcIndex:=g_EventManager.RegisterProc (GetMessage, Name);

//  Ffrm_MapView.OnWmsSelected:=DoOnWmsSelected;

//  ed_WMS.Text:= Ffrm_MapView.WMS.LayerName;



// {$IFDEF wms}
    TBSubmenuItem_Z.Clear;

    for I := 1 to 20 do    // Iterate
    begin
      oBItem:=TTBItem.create(TBSubmenuItem_Z);
      oBItem.Caption:= Format('z %d', [i]);
      oBItem.Tag:=i;
      oBItem.GroupIndex:=1;

//      oBItem.RadioItem:=False;

      oBItem.OnClick:=TBItemClick_Z;

      TBSubmenuItem_Z.Add(oBItem);


    //  TBSubmenuItem_Z.Add();
     // oBItem:=Sender as TTBItem;

    //  e:=_GetScaleByTileZ (oBItem.Tag);

    end;    // for


    // ---------------------------------------------------------------
  //  dxBarCombo_WMS.Items.Clear;

   {
    Ffrm_MapView.FTileManager.Get_Layers_text(s);
    ComboBox_WMS.Items.Text:=s;

     ComboBox_WMS.ItemIndex:=2;
     }

    // ---------------------------------------------------------------
{
    ComboBox_WMS.Items.Clear;

    for I := 0 to High(DEF_WMS) do    // Iterate
      ComboBox_WMS.Items.AddObject (DEF_WMS[i].Caption, Pointer(i));

    ComboBox_WMS.ItemIndex:=4;
}


   TBToolbar_WMS.Visible:=True;


   // ---------------------------------------------------------------
   // WMS
   // ---------------------------------------------------------------

   TdmSettings.Init.Open_WMS;

   cxLookupComboBox_WMS.Properties.ListSource:=dmSettings.ds_WMS;
   cxLookupComboBox_WMS.Properties.KeyFieldNames:='name';
   cxLookupComboBox_WMS.Properties.ListFieldNames:='caption';

   cxLookupComboBox_WMS.EditValue:=dmSettings.WMS_Get_Default_Map_Name;
   Assert(cxLookupComboBox_WMS.EditValue<>'');
//   cxLookupComboBox1.EditValue:=dmSettings.ds_WMS.DataSet['name'];


//  if WMS.Active then

   WMS_change;

   TcxLookupComboBoxProperties(cxBarEditItem_WMS.Properties).ListSource:=dmSettings.ds_WMS;
   TcxLookupComboBoxProperties(cxBarEditItem_WMS.Properties).KeyFieldNames:='name';
   TcxLookupComboBoxProperties(cxBarEditItem_WMS.Properties).ListFieldNames:='caption';

//   cxBarEditItem_WMS.KeyFieldNames:='name';
//   cxBarEditItem_WMS.ListFieldNames:='caption';

   cxBarEditItem_WMS.EditValue:=dmSettings.WMS_Get_Default_Map_Name;
//   Assert(cxLookupComboBox_WMS.EditValue<>'');



// {$ELSE}
//   TBToolbar_WMS.Visible:=False;
// {$ENDIF}


//  lb_Z.Caption:='';
end;



//--------------------------------------------------------------------
procedure Tframe_Map.FormDestroy(Sender: TObject);
//--------------------------------------------------------------------
begin
//  g_EventManager.UnRegisterProc (FProcIndex, GetMessage);


 // gl_Reg.

  if FWorkspaceFileName <> '' then
    Ffrm_MapView.WorkspaceSave (FWorkspaceFileName);


  FreeAndNil(Ffrm_MapView);

//  IMapView:=nil;
 // reg_WriteBool(FRegPath, cb_ShowHints.Name, cb_ShowHints.Checked);

 // Ffrm_MapView.Free;

  inherited;
end;



procedure Tframe_Map.SetDefaultTool;
begin
  SetWindowTool (toolObject);
end;


procedure Tframe_Map.ZoomPlan;
var
  blRect: TBLRect;

begin
  Ffrm_MapView.ZoomPlan;

  blRect:=Ffrm_MapView.GetPlanBounds();

  Log(geo_FormatBLRect(blRect));
  SetBounds (blRect);

//  mapx_


end;

procedure Tframe_Map.ZoomInBLRect(aRect: TBLRect);
begin
  SetBounds (aRect);
end;

procedure Tframe_Map.ZoomOut(aTimes: integer);
begin
  SetBounds (geo_ZoomOut(Ffrm_MapView.GetBounds(), aTimes));
end;


procedure Tframe_Map.Log (aMsg: string);
begin
  g_Log.Add(MODULE_NAME + aMsg);
end;


procedure Tframe_Map.SaveWorkspaceFile ();
begin
  if FWorkspaceFileName <> '' then
    Ffrm_MapView.WorkspaceSave (FWorkspaceFileName);
end;



//------------------------------------------------------------
procedure Tframe_Map.DoOnCursor (aBLPoint: TBLPoint);
//------------------------------------------------------------
begin

  if Assigned(FOnCursor) then
    FOnCursor (aBLPoint);
end;


procedure Tframe_Map.SetWindowTool (Value: TMapWindowTool);
begin
  Ffrm_MapView.WindowTool:=Value;

end;


function Tframe_Map.GetMapViewRegistryRoot(): string; // (aMapDesktopID: integer): string;
begin
  Result:=Format('Projects\%d\MapView\', [dmMain.ProjectID] );
end;



//--------------------------------------------------------------------
procedure Tframe_Map.CloseData;
//--------------------------------------------------------------------
var
  i: Integer;
begin
  Clear;

//  with StatusBar11 do
//    for i:=0 to Panels.Count-1 do  Panels[i].Text:='';

//   FMapDesktopID := 0;
   FWorkspaceFileName := '';
end;



//--------------------------------------------------------------------
procedure Tframe_Map.SetMapViewName2;
//--------------------------------------------------------------------

var
  bExists: Boolean;
  blRect: TblRect;
  i: integer;
  s : string;
 // sName: string;
//  FMapDesktopID: string;
begin
//  Assert(dmMain.ProjectID>0, 'Value <=0');


//  aMapViewName:='map_default' ;


  if FWorkspaceFileName<>'' then
    Ffrm_MapView.WorkspaceSave (FWorkspaceFileName);




//  FMapViewName:=aMapViewName;
                                                                                                               
  Clear;



 // if
 // dmMap_Desktop.GetNameByID_new (FMapDesktopID, sName);// then
//  ed_Desktop.Text:=sName;

 ////// ed_Desktop.Text:= gl_DB.GetNameByID (TBL_Map_Desktops, FMapDesktopID);


//  if aMapViewName<>'' then
//  begin
//    FWorkspaceFileName:= GetProjectMapDir() + 'geo5\map_default.gst';
    FWorkspaceFileName:= GetProjectMapDir() + 'map.gst';

//    Ffrm_MapView.StatusBar11.Panels[3].text:=FWorkspaceFileName;

  //  StatusBar.Panels[2].Text:= FWorkspaceFileName;

    bExists:= FileExists ( FWorkspaceFileName);

    if bExists then
      Ffrm_MapView.WorkspaceLoad (FWorkspaceFileName);


    try
    //  Result:=

(*    if not geo_Eq(aDefaultBLRect, NULL_RECT) then
      Ffrm_MapView.SetBoundsWithOffset (aDefaultBLRect);
*)
    except
   //   Result:=false;
    end;                                                                                                         




  if not bExists then
  begin
    dmMap_Desktop.Open ();
    dmMap_Desktop.MapDesktopItem_Init2 ();
    Ffrm_MapView.LoadMapsFromclass (dmMap_Desktop.MapDesktopItem );

    Ffrm_MapView.ZoomPlan;
  end else
  begin
    blRect:=Ffrm_MapView.GetPlanBounds();

    Ffrm_MapView.LoadMaps_Update;


    blRect:=Ffrm_MapView.GetPlanBounds();

    if blRect.TopLeft.B=0 then
    begin

    end;

  
  end;


end;




//--------------------------------------------------------------------
procedure Tframe_Map._Actions(Sender: TObject);
//--------------------------------------------------------------------

begin
//  oIDList:= TIDList.Create;

  if Sender=act_map_PropertyPage then
    Ffrm_MapView.Map.PropertyPage
  else


  if Sender=act_WMS_log then
    Ffrm_MapView.WMS_log
  else

  if Sender=act_Map_RunMapinfoWorkspace then
    Ffrm_MapView.RunMapinfoWorkspace else


  if sender = act_MapX_Version then
    u_mapx_func.mapx_Dlg_Ver (Ffrm_MapView.Map)  else

{
    MsgDlg(Format('������ MapX: %s', [gl_Map.Ffrm_MapView.Map.Version]));
  end else

ShowMessage(Format('������ MapX: %s', [gl_Map.Ffrm_MapView.Map.Version]));
}

//  if Sender=act_Map_set_projection then
//     mapx_SetMapDefaultProjection(Ffrm_MapView.Map) else
                                           

  if Sender=act_Zoom_Pan_move then WindowTool:=toolPan else

  //---------------------------------------------------------
  // ZOOM
  //---------------------------------------------------------

  if Sender=act_Zoom_In       then WindowTool:=toolZoomIn else
  if Sender=act_Zoom_Out      then WindowTool:=toolZoomOut else
  if Sender=act_Zoom_Out_2    then begin WindowTool:=toolZoomOut; ZoomOut (2); end else
  if Sender=act_Zoom_Out_4    then begin WindowTool:=toolZoomOut; ZoomOut (4); end else
//  if Sender=act_Zoom_Out_8    then ZoomOut (8) else
  if Sender=act_Zoom_Plan     then ZoomPlan else






  if Sender=act_Zoom_10000    then Ffrm_MapView.SetScale(10000 div 100)    else
//  if Sender=act_Zoom_25000    then Ffrm_MapView.SetZoom(25000)    else
  if Sender=act_Zoom_50000    then Ffrm_MapView.SetScale(50000 div 100)    else
  if Sender=act_Zoom_100000   then Ffrm_MapView.SetScale(100000 div 100)   else
  if Sender=act_Zoom_200000   then Ffrm_MapView.SetScale(200000 div 100)   else
  if Sender=act_Zoom_500000   then Ffrm_MapView.SetScale(500000 div 100)   else
  if Sender=act_Zoom_1000000  then Ffrm_MapView.SetScale(1000000 div 100)  else

  if Sender=act_Zoom_Custom   then Ffrm_MapView.SetCustomZoom_Dlg     else


  if Sender=act_Liner then
  begin
    Ffrm_MapView.Dlg_Liner();

    
    { TODO : ! }
//    Ffrm_MapView.Dl
 //   Tfrm_Map_tool_Liner.ShowWindow(Ffrm_MapView, NULL_POINT);//, Ffrm_MapView.GetBounds());
    WindowTool:=toolLiner;
  end  else

  if Sender=act_Map_ObjectInfo      then begin
//   Tfrm_Map_tool_Info.ShowWindow (NULL_POINT, GetBounds);
    WindowTool:=toolInfo;
  end  else

  if Sender=act_Map_Open      then Ffrm_MapView.Dlg_MapAdd() else
  if Sender=act_Map_Escape    then DoOnEscape(nil) else


  //-----------------------------
  // Map_Select
  //-----------------------------
  if Sender=act_Map_Select_Group  then WindowTool:=toolSelect else
  if Sender=act_Map_UnSelect  then Ffrm_MapView.ClearSelection else

  if Sender=act_Map_Select_Group_by_region  then WindowTool:=toolSelectRect else
  if Sender=act_Map_Select_Group_by_circle  then WindowTool:=toolSelectRadius else
  if Sender=act_Map_Select_Group_by_points  then WindowTool:=toolSelectPolygon else



  if Sender=act_Map_Refresh    then MapRefresh else

  if Sender=act_Setup_Layers        then DlgLayers else
  if Sender=act_Setup_Layers_System then Ffrm_MapView.Dlg_SystemLayers else


  if Sender=act_PrintToFile then
    Ffrm_MapView.Dlg_PrintToFile else

 // if Sender=act_Print_to_file then
 //   Tdlg_MapX_Print.ExecDlg(Ffrm_MapView.Map1) else


 // if Sender=act_Map_Show_Feature then
 //   TDlg_Map_Show_Attributes.CreateSingleForm() else


//  if Sender=act_Map_Previous_Tool then Ffrm_MapView.PreviousTool else

                     

  if Sender=act_Desktop_Setup then
  begin
   // Assert(Assigned(IMapDesktop));

      Tdlg_Map_desktops.CreateSingleForm( Ffrm_MapView.Map );


//    dmAct_MapDesktop.Dlg_Map_desktop (FMapDesktopID);//, sDesktopName);

  end else

end;



// -------------------------------------------------------------------
procedure Tframe_Map.SetBounds(aBLRect: TBLRect);
// -------------------------------------------------------------------

begin
  Ffrm_MapView.SetBounds (aBLRect);
end;



procedure Tframe_Map.Clear;
begin
  Ffrm_MapView.Clear;
end;


function Tframe_Map.Count: Integer;
begin
  result := Ffrm_MapView.Count;
end;



//function Tframe_Map.Count1: integer;
//begin
// //zzzzzzzzzzzzzzzz Result:=Ffrm_MapView.Count;
//end;

procedure Tframe_Map.MapAdd(aFileName: string);
begin
  Ffrm_MapView.MapAddSimple (aFileName );
end;

{
procedure Tframe_Map.MapDelByID(aID: Integer);
begin
  Ffrm_MapView.MapDelByID (aID);
end;
}


procedure Tframe_Map.MapDelByFileName(aFileName: string);
begin
  Ffrm_MapView.MapDelByFileName (aFileName);
end;




function Tframe_Map.MapExists(aFileName: string): boolean;
begin
  Result:=Ffrm_MapView.GetMapLayerIndexByFileName_from_1(aFileName)>=1;
end;

procedure Tframe_Map.DlgLayers;
begin
  Ffrm_MapView.Dlg_Layers;
end;

procedure Tframe_Map.DlgMapAdd();
begin
  Ffrm_MapView.Dlg_MapAdd();
end;

procedure Tframe_Map.MapRefresh;
begin
  Ffrm_MapView.MapRefresh;
end;


//-------------------------------------------------------------------
// don't remove
//-------------------------------------------------------------------
procedure Tframe_Map.DoOnLog (aMsg: string);
begin
  Log (aMsg);
end;


procedure Tframe_Map.DoOnPoint(aBLPoint: TBLPoint);
begin
end;

procedure Tframe_Map.DoOnRect(aBLRect: TBLRect);
begin
  if Assigned(FOnRect) then
    FOnRect (aBLRect);
end;

procedure Tframe_Map.DoOnPoly(var aBLPoints: TBLPointArrayF);
begin
  if Assigned(FOnPoly) then
    FOnPoly (aBLPoints);
end;


procedure Tframe_Map.DoOnRing(aCenter: TBLPoint; aRadius: double);
begin
  if Assigned(FOnRing) then
    FOnRing (aCenter, aRadius);
end;



procedure Tframe_Map.DoOnVector (aBLVector: TBLVector);
begin

end;

procedure Tframe_Map.DoOnFixedPoint (aBLVector: TBLVector);
begin

end;



procedure Tframe_Map.DoOnEscape(Sender: TObject);
begin
  Ffrm_MapView.SetDefaultTool;
end;

//--------------------------------------------------------------------
procedure Tframe_Map.FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
//--------------------------------------------------------------------
begin
  Ffrm_MapView.FormKeyDown (Sender, Key, Shift);
end;


procedure Tframe_Map.ed_DesktopButtonClick (Sender: TObject; AbsoluteIndex: Integer);
begin
  act_Desktop_Setup.Execute;
end;

procedure Tframe_Map.ed_DesktopClick(Sender: TObject);
begin
  act_Desktop_Setup.Execute;
end;


procedure Tframe_Map.SetBoundsWithOffset (aBLRect: TBLRect);
begin
  Ffrm_MapView.SetBoundsWithOffset (aBLRect);
end;


procedure Tframe_Map.AlignBoundsForBLVector(aBLVector: TBLVector);
begin
  Ffrm_MapView.AlignBoundsForBLVector (aBLVector);
end;



procedure Tframe_Map.Button2Click(Sender: TObject);
begin
 // Ffrm_MapView.LoadMapsFromclass (dmMap_Desktop.MapDesktopItem);
end;


procedure Tframe_Map.DoOnMapDblClick(Sender: tobject);
begin
  if Ffrm_MapView.SelectedItemList.Count>0 then
    Tdlg_Map_Selected.ExecDlg1 (Ffrm_MapView.SelectedItemList);

end;

// ---------------------------------------------------------------
procedure Tframe_Map.DoOnMapObjectClick(Sender: tobject);
// ---------------------------------------------------------------
var
  iID: Integer;
  sObjName: string;
//  sGUID: string;

  oItem: TMapSelectedItem;
begin
  if Ffrm_MapView.SelectedItemList.Count=0 then
    exit;


  oItem:=Ffrm_MapView.SelectedItemList[0];

  iID     := oItem.ID;
  sObjName:= oItem.ObjName;
 // sGUID   := oItem.guid;


  //to  TdmAct_Base
//  dmObject_Manager.OBJECT_FOCUS_IN_PROJECT_TREE (iID,sObjName);//,sGUID);
//  g_Object_Manager.OBJECT_FOCUS_IN_PROJECT_TREE (iID,sObjName);//,sGUID);
  g_Shell.OBJECT_FOCUS_IN_PROJECT_TREE  (sObjName, iID);


end;



procedure Tframe_Map.SetCenter (aBLPoint: TBLPoint);
begin
  Ffrm_MapView.SetCenter (aBLPoint);
end;

//-------------------------------------------------------------------
function Tframe_Map.GetProjectMapDir: string;
//-------------------------------------------------------------------
begin
  Result:= dmMain.Dirs.ProjectMapDir;
end;



procedure Tframe_Map.MapDelByMask(aMask: string);
begin
  Ffrm_MapView.MapDelByMask (aMask);

end;



// ---------------------------------------------------------------
procedure Tframe_Map.TBItemClick_Z(Sender: TObject);
// ---------------------------------------------------------------
var
  eScale: Double;
  k: Integer;
  oBItem: TTBItem;
begin
  oBItem:=Sender as TTBItem;

//  oBItem.Checked:=True;

  k:=oBItem.Tag;

  Ffrm_MapView.FTileManager.Set_Z(oBItem.Tag);
  Ffrm_MapView.WMS_Update;
//  FTileManager.Load_CMap();
  Ffrm_MapView.FTileManager.Set_Z(0);



  DoOnMapViewChanged(nil);

end;

// ---------------------------------------------------------------
procedure Tframe_Map.DoOnMapViewChanged(Sender: tobject);
// ---------------------------------------------------------------
begin

  if Ffrm_MapView.WMS.Z>0 then
    TBSubmenuItem_Z.Caption:= Format('z %d', [Ffrm_MapView.WMS.Z])
  else
    TBSubmenuItem_Z.Caption:= 'z';


  if Ffrm_MapView.WMS.Z>0 then
    dxBarSubItem_WMS_Z.Caption:= Format('z %d', [Ffrm_MapView.WMS.Z])
  else
    dxBarSubItem_WMS_Z.Caption:= 'z';

end;


procedure Tframe_Map.dxBarButton351_z1Click(Sender: TObject);
begin
  Ffrm_MapView.FTileManager.Set_Z(TdxBarButton(Sender).Tag);
  Ffrm_MapView.WMS_Update;
//  FTileManager.Load_CMap();
  Ffrm_MapView.FTileManager.Set_Z(0);


  DoOnMapViewChanged(nil);

end;


procedure Tframe_Map.GetMessage(aMsg: TEventType; aParams: TEventParamList ;
    var aHandled: boolean);
begin
  case aMsg of
    et_MAP_REFRESH_FROM_DESKTOP:  Ffrm_MapView.LoadMapsFromclass (dmMap_Desktop.MapDesktopItem);
    et_MAP_REFRESH_WMS:           refresh_WMS;

  end;

end;


// ---------------------------------------------------------------
procedure Tframe_Map.refresh_WMS;
// ---------------------------------------------------------------
begin
   dmSettings.Open_WMS;

   cxBarEditItem_WMS.EditValue:=dmSettings.WMS_Get_Default_Map_Name;
end;


// ---------------------------------------------------------------
procedure Tframe_Map.cxLookupComboBox_WMSClick(Sender: TObject);
// ---------------------------------------------------------------
begin
  WMS_change;

  pn_Main.SetFocus;
end;


// ---------------------------------------------------------------
procedure Tframe_Map.WMS_change;
// ---------------------------------------------------------------
var
  i: Integer;
  iMaxZ: Integer;
  s: string;
begin
  if cb_WMS.Checked then
//  if ComboBox_WMS.ItemIndex>=0 then
  begin
    s:=cxLookupComboBox_WMS.EditValue;
    if s='' then
      exit;

    Assert (s<>'');

    if dmSettings.sp_WMS_SEL.Locate ('name',s,[]) then
    begin
      Ffrm_MapView.WMS_Change_Layer(s,
          dmSettings.sp_WMS_SEL['url'],
          dmSettings.sp_WMS_SEL['epsg'],
          dmSettings.sp_WMS_SEL['max_z']);

      iMaxZ:=dmSettings.sp_WMS_SEL['max_z'];

      for i:=0 to TBSubmenuItem_Z.Count-1 do
        TBSubmenuItem_Z.Items[i].Enabled:=(TBSubmenuItem_Z.Items[i].Tag <= iMaxZ);


      for i:=0 to dxBarSubItem_WMS_Z.ItemLinks.Count-1 do
;
//        dxBarSubItem_WMS_Z.ItemLinks[i].Enabled:=(dxBarSubItem_WMS_Z.ItemLinks[i].Tag <= iMaxZ);


//    for I := 1 to 20 do    // Iterate
//    begin
//      oBItem:=TTBItem.create(TBSubmenuItem_Z);
//      oBItem.Caption:= Format('z %d', [i]);
//      oBItem.Tag:=i;
//      oBItem.GroupIndex:=1;
//
////      oBItem.RadioItem:=False;
//
//      oBItem.OnClick:=TBItemClick_Z;
//
//      TBSubmenuItem_Z.Add(oBItem);
//
    end
      else
         raise exception.Create ('dmSettings.q_WMS.Locate - '+ s);

//    s:= DEF_WMS[ComboBox_WMS.ItemIndex].Name;

//    Ffrm_MapView.WMS_Change_Layer(s);

 //   if cb_WMS.Checked then
    Ffrm_MapView.WMS_Update;

  end else
    Ffrm_MapView.FMapEngine.TilesClear;

//  pn_Main.SetFocus;;


end;

// ---------------------------------------------------------------
procedure Tframe_Map.cb_WMSClick(Sender: TObject);
// ---------------------------------------------------------------
begin
  Ffrm_MapView.WMS.Active:=cb_WMS.Checked;

  WMS_change;

end;


end.


(*


{


// ---------------------------------------------------------------
procedure Tframe_Map.ComboBox_WMSClick(Sender: TObject);
// ---------------------------------------------------------------
var
  s: string;
begin
{  exit;

  if ComboBox_WMS.ItemIndex>=0 then
  begin
    Ffrm_MapView.WMS_Change_Layer_Index(ComboBox_WMS.ItemIndex);

//    s:= DEF_WMS[ComboBox_WMS.ItemIndex].Name;

//    Ffrm_MapView.WMS_Change_Layer(s);

    if cb_WMS.Checked then
      Ffrm_MapView.WMS_Update;

  end;

  pn_Main.SetFocus;;
  }

end;

{
procedure Tframe_Map.dxBarCombo_WMSChange(Sender: TObject);
begin
  cb_WMS_Change(nil);
end;
}

{
// ---------------------------------------------------------------
procedure Tframe_Map.dxBarCombo_WMSClick(Sender: TObject);
// ---------------------------------------------------------------
var
  s: string;
begin
  if ComboBox_WMS.ItemIndex>=0 then
  begin
//    s:= DEF_WMS[ComboBox_WMS.ItemIndex].Name;

    Ffrm_MapView.WMS_Change_Layer_Index(ComboBox_WMS.ItemIndex);
//    Ffrm_MapView.WMS_Change_Layer(s);

    if cb_WMS.Checked then
      Ffrm_MapView.WMS_Update;

  end;

  pn_Main.SetFocus;;

end;
  }




  procedure Tframe_Map.Button1Click(Sender: TObject);
{
var
  vLayer: CMapXLayer;

  sObjName: string;
 }
begin
{
  sObjName:=OBJ_PMP_CALC_REGION;


  if Assigned (Ffrm_MapView.Map1) then
  begin
    vLayer :=mapx_GetLayerByName(Ffrm_MapView.Map1, '~'+sObjName);
    if Assigned(vLayer) then
    try
      ShowMessage('dmMapEngine_store.Map.Layers.Remove(vLayer);');
      Ffrm_MapView.Map1.Layers.Remove(vLayer);
    except
    end;

  end;




  dmMapEngine_store.Delete_Object_Map(sObjName);
  dmMapEngine_store.Open_Object_Map  (sObjName);

  dmMapEngine_store.RefreshObjectThemas_All;
 }
end;

procedure Tframe_Map.cb_WMS_Click(Sender: TObject);
begin

end;



