unit f_Map;

interface
{$I ver.Inc}

uses
  Classes, Controls, Forms, ActnList, dxBar,  TB2Item, TB2Toolbar, rxPlacemnt,

  u_LinkFreqPlan_from_map_run,

//  d_LinkFreqPlan_Add_from_map,
  i_Map_engine,

  I_Act_Profile,      //  dm_Act_Profile,


//  d_Map_Search_Property,
//  d_Map_Search_Yandex,


  d_GetRegionType,

//  I_Act_LOS, // dm_Act_LOS,

  fr_Map,

  u_classes,

  fr_MapView_base,

  //los

  dm_Main,
                                        
  I_MapAct,
  I_MapView,
                                         

  d_Map_Links,                                                                                                     

  u_const_str,

  u_func,
  u_Geo,
  u_log,

  u_func_msg,
  

  dm_Rel_Engine,

  u_types,

  f_tool_Map_Matrix_monitor,

  cxCheckBox, cxLookAndFeels, cxBarEditItem,
  cxClasses, Menus, ImgList, StdCtrls, ExtCtrls, TB2Dock, dxSkinsCore,
  dxSkinsDefaultPainters, cxGraphics, cxControls, cxLookAndFeelPainters,
  cxContainer, cxEdit, cxTextEdit, cxMaskEdit, cxDropDownEdit,
  cxLookupEdit, cxDBLookupEdit, cxDBLookupComboBox;

type
  TNextAction =
    (naNone,

     naLOS_from_prop,
     naLOS,

     naLOSRegion,
     naLOSByCircle,
     naLOSByPoly,

     naAddProperty,
//     naAddSite,

     naAddLinkLine,
     naAddLinkEnd,
     naAddLink,

     naAddLinkFreqPlan,
     naAddLinkFreqPlanByCircle,
     naAddLinkFreqPlanByPoly,


     naAddRepeater,

     naAddPMP,
     naAddPmpTerminal,


     naAddBSC,
     naAddMSC,

     naAddPMPRegion,
     naAddPMPRegionByCircle,
     naAddPMPRegionByPoly,

   //  naAddRepeater,
   //  naAddPolyline,
  //   naAddXConn,
//     naPrintPicture,

     naMatrixSelectRegion,
     naShowPointInfo,

//     naViewMatrixMonitor,
     naShowRuler,

     naSetProfilePoints,
     naSetProfileByExactCoord,
     naSetProfileFixedPoint,
     naSetProfileBetween_BS_And_Point,
     naSetProfileBetweenBS

     ,naSelectGroup
     // - �� 2 ������
     // - � ������������� ������
     // - ����� ��
     );

    // naSetProfilePoint1, // �������� 1 ����� ��� �������
     //naSetProfilePoint2);
    // naZoomIn  );


  TMapSelectionRec = record
                        SelectionType: (stRect,stCircle,stPoint,stPoints);

                        Center: TBLPoint;
                        Radius: Double;

                        BLRect: TBLRect;
                        BLPoint: TBLPoint;

                        BLPoints: TBLPointArrayF;
                     end;


                             //, IMapX
  Tfrm_Map = class(Tframe_Map, IMessageHandler)
    act_Cancel: TAction;
    act_Close: TAction;
    act_Delete: TAction;
    act_Layer_scheme11: TAction;
    act_LOS: TAction;
    act_Map_Options: TAction;
    act_MapMatrix_Select_Region: TAction;
    act_Move: TAction;
    act_New_BSC: TAction;
    act_New_Link: TAction;
    act_New_LinkEnd: TAction;
    act_New_LinkLine: TAction;
    act_New_MSC: TAction;
    act_New_PMP: TAction;
    act_New_PmpTerminal: TAction;
    act_New_Property: TAction;
    act_New_region_by_circle: TAction;
    act_New_Region_PMP: TAction;
    act_New_Repeater: TAction;
    act_Options: TAction;
    act_Point_info111111: TAction;
    act_Print: TAction;
    act_Profile_1_point: TAction;
    act_Profile_2_Points: TAction;
    act_Profile_Between_BS: TAction;
    act_Profile_Between_BS_And_Point: TAction;
    act_Profile_exact_coord: TAction;
    act_Recreate_Maps1111: TAction;
    act_Show_Best_Server: TAction;
    act_Show_Point_Info: TAction;
    act_Workspace_setup111: TAction;
    Action1: TAction;
    Action2: TAction;
    Action3: TAction;
    ActionList2: TActionList;
    ImageList1111111: TImageList;
    TBToolbar5: TTBToolbar;
    TBItem26: TTBItem;
    TBSeparatorItem16: TTBSeparatorItem;
    TBSubmenuItem2: TTBSubmenuItem;
    TBItem99: TTBItem;
    mnu_PMP: TTBItem;
    TBItem14: TTBItem;
    TBItem33: TTBItem;
    TBItem31: TTBItem;
    mnu_Link: TTBItem;
    TBItem15: TTBItem;
    TBItem23: TTBItem;
    mnu_MSC: TTBItem;
    TBItem16: TTBItem;
    TBSeparatorItem10: TTBSeparatorItem;
    TBSubmenuItem6: TTBSubmenuItem;
    TBItem30: TTBItem;
    TBItem32: TTBItem;
    TBItem22: TTBItem;
    TBItem28: TTBItem;
    TBItem27: TTBItem;
    TBItem39: TTBItem;
    TBItem38: TTBItem;
    TBItem37: TTBItem;
    TBItem35: TTBItem;
    TBItem42: TTBItem;
    TBItem43: TTBItem;
    TBItem40: TTBItem;
    TBItem41: TTBItem;
    TBSubmenuItem1: TTBSubmenuItem;
    act_LOS_m: TAction;
    TBItem12: TTBItem;
    dxBarSubItem31: TdxBarSubItem;
    dxBarSubItem41: TdxBarSubItem;
    dxBarButton160: TdxBarButton;
    dxBarButton161: TdxBarButton;
    dxBarSubItem311: TdxBarSubItem;
    dxBarButton1911: TdxBarButton;
    dxBarButton19423: TdxBarButton;
    dxBarButton2064: TdxBarButton;
    dxBarButton20345: TdxBarButton;
    dxBarButton2033: TdxBarButton;
    dxBarButton20765: TdxBarButton;
    dxBarButton207567: TdxBarButton;
    dxBarButton20234: TdxBarButton;
    dxBarButton2055: TdxBarButton;
    TBItem53: TTBItem;
    FormPlacement1: TFormPlacement;
    TBItem54: TTBItem;
    TBSubmenuItem5: TTBSubmenuItem;
    TBItem56: TTBItem;
    act_Search_yandex: TAction;
    act_Search_prop: TAction;
    Action4: TAction;
    TBItem9: TTBItem;
    dxBarSubItem4: TdxBarSubItem;
    dxBarButton11: TdxBarButton;
    TBSeparatorItem6: TTBSeparatorItem;
    dxBarManager1Bar1: TdxBar;
    TBItem17: TTBItem;
    dxBarButton12: TdxBarButton;
    dxBarButton13: TdxBarButton;
    dxBarButton14: TdxBarButton;
    dxBarButton15: TdxBarButton;
    dxBarButton20: TdxBarButton;
    act_LOS_from_property: TAction;
    TBItem19: TTBItem;
    dxBarButton21: TdxBarButton;
    act_LinkFreqPlan_add: TAction;
    TBItem24: TTBItem;
    dxBarButton22: TdxBarButton;
    dxBarButton23: TdxBarButton;
    dxBarButton24: TdxBarButton;
    dxBarButton30: TdxBarButton;
    dxBarButton35: TdxBarButton;
    dxBarButton36: TdxBarButton;
    dxBarButton37: TdxBarButton;
    dxBarButton38: TdxBarButton;
    procedure FormDestroy(Sender: TObject);
//    procedure act_Search_yandexExecute(Sender: TObject);
//    procedure ApplicationEvents1Message(var Msg: tagMSG; var Handled: Boolean);
//    procedure cb_WMSClick(Sender: TObject);
//    procedure Button2Click(Sender: TObject);
//    procedure FormActivate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
 //   procedure Button1Click(Sender: TObject);
//    procedure Button2Click(Sender: TObject);
//    TBItem23: TTBItem;
    procedure FormCreate(Sender: TObject);
    procedure _Execute(Sender: TObject);
//    procedure FormDestroy(Sender: TObject);
//    procedure _Actions(Sender: TObject);

  private
//    FIMapView: IMapViewX;

    FNextAction: TNextAction;
    FIsBuildProfileByFixedPoint: boolean;

    procedure DoOnChangeFirstPointPos(Sender: TObject);
    procedure DoOnEscape(Sender: TObject); override;
    procedure DoOnVector(aBLVector: TBLVector); override;
    procedure ShowMatrixMonitor();
// TODO: GetIMapView
/////    procedure ShowRulerTool(aBLPoint: TBLPoint);
//
////    function MapDelByFileName (aFileName: string): boolean;
////    function  MapIndex (aFileName: string): integer;
//
// // protected
//
//
//  function GetIMapView: IMapViewX;

    procedure ChangeProject;

//    procedure GetMessage (aMsg: integer; aParams: TEventParamList; var aHandled: Boolean); //override;

    procedure SetNextAction (ANextAction: TNextAction);

    procedure DoOnPoint  (aBLPoint: TBLPoint);   override;
//    procedure DoOnCursor (aBLPoint: TBLPoint);
    procedure DoOnRect   (aBLRect: TBLRect);
    procedure DoOnPoly   (var aBLPoints: TBLPointArrayF);
    procedure DoOnRing   (aCenter: TBLPoint; aRadius: double);
//////    procedure DoOnDraftVector (aBLVector: TBLVector);
    procedure DoOnFixedPoint  (aBLVector: TBLVector); override;

    procedure DoOnLog (aMsg: string);
    procedure DoOnSelected(aRec: TMapSelectionRec);
//    procedure SearchFind(aLat: Double; aLon: Double);

    procedure Search_property;
    procedure Search_yandex;

    procedure GetMessage(aMsg: TEventType; aParams: TEventParamList; var aHandled:   boolean);

//    procedure PostMsgBLVector (aMsg: integer; aBLPoint1,aBLPoint2: TBLPoint);

//    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);


//    procedure WM_USER_MSG1(var Message: Messages.TMessage); //message WM_USER;
  public



    class function ShowForm: Tfrm_Map;
    class function CreateNewForm: Tfrm_Map;

// TODO: WM_USER_MSG
//  procedure WM_USER_MSG(var Message: Messages.TMessage); message WM_USER;

 //   class function CreateSingleForm: Tfrm_Map;

//    procedure Dlg_CalcLOS(aBLPoint: TBLPoint);

 //   constructor Create(AOwner: TComponent); override;
  end;



//==================================================================
implementation {$R *.dfm}
//==================================================================
uses
  dm_Act_LOS,

  dm_Act_Profile,      //  dm_Act_Profile,

  dm_Act_Property,
  dm_Act_Repeater,
  dm_Act_Link,         // dm_Act_Link,
  dm_Act_LinkEnd,      // dm_Act_LinkEnd,
  dm_act_MSC,          //  dm_act_MSC,
  dm_Act_Pmp_Site,     //  dm_Act_Pmp_Site,
  dm_act_BSC,          // dm_act_BSC;
  dm_Act_PmpTerminal, //dm_Act_PmpTerminal,
  dm_Act_PMP_CalcRegion;  //dm_Act_PMP_CalcRegion,


//-------------------------------------------------------
class function Tfrm_Map.CreateNewForm: Tfrm_Map;
//-------------------------------------------------------
begin
  Result:=Tfrm_Map.Create(Application);

  if Assigned(Application.MainForm) then
    if Application.MainForm.FormStyle=fsMDIForm then
      Result.FormStyle:=fsMDIChild;
                                                                                                                                 
  Result.Show;

end;



//-------------------------------------------------------
class function Tfrm_Map.ShowForm: Tfrm_Map;
//-------------------------------------------------------
var oForm: TForm;
begin
  oForm:= IsFormExists (Application,Tfrm_Map.ClassName);

  if not Assigned(oForm) then
    oForm:= Tfrm_Map.CreateNewForm();

  oForm.Show;

  if oForm.WindowState = wsMinimized then
    oForm.WindowState:= wsNormal;

  Result:= Tfrm_Map(oForm);
end;


//-------------------------------------------------------
procedure Tfrm_Map.FormCreate(Sender: TObject);
//-------------------------------------------------------
var
  b: boolean;

begin
  inherited;


  FormPlacement1.IniFileName:=FRegPath;
  FormPlacement1.Active:=true;
  FormPlacement1.RestoreFormPlacement;


//  mnu_GSM.Caption:=STR_GSM_GROUP_CAPTION;                                         
  mnu_Link.Caption :=STR_PP_GROUP_CAPTION ;
  mnu_PMP.Caption:=STR_PMP_GROUP_CAPTION;
  mnu_MSC.Caption:=STR_MSC_GROUP_CAPTION;



  Caption:=DLG_CAPTION_MAP;

 // OnEscape:=DoOnEscape;
//  OnCursor:=DoOnCursor;
  OnRect:=DoOnRect;
  OnVector:=DoOnVector;

//  OnPoint:=DoOnPoint;
  OnPoly:=DoOnPoly;
  OnRing:=DoOnRing;
  OnDraftVector:=DoOnFixedPoint;/////////DoOnDraftVector;
  OnFixedPoint:=DoOnFixedPoint;

  Ffrm_MapView.OnChangeFirstPointPos:=DoOnChangeFirstPointPos;


  act_New_PMP.Caption            :='�� PMP';

 // act_New_XConn.Caption         :='X connector';
  act_New_LinkEnd.Caption        :='���';
  act_New_Link.Caption           :='�� ��������';
  act_New_LinkLine.Caption       :='������� �� �����';

  act_Profile_exact_coord.Caption:='�� ������ �����������';
  act_Profile_2_Points.Caption   :='�� 2 ������';
  act_Profile_1_Point.Caption    :='� ���� ������';

  act_PrintToFile.Caption        :='������ ����� � ����';

///////  act_Ruler_Tool.Caption :='������� (���)';

  act_New_Property.Enabled:=    FoundInProjectTypes(otProperty);
  act_New_Link.Enabled:=        FoundInProjectTypes(otLink);
//  act_New_XConn.Enabled:=       FoundInProjectTypes(otXConn);
  act_New_LinkEnd.Enabled:=     FoundInProjectTypes(otLinkEnd);
  act_New_LinkLine.Enabled:=    FoundInProjectTypes(otLinkLine);
  act_New_PMP.Enabled:=         FoundInProjectTypes(otPmpSite);
  act_New_PmpTerminal.Enabled:= FoundInProjectTypes(otPmpTerminal);

//  act_New_Site.Enabled:=        FoundInProjectTypes(otSite);
 // act_New_Region.Enabled:=      FoundInProjectTypes(otPmpCalcRegion);
////////////////  act_New_PMP_Region.Enabled:=  false; //FoundInProjectTypes(otCalcRegion);
  act_New_BSC.Enabled:=         FoundInProjectTypes(otBsc);
  act_New_MSC.Enabled:=         FoundInProjectTypes(otMsc);


 // act_New_Region.Enabled:=True;


  act_New_Property.Visible:=True;

  mnu_MSC.Visible:=True;
  act_New_BSC.Visible:=True;
  act_New_MSC.Visible:=True;



  mnu_PMP.Visible:=True;
  act_New_PMP.Visible:=True;
  act_New_Region_PMP.Visible:=True;
  act_New_PmpTerminal.Visible:=True;



 // b:=True;
  mnu_Link.Visible:=True;
  act_New_Link.Visible:=True;
  act_New_LinkEnd.Visible:=True;
  act_New_LinkLine.Visible:=True;

 // Assert(Assigned(ILinkEnd));


//  Ffrm_MapView.GetInterface(IMapViewX, IMainMapView);



 // dmObject_Manager.MainMapForm:=Ffrm_MapView;
//z  vIMapViewX;

// dmObject_Manager.IMainMap1:=vIMapViewX;



//  Ffrm_MapView.Dlg_Layers;

 // IMapViewX(dmObject_Manager.IMainMap).Dlg_Layers;

//  gl_Map:= Self;


//  act_Zoom_out_8.Caption:='�������� x 8';

//  SetActionsExecuteProc ([act_New_XConn], _Execute);



//    Screen.ActiveForm.Handle


 /////////////
////////////
  Ffrm_MapView.GetInterface(IMapViewX, IActiveMapView);

////  _PostMessage(WE_PROJECT_CHANGED, 0,0);

  ChangeProject;

//  PostMessage(Handle, WE_PROJECT_CHANGED_, 0,0);
//  Application.ProcessMessages;


 // Perform(WE_PROJECT_CHANGED, 0,0);
///  ChangeProject;

//  IActiveMapView:=FIMapView;


end;



//-------------------------------------------------------
procedure Tfrm_Map.FormDestroy(Sender: TObject);
//-------------------------------------------------------
begin

//  FIMapView := nil;
  IActiveMapView := nil;

  inherited;
end;



//-------------------------------------------------------
procedure Tfrm_Map.DoOnRing (aCenter: TBLPoint; aRadius: double);
//-------------------------------------------------------
var rec: TMapSelectionRec;
begin
  rec.SelectionType:=stCircle;
  rec.Radius:=aRadius;
  rec.Center:=aCenter;

  DoOnSelected(rec);
end;


//-------------------------------------------------------
procedure Tfrm_Map.DoOnPoly(var aBLPoints: TBLPointArrayF);
//-------------------------------------------------------
var  rec: TMapSelectionRec;
begin
  rec.SelectionType:=stPoints;
  rec.BLPoints:=aBLPoints;

  DoOnSelected(rec);
end;


//-------------------------------------------------------------------
procedure Tfrm_Map.DoOnSelected(aRec: TMapSelectionRec);
//-------------------------------------------------------------------
begin
  //---------------------------------------------------------
  if aRec.SelectionType = stCircle then
  //---------------------------------------------------------
     case FNextAction of
        naLOSByCircle:  begin
                          SetNextAction(naNone);
                          dmAct_LOS.AddByRing(aRec.Center, aRec.Radius);
                        end;


        naAddPMPRegionByCircle:  dmAct_Pmp_CalcRegion.AddByRing(aRec.Center, aRec.Radius);
        naAddLinkFreqPlanByCircle: TLinkFreqPlan_from_Map_run.AddByRing(aRec.Center, aRec.Radius);
      end;

  //---------------------------------------------------------
  if aRec.SelectionType = stRect then
  //---------------------------------------------------------
    case FNextAction of

      naLosRegion:  begin
                      SetNextAction(naNone);
                    //  Application.ProcessMessages;

                      dmAct_LOS.AddByRect(aRec.BLRect);
                    end;


      naAddPMPRegion:  dmAct_Pmp_CalcRegion.AddByRect(aRec.BLRect);
      naAddLinkFreqPlan: TLinkFreqPlan_from_Map_run.AddByRect(aRec.BLRect);

    end;

  //---------------------------------------------------------
  if aRec.SelectionType = stPoints then
  //---------------------------------------------------------
    case FNextAction of

      naLosByPoly:  begin
                      SetNextAction(naNone);
                      dmAct_LOS.AddByPoly(aRec.BLPoints);
                    end;

      naAddPMPRegionByPoly:     dmAct_Pmp_CalcRegion.AddByPoly(aRec.BLPoints);
      naAddLinkFreqPlanByPoly:  TLinkFreqPlan_from_Map_run.AddByPoly(aRec.BLPoints);
                              
   end;


  SetNextAction(naNone);

end;


//-------------------------------------------------------
procedure Tfrm_Map.DoOnRect (aBLRect: TBLRect);
//-------------------------------------------------------
var rec: TMapSelectionRec;
begin
  rec.SelectionType:=stRect;
  rec.BLRect:=aBLRect;

  DoOnSelected(rec);
end;


//-------------------------------------------------------
procedure Tfrm_Map.DoOnVector(aBLVector: TBLVector);
//-------------------------------------------------------
var rec: TMapSelectionRec;
begin    
  case FNextAction of
    naAddLink: begin
                 dmAct_Link.AddByVector(aBLVector);
                 SetNextAction(naNone);
               end;

    naSetProfilePoints:       dmAct_Profile.BuildByVector(DEF_PROFILE_POINTS, aBLVector);
    naSetProfileBetweenBS:    dmAct_Profile.BuildByVector(DEF_PROFILE_BETWEEN_BS, aBLVector);
    naSetProfileByExactCoord: dmAct_Profile.BuildByVector(DEF_PROFILE_BY_COORD, aBLVector);
    naSetProfileFixedPoint:   dmAct_Profile.BuildByVector(DEF_PROFILE_BY_FIXED_POINT, aBLVector);
    naSetProfileBetween_BS_and_Point:  dmAct_Profile.BuildByVector(DEF_PROFILE_BETWEEN_BS_AND_POINT, aBLVector);

  end;

end;

//-------------------------------------------------------
procedure Tfrm_Map.DoOnFixedPoint (aBLVector: TBLVector);
//-------------------------------------------------------
begin
  case FNextAction of
    naSetProfileFixedPoint:
      dmAct_Profile.BuildByVector(DEF_PROFILE_BY_FIXED_POINT, aBLVector);
  end;
end;


//-------------------------------------------------------
procedure Tfrm_Map.DoOnPoint (aBLPoint: TBLPoint);
//-------------------------------------------------------

begin
  if FNextAction=naNone then
    Exit;
    


  case FNextAction of

    naLOS: begin
             SetNextAction(naNone);
             dmAct_LOS.AddByPoint( aBLPoint);    
           end;

    naLOS_from_prop: dmAct_Property.LOS_FromMap(aBLPoint);

    naAddProperty:   dmAct_Property.AddFromMap(aBLPoint);
    naAddRepeater:   dmAct_Link_Repeater.AddByPos(aBLPoint);
    naAddPMP:        dmAct_Pmp_Site.AddByPos (aBLPoint);//PostMsgDouble (WM_PMP_ADD_BY_POS, aBLPoint.B, aBLPoint.L);
    naAddLinkEnd:    dmAct_LinkEnd.AddByPos (aBLPoint);// PostMsgDouble (WM_LINKEND_ADD_BY_POS,  aBLPoint.B, aBLPoint.L);
    naAddBSC:        dmAct_BSC.AddByPos ( aBLPoint); //PostMsgDouble (WM_BSC_ADD_BY_POS,      aBLPoint.B, aBLPoint.L);
    naAddMSC:        dmAct_MSC.AddByPos ( aBLPoint); //PostMsgDouble (WM_MSC_ADD_BY_POS,      aBLPoint.B, aBLPoint.L);

    naAddPMPTerminal: dmAct_PmpTerminal.AddByPos (aBLPoint);

  end;


  case FNextAction of

    naLOS,
    naLOS_from_prop,

    naAddPMP,
    naAddPMPTerminal,
    naAddProperty,
    naAddRepeater,
    naAddLinkEnd,

    naAddBSC,
    naAddMSC: SetNextAction(naNone);
  end;

end;


//-----------------------------------------------------
procedure Tfrm_Map.DoOnEscape(Sender: TObject);
//-----------------------------------------------------
begin
  SetNextAction (naNone);
end;


//-----------------------------------------------------
procedure Tfrm_Map.SetNextAction (ANextAction: TNextAction);
//-----------------------------------------------------
var oBLVector: TBLVector;
begin
 // dmRel_Engine.Close;

  FNextAction:=ANextAction;

  case FNextAction of
     naNone         : begin
                        SetDefaultTool();
                       // Ffrm_MapView.ClearSelection();
                        // btn_Arrow.Down:=true;
                        // PostMessageAll (WM_PROFILE_STOP, 0, 0);
                      end;

     naSetProfileFixedPoint: begin
                               dmRel_Engine.Open;
                               WindowTool:=toolVector;
                             end;

     naSetProfilePoints :    WindowTool:=toolVector;
     naSetProfileBetweenBS:  WindowTool:=toolVector;
     naSetProfileBetween_BS_and_Point: WindowTool:=toolVector;
     naSetProfileByExactCoord :  WindowTool:=toolVector;

 ////    naShowRuler:         WindowTool:=toolPoint;
     naAddLink          : WindowTool:=toolVector;

     naLOS,
     naLOS_from_prop,
     naAddPMP,
//     naAddXConn,
    

     naAddProperty,
     naAddRepeater,
     naAddMSC,
     naAddBSC,
     naAddPMPTerminal,
     naAddLinkEnd       : WindowTool:=toolPoint;

     naAddLinkLine      : WindowTool:=toolObject;
                         
     naAddPMPRegion         : WindowTool:=toolRect;
     naAddPMPRegionByCircle : WindowTool:=toolRing;
     naAddPMPRegionByPoly   : WindowTool:=toolPoly;

     naAddLinkFreqPlan         : WindowTool:=toolRect;
     naAddLinkFreqPlanByCircle : WindowTool:=toolRing;
     naAddLinkFreqPlanByPoly   : WindowTool:=toolPoly;


     naLOSRegion    : WindowTool:=toolRect;
     naLOSByCircle  : WindowTool:=toolRing;
     naLOSByPoly    : WindowTool:=toolPoly;

  end;

//  if FNextAction in [naSetProfilePoints,naSetProfilePoint1]
  //  then FillChar(FMapInfo.Vector, SizeOf(FMapInfo.Vector), 0);


end;


//--------------------------------------------------------------------
procedure Tfrm_Map._Execute(Sender: TObject);
//--------------------------------------------------------------------
var
  sGeoRegionName: string;
begin
  inherited;

  if Sender=act_Cancel then
    SetNextAction (naNone) else

  if Sender=act_LOS_from_property then
  begin
  //  dmAct_LOS.MapDesktopID:=FMapDesktopID;
    SetNextAction (naLOS_from_prop);
  end else


  if Sender=act_LOS then
  begin
  //  dmAct_LOS.MapDesktopID:=FMapDesktopID;
    SetNextAction (naLOS);
  end else

  if Sender=act_Los_m then
  begin

  //  dmAct_LOS.MapDesktopID:=FMapDesktopID;

  //  SetNextAction (naLos_m);
    case Tdlg_GetRegionType.ExecDlg(sGeoRegionName) of
      0: SetNextAction (naLOSRegion);
      1: SetNextAction (naLOSByPoly);
      2: SetNextAction (naLOSByCircle);
      3: begin
           SetNextAction (naNone);
           dmAct_LOS.AddByGeoRegionID(sGeoRegionName);
         end;
    end;
  end else

  if Sender=act_New_Region_PMP then
  begin
 //   SetNextAction (naAddPMPRegion);
    case Tdlg_GetRegionType.ExecDlg(sGeoRegionName) of
      0: SetNextAction (naAddPMPRegion);
      1: SetNextAction (naAddPMPRegionByPoly);
      2: SetNextAction (naAddPMPRegionByCircle);

      3: begin
          SetNextAction (naNone);
          dmAct_Pmp_CalcRegion.AddByGeoRegionID(sGeoRegionName);
         end;

    end;
  end else

  {
     naAddLinkFreqPlan,
     naAddLinkFreqPlanByCircle,
     naAddLinkFreqPlanByPoly,
  }

  if Sender=act_LinkFreqPlan_add  then
  begin
 //   SetNextAction (naAddPMPRegion);
    case Tdlg_GetRegionType.ExecDlg(sGeoRegionName) of
      0: SetNextAction (naAddLinkFreqPlan);
      1: SetNextAction (naAddLinkFreqPlanByPoly);
      2: SetNextAction (naAddLinkFreqPlanByCircle );

      3: begin
          SetNextAction (naNone);
        //  dmAct_Pmp_CalcRegion.AddByGeoRegionID(sGeoRegionName);
         end;

    end;
  end else


  if Sender=act_New_BSC          then SetNextAction (naAddBSC) else
  if Sender=act_New_Link         then SetNextAction (naAddLink) else
  if Sender=act_New_LinkEnd      then SetNextAction (naAddLinkEnd) else
  if Sender=act_New_MSC          then SetNextAction (naAddMSC) else
  if Sender=act_New_PMP          then SetNextAction (naAddPMP) else
  if Sender=act_New_PmpTerminal  then SetNextAction (naAddPMPTerminal) else
  if Sender=act_New_Property     then SetNextAction (naAddProperty) else
  if Sender=act_New_Repeater     then SetNextAction (naAddRepeater) else

 // if Sender=act_New_Site         then SetNextAction (naAddSite) else
//  if Sender=act_New_XConn    then SetNextAction (naAddXConn) else

  if Sender=act_Profile_2_Points    then SetNextAction (naSetProfilePoints) else
  if Sender=act_Profile_1_point     then SetNextAction (naSetProfileFixedPoint) else
  if Sender=act_Profile_exact_coord then SetNextAction (naSetProfileByExactCoord) else
  if Sender=act_Profile_Between_BS  then SetNextAction (naSetProfileBetweenBS) else
  if Sender=act_Profile_Between_BS_And_Point then SetNextAction (naSetProfileBetween_BS_And_Point) else


  if Sender=act_Search_yandex then
    Search_yandex() else

  if Sender=act_Search_prop   then
    Search_property() else


  if Sender=act_Map_Ground then
    ShowMatrixMonitor() else


///  if Sender=act_Ruler_Tool then SetNextAction (naShowRuler) else

  if Sender=act_New_LinkLine then
  begin
    SetNextAction (naAddLinkLine);

    Tdlg_Map_Links.ShowWindow;

//    raise Exception.Create('');

 //   PostWMsg (WM_LINKLINE_ADD_MAP_LINKS);
  end else

  ;

end;

//-------------------------------------------------------
procedure Tfrm_Map.Search_yandex;
//-------------------------------------------------------

begin
   Init_IMapEngineX.Dlg_Search_Yandex (Ffrm_MapView.Map1.DefaultInterface);

end;


//-------------------------------------------------------
procedure Tfrm_Map.Search_property;
//-------------------------------------------------------
{
var
  oEvents: TSearchEventsX;
  v: ISearchX;
}
begin

  Init_IMapEngineX.Dlg_Search_Property (Ffrm_MapView.Map1.DefaultInterface, dmMain.GetConnectionString, dmMain.ProjectID);



//  Tdlg_Map_Search_Property.ExecDlg (dmMain.ProjectID, SearchFind);


//  Tdlg_Prop_search.ExecDlg(nil);

{  oEvents:=TSearchEventsX.Create;
  oEvents.OnFind:=SearchFind;

  v:=CoSearchX.Create;
  v.Dlg (dmMain.ProjectID, dmMain.ADOConnection.ConnectionString, oEvents as ISearchEventsX);
}
 // Tdlg_Prop_search.ExecDlg(nil);


end;

//-------------------------------------------------------
procedure Tfrm_Map.ShowMatrixMonitor();
//-------------------------------------------------------
begin
  Tfrm_Map_Matrix_Monitor.ShowForm( Ffrm_MapView.GetBounds() );
end;


//-------------------------------------------------------
procedure Tfrm_Map.ChangeProject;
//-------------------------------------------------------
begin
  if dmMain.ProjectID>0 then begin
  //  WorkspaceFileDir:=dmMain.ProjectMapDir;
    SetMapViewName2 ();
//    SetMapViewName2 ('map_default');
  end else
    CloseData();
//    SetMapViewName2 ('');


end;


procedure Tfrm_Map.DoOnLog(aMsg: string);
begin
  g_Log.Add(ClassName + aMsg);

end;




procedure Tfrm_Map.DoOnChangeFirstPointPos(Sender: TObject);
begin
  SetNextAction(naSetProfileFixedPoint);
end;


procedure Tfrm_Map.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action :=caFree;
end;



procedure Tfrm_Map.GetMessage(aMsg: TEventType; aParams: TEventParamList; var aHandled: boolean);
var
  blRect: TblRect;
  I: Integer;
  iCount: Integer;
  k: Integer;
 // iID: Integer;
  oStrings: TStrings;

  oIDList: TIDList;
  s: string;

begin
  inherited;

  case aMsg of
    et_PROJECT_CLEAR:    Clear;
    et_PROJECT_CHANGED:  ChangeProject;
    et_PROJECT_CLOSED:   Close;


//    ShowMessage('Tfrm_Map: WM__PROJECT_CLEAR');


    et_MAP_REFRESH:   Ffrm_MapView.MapRefresh;


    et_MAP_UPDATE_STOP:   Ffrm_MapView.MapUpdateStop;
    et_MAP_UPDATE_START:  Ffrm_MapView.MapUpdateStart;



//    WE_MAP_REFRESH_OBJECTS:   RefreshObjectStyles;
    et_MAP_CLOSE:  Close;

    et_MAP_SAVE_AND_CLOSE_WORKSPACE:   Ffrm_MapView.MAP_SAVE_AND_CLOSE_WORKSPACE;

    // ---------------------------------------------------------------
    et_ADD_MAP:
    // ---------------------------------------------------------------
      begin
//        Assert(Msg.wParam > 0);

//        iCount:=Count();


        blRect:=Ffrm_MapView.GetPlanBounds();

//        aParams.Files
        Assert(Assigned(aParams.Files));


        oStrings:= aParams.Files; // TStrings ( Msg.wParam );


        k:=oStrings.Count;

        for I := 0 to oStrings.Count - 1 do
        begin
        //  iID:=Integer(oStrings.Objects[i]);
            s:=oStrings[i];

       //   if iID>0 then
            MapAdd(oStrings[i]);
        end;

        if blRect.TopLeft.b=0 then
          ZoomPlan();

      //  ShowMessage('procedure Tfrm_Map.ApplicationEvents1Message(var Msg: tagMSG; var Handled:');

      end;

    // ---------------------------------------------------------------
    et_DEL_MAP:
    // ---------------------------------------------------------------
      begin
//        Assert(Msg.wParam > 0);

      //   iCount:=Count();
        Assert(Assigned(aParams.IDList));

        oIDList:= aParams.IDList;

//        oIDList:=  TIDList ( Msg.wParam );

        for I := 0 to oIDList.Count - 1 do
        begin
          s:=oIDList[i].Name;
      //    iID:=oIDList[i].ID;
          Assert(s<>'');

          MapDelByFileName(oIDList[i].Name);
        end;

     //   if iCount=0 then
       //   ZoomPlan();

      //  ShowMessage('procedure Tfrm_Map.ApplicationEvents1Message(var Msg: tagMSG; var Handled:');

      end;


    // ---------------------------------------------------------------
    et_DEL_MAP_BY_MASK:
    // ---------------------------------------------------------------
      begin
     //   Assert(Msg.wParam > 0);

      //   iCount:=Count();

        Assert(Assigned(aParams.Files));

        oStrings:= aParams.Files; // TStringList ( Msg.wParam );

//        for I := 0 to oIDList.Count - 1 do
       // begin
         // s:=oIDList[i].Name;
      //    iID:=oIDList[i].ID;
       //   Assert(s<>'');

          MapDelByMask(oStrings.Text);
       // end;

     //   if iCount=0 then
       //   ZoomPlan();

      //  ShowMessage('procedure Tfrm_Map.ApplicationEvents1Message(var Msg: tagMSG; var Handled:');

      end;

  end;

end;




end.



