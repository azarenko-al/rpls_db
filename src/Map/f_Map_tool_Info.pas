unit f_Map_tool_Info;

interface
//{$I Mapx.inc}


uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, ComCtrls, rxPlacemnt, ToolWin, StdCtrls, Variants, MapXLib_TLB,

  f_Map_Tool_base,

  u_Storage,

  I_MapAct,
  I_MapView,
 

  u_const,
  u_const_db,

  u_mapX,
  u_mapX_lib,
  u_GEO,
  u_reg,
  u_db,
 //   
  u_func, Grids, ValEdit, cxControls, cxSplitter
  ;


type

  Tfrm_Map_tool_Info = class(Tfrm_Map_Tool_base)
    cxSplitter1: TcxSplitter;
    ValueListEditor1: TValueListEditor;
    ValueListEditor_layers: TValueListEditor;
    procedure FormDestroy(Sender: TObject);
    procedure FormCreate  (Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ValueListEditor_layersSelectCell(Sender: TObject; ACol, ARow:
        Integer; var CanSelect: Boolean);

  private
    FBLPoint: TBLPoint;

    FDisableEdit : Boolean;

//    procedure LoadLayerFields;
    procedure LoadLayerFields_new(aFileName: string);
    procedure LoadLayers;
 protected
    Map: TMap;

    procedure ShowPoint(aBLPoint: TBLPoint);

  public

    class procedure ShowWindow (aMap: TMap; aBLPoint: TBLPoint);
  end;


//====================================================
// implementation
//====================================================
implementation {$R *.DFM}


//---------------------------------------------------
class procedure Tfrm_Map_tool_Info.ShowWindow (aMap: TMap; aBLPoint: TBLPoint);
//---------------------------------------------------
var
  oForm: Tfrm_Map_tool_Info;
  vIMapView: IMapViewX;
begin
  if not Assigned(IActiveMapView) then
    Exit;


  oForm:= Tfrm_Map_tool_Info(IsFormExists (Application, Tfrm_Map_tool_Info.ClassName));


  if not Assigned(oForm) then
  begin
    vIMapView:=IActiveMapView;
    oForm:= Tfrm_Map_tool_Info.Create(Application);
    
  end;

  oForm.Map:=aMap;
  oForm.ShowPoint(aBLPoint);

  oForm.Show;        

  

 { with oForm do
  begin
    Screen.Cursor:= crHourGlass;

    try
      FMap:= aMap;
      FBLPoint.B:= aBLPoint.B;
      FBLPoint.L:= aBLPoint.L;

      LoadLayers();

      dx_LayersChangeNode(nil, nil, nil);

      Show;

    finally
      Screen.Cursor:= crDefault;
    end;
  end;
}

end;

//------------------------------------------------------------------------------
procedure Tfrm_Map_tool_Info.FormCreate(Sender: TObject);
//------------------------------------------------------------------------------
begin
  inherited;
  Height:= 270;

 // dx_Layers.Align:=      alTop;
  //dx_LayerFields.Align:= alClient;

//  cxDBVerticalGrid1.Align:= alClient;
(*
  db_CreateField(mem_Layers, [db_Field(FLD_NAME,    ftString, 100),
                              db_Field(FLD_FILENAME,ftString, 500) ]);

  mem_Layers.Open;
*)

  //
 // FRegPath:=REGISTRY_COMMON_FORMS+ClassName+'\';

  ValueListEditor_layers.Height:=reg_ReadInteger(FRegPath, ValueListEditor_layers.Name, ValueListEditor_layers.Height);

 //   if not mapx_CreateMemFromMapFields (sFileName, mem_LayerFields ) then


  ValueListEditor1.Align:=alClient;

end;


procedure Tfrm_Map_tool_Info.FormDestroy(Sender: TObject);
begin

  reg_WriteInteger(FRegPath, ValueListEditor_layers.Name, ValueListEditor_layers.Height);

  inherited;
end;


//------------------------------------------------------------------------------
procedure Tfrm_Map_tool_Info.LoadLayers;
//------------------------------------------------------------------------------
var
  sShortFileName: string;
  sFileName: string;
  iCount: integer;
  i: integer;
  s: string;
  vLayer: CMapXLayer;
  vPt : CMapXPoint;
  vFeatures: CMapXFeatures;

begin
  vPt := CoPoint.Create;
  vPt.Set_(FBLPoint.L, FBLPoint.B);


  ValueListEditor_layers.Strings.Clear;
  ValueListEditor1.Strings.Clear;


  for i:=1 to Map.Layers.Count do
  begin
      vLayer:= Map.Layers.Item[i];

    if (not vLayer.Visible) or
      (vLayer.Type_ <> miLayerTypeNormal)
    then
      Continue;

    vLayer.BeginAccess(miAccessRead);

      vFeatures:= vLayer.SearchAtPoint(vPt, EmptyParam);

    iCount:= vFeatures.Count;

    if iCount>0 then
    begin
      sFileName:=      vLayer.Filespec;
      sShortFileName:= ExtractFileName(sFileName);

      ValueListEditor_layers.InsertRow(vLayer.Name, sFileName, True);

      end;

    vLayer.EndAccess(miAccessEnd);
  end;


  if ValueListEditor_layers.Strings.Count>0 then
  begin
    s := ValueListEditor_layers.Strings.ValueFromIndex[0];
    LoadLayerFields_new(s);
  end;



end;



//------------------------------------------------------------------------------
procedure Tfrm_Map_tool_Info.LoadLayerFields_new(aFileName: string);
//------------------------------------------------------------------------------
var
  iDataset: Integer;
  sFieldName: string;
  j ,k: Integer;
  s: string;

  vLayer: CMapXLayer;
  vPt : CMapXPoint;
  vFeatures: CMapXFeatures;
  vFeature: CMapXFeature;

  vDS: CMapXDataset;
  vField: CMapXField;

begin
 // Exit;

(*
  if mem_Layers.IsEmpty then
  begin
    db_ClearFields(mem_LayerFields);
    exit;
  end;
*)
 // sFileName:= mem_Layers[FLD_FILENAME];

  vLayer:=mapx_GetLayerByFileName(Map, aFileName);
  if VarIsNUll(vLayer) then
    Exit;




  if not Assigned(vLayer) then
  begin
 //   mem_LayerFields.EnableControls;
    exit;
  end;
(*
  if not mapx_CreateMemFromMapFile (aFileName, mem_LayerFields ) then
  begin
//    mem_LayerFields.EnableControls;
    Exit;
  end;*)

//  mem_LayerFields.DisableControls;
 // db_ClearFields(mem_LayerFields);
 // mem_LayerFields.Open;


  vPt := CoPoint.Create;
  vPt.Set_(FBLPoint.L, FBLPoint.B);


    vFeatures:= vLayer.SearchAtPoint(vPt, EmptyParam);

 // vLayer.BeginAccess(miAccessRead);

  for j:=1 to vFeatures.Count do
  begin

    vFeature:= vFeatures.Item[j];


    if (vLayer.Datasets.Count = 0) then
      Map.Datasets.Add (miDataSetLayer, vLayer, EmptyParam, EmptyParam,
                         EmptyParam, EmptyParam, EmptyParam, EmptyParam) ;


    ValueListEditor1.Strings.Clear;


    for iDataset:=1 to vLayer.Datasets.Count do
    begin
      vDS := vLayer.Datasets.Item[iDataset];


    //  mem_LayerFields.Append;
      for k := 1 to vDS.Fields.Count do
      begin

        vField := vDS.Fields.Item[k];

        sFieldName := vField.Name;

        s:=AsString(mapx_GetFieldValue(vDS,  vFeature, sFieldName));

        ValueListEditor1.InsertRow(sFieldName, s,True);
      end;
    end;
  end;

end;


procedure Tfrm_Map_tool_Info.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action:=caFree;
end;


procedure Tfrm_Map_tool_Info.ShowPoint(aBLPoint: TBLPoint);
begin
  FBLPoint.B:= aBLPoint.B;
  FBLPoint.L:= aBLPoint.L;

  FDisableEdit := True;

  LoadLayers();

  FDisableEdit := False;

 // dx_LayersChangeNode(nil, nil, nil);

 // Show;

end;

procedure Tfrm_Map_tool_Info.ValueListEditor_layersSelectCell(Sender: TObject;
    ACol, ARow: Integer; var CanSelect: Boolean);
var
  s: string;
begin
  if FDisableEdit then
    Exit;

  s := ValueListEditor_layers.Strings.ValueFromIndex[ARow-1];

  LoadLayerFields_new(s);
end;

end.
