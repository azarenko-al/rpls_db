inherited frm_Map_tool_Info: Tfrm_Map_tool_Info
  Left = 1224
  Top = 416
  Width = 415
  Height = 451
  BorderIcons = [biSystemMenu]
  Caption = #1048#1085#1092#1086#1088#1084#1072#1094#1080#1103
  OldCreateOrder = True
  Position = poDefault
  Visible = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited ToolBar1: TToolBar
    Width = 407
    Height = 73
  end
  inherited StatusBar1: TStatusBar
    Top = 404
    Width = 407
    Visible = False
  end
  object cxSplitter1: TcxSplitter [2]
    Left = 0
    Top = 177
    Width = 407
    Height = 8
    HotZoneClassName = 'TcxSimpleStyle'
    AlignSplitter = salTop
    Control = ValueListEditor_layers
  end
  object ValueListEditor1: TValueListEditor [3]
    Left = 0
    Top = 291
    Width = 407
    Height = 113
    Align = alBottom
    Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goColSizing, goAlwaysShowEditor, goThumbTracking]
    TabOrder = 3
    TitleCaptions.Strings = (
      #1055#1086#1083#1077
      #1047#1085#1072#1095#1077#1085#1080#1077)
    ColWidths = (
      135
      266)
  end
  object ValueListEditor_layers: TValueListEditor [4]
    Left = 0
    Top = 73
    Width = 407
    Height = 104
    Align = alTop
    Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goColSizing, goRowSelect, goThumbTracking]
    TabOrder = 4
    TitleCaptions.Strings = (
      #1057#1083#1086#1081
      #1060#1072#1081#1083)
    OnSelectCell = ValueListEditor_layersSelectCell
    ColWidths = (
      135
      266)
  end
  inherited FormStorage1: TFormStorage
    Left = 40
    Top = 14
  end
end
