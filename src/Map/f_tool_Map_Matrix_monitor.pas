unit f_tool_Map_Matrix_monitor;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, rxPlacemnt,  ToolWin, Db, dxmdaset, ADODB,   Menus, ActnList,
  cxGridLevel, cxClasses, cxControls, cxGridCustomView, Variants,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid, Grids,

  DBGrids, StdCtrls, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters,
  cxStyles, dxSkinsCore, dxSkinsDefaultPainters, dxSkinscxPCPainter,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxDBData,

  //u_mitab,


  dm_Onega_DB_data,
  f_map_Tool_base,

  u_Storage,

  u_vars,

  dm_RelMatrixFile_export_import,

  u_cx,

  I_rel_Matrix1,

  I_MapAct,
  I_MapView,

  dm_Main,

  u_func,
  u_Geo,
  u_db,

  u_const_db,

  dm_Rel_Engine,


  cxDBLookupComboBox

  ;

type
  Tfrm_Map_Matrix_Monitor = class(Tfrm_Map_Tool_base)
    PopupMenu1: TPopupMenu;
    ActionList1: TActionList;
    act_Opened_matrixes: TAction;
    N1: TMenuItem;
    ADOStoredProc1: TADOStoredProc;
    DataSource1: TDataSource;
    Action1: TAction;
    act_SaveFramesToFile: TAction;
    ADOConnection1: TADOConnection;
    t_ClutterModelType: TADOTable;
    ds_ClutterModelType: TDataSource;
    ADOStoredProc1id: TAutoIncField;
    ADOStoredProc1relief_id: TIntegerField;
    ADOStoredProc1checked: TBooleanField;
    ADOStoredProc1host: TStringField;
    ADOStoredProc1name: TStringField;
    ADOStoredProc1step_x: TFloatField;
    ADOStoredProc1step_y: TFloatField;
    ADOStoredProc1step_lat: TFloatField;
    ADOStoredProc1step_lon: TFloatField;
    ADOStoredProc1lat_min: TFloatField;
    ADOStoredProc1lat_max: TFloatField;
    ADOStoredProc1lon_min: TFloatField;
    ADOStoredProc1lon_max: TFloatField;
    ADOStoredProc1x_min: TFloatField;
    ADOStoredProc1y_min: TFloatField;
    ADOStoredProc1x_max: TFloatField;
    ADOStoredProc1y_max: TFloatField;
    ADOStoredProc1h: TIntegerField;
    ADOStoredProc1Clutter_Code: TIntegerField;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    col_Name: TcxGridDBColumn;
    col_rel_h: TcxGridDBColumn;
    col_Clutter_Name: TcxGridDBColumn;
    col_Clutter_Code: TcxGridDBColumn;
    col_ID: TcxGridDBColumn;
    cxGrid1Level1: TcxGridLevel;
    col_Clutter_H: TcxGridDBColumn;
    ADOStoredProc1Clutter_H: TIntegerField;
    col_Zone: TcxGridDBColumn;
    ADOStoredProc1zone: TIntegerField;
    procedure FormDestroy(Sender: TObject);
//    procedure Action2Execute(Sender: TObject);
    procedure act_Opened_matrixesExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
//    FRegPath : string;

    procedure UpdateData;

  protected
    procedure SetMapCursor (aBLCursor: TBLPoint); override;

  public
    procedure DrawTempLayer;
    class procedure ShowForm(aBLRect: TBLRect);
  end;



//====================================================================
implementation {$R *.DFM}
//====================================================================

const
  DEF_TEMP_LAYER_Relief = 'relief';

  FORM_CAPTION = '������� ������ �����';


//------------------------------------------------------------------
class procedure Tfrm_Map_Matrix_Monitor.ShowForm(aBLRect: TBLRect);
//------------------------------------------------------------------
var
  oForm: TForm;
  vIMapView: IMapViewX;

begin
  if not Assigned(IActiveMapView) then
    Exit;


  oForm:= IsFormExists (Application, Tfrm_Map_Matrix_Monitor.ClassName);

  if not Assigned(oForm) then
  begin
    vIMapView:=IActiveMapView;
    oForm:= Tfrm_Map_Matrix_Monitor.Create (Application);
    Tfrm_Map_Matrix_Monitor(oForm).SetIMapView(vIMapView);
  end;

  oForm.Show;



  if oForm.WindowState = wsMinimized then
    oForm.WindowState:= wsNormal;

//  Result:= Tfrm_Map(oForm);

//  dmRel_Engine.OpenByRect(aBLRect);
  dmRel_Engine.Open_All();

  Tfrm_Map_Matrix_Monitor(oForm).DrawTempLayer;


end;

//---------------------------------------------------
procedure Tfrm_Map_Matrix_Monitor.FormCreate(Sender: TObject);
//---------------------------------------------------
begin
  inherited;

//  FRegPath := vars_Form_GetRegPath(className);

//  cxGrid1DBTableView1.RestoreFromRegistry(FRegPath);

  g_Storage.RestoreFromRegistry(cxGrid1DBTableView1, ClassName);



  if ADOConnection1.Connected then
     ShowMessage('ADOConnection1.Connected');


  Caption:=FORM_CAPTION;

  cxGrid1.Align:=alClient;

  db_SetComponentADOConnection(Self, dmMain.ADOConnection1);

  db_TableReOpen(t_ClutterModelType, TBL_ClutterModelType);

  Assert(t_ClutterModelType.RecordCount>0, 't_ClutterModelType.RecordCount>0');

//  db_View (t_ClutterModelType);

  dmOnega_DB_data.Relief_Select(ADOStoredProc1, dmMain.ProjectID, True);

end;


procedure Tfrm_Map_Matrix_Monitor.FormDestroy(Sender: TObject);
begin
  g_Storage.StoreToRegistry(cxGrid1DBTableView1, ClassName);


 // cxGrid1DBTableView1.StoreToRegistry(FRegPath);

  FIMapView.TempLayer_Clear(DEF_TEMP_LAYER_Relief);

///////////////  dmRel_Engine.Close1;

  inherited; //!!!!
end;



//----------------------------------------------------
procedure Tfrm_Map_Matrix_Monitor.SetMapCursor (aBLCursor: TBLPoint);
//----------------------------------------------------
begin
  UpdateData;
end;


procedure Tfrm_Map_Matrix_Monitor.act_Opened_matrixesExecute(Sender: TObject);
begin
   dmRel_Engine.Dlg_ShowMatrixList;
end;


// ---------------------------------------------------------------
procedure Tfrm_Map_Matrix_Monitor.DrawTempLayer;
// ---------------------------------------------------------------
var
  blPoints: TBLPointArray;
//  blPointsF: TBLPointArrayF;
  sFileName: string;
begin
  FIMapView.TempLayer_Create(DEF_TEMP_LAYER_Relief);


  dmOnega_DB_data.Relief_Select(ADOStoredProc1, dmMain.ProjectID, True);


 // db_View (ADOStoredProc1);

  with ADOStoredProc1 do
     while not EOF do
  begin
    blPoints:= dmRelMatrixFile_export_import.ExtractReliefBoundsFromDataset(    ADOStoredProc1);

     sFileName := FieldByName(FLD_NAME).AsString;

     FIMapView.Layer_AddPolygon(DEF_TEMP_LAYER_Relief, blPoints, clBlue,0,2,  sFileName);


    Next;
  end;    
end;


// ---------------------------------------------------------------
procedure Tfrm_Map_Matrix_Monitor.UpdateData;
// ---------------------------------------------------------------
var
  sFileName: string;
  rec: Trel_Record;
  I: Integer;
  iZone: Integer;
begin
//  dmRel_Engine.FindPointBL_ex(aBLCursor, Rlf, sFileName);


  cxGrid1.BeginUpdate;

  with cxGrid1DBTableView1.DataController do
    for I := 0 to RecordCount - 1 do
    begin
      sFileName :=Values[i, col_Name.Index];

      if dmRel_Engine.FindPointBLInFile(sFileName, FBLCursor, rec, iZone) then
      begin
         Values[i, col_rel_h.Index]       :=rec.Rel_H;
         Values[i, col_clutter_Code.Index]:=rec.Clutter_Code;
         Values[i, col_clutter_H.Index]   :=rec.Clutter_H;
         Values[i, col_Zone.Index]        :=iZone;

      end else
      begin
         Values[i, col_rel_h.Index]       :=null;
         Values[i, col_clutter_Code.Index]:=null;
         Values[i, col_clutter_H.Index]   :=null;
         Values[i, col_Zone.Index]   :=null;

      end;

    end;

  cxGrid1.EndUpdate;
  cxGrid1.Repaint;

end;
          

end.