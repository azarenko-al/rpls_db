object frm_map_panorama: Tfrm_map_panorama
  Left = 142
  Top = 279
  Width = 1209
  Height = 549
  Caption = 'frm_map_panorama'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 29
    Width = 577
    Height = 474
    Align = alLeft
    Caption = 'Panel1'
    TabOrder = 0
  end
  object ToolBar1: TToolBar
    Left = 0
    Top = 0
    Width = 1201
    Height = 29
    Caption = 'ToolBar1'
    TabOrder = 1
    object Button1: TButton
      Left = 0
      Top = 2
      Width = 75
      Height = 22
      Caption = 'Run'
      TabOrder = 0
      OnClick = Button1Click
    end
  end
  object FilenameEdit1_Map: TFilenameEdit
    Left = 696
    Top = 296
    Width = 425
    Height = 21
    NumGlyphs = 1
    TabOrder = 2
    Text = 'D:\333\test.sit'
  end
  object FilenameEdit2_rsc: TFilenameEdit
    Left = 696
    Top = 336
    Width = 425
    Height = 21
    NumGlyphs = 1
    TabOrder = 3
    Text = 'D:\_panorama_data\Topo100t.rsc'
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 503
    Width = 1201
    Height = 19
    Panels = <>
    SimplePanel = True
  end
  object ADOConnection1: TADOConnection
    Connected = True
    ConnectionString = 
      'Provider=SQLOLEDB.1;Integrated Security=SSPI;Persist Security In' +
      'fo=False;User ID=sa;Initial Catalog=onega;Data Source=ALEX\SQLEX' +
      'PRESS'
    LoginPrompt = False
    Mode = cmShareDenyNone
    Provider = 'SQLOLEDB.1'
    Left = 872
    Top = 88
  end
  object qry_Prop: TADOQuery
    Active = True
    Connection = ADOConnection1
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select  top 100   * from Property')
    Left = 888
    Top = 160
  end
  object q_Links: TADOQuery
    Active = True
    Connection = ADOConnection1
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select  top 100   * from view_Link')
    Left = 992
    Top = 160
  end
end
