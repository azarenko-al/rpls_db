unit I_MapView;

interface
uses
  Classes, Db,

  u_Map_Classes,

  MapXLib_TLB,

  u_Geo;


const
    //0-free; 1-select; 2-cursor
  MAP_EVENT_ON_FREE_   = 0;
  MAP_EVENT_ON_SELECT_ = 1;
  MAP_EVENT_ON_CURSOR_ = 2;


type
  TMapNotifyEvent = procedure (Sender: TObject; aEventIndex: integer) of object;


  IMapViewX = interface(IInterface)
    ['{3F327FFA-4A69-4BE0-BB79-1BFE137E369A}']

    //---------------------------------------------
    function GetBLCursor: TBLPoint;
    //---------------------------------------------

    procedure Clear;
    procedure MAP_SAVE_AND_CLOSE_WORKSPACE;
    procedure MAP_RESTORE_WORKSPACE;

    procedure SelectObjectByID(aObjName: string; aBLPoint: TBLPoint; aID: Integer);

    procedure ChangeProfileFirstPointPos(aBLPoint: TBLPoint);

    function GetLayerByObjectName(aObjectName: string): CMapxLayer;
//    function GetMapObjectLayer(aType: TrpObjectType): CMapxLayer;
    function GetMapLayerByFileName(aFileName: string): CMapxLayer;

    function GetLayerByIndex_1(aIndex: integer): CMapxLayer;


    function GetLayerIndexByName_from_1(aName: string): integer;
    function GetLayerByName(aName: string): CMapxLayer;
    procedure RemoveLayerByName(aName: string);

    function GetFeaturesByScreenBounds_ByObjectName(aObjectName: string): CMapXFeatures;

    { TODO : remove }
  //  procedure CloseMap;

    function Count: integer;



    procedure Dlg_Layers;
//    procedure Dlg_;

    function GetBounds: TBLRect;
    function GetPlanBounds: TBLRect;

    procedure MAP_SHOW_BLRECT (aBLRect: TBLRect);
    procedure MAP_SHOW_VECTOR(aBLVector: TBLVector);
    procedure MAP_SHOW_POINT(aBLPoint: TBLPoint);
//    procedure MAP_SHOW_RECT(aBLRect: TBLRect);

    function Map: TMap;

    procedure MapRefresh;
    procedure SetDefaultTool;

  //  function GetSelectedDataset: TDataSet;

    function GetSelectedItemList: TMapSelectedItemList;

  //   SelectedItemList: TMapSelectedItemList;

    //-----------------------------
    // User_Layer
    //-----------------------------
    procedure UserLayerDraw;
    procedure UserLayerDestroy;

    procedure MAP_CLEAR_USER_DRAWINGS;

    procedure MAP_ADD_USER_LINE (aColor, aWidth: Integer; aBLPoint1,aBLPoint2: TBLPoint);
    procedure Map_Add_user_Poly(var aBLPointArr: TBLPointArrayF; aColor: integer);
    procedure MAP_ADD_USER_TEXT(aBLPoint: TBLPoint; aText: string);
    
(*
    procedure MAP_ADD_USER_ARROW(aColor, aWidth: Integer; aBLVector: TBLVector);
    procedure MAP_ADD_USER_BEZIER(aColor: Integer; var aBLPointArr: TBLPointArrayF);
*)

    procedure HideMap1(aFileName: string);
    procedure RestoreMap1(aFileName: string);

    procedure MapAddSimple(aFileName: string);

//    procedure MAP_SET_DEFAULT_TOOL;

    function TempLayer_Create(aLayerName: string): CMapxLayer;
    procedure TempLayer_Clear(aLayerName: string);

    procedure Layer_AddPolygon(aLayerName: string; aBLPoints:
        TBLPointArray; aColor, aStyle, aWidth: Integer; aLabel: string);

    procedure MAP_ADD_LAYER_LINE(aLayerName: string;
                                    //  aCMapxLayer: CMapxLayer;
                                      aBLVector: TBLVector;
                                      aColor, aStyle, aWidth: integer;
                                      aLabel: string );

    procedure MAP_ADD_LAYER_BEZIER (aLayerName: string;
                                       //  aCMapxLayer: CMapxLayer;
                                         var aBLPointArr: TBLPointArrayF;
                                         aColor: integer);


//    function  RegisterNotifyEvent(Sender: TObject; aEvent: TMapNotifyEvent): integer;
//    procedure UnRegisterSender(aProcIndex: integer);


//    procedure RemoveCalcRegionMaps(aCalcRegionID: Integer);

    property BLCursor: TBLPoint read GetBLCursor;

  end;



  IMapX = interface(IInterface)
    ['{F3B2FC9B-65AC-449A-B0C8-1F51B9520D40}']

    //---------------------------------------------
    function GetIMapView: IMapViewX;
  end;



implementation

end.

