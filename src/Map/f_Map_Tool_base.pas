unit f_Map_Tool_base;

interface

uses
  Classes, Controls, Forms, ComCtrls, rxPlacemnt,  Dialogs,

  I_MapView,
  u_func_msg, 
  u_Geo, ToolWin
  ;

type
  Tfrm_Map_Tool_base = class(TForm, IMessageHandler)
    ToolBar1: TToolBar;
    StatusBar1: TStatusBar;
    FormStorage1: TFormStorage;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    FProcIndex: integer;
    procedure UpdateStatusBar;

  protected
    FIMapView: IMapViewX;
    FRegPath: string;

    FBLCursor: TBLPoint;

//    procedure DoOnMapEvent(Sender: TObject; aEventIndex: integer); virtual;
    procedure SetMapCursor (aBLCursor: TBLPoint); virtual;

    procedure GetMessage(aMsg: TEventType; aParams: TEventParamList ; var aHandled:
        boolean); virtual;

  public
    procedure SetIMapView(aIMapView: IMapViewX);

  end;



//====================================================================
implementation {$R *.DFM}
//====================================================================

const
  REGISTRY_Common_ROOT  = 'Software\Onega\Common\Forms\';


//--------------------------------------------------------------------
procedure Tfrm_Map_Tool_base.FormCreate(Sender: TObject);
//--------------------------------------------------------------------
begin
  inherited;

  if FormStorage1.Active then
    ShowMessage('Tfrm_Map_Tool_base.FormStorage1.Active ');
    


  FRegPath:=REGISTRY_Common_ROOT+ ClassName +'\';

  FormStorage1.IniFileName:=FRegPath;
  FormStorage1.IniSection:='Window';
 // FormPlacement1.UseRegistry:=True;
  FormStorage1.Active:=True;

{
  if Assigned(IMainMapView) then
  begin
    FIMapView:=IMainMapView;
    FProcIndex:=IMainMapView.RegisterNotifyEvent(Self, DoOnMapEvent);
  end;}

end;


procedure Tfrm_Map_Tool_base.SetMapCursor;
begin
  //StatusBar1.Panels[0].Text:=geo_FormatBLPoint(FBLCursor);
//  StatusBar1.Panels[1].Text:=geo_FormatXYPoint(geo_BL_to_XY(FBLCursor, EK_KRASOVSKY42, EK_WGS84));

end;


procedure Tfrm_Map_Tool_base.UpdateStatusBar;
begin
//  StatusBar1.Panels[0].Text:=geo_FormatBLPoint(FBLCursor);
  StatusBar1.SimpleText:=geo_FormatBLPoint(FBLCursor);
//  StatusBar1.Panels[1].Text:=geo_FormatXYPoint(geo_BL_to_XY(FBLCursor, EK_KRASOVSKY42, EK_WGS84));

end;




procedure Tfrm_Map_Tool_base.FormDestroy (Sender: TObject);
begin

//  if Assigned(FIMapView) then
//    FIMapView.UnRegisterSender (FProcIndex);

  FIMapView := nil;

  inherited;
end;


procedure Tfrm_Map_Tool_base.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action:=caFree;
end;


// ---------------------------------------------------------------
procedure Tfrm_Map_Tool_base.GetMessage(aMsg: TEventType; aParams:  TEventParamList ; var aHandled: boolean);
// ---------------------------------------------------------------
begin
//  inherited;

  case aMsg of

      et_MAP_EVENT_ON_CURSOR:
            begin
              FBLCursor:=MakeBLPoint(aParams.Lat, aParams.Lon);
//              FIMapView.BLCursor;
              UpdateStatusBar();
              SetMapCursor (FBLCursor);
            end;

      et_MAP_EVENT_ON_Free: Free;
  end;
end;


procedure Tfrm_Map_Tool_base.SetIMapView(aIMapView: IMapViewX);
begin
  Assert(Assigned(aIMapView));

  FIMapView:=aIMapView;
//  FProcIndex:=aIMapView.RegisterNotifyEvent(Self, DoOnMapEvent);

end;


end.


(*

{

procedure Tfrm_Map_Tool_base.DoOnMapEvent(Sender: TObject;  aEventIndex: integer);
begin
{
 case aEventIndex of

    MAP_EVENT_ON_CURSOR_:
          begin
            FBLCursor:=FIMapView.BLCursor;
            UpdateStatusBar();
            SetMapCursor (FBLCursor);
          end;

    MAP_EVENT_ON_Free_:
        Free;
  end;
  }
end;


