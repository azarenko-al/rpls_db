unit u_Map_Classes;

interface

uses Classes, DB,

u_func,
  u_db;

type
  // -------------------------------------------------------------------
  TMapSelectedItem = class(TCollectionItem)
  // -------------------------------------------------------------------
  public
    ID            : Integer; // record ID

    FileName      : string;
    Caption       : string;
    Name          : string;

  //  GUID : string;

    ObjName    : string;

  end;


  // ---------------------------------------------------------------
  TMapSelectedItemList = class(TCollection)
  // ---------------------------------------------------------------
  private
    function GetItems(Index: Integer): TMapSelectedItem;
  public
    constructor Create;
    function AddItem: TMapSelectedItem;

    function FindByObjName(aObjName: string): TMapSelectedItem;
    procedure InitDataset(aDataset: TDataset);
    procedure SaveToDataset(aDataset: TDataset);

    property Items[Index: Integer]: TMapSelectedItem read GetItems; default;
  end;


implementation

const
  FLD_OBJCAPTION  = 'OBJCAPTION';
  FLD_GEOREGION_ID= 'GEOREGION_ID'; 


constructor TMapSelectedItemList.Create;
begin
  inherited Create (TMapSelectedItem);
end;

function TMapSelectedItemList.AddItem: TMapSelectedItem;
begin
  Result := TMapSelectedItem(Add);
end;


function TMapSelectedItemList.FindByObjName(aObjName: string): TMapSelectedItem;
var I: Integer;
begin
 Result := nil;

  for I := 0 to Count - 1 do
    if Eq(Items[i].ObjName, aObjName) then
    begin
      Result := Items[i];
      Break;
    end;
end;


function TMapSelectedItemList.GetItems(Index: Integer): TMapSelectedItem;
begin
  Result := TMapSelectedItem(inherited Items[Index]);
end;

procedure TMapSelectedItemList.InitDataset(aDataset: TDataset);
begin
  db_CreateField (aDataset,
           [db_Field(FLD_CAPTION,      ftString),
            db_Field(FLD_NAME,         ftString),
          //  db_Field(FLD_GUID,         ftString),
            db_Field(FLD_FILENAME,     ftString, 200),
            db_Field(FLD_OBJNAME,      ftString),
            db_Field(FLD_OBJCAPTION,   ftString),

            db_Field(FLD_GEOREGION_ID, ftInteger),
            db_Field(FLD_ID,           ftInteger)
//            db_Field(FLD_TYPE,         ftString)
           ]);
end;

procedure TMapSelectedItemList.SaveToDataset(aDataset: TDataset);
var
  obj: TMapSelectedItem;
  i : Integer;
begin
  db_CreateField (aDataset,
           [db_Field(FLD_CAPTION,      ftString),
            db_Field(FLD_NAME,         ftString),
           // db_Field(FLD_GUID,         ftString),
            db_Field(FLD_FILENAME,     ftString, 200),
            db_Field(FLD_OBJNAME,      ftString),
            db_Field(FLD_OBJCAPTION,   ftString),

            db_Field(FLD_GEOREGION_ID, ftInteger),
            db_Field(FLD_ID,           ftInteger)
//            db_Field(FLD_TYPE,         ftString)
           ]);

  aDataset.Open;

  for I := 0 to Count - 1 do
  begin
    obj :=Items[i];

    db_AddRecord_ (aDataset,
               [FLD_ID,           obj.ID,
//                db_Par(FLD_NAME,       IIF(sName='', sCaption, sName)),
                FLD_NAME,         obj.Name,
                FLD_CAPTION,      obj.Caption,
             //   db_Par(FLD_GUID,         obj.GUID),
                FLD_OBJNAME,      obj.ObjName,
              //  db_Par(FLD_OBJCAPTION, sObjCaption),
//                db_Par(FLD_GEOREGION_ID, iGeoRegion),
                FLD_FILENAME,     obj.FileName
                ]);

  end;

end;

end.
