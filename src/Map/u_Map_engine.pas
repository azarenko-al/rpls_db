
unit u_Map_engine;

interface


uses
  Windows, SysUtils,   Variants, Forms, Dialogs,
  ExtCtrls, ComCtrls, Menus, Controls , Classes, Graphics,

  XMLIntf,

  u_xml_document,

  MapXLib_TLB,

  u_func,
  u_mapx,
 // u_Geo_wms,
  u_Geo
  ;


type
  TOnMapCursorEvent = procedure (aBLPoint: TBLPoint) of object;
  TOnMapPolyEvent   = procedure (var aBLPoints: TBLPointArrayF) of object;
  TOnMapMouseDownEvent = procedure (aBLPoint: TBLPoint) of object;


  TMapEngine = class(TObject)
  private
    FMoveBitmap: Graphics.TBitmap;
    FOldMousePos: TPoint;
    CursorPosXY: TPoint;

 //   FMoveBitmap: TBitmap;

    FCursorPos: TBLPoint;

    FLeftButtonClicked : Boolean;
    FRightButtonPressed: boolean;

  private
    FMap: TMap;
  private
    FOnMapCursor: TOnMapCursorEvent;
    FOnMapMouseDown: TOnMapMouseDownEvent;
    FOnMapPoly: TOnMapPolyEvent;

//    function Add_Tile___________(aFileName: string): CMapXLayer;
    function CreateTempLayerWithFields(aName: string; aParams: array of
        TmiFieldRec): CMapXLayer;
    function GetLayerByName(aName: string): CMapXLayer;
    function GetLayerIndexByName_from_1(aName: string): integer;
//    procedure ExtractTileXYZ(aFileName: string; var aX, aY, aZ: integer);
    function GetScaleInCm: double;
  { TODO : remove }
//    procedure DeleteMapsByDir (aCalcRegionDir: string);
//    procedure DeleteMapsByDir(aDir: string);
    procedure Log(aMsg: string);
    procedure Map1MouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y:
        Integer);
    procedure Map1MouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
    procedure Map1MouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y:
        Integer);
    procedure Map1MouseWheel(Sender: TObject; Flags: integer; zDelta: Smallint; var X, Y:
        Single; var EnableDefault: WordBool);
    procedure Map1PolyToolUsed(Sender: TObject; ToolNum: Smallint; Flags: Integer; const
        Points: IDispatch; bShift, bCtrl: WordBool; var EnableDefault: WordBool);
    procedure PaintBitmapWhileMoving;
    procedure Init;
    procedure RemoveLayer(aName: string);
    procedure SetCenter(aBLPoint: TBLPoint);
//    procedure SetDefaultDisplayCoordSys;
    procedure SetOnMapCursor(const Value: TOnMapCursorEvent);
//    function Tile_Get_outside_Rect_old(aRect: TTileBounds; aList: TStrings):
     //   Integer;
  public
  //    MapList: TMapViewList;
  public
    PopupMenu: TPopupMenu;


    FFirstPointClicked: boolean;
    FFirstPointPos: TBLpoint;

    procedure AlignBoundsForBLVector(aBLVector: TBLVector);
    procedure FormKeyDown(var Key: Word; Shift: TShiftState; aCursorPos: TBLPoint);

    function GetBounds: TBLRect;
    function GetPlanBounds: TBLRect;
    function GetScale: double;

//    procedure PageDown;
//    procedure PageUp;
//    procedure PreviousTool(); virtual; abstract;
    procedure SetBounds(aBLRect: TBLRect);

    function WorkspaceLoad(aFileName: string): boolean;
    procedure ZoomInRect(aRect: TBLRect);

    constructor Create(aMap: TMap);
    destructor Destroy; override;

    function Add_Map(aFileName: string): CMapXLayer;

    procedure Clear;
    procedure Legend_Clear;
    procedure Del_Layer_ByName(aName: string);
    procedure Del_List(aList: TStrings);
    procedure Del_Map(aFileName: string);
    procedure Legend_Show_;

//    function GetTileBounds(var aRec: TTileBounds): Boolean;
    function MapExists(aFileName: string): Boolean;

    function MapToScreen(aBLPoint: TBLPoint): TPoint;
    procedure SetCenter_LatLon(aLat, aLon: double);
    procedure SetBoundsForBLVector(aBLVector: TBLVector);

    procedure SetNumericCoordSys_DATUM_WGS84;

    function SetProj_Mercator: Integer;

    procedure SetScale(aScale: double);
    procedure Set_LongLat_WGS84;
    function ShowLegend(aTitle: string; aMapFileName: string): boolean;

    procedure TilesClear;
//    procedure Tile_Del_outside_Rect(aRect: TTileBounds);
//    function Tile_Get_outside_Rect(aRect: TTileBounds; aList: TStrings): Integer;

    property OnMapMouseDown: TOnMapMouseDownEvent read FOnMapMouseDown write FOnMapMouseDown;
    property OnMapPoly: TOnMapPolyEvent read FOnMapPoly write FOnMapPoly;
    property OnMapCursor: TOnMapCursorEvent read FOnMapCursor write SetOnMapCursor;

  end;


const
   // Custom Tool Constants
   CUSTOM_ARROW_TOOL    = 1;
   CUSTOM_POINT_TOOL    = 2;
   CUSTOM_LINE_TOOL     = 3;
   CUSTOM_POLYGON_TOOL  = 4;
   CUSTOM_POLYLINE_TOOL = 5;
   CUSTOM_RING_TOOL     = 6;
   CUSTOM_RECT_TOOL     = 7;

   CUSTOM_INFO_TOOL     = 8;
   CUSTOM_LINER         = 9;
   CUSTOM_SELECT_TOOL   = 10;

  // CUSTOM_NEIGHBOR_TOOL = 11;
   //CUSTOM_CoChannelFreqs_TOOL = 12;

   RULERTOOLID = 500;


   ELLIPSOID_WGS  = 28;
   ELLIPSOID_KRAS = 3;



implementation

uses StrUtils;

constructor TMapEngine.Create(aMap: TMap);
begin
  inherited Create;

  FMoveBitmap := Graphics.TBitmap.Create();
  FMap := aMap;

  Init;
//  MapList := TMapViewList.Create();
end;

destructor TMapEngine.Destroy;
begin
//  FreeAndNil(MapList);
  FreeAndNil(FMoveBitmap);

  inherited;
end;


procedure TMapEngine.Set_LongLat_WGS84;
begin
  FMap.NumericCoordSys.Set_ (miLongLat, DATUM_WGS84,
//  FMap.NumericCoordSys.Set_ (miLongLat, DATUM_KRASOVKSY42,
      EmptyParam, EmptyParam, EmptyParam, EmptyParam,
      EmptyParam, EmptyParam, EmptyParam, EmptyParam,
      EmptyParam, EmptyParam, EmptyParam, EmptyParam);
  
end;



// ---------------------------------------------------------------
function TMapEngine.SetProj_Mercator: Integer;
// ---------------------------------------------------------------
var
  k: Integer;
  v1: Variant;
  v2: Variant;
  v3: Variant;
begin
  //it;

//  k:=FMap.NumericCoordSys.type_;
//  k:=FMap.NumericCoordSys.Datum.Ellipsoid;
//  k:=FMap.NumericCoordSys.Units;

//Exit;


  v1:=FMap.DisplayCoordSys.type_;
  v2:=FMap.DisplayCoordSys.Datum.Ellipsoid;
  v3:=FMap.DisplayCoordSys.Units;

//  Assert( FMap.DisplayCoordSys.Units = miUnitDegree);

  
//
//type
//  MapUnitConstants = TOleEnum;
//const
//  miUnitMile = $00000000;
//  miUnitKilometer = $00000001;
//  miUnitInch = $00000002;
//  miUnitFoot = $00000003;
//  miUnitYard = $00000004;
//  miUnitMillimeter = $00000005;
//  miUnitCentimeter = $00000006;
//  miUnitMeter = $00000007;
//  miUnitSurveyFoot = $00000008;
//  miUnitNauticalMile = $00000009;
//  miUnitTwip = $0000000A;
//  miUnitPoint = $0000000B;
//  miUnitPica = $0000000C;
//  miUnitDegree = $0000000D;
//  miUnitLink = $0000001E;
//  miUnitChain = $0000001F;
//  miUnitRod = $00000020;
//


 // Exit;

  //  miUnitDegree = $0000000D;

  //    procedure Set_(Type_: CoordSysTypeConstants; Datum: OleVariant; Units: OleVariant;
//                   OriginLongitude: OleVariant; OriginLatitude: OleVariant;

//  FMap.NumericCoordSys.Datum:=DATUM_WGS84;
//
//    procedure Set_(Type_: CoordSysTypeConstants; Datum: OleVariant; Units: OleVariant;
//                   OriginLongitude: OleVariant; OriginLatitude: OleVariant;
//                   StandardParallelOne: OleVariant; StandardParallelTwo: OleVariant;
//                   Azimuth: OleVariant; ScaleFactor: OleVariant; FalseEasting: OleVariant;
//                   FalseNorthing: OleVariant; Range: OleVariant; Bounds: OleVariant;
//                   AffineTransform: OleVariant); dispid 15;


 // Exit;

 // FMap.DisplayCoordSys.Set_ (miMercator, DATUM_WGS84, miUnitDegree,

  FMap.DisplayCoordSys.Set_ (miMercator, DATUM_WGS84,
  //miUnitDegree,
   miUnitMeter,
   0,
//  FMap.NumericCoordSys.Set_ (miLongLat, DATUM_KRASOVKSY42,
      EmptyParam, EmptyParam, //EmptyParam,
      EmptyParam, EmptyParam, EmptyParam, EmptyParam,
      EmptyParam, EmptyParam, EmptyParam, EmptyParam);


  FMap.NumericCoordSys.Set_ (miMercator, DATUM_WGS84, 
  //miUnitDegree,  
   miUnitMeter, 
   0,
//  FMap.NumericCoordSys.Set_ (miLongLat, DATUM_KRASOVKSY42,
      EmptyParam, EmptyParam, //EmptyParam,
      EmptyParam, EmptyParam, EmptyParam, EmptyParam,
      EmptyParam, EmptyParam, EmptyParam, EmptyParam);
      
               
//  CoordSys Earth Projection 10, 104, "m", 0
//
//    procedure Set_(Type_: CoordSysTypeConstants; Datum: OleVariant; Units: OleVariant; 
//                   OriginLongitude: OleVariant; OriginLatitude: OleVariant; 
//                   StandardParallelOne: OleVariant; StandardParallelTwo: OleVariant; 
//                   Azimuth: OleVariant; ScaleFactor: OleVariant; FalseEasting: OleVariant; 
//                   FalseNorthing: OleVariant; Range: OleVariant; Bounds: OleVariant; 
//                   AffineTransform: OleVariant); dispid 15;

  
  //Assert( FMap.DisplayCoordSys.Units = miUnitMeter);
{
  v1:=FMap.DisplayCoordSys.type_;  
  v2:=FMap.DisplayCoordSys.Datum.Ellipsoid;
  v3:=FMap.DisplayCoordSys.Units;
  

  
  v1:=FMap.NumericCoordSys.type_;  
  v2:=FMap.DisplayCoordSys.Datum.Ellipsoid;
  v3:=FMap.DisplayCoordSys.Units;
  }
  
end;
{

procedure TMapEngine.SetNumericCoordSys_DATUM_WGS84;
begin
  FMap.NumericCoordSys.Set_ (miLongLat, DATUM_WGS84,
//  FMap.NumericCoordSys.Set_ (miLongLat, DATUM_KRASOVKSY42,
      EmptyParam, EmptyParam, EmptyParam, EmptyParam,
      EmptyParam, EmptyParam, EmptyParam, EmptyParam,
      EmptyParam, EmptyParam, EmptyParam, EmptyParam);

end;

}


procedure TMapEngine.AlignBoundsForBLVector(aBLVector: TBLVector);
var blRect: TBLRect;
begin
  blRect:=geo_BoundBLVector(aBLVector);
  blRect:=geo_ZoomRect (blRect, 0.01);
  SetBounds (blRect);
end;


//--------------------------------------------------------------------
procedure TMapEngine.Map1MouseWheel(Sender: TObject; Flags: integer; zDelta: Smallint;
    var X, Y: Single; var EnableDefault: WordBool);
//--------------------------------------------------------------------
var
  dNewZoom: double;
begin
  EnableDefault:= false;

  dNewZoom:= IIF(zDelta < 0, FMap.Zoom * 2, FMap.Zoom / 2);
  if dNewZoom > 0 then
    FMap.ZoomTo(dNewZoom, FCursorPos.L, FCursorPos.B);

end;


//--------------------------------------------------------------------
function TMapEngine.GetPlanBounds: TBLRect;
//--------------------------------------------------------------------
begin
  Result:=mapx_GetPlanBounds (FMap);
end;


// -------------------------------------------------------------------
function TMapEngine.GetScale: double;
// -------------------------------------------------------------------
begin
  Result:=mapx_GetScale (FMap);
end;

// -------------------------------------------------------------------
procedure TMapEngine.SetScale(aScale: double);
// -------------------------------------------------------------------
begin
  mapx_SetScale (FMap, aScale);
end;


   {
procedure TMapEngine.PageDown;
var blRect: TBLRect;
begin
  blRect:=GetBounds();
  blRect:=geo_MoveBLRect (blRect, - Abs(blRect.TopLeft.B - blRect.BottomRight.B), 0);
  ZoomInRect(blRect);
end;

procedure TMapEngine.PageUp;
var blRect: TBLRect;
begin
  blRect:=GetBounds();
  blRect:=geo_MoveBLRect (blRect, + Abs(blRect.TopLeft.B - blRect.BottomRight.B), 0);
  ZoomInRect(blRect);
end;
  }



// -------------------------------------------------------------------
procedure TMapEngine.FormKeyDown(var Key: Word; Shift: TShiftState; aCursorPos: TBLPoint);
// -------------------------------------------------------------------
   function DoGetBounds: TBLRect;
   begin
      Result:=mapx_GetMAPBounds (FMap);
   end;

   procedure DoSetBounds(aBLRect: TBLRect);
   begin
     mapx_SetBounds (FMap, aBLRect);
   end;

   procedure DoSetCenter(aBLPoint: TBLPoint);
   begin
      mapx_SetCenter (FMap, aBLPoint);
   end;


var
  blRect: TBLRect;

{const
  VK_PAGEDOWN_ = 34;
  VK_PAGEUP_ = 33;
}

begin
  if Key = VK_ESCAPE then
  begin
    mapx_ClearSelection (FMap);

///////////////    ClearSelection();
//    SetDefaultTool();
  end else
 {
  if (Key=VK_PAGEDOWN_)  then
  begin
    blRect:=DoGetBounds();
   // blRect:=
    blRect:=geo_MoveBLRect (blRect, - Abs(blRect.TopLeft.B - blRect.BottomRight.B), 0);

//    ZoomInRect(blRect);
    DoSetBounds(blRect);
  end else

  // ---------------------------------------------------------------
  if (Key=VK_PAGEUP_)  then
  // ---------------------------------------------------------------
  begin
    blRect:=DoGetBounds();
   // blRect:=
    blRect:=geo_MoveBLRect (blRect, + Abs(blRect.TopLeft.B - blRect.BottomRight.B), 0);
//    ZoomInRect(blRect);
    DoSetBounds(blRect);
  end else

  if Key = VK_SPACE then
  begin
    DoSetCenter(aCursorPos);
  end else
  }


  if (Key=VK_ADD) or (Key=187) then //btn +
  begin
    blRect:=geo_MoveBLRectToCenter (aCursorPos, DoGetBounds());
    blRect:=geo_ZoomIn(blRect);
    DoSetBounds(blRect);
//    ZoomInRect(geo_ZoomIn(blRect));
  end else

  if (Key=VK_SUBTRACT) or (Key=189) then
  begin
    blRect:= geo_MoveBLRectToCenter (aCursorPos, DoGetBounds());
    blRect:=geo_ZoomOut(blRect, 2);
    DoSetBounds (blRect);
  end;

end;

//--------------------------------------------------------------------
function TMapEngine.GetBounds: TBLRect;
//--------------------------------------------------------------------
begin
  Result:=mapx_GetMapBounds (FMap);
//  Result:=mapx_XRectangleToBLRect(Map1.Bounds);
end;

procedure TMapEngine.Log(aMsg: string);
begin

(*  if Assigned(FOnLog) then
    FOnLog('map: ' + aMsg);
*)
end;

//---------------------------------------------------
function TMapEngine.MapToScreen(aBLPoint: TBLPoint): TPoint;
//---------------------------------------------------
var tempX, tempY: single;
begin
  FMap.ConvertCoord(tempX, tempY, aBLPoint.L,  aBLPoint.B, miMapToScreen);
  Result.X:=Trunc(tempX);
  Result.Y:=Trunc(tempY);
end;


//--------------------------------------------------------------------
// ��������� �������� ����� �� ��������� ������ ������ ����
procedure TMapEngine.PaintBitmapWhileMoving;
//--------------------------------------------------------------------
var
  DC: HDC;
  brush: HBRUSH;
  r1, r2: TRect;
  iOffsetX, iOffsetY: Integer;
begin
  if FRightButtonPressed then
  begin
    iOffsetX:= CursorPosXY.X - FOldMousePos.X;
    iOffsetY:= CursorPosXY.Y - FOldMousePos.Y;

    if (iOffsetX=0) and (iOffsetY=0) then
      exit;

    Screen.Cursor:=crHandPoint;

//    CursorSet (crHandPoint);

    DC:= GetDC(FMap.Handle);
    brush:= CreateSolidBrush(clWhite);

    r1:= FMap.ClientRect;
    r2:= r1;
    if iOffsetY<0 then
      r1.Top:= r1.bottom+iOffsetY
    else
      r1.bottom:= r1.top+iOffsetY;

    if iOffsetX<0 then
      r2.left:= r2.right+iOffsetX
    else
      r2.right:= r2.left+iOffsetX;

    FillRect(DC, r1, brush);
    FillRect(DC, r2, brush);

    DeleteObject(brush);
    BitBlt(DC, iOffsetX, iOffsetY, FMap.Width, FMap.Height, FMoveBitmap.Canvas.Handle, 0, 0, SRCCOPY);
    ReleaseDC(FMap.Handle, DC);
  end;
end;

//--------------------------------------------------------------------
procedure TMapEngine.SetBounds(aBLRect: TBLRect);
//--------------------------------------------------------------------
begin
  Log(geo_FormatBLRect(aBLRect));

  mapx_SetBounds (FMap, aBLRect);
end;


//--------------------------------------------------------------------
procedure TMapEngine.SetCenter(aBLPoint: TBLPoint);
//--------------------------------------------------------------------
begin

  mapx_SetCenter (FMap, aBLPoint);
end;

//--------------------------------------------------------------------
procedure TMapEngine.SetCenter_LatLon(aLat, aLon: double);
//--------------------------------------------------------------------
begin
  SetCenter (MakeBLPoint(aLat, aLon));
end;


procedure TMapEngine.SetBoundsForBLVector(aBLVector: TBLVector);
begin
  mapx_SetBoundsForBLVector(FMap, aBLVector);
end;


//--------------------------------------------------------------------
procedure TMapEngine.Init;
//--------------------------------------------------------------------
var
  vEmpty: Variant;
begin
//  Assert(Assigned(aMap), 'Value not assigned');

//  FMap := aMap;

  FMap.Title.X:=0;
  FMap.Title.Y:=0;
  FMap.Title.Editable:= False;
  FMap.Title.Border:= False;
  FMap.Title.Caption:=' ';
  FMap.Title.Visible:=False;

  TVarData(vEmpty).vType := varError;
  TVarData(vEmpty).vError := DISP_E_PARAMNOTFOUND;


  FMap.CreateCustomTool(RULERTOOLID,          miToolTypeLine,    miCrossCursor, miCrossCursor, miCrossCursor, vEmpty);

  FMap.CreateCustomTool(CUSTOM_LINER,         miToolTypeLine,    miCrossCursor, miCrossCursor, miCrossCursor, vEmpty);
  FMap.CreateCustomTool(CUSTOM_ARROW_TOOL,    miToolTypePoint,   miArrowToolCursor, miArrowCursor, miArrowCursor, vEmpty);
  FMap.CreateCustomTool(CUSTOM_POINT_TOOL,    miToolTypePoint,   miCrossCursor, miArrowCursor, miArrowCursor, vEmpty);
  FMap.CreateCustomTool(CUSTOM_INFO_TOOL,     miToolTypePoint,   miCrossCursor, miArrowCursor, miArrowCursor, vEmpty);
  FMap.CreateCustomTool(CUSTOM_LINE_TOOL,     miToolTypeLine,    miCrossCursor, miArrowCursor, miArrowCursor, vEmpty);
  FMap.CreateCustomTool(CUSTOM_POLYGON_TOOL,  miToolTypePoly,    miCrossCursor, miArrowCursor, miArrowCursor, vEmpty);
  FMap.CreateCustomTool(CUSTOM_POLYLINE_TOOL, miToolTypePoly,    miCrossCursor, miArrowCursor, miArrowCursor, vEmpty);
  FMap.CreateCustomTool(CUSTOM_RING_TOOL,     miToolTypeCircle,  miCrossCursor, miArrowCursor, miArrowCursor, vEmpty);
  FMap.CreateCustomTool(CUSTOM_RECT_TOOL,     miToolTypeMarquee, miCrossCursor, miArrowCursor, miArrowCursor, vEmpty);
  FMap.CreateCustomTool(CUSTOM_SELECT_TOOL,   miToolTypeMarquee, miCrossCursor, miArrowCursor, miArrowCursor, vEmpty);


  FMap.MousewheelSupport:=3; //3 - miFullMousewheelSupport
  FMap.BackColor    :=clWhite;
  FMap.CurrentTool  := miZoomInTool;


//  FMap.PaperUnit:= miUnitCentimeter;
//  FMap.MapUnit  := miUnitKilometer;


  FMap.ShowHint:= true;
  FMap.InfotipSupport:= False;


  FMap.OnPolyToolUsed :=Map1PolyToolUsed;
  FMap.OnMouseMove    :=Map1MouseMove;
  FMap.OnMouseWheel   :=Map1MouseWheel;
  FMap.OnMouseUp      :=Map1MouseUp;
//  FMap.OnMouseDown    :=Map1Mouse;
  FMap.OnMouseDown    :=Map1MouseDown;        

end;

procedure TMapEngine.SetOnMapCursor(const Value: TOnMapCursorEvent);
begin
  FOnMapCursor := Value;
end;

//--------------------------------------------------------------------
function TMapEngine.WorkspaceLoad(aFileName: string): boolean;
//--------------------------------------------------------------------
begin
  Result := mapx_LoadWorkspace (FMap, aFileName);
end;


procedure TMapEngine.ZoomInRect(aRect: TBLRect);
begin
  SetBounds (aRect);
end;

//--------------------------------------------------------------------
procedure TMapEngine.Map1MouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
//--------------------------------------------------------------------
var //iCoord,
  iScale: integer;
  dMapLengthInCm, dMapLengthInKm, dZoom, dMapX, dMapY: Double;

  dX, dY: Single;



  //  S, sHint: string;
begin
  if FMap.Layers.Count=0 then
    exit;


  CursorPosXY.X:= X; //fScreenX:= X;
  CursorPosXY.Y:= Y; //fScreenY:= Y;

  PaintBitmapWhileMoving();

  dX:=X;  dY:=Y;

  dMapX:=0;
  dMapY:=0;

//  Map1.ConvertCoord(fScreenX, fScreenY, dMapX, dMapY, miScreenToMap);
  try
    FMap.ConvertCoord(dX, dY, dMapX, dMapY, miScreenToMap);

    FCursorPos:= MakeBLPoint(dMapY, dMapX);
    //  DoOnCursor (FCursorPos);

    if Assigned(FOnMapCursor)  then
      FOnMapCursor(FCursorPos);

  except
  end


 // RefreshStatusBar();


end;



//--------------------------------------------------------------------
procedure TMapEngine.Map1PolyToolUsed(Sender: TObject; ToolNum: Smallint; Flags: Integer;
    const Points: IDispatch; bShift, bCtrl: WordBool; var EnableDefault: WordBool);
//--------------------------------------------------------------------
var
  ps: CMapXPoints;
  vPoint: CMapXPoint;
  i: integer;

  FBLPointArr: TBLPointArrayF;

begin
  case Flags of

    miPolyToolBegin: ;{ Someone's beginning the use of a PolyTool.. }
    miPolyToolEndEscaped: ; { The users hit 'Esc' or backspaced }

    miPolyToolEnd:  { The user finished using a PolyTool by double clicking }

      if ToolNum = CUSTOM_POLYLINE_TOOL then
      begin
        ps:=CMapXPoints(Points);

        FBLPointArr.Count:=ps.Count;

      //  iCount:=ps.Count;

        for i:=1 to FBLPointArr.Count do
        begin

            vPoint:=ps.Item[i];


          FBLPointArr.Items[i-1]:=MakeBLPoint (vPoint.Y, vPoint.X);
        end;

        if Assigned(FOnMapPoly) then
          FOnMapPoly(FBLPointArr);


      //  DoOnPoly (FBLPointArr);
      end;
  end;
end;

//-------------------------------------------------------------------
procedure TMapEngine.Map1MouseUp(Sender: TObject; Button: TMouseButton; Shift:
    TShiftState; X, Y: Integer);
//-------------------------------------------------------------------
var
  iOffsetX, iOffsetY: Integer;
begin
  inherited;


  if FMap.Layers.Count=0 then
    exit;


  if FRightButtonPressed then
  begin
    Screen.Cursor:=crDefault;

    if (FOldMousePos.X = X) and (FOldMousePos.Y = Y) then
    begin
      FRightButtonPressed:= false;
//      FLeftButtonPressed:= false;
      FMap.AutoRedraw:= true;

      if assigned(PopupMenu) then
        PopupMenu.Popup(Mouse.CursorPos.x, Mouse.CursorPos.y);

    //////  PopupMenu1.Popup(Mouse.CursorPos.x, Mouse.CursorPos.y);

      exit;
    end;

    iOffsetX:= CursorPosXY.X - FOldMousePos.X;
    iOffsetY:= CursorPosXY.Y - FOldMousePos.Y;

(*    if (iOffsetX=0) and (iOffsetY=0) then begin
      FRightButtonPressed:= false;
      exit;
    end;*)

    //��� ���������� ������ ���� �������� �����
    FMap.Pan (-iOffsetX, iOffsetY);

    FRightButtonPressed:= false;
    FMap.AutoRedraw:= true;
    FMap.Refresh;
  end;

//  FLeftButtonPressed:= False;

end;


// -------------------------------------------------------------------
procedure TMapEngine.Map1MouseDown(Sender: TObject; Button: TMouseButton; Shift:
    TShiftState; X, Y: Integer);
// -------------------------------------------------------------------
(*
    //------------------------------------------------------------
    procedure DoOnMouseDown ();
    //------------------------------------------------------------
    begin
      FFirstPointClicked:=True;
      FFirstPointPos:=FCursorPos;

      if Assigned(FOnMapMouseDown) then
        FOnMapMouseDown (FCursorPos);
    end;
*)

var
  sObjName: Integer;
  i, iID  : Integer;
  sAction: String;
begin
  if FMap.Layers.Count=0 then
    exit;
    


{if FIsTempPan then begin
  //  SetWindowTool (toolObject);
    FIsTempPan:=False;
 end;  }
  FLeftButtonClicked:= (not (ssRight in Shift)) AND (ssLeft in Shift);

  FRightButtonPressed:= ((ssRight in Shift)) AND (not (ssLeft in Shift));

{
  if (not FIsTempPan) and (ssRight in Shift) then begin
    SetWindowTool (toolPan);
    FIsTempPan:=True;
  end else

  if (FIsTempPan) and (ssRight in Shift) then
  begin
    if Assigned(FindForm( Tfrm_Map_Tool_Liner.ClassName)) then
      SetWindowTool(toolLiner)
    else


    SetWindowTool(toolObject);

    FIsTempPan:=False;
  end;}


  if FRightButtonPressed then
  begin
    //CursorSet (crHandPoint);
    FMap.AutoRedraw:= false;

    FOldMousePos.X:= X;
    FOldMousePos.Y:= Y;
//    FOldMousePos:= CursorPosXY;

    FMoveBitmap.Width:= FMap.Width;
    FMoveBitmap.Height:= FMap.Height;

    FMap.PaintTo (FMoveBitmap.Canvas.Handle, 0, 0);
  end;


  FFirstPointClicked:=True;
  FFirstPointPos:=FCursorPos;

  if Assigned(FOnMapMouseDown) then
    FOnMapMouseDown (FCursorPos);

 // DoOnMouseDown();

end;


//------------------------------------------------------------------------------
function TMapEngine.GetScaleInCm: double;
//------------------------------------------------------------------------------
begin
//  FMap.PaperUnit:= miUnitCentimeter;
//  FMap.MapUnit  := miUnitKilometer;

  Result:= Round(FMap.Zoom * 100000 / FMap.MapPaperWidth);
end;






// ---------------------------------------------------------------
function TMapEngine.Add_Map(aFileName: string): CMapXLayer;
// ---------------------------------------------------------------
var
  s: string;
  vLayer: CMapXLayer;

begin
  result:=nil;

  if MapExists(aFileName) then
    exit;


  if FMap.Layers.Count=0 then
    mapx_SetMapDefaultProjection_NumericCoordSys_KRASOVKSY42_simple(FMap);


  vLayer:=mapx_GetLayerByFileName(FMap, aFileName);

  Assert(vLayer=nil, '  vLayer:=mapx_GetLayerByFileName(aMap, aFileName);');


  Result:=mapx_MapAdd_Ex(FMap, aFileName);


  map_Tile_Align_Back(FMap);

end;



procedure TMapEngine.Clear;
begin
  FMap.Layers.RemoveAll;
//  FMap.Layers.RemoveAll;
  FMap.DataSets.RemoveAll;

  // TODO -cMM: TMapEngine.Clear default body inserted
end;


// ---------------------------------------------------------------
procedure TMapEngine.Del_List(aList: TStrings);
// ---------------------------------------------------------------
var
  I: Integer;
begin
  for I := 0 to aList.Count-1 do
    Del_Map(aList[i]);

end;

// ---------------------------------------------------------------
procedure TMapEngine.Del_Map(aFileName: string);
// ---------------------------------------------------------------
var
  iInd: Integer;
begin
  iInd:=mapx_GetLayerIndexByFileName_from_1( FMap, aFileName);
 //  v:=mapx_GetLayerByFileName(sFile);

  if iInd>0 then
     FMap.Layers.Remove(iInd);

 // MapList.Del_Map (aFileName)   ;

end;

// ---------------------------------------------------------------
procedure TMapEngine.Del_Layer_ByName(aName: string);
// ---------------------------------------------------------------
var
  iInd: Integer;
begin
  iInd:=mapx_GetLayerIndexByName_from_1( FMap, aName);
 //  v:=mapx_GetLayerByFileName(sFile);

  if iInd>0 then
     FMap.Layers.Remove(iInd);

 // MapList.Del_Map (aFileName)   ;

end;



// ---------------------------------------------------------------
function TMapEngine.MapExists(aFileName: string): Boolean;
// ---------------------------------------------------------------
var
  iInd: Integer;
begin
  iInd:=mapx_GetLayerIndexByFileName_from_1( FMap, aFileName);

  Result:=iInd>0

end;



// ---------------------------------------------------------------
procedure TMapEngine.TilesClear;
// ---------------------------------------------------------------
var
  I: Integer;
  s: string;
begin
  for i:=FMap.Layers.Count downto 1 do
  begin     
    s:=FMap.Layers.Item[i].FileSpec;

    s:= LowerCase (ExtractFileName(s));

    if LowerCase(LeftStr(s, Length('wms_tile'))) = 'wms_tile' then
    begin
      FMap.Layers.Remove(i);
      Break;
    end;
  //    Continue;

//    if Copy(s,1,4) = 'tile' then

  end;



//  for I := MapList.Count-1 downto 0 do
//    if MapList[i].IsTile then
//      Del_Map(MapList[i].FileName);

end;



procedure TMapEngine.SetNumericCoordSys_DATUM_WGS84;
begin
     // miMercator = $0000000A;
                                   
  FMap.DisplayCoordSys.Set_ (miLongLat, DATUM_WGS84,
//  FMap.NumericCoordSys.Set_ (miLongLat, DATUM_KRASOVKSY42,
      EmptyParam, EmptyParam, EmptyParam, EmptyParam,
      EmptyParam, EmptyParam, EmptyParam, EmptyParam,
      EmptyParam, EmptyParam, EmptyParam, EmptyParam);
end;



//--------------------------------------------------------------------
function TMapEngine.GetLayerByName(aName: string): CMapXLayer;
//--------------------------------------------------------------------
begin
  result :=mapx_GetLayerByName (FMap, aName);
end;



//------------------------------------------------------------------------------
procedure TMapEngine.RemoveLayer(aName: string);
//------------------------------------------------------------------------------
var iIndex: Integer;
begin
  iIndex:= GetLayerIndexByName_from_1(aName);
  if iIndex<>-1 then 
    FMap.Layers.Remove(iIndex);
end;

//--------------------------------------------------------------------
function TMapEngine.GetLayerIndexByName_from_1(aName: string): integer;
//--------------------------------------------------------------------
begin
  result :=mapx_GetLayerIndexByName_from_1 (FMap, aName);
end;


//------------------------------------------------------------------------------
procedure TMapEngine.Legend_Clear;
//------------------------------------------------------------------------------
var
  vLayer: CMapxLayer;
  i: Integer;

const
  DEF_LAYER_LEGEND = 'LEGEND';  
    
begin

  vLayer:= GetLayerByName(DEF_LAYER_LEGEND);
  if not Assigned(vLayer) then
    exit;

  if vLayer.DataSets.Count>0 then
     for i:=1 to vLayer.DataSets.Count do
        vLayer.DataSets[i].Themes.RemoveAll();


  RemoveLayer(DEF_LAYER_LEGEND);
  FMap.Refresh;
end;




//------------------------------------------------------------------------------
function TMapEngine.CreateTempLayerWithFields(aName: string; aParams: array of
    TmiFieldRec): CMapXLayer;
//------------------------------------------------------------------------------
var 
  rRec: TmiMapCreateRec;

  vLayer: CMapXLayer;
  
begin
  vLayer:= mapx_GetLayerByName(FMap, aName);


//  GetLayerIndexByName_from_1()
 // Result:= 
//  LayerByName(aName);

  if not Assigned(vLayer) then
  begin
    rRec.Fields:= Make_miFieldArray(aParams);
    Result:= mapx_CreateTempLayerWithFeatures(FMap, aName, rRec, nil, 1, true);
  end;
end;


// ---------------------------------------------------------------
procedure TMapEngine.Legend_Show_;
// ---------------------------------------------------------------
var
  i: Integer;
  sFileName: string;
  sLegendXmlFileName: string;

  vLayer: CMapxLayer;

begin

  for i:=FMap.Layers.Count downto 1 do
  begin
    sFileName:=FMap.Layers.Item[i].FileSpec;

    vLayer:=FMap.Layers.Item[i];

       //check if map is system objects
     if (not vLayer.Visible) //or (vLayer.Type_<>miLayerTypeRaster)
       then Continue;


     sLegendXmlFileName:= ChangeFileExt(sFileName,'.colors.xml');
     if FileExists(sLegendXmlFileName) then
     begin
       ShowLegend('Title', sFileName);

       Break;
     end;
       
         

{         miLayerTypeNormal = $00000000;
  miLayerTypeRaster = $00000002;
  miLayerTypeSeamless = $00000004;
  miLayerTypeUnknown = $00000005;
  miLayerTypeUserDraw = $00000006;
  miLayerTypeDrilldown = $00000007;
}

   // blRect:=mapx_XRectangleToBLRect (FMap.Layers.Item[i].Bounds);

  end;

end;

//------------------------------------------------------------------------------
function TMapEngine.ShowLegend(aTitle: string; aMapFileName: string): boolean;
//------------------------------------------------------------------------------
type
  //-------------------------------
  TLegendItemRec = record
  //-------------------------------
  //   id,
   //  SchemaID      : integer;
  //   SchemaName    : string;
     Min_value     : double;
     Max_value     : double;
     Color         : integer;
     Min_color     : integer;
     Max_color     : integer;
  //   Transparent   : boolean;
     Type_         : integer;  // 0-normal; 1-gradient
  //   Enabled       : boolean;
     Comment       : string;
   end;


  //-------------------------------
 // TdmColorRangeParamRecArr = array of TdmColorRangeParamRec;
  //-------------------------------
  
  
  TLegendRec = record
    Items : array of TLegendItemRec;
                 
  end;


          //-------------------------------------------------
          function DoLoadColorSchema(aFileName: string): TLegendRec;
          //-------------------------------------------------
          var
            j, iCount: Integer;
            oXMLDoc: TXMLDoc;
            vItem,vNode, vNode1, vNode2: IXMLNode;
            I: Integer;

            r: TLegendItemRec;
          begin
            Assert(FileExists(aFileName));

          
            oXMLDoc:=TXMLDoc.Create;
            oXMLDoc.LoadFromFile(aFileName);

//            vItem:=xml_FindNode(oXMLDoc.DocumentElement, 'item');

            if oXMLDoc.DocumentElement.ChildNodes.Count>0 then
//            if Assigned(vItem) then
            begin
              vItem:=oXMLDoc.DocumentElement.ChildNodes[i];

//              aLegendCaption

              SetLength(Result.Items, vItem.ChildNodes.Count);

              for I := 0 to vItem.ChildNodes.Count-1 do
              begin
                vNode:= vItem.ChildNodes[i];

                r.Color       := xml_GetIntAttr(vNode, ATT_COLOR);
                r.Min_value   := xml_GetIntAttr(vNode, ATT_MIN_VALUE);
                r.Max_value   := xml_GetIntAttr(vNode, ATT_MAX_VALUE);
                r.Min_color   := xml_GetIntAttr(vNode, ATT_MIN_COLOR);
                r.Max_color   := xml_GetIntAttr(vNode, ATT_MAX_COLOR);
                r.Type_       := xml_GetIntAttr(vNode, ATT_TYPE);
              //  Enabled     := AsBoolean(xml_GetAttr (vNode2, ATT_ENABLED));
             //   Transparent := AsBoolean(xml_GetAttr (vNode2, ATT_IS_TRANSPARENT));
                r.Comment     := xml_GetStrAttr (vNode, ATT_COMMENT);
                       
                Result.Items[i]:=r;  
                
              end;
              
            end;              


            FreeAndNil(oXMLDoc);
          end;   



          //-------------------------------------------------
          procedure DoAssignThemeProperties(aRangeCategories: CMapXRangeCategoriesCollection;
                    aLegend: TLegendRec);
          //-------------------------------------------------
          var i: Integer;
          begin
            for i:=0 to High(aLegend.Items) do
            begin
              aRangeCategories[i+1].Style.RegionBorderWidth:= 3;

              with aRangeCategories[i+1].Style do
                if aLegend.Items[i].Type_=1 then
                begin
                  RegionBorderColor := IIF(aLegend.Items[i].Min_Color<0, 0, aLegend.Items[i].Min_Color);
                  RegionColor       := IIF(aLegend.Items[i].Max_Color<0, 0, aLegend.Items[i].Max_Color);
                end else begin
                  RegionColor       := IIF(aLegend.Items[i].Color<0, 0, aLegend.Items[i].Color);
                  RegionBorderColor := IIF(aLegend.Items[i].Color<0, 0, aLegend.Items[i].Color);
                end;

              aRangeCategories[i+1].Min:= aLegend.Items[i].Min_value;
              aRangeCategories[i+1].Max:= aLegend.Items[i].Max_value;
            end;
          end;


//          //-------------------------------------------------
//          function DoExistStr(const aStr: string; aSubStrings: array of string): boolean;
//          //-------------------------------------------------
//          var i: integer;
//          begin
//            Result:=false;
//            for i:=0 to High(aSubStrings) do
//              if pos(aSubStrings[i], aStr) > 0 then
//              begin
//                Result:=true;
//                exit;
//              end;
//          end;


var
  i: Integer;
  s, sFileName: string;
  ds: CMapxDataset;
  th: CMapxTheme;

  vLayer: CMapxLayer;
  vFeature: CMapXFeature;
  vPoints: CMapXPoints;

  rLegendRec: TLegendRec;
  sLegendXmlFileName: string;

const
  DEF_LAYER_LEGEND = 'LEGEND';
  
begin
  Result:=false;

  Legend_Clear();

//  if MapFileIsPart(aMapFileName) then
 //    aMapFileName:= PartFileInitFileName(aMapFileName);

  sLegendXmlFileName:= ChangeFileExt(aMapFileName,'.colors.xml');
//  aMapFileName + '.xml';

  if not FileExists(sLegendXmlFileName) then
  begin
    ShowMessage('��� ������� ��� �����: '+ sLegendXmlFileName);
    exit;
  end;

  rLegendRec:= DoLoadColorSchema (sLegendXmlFileName);
  vLayer:= CreateTempLayerWithFields (DEF_LAYER_LEGEND, [mapx_Field(FLD_ID, miTypeInteger) ]);

  //---------------------------------------------------
  // ??????? ???? ???????????? ??????, ?????
  // ???? ?? ??????? ????????? ???????????????? (?? ????????? ?????????)
  //---------------------------------------------------

  vFeature:= FMap.FeatureFactory.CreateRegion(EmptyParam, EmptyParam);
  vFeature.Style:= FMap.DefaultStyle;

  vPoints:= CoPoints.Create;
  vPoints.AddXY (0, 0, EmptyParam);
  vPoints.AddXY (1, 0, EmptyParam);
  vPoints.AddXY (1, 1, EmptyParam);
  vPoints.AddXY (0, 1, EmptyParam);

  vFeature.Parts.Add (vPoints);
  vLayer.AddFeature  (vFeature, EmptyParam);

  //---------------------------------------------------

  ds:= vLayer.DataSets[1];
  th:= ds.Themes.Add(mithemeRanged, EmptyParam, EmptyParam, false);


  th.AutoRecompute:= false;
  th.ComputeTheme:= false;

  th.ThemeProperties.DistMethod := miCustomRanges;

  Assert(Length(rLegendRec.Items)>0, 'Length(rLegendRec.Items)=0');

  th.ThemeProperties.NumRanges  := Length(rLegendRec.Items);

  DoAssignThemeProperties (th.ThemeProperties.RangeCategories, rLegendRec);

  //---------------------------------------------------

  th.Legend.LegendTexts.AutoGenerate:= true;
  th.Legend.Compact         := false;
  th.Legend.Title           := '�������';
  th.Legend.Subtitle        := ' ';//'����� LOS';
  th.Legend.ShowEmptyRanges := true;
  th.Legend.ShowCount       := false;
  th.Legend.Top             := 10;
  th.Legend.Left            := 10;

  for i:=0 to High(rLegendRec.Items) do
    with rLegendRec.Items[i] do
    begin
      if Min_value <> Max_value then
      begin
        if Comment='' then s:= Format('%g .. %g',   [Min_value, Max_value])
                      else s:= Format('%g .. %g%s', [Min_value, Max_value, '  ('+Comment+')' ]);
      end else
        if Comment='' then s:= Format('%g',   [Min_Value])
                      else s:= Format('%g%s', [Min_Value, '  ('+Comment+')' ]);

      th.Legend.LegendTexts[i+1].Text:= s;
    end;

  th.Legend.visible:=true;
  FMap.Refresh;

  //---------------------------------------------------

 // SetLength(arrColorSchema,0);
  Result:=true;
end;






end.


{


{

//------------------------------------------------------------------------------
procedure TMapEngine.SetDefaultDisplayCoordSys;
//------------------------------------------------------------------------------
const
  ZONE_INFO_ARR : array[4..29] of record
    origin_lon: Integer;
    origin_lat: Integer;
    scale_factor: Integer;
    false_easting: Integer;
    false_norsing: Integer;
  end =
  ( (origin_lon: 21; origin_lat:0; scale_factor:1; false_easting: 4500000; false_norsing:0)  //5
  , (origin_lon: 27; origin_lat:0; scale_factor:1; false_easting: 5500000; false_norsing:0)  //5
  , (origin_lon: 33; origin_lat:0; scale_factor:1; false_easting: 6500000; false_norsing:0)  //6
  , (origin_lon: 39; origin_lat:0; scale_factor:1; false_easting: 7500000; false_norsing:0)  //7
  , (origin_lon: 45; origin_lat:0; scale_factor:1; false_easting: 8500000; false_norsing:0)  //8
  , (origin_lon: 51; origin_lat:0; scale_factor:1; false_easting: 9500000; false_norsing:0)  //9
  , (origin_lon: 57; origin_lat:0; scale_factor:1; false_easting:10500000; false_norsing:0)  //10
  , (origin_lon: 63; origin_lat:0; scale_factor:1; false_easting:11500000; false_norsing:0)  //11
  , (origin_lon: 69; origin_lat:0; scale_factor:1; false_easting:12500000; false_norsing:0)  //12
  , (origin_lon: 75; origin_lat:0; scale_factor:1; false_easting:13500000; false_norsing:0)  //13
  , (origin_lon: 81; origin_lat:0; scale_factor:1; false_easting:14500000; false_norsing:0)  //14
  , (origin_lon: 87; origin_lat:0; scale_factor:1; false_easting:15500000; false_norsing:0)  //15
  , (origin_lon: 93; origin_lat:0; scale_factor:1; false_easting:16500000; false_norsing:0)  //16
  , (origin_lon: 99; origin_lat:0; scale_factor:1; false_easting:17500000; false_norsing:0)  //17
  , (origin_lon:105; origin_lat:0; scale_factor:1; false_easting:18500000; false_norsing:0)  //18
  , (origin_lon:111; origin_lat:0; scale_factor:1; false_easting:19500000; false_norsing:0)  //19
  , (origin_lon:117; origin_lat:0; scale_factor:1; false_easting:20500000; false_norsing:0)  //19
  , (origin_lon:123; origin_lat:0; scale_factor:1; false_easting:21500000; false_norsing:0)  //19
  , (origin_lon:129; origin_lat:0; scale_factor:1; false_easting:22500000; false_norsing:0)  //19
  , (origin_lon:135; origin_lat:0; scale_factor:1; false_easting:23500000; false_norsing:0)  //19
  , (origin_lon:141; origin_lat:0; scale_factor:1; false_easting:24500000; false_norsing:0)  //19
  , (origin_lon:147; origin_lat:0; scale_factor:1; false_easting:25500000; false_norsing:0)  //19
  , (origin_lon:153; origin_lat:0; scale_factor:1; false_easting:26500000; false_norsing:0)  //19
  , (origin_lon:159; origin_lat:0; scale_factor:1; false_easting:27500000; false_norsing:0)  //19
  , (origin_lon:165; origin_lat:0; scale_factor:1; false_easting:28500000; false_norsing:0)  //19
  , (origin_lon:171; origin_lat:0; scale_factor:1; false_easting:29500000; false_norsing:0)  //19
  )    ;
var
  i, iZone: Integer;
  bSet: boolean;
  dScaleCm: double;
begin
  bSet:= true;
  for i := 1 to FMap.Layers.Count do

{

//    if FMap.Layers.Item[i].type_ = miLayerTypeRaster then  bSet:= false;


  iZone:= geo_Get6ZoneBL(MakeBLPoint(FMap.CenterY, FMap.CenterX));
  dScaleCm:= GetScaleInCm;

  if not bSet then
    exit;

  try
    if (iZone < Low(ZONE_INFO_ARR)) or
       (iZone > High(ZONE_INFO_ARR)) or
       (dScaleCm > 5000000) then
    begin
      if (FMap.DisplayCoordSys.type_<>miLongLat) or
         (FMap.DisplayCoordSys.Datum.Ellipsoid<>ELLIPSOID_WGS)
      then
        FMap.DisplayCoordSys.Set_ (miLongLat, DATUM_WGS84,
             EmptyParam, EmptyParam, EmptyParam, EmptyParam,
             EmptyParam, EmptyParam, EmptyParam, EmptyParam,
             EmptyParam, EmptyParam, EmptyParam, EmptyParam);
    end else begin

      if (FMap.DisplayCoordSys.type_<>miTransverseMercator) or
         (FMap.DisplayCoordSys.Datum.Ellipsoid<>ELLIPSOID_KRAS) or
         (FMap.DisplayCoordSys.OriginLongitude<>ZONE_INFO_ARR[iZone].origin_lon) or
         (FMap.DisplayCoordSys.FalseEasting<>ZONE_INFO_ARR[iZone].false_easting)
      then
        with ZONE_INFO_ARR[iZone] do
          FMap.DisplayCoordSys.Set_ (miTransverseMercator, DATUM_KRASOVKSY42,
             miUnitMeter, origin_lon, origin_lat, EmptyParam,
             EmptyParam, EmptyParam, scale_factor, false_easting,
             false_norsing, EmptyParam, EmptyParam, EmptyParam);
    end;

  except
  end;

end;




// ---------------------------------------------------------------
function TMapEngine.GetTileBounds(var aRec: TTileBounds): Boolean;
// ---------------------------------------------------------------
  
    // ---------------------------------------------------------------
    procedure DoGetTile_XY_blocks(aBLPoint: TBLPoint; aZ: integer; var aX,aY:
        integer);
    // ---------------------------------------------------------------
    var
      iMax: Integer;
      x, y, s: Double;
    begin
      x:=0.5 + aBLPoint.L/360.0;
      s:=sin(DegToRad(aBLPoint.B));
    
      y:=0.5-(1/(4*Pi))*ln((1+s)/(1-s));
    
      iMax:=  Trunc(Math.Power(2, aZ));

      aX:=Floor(iMax*x);
      aY:=Floor(iMax*y);    
    
    end;

  


const
  MAX_ZOOM = 559082264;
//       140 733 037
var
  eLat_step: Double;
  eLon_step: Double;
  eScale: double;
  iMax: Integer;
  iZ: Integer;

  blRect: TBLRect;
  e: Double;
 
  eMax_scale: Double;
  iX: Integer;
  iX1: Integer;
  iX2: Integer;
  iX_min: Integer;
  iY1: Integer;
  iY2: Integer;

begin
  FillChar(Result,SizeOf(Result),0);

  blRect:=GetBounds();

  
  eScale:=GetScale();

  for iZ := 0 to 28 do     
  begin
//    e:=+Math.Power(2,iZ+2);
  
//    eMax_scale:=MAX_ZOOM / Math.Power(2,iZ);
    eMax_scale:=MAX_ZOOM / Math.Power(2,iZ+1+1);
  
    if eScale > eMax_scale then
    begin
      aRec.Z:=iZ;
      
      aRec.Scale:= Trunc(eMax_scale);

      iMax:= Trunc(Math.Power(2,iZ));

      eLat_step:=(2*85)/iMax;
      eLon_step:=(2*180)/iMax; 


      DoGetTile_XY_blocks(blRect.TopLeft,     iZ, iX1,iY1);
      DoGetTile_XY_blocks(blRect.BottomRight, iZ, iX2,iY2);

      aRec.X_min:=Min(iX1,iX2);
      aRec.X_max:=Max(iX1,iX2);      

      aRec.Y_min:=Min(iY1,iY2);
      aRec.Y_max:=Max(iY1,iY2);      
      
//        
//      aRec.Y_min:= Trunc ((85 - blRect.TopLeft.B) / eLat_step);
//      aRec.Y_max:= Trunc ((85 - blRect.BottomRight.B) / eLat_step);
//
//  
//      aRec.X_min:= Trunc ((blRect.TopLeft.L     + 180) / eLon_step);
//      aRec.X_max:= Trunc ((blRect.BottomRight.L + 180) / eLon_step);
//
//      if aRec.Y_min<0 then aRec.Y_min:=0;

      
//      if Result._max<0 then Result.Y_min:=0;

      Result:=true;
      
      Exit;
    end;
    
  end;

  Result:=False;

end;





// ---------------------------------------------------------------
function TMapEngine.Tile_Get_outside_Rect_old(aRect: TTileBounds; aList:
    TStrings): Integer;
// ---------------------------------------------------------------
var
  I: Integer;
  s: string;
  sFileName: string;
  X, Y, Z: integer;
  
begin
  aList.Clear;
     

  for i:=1 to FMap.Layers.Count do
  begin     
    sFileName:=FMap.Layers.Item[i].FileSpec;    

    s:= LowerCase (ExtractFileName(sFileName));

    if LeftStr(s,4) = 'tile' then
    begin
      ExtractTileXYZ(sFileName, X, Y, Z);
          
      if
        (Z <> aRect.Z ) or

        (X < aRect.X_min ) or
        (X > aRect.X_max ) or
        
        (Y < aRect.Y_min ) or
        (Y > aRect.Y_max ) 
      then
       aList.Add(sFileName);      
      
    end;
//      FMap.Layers.Remove(i);
                 
  end;

//  
//
//  for I := 0 to Count-1 do
//    if Items[i].IsTile then
//      if 
//        (Items[i].Tile.Z <> aRect.Z ) or
//
//        (Items[i].Tile.X < aRect.X_min ) or
//        (Items[i].Tile.X > aRect.X_max ) or
//        
//        (Items[i].Tile.Y < aRect.Y_min ) or
//        (Items[i].Tile.Y > aRect.Y_max ) 
//      then
//       aList.Add(Items[i].FileName);

  Result:=aList.Count;
       
end;






// ---------------------------------------------------------------
procedure TMapEngine.ExtractTileXYZ(aFileName: string; var aX, aY, aZ: integer);
// ---------------------------------------------------------------
var
  i: Integer;
  i1: Integer;
  i2: Integer;
  s: string;
  sName: string;
begin
  sName:= LowerCase (ExtractFileName(aFileName));

  
 // if LeftStr() then
  i1:=pos('z', sName);
  i2:=PosEx('_', sName, i1);
  s:=Copy(sName, i1+1, i2-i1-1);
  aZ:=StrToInt(s);
  
  i1:=pos('x', sName);
  i2:=PosEx('_', sName, i1);
  s:=Copy(sName, i1+1, i2-i1-1);
  aX:=StrToInt(s);

  i1:=pos('y', sName);
  i2:=PosEx('.', sName, i1);
  s:=Copy(sName, i1+1, i2-i1-1);
  aY:=StrToInt(s);

end;


// ---------------------------------------------------------------
procedure TMapEngine.Tile_Del_outside_Rect(aRect: TTileBounds);
// ---------------------------------------------------------------
var
  I: Integer;
  s: string;
  sFileName: string;
  X, Y, Z: integer;

  blRect: TBLRect;

begin
 // aList.Clear;
  blRect:=mapx_XRectangleToBLRect (FMap.Bounds);


  for i:=FMap.Layers.Count downto 1 do
  begin
    sFileName:=FMap.Layers.Item[i].FileSpec;    

    blRect:=mapx_XRectangleToBLRect (FMap.Layers.Item[i].Bounds);


    s:= LowerCase (ExtractFileName(sFileName));
    
    if LeftStr(s,4) = 'tile' then
    begin
      ExtractTileXYZ(sFileName, X, Y, Z);
          
      if 
        (Z <> aRect.Z ) or

        (X < aRect.block_X_min ) or
        (X > aRect.block_X_max ) or

        (Y < aRect.block_Y_min ) or
        (Y > aRect.block_Y_max )
      then begin
  //       if iInd>0 then
        FMap.Layers.Remove(i);

     //  aList.Add(sFileName);
      end;
    end;
//      FMap.Layers.Remove(i);

  end;


  //Result:=aList.Count;

end;





// ---------------------------------------------------------------
function TMapEngine.Add_Tile___________(aFileName: string): CMapXLayer;
// ---------------------------------------------------------------
begin
  result:=Add_Map (aFileName);

end;


