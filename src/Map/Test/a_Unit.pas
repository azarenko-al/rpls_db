unit a_Unit;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, rxPlacemnt, ComCtrls, ToolWin,

  u_func,


  dm_Act_Explorer,

  I_Options,

 // dm_RelMatrixFile,

//  d_Map_Show_Characteristic,


  f_LinkFreqPlan_CIA,

  dm_Main,
  dm_Main,

  d_Map_Desktops,

  fr_MapView_MIF,
  f_Map,


//  dm_Act_CalcRegion,
//  dm_Act_Site,
  dm_Act_Antenna,
  dm_Act_BSC,
  dm_Act_PMP_CalcRegion,
  dm_Act_Filter,
  dm_Act_Link,
  dm_Act_LinkEnd,
  dm_Act_LinkEndType,
  dm_Act_Map,
  dm_Act_Map_Engine,
  dm_Act_MSC,
  dm_Act_Pmp_Sector,
  dm_Act_Pmp_Site,
  dm_Act_MapDesktop,
  dm_Act_PmpTerminal,
  dm_Act_Profile,
  dm_Act_Project,
  dm_Act_Property,

  f_Log,

  //u_func_reg_lib,
  u_reg,
  u_dlg,

  u_const,
  u_const_msg,
  u_Geo,
  u_func_msg,
  u_db,
  u_types
  ;


type
  Tfrm_test_Map = class(TForm)
    FormPlacement1: TFormPlacement;
    pc_Main: TPageControl;
    ts_Region: TTabSheet;
    ToolBar1: TToolBar;
    Button1: TButton;
    Button2: TButton;
    btn_MapReb: TButton;
    Button3: TButton;
    Button4: TButton;
    procedure FormDestroy(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure btn_MapRebClick(Sender: TObject);
    procedure Button3Click(Sender: TObject);
  private
    { Private declarations }
  public
  //  Fframe_Region : Tframe_Explorer;

  //  Fframe_Map : Tframe_Explorer;
    { Public declarations }
  end;

var
  frm_test_Map: Tfrm_test_Map;

implementation

uses dm_Object_Manager;

//uses dm_Act_Map_Engine;
{$R *.DFM}


procedure Tfrm_test_Map.FormDestroy(Sender: TObject);
var
  i: integer;
begin
  i:=0;
end;

procedure Tfrm_test_Map.FormCreate(Sender: TObject);
begin
 // ShowComponents();

// Init_EventManager1;

//  gl_Reg :=TRegStore_Create (REGISTRY_ROOT);

 // Init_EventManager1;

  TdmMain.Init;
  if not dmMain.OpenDlg then  Exit;


 ////// dmAct_Map_Engine.DebugMode := False;


  TdmAct_BSC.Init;
  TdmAct_MSC.Init;
  TdmAct_Link.Init;

  TdmAct_Project.Init;


  TdmAct_MapDesktop.Init;
  
  TdmAct_Antenna.Init;

  TdmAct_Profile.Init;
  TdmAct_LinkEndType.Init;
  TdmAct_LinkEnd.Init;

  TdmAct_Filter.Init;
  TdmAct_Profile.Init;
//  TdmAct_CalcRegion.Init;
  TdmAct_Pmp_CalcRegion.Init;
  TdmAct_Property.Init;

  TdmAct_PmpTerminal.Init;
  TdmAct_Pmp_Sector.Init;
  TdmAct_Pmp_Site.Init;


  TdmAct_Map.Init;

  TdmAct_Explorer.Init;
  

  IOptions_Init;


{



}

 ///////////////// dmAct_Map_Engine.Enabled:=False;

 // Tfrm_Log.CreateForm;

  {
  Fframe_Region := Tframe_Explorer.CreateChildForm( ts_Region);
  Fframe_Region.SetViewObjects([otProperty, otMapFile, otCalcRegion]);
  Fframe_Region.Load;
   }


  dmAct_Project.LoadLastProject;

//////////  Tfrm_Map.CreateSingleForm;     dgfdfg

//  Tdlg_Map_Show_Characteristic.CreateSingleForm;


   Tfrm_Map.CreateNewForm;


end;


procedure Tfrm_test_Map.Button1Click(Sender: TObject);
begin
//  Tdlg_Calc.ExecDlg;
end;

procedure Tfrm_test_Map.Button2Click(Sender: TObject);
//var s1, s2: string;
begin
//  dmRelMatrixFile.SaveBoundFramesToMIF('e:\bounds.Tab');

//  Tdlg_Map_Desktops.ExecDlg ({1,} s1, s2);
end;


procedure Tfrm_test_Map.btn_MapRebClick(Sender: TObject);
begin
   Tfrm_LinkFreqPlan_CIA.ShowWindow(248);

//  dmAct_Project.act_Service_Map_Rebuild.Execute;;
end;


procedure Tfrm_test_Map.Button3Click(Sender: TObject);
begin
   Tfrm_Map.CreateNewForm;
end;

end.