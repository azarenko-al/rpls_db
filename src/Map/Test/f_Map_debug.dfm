object frm_Map_debug: Tfrm_Map_debug
  Left = 975
  Top = 532
  Width = 395
  Height = 396
  Caption = 'frm_Map_debug'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object GSPages_Data: TGSPages
    Left = 0
    Top = 0
    Width = 387
    Height = 361
    Align = alTop
    ActivePage = GSPage2
    Constraints.MinHeight = 150
    Visible = False
    object GSPage3: TGSPage
      Left = 0
      Top = 23
      Width = 387
      Height = 338
      HorzScrollBar.Smooth = True
      HorzScrollBar.Tracking = True
      VertScrollBar.Smooth = True
      VertScrollBar.Tracking = True
      Align = alClient
      TabOrder = 0
      Visible = False
      Caption = 'Selection'
      object DBGrid_Selection: TDBGrid
        Left = 0
        Top = 0
        Width = 387
        Height = 209
        Align = alTop
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'filename'
            Width = 156
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'id'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'objname'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'name'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'guid'
            Visible = True
          end>
      end
      object dxDBInspector1: TdxDBInspector
        Left = 0
        Top = 209
        Width = 387
        Height = 129
        Align = alClient
        TabOrder = 1
        DividerPos = 191
      end
    end
    object GSPage2: TGSPage
      Left = 0
      Top = 23
      Width = 387
      Height = 338
      HorzScrollBar.Smooth = True
      HorzScrollBar.Tracking = True
      VertScrollBar.Smooth = True
      VertScrollBar.Tracking = True
      Align = alClient
      TabOrder = 1
      Caption = 'Maps'
      object DBGrid_Maps: TDBGrid
        Left = 0
        Top = 0
        Width = 387
        Height = 201
        Align = alTop
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
      end
      object dxDBInspector2: TdxDBInspector
        Left = 0
        Top = 201
        Width = 387
        Height = 137
        Align = alClient
        TabOrder = 1
        DividerPos = 202
      end
    end
  end
end
