program test_Map;



{%File '..\..\ver.inc'}

uses
  a_Unit1 in 'a_Unit1.pas' {frm_test_Map},
  d_GetRegionType in '..\..\CalcRegion\d_GetRegionType.pas' {dlg_GetRegionType},
  d_Map_Desktops in '..\..\MapDesktop\d_Map_Desktops.pas' {dlg_Map_Desktops},
  d_Map_Links in '..\d_Map_Links.pas' {dlg_Map_Links},
  d_Map_Selected in '..\d_Map_Selected.pas' {dlg_Map_Selected},
  d_Map_Show_Attributes in '..\d_Map_Show_Attributes.pas' {dlg_Map_Show_Attributes},
  dm_act_Filter in '..\..\Filter\dm_act_Filter.pas' {dmAct_Filter: TDataModule},
  dm_act_Map in '..\dm_act_Map.pas' {dmAct_Map: TDataModule},
  dm_Act_MapDesktop in '..\..\MapDesktop\dm_Act_MapDesktop.pas' {dmAct_MapDesktop: TDataModule},
  dm_act_Profile in '..\..\Profile\dm_act_Profile.pas' {dmAct_Profile: TDataModule},
  dm_Main in '..\..\DataModules\dm_Main.pas' {dmMain: TDataModule},
  dm_Map_Attributes in '..\dm_Map_Attributes.pas' {dmMap_Attributes: TDataModule},
  dm_Map_Desktop in '..\..\MapDesktop\dm_Map_Desktop.pas' {dmMap_Desktop: TDataModule},
  dm_Map_Tools in '..\dm_Map_Tools.pas' {dmMap_Tools1: TDataModule},
  f_Custom in '..\..\.common\f_Custom.pas' {frm_Custom},
  f_Map in '..\f_Map.pas' {frm_Map},
  f_Map_Tool_base in '..\f_Map_Tool_base.pas' {frm_Map_Tool_base},
  f_Map_tool_Info in '..\f_Map_tool_Info.pas' {frm_Map_tool_Info},
  f_Map_tool_Liner in '..\f_Map_tool_Liner.pas' {frm_Map_tool_Liner},
  f_tool_Map_Matrix_monitor in '..\f_tool_Map_Matrix_monitor.pas' {frm_Map_Matrix_Monitor},
  Forms,
  fr_Map in '..\fr_Map.pas' {frame_Map},
  fr_MapView_base in '..\fr_MapView_base.pas' {frame_MapView_base},
  fr_MapView_MIF in '..\fr_MapView_MIF.pas' {frame_MapView_MIF},
  I_Antenna in '..\..\Antenna\I_Antenna.pas',
  I_CalcRegion_Pmp in '..\..\CalcRegion\I_CalcRegion_Pmp.pas',
  I_Cell in '..\..\Cell\I_Cell.pas',
  I_Filter in '..\..\Filter\I_Filter.pas',
  I_Link in '..\..\Link\I_Link.pas',
  I_LinkEnd in '..\..\LinkEnd\I_LinkEnd.pas',
  I_MapAct in '..\I_MapAct.pas',
  I_MapView in '..\I_MapView.pas',
  I_Property in '..\..\Property\I_Property.pas',
  I_Shell in '..\..\Explorer\I_Shell.pas',
  I_Site in '..\..\Site\I_Site.pas',
  ShareMem,
  u_const_msg in '..\..\u_const_msg.pas',
  u_Displayed_Attrib in '..\..\MapEngine\u_Displayed_Attrib.pas',
  u_explorer in '..\..\Explorer\u_Explorer.pas',
  u_MapEngine_func in '..\..\MapEngine\u_MapEngine_func.pas',
  u_MapViewClasses in '..\u_MapViewClasses.pas',
  fr_MapsOrder in '..\..\MapDesktop\fr_MapsOrder.pas' {frame_MapsOrder};

{$R *.res}
 

begin
  Application.Initialize;
  Application.CreateForm(Tfrm_test_Map, frm_test_Map);
  Application.Run;
end.

