unit I_MapAct;

interface

uses
  SysUtils, Classes,

  u_geo,

  I_MapView;


type

  IAct_Map_X = interface(IInterface)
  ['{EFF3060A-CD54-45F8-A88B-45FB834F3E74}']

//    procedure CreateNewForm;

    procedure ShowForm();



(*
    procedure MapRefresh;
    procedure MAP_SAVE_AND_CLOSE_WORKSPACE;
    procedure MAP_RESTORE_WORKSPACE;
    procedure RemoveCalcRegionMaps(aCalcRegionID: Integer);
*)

  end;

  TMapInterfaceList = class(TInterfaceList)
  public
    procedure ShowForm;

    procedure MAP_SHOW_BLRECT(aBLRect: TBLRect);
    procedure MAP_SHOW_VECTOR(aBLVector: TBLVector);
    procedure MAP_SHOW_POINT(aBLPoint: TBLPoint);

    procedure SelectObjectByID(aObjName: string; aBLPoint: TBLPoint; aID: Integer);


  end;



var
  g_act_Map: TMapInterfaceList;

var
  IAct_Map: IAct_Map_X;

//
//*)

var
  IActiveMapView: IMapViewX;


implementation

procedure TMapInterfaceList.ShowForm;
begin
  // TODO -cMM: ShowForm default body inserted
end;

procedure TMapInterfaceList.SelectObjectByID(aObjName: string; aBLPoint: TBLPoint; aID: Integer);
begin
  IActiveMapView.SelectObjectByID(aObjName, aBLPoint, aID);
end;

procedure TMapInterfaceList.MAP_SHOW_BLRECT(aBLRect: TBLRect);
begin
  IActiveMapView.MAP_SHOW_BLRECT(aBLRect);
end;

procedure TMapInterfaceList.MAP_SHOW_VECTOR(aBLVector: TBLVector);
begin
  IActiveMapView.MAP_SHOW_VECTOR(aBLVector);
end;

procedure TMapInterfaceList.MAP_SHOW_POINT(aBLPoint: TBLPoint);
begin
  IActiveMapView.MAP_SHOW_POINT(aBLPoint);
end;
 


initialization
  g_act_Map:=TMapInterfaceList.Create;

finalization
  Assert(g_act_Map.Count=0, 'Value <=0');

  FreeAndNil(g_act_Map);

end.

