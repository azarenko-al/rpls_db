unit fr_MapView_base;

interface
{$I Mapx.inc}
{$I ver.inc}


//{$define WMS}


uses
  u_debug,

  u_func_msg,
  u_func,

  Options_TLB,

  Graphics,  StrUtils, Windows, ExtCtrls, ComCtrls, SysUtils, Classes, ADODB, ToolWin,
  Controls, Forms, StdCtrls, Dialogs, Menus, Variants,  DB, OleCtrls,

  i_Map_engine,

  u_mapx,

  I_rel_Matrix1,

  dm_Rel_Engine,                                                                                                    

  u_vars,

//  u_dll_geo_convert,

//  i_WMS,
 // u_dll_WMS7,


//  u_Tile_Manager,

  u_dll_geo_convert,

  MapXLib_TLB,

//  i_WMS_reg,
  WMS_tlb,

  u_MapX_lib,
  x_Tile_Manager,

  dm_Main,

  dm_MapEngine_store,

//  dm_MapView_tools,

//  dm_Main,

  I_Options_,

  I_MapView,

  u_types,

 // u_MapView_Classes_new,


 // u_MapX,

  u_Mapinfo_WOR,
  u_Mapinfo_WOR_classes,

  u_MapX_func,
  u_Mapx_Style,

  u_Map_engine,

//  dm_Onega_DB_data,

  u_db,

  u_Map_Classes,

  u_MapDesktop_classes,

  d_Map_MIF_Layers,
  f_Map_tool_Info,

  f_Map_tool_Liner,

  // common units
  u_Classes,

//  u_func,

  u_Geo,
 // u_Geo_WMS,

   u_const_msg,


//  u_geo_convert,
  u_geo_convert_new,

  u_log,

  u_const_db,

  u_MapViewClasses,
  u_MapView_engine,


  cxInplaceContainer, cxTL, cxControls, Mask, rxToolEdit, rxPlacemnt,
  dxStatusBar, ActnList, AppEvnts, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, dxSkinsCore, dxSkinsDefaultPainters;

type
//  TTileManagerEvents = class;

//  TOnMapObjectEvent         = procedure (aObjectName: string; aID: integer) of object;

  TOnMapCursorEvent         = procedure (aPoint: TBLPoint) of object;
  TOnMapViewVectorEvent     = procedure (aVector: TBLVector) of object;
  TOnMapLogEvent            = procedure (aMsg: string) of object;
  TOnMapViewRectEvent       = procedure (aRect: TBLRect) of object;
  TOnMapViewRingEvent       = procedure (aCenter: TBLPoint; aRadius: double) of object;
  TOnMapViewPolyEvent       = procedure (var aBLPoints: TBLPointArrayF) of object;
//  TOnMapViewPolyEvent       = procedure (aBLPoints: TBLPointList) of object;
  TOnMapViewPointEvent      = procedure (aPoint: TBLPoint) of object;
  TOnMapViewMouseDownEvent  = procedure (aPoint: TBLPoint) of object;

  TOnMapViewDistanceEvent   = procedure (aDistance,aAzimuth: double) of object;

  TMapWindowTool = (
        toolObject,
        toolPoint,
        toolVector,
        toolLiner,
        toolInfo,
        toolNeighbor,
        toolCoChannelFreqs,
        toolFixedPoint,

        toolRect,
        toolPort,
        toolRing,
        toolPoly,

        toolZoomIn,
        toolZoomOut,

        toolPan,
        toolSelect,


        toolSelectRect,
        toolSelectRadius,
        toolSelectPolygon,

        toolPolygon,
        toolLocator,
        toolCursor
        );


  Tframe_MapView_base = class(TForm, IMapViewX)
    Panel1: TPanel;
    OpenDialog1: TOpenDialog;
    PopupMenu1: TPopupMenu;
    Map1: TMap;
    Panel2: TPanel;
    FormStorage1: TFormStorage;
    PopupMenu_StatusBar: TPopupMenu;
    btn_CK42: TMenuItem;
    btn_CK95: TMenuItem;
    btn_WGS: TMenuItem;
    btn_GCK: TMenuItem;
    dxStatusBar1: TdxStatusBar;
    PopupMenu_map: TPopupMenu;
    ActionList1: TActionList;
    act_Legend_Show: TAction;
    actLegend1: TMenuItem;
    act_Legend_Hide: TAction;
    actLegendHide1: TMenuItem;
    Timer_WMS: TTimer;
    Button4: TButton;
    Button1: TButton;
    Button2: TButton;
    N1: TMenuItem;
    Button3: TButton;
    Button5: TButton;
    procedure act_Legend_ShowExecute(Sender: TObject);
//    procedure ApplicationEvents1Message(var Msg: tagMSG; var Handled: Boolean);
    procedure bCrossClick(Sender: TObject);
    procedure bSearchClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
//    procedure Button2Click(Sender: TObject);
  //  procedure Button3Click(Sender: TObject);
    procedure cxTreeList2MouseWheel(Sender: TObject; Shift: TShiftState;      WheelDelta: Integer; MousePos: TPoint; var Handled: Boolean);
 //   procedure Button9Click(Sender: TObject);
//    procedure TestSaveClick(Sender: TObject);
//    procedure TestLoadClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure Map1UserLayerDraw(Sender: TObject; const Layer: IDispatch; hOutputDC,
        hAttributeDC: Cardinal; const RectFull, RectInvalid: IDispatch);
//    procedure Map1MouseWheel(Sender: TObject; Flags: Integer; zDelta: Smallint; var
 //       X, Y: Single; var EnableDefault: WordBool);
    procedure Map1PolyToolUsed(Sender: TObject; ToolNum: Smallint; Flags: Integer;
        const Points: IDispatch; bShift, bCtrl: WordBool; var EnableDefault:
        WordBool);

    procedure Map1Click(Sender: TObject);


    procedure Map1DblClick(Sender: TObject);

    procedure Map1MapViewChanged(Sender: TObject);

    procedure btn_CK42Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
//    procedure Button5Click(Sender: TObject);
//    procedure Button4_themeClick(Sender: TObject);
//    procedure Map1MouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
//    procedure Timer_WMSTimer(Sender: TObject);


  protected
//    FUpdated: boolean;

    FMap: TMap;



  public
    FMapEngine: TMapEngine;


  private
    FBounds_old : string;

    FDisableMapViewChanged : Boolean;

  public
//    FTileManager: ITileManagerX;
    FTileManager: TTileManagerX;

//    FTileManagerX: ITileManagerX;
//    FTileManagerEvents: TTileManagerEvents;


 //   FTileManagerEvents: TTileManagerEvents;


//    FMapList: TStringList;

//    FObjectMapList: TStringList;
//    FObjectMapList_new: TObjectMapList;


    // ��� �������� �����
//    FMoveBitmap: TBitmap;
//    FLeftButtonPressed: Boolean;
    FRightButtonPressed: boolean;
    FOldMousePos: TPoint;
    CursorPosXY: TPoint;

    FRefreshRect: CMapxRectangle;

    FHiddenMaps: TStringList;

    FOnCursor         : TOnMapCursorEvent;
    FOnVector         : TOnMapViewVectorEvent;
    FOnDraftVector    : TOnMapViewVectorEvent;
    FOnRect           : TOnMapViewRectEvent;
    FOnRing           : TOnMapViewRingEvent;
    FOnMapViewPoly    : TOnMapViewPolyEvent;
    FOnPoint          : TOnMapViewPointEvent;
    FOnMouseDown      : TOnMapViewMouseDownEvent;

    FOnLog            : TOnMapLogEvent;
    FOnDistance       : TOnMapViewDistanceEvent;

  private

    //0-free; 1-select; 2-cursor

    {
    FEvents:    Array [0..10] of record //0..2,
                //  Sender: TObject;
                  Proc: TMapNotifyEvent;
                end;
     }

//    procedure CallNotifyEvents(aIndex: integer);

    procedure MAP_CLEAR_USER_DRAWINGS;


  private
    FCursorPos: TBLPoint;

    FWorkspaceFileName: string;
    /// list of temp layer names
    FTempLayerNameList: TStringList;

  //  FBlPos: TBLPoint;
    FOnChangeProfileFirstPointPos: TnotifyEvent;

    function AddBezierToLayer(aName: string; var aBlPoints: TBLPointArrayF; aColor:
        integer): Boolean;

// TODO: CreateLayerFromDb
//  function CreateLayerFromDb(aDataSet: TCustomAdoDataSet; aFileName: string;
//      aIDFld: string = 'id'; aLatFld: string = 'lat'; aLonFld: string = 'lon'):
//      CMapxLayer;
    procedure DoOnCursor (aBLPoint: TBLPoint);
    procedure DoOnDistance (aDistance,aAzimuth: double);
    procedure DoOnPoly (var aBLPoints: TBLPointArrayF);
//    procedure DoOnTileLayerChanged(aName: string);
//    procedure DoOnTileLoadEvent(aFileName: string; aZ: integer);
    procedure DrawCustomShapes(aOutputDC: Cardinal);
    function GetCenter: TBLPoint;
//    function GetSelectedDataset1111111111: TDataSet;
    function GetSelectedItemList: TMapSelectedItemList;
// TODO: LoadMapsFromClass_old
//  procedure LoadMapsFromClass_old(aMapDesktopItem: TMapDesktopItem);

  { TODO : remove }

    procedure Log(aMsg: string);
//    procedure MapUpdate111(aRec: TMapViewAddRec; aLayer: CMapXLayer);
//    procedure RefreshStatusBar(aBLPoint: TBLPoint);
    procedure SetMapHinting(aHintingEnabled: boolean);

    function MapToScreen(aBLPoint: TBLPoint): TPoint;



    procedure MAP_ADD_USER_LINE(aColor, aWidth: Integer; aBLPoint1,aBLPoint2:
        TBLPoint);
    procedure Map_Add_user_Poly(var aBLPointArr: TBLPointArrayF; aColor: integer);
    procedure MAP_ADD_USER_TEXT(aBLPoint: TBLPoint; aText: string);
    procedure RefreshStatusBar_new(aBLPoint: TBLPoint);
    procedure SearchFind(aLat: Double; aLon: Double);
    procedure StatusBar_Update_HeightInfo(aBLPoint: TBLPoint);
//    procedure THEME_CHANGED;
// TODO: SetDefaultDisplayCoordSys
//  procedure SetDefaultDisplayCoordSys;
    procedure UpdateZoom;

    procedure UserLayerDestroy;
  protected
    procedure DoOnPoint (aBLPoint: TBLPoint);
    procedure DoOnRing (aCenter: TBLPoint; aRadius: double);
    procedure DoOnRect   (aBLRect: TBLRect);
    procedure DoOnVector (aBLVector: TBLVector);


    procedure SelectObjectByID(aObjName: string; aBLPoint: TBLPoint; aID: Integer);

    procedure MAP_RESTORE_WORKSPACE;

  private
//    FLeftButtonClicked, FIsTempPan: boolean;


    FWindowTool: TMapWindowTool;
    FBLPointArr: TBLPointArrayF;
    FUserShapeList: TMapUserShapeList;

    FMapHintingEnabled: boolean;
    FOnDblClick: TNotifyEvent;
    FOnMapViewChanged: TNotifyEvent;

    FOnObjectClick: TNotifyEvent;
//    FOnWmsSelected: TnotifyEvent;

    FSelectedLayerList: TIDList;

  protected
//    function  RegisterNotifyEvent(Sender: TObject; aEvent: TMapNotifyEvent): integer;  //aEventIndex: Integer;
//    procedure UnRegisterSender(aProcIndex: integer);


    function  GetWindowTool (): TMapWindowTool;
    procedure SetWindowTool (Value: TMapWindowTool); virtual;

    //------------------------------
    // TempLayer
    //------------------------------
    procedure TempLayer_Clear(aName: string);
    function TempLayer_Create(aLayerName: string): CMapxLayer;


    procedure AddLineToLayer(aName: string; aBlVector: TBLVector; aColor: integer);
    procedure DeleteTempLayers;

    procedure RemoveLayerByName(aName: string);
    function GetLayerIndexByName_from_1(aName: string): integer;

    procedure RemoveLayerByIndex_from_1(aIndex: integer);
    function GetLayerByName(aName: string): CMapxLayer;

    procedure UserLayerDraw;


    procedure SearchAtPoint(aBLPoint: TBLPoint);

    procedure ChangeProfileFirstPointPos(aBLPoint: TBLPoint);

    function GetBLCursor: TBLPoint;
    function GetFeaturesByScreenBounds_ByObjectName(aObjectName: string): CMapXFeatures; //override;
    function GetZoomByScale(aZoomInCm: Integer): double;
  public
    SelectedItemList: TMapSelectedItemList;

    WMS: record
      Active : boolean;
      LayerName : string;

      Z : Integer; 
    end;



    // ---------------------------------------------------------------


    procedure MAP_ADD_LAYER_LINE(aLayerName: string; aBLVector: TBLVector; aColor,
        aStyle, aWidth: integer; aLabel: string);


    procedure Layer_AddPolygon(aLayerName: string; aBLPoints: TBLPointArray;
        aColor, aStyle, aWidth: Integer; aLabel: string);

    procedure MAP_ADD_LAYER_BEZIER(aLayerName: string; var aBLPointArr:
        TBLPointArrayF; aColor: integer);

    // ---------------------------------------------------------------

    procedure MAP_SHOW_BLRECT(aBLRect: TBLRect);
    procedure MAP_SHOW_VECTOR(aBLVector: TBLVector);
    procedure MAP_SHOW_POINT(aBLPoint: TBLPoint);


//    procedure PreviousTool(); virtual; abstract;
    procedure SetBounds (aBLRect: TBLRect);
    function  GetBounds: TBLRect;
    function  GetScale: double;
    function  GetPlanBounds: TBLRect;


    procedure ZoomInRect  (aRect: TBLRect);
    procedure ZoomOut (aTimes: integer);
    procedure ZoomPlan;
    procedure SetCenter(aBLPoint: TBLPoint);

    procedure AlignBoundsForBLVector (aBLVector: TBLVector);
// TODO: RefreshObjectStyles
//  procedure RefreshObjectStyles;
    procedure SetBoundsWithOffset (aBLRect: TBLRect);

    //dialogs
    procedure Dlg_MapAdd;
    procedure Dlg_Layers;
    procedure Dlg_PrintToFile;
    procedure Dlg_SystemLayers;

    procedure MapAddSimple(aFileName: string);

    procedure ClearSelection;

    procedure SetDefaultTool;

//    procedure PageDown;
//    procedure PageUp;

    function WorkspaceLoad(aFileName: string): boolean;

    procedure WorkspaceSave(aFileName: string);

    //aIsRebuildMaps: boolean;  //aIsRebuildMaps: boolean;

    procedure LoadMapsFromClass(aMapDesktopItem: TMapDesktopItem);
    procedure LoadMaps_Update;


    function  Map(): TMap;

    procedure Clear;
    function Count: integer;
    procedure Dlg_Info;
    procedure Dlg_Liner;

    procedure DrawCross(aBLPoint: TBLPoint);

    procedure LogError (aProcName, aMsg: string);

//    function MapAdd11(aRec: TMapViewAddRec): CMapXLayer;

    procedure MapRefresh;

    function GetLayerByIndex_1(aIndex: integer): CMapxLayer;


    function GetLayerByObjectName(aObjectName: string): CMapxLayer;
    function GetMapLayerByFileName(aFileName: string): CMapxLayer;
    function GetMapLayerIndexByFileName_from_1(aFileName: string): integer;
//    function WMS_GetScaleByTileZ(aZ: integer): double;
//    function GetMapLayerIndexByFileName(aFileName: string): integer;

    procedure HideMap1(aFileName: string);
    procedure MapDelByFileName(aFileName: string);
    procedure MapDelByMask(aMask: string);
    procedure MapUpdateStart;
    procedure MapUpdateStop;
//    procedure MapDelByID(aID: Integer);

    procedure MAP_SAVE_AND_CLOSE_WORKSPACE;

    procedure RestoreMap1(aFileName: string);
    procedure RunMapinfoWorkspace;
    procedure SetCustomZoom_Dlg;
    procedure SetScale(aScaleInCm: Integer);
    procedure UpdateObjectMaps;

//    procedure WMS_Select_Dlg(aX, aY: Integer);


    procedure WMS_Change_Layer(aName: string; aURL: string; aEPSG: word; aZ_max:
        byte);



//    procedure WMS_Change_Layer_Index(aIndex: Integer);
    procedure WMS_Init_proc;
    procedure WMS_Log;
    procedure WMS_Update;

    function WriteSymbol(aLayerName: string; aPoint: TBLPoint; aCode, aColor,
        aFontSize: integer): CMapXFeature;

    // -------------------------------------------------------------------

    property  MapHintingEnabled: boolean   read FMapHintingEnabled  write SetMapHinting;

    property  OnChangeFirstPointPos: TNotifyEvent read FOnChangeProfileFirstPointPos write FOnChangeProfileFirstPointPos;
    property  OnCursor  : TOnMapCursorEvent       read FOnCursor         write FOnCursor;
    property  OnVector  : TOnMapViewVectorEvent   read FOnVector         write FOnVector;
    property  OnDraftVector: TOnMapViewVectorEvent   read FOnDraftVector write FOnDraftVector;
    property  OnLog     : TOnMapLogEvent          read FOnLog            write FOnLog;
    property  OnRect    : TOnMapViewRectEvent     read FOnRect           write FOnRect;
    property  OnPoly    : TOnMapViewPolyEvent     read FOnMapViewPoly    write FOnMapViewPoly;
    property  OnRing    : TOnMapViewRingEvent     read FOnRing           write FOnRing;
    property  OnPoint   : TOnMapViewPointEvent    read FOnPoint          write FOnPoint;
    property  OnMouseDown: TOnMapViewMouseDownEvent read FOnMouseDown    write FOnMouseDown;

    property OnMapViewChanged: TNotifyEvent read FOnMapViewChanged write
        FOnMapViewChanged;


    property  OnDistance: TOnMapViewDistanceEvent read FOnDistance       write FOnDistance;

    property BLCursor: TBLPoint read GetBLCursor;
    property OnObjectClick: TnotifyEvent read FOnObjectClick write FOnObjectClick;
    property OnDblClick: TnotifyEvent read FOnDblClick write FOnDblClick;
    property WindowTool: TMapWindowTool read GetWindowTool write SetWindowTool;

//    property OnWmsSelected: TnotifyEvent read FOnWmsSelected write FOnWmsSelected;



  end;

const
   DISP_E_PARAMNOTFOUND =0;


//==================================================================
//implementation
//==================================================================
implementation {$R *.dfm}

const
   USER_LAYER_NAME = 'UserLayer';

   DEF_COORD_INDEX  = 0;
   DEF_SCALE_INDEX  = 1;
   DEF_RELIEF_INDEX = 2;



procedure Tframe_MapView_base.act_Legend_ShowExecute(Sender: TObject);
begin
  if Sender=act_Legend_Show then
  begin
    FMapEngine.Legend_Show_;

  end;

  if Sender=act_Legend_Hide then
    FMapEngine.Legend_Clear;


end;

   //StatusBar1


//-------------------------------------------------------------------
procedure Tframe_MapView_base.FormCreate(Sender: TObject);
//-------------------------------------------------------------------
var
  I: Integer;
//  vEmpty: Variant;
  j: Integer;
begin
  inherited;


//  FMapList:=TStringList.Create;


 // FMap :=TMap.Create(Self);
//  FMap.Parent  := pn_Map;

  FMap:=Map1;

  FMap.Align:=alClient;
  

  FMapEngine := TMapEngine.Create(FMap);

  FMapEngine.PopupMenu:= PopupMenu_map;

  FMapEngine.OnMapCursor  := DoOnCursor;

  FSelectedLayerList:= TIDList.Create;
  FHiddenMaps:=TStringList.Create;
  FHiddenMaps.Duplicates:=dupIgnore;

  FUserShapeList:=TMapUserShapeList.Create;

  FTempLayerNameList := TStringList.Create();
  FTempLayerNameList.Duplicates:=dupIgnore;

  Map1.Align:=alClient;

  i:=FMap.MousewheelSupport;


//  Map1.MousewheelSupport:=3; //3 - miFullMousewheelSupport

 // FMap.PopupMenu:=PopupMenu1;


(*  TVarData(vEmpty).vType := varError;
  TVarData(vEmpty).vError := DISP_E_PARAMNOTFOUND;
*)

 // for I := 0 to StatusBar1.Count - 1 do    // Iterate
  //  StatusBar1.Panels[i].Text:='';

//  dxStatusBar1.Panels[0].Width:=230;
//  dxStatusBar1.Panels[1].Width:=230;

  dxStatusBar1.PopupMenu:=PopupMenu_StatusBar;


{
  StatusBar1.Panels[2].Width:=230;
  StatusBar1.Panels[3].Width:=200;
  StatusBar1.Panels[4].Width:=200;
}


//  StatusBar1.Panels[3].Width:=110;
//  StatusBar1.Panels[4].Width:=110;



//  for i := 0 to StatusBar1.Panels.Count - 1 do
//    StatusBar1.Panels[i].Text:='';

  FMap.OnClick        :=Map1Click;
  FMap.OnDblClick     :=Map1DblClick;
  FMap.OnDrawUserLayer:=Map1UserLayerDraw;
  FMap.OnMapViewChanged:=Map1MapViewChanged;

  FMap.OnPolyToolUsed :=Map1PolyToolUsed;

  FMap.Visible := True;


  FRefreshRect := CoRectangle.Create;
  FRefreshRect.Set_(0, 0, Screen.Width, Screen.Height);


{
  if g_Debug then
  begin
    ToolBar1.Visible := True;
    dxDBInspector1.RestoreRowsDefaults;
  end;

}

  SelectedItemList := TMapSelectedItemList.Create();

//  FObjectMapList := TStringList.Create();

//  FObjectMapList_new := TObjectMapList.Create();


//  {$IFDEF test}

 // Panel_test.Visible := False;

  TdmMapEngine_store.Init;
  dmMapEngine_store.Map := FMap;

//  {$ELSE}
//  Panel_test.Visible := False;
//
//  {$ENDIF}
//




//{$IFDEF wms}
  WMS_Init_proc;
//{$ENDIF}


  dmRel_Engine.Open();



  act_Legend_Show.Caption:='�������� �������';
  act_Legend_Hide.Caption:='������ �������';

  //

  //  DefaultStyle.SetLine (MI_PEN_PATTERN_ARROW, clGreen, 1 );
{
  FMap.DefaultStyle.RegionBorderColor:=clRed;
  FMap.DefaultStyle.RegionBorderWidth:=1;
  FMap.DefaultStyle.RegionBorderStyle:=MI_PEN_PATTERN_SOLID;
  FMap.DefaultStyle.RegionColor := clBlack;
  FMap.DefaultStyle.RegionBackColor := clFuchsia;
}

  FMap.DefaultStyle.RegionBorderColor:=clRed;
  FMap.DefaultStyle.RegionBorderWidth:=2;
  FMap.DefaultStyle.RegionBorderStyle:=2;
  FMap.DefaultStyle.RegionBackColor  := clFuchsia;

//  SetDefaultDisplayCoordSys();
end;


// -------------------------------------------------------------------
procedure Tframe_MapView_base.WMS_Init_proc;
// -------------------------------------------------------------------
begin
    // -----------------------------

//  FMapEngine.SetNumericCoordSys_DATUM_WGS84;

//  FWmsComClient:=TWmsComClient.Create;

//  FTileManager:=FWmsComClient.Intf;

  FTileManager:=TTileManagerX.Create;
  FTileManager.Init(Map1);
//  FTileManager:=WMS_Init_ITileManagerX();
//  FTileManager:=WMS_Init_ITileManagerX();

 // FTileManager:=CoTileManagerX.Create;

//  Assert ( GUIDToString(FTileManager.GetClassGUID) = GUIDToString(IID_ITileManagerX) ,'procedure Tframe_MapView_base.WMS_Init_proc;' );


//  FTileManager.Init(Map1.DefaultInterface);



 // FTileManager:=TTileManager.Create;



end;


// -------------------------------------------------------------------
procedure Tframe_MapView_base.FormDestroy(Sender: TObject);
// -------------------------------------------------------------------
begin
  Map1.OnMapViewChanged:=nil;


//  FTileManager:=nil;

//  FreeAndNil (FWmsComClient);

  FreeAndNil (FTileManager);

//  FMapEngine.Del_Layer_ByName('temp');


//  TdmMapEngine_store.Init;

//  mapx_dele

{
   vLayer:= mapx_GetLayerByName(Map1, 'temp');
   if Assigned(vLayer) then
     mapx_DeleteLayerFeatures(Map1, 'temp')
   else
    vLayer:=Map1.Layers.CreateLayer ('temp', EmptyParam, 1, EmptyParam, EmptyParam);

}

  dmMapEngine_store.Map := nil;


//  FreeAndNil (FTileManagerEvents);


//  FreeAndNil(FObjectMapList);
//  FreeAndNil(FObjectMapList_new);
  FreeAndNil(FMapEngine);

  FreeAndNil(SelectedItemList);

//  CallNotifyEvents(MAP_EVENT_ON_FREE_);

  g_EventManager.PostEvent_(et_MAP_EVENT_ON_Free, [] );

  FreeAndNil(FHiddenMaps);

  UserLayerDestroy();

  FreeAndNil(FTempLayerNameList);
  FreeAndNil(FSelectedLayerList);
  FreeAndNil(FUserShapeList);

//  FreeAndNil( FMap);

  inherited;
end;


procedure Tframe_MapView_base.SetDefaultTool;
begin
   SetWindowTool (toolObject);
end;

procedure Tframe_MapView_base.ZoomPlan;
begin
  FMapEngine.Legend_Clear;

  SetBounds (GetPlanBounds());


end;

procedure Tframe_MapView_base.ZoomInRect(aRect: TBLRect);
begin
  SetBounds (aRect);
end;

procedure Tframe_MapView_base.ZoomOut(aTimes: integer);
begin
  SetBounds (geo_ZoomOut(GetBounds(), aTimes));
end;

procedure Tframe_MapView_base.Log(aMsg: string);
begin
  if Assigned(FOnLog) then  FOnLog('map: ' + aMsg);
end;


// ---------------------------------------------------------------
procedure Tframe_MapView_base.MapDelByFileName(aFileName: string);
// ---------------------------------------------------------------

begin
  FMapEngine.Del_Map (aFileName);
              
end;



// ---------------------------------------------------------------
procedure Tframe_MapView_base.MapDelByMask(aMask: string);
// ---------------------------------------------------------------
var
  iInd: Integer;
 // sFile: string;
  sID: string;

begin
  Assert(aMask<>'');

//  sID:=IntToStr(aID);

 // sFile:= FMapList.Values[ sID ];

  repeat
    iInd:=mapx_GetLayerIndexByMask_from_1( FMap, aMask);
 //  v:=mapx_GetLayerByFileName(sFile);

    if iInd>0 then
      FMap.Layers.Remove(iInd);
      
  until
    iInd<0;

 //   FMapList.Values[ sID ]:='';

//    MapDelByID (aFileName, aID);

end;



//------------------------------------------------------------
procedure Tframe_MapView_base.MapAddSimple(aFileName: string);
//------------------------------------------------------------
//var
 // s: string;
//  v: CMapXLayer;
begin
  FMapEngine.Add_Map (aFileName);

//  mapx_


{  if FMap.Layers.Count=0 then
    mapx_SetMapDefaultProjection(FMap);

  mapx_AddMap(FMap, aFileName);

}
{
  if not mapx_MapHasRaster(FMap) then
    mapx_SetMapDefaultProjection(FMap);
}



//  s:=v.KeyField;
//  s:='';

//  if aID>0 then
 //   FMapList.Values[ IntToStr(aID) ]:=aFileName;

end;


//------------------------------------------------------------
procedure Tframe_MapView_base.ChangeProfileFirstPointPos(aBLPoint: TBLPoint);
//------------------------------------------------------------
begin
  FMapEngine.FFirstPointPos:= aBLPoint;

  if Assigned(FOnChangeProfileFirstPointPos) then
    FOnChangeProfileFirstPointPos(Self);


  FMapEngine.FFirstPointClicked:= true;

end;


//------------------------------------------------------------
procedure Tframe_MapView_base.DoOnCursor (aBLPoint: TBLPoint);
//------------------------------------------------------------
begin
  Debug (Format('lat: %1.5f   lon: %1.5f', [aBLPoint.B, aBLPoint.L]));


  if geo_ComparePoints(FCursorPos, aBLPoint) then
    Exit;

   try

    FCursorPos:=aBLPoint;

//    RefreshStatusBar(aBLPoint);
    RefreshStatusBar_new(aBLPoint);

    if Assigned(FOnCursor) then
      FOnCursor(aBLPoint);

/////////    CallNotifyEvents(MAP_EVENT_ON_CURSOR_);
    g_EventManager.PostEvent_LatLon(et_MAP_EVENT_ON_CURSOR, FCursorPos.B, FCursorPos.L );

    if (FWindowTool = toolVector) and (FMapEngine.FFirstPointClicked) then
      if Assigned(FOnDraftVector) then
        FOnDraftVector(MakeBLVector(FMapEngine.FFirstPointPos, aBLPoint));



     // statements to try
   except


    // on e: EClassNotFound do
     //  Application.HandleException(Sender);
   end;    // try/except


{    blRect:=FMapEngine.GetBounds();



  sBounds:= Format('%1.6f %1.6f %1.6f %1.6f', [blRect.TopLeft.B, blRect.TopLeft.L, blRect.BottomRight.B, blRect.BottomRight.L]);

}


end;

//------------------------------------------------------------
procedure Tframe_MapView_base.DoOnPoly(var aBLPoints: TBLPointArrayF);
//------------------------------------------------------------
var i: integer;
begin
//  for i:=0 to aBLPoints.Count-1 do
 //   aBLPoints.Items[i] :=geo_TruncPoint (aBLPoints.Items[i]);

  if Assigned(FOnMapViewPoly) then
    FOnMapViewPoly (aBLPoints);
end;



//------------------------------------------------------------
procedure Tframe_MapView_base.DoOnVector (aBLVector: TBLVector);
//------------------------------------------------------------
begin
  if geo_Distance_m (aBLVector.Point1, aBLVector.Point2) < 2 then
    Exit;

  if Assigned(FOnVector) then
    FOnVector(aBLVector);
end;

//------------------------------------------------------------
procedure Tframe_MapView_base.DoOnRing (aCenter: TBLPoint; aRadius: double);
//------------------------------------------------------------
begin
  if Assigned(FOnRing) then
    FOnRing (aCenter, aRadius);
end;

procedure Tframe_MapView_base.DoOnDistance (aDistance,aAzimuth: double);
begin
  if Assigned(FOnDistance) then
    FOnDistance (aDistance,aAzimuth);
end;


//------------------------------------------------------------
procedure Tframe_MapView_base.DoOnRect (aBLRect: TBLRect);
//------------------------------------------------------------
begin
  if 2 > geo_Distance_m (aBLRect.TopLeft, aBLRect.BottomRight) then
    Exit;

//  if 2 > geo_DistanceBL (aBLRect.TopLeft, aBLRect.BottomRight) then

//  aBLRect.TopLeft:=geo_TruncPoint (aBLRect.TopLeft);
//  aBLRect.BottomRight:=geo_TruncPoint (aBLRect.BottomRight);

  if Assigned(FOnRect) then
    FOnRect (aBLRect);
end;

//------------------------------------------------------------
procedure Tframe_MapView_base.DoOnPoint (aBLPoint: TBLPoint);
//------------------------------------------------------------
begin
  if Assigned(FOnPoint) then
    FOnPoint (aBLPoint);




end;

//------------------------------------------------------------
function Tframe_MapView_base.GetWindowTool (): TMapWindowTool;
//------------------------------------------------------------
begin
  Result:=FWindowTool;
end;



procedure Tframe_MapView_base.SetWindowTool (Value: TMapWindowTool);
begin
  FWindowTool:=Value;
  FMapEngine.FFirstPointClicked:=False;
end;


//--------------------------------------------------------------------
procedure Tframe_MapView_base.Dlg_MapAdd;
//--------------------------------------------------------------------
var i: integer;  fname: string;  bSetPlan: boolean;
begin
  bSetPlan:=(Count=0);

  OpenDialog1.Filter:='MapInfo (*.tab)|*.tab';

  if OpenDialog1.Execute then
  begin
    for i:=0 to OpenDialog1.Files.Count-1 do
    begin
      fname:=OpenDialog1.Files[i];
      MapAddSimple (fname);
    end;

    if bSetPlan then
      ZoomPlan;
  end;
end;


// -------------------------------------------------------------------
procedure Tframe_MapView_base.FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
// -------------------------------------------------------------------
begin
  FMapEngine.FormKeyDown(Key, Shift, FCursorPos);

end;


procedure Tframe_MapView_base.AlignBoundsForBLVector(aBLVector: TBLVector);
//var blRect: TBLRect;
begin
  FMapEngine.AlignBoundsForBLVector(aBLVector);

(*  blRect:=geo_BoundBLVector(aBLVector);
  blRect:=geo_ZoomRect (blRect, 0.01);
  SetBounds (blRect);
*)
end;


procedure Tframe_MapView_base.SetBoundsWithOffset(aBLRect: TBLRect);
begin
  aBLRect:=geo_ZoomRect (aBLRect, 0.01);
  SetBounds (aBLRect);
end;

procedure Tframe_MapView_base.MAP_ADD_LAYER_BEZIER(aLayerName: string; var
    aBLPointArr: TBLPointArrayF; aColor: integer);
begin
  AddBezierToLayer(aLayerName, aBLPointArr, aColor);
end;

// ---------------------------------------------------------------
function Tframe_MapView_base.AddBezierToLayer(aName: string; var aBlPoints:
    TBLPointArrayF; aColor: integer): Boolean;
// ---------------------------------------------------------------
var
  vCMapXLayer: CMapXLayer;
begin
  vCMapXLayer:=GetLayerByName(aName);

  if Assigned(vCMapXLayer) then
    mapx_AddBezierToLayer (Map, vCMapXLayer, aBlPoints, aColor,  2)
  else
    raise Exception.Create('procedure Tframe_MapView_base.AddBezierToLayer(aName: string;');
  //AddBezierToLayer
end;

//--------------------------------------------------------------------
procedure Tframe_MapView_base.AddLineToLayer(aName: string; aBlVector:
    TBLVector; aColor: integer);
//--------------------------------------------------------------------
begin
  mapx_AddLineToLayer_ (FMap, aName, aBlVector, aColor);

end;




procedure Tframe_MapView_base.bCrossClick(Sender: TObject);
var
  vCMapXFeature: CMapXFeature;
  vCMapxLayer: CMapxLayer;

  bl: TBLPoint;

begin

  bl:= mapx_GetCenter(FMap) ;


 vCMapxLayer:=TempLayer_Create('temp');

 vCMapXFeature:=WriteSymbol('temp', bl, 4, clRed, 28);


end;


//------------------------------------------------------
function Tframe_MapView_base.WriteSymbol(aLayerName: string; aPoint: TBLPoint;
    aCode, aColor, aFontSize: integer): CMapXFeature;
//------------------------------------------------------
//aAngle: integer;
var f: CMapXFeature;
    p: CMapXPoint;
  s: string;
  vCMapXLayer: CMapxLayer;
begin
//  Assert(Assigned(VLayer), 'Value not assigned');

  vCMapXLayer:=GetLayerByName(aLayerName);


  p := CoPoint.Create;
  p.Set_ (aPoint.L, aPoint.B);

  {
     FontName  :=xml_GetAttr   (aNode,'FontName',  FontName );
    Character :=xml_GetIntAttr(aNode,'Character', Character);
    FontColor :=xml_GetIntAttr(aNode,'FontColor', FontColor);
    FontSize  :=xml_GetIntAttr(aNode,'FontSize',  FontSize );
   }


  //FMap.DefaultStyle.SymbolFontRotation:=aAngle;

  FMap.DefaultStyle.SymbolFontColor:=aColor;
  FMap.DefaultStyle.SymbolCharacter:=36; // aCode  ;

  s:=OleVariant(FMap.DefaultStyle.SymbolFont).Name;

  OleVariant(FMap.DefaultStyle.SymbolFont).Size:=28;


//  FMap.DefaultStyle.SymbolFontSize:=28;
//  FMap.DefaultStyle.SymbolCharacter:=aCode;

         {
           vLayer.Style.SymbolFontColor:=rStyle.MIStyle.FontColor;
//           vLayer.Style.SymbolFontColor:=rStyle.MIStyle.FontColor;

  //         j:=vLayer.Style.SymbolFontColor;

            //vFeature.Style.SymbolFontColor := miColorRed;

            OleVariant(vLayer.Style.SymbolFont).Name  := rStyle.MIStyle.FontName;// 'Mapinfo Symbols';
            OleVariant(vLayer.Style.SymbolFont).Size  := rStyle.MIStyle.FontSize; //  20;
          //  OleVariant(vFeature.Style.SymbolFont).Color  := clRed;


        //    vFeature.Style.SymbolFont := 41;
            vLayer.Style.SymbolCharacter := rStyle.MIStyle.Character;// FontColor; //;;//41;
         //   vFeature.Style.SymbolFontBackColor := miColorRed;
      }



  f:=FMap.FeatureFactory.CreateSymbol (p, FMap.DefaultStyle);

//  f:= aMap.FeatureFactory.CreateLine (ps, aMap.DefaultStyle);
  vCMapXLayer.AddFeature (f, EmptyParam);



  try

  //  VLayer.BeginAccess(miAccessReadWrite);
  //  Result:=AddFeature (f);

  //  WriteFeatureFieldValues (Result, aParams);
  finally
  //  VLayer.EndAccess(miAccessEnd);
  end;
end;





procedure Tframe_MapView_base.bSearchClick(Sender: TObject);
begin
//
//RPLS_search_TLB
{
  FTileManagerX:=CoTileManagerX.Create;
  FTileManagerX.Set_Events(FTileManagerEvents as ITileEventsX);
}


end;

procedure Tframe_MapView_base.Button1Click(Sender: TObject);
begin
  //Map1.PropertyPage;


   showMessage ('NumericCoordSys ' +  mapx_CMapXCoordSys_to_str(FMap.NumericCoordSys) + CRLF +
                'DisplayCoordSys ' +  mapx_CMapXCoordSys_to_str(FMap.DisplayCoordSys)
                );

end;



procedure Tframe_MapView_base.SearchFind(aLat: Double; aLon: Double);
begin
  FMapEngine.SetCenter_LatLon(aLat,aLon);

 // ShowMessage('Find');
end;





procedure Tframe_MapView_base.Clear;
begin
  FMapEngine.Clear;


//    if aFileName='' then
 //   aFileName := FWorkspaceFileName;

  DeleteTempLayers();

//  if aFileName='' then
//    aFileName := FWorkspaceFileName;

  DeleteFile(FWorkspaceFileName);


 // WorkspaceSave('');

end;

procedure Tframe_MapView_base.ClearSelection;
begin
  mapx_ClearSelection (FMap);

  if FMap.Layers.Count>1 then
    FSelectedLayerList.Clear;

end;

//--------------------------------------------------------------------
procedure Tframe_MapView_base.TempLayer_Clear(aName: string);
//--------------------------------------------------------------------
begin
  mapx_DeleteLayerFeatures(FMap, aName);
end;

function Tframe_MapView_base.Count: integer;
begin
  Result:=FMap.Layers.Count;
end;

//--------------------------------------------------------------------
function Tframe_MapView_base.TempLayer_Create(aLayerName: string): CMapxLayer;
//--------------------------------------------------------------------
var
  s: string;
//  s: string;
  vLayer: CMapXLayer;
begin
  Result:=nil;

  if GetLayerIndexByName_from_1(aLayerName) < 0 then
  begin
    vLayer:=FMap.Layers.CreateLayer (aLayerName, EmptyParam, 1, EmptyParam, EmptyParam);

//    vLayer.

    s:=vLayer.Filespec;

    vLayer.Selectable := True;
    vLayer.LabelProperties.Visible := True;

    FMap.Layers.AnimationLayer := vLayer;

    FTempLayerNameList.Add(vLayer.Name);

    Result:=vLayer;

  end;
end;

//--------------------------------------------------------------------
procedure Tframe_MapView_base.UserLayerDestroy;                                   
//--------------------------------------------------------------------
begin
  inherited;

  FUserShapeList.Clear;

// if mapx_GetLayerIndexByName(Map1.Layers, 'UserLayer')>0 then
  if mapx_GetLayerIndexByName_from_1(FMap, USER_LAYER_NAME)>0 then
    FMap.Layers.Remove(1);
end;

procedure Tframe_MapView_base.Dlg_Layers;
begin
  Tdlg_Map_MIF_Layers.ExecDlg (FMap);
end;

procedure Tframe_MapView_base.Dlg_SystemLayers;
begin
  FMap.Layers.LayersDlg(EmptyParam, EmptyParam);
end;

//--------------------------------------------------------------------
procedure Tframe_MapView_base.UserLayerDraw;
//--------------------------------------------------------------------
var
  iIndex: integer;
  vLayer: CMapXLayer;
begin
  iIndex:=mapx_GetLayerIndexByName_from_1(FMap, USER_LAYER_NAME);

  if iIndex>0 then
  begin
    try
      vLayer:=FMap.Layers.Item[iIndex];

      vLayer.Invalidate (FRefreshRect);


    //zz  Map1.Layers.Item[1].Invalidate (FRefreshRect);
    except
      on E: Exception do
        LogError  ('UserLayerDraw', 'Map1.Layers.Item(1).Invalidate (FRefreshRect);'+ E.Message);
//      gl_ErrorLog.AddRecord('Tframe_MapView_MIF', 'UserLayerDraw, Map1.Layers.Item(1).Invalidate (FRefreshRect);', E.Message);
    end;
  end else begin
    try
      FMap.Layers.AddUserDrawLayer(USER_LAYER_NAME, 1);


     FMap.Layers.AnimationLayer:= FMap.Layers.Item[1];
    except
      on E: Exception do
        LogError  ('UserLayerDraw', 'Map1.Layers.AddUserDrawLayer'+ E.Message);
      //gl_ErrorLog.AddRecord('Tframe_MapView_MIF', 'UserLayerDraw, Map1.Layers.AddUserDrawLayer', E.Message);
    end;
  end;
end;

//--------------------------------------------------------------------
function Tframe_MapView_base.GetBounds: TBLRect;
//--------------------------------------------------------------------
begin
  Result:=mapx_GetMapBounds(FMap);
end;

//--------------------------------------------------------------------
function Tframe_MapView_base.GetPlanBounds: TBLRect;
//--------------------------------------------------------------------
begin
  Result:=mapx_GetPlanBounds(FMap);
end;

// -------------------------------------------------------------------
function Tframe_MapView_base.GetScale: double;
// -------------------------------------------------------------------
begin
  Result:=mapx_GetScale (FMap);
end;

//--------------------------------------------------------------------
function Tframe_MapView_base.GetLayerByName(aName: string): CMapxLayer;
//--------------------------------------------------------------------
begin
  result :=mapx_GetLayerByName (FMap, aName);
end;

//--------------------------------------------------------------------
function Tframe_MapView_base.GetLayerIndexByName_from_1(aName: string): integer;
//--------------------------------------------------------------------
begin
  result :=mapx_GetLayerIndexByName_from_1 (FMap, aName);
end;

//--------------------------------------------------------------------
function Tframe_MapView_base.Map: TMap;
//--------------------------------------------------------------------
begin
  Result:= FMap;
end;

//--------------------------------------------------------------------
function Tframe_MapView_base.GetMapLayerIndexByFileName_from_1(aFileName:
    string): integer;
//--------------------------------------------------------------------
begin
  Result :=mapx_GetLayerIndexByFileName_from_1(FMap, aFileName);
end;

//--------------------------------------------------------------------
function Tframe_MapView_base.GetMapLayerByFileName(aFileName: string): CMapxLayer;
//--------------------------------------------------------------------
begin
  result :=mapx_GetLayerByFileName (FMap, aFileName);
end;

procedure Tframe_MapView_base.MapRefresh;
begin
  FMap.Refresh;
end;

//---------------------------------------------------------------------------
procedure Tframe_MapView_base.Dlg_PrintToFile;
//---------------------------------------------------------------------------
begin
  mapx_Dlg_PrintToFile (FMap);
end;

//--------------------------------------------------------------------
function Tframe_MapView_base.WorkspaceLoad(aFileName: string): boolean;
//--------------------------------------------------------------------
//var
  //sFileName: string;
//  sIniFileName: string;
var
  sFile: string;
begin
  FWorkspaceFileName:=aFileName;

 // if not
  mapx_LoadWorkspace (FMap, aFileName);//
  //  Exit;

//  sFile:=ChangeFileExt(aFileName, '.list');

//  if FileExists(sFile) then
  //  FMapList.LoadFromFile ( sFile );                     then


 // sFileName:=ChangeFileExt(aFileName, '.txt');

 // if FileExists(sFileName) then
   // mem_Maps.LoadFromTextFile (sFileName);


 // sIniFileName:=ChangeFileExt(aFileName, '.objects.ini');
//  FObjectMapList_new.loadFromIni(sIniFileName);


  mapx_SetMapDefaultProjection_NumericCoordSys_KRASOVKSY42_simple(Map);

(*
  sFileName:=ChangeFileExt(aFileName, '.str');
  if fileExists(sFileName) then
    FObjectMapList.loadFromFile (sFileName);
*)

  FMap.Title.Visible:=False;

//  RefreshObjectStyles ();
           
end;

procedure Tframe_MapView_base.RemoveLayerByIndex_from_1(aIndex: integer);
begin
  FMap.Layers.Remove (aIndex);
end;

//--------------------------------------------------------------------
procedure Tframe_MapView_base.RemoveLayerByName(aName: string);
//--------------------------------------------------------------------
var
  iIndex: Integer;
begin
  iIndex:= GetLayerIndexByName_from_1(aName);
  if iIndex<>-1 then
    FMap.Layers.Remove (iIndex);
end;

procedure Tframe_MapView_base.DeleteTempLayers;
var
  i: integer;
begin
  UserLayerDestroy();

  for i := 0 to FTempLayerNameList.Count - 1 do
    RemoveLayerByName(FTempLayerNameList[i]);

  FTempLayerNameList.Clear;
end;

procedure Tframe_MapView_base.LogError(aProcName, aMsg: string);
begin
  g_Log.AddRecord(ClassName, aProcName, aMsg);
end;

//-------------------------------------------------------------------------
procedure Tframe_MapView_base.Map1UserLayerDraw(Sender: TObject; const Layer:
    IDispatch; hOutputDC, hAttributeDC: Cardinal; const RectFull, RectInvalid: IDispatch);
//-------------------------------------------------------------------------
begin
  DrawCustomShapes(hOutputDC);
end;

//--------------------------------------------------------------------
procedure Tframe_MapView_base.Map1PolyToolUsed(Sender: TObject; ToolNum:
    Smallint; Flags: Integer; const Points: IDispatch; bShift, bCtrl: WordBool;
    var EnableDefault: WordBool);
//--------------------------------------------------------------------
var
  ps: CMapXPoints;
  vPoint: CMapXPoint;
  i: integer;
begin
  case Flags of

    miPolyToolBegin     : ; { Someone's beginning the use of a PolyTool.. }
    miPolyToolEndEscaped: ; { The users hit 'Esc' or backspaced }

    miPolyToolEnd:  { The user finished using a PolyTool by double clicking }

                      if ToolNum = CUSTOM_POLYLINE_TOOL then
                      begin
                        ps:=CMapXPoints(Points);

                        FBLPointArr.Count:=ps.Count;

                      //  iCount:=ps.Count;

                        for i:=1 to FBLPointArr.Count do
                        begin
                            vPoint:=ps.Item[i];

                          FBLPointArr.Items[i-1]:=MakeBLPoint (vPoint.Y, vPoint.X);
                        end;

                        DoOnPoly (FBLPointArr);
                      end;
  end;
end;

//---------------------------------------------------
function Tframe_MapView_base.MapToScreen(aBLPoint: TBLPoint): TPoint;
//---------------------------------------------------
//var tempX, tempY: single;
begin
  Result := FMapEngine.MapToScreen(aBLPoint);

  (*FMap.ConvertCoord(tempX, tempY, aBLPoint.L,  aBLPoint.B, miMapToScreen);
  Result.X:=Trunc(tempX);
  Result.Y:=Trunc(tempY);
*)
end;

//--------------------------------------------------------------------
procedure Tframe_MapView_base.WorkspaceSave(aFileName: string);
//--------------------------------------------------------------------
var
  sFileName: string;
//  sIniFileName: string;
begin
  DeleteTempLayers();

  if aFileName='' then
    aFileName := FWorkspaceFileName;

  if aFileName='' then
    Exit;


  mapx_SaveWorkspace (FMap, aFileName);

 // FMapList.SaveToFile ( ChangeFileExt(aFileName, '.list') );


(*  sFileName:=ChangeFileExt(aFileName, '.str');
  FObjectMapList.SaveToFile(sFileName);

  Assert(aFileName<>'', 'aFileName<>''''');

  sIniFileName:=ChangeFileExt(aFileName, '.objects.ini');
  FObjectMapList_new.SaveToIni(sIniFileName);
*)
 // FMapItemList.SaveToIniFile(sFileName);

//  mem_Maps.SaveToTextFile(ChangeFileExt(aFileName, '.txt'));

end;

procedure Tframe_MapView_base.MAP_SAVE_AND_CLOSE_WORKSPACE;
begin
  if FWorkspaceFileName<>'' then
    WorkspaceSave (FWorkspaceFileName);
  Clear;
end;

procedure Tframe_MapView_base.MAP_RESTORE_WORKSPACE;
begin
  if FWorkspaceFileName<>'' then
    WorkspaceLoad (FWorkspaceFileName);

end;

//-------------------------------------------------------------------
procedure Tframe_MapView_base.SearchAtPoint(aBLPoint: TBLPoint);
//-------------------------------------------------------------------
var
  s: string;
//  iGeoRegion: integer;
 // sGUID: string;
  sObjCaption, sCaption, sName, sLayerObjName: string; //, sObjCode
  isFound: boolean;
  i, j, k, iCount, iID: integer;
  vPt:    CMapXPoint;
  vLayer: CMapXLayer;
  vSelectedFeature, vFeature: CMapXFeature;
  vFeatures: CMapXFeatures;

  vID, vName: Variant;
  bObjectMap: Boolean;


  oMapSelectedItem: TMapSelectedItem;
  ss: string;
  vDS: CMapxDataset;
 // sLayerObjName: string;
begin

////  db_Clear(mem_Selection);

  SelectedItemList.Clear;


{   if Map1.Zoom >= 150 then
   begin
     g_Log.Add ( '��� ��������� ���������� �� �������� ������� ����� ������ ���� �� ����� 150');
     exit;
   end;
}

   vPt := CoPoint.Create;
   vPt.Set_(aBLPoint.L, aBLPoint.B);

//   CursorHourGlass;

   try
     i:= FMap.Layers.Count;


     for i:=1 to FMap.Layers.Count do
     begin
       vLayer:=mapx_GetLayerByIndex_from_1(FMap, i);

       //check if map is system objects
       if (not vLayer.Visible) or
          (vLayer.Type_<>miLayerTypeNormal)
       then Continue;


       k:=vLayer.Datasets.Count;


  //     if vLayer.Datasets.Count<2 then
  //       Continue;


       s := vLayer.FileSpec;
//       s :=FObjectMapList.Text;

       sLayerObjName:='';

//       FObjectMapList.
       s:=vLayer.FileSpec;
       s:=vLayer.Name;

       if LeftStr(vLayer.Name,1)='~' then
         sLayerObjName:=Copy(vLayer.Name, 2, 100)
       else
         Continue;



(*       j:=GetStringListIndexByValue (FObjectMapList, vLayer.FileSpec);
       if j>=0 then
         sLayerObjName:=FObjectMapList.Names[j]
       else
         Continue;
*)

(*
       s:=FObjectMapList.Text;


       bObjectMap := Pos(LowerCase(vLayer.FileSpec), LowerCase(FObjectMapList.Text))>0;

       if not bObjectMap then
         Continue;*)



       vFeatures:= vLayer.SearchAtPoint(vPt,EmptyParam);


       iCount:= vFeatures.Count;

       if vFeatures.Count=0 then
         Continue;


    //   vLayer.BeginAccess(miAccessRead);

       for j:=1 to vFeatures.Count do
       begin
         vFeature:=vFeatures.Item[j];

         s:=vLayer.Name;

         k:=vLayer.Datasets.Count;


          vDS:= mapx_GetDatasetByName (vLayer, sLayerObjName+'-Data');


   //      if (vLayer.Datasets.Count = 0) then
     //      FMap.Datasets.Add (miDataSetLayer, vLayer, vLayer.Name, EmptyParam,
//           FMap.Datasets.Add (miDataSetLayer, vLayer, EmptyParaDatam, EmptyParam,
         //                     EmptyParam, EmptyParam, EmptyParam, EmptyParam) ;

      //   iID := mapx_GetIntFieldValue (vLayer, 1, vFeature, FLD_GEONAME);

         iID := mapx_GetIntFieldValue (//vLayer,
                                         vDS, vFeature, FLD_ID);


       //  iID := mapx_GetIntFieldValue (vLayer, vFeature, FLD_ID);

         if iID>0 then
         begin
           sName    := mapx_GetStringFieldValue (//vLayer,
                                                  vDS, vFeature, FLD_NAME);

           sCaption := mapx_GetStringFieldValue (//vLayer,
                                                  vDS, vFeature, FLD_CAPTION);

 //          sLayerObjName:= mapx_GetStringFieldValue (vLayer, vFeature, FLD_OBJNAME);
//           sObjCode:= mapx_GetStringFieldValue (vLayer, vFeature, FLD_CODE);
       //    sGUID   := mapx_GetStringFieldValue (vLayer,2,  vFeature, FLD_GUID);


    //     end;


        // if (sLayerObjName<>'') then              /////and
      //   if (iID > 0) and (sLayerObjName<>'') then
       //  begin
           if SelectedItemList.Count > 100 then
           begin
             vLayer.EndAccess(miAccessEnd);
             exit;
           end;


           oMapSelectedItem := SelectedItemList.AddItem;

           oMapSelectedItem.FileName := vLayer.Filespec;
           oMapSelectedItem.ID       := iID;
           oMapSelectedItem.NAME     := sName;
           oMapSelectedItem.CAPTION  := sCaption;
        
           oMapSelectedItem.ObjName  := sLayerObjName;

         end;
   end;

    //   vLayer.EndAccess(miAccessEnd);
     end;

   finally
  //   CursorDefault;
   end;

end;

//--------------------------------------------------------------------
procedure Tframe_MapView_base.SetBounds(aBLRect: TBLRect);
//--------------------------------------------------------------------
begin
  Log(geo_FormatBLRect(aBLRect));

  mapx_SetBounds (FMap, aBLRect);
end;

procedure Tframe_MapView_base.SetMapHinting(aHintingEnabled: boolean);
begin
  FMapHintingEnabled:= aHintingEnabled;
  FMap.ShowHint:= FMapHintingEnabled;
end;

//--------------------------------------------------------------------
procedure Tframe_MapView_base.SetCenter(aBLPoint: TBLPoint);
//--------------------------------------------------------------------
begin
  mapx_SetCenter (FMap, aBLPoint);
//  FMap.ZoomTo (FMap.Zoom, );
end;

//--------------------------------------------------------------------
function Tframe_MapView_base.GetCenter: TBLPoint;
//--------------------------------------------------------------------
begin
//  FMapEngine.G
  result := mapx_GetCenter (FMap);
//  FMap.ZoomTo (FMap.Zoom, );
end;

//-------------------------------------------------------------------
procedure Tframe_MapView_base.SetScale(aScaleInCm: Integer);
//-------------------------------------------------------------------
begin
  FMapEngine.SetScale(aScaleInCm);

//  mapx_SetZoom (FMap, aScaleInCm);
//  FMap.Zoom:= GetZoomByScale(aScaleInCm);
///  Map1.MapPaperWidth * aScaleInCm/100000;
end;

//-------------------------------------------------------------------
function Tframe_MapView_base.GetZoomByScale(aZoomInCm: Integer): double;
//-------------------------------------------------------------------
begin
  Result:=  mapx_GetZoomByScale (FMap, aZoomInCm);

//  Result:= FMap.MapPaperWidth * aZoomInCm / 100000;
end;

// -------------------------------------------------------------------
procedure Tframe_MapView_base.SetCustomZoom_Dlg;
// -------------------------------------------------------------------
begin
  mapx_SetCustomZoomDlg (FMap);
end;

//-------------------------------------------------------------------------
procedure Tframe_MapView_base.DrawCustomShapes(aOutputDC: Cardinal);
//-------------------------------------------------------------------------
var oCanvas: TCanvas;
begin
  if (FUserShapeList.Count>0)  then
  begin
    oCanvas:=TCanvas.Create;
    oCanvas.Handle:=aOutputDC;

    FUserShapeList.OnMapToScreen:=MapToScreen;

    FUserShapeList.DrawShapeList (oCanvas);
    FUserShapeList.Clear;

    oCanvas.Free;
  end;
end;

// ---------------------------------------------------------------
function Tframe_MapView_base.GetLayerByObjectName(aObjectName: string): CMapxLayer;
// ---------------------------------------------------------------
begin
  result :=mapx_GetLayerByName (FMap, '~'+aObjectName); 
end;


procedure Tframe_MapView_base.HideMap1(aFileName: string);
var
  v: CMapxLayer;
begin
  v:=GetMapLayerByFileName(aFileName);

  if assigned(v) then
  begin
    FMap.Layers.Remove (v);
    FHiddenMaps.Add(aFileName);
  end;
end;

procedure Tframe_MapView_base.RestoreMap1(aFileName: string);
begin
  if FHiddenMaps.IndexOf(aFileName)>=0 then
  begin
    FHiddenMaps.Delete(FHiddenMaps.IndexOf(aFileName));
    MapAddSimple(aFileName);
  end;
end;

procedure Tframe_MapView_base.MAP_ADD_USER_LINE(aColor, aWidth: Integer;
    aBLPoint1,aBLPoint2: TBLPoint);
begin
  FUserShapeList.Add(TMapUserShape.CreateLine(aColor, MakeBLVector(aBLPoint1,aBLPoint2), aWidth));
end;

//------------------------------------------------------------------------
procedure Tframe_MapView_base.DrawCross(aBLPoint: TBLPoint);
//------------------------------------------------------------------------
const
  DEF_ = 0.1;
var
  v1 : TBLVector;
  v2 : TBLVector;
begin
  v1:=MakeBLVector(aBLPoint.b + DEF_, aBLPoint.L, aBLPoint.b-DEF_, aBLPoint.L);
  v2:=MakeBLVector(aBLPoint.b, aBLPoint.L-DEF_, aBLPoint.b, aBLPoint.L+DEF_);

  MAP_ADD_USER_LINE(clRed, 1, v1.Point1, v1.Point2);
  MAP_ADD_USER_LINE(clRed, 1, v2.Point1, v2.Point2);

  UserLayerDraw;
end;

// -------------------------------------------------------------------
procedure Tframe_MapView_base.Map1Click(Sender: TObject);
// -------------------------------------------------------------------
begin

 // if mem_Selection.IsEmpty then
  //  exit;

  if Assigned(FOnObjectClick) then
    FOnObjectClick(Self);

  //
 // CallNotifyEvents(MAP_EVENT_ON_SELECT_);
  g_EventManager.PostEvent_(et_MAP_EVENT_ON_SELECT, []);
end;

//--------------------------------------------------------------------
procedure Tframe_MapView_base.Map1DblClick(Sender: TObject);
//--------------------------------------------------------------------
begin
  if Assigned(FOnDblClick) then
    FOnDblClick(Self);
end;

{
procedure Tframe_MapView_base.CallNotifyEvents(aIndex: integer);
var i: integer;
begin
  Exit;


  for i := 0 to High(FEvents) do
    if Assigned(FEvents[i].Proc) then
      FEvents[i].Proc(Self, aIndex);
end;
}

procedure Tframe_MapView_base.cxTreeList2MouseWheel(Sender: TObject; Shift:
    TShiftState; WheelDelta: Integer; MousePos: TPoint; var Handled: Boolean);
begin
  Handled := FindVCLWindow( MousePos ) <> Sender;
end;

function Tframe_MapView_base.GetBLCursor: TBLPoint;
begin
  Result := FCursorPos;
end;

//--------------------------------------------------------------------
function Tframe_MapView_base.GetFeaturesByScreenBounds_ByObjectName(aObjectName: string):
    CMapXFeatures;
//--------------------------------------------------------------------
var
  vLayer:   CMapXLayer;
  sCaption: string;
  vMiRect:  CMapXRectangle;
  blPoint: TBLRect;
begin
  Result:= nil;

  vLayer:=GetLayerByObjectName(aObjectName);
  if vLayer=nil then
  begin
    sCaption:= aObjectName;
//    sCaption:= g_Obj.ItemByName[aObjectName].RootFolderName;

    g_Log.Add ( '���� '+sCaption+' �� ��������');
    exit;
  end;

  blPoint:= GetBounds();

  vMiRect := CoRectangle.Create;

  with blPoint do
    vMiRect.Set_(TopLeft.L, TopLeft.B, BottomRight.L, BottomRight.B);

  Result:= vLayer.SearchWithinRectangle (vMiRect, miSearchTypePartiallyWithin);
end;

// ---------------------------------------------------------------
procedure Tframe_MapView_base.MAP_ADD_LAYER_LINE(aLayerName: string; aBLVector:
    TBLVector; aColor, aStyle, aWidth: integer; aLabel: string);
// ---------------------------------------------------------------
var
  vCMapxLayer: CMapxLayer;
begin
  vCMapxLayer:=GetLayerByName (aLayerName);

  if Assigned(vCMapxLayer) then
      mapx_AddLineToLayer(Map, vCMapxLayer, ablVector, aColor, aStyle, aWidth, aLabel);

end;

// ---------------------------------------------------------------
procedure Tframe_MapView_base.Map_Add_user_Poly(var aBLPointArr:
    TBLPointArrayF; aColor: integer);
// ---------------------------------------------------------------
var
  i: integer;
begin
  Assert(aBLPointArr.Count<>0);

  for i:= 0 to aBLPointArr.Count-2 do
    MAP_ADD_USER_LINE (aColor, 1, aBLPointArr.Items[i], aBLPointArr.Items[i+1]);

  MAP_ADD_USER_LINE (aColor, 1, aBLPointArr.Items[aBLPointArr.Count-1], aBLPointArr.Items[0]);

end;

{
//-------------------------------------------------------------------
function Tframe_MapView_base.RegisterNotifyEvent(Sender: TObject; aEvent: TMapNotifyEvent): integer;
//-------------------------------------------------------------------
var i: integer;
begin
  Assert (Assigned(aEvent), 'function Tframe_MapView_base.RegisterNotifyEvent(aEventIndex: Integer; aEvent: TNotifyEvent): integer');

  for i := 0 to High(FEvents) do
     if not Assigned(FEvents[i].Proc) then
   begin
      FEvents[i].Proc  := aEvent;
      Result := i;
      Exit;
   end;

  Result := -1;
  raise Exception.Create('function Tframe_MapView_base.RegisterNotifyEvent(aEvent: TNotifyEvent): Integer;');

end;
 }

procedure Tframe_MapView_base.MAP_SHOW_BLRECT(aBLRect: TBLRect);
begin
  SetBoundsWithOffset (ablRect);
end;

procedure Tframe_MapView_base.MAP_SHOW_VECTOR(aBLVector: TBLVector);
begin
  AlignBoundsForBLVector (aBLVector);
end;

procedure Tframe_MapView_base.MAP_SHOW_POINT(aBLPoint: TBLPoint);
begin
  SetCenter(aBLPoint);
end;

procedure Tframe_MapView_base.MAP_ADD_USER_TEXT(aBLPoint: TBLPoint; aText:
    string);
begin
  FUserShapeList.Add(TMapUserShape.CreateCaption1 (clWhite, aBLPoint, aText, taLeftJustify));
end;

function Tframe_MapView_base.GetSelectedItemList: TMapSelectedItemList;
begin
  Result := SelectedItemList;
end;

procedure Tframe_MapView_base.MAP_CLEAR_USER_DRAWINGS;
begin
  FUserShapeList.Clear;
end;

{
procedure Tframe_MapView_base.UnRegisterSender(aProcIndex: integer);
begin
  FEvents[aProcIndex].Proc:=nil;
end;
}

procedure Tframe_MapView_base.Dlg_Info;
begin
  Init_IMapEngineX.Dlg_Feature_Info(FMap.DefaultInterface, BLCursor.B, BLCursor.L);

//  Tfrm_Map_tool_Info.ShowWindow(FMap, BLCursor);

end;

procedure Tframe_MapView_base.Dlg_Liner;
begin
  Tfrm_Map_tool_Liner.ShowWindow (NULL_POINT);  //Self,

end;

procedure Tframe_MapView_base.RunMapinfoWorkspace;
var
  S: string;

  oWorkspace: TWorkspace;
begin
  UpdateObjectMaps();

  s:=ChangeFileExt(FWorkspaceFileName,'.wor');

  u_Mapinfo_WOR.mapx_SaveToWOR (FMap, s);

(*

  oWorkspace := TWorkspace.Create;
  oWorkspace.LoadFromMap(FMap);
  oWorkspace.SaveToFile(s);
  FreeAndNil(oWorkspace);

  *)


  ShellExec(s,'');
end;

//--------------------------------------------------------------------
procedure Tframe_MapView_base.SelectObjectByID(aObjName: string; aBLPoint: TBLPoint; aID: Integer);
//--------------------------------------------------------------------
const
  FLD_GEONAME = 'GEONAME';

var
  vLayer: CMapXLayer;
  vFeatures: CMapXFeatures;
  vFeature: CMapXFeature;
  i: integer;
  s: string;
begin
  vLayer:=GetLayerByObjectName(aObjName);

  if Assigned(vLayer) then
  begin
    i := vLayer.Datasets.Count;

    if vLayer.Datasets.Count = 0 then
      Exit;

(*   if (vLayer.Datasets.Count = 0) then
     FMap.Datasets.Add (miDataSetLayer, vLayer, EmptyParam, EmptyParam,
                        EmptyParam, EmptyParam, EmptyParam, EmptyParam) ;
*)
    s := Format('%s = "%d"', [FLD_GEONAME, aID]);

//    vFeatures:=vLayer.Search('id='+IntToStr(aID), EmptyParam);
    vFeatures:=vLayer.Search(s, EmptyParam);

    if vFeatures.Count>0 then
    begin
      vFeature:=vFeatures.Item[1];

      vLayer.Selection.SelectByid (vFeature.FeatureID,  miSelectionNew );
    end;
  end;
end;

//----------------------------------------------------------
procedure Tframe_MapView_base.UpdateZoom;
//----------------------------------------------------------
//var
 // eScale: Double;

begin
//  StatusBar1.Panels[3].Text:=mapx_GetScaleStr(FMap);
  dxStatusBar1.Panels[DEF_SCALE_INDEX].Width:=100;
  dxStatusBar1.Panels[DEF_SCALE_INDEX].Text:=mapx_GetScaleStr(FMap);

  Map1.MapUnit   := miUnitKilometer;
  Map1.PaperUnit := miUnitCentimeter;

  dxStatusBar1.Panels[3].Text:= Format('%f ��', [FMap.Zoom]); // FloatToStr() + ' ��';


//  dxStatusBar1.Panels[DEF_SCALE_INDEX].Width:=100;
//  dxStatusBar1.Panels[DEF_SCALE_INDEX].Text:=mapx_GetScaleStr(FMap);


//  StatusBar1.Panels[3].Text:= Format('%f ��', [FMap.Zoom]);


 // eScale:=FMapEngine.GetScale();

//  StatusBar1.Panels[3].Text := Format('1 cm = %d m',[ Trunc(eScale)]);
  //StatusBar1.SimpleText := Format('1 : %n',[eScale]);

//  ed_Scale.Text:=FloatToStr(eScale);


end;

procedure Tframe_MapView_base.UpdateObjectMaps;
begin
  g_Log.Msg('UpdateObjectMaps');

//////////////  TdmMapView_tools.Init;
/////////////  dmMapView_tools.Exec (map);

end;

procedure Tframe_MapView_base.Layer_AddPolygon(aLayerName: string; aBLPoints:
    TBLPointArray; aColor, aStyle, aWidth: Integer; aLabel: string);
var
  vCMapxLayer: CMapxLayer;
begin
  vCMapxLayer:=GetLayerByName (aLayerName);

  if Assigned(vCMapxLayer) then
//      mapx_WritePolygon(Map, vCMapxLayer, aBLPoints, aColor,  aWidth,  aLabel); //, aColor, aStyle, aWidth, aLabel);
      mapx_WritePolygon(Map, vCMapxLayer, aBLPoints, aColor, aStyle, aWidth, aLabel); //, aColor, aStyle, aWidth, aLabel);

end;

// ---------------------------------------------------------------
procedure Tframe_MapView_base.LoadMaps_Update;
// ---------------------------------------------------------------
var
  I: Integer;
  iCount: Integer;

  s: string;

  sName: string;

 // ix: Integer;

  j: integer;

  vLayer: CMapXLayer;

  k: Integer;


  sObjectName: string;
begin

  i:=FMap.Layers.Count;


//  for I := 1 to FMap.Layers.Count do
  for I := FMap.Layers.Count downto 1 do
  begin
    vLayer:= FMap.Layers.Item [i];
  //  sFileName:=vLayer.Filespec;
    sName    :=vLayer.Name;

     if LeftStr(sName,1)='~' then
     begin
       sObjectName:=Copy(sName, 2, 100);

       vLayer := dmMapEngine_store.Open_Object_Map(sObjectName);

     end;


  end;

  dmMapEngine_store.RefreshObjectThemas_All;

end;

//------------------------------------------------------------
procedure Tframe_MapView_base.LoadMapsFromclass(aMapDesktopItem:  TMapDesktopItem);
//------------------------------------------------------------
var
  I: Integer;
  iCount: Integer;

  s: string;

  iZoom: Integer;

  sName,sFileName: string;

  ix: Integer;
  oMapItem: TMapItem;
  j: integer;
  iii: integer;
  iInd: Integer;

  vLayer: CMapXLayer;
  iIndex: integer;
  iIndex1: integer;
  iIndex_1: Integer;
  k: Integer;

  sLayerName: string;
  sObj: string;
begin

 // mapx_Show_Layers_Dlg(FMap);


  g_log.Msg('procedure Tframe_MapView_base.LoadMapsFromClass(aIsRebuildMaps: boolean; aMapDesktopItem: TMapDesktopItem);');


  iCount:=FMap.Layers.Count;


  //    if Copy(s,1,4) = 'tile' then
//      FMap.Layers.Remove(i);


//  for I := 1 to FMap.Layers.Count do
  for I := FMap.Layers.Count downto 1 do
  begin
    vLayer:=mapx_GetLayerByIndex_from_1(FMap, i);
    sFileName:=vLayer.Filespec;
    sName    :=vLayer.Name;

    if LowerCase(LeftStr(sName,Length('wms_tile'))) = 'wms_tile' then
      Continue;


    // delete
    if LeftStr(sName,1)<>'~' then
    begin
      oMapItem:=aMapDesktopItem.MapList.FindByFileName(sFileName);

      if not Assigned( oMapItem) then
      begin
        g_Log.SysMsg(Format('FMap.Layers.Remove %d ; filename: %s );', [i, sFileName]));

        FMap.Layers.Remove (i);

      //  if FMapList.Find(sFileName, iInd) then
       //    FMapList.Delete(iInd);


      //  if oMapItem.map_ID>0 then
       //     FMapList.Values[ IntToStr( oMapItem.map_ID) ]:='';


        Continue;
      end;

    end else
    begin
      sObj:=Copy(sName,2,100);
//      oLayersNameList.Add(Copy(sName,2,100));

       oMapItem:=aMapDesktopItem.MapList.FindByObjectName(sObj);

      if not Assigned( oMapItem ) then
//      if not Assigned(aMapDesktopItem.MapList.FindByObjectName(sObj)) then
    //  if not Assigned(aMapDesktopItem.ObjectMapList.FindByObjectName(sObj)) then
      begin
        g_Log.SysMsg(Format('Delete_Object_Map  filename: %s );', [sObj]));

        dmMapEngine_store.Delete_Object_Map(sObj);
      end;

    end;
  end;



//  mapx_Show_Layers_Dlg(FMap);

//  FMap.


//  ShowMessage(oSList.Text);
  iCount:=FMap.Layers.Count;

  if FMap.Layers.Count=0 then
    mapx_SetMapDefaultProjection_NumericCoordSys_KRASOVKSY42_simple(FMap);

  FMap.MapUnit  := miUnitKilometer;


  for i := 0 to aMapDesktopItem.MapList.Count-1 do
  //  if aMapDesktopItem.MapList[i].MapKind = mkObjectMap then
  begin
    oMapItem:=aMapDesktopItem.MapList[i];

    g_Log.SysMsg(Format('FileName: %s;', [oMapItem.FileName]));


    case oMapItem.MapKind of
      mkObjectMap:
        begin
          Assert(oMapItem.ObjectName<>'', 'oMapItem.ObjectName = '''' ');

          vLayer := dmMapEngine_store.Open_Object_Map(oMapItem.ObjectName);

     //     mapx_Show_Layers_Dlg(FMap);


       //   oMapItem.ApplyStyle(vLayer);
        //  Continue;
        end;

      mkGeoMap,
      mkCalcMap:
        begin
          if not FileExists (oMapItem.FileName) then
          begin
            g_log.Error('FileExists (oMapItem.FileName): '+ oMapItem.FileName);
            Continue;
          end;

          vLayer:=mapx_GetLayerByFileName(Fmap, oMapItem.FileName);

          if vLayer<>nil then
            vLayer.Visible := True;
           // ShowMessage('if vLayer<>nil then');


          if vLayer=nil then
          //�������� � ����� ������

            try

              Assert(FileExists(oMapItem.FileName));


              vLayer:= FMap.Layers.Add (oMapItem.FileName, FMap.Layers.Count+1);

           //   if oMapItem.map_ID>0 then
           //      FMapList.Values[ IntToStr( oMapItem.map_ID) ]:=oMapItem.FileName;


            except
              on E: Exception do
                Continue;
            end;

        end;

    end;


    oMapItem.ApplyStyle(vLayer);

  end;

// ---------------------------------------------------------------
  // mapx_Show_Layers_Dlg(FMap);

  for i := 0 to aMapDesktopItem.MapList.Count-1 do
  //  if aMapDesktopItem.MapList[i].MapKind <> mkObjectMap then
  begin
    oMapItem:=aMapDesktopItem.MapList[i];

    Assert(oMapItem.FileName<>'');

    iIndex:= GetMapLayerIndexByFileName_from_1(oMapItem.FileName);

    k:=FMap.Layers.Count;

    // -------------------------
    if (iIndex>=0) and (iIndex <> i+1) then
    begin
      g_Log.SysMsg(Format('---FMap.Layers.Move(%d, %d); file: %s ', [iIndex, i+1, sFileName]));

      FMap.Layers.Move(iIndex, i+1);
    end;
  end;

// ---------------------------------------------------------------

  dmMapEngine_store.RefreshObjectThemas_All;



  if (iCount=0) and (FMap.Layers.Count>0) then
    ZoomPlan;




//  mapx_Show_Layers_Dlg(FMap);

end;

//-------------------------------------------------------------------
procedure Tframe_MapView_base.RefreshStatusBar_new(aBLPoint: TBLPoint);
//-------------------------------------------------------------------
var
  iScale: integer;
//  dMapLengthInCm, dMapLengthInKm, dZoom, dMapX, dMapY: Double;

  bl: TblPoint;
  k: Integer;
  S: string;

 // xyPoint: TxyPoint;

const
  DEF_DELIMITER = '  |  ';

begin
  s:='';

  if btn_CK42.Checked then
  begin
    s:=s + geo_FormatBLPoint(aBLPoint) + ' (CK-42)' + DEF_DELIMITER;


    s:=s + Format('lat: %1.8f - lon: %1.8f',[aBLPoint.B,aBLPoint.L]) + DEF_DELIMITER;

 //   s:=s + geo_FormatBLPoint(aBLPoint) + ' (CK-42)' + DEF_DELIMITER;

  end;

  if btn_CK95.Checked then
  begin
    bl:= geo_Pulkovo42_to_CK_95 (aBLPoint);//dmOptions.GetGeoCoord);

    s:=s  + geo_FormatBLPoint(bl)+ ' (CK-95)' + DEF_DELIMITER;
  end;
  if btn_GCK.Checked then
  begin
    CK42_to_GSK_2011 (aBLPoint.B, aBLPoint.L, bl.B, bl.L  );

   // bl:= geo_Pulkovo42_to_WGS84 (aBLPoint);//dmOptions.GetGeoCoord);

    s:=s  + geo_FormatBLPoint(bl) + ' (�CK-2011)'+ DEF_DELIMITER;
  end;



  if btn_WGS.Checked then
  begin
    bl:= geo_Pulkovo42_to_WGS84 (aBLPoint);//dmOptions.GetGeoCoord);

    s:=s  + geo_FormatBLPoint(bl) + ' (WGS-84)'+ DEF_DELIMITER;
    s:=s + Format('lat: %1.8f - lon: %1.8f',[bl.B,bl.L]) + DEF_DELIMITER;

  end;




//  StatusBar1.SimpleText:=s;


  k:=dxStatusBar1.Canvas.TextWidth(s);

//  StatusBar1.Panels[0].Width:=k;
//  StatusBar1.Panels[0].Text:=s;
                   //   GetRelInfo_

  dxStatusBar1.Panels[0].Width:=k;
  dxStatusBar1.Panels[0].Text:=s;


  UpdateZoom();


  StatusBar_Update_HeightInfo (aBLPoint);

end;

procedure Tframe_MapView_base.btn_CK42Click(Sender: TObject);
begin
  with TMenuItem(Sender) do
    Checked:=not Checked;

//  ShowMessage(Sender.ClassName);
end;

procedure Tframe_MapView_base.Button2Click(Sender: TObject);
begin
  Map1.PropertyPage;
end;

procedure Tframe_MapView_base.Button3Click(Sender: TObject);
begin
  mapx_SetMapDefaultProjection_NumericCoordSys_KRASOVKSY42_simple(Map);

end;


function Tframe_MapView_base.GetLayerByIndex_1(aIndex: integer): CMapxLayer;
begin
  Result := Map1.Layers[aIndex];
end;

//-------------------------------------------------------------------
procedure Tframe_MapView_base.StatusBar_Update_HeightInfo(aBLPoint: TBLPoint);
//-------------------------------------------------------------------
var
  rec: Trel_Record;
  s: string;
  sClu: string;

begin

  if dmRel_Engine.GetRelInfo_(aBLPoint, rec) then
  begin
    sClu:= dmRel_Engine.GetClutterNameByCode (rec.Clutter_Code);

//    rec.Clutter_H

//    s:= Format('H�: %d, ��: %s, H ��:%d', [rec.Rel_H, sClu, rec.Clutter_H]);
    s:= Format('H�=%d | %s | H=%d', [rec.Rel_H, sClu, rec.Clutter_H]);

    dxStatusBar1.Panels[DEF_RELIEF_INDEX].Text:=s;



  //  k:=StatusBar1.Canvas.TextWidth(s);

  //  StatusBar1.Panels[DEF_SCALE_INDEX].Width:=k;

  end else
    dxStatusBar1.Panels[DEF_RELIEF_INDEX].Text:='';


end;


// ---------------------------------------------------------------
//TTileManagerEvents  new
// ---------------------------------------------------------------

// ---------------------------------------------------------------
procedure Tframe_MapView_base.WMS_Change_Layer(aName: string; aURL: string;
    aEPSG: word; aZ_max: byte);
// ---------------------------------------------------------------
begin
  FMapEngine.TilesClear;

//  WMS.Active:=True;
  WMS.LayerName:=aName;

  assert(Assigned(FTileManager));

  FTileManager.Set_WMS_Layer (aName, aURL, aEPSG, aZ_max);


//..function TTileManagerX.Set_WMS_layer(aName: WideString; aURL: string; aEPSG:
//    word; aZ_max: byte): HResult;


//  wms_Set_WMS_Layer (aName);

end;

// ---------------------------------------------------------------
procedure Tframe_MapView_base.WMS_Log;
// ---------------------------------------------------------------
begin
  assert(Assigned(FTileManager));

  FTileManager.Dlg_Log;// Exec(DEF_Dlg_Log);

//  wms_Set_WMS_Layer (aName);

end;

// ---------------------------------------------------------------
procedure Tframe_MapView_base.WMS_Update;
// ---------------------------------------------------------------

begin
//  if not cb_Map1MapViewChanged.Checked then
 //   exit;


  if not WMS.Active then
    exit;

  g_Log.Add('procedure Tframe_MapView_base.WMS_Update;');

///  ShowMessage('WMS_Update');


   Map1.OnMapViewChanged:=nil;

   FTileManager.Load_CMap();
   WMS.Z:=FTileManager.Get_Z;
//   FTileManager.Set_Z(0);



//   Timer1.Interval:=10;
//   Timer1.Enabled:=True;  // set  Map1.OnMapViewChanged:=nil;

   mapx_SetMapDefaultProjection_NumericCoordSys_KRASOVKSY42_WMS(Map1);

   Map1.OnMapViewChanged:=Map1MapViewChanged;

end;

//---------------------------------------------------------------------------
procedure Tframe_MapView_base.Map1MapViewChanged(Sender: TObject);
//---------------------------------------------------------------------------
var
  blRect: TBLRect;
  sBounds: string;

const
  DEF_MAX_ZOOM = 20000;

begin
  if FDisableMapViewChanged then
    Exit;

  g_Log.Add('procedure Tframe_MapView_base.Map1MapViewChanged(Sender: TObject);');

   // test --------------------
  blRect:=FMapEngine.GetBounds();
  sBounds:= Format('%1.6f %1.6f %1.6f %1.6f', [blRect.TopLeft.B, blRect.TopLeft.L, blRect.BottomRight.B, blRect.BottomRight.L]);

  g_Log.Add('Bound - ' + sBounds);

  if FBounds_old = sBounds then
  begin
    g_Log.Add('���������� ���������.');

    Exit;
  end;

  FBounds_old := sBounds;


//  if FUpdated then
//    Exit;


//  Cursor:=crHourGlass;

  Assert(Assigned(FMap), 'Value not assigned');

//  g_Log.Add('���������� ���������.');

  debug (Format ('FMap.Zoom : %1.6f', [FMap.Zoom]));


  if FMap.Zoom > DEF_MAX_ZOOM then
    FMap.Zoom:= DEF_MAX_ZOOM;

//  if FMap.Zoom < 1 then
//    FMap.Zoom:= 1;

  UpdateZoom;

//     WMS_Update;



  if Assigned(FOnMapViewChanged) then
    FOnMapViewChanged(nil);
//     Map1MapViewChanged(Sender: TObject);



  if WMS.Active then
    WMS_Update;

//     Timer_WMS.Enabled:=True;

//  Cursor:=crDEfault;

end;


procedure Tframe_MapView_base.MapUpdateStop;
begin
  FDisableMapViewChanged :=True;

end;


procedure Tframe_MapView_base.MapUpdateStart;
begin
  FDisableMapViewChanged :=False;

end;




end.



 {


procedure Tframe_MapView_base.Button5Click(Sender: TObject);
begin
//  mapx_SetMapDefaultProjection_DisplayCoordSys_KRASOVKSY42(Map);

end;

