unit u_MapView_engine;

interface
 uses
   Controls,Graphics,Types,Classes,Forms,
   MapXLib_TLB,

   u_Geo;

type
  TMapView_Engine111111111111 = class(TObject)
  private
    FMap: TMap;
  private
    FFirstPointClicked: boolean;
    FFirstPointPos: TBLpoint;

    FMoveBitmap: TBitmap;
    FOldMousePos: TPoint;
    CursorPosXY: TPoint;

    FCursorPos: TBLPoint;

    FLeftButtonClicked : Boolean;
    FRightButtonPressed: boolean;


    procedure PaintBitmapWhileMoving;
    procedure SetMap(const Value: TMap);

  public
    procedure Map1MouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y:
        Integer);
    procedure Map1MouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y:
        Integer);

    property Map: TMap read FMap write SetMap;
  end;

implementation

uses
   Windows;


// -------------------------------------------------------------------
procedure TMapView_Engine111111111111.Map1MouseDown(Sender: TObject; Button: TMouseButton; Shift:
    TShiftState; X, Y: Integer);
// -------------------------------------------------------------------

    //------------------------------------------------------------
    procedure DoOnMouseDown ();
    //------------------------------------------------------------
    begin
      FFirstPointClicked:=True;
      FFirstPointPos:=FCursorPos;

    (*  if Assigned(FOnMouseDown) then
        FOnMouseDown (FCursorPos);*)
    end;


var
  sObjName: Integer;
  i, iID  : Integer;
  sAction: String;
begin
{if FIsTempPan then begin
  //  SetWindowTool (toolObject);
    FIsTempPan:=False;
 end;  }
  FLeftButtonClicked:= (not (ssRight in Shift)) AND (ssLeft in Shift);

  FRightButtonPressed:= ((ssRight in Shift)) AND (not (ssLeft in Shift));

{
  if (not FIsTempPan) and (ssRight in Shift) then begin
    SetWindowTool (toolPan);
    FIsTempPan:=True;
  end else

  if (FIsTempPan) and (ssRight in Shift) then
  begin
    if Assigned(FindForm( Tfrm_Map_Tool_Liner.ClassName)) then
      SetWindowTool(toolLiner)
    else


    SetWindowTool(toolObject);

    FIsTempPan:=False;
  end;}


  if FRightButtonPressed then
  begin
    //CursorSet (crHandPoint);
    FMap.AutoRedraw:= false;

    FOldMousePos.X:= X;
    FOldMousePos.Y:= Y;
//    FOldMousePos:= CursorPosXY;

    FMoveBitmap.Width:= Fmap.Width;
    FMoveBitmap.Height:= Fmap.Height;

    FMap.PaintTo (FMoveBitmap.Canvas.Handle, 0, 0);
  end;


  DoOnMouseDown();
end;


//--------------------------------------------------------------------
// ��������� �������� ����� �� ��������� ������ ������ ����
procedure TMapView_Engine111111111111.PaintBitmapWhileMoving;
//--------------------------------------------------------------------
var
  DC: HDC;
  brush: HBRUSH;
  r1, r2: TRect;
  iOffsetX, iOffsetY: Integer;
begin
  if FRightButtonPressed then
  begin
    iOffsetX:= CursorPosXY.X - FOldMousePos.X;
    iOffsetY:= CursorPosXY.Y - FOldMousePos.Y;

    if (iOffsetX=0) and (iOffsetY=0) then
      exit;

    Screen.Cursor:=crHandPoint;

//    CursorSet (crHandPoint);

    DC:= GetDC(FMap.Handle);
    brush:= CreateSolidBrush(clWhite);

    r1:= FMap.ClientRect;
    r2:= r1;
    if iOffsetY<0 then
      r1.Top:= r1.bottom+iOffsetY
    else
      r1.bottom:= r1.top+iOffsetY;

    if iOffsetX<0 then
      r2.left:= r2.right+iOffsetX
    else
      r2.right:= r2.left+iOffsetX;

    FillRect(DC, r1, brush);
    FillRect(DC, r2, brush);

    DeleteObject(brush);
    BitBlt(DC, iOffsetX, iOffsetY, Fmap.Width, Fmap.Height, FMoveBitmap.Canvas.Handle, 0, 0, SRCCOPY);
    ReleaseDC(Fmap.Handle, DC);
  end;
end;



//-------------------------------------------------------------------
procedure TMapView_Engine111111111111.Map1MouseUp(Sender: TObject; Button: TMouseButton; Shift:
    TShiftState; X, Y: Integer);
//-------------------------------------------------------------------
var
  iOffsetX, iOffsetY: Integer;
begin
  inherited;

  if FRightButtonPressed then
  begin
    Screen.Cursor:=crDefault;

    if (FOldMousePos.X = X) and (FOldMousePos.Y = Y) then
    begin
      FRightButtonPressed:= false;
//      FLeftButtonPressed:= false;
      Fmap.AutoRedraw:= true;
 //////////     PopupMenu1.Popup(Mouse.CursorPos.x, Mouse.CursorPos.y);

      exit;
    end;

    iOffsetX:= CursorPosXY.X - FOldMousePos.X;
    iOffsetY:= CursorPosXY.Y - FOldMousePos.Y;

(*    if (iOffsetX=0) and (iOffsetY=0) then begin
      FRightButtonPressed:= false;
      exit;
    end;*)

    //��� ���������� ������ ���� �������� �����
    Fmap.Pan (-iOffsetX, iOffsetY);

    FRightButtonPressed:= false;
    Fmap.AutoRedraw:= true;
    Fmap.Refresh();
  end;

//  FLeftButtonPressed:= False;

end;

procedure TMapView_Engine111111111111.SetMap(const Value: TMap);
begin
  FMap := Value;
end;


end.
