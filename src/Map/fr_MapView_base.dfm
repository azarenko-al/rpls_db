object frame_MapView_base: Tframe_MapView_base
  Left = 969
  Top = 292
  Width = 793
  Height = 474
  Caption = 'frame_MapView_base'
  Color = clBtnFace
  ParentFont = True
  KeyPreview = True
  OldCreateOrder = False
  Position = poDefault
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnKeyDown = FormKeyDown
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 777
    Height = 53
    Align = alTop
    BevelOuter = bvLowered
    Color = clAppWorkSpace
    TabOrder = 0
    Visible = False
  end
  object Map1: TMap
    Left = 0
    Top = 145
    Width = 777
    Height = 72
    ParentColor = False
    Align = alTop
    TabOrder = 1
    OnDblClick = Map1DblClick
    OnClick = Map1Click
    OnDrawUserLayer = Map1UserLayerDraw
    OnPolyToolUsed = Map1PolyToolUsed
    ControlData = {
      2CA107004E50000071070000010000000F0000FFFFFEFF0D470065006F004400
      69006300740069006F006E0061007200790000FFFEFF3745006D007000740079
      002000470065006F007300650074004E0061006D00650020007B003900410039
      00410043003200460034002D0038003300370035002D0034003400640031002D
      0042004300450042002D00340037003600410045003900380036004600310039
      0030007D00FFFEFF001E00FFFEFF000600010A000000000000FFFEFF00500001
      0100000001F4010000050000800C0000000014000000000002000E00E8030005
      00000100000000FFFFFF000000000000000001370000000000FFFFFF00000000
      0000000352E30B918FCE119DE300AA004BB85101CC00009001DC7C0100054172
      69616C000352E30B918FCE119DE300AA004BB851010200009001A42C02000B4D
      61702053796D626F6C730000000000000001000100FFFFFF000200FFFFFF0000
      0000000001000000010000FFFEFF3245006D0070007400790020005400690074
      006C00650020007B00300031004100390035003000340042002D004300450031
      0033002D0034003400310035002D0041003500410030002D0035003100440038
      00430032004600310035003200300034007D0000000000FFFFFF000100000000
      000000000000000000000000000000000000000352E30B918FCE119DE300AA00
      4BB85101CC00009001348C030005417269616C000352E30B918FCE119DE300AA
      004BB85101CC00009001348C030005417269616C000000000000000000000000
      00000000000000000000000000000000000000000000000000B8AD4000000000
      004051400100000100001801000001000000FFFFFFFF1C000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000020000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000008076C000000000008056C0000000000080764000000000008056400000
      0000010000001801000001000000FFFFFFFF1C00000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000200
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000080
      76C000000000008056C000000000008076400000000000805640000000000100
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000}
  end
  object Panel2: TPanel
    Left = 0
    Top = 53
    Width = 777
    Height = 92
    Align = alTop
    TabOrder = 2
    Visible = False
    object Button4: TButton
      Left = 13
      Top = 7
      Width = 79
      Height = 20
      Caption = 'Dlg'
      TabOrder = 0
    end
    object Button1: TButton
      Left = 111
      Top = 7
      Width = 72
      Height = 20
      Caption = 'Button1'
      TabOrder = 1
      OnClick = Button1Click
    end
    object Button2: TButton
      Left = 200
      Top = 7
      Width = 81
      Height = 21
      Caption = 'PropertyPage;'
      TabOrder = 2
      OnClick = Button2Click
    end
    object Button3: TButton
      Left = 304
      Top = 8
      Width = 433
      Height = 25
      Caption = 'mapx_SetMapDefaultProjection_NumericCoordSys_KRASOVKSY42'
      TabOrder = 3
      OnClick = Button3Click
    end
    object Button5: TButton
      Left = 304
      Top = 40
      Width = 433
      Height = 25
      Caption = 'mapx_SetMapDefaultProjection_DisplayCoordSys_KRASOVKSY42'
      TabOrder = 4
    end
  end
  object dxStatusBar1: TdxStatusBar
    Left = 0
    Top = 418
    Width = 777
    Height = 17
    Panels = <
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
      end
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
      end
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Width = 100
      end
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Width = 100
      end>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    PopupMenu = PopupMenu_StatusBar
  end
  object OpenDialog1: TOpenDialog
    Left = 20
    Top = 4
  end
  object PopupMenu1: TPopupMenu
    Left = 56
    Top = 4
  end
  object FormStorage1: TFormStorage
    IniFileName = 'Software\Onega\MapBase'
    StoredProps.Strings = (
      'btn_CK42.Checked'
      'btn_CK95.Checked'
      'btn_GCK.Checked'
      'btn_WGS.Checked')
    StoredValues = <
      item
        Name = 'qqq'
      end
      item
      end
      item
      end>
    Left = 400
    Top = 320
  end
  object PopupMenu_StatusBar: TPopupMenu
    Left = 400
    Top = 256
    object btn_CK42: TMenuItem
      Caption = 'CK 42'
      Checked = True
      OnClick = btn_CK42Click
    end
    object btn_CK95: TMenuItem
      Caption = 'CK 95'
      Checked = True
      OnClick = btn_CK42Click
    end
    object btn_GCK: TMenuItem
      Caption = #1043#1057#1050' 2011'
      OnClick = btn_CK42Click
    end
    object btn_WGS: TMenuItem
      Caption = 'WGS 84'
      OnClick = btn_CK42Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
  end
  object PopupMenu_map: TPopupMenu
    Left = 40
    Top = 256
    object actLegend1: TMenuItem
      Action = act_Legend_Show
    end
    object actLegendHide1: TMenuItem
      Action = act_Legend_Hide
    end
  end
  object ActionList1: TActionList
    Left = 168
    Top = 256
    object act_Legend_Show: TAction
      Caption = 'Show '#1051#1077#1075#1072#1085#1076#1072
      OnExecute = act_Legend_ShowExecute
    end
    object act_Legend_Hide: TAction
      Caption = 'Hide '#1051#1077#1075#1072#1085#1076#1072
      OnExecute = act_Legend_ShowExecute
    end
  end
  object Timer_WMS: TTimer
    Enabled = False
    Interval = 100
    Left = 280
    Top = 256
  end
end
