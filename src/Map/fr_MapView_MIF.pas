unit fr_MapView_MIF;

interface
//{$I ver.inc}


uses
  Classes, ExtCtrls, ADODB, DB,  Dialogs, StdCtrls, ToolWin, ComCtrls,
  Controls, Forms, Variants, Menus, OleCtrls,

  MapXLib_TLB,

  u_Map_engine,

  f_Map_tool_Liner,

  fr_MapView_base,

  u_mapX,
  u_classes,

//  d_Prop_search,                                                               

  u_GEO,

  rxToolEdit, Mask, rxPlacemnt, cxControls, dxStatusBar, ActnList, AppEvnts,
  cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, dxSkinsCore,
  dxSkinsDefaultPainters
  ;


type

  Tframe_MapView_MIF = class(Tframe_MapView_base)
    Panel4: TPanel;
    bSetDefault: TButton;                                                                                                
    bSave: TButton; //, IMapViewX)
    procedure FormCreate(Sender: TObject);                                                                                     
//    procedure FormDestroy(Sender: TObject);
//    procedure bSaveClick(Sender: TObject);

    procedure Map1ToolUsed (Sender: TObject; ToolNum: Smallint; X1, Y1, X2, Y2, Distance: Double; Shift,
           Ctrl: WordBool; var EnableDefault: WordBool);

    procedure PopupMenu1Popup(Sender: TObject);
//    procedure bSetDefaultClick(Sender: TObject);
  private


  protected
    procedure SetWindowTool (Value: TMapWindowTool);    override;

  public

  end;


//==================================================================
// implementation
//==================================================================

implementation {$R *.dfm}



procedure Tframe_MapView_MIF.FormCreate(Sender: TObject);
//--------------------------------------------------------------------
begin
  inherited;                                       

  FMap.OnToolUsed:=Map1ToolUsed;

  SetDefaultTool;

  Clear;
end;


//--------------------------------------------------------------------
procedure Tframe_MapView_MIF.SetWindowTool (Value: TMapWindowTool);
//--------------------------------------------------------------------
begin
  inherited;

  with FMap do
    case Value of
      toolObject  : CurrentTool:=miSelectTool;

      toolLiner   : begin CurrentTool:=CUSTOM_LINER; end; //FPreviousTool:= Value;
      toolInfo    : begin CurrentTool:=CUSTOM_INFO_TOOL; end; //FPreviousTool:= Value;


      toolPoint   : CurrentTool:=CUSTOM_POINT_TOOL;
      toolVector  : CurrentTool:=CUSTOM_LINE_TOOL; //miAddLineTool; //CUSTOM_LINE_TOOL;

      toolPan     : CurrentTool:=miPanTool;
      toolRect    : CurrentTool:=CUSTOM_RECT_TOOL; //miRectSelectTool;
      toolRing    : CurrentTool:=CUSTOM_RING_TOOL;
      toolPoly    : CurrentTool:=CUSTOM_POLYLINE_TOOL;

      toolSelect  : CurrentTool:=CUSTOM_SELECT_TOOL;

      toolSelectRect:     CurrentTool:= miRectSelectTool;
      toolSelectRadius:   CurrentTool:= miRadiusSelectTool;
      toolSelectPolygon:  CurrentTool:= miPolygonSelectTool;


      toolZoomIn  : CurrentTool:=miZoomInTool;
      toolZoomOut : CurrentTool:=miZoomOutTool;

      // -------------------------------------------------------------------

    end;
end;
      
//--------------------------------------------------------------------
procedure Tframe_MapView_MIF.Map1ToolUsed (Sender: TObject; ToolNum: Smallint;
    X1, Y1, X2, Y2, Distance: Double; Shift, Ctrl: WordBool; var EnableDefault: WordBool);
//--------------------------------------------------------------------
var
  s: string;
  vPt:    CMapXPoint;
  vFeature: CMapXFeature;
  vFeatures: CMapXFeatures;
  vLayer: CMapxLayer;

  blVector: TBLVector;
  oBLPoint: TBLPoint;
  blRect: TBLRect;

  i, j, iID: integer;
  sDist, sAction: string;
  fDistance,fAzimuth: double;

  //vLayer: CMapXLayer;

begin
  case ToolNum of
    CUSTOM_LINER: begin
        DoOnPoint(MakeBLPoint(Y1,X1));

      //  Dlg_Liner();              Self,
        Tfrm_Map_tool_Liner.ShowWindow ( MakeBLPoint(Y1,X1));///, GetBounds);
    end;

    CUSTOM_INFO_TOOL: begin

//        SearchAtPoint(MakeBLPoint(Y1,X1));
//        Map1.Layers.LayersDlg(null, null);
     //   if Map1.Zoom >= 150 then
    //      exit;
        Dlg_Info();
//        Tfrm_Map_tool_Info.ShowWindow(Map1, MakeBLPoint(Y1,X1));
    end;

    CUSTOM_RING_TOOL: begin
        blVector:=MakeBLVector (Y1,X1,Y2,X2);
        fDistance:=geo_Distance_m (blVector);
        if fDistance>1 then
          DoOnRing (MakeBLPoint(Y1,X1), fDistance);
      end;

    CUSTOM_RECT_TOOL :
        DoOnRect (MakeBLRect(Y1,X1,Y2,X2));

    CUSTOM_POINT_TOOL:
        DoOnPoint (MakeBLPoint(Y1,X1));

    miSelectTool: begin
        SearchAtPoint (MakeBLPoint(Y1,X1));
        DoOnPoint (MakeBLPoint(Y1,X1));
      end;

    CUSTOM_LINE_TOOL: begin
        DoOnVector (MakeBLVector(Y1,X1,Y2,X2));
      end;


    // -------------------------------------------------------------------
    CUSTOM_SELECT_TOOL:
    // -------------------------------------------------------------------
    begin
    //  with MakeBLRect(Y1, X1, Y2, X2) do
    //  begin
//        SelectedLayerList.Clear;
        ClearSelection;

        for i := 1 to FMap.Layers.Count do
        begin
          vLayer := mapx_GetLayerByIndex_from_1(FMap, i);
       //   with Map1.Layers.Item[i] do
       //   with mapx_GetLayerByIndex_from_1(FMap, i) do
            if (vLayer.Visible) and (vLayer.Selectable) and (vLayer.Type_=miLayerTypeNormal) then
            //  if mem_Maps.Locate(FLD_FILENAME, Filespec, []) then
              begin
                blRect:=MakeBLRect(Y1, X1, Y2, X2);

                with blRect do
                  vLayer.Selection.SelectByRectangle(TopLeft.L, TopLeft.B, BottomRight.L, BottomRight.B, miSelectionNew);
              end;
        end;
    end;

  end; //case
end;


//--------------------------------------------------------------------
procedure Tframe_MapView_MIF.PopupMenu1Popup(Sender: TObject);
//--------------------------------------------------------------------
begin
  PopupMenu1.Items.Clear;
end;


end.



