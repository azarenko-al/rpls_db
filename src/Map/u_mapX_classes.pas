
unit u_mapX_classes;


interface




type
  

 //-------------------------------------------------------------------
  TMapItem = class
  //-------------------------------------------------------------------
  public    
//    KEY: Integer;
//    ExCode: Integer;
//    Local: Integer;
//
    FileName: string;   

    IsTile : boolean;
    
    Tile: record
            X,Y,Z : integer;
          end;

//    procedure LoadFromDataset(aDataset: TDataset);

  end;




implementation



end.


{
// ---------------------------------------------------------------
procedure TMapViewList1111111111111111.Get_outside_Rect(aRect: TTileBounds; aList: TStrings);
// ---------------------------------------------------------------
var
  I: Integer;
begin
  aList.Clear;


  for I := 0 to Count-1 do
    if Items[i].IsTile then
      if 
        (Items[i].Tile.Z <> aRect.Z ) or

        (Items[i].Tile.X < aRect.X_min ) or
        (Items[i].Tile.X > aRect.X_max ) or
        
        (Items[i].Tile.Y < aRect.Y_min ) or
        (Items[i].Tile.Y > aRect.Y_max ) 
      then
       aList.Add(Items[i].FileName);

end;







  //-------------------------------------------------------------------
  TMapViewList1111111111111111 = class(TStringList)
  //-------------------------------------------------------------------
  private
    function GetItems(Index: Integer): TMapItem;
  public
    function Add_Map(aFileName: string): TMapItem;    
    function Add_Tile(aFileName: string; aX, aY, aZ: integer): TMapItem;
    procedure Del_List(aList: TStrings);
    procedure Del_Map(aFileName: string);
    
    procedure Get_outside_Rect(aRect: TTileBounds; aList: TStrings);

    property Items[Index: Integer]: TMapItem read GetItems; default; 
  end;



  
// ---------------------------------------------------------------
function TMapViewList1111111111111111.Add_Map(aFileName: string): TMapItem;
// ---------------------------------------------------------------
var
  k: Integer;
begin
  Result := TMapItem.Create;
  Result.FileNAME:=aFileName;
  
  k:=AddObject(aFileName, Result);

//  Result := TMapItem (Add(aFileName));    

end;


// ---------------------------------------------------------------
procedure TMapViewList1111111111111111.Del_Map(aFileName: string);
// ---------------------------------------------------------------
var
  k: Integer;
begin
  if Find(aFileName,k) then
    Delete(k);
  
//  k:=IndexOfName(aFileName);
//  if k>0 then
//    Delete(k);

//  Result := TMapItem (Add(aFileName));    

end;


// ---------------------------------------------------------------
function TMapViewList1111111111111111.Add_Tile(aFileName: string; aX, aY, aZ: integer):
    TMapItem;
// ---------------------------------------------------------------
begin
  Assert(aZ<30);

  Result := Add_Map(aFileName);

  Result.IsTile:=True;
  
  Result.Tile.X:=aX;
  Result.Tile.Y:=aY;  
  Result.Tile.Z:=aZ;  
  
end;


function TMapViewList1111111111111111.GetItems(Index: Integer): TMapItem;
begin
  Result := TMapItem(inherited Objects[Index]);
end;
          

// ---------------------------------------------------------------
procedure TMapViewList1111111111111111.Del_List(aList: TStrings);
// ---------------------------------------------------------------
var
  I: Integer;
begin
  for I := 0 to aList.Count-1 do
    Del_Map(aList[i]);

end;



