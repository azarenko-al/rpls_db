inherited frm_Map_Matrix_Monitor: Tfrm_Map_Matrix_Monitor
  Left = 2004
  Top = 344
  Width = 661
  Height = 415
  BorderIcons = [biSystemMenu, biMinimize]
  Caption = 'frm_Map_Matrix_Monitor'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited ToolBar1: TToolBar
    Width = 653
  end
  inherited StatusBar1: TStatusBar
    Top = 368
    Width = 653
    Panels = <
      item
        Width = 200
      end
      item
        Width = 50
      end>
  end
  object cxGrid1: TcxGrid [2]
    Left = 0
    Top = 253
    Width = 653
    Height = 115
    Align = alBottom
    TabOrder = 2
    LookAndFeel.Kind = lfFlat
    object cxGrid1DBTableView1: TcxGridDBTableView
      NavigatorButtons.ConfirmDelete = False
      DataController.DataSource = DataSource1
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsBehavior.ImmediateEditor = False
      OptionsCustomize.ColumnFiltering = False
      OptionsCustomize.ColumnGrouping = False
      OptionsCustomize.ColumnsQuickCustomization = True
      OptionsView.GroupByBox = False
      object col_Name: TcxGridDBColumn
        Caption = #1060#1072#1081#1083
        DataBinding.FieldName = 'name'
        Width = 246
      end
      object col_rel_h: TcxGridDBColumn
        Caption = #1053
        DataBinding.FieldName = 'rel_h'
        Width = 43
      end
      object col_Clutter_H: TcxGridDBColumn
        Caption = 'H '#1084#1087
        DataBinding.FieldName = 'Clutter_H'
      end
      object col_Clutter_Name: TcxGridDBColumn
        Caption = #1058#1080#1087' '#1052#1055
        DataBinding.FieldName = 'Clutter_Code'
        PropertiesClassName = 'TcxLookupComboBoxProperties'
        Properties.KeyFieldNames = 'code'
        Properties.ListColumns = <
          item
            FieldName = 'name'
          end>
        Properties.ListSource = ds_ClutterModelType
        Width = 144
      end
      object col_Clutter_Code: TcxGridDBColumn
        Caption = #1050#1086#1076
        DataBinding.FieldName = 'Clutter_Code'
        Width = 39
      end
      object col_ID: TcxGridDBColumn
        DataBinding.FieldName = 'id'
        Visible = False
        Width = 103
      end
      object col_Zone: TcxGridDBColumn
        Caption = 'Zone'
        DataBinding.FieldName = 'zone'
        Width = 32
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1DBTableView1
    end
  end
  object PopupMenu1: TPopupMenu
    Left = 160
    Top = 8
    object N1: TMenuItem
      Action = act_Opened_matrixes
    end
  end
  object ActionList1: TActionList
    Left = 88
    Top = 8
    object act_Opened_matrixes: TAction
      Caption = #1054#1090#1082#1088#1099#1090#1099#1077' '#1084#1072#1090#1088#1080#1094#1099'...'
      OnExecute = act_Opened_matrixesExecute
    end
    object Action1: TAction
      Caption = 'Action1'
    end
    object act_SaveFramesToFile: TAction
      Caption = 'act_SaveFramesToFile'
    end
  end
  object ADOStoredProc1: TADOStoredProc
    Connection = ADOConnection1
    CursorType = ctStatic
    LockType = ltBatchOptimistic
    ProcedureName = 'sp_Relief_Select'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@PROJECT_ID'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 1217
      end
      item
        Name = '@CHECKED'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = True
      end>
    Left = 512
    Top = 80
    object ADOStoredProc1id: TAutoIncField
      FieldName = 'id'
      ReadOnly = True
    end
    object ADOStoredProc1relief_id: TIntegerField
      FieldName = 'relief_id'
    end
    object ADOStoredProc1checked: TBooleanField
      FieldName = 'checked'
    end
    object ADOStoredProc1host: TStringField
      FieldName = 'host'
      Size = 100
    end
    object ADOStoredProc1name: TStringField
      FieldName = 'name'
      Size = 250
    end
    object ADOStoredProc1step_x: TFloatField
      FieldName = 'step_x'
    end
    object ADOStoredProc1step_y: TFloatField
      FieldName = 'step_y'
    end
    object ADOStoredProc1step_lat: TFloatField
      FieldName = 'step_lat'
    end
    object ADOStoredProc1step_lon: TFloatField
      FieldName = 'step_lon'
    end
    object ADOStoredProc1lat_min: TFloatField
      FieldName = 'lat_min'
    end
    object ADOStoredProc1lat_max: TFloatField
      FieldName = 'lat_max'
    end
    object ADOStoredProc1lon_min: TFloatField
      FieldName = 'lon_min'
    end
    object ADOStoredProc1lon_max: TFloatField
      FieldName = 'lon_max'
    end
    object ADOStoredProc1x_min: TFloatField
      FieldName = 'x_min'
    end
    object ADOStoredProc1y_min: TFloatField
      FieldName = 'y_min'
    end
    object ADOStoredProc1x_max: TFloatField
      FieldName = 'x_max'
    end
    object ADOStoredProc1y_max: TFloatField
      FieldName = 'y_max'
    end
    object ADOStoredProc1h: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'rel_h'
      Calculated = True
    end
    object ADOStoredProc1Clutter_Code: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'Clutter_Code'
      Calculated = True
    end
    object ADOStoredProc1Clutter_H: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'Clutter_H'
      Calculated = True
    end
    object ADOStoredProc1zone: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'zone'
      Calculated = True
    end
  end
  object DataSource1: TDataSource
    DataSet = ADOStoredProc1
    Left = 512
    Top = 128
  end
  object ADOConnection1: TADOConnection
    ConnectionString = 
      'Provider=MSDASQL.1;Persist Security Info=False;User ID=sa;Data S' +
      'ource=onega_1'
    LoginPrompt = False
    Provider = 'MSDASQL.1'
    Left = 264
    Top = 80
  end
  object t_ClutterModelType: TADOTable
    Connection = ADOConnection1
    CursorType = ctStatic
    LockType = ltBatchOptimistic
    TableName = 'ClutterModelType'
    Left = 392
    Top = 80
  end
  object ds_ClutterModelType: TDataSource
    DataSet = t_ClutterModelType
    Left = 392
    Top = 128
  end
end
