object frm_MapView_test: Tfrm_MapView_test
  Left = 275
  Top = 266
  Width = 568
  Height = 553
  Caption = 'frm_MapView_test'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Scaled = False
  OnClose = FormClose
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl1: TPageControl
    Left = 0
    Top = 0
    Width = 529
    Height = 519
    ActivePage = TabSheet2
    Align = alLeft
    Anchors = [akLeft, akTop, akRight, akBottom]
    TabOrder = 0
    object TabSheet1: TTabSheet
      Caption = 'TabSheet1'
      object DBGrid_Selection: TDBGrid
        Left = 0
        Top = 0
        Width = 521
        Height = 209
        Align = alTop
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'filename'
            Width = 156
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'id'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'objname'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'name'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'guid'
            Visible = True
          end>
      end
      object dxDBInspector1: TdxDBInspector
        Left = 0
        Top = 209
        Width = 521
        Height = 391
        Align = alClient
        TabOrder = 1
        DividerPos = 312
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'TabSheet2'
      ImageIndex = 1
      object DBGrid_Maps: TDBGrid
        Left = 0
        Top = 0
        Width = 521
        Height = 193
        Align = alTop
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
      end
      object dxDBInspector2: TdxDBInspector
        Left = 0
        Top = 193
        Width = 521
        Height = 298
        Align = alClient
        TabOrder = 1
        DividerPos = 330
      end
    end
  end
  object ds_Maps: TDataSource
    Left = 148
    Top = 268
  end
  object ds_Selection: TDataSource
    Left = 220
    Top = 268
  end
end
