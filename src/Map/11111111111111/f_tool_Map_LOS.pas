unit f_tool_Map_LOS;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, rxPlacemnt,  ToolWin, Db, dxmdaset, ADODB,   Menus, ActnList,
  cxGridLevel, cxClasses, cxControls, cxGridCustomView, Variants,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid, Grids,

  DBGrids, StdCtrls, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters,
  cxStyles, dxSkinsCore, dxSkinsDefaultPainters, dxSkinscxPCPainter,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxDBData,


//  dm_Onega_DB_data,
  f_map_Tool_base,

  u_Storage,

 // u_vars,

 // dm_RelMatrixFile_export_import,

  u_cx,

//  I_rel_Matrix1,

  I_MapAct,
  I_MapView,

 // dm_Main,

  u_func,
  u_Geo,
  u_db,

  u_const_db,

//  dm_Rel_Engine,

  cxDBLookupComboBox

  ;

type
  Tfrm_Map_LOS = class(Tfrm_Map_Tool_base)
    ActionList1: TActionList;
    Action1: TAction;
    cxGrid1: TcxGrid;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1TableView1: TcxGridTableView;
    cxGrid1TableView1Column1: TcxGridColumn;
    cxGrid1TableView1Column2: TcxGridColumn;
    procedure FormDestroy(Sender: TObject);
//    procedure Action2Execute(Sender: TObject);
    procedure act_Opened_matrixesExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
//    FRegPath : string;

    procedure UpdateData;

  protected
    procedure SetMapCursor (aBLCursor: TBLPoint); override;

  public
   // procedure DrawTempLayer;
    class procedure ShowForm(aBLRect: TBLRect);
  end;



//====================================================================
implementation {$R *.DFM}
//====================================================================

const
 // DEF_TEMP_LAYER_Relief = 'relief';

  FORM_CAPTION = '������� LOS';


//------------------------------------------------------------------
class procedure Tfrm_Map_LOS.ShowForm(aBLRect: TBLRect);
//------------------------------------------------------------------
var
  oForm: Tfrm_Map_Tool_base;
  vIMapView: IMapViewX;

begin
  if not Assigned(IActiveMapView) then
    Exit;


  oForm:= IsFormExists (Application, Tfrm_Map_LOS.ClassName);

  if not Assigned(oForm) then
  begin
    vIMapView:=IActiveMapView;

    //Tfrm_Map_Tool_base
    oForm:= Tfrm_Map_LOS.Create (Application);
    oForm.SetIMapView(vIMapView);
  end;

  oForm.Show;



  if oForm.WindowState = wsMinimized then
    oForm.WindowState:= wsNormal;

//  Result:= Tfrm_Map(oForm);

//  dmRel_Engine.OpenByRect(aBLRect);

//  Tfrm_Map_Matrix_Monitor(oForm).DrawTempLayer;


end;

//---------------------------------------------------
procedure Tfrm_Map_LOS.FormCreate(Sender: TObject);
//---------------------------------------------------
begin
  inherited;

//  FRegPath := vars_Form_GetRegPath(className);

//  cxGrid1DBTableView1.RestoreFromRegistry(FRegPath);

  g_Storage.RestoreFromRegistry(cxGrid1DBTableView1, ClassName);



  if ADOConnection1.Connected then
     ShowMessage('ADOConnection1.Connected');


  Caption:=FORM_CAPTION;


  FIMapView.G


  cxGrid1TableView1.row


 // cxGrid1.Align:=alClient;

{  db_SetComponentADOConnection(Self, dmMain.ADOConnection1);

  db_TableReOpen(t_ClutterModelType, TBL_ClutterModelType);

  Assert(t_ClutterModelType.RecordCount>0, 't_ClutterModelType.RecordCount>0');

//  db_View (t_ClutterModelType);

  dmOnega_DB_data.Relief_Select(ADOStoredProc1, dmMain.ProjectID, True);
}
end;


procedure Tfrm_Map_LOS.FormDestroy(Sender: TObject);
begin
  g_Storage.StoreToRegistry(cxGrid1DBTableView1, ClassName);


 // cxGrid1DBTableView1.StoreToRegistry(FRegPath);

//  FIMapView.TempLayer_Clear(DEF_TEMP_LAYER_Relief);

 // dmRel_Engine.Close1;

  inherited; //!!!!
end;



//----------------------------------------------------
procedure Tfrm_Map_LOS.SetMapCursor (aBLCursor: TBLPoint);
//----------------------------------------------------
begin
  UpdateData;
end;


procedure Tfrm_Map_LOS.act_Opened_matrixesExecute(Sender: TObject);
begin
  // dmRel_Engine.Dlg_ShowMatrixList;
end;


// ---------------------------------------------------------------
procedure Tfrm_Map_LOS.UpdateData;
// ---------------------------------------------------------------

{var
  sFileName: string;

  rec: Trel_Record;
  I: Integer;
  iZone: Integer;
}

begin
//  dmRel_Engine.FindPointBL_ex(aBLCursor, Rlf, sFileName);

{
  cxGrid1.BeginUpdate;

  with cxGrid1DBTableView1.DataController do
    for I := 0 to RecordCount - 1 do
    begin
      sFileName :=Values[i, col_Name.Index];

      if dmRel_Engine.FindPointBLInFile(sFileName, FBLCursor, rec, iZone) then
      begin
         Values[i, col_rel_h.Index]       :=rec.Rel_H;
         Values[i, col_clutter_Code.Index]:=rec.Clutter_Code;
         Values[i, col_clutter_H.Index]   :=rec.Clutter_H;
         Values[i, col_Zone.Index]        :=iZone;

      end else
      begin
         Values[i, col_rel_h.Index]       :=null;
         Values[i, col_clutter_Code.Index]:=null;
         Values[i, col_clutter_H.Index]   :=null;
         Values[i, col_Zone.Index]   :=null;

      end;

    end;

  cxGrid1.EndUpdate;
  cxGrid1.Repaint;
}
end;


end.

(*

{


// ---------------------------------------------------------------
procedure Tfrm_Map_LOS.DrawTempLayer;
// ---------------------------------------------------------------

{var
  blPoints: TBLPointArray;
//  blPointsF: TBLPointArrayF;
  sFileName: string;
}

begin
 { FIMapView.TempLayer_Create(DEF_TEMP_LAYER_Relief);


  dmOnega_DB_data.Relief_Select(ADOStoredProc1, dmMain.ProjectID, True);


 // db_View (ADOStoredProc1);

  with ADOStoredProc1 do
     while not EOF do
  begin
    blPoints:= dmRelMatrixFile_export_import.ExtractReliefBoundsFromDataset(    ADOStoredProc1);

     sFileName := FieldByName(FLD_NAME).AsString;

     FIMapView.Layer_AddPolygon(DEF_TEMP_LAYER_Relief, blPoints, clBlue,0,2,  sFileName);


    Next;
  end;
  }
end;


}



  oLosMatrix := TIntMatrix.Create;

  oLosMatrix.SetMatrixSize(FRowCount, FColCount);
  oLosMatrix.ZoneNum   := iBest6Zone;
  oLosMatrix.TopLeft.X := xyRect.TopLeft.X;
  oLosMatrix.TopLeft.Y := xyRect.TopLeft.Y;
  oLosMatrix.Step      := Params.CalcStep_m;

  oLosMatrix.FillBlank;