inherited frm_Map_LOS: Tfrm_Map_LOS
  Left = 1074
  Top = 366
  Width = 635
  Height = 397
  BorderIcons = [biSystemMenu, biMinimize]
  Caption = 'frm_Map_LOS'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited ToolBar1: TToolBar
    Width = 627
  end
  inherited StatusBar1: TStatusBar
    Top = 351
    Width = 627
    Panels = <
      item
        Width = 200
      end
      item
        Width = 50
      end>
  end
  object cxGrid1: TcxGrid [2]
    Left = 0
    Top = 208
    Width = 627
    Height = 143
    Align = alBottom
    TabOrder = 2
    LookAndFeel.Kind = lfFlat
    object cxGrid1TableView1: TcxGridTableView
      NavigatorButtons.ConfirmDelete = False
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsView.GroupByBox = False
      object cxGrid1TableView1Column1: TcxGridColumn
        Width = 358
      end
      object cxGrid1TableView1Column2: TcxGridColumn
        Width = 80
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1TableView1
    end
  end
  object ActionList1: TActionList
    Left = 88
    Top = 8
    object Action1: TAction
      Caption = 'Action1'
    end
  end
end
