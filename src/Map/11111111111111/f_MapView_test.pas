]unit f_MapView_test;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs,      Grids, DBGrids, ComCtrls, DB;

type
  Tfrm_MapView_test = class(TForm)
    DBGrid_Maps: TDBGrid;
    DBGrid_Selection: TDBGrid;
    ds_Maps: TDataSource;
    ds_Selection: TDataSource;
    dxDBInspector1: TdxDBInspector;
    dxDBInspector2: TdxDBInspector;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormDestroy(Sender: TObject);
    procedure FormShow(Sender: TObject);
  public
    class procedure ShowForm(aMaps, aSelection: TDataset);
  end;

var
  frm_MapView_test: Tfrm_MapView_test;

implementation

{$R *.dfm}


class procedure Tfrm_MapView_test.ShowForm(aMaps, aSelection: TDataset);
begin
  if not Assigned(frm_MapView_test) then
  begin
    Application.CreateForm(Tfrm_MapView_test, frm_MapView_test);

    frm_MapView_test.ds_Maps.DataSet:=aMaps;
    frm_MapView_test.ds_Selection.DataSet:=aSelection;
    

  end;

  frm_MapView_test.Show;


end;

procedure Tfrm_MapView_test.FormClose(Sender: TObject; var Action:
    TCloseAction);
begin
  //Action:=caFree;

  FreeAndNil(frm_MapView_test);
end;

procedure Tfrm_MapView_test.FormDestroy(Sender: TObject);
begin
 // frm_MapView_test:=nil;
end;

procedure Tfrm_MapView_test.FormShow(Sender: TObject);
begin
  dxDBInspector2.RestoreRowsDefaults;
end;

end.