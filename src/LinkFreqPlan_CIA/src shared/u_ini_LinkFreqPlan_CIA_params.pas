unit u_ini_LinkFreqPlan_CIA_params;

interface
uses
   IniFiles, SysUtils, Dialogs;

type
  TChannelsKind = (cpDistr_, cpFixed_);

  // ---------------------------------------------------------------
  TIni_LinkFreqPlan_CIA_params = class
  // ---------------------------------------------------------------
  public
    ProjectID     : Integer;

    LinkFreqPlanID: integer;
    ChannelsKind  : string;


    ChannelsKind1 : TChannelsKind;

    ConnectionString : string;





(*     :=ReadInteger('main', 'LinkFreqPlanID',  0);
    sChannelsKind   :=ReadString ('main', 'ChannelsKind',    '');
    iProjectID      :=ReadInteger('main', 'ProjectID',       0);
*)

//    UsePassiveElements       : Boolean;
   // IsCalcWithAdditionalRain : Boolean;


    procedure LoadFromFile(aFileName: string);
    procedure SaveToFile(aFileName: string);

    function Validate: boolean;
  end;


implementation

const
  ChannelStr : array[TChannelsKind] of string =  ('Distr', 'Fixed');


// ---------------------------------------------------------------
procedure TIni_LinkFreqPlan_CIA_params.LoadFromFile(aFileName: string);
// ---------------------------------------------------------------
begin
  Assert(FileExists(aFileName), aFileName);


  with TIniFile.Create(aFileName) do
  begin
    LinkFreqPlanID :=ReadInteger('main', 'LinkFreqPlanID',  0);
    ChannelsKind   :=ReadString ('main', 'ChannelsKind',    '');

    ProjectID      :=ReadInteger('main', 'ProjectID',       0);

    ConnectionString :=ReadString('main', 'ConnectionString', '');



(*    ProjectID  :=ReadInteger ('main', 'ProjectID',  0);
    LinkID     :=ReadInteger ('main', 'ID',  0);

    UsePassiveElements   := ReadBool ('main', 'UsePassiveElements',  False);

*)    Free;
  end;
end;

// ---------------------------------------------------------------
procedure TIni_LinkFreqPlan_CIA_params.SaveToFile(aFileName: string);
// ---------------------------------------------------------------
begin
  with TIniFile.Create(aFileName) do
  begin
    WriteInteger('main', 'LinkFreqPlanID',  LinkFreqPlanID);
    WriteString ('main', 'ChannelsKind',    ChannelsKind);
    WriteInteger('main', 'ProjectID',       ProjectID);

    WriteString('main', 'ConnectionString', ConnectionString);


//    ChannelsKind : (cpDistr, cpFixed);


    Free;
  end;
end;




function TIni_LinkFreqPlan_CIA_params.Validate: boolean;
begin
  Result := (LinkFreqPlanID>0) and (ProjectID>0);

  if not Result then
    ShowMessage('(LinkFreqPlanID=0) or (ProjectID=0)');

end;


end.
