unit i_LinkFreqPlan_CIA;

interface

uses ADOInt,
  u_dll;

type

  ILinkFreqPlan_CIA_X = interface(IInterface)
  ['{432F20AE-8112-49F4-AE06-7F3F2EB96FDB}']

    procedure InitADO(aConnection: _Connection); stdcall;
//    procedure InitAppHandle(aHandle: integer); stdcall;

    procedure Execute (aFileName: WideString);  stdcall;
  end;

var
  ILinkFreqPlan_CIA: ILinkFreqPlan_CIA_X;

  function Load_ILinkFreqPlan_CIA: Boolean;
  procedure UnLoad_ILinkFreqPlan_CIA;

implementation

const
//  DEF_BPL_FILENAME = 'bpl_LOS.bpl';
  DEF_FILENAME = 'dll_LinkFreqPlan_CIA.dll';

var
  LHandle : Integer;


function Load_ILinkFreqPlan_CIA: Boolean;
begin
  Result:=GetInterface_DLL(DEF_FILENAME, LHandle, ILinkFreqPlan_CIA_X, ILinkFreqPlan_CIA)=0;
end;


procedure UnLoad_ILinkFreqPlan_CIA;
begin
  ILinkFreqPlan_CIA := nil;
  UnloadPackage_dll(LHandle);
end;


end.



