object frm_Data: Tfrm_Data
  Left = 547
  Top = 264
  Width = 728
  Height = 445
  Caption = 'frm_Data'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl1: TPageControl
    Left = 0
    Top = 0
    Width = 612
    Height = 411
    ActivePage = TabSheet1
    Align = alLeft
    Anchors = [akLeft, akTop, akRight, akBottom]
    TabOrder = 0
    object TabSheet1: TTabSheet
      Caption = 'FreqPlan_LinkEnd'
      object DBGrid1: TDBGrid
        Left = 0
        Top = 0
        Width = 604
        Height = 273
        Align = alTop
        DataSource = dmLinkFreqPlan_CIA.ds_LinkFreqPlan_LinkEnd
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'Neighbors'
      ImageIndex = 1
      object DBGrid2: TDBGrid
        Left = 0
        Top = 0
        Width = 604
        Height = 273
        Align = alTop
        DataSource = dmLinkFreqPlan_CIA.ds_Neighbors
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
      end
    end
  end
end
