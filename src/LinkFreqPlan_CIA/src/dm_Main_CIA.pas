unit dm_Main_CIA;

interface

uses
  Classes, ADODB, Forms, Dialogs,
  u_LinkFreqPlan_classes,
 u_const_db,

//  u_const_msg,

  u_db, DB


  ;

type
  TdmMain_CIA = class(TDataModule)
    ADOConnection1: TADOConnection;
    qry_LinkFreqPlan_LinkEnd: TADOQuery;
    qry_Neighbors: TADOQuery;
    procedure DataModuleCreate(Sender: TObject);
  private
   FData: TnbData;

   class procedure Init;

    procedure OpenData(aLinkFreqPlan_ID: Integer);
    { Public declarations }

    { Private declarations }
  public
   end;

var
  dmMain_CIA: TdmMain_CIA;

implementation

{$R *.dfm}

// ---------------------------------------------------------------
class procedure TdmMain_CIA.Init;
// ---------------------------------------------------------------
begin
  if not Assigned(dmMain_CIA) then
    dmMain_CIA := TdmMain_CIA.Create(Application);

end;


procedure TdmMain_CIA.DataModuleCreate(Sender: TObject);
begin
 // db_SetComponentADOConnection (Self, ADOConnection1);

   FData:=TnbData.Create();

end;



//--------------------------------------------------------------------
procedure TdmMain_CIA.OpenData(aLinkFreqPlan_ID: Integer);
//--------------------------------------------------------------------
const

(*  SQL_SELECT_LinkFreqPlan_LINKEND =
      'SELECT  id, LinkEnd_id, threshold_degradation '+  //Enabled,
      ' FROM ' + TBL_LinkFreqPlan_LinkEnd +
      ' WHERE (LinkFreqPlan_id=:LinkFreqPlan_id)';

  SQL_SELECT_LinkFreqPlan =
      'SELECT  * FROM ' + TBL_LinkFreqPlan +
      ' WHERE (id=:id)';
*)

{
  view_LinkFreqPlan_LinkEnd_for_calc_NB =
       'view_LinkFreqPlan_LinkEnd_for_calc_NB';

  SQL_SELECT_view_LinkFreqPlan_LinkEnd_for_calc_NB =
      'SELECT * FROM ' + view_LinkFreqPlan_LinkEnd_for_calc_NB +
      ' WHERE (LinkFreqPlan_id=:LinkFreqPlan_id)';
 }


  SQL_SELECT_LinkFreqPlan_LinkEnd =
      'SELECT * FROM ' + view_LinkFreqPlan_LinkEnd +
      ' WHERE (LinkFreqPlan_id=:LinkFreqPlan_id)';



  view_LINKFREQPLAN_Calc_NEIGHBORS = 'view_LINKFREQPLAN_Calc_NEIGHBORS';
                                              
  SQL_SELECT_LINK_FREQPLAN_NEIGHBORS =
      'SELECT *  FROM ' + view_LINKFREQPLAN_Calc_NEIGHBORS +
      ' WHERE (LinkFreqPlan_id=:LinkFreqPlan_id) ';
//      '       (rx_linkend_id=:rx_linkend_id) and (IsLink<>1) ';



(*/ -------------------------
  view_LinkFreqPlan_LinkEnd_antennas_for_calc_NB = 'view_LinkFreqPlan_LinkEnd_antennas_for_calc_NB';

  SQL_SELECT_LinkEnd_antennas =
      'SELECT * FROM ' + view_LinkFreqPlan_LinkEnd_antennas_for_calc_NB +
      ' WHERE (LinkFreqPlan_id=:LinkFreqPlan_id)';
*)

begin

   ShowMessage(' procedure TdmMain_CIA.OpenData(aLinkFreqPlan_ID: Integer);');


(*// ---------------------------------------------------------------
  db_Ope nQuery (qry_Antennas,
                SQL_SELECT_LinkEnd_antennas,
                [db_Par (FLD_LINKFREQPLAN_ID, Params.LinkFreqPlan_ID)]);

  FAntennaList.LoadFromDataset(qry_Antennas);
*)
  ShowMessage('  dmOnega_DB_data.OpenQuery(qry_LinkFreqPlan_LinkEnd,');

//  qry_LinkFreqPlan_LinkEnd.SQL.Text:=

  dmOnega_DB_data.OpenQuery(qry_LinkFreqPlan_LinkEnd,
                SQL_SELECT_LinkFreqPlan_LinkEnd,
//                SQL_SELECT_view_LinkFreqPlan_LinkEnd_for_calc_NB,
                [FLD_LINKFREQPLAN_ID, aLinkFreqPlan_ID]);

 // FLinkFreqPlan_Linkends.LoadFromDataset(qry_LinkFreqPlan_LinkEnd);

  FData.Linkends.LoadFromDataset(qry_LinkFreqPlan_LinkEnd);

  ShowMessage('  dmOnega_DB_data.OpenQuery(qry_LinkFreqPlan_LinkEnd,   - done');


  ShowMessage('  dmOnega_DB_data.OpenQuery(qry_Neighbors,');



  dmOnega_DB_data.OpenQuery(qry_Neighbors,
                    SQL_SELECT_LINK_FREQPLAN_NEIGHBORS,
                        [FLD_LINKFREQPLAN_ID, aLinkFreqPlan_ID]);
//                         db_par(FLD_rx_linkend_id,   FieldValues[FLD_LINKEND_ID])]);

//  i:=qry_Neighbors.RecordCount;

  FData.Neighbors.LoadFromDataset(qry_Neighbors);


  ShowMessage('  dmOnega_DB_data.OpenQuery(qry_Neighbors,   - done');


// ---------------------------------------------------------------


(*
  // -------------------------
  oErrorList:=TStringList.Create;
  FLinkFreqPlan_Linkends.Validate(oErrorList);

  if oErrorList.Count>0 then
  begin
    if ConfirmDlg('���������� ������ ������. �������� ������?') then
      ShellExec_Notepad_temp(oErrorList.Text);

    if not ConfirmDlg('���������� ������?') then
      Exit;

  end;

  FreeAndNil(oErrorList);
  // -------------------------
*)


(*
  db_OpenQuery (qry_LinkFreqPlan,
                SQL_SELECT_LinkFreqPlan,
                [db_Par (FLD_ID, Params.LinkFreqPlan_ID)]);

  FLinkFreqPlan.LoadFromDataset(qry_LinkFreqPlan);
*)

   ShowMessage(' procedure TdmMain_CIA.OpenData(aLinkFreqPlan_ID: Integer);        ---------   ');


end;




end.
