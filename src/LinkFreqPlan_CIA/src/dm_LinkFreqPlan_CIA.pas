unit dm_LinkFreqPlan_CIA;

interface

uses
  SysUtils, Classes, Forms, Db, ADODB, math, Variants, Dialogs,

  dm_Onega_DB_data,

  u_db_manager,
  dm_Progress,

  dm_Main,
  dm_LinkEndType,

  u_const_db,

//  u_const_msg,
  u_LinkFreqPlan_const,

  u_Link_const,

  u_func,

  u_db,
  u_LinkFreqPlan_classes;

type
  TdmLinkFreqPlan_CIA = class(TdmProgress)
    qry_LinkFreqPlan_LinkEnd: TADOQuery;
    qry_Neighbors: TADOQuery;
    qry_LinkEnd1: TADOQuery;
    qry_LinkEnd2: TADOQuery;
    qry_Mode1: TADOQuery;
    qry_Mode2: TADOQuery;
    procedure DataModuleDestroy(Sender: TObject);
    procedure DataModuleCreate(Sender: TObject);

  private
 //   FLinkFreqPlan_Linkends: TnbLinkFreqPlan_Linkend_List;


    FData: TnbData;

    procedure Get_Params(
        aLinkEnd1_ID, aLinkEnd2_ID: Integer;

        aLinkEnd_tx,aLinkEnd_rx: TnbLinkFreqPlan_Linkend;

        var aA1, aA2, aB1, aB2, aBandWidth1, aBandWidth2: double);


    function CalcInterference_Pair(aLinkFreqPlan_ID: integer): boolean;
    function CalcInterference_Summ(aLinkFreqPlan_ID: integer): boolean;

    procedure OpenData;

  public
    Params: record
      LinkFreqPlan_ID: integer;

      ChannelsKind : (opUseDistrChannels,
                      opUseFixedChannels);
    end;
//    procedure Clear_Interference(aLinkFreqPlan_ID: integer);

    procedure ExecuteProc; override;
  //  destructor Destroy; override;


    class procedure Init;
  end;



type

  TNeighbor = class
    Power_CIA : Double;
    POWER_NOISE : Double;
    CIA : Double;
    CA : Double;
    CI_ : Double;
  end;

  TLinkend = class
    level_tx_a : Double;
  end;



{
    begin
      if dPowerNoise = 0 then
        dPowerNoise:= qry_Neighbors.FieldByName(FLD_POWER_NOISE).AsFloat;

      if not((qry_Neighbors[FLD_CIA] = null) or (qry_Neighbors[FLD_CIA] = 255)) then
        dCIA:= dCIA + Power(10, -0.1*qry_Neighbors.FieldByName(FLD_CIA).AsFloat);
      if not((qry_Neighbors[FLD_CA] = null) or (qry_Neighbors[FLD_CA] = 255)) then
        dCA:= dCA + Power(10, -0.1*qry_Neighbors.FieldByName(FLD_CA).AsFloat);
      if not((qry_Neighbors['ci_'] = null) or (qry_Neighbors['ci_'] = 255)) then
        dCI:= dCI + Power(10, -0.1*qry_Neighbors.FieldByName('ci_').AsFloat);

      if not(qry_Neighbors[FLD_POWER_CIA] = null) then
      dPower_Neigh_Sum:= dPower_Neigh_Sum +
                 Power(10, 0.1*qry_Neighbors.FieldByName(FLD_POWER_CIA).AsFloat);

      qry_Neighbors.Next;
    end;

}


var
  dmLinkFreqPlan_CIA: TdmLinkFreqPlan_CIA;

//==============================================================
implementation
 {$R *.DFM}
//==============================================================



// ---------------------------------------------------------------
class procedure TdmLinkFreqPlan_CIA.Init;
// ---------------------------------------------------------------
begin
  if not Assigned(dmLinkFreqPlan_CIA) then
    dmLinkFreqPlan_CIA := TdmLinkFreqPlan_CIA.Create(Application);


end;



procedure TdmLinkFreqPlan_CIA.DataModuleDestroy(Sender: TObject);
begin
  FreeAndNil(FData);

//  FreeAndNil(FLinkFreqPlan_Linkends);
  inherited;
end;


//--------------------------------------------------------------------
procedure TdmLinkFreqPlan_CIA.DataModuleCreate(Sender: TObject);
//--------------------------------------------------------------------
begin
  inherited;

  db_SetComponentADOConnection (Self, dmMain.ADOConnection);

 // FLinkFreqPlan_Linkends := TnbLinkFreqPlan_Linkend_List.Create();

  FData:=TnbData.Create();

end;


//--------------------------------------------------------------------
function TdmLinkFreqPlan_CIA.CalcInterference_Pair(aLinkFreqPlan_ID: integer): boolean;
//--------------------------------------------------------------------
type
  TRec = record
         Freq_Spacing_MHz: Double;
         CI_0: Double;
         Freq1_Bandwidth: Double;
         Freq2_Bandwidth: Double;
         PowerNoise: Double;
         PowerNeighbour: Double;
         A1: Double;
         A2: Double;
         B1: Double;
         B2: double;

         //out
         CIA, CI, CA : Double;
         Degradation : Double;
         PowerCIA : Double;
       end;


    // ---------------------------------------------------------------
    function DoCalc(var aRec: TRec): boolean;
    // ---------------------------------------------------------------

//    function DoCalc(aFreq_Spacing, aCI_0, aFreq1_Bandwidth, aFreq2_Bandwidth,
//                    aPowerNoise, aPowerNeighbour, A1, A2, B1, B2: double;
//                    var aCIA, aCI, aCA, aDegradation, aPowerCIA : double): boolean;
    // ---------------------------------------------------------------

      //-------�������� ������--------
      // aCI_0 - �������� CI (��� ���������� ������), ��  - ����� �� ����������� ������� NeighBors - ���� ci
      // aFreq1_Bandwidth, aFreq2_Bandwidth - ������ ������, ���
      // A1, A2 - ���. ���������� ����� ��� ������� �� ������ 10 �����, ��
      // B1, B2 - ���������� ��������� ������, ��
      // (��� � � �: 1-��� ���1; 2-��� ���2)
      // aFreq_Spacing  - �������� ��������� ������
      // aPowerNoise - �������� ����, dBm
      // aPowerNeighbour - �������� ������ ��� ���������� ������, dBm
      // aPowerCIA - �������� ������ ��� ����������� ��������, dBm
    var
      dY_f       : double; //���������� �������� �����
      dFreq_delta: double; //
      dQ_0       : double; //�����. ���������� �� ��������� ������, � �����
      dQ_1       : double; //�����. ���������� �� �������� ������ ���1, � �����
      dQ_2       : double; //�����. ���������� �� �������� ������ ���2, � �����
    begin
      Result:= False;

      aRec.CIA:= 255;
      aRec.CI:= 255;
      aRec.CA:= 255;
      aRec.PowerCIA:= 255;
      aRec.Degradation:= 0;

      if (aRec.Freq1_Bandwidth=0) or (aRec.Freq2_Bandwidth=0) then
        Exit;

      dY_f:= 0.5*(aRec.Freq1_Bandwidth + aRec.Freq2_Bandwidth) - aRec.Freq_Spacing_MHz;

      if dY_f > 0 then
        dFreq_delta:= IIF(dY_f < Min(aRec.Freq1_Bandwidth, aRec.Freq2_Bandwidth), dY_f,
                                 Min(aRec.Freq1_Bandwidth, aRec.Freq2_Bandwidth))
      else
        dFreq_delta:= 0;

      dQ_0 := dFreq_delta/aRec.Freq1_Bandwidth;
      if aRec.Freq_Spacing_MHz=0 then
      begin
        dQ_1:=0;
        dQ_2:=0;
      end
      else
      begin
        dQ_1:= IIF(aRec.Freq_Spacing_MHz<0.5 * aRec.Freq1_Bandwidth, 0,
                  Power(10, 0.1*(-aRec.B1-aRec.A1*log10(aRec.Freq_Spacing_MHz / aRec.Freq1_Bandwidth))));

        dQ_2:= IIF(aRec.Freq_Spacing_MHz<0.5 * aRec.Freq2_Bandwidth, 0,
                  Power(10, 0.1*(-aRec.B2-aRec.A2*log10(aRec.Freq_Spacing_MHz / aRec.Freq2_Bandwidth))));
      end;

      if (dQ_0 = 0) and (dQ_1 = 0) and (dQ_2 = 0) then
        aRec.CIA:= 255
      else
        aRec.CIA:= aRec.CI_0 - 10*log10(dQ_0 + dQ_1 + dQ_2);

      if dQ_0 = 0 then
        aRec.CI := 255
      else
        aRec.CI := aRec.CI_0 - 10*log10(dQ_0);

      if (dQ_1 = 0) and (dQ_2 = 0) then
        aRec.CA:= 255
      else
        aRec.CA:=aRec.CI_0 - 10*log10(dQ_1 + dQ_2);

      if (dQ_0 <> 0) or (dQ_1 <> 0) or (dQ_2 <> 0) then
      begin
        aRec.PowerCIA:= aRec.PowerNeighbour + 10*log10(dQ_0 + dQ_1 + dQ_2);
        aRec.Degradation:= 10*log10(Power(10, 0.1* aRec.PowerNoise) +
                           Power(10, 0.1 * aRec.PowerCIA)) - aRec.PowerNoise;
      end;

      Result := True;
    end;    //
    // ---------------------------------------------------------------


var dA1, dA2, dB1, dB2, dFreqTX_LinkEnd1, dFreqRX_LinkEnd2: double;
    dBandwidth1, dBandwidth2, dPowerNeigh, dPowerNeigh_cia, dPowerNoise: double;
    dCIA, dCI, dCA, dDegradation : double;
    sFieldNameTx, sFieldNameRx: string;

    rec: TRec;

var
  bIsLink: Boolean;
  dPowerNeigh1: Double;
//  i1: integer;
//  i2: integer;
  iCount1: Integer;
  iCount2: Integer;
  iRX_LINKEND_ID: Integer;
  iTX_LINKEND_ID: Integer;

  oLinkEnd_tx,oLinkEnd_rx: TnbLinkFreqPlan_Linkend;
  oNeighbor: TnbNeighbor;
  sFreqRX: string;
  sFreqTX: string;

  txArr1, rxArr1: TDoubleArray;
  txArr2, rxArr2: TDoubleArray;

begin

//  ShowMessage('CalcInterference_pare    ');



  Result := False;

  dFreqTX_LinkEnd1:= 0;
  dFreqRX_LinkEnd2:= 0;

  db_ExecCommand(dmMain.ADOConnection1,
    'update '+ TBL_LINKFREQPLAN_NEIGHBORS+
    ' set CIA=null, ci_=null, ca=null, POWER_CIA=null, THRESHOLD_DEGRADATION_CIA=null '+
    ' WHERE LinkFreqPlan_id=:LinkFreqPlan_id',
                              [db_par(FLD_LINKFREQPLAN_ID, aLinkFreqPlan_ID)]);


  dmOnega_DB_data.OpenQuery(qry_Neighbors, 'SELECT * FROM '+ TBL_LINKFREQPLAN_NEIGHBORS+
                              ' WHERE LinkFreqPlan_id=:LinkFreqPlan_id',
                              [FLD_LINKFREQPLAN_ID, aLinkFreqPlan_ID]);

  dmOnega_DB_data.OpenQuery(qry_LinkFreqPlan_LinkEnd, 'SELECT * FROM '+ TBL_LINKFREQPLAN_LINKEND+
                              ' WHERE LinkFreqPlan_id=:LinkFreqPlan_id',
                              [FLD_LINKFREQPLAN_ID, aLinkFreqPlan_ID]);

 // db_View (qry_LinkFreqPlan_LinkEnd);



{
                               [db_par(FLD_CIA, null),
                         db_par(FLD_ci_, null),
                         db_par(FLD_CA,  null),

                         db_par(FLD_POWER_CIA, null),
                         db_par(FLD_THRESHOLD_DEGRADATION_CIA, null)]);
   }

//  FData.Neighbors.LoadFromDataset(qry_Neighbors);

//  FData.Linkends.LoadFromDataset(qry_LinkFreqPlan_LinkEnd);

  Assert(qry_Neighbors.RecordCount>0 , 'qry_Neighbors.RecordCount=0');


  if qry_Neighbors.RecordCount=0 then
  begin
    ShowMessage('qry_Neighbors.RecordCount=0');
    Exit;
  end;



 // db_View (qry_Neighbors);
 // db_View (qry_LinkFreqPlan_LinkEnd);


  sFieldNameTx:= IIF(Params.ChannelsKind=opUseDistrChannels, 'freq_distributed_tx', 'freq_fixed_tx');
  sFieldNameRx:= IIF(Params.ChannelsKind=opUseDistrChannels, 'freq_distributed_rx', 'freq_fixed_rx');

  with qry_Neighbors do
    while (not Eof) and (not Terminated) do
  begin
    DoProgress2(RecNo, RecordCount);


 //   ShowMessage( IntToStr(RecNo) );

    oNeighbor:=FData.Neighbors.FindIByID(FieldValues[FLD_ID]);

    Assert(Assigned(oNeighbor));

    bIsLink:=oNeighbor.IsLink;

    bIsLink:= FieldByName(FLD_IsLink).AsBoolean;

    if (not FieldByName(FLD_IsLink).AsBoolean) and
       (not FieldByName(FLD_IsPropertyOne).AsBoolean)

    then
    begin
      //

//      i1:=qry_Neighbors['rx_for_linkend1_id'];
  //    i2:=qry_Neighbors[FLD_RX_LINKEND_ID];

      iTX_LINKEND_ID:=qry_Neighbors[FLD_RX_LINKEND_ID];
      iRX_LINKEND_ID:=qry_Neighbors['rx_for_linkend1_id'];

{
      i1:=qry_Neighbors[FLD_TX_LINKEND_ID];
      i2:=qry_Neighbors[FLD_RX_LINKEND_ID];

      iTX_LINKEND_ID:=qry_Neighbors[FLD_TX_LINKEND_ID];
      iRX_LINKEND_ID:=qry_Neighbors[FLD_RX_LINKEND_ID];
}

      oLinkEnd_tx:=FData.Linkends.FindByLinkendID(iTX_LINKEND_ID);
      oLinkEnd_rx:=FData.Linkends.FindByLinkendID(iRX_LINKEND_ID);


      Get_Params(iTX_LINKEND_ID,
                 iRX_LINKEND_ID,
                 oLinkEnd_tx,
                 oLinkEnd_rx,
                 dA1, dA2, dB1, dB2, dBandwidth1, dBandwidth2);

      if Params.ChannelsKind = opUseDistrChannels then
      begin
        sFreqTX:= oLinkEnd_tx.freq_distributed;
        sFreqRX:= oLinkEnd_rx.freq_distributed;

//        dFreqTX_LinkEnd1:=

      end else
      begin
        sFreqTX:= oLinkEnd_tx.FREQ_FIXED;
        sFreqRX:= oLinkEnd_rx.FREQ_FIXED;

      end;

      iCount1:=oLinkEnd_tx.ChannelsToFreqMHz (sFreqTX, txArr1, rxArr1);
      iCount2:=oLinkEnd_rx.ChannelsToFreqMHz (sFreqRX, txArr2, rxArr2);





      if qry_LinkFreqPlan_LinkEnd.Locate(FLD_LINKEND_ID, iRX_LINKEND_ID , []) then //FieldValues[FLD_RX_LINKEND_ID]
        dFreqTX_LinkEnd1:= qry_LinkFreqPlan_LinkEnd.FieldByName(sFieldNameTx).AsFloat;

      if qry_LinkFreqPlan_LinkEnd.Locate(FLD_LINKEND_ID, iTX_LINKEND_ID , []) then // FieldValues['rx_for_linkend1_id']
        dFreqRX_LinkEnd2:= qry_LinkFreqPlan_LinkEnd.FieldByName(sFieldNameRx).AsFloat;

      if (dFreqTX_LinkEnd1 <= 0) or (dFreqRX_LinkEnd2 <= 0) then
      begin
        Next;
        Continue;
      end;


      dPowerNeigh1:= oNeighbor.RX_LEVEL_DBM -
                    oNeighbor.DIAGRAM_LEVEL +
                    oNeighbor.EXTRA_LOSS;

      dPowerNeigh:= FieldByName(FLD_RX_LEVEL_dBm).AsFloat -
                   // FieldByName(FLD_POWER_dBm).AsFloat -
                     FieldByName(FLD_DIAGRAM_LEVEL).AsFloat +
                     FieldByName(FLD_EXTRA_LOSS).AsFloat;

      dPowerNoise:= FieldByName(FLD_POWER_NOISE).AsFloat;


      FillChar(rec, SizeOf(rec), 0);

      rec.CI_0             := oNeighbor.CI;


      rec.Freq_Spacing_MHz := Abs(dFreqTX_LinkEnd1-dFreqRX_LinkEnd2);
      rec.CI_0             := FieldByName(FLD_CI).AsFloat;
      rec.Freq1_Bandwidth  := dBandwidth1;
      rec.Freq2_Bandwidth  := dBandwidth2;
      rec.PowerNoise       := dPowerNoise;
      rec.PowerNeighbour   := dPowerNeigh;
      rec.A1               := dA1;
      rec.A2               := dA2;
      rec.B1               := dB1;
      rec.B2               := dB2;


      if DoCalc (rec)

//      if DoCalc (Abs(dFreqTX_LinkEnd1-dFreqRX_LinkEnd2),
 //                FieldByName(FLD_CI).AsFloat,
  //               dBandwidth1,dBandwidth2, dPowerNoise, dPowerNeigh,
    //             dA1, dA2, dB1, dB2, dCIA, dCI, dCA, dDegradation, dPowerNeigh_cia)
      then
      begin
         dCIA:=rec.CIA;
         dCI:=rec.CI;
         dCA:=rec.CA;

         dDegradation   :=rec.Degradation;
         dPowerNeigh_cia:=rec.PowerCIA;


         db_UpdateRecord(qry_Neighbors,
                        [db_par(FLD_CIA, IIF(dCIA>=255, null, dCIA)),
                         db_par(FLD_ci_, IIF(dCI>=255,  null, dCI)),
                         db_par(FLD_CA,  IIF(dCA>=255,  null, dCA)),

                         db_par(FLD_POWER_CIA, IIF(dPowerNeigh_cia=255, null, dPowerNeigh_cia)),
                         db_par(FLD_THRESHOLD_DEGRADATION_CIA, dDegradation)])
      end;
   //   else
      {
        db_UpdateRecord(qry_Neighbors,
                        [db_par(FLD_CIA, null),
                         db_par(FLD_ci_, null),
                         db_par(FLD_CA,  null),

                         db_par(FLD_POWER_CIA, null),
                         db_par(FLD_THRESHOLD_DEGRADATION_CIA, null)]);
       }
    end;

    Next;
  end;
  if (not Terminated) then
    Result := True;


 // ShowMessage('CalcInterference_pare   done ');

end;

//--------------------------------------------------------------------
procedure TdmLinkFreqPlan_CIA.Get_Params(
              aLinkEnd1_ID, aLinkEnd2_ID: integer;

              aLinkEnd_tx,aLinkEnd_rx: TnbLinkFreqPlan_Linkend;

              var aA1, aA2, aB1, aB2, aBandWidth1, aBandWidth2: double);
//--------------------------------------------------------------------

   //---------------------------------------------------
   procedure Do_Calc_Param_A_B();
   //---------------------------------------------------
   var
     iLevel_TX_a, iLevel_RX_a: integer;

     dValue_30, dValue_3, dValue_A, dDeltaF : double;
     iLinkEndType1: Integer;
     iLinkEndType2: Integer;
   begin
     aA1:=0; aA2:=0; aB1:=0; aB2:=0;

     // -------------------------
     //for TX
     // -------------------------
     dDeltaF  :=aLinkEnd_tx.BANDWIDTH;

     dDeltaF  := qry_Mode1.FieldByName(FLD_BANDWIDTH).AsFloat;


     iLevel_TX_a :=aLinkEnd_tx.LinkEndType.level_tx_a;

     iLinkEndType1 := qry_LinkEnd1.FieldByName(FLD_LINKENDTYPE_ID).AsInteger;

     iLevel_TX_a  :=aLinkEnd_tx.LinkEndType.Level_TX_a;


     iLevel_TX_a   := gl_DB.GetIntFieldValueByID (TBL_LINKENDTYPE, 'level_tx_a', iLinkEndType1);
     iLevel_TX_a   := Abs(dmLinkEndType.GetIntFieldValue(iLinkEndType1, 'level_tx_a'));

     dValue_A  :=aLinkEnd_tx.BANDWIDTH_TX_A;
     dValue_A := qry_Mode1.FieldByName(FLD_BANDWIDTH_TX_A).AsFloat;

     if (iLevel_TX_a <> 0) and (dValue_A <> 0) then
     begin
       dValue_30:= qry_Mode1.FieldByName(FLD_BANDWIDTH_TX_30).AsFloat;

       if dValue_30 <> 0 then
       begin
         try
           Assert (dValue_A<>dValue_30);
           Assert (log10(dValue_A/dValue_30) <> 0, ' level_tx_a, level_tx_a, log10(dValue_A/dValue_30) <> 0');

           aA1:= (iLevel_TX_a-30)/log10(dValue_A/dValue_30);

         except
         end;


         aB1:= (30*log10(dValue_A/(2*dDeltaF))-iLevel_TX_a*log10(dValue_30/(2*dDeltaF)))/
                                                 log10(dValue_A/dValue_30);
       end;
     end
     else
     begin
       dValue_30  :=aLinkEnd_tx.BANDWIDTH_TX_30;
       dValue_3   :=aLinkEnd_tx.BANDWIDTH_TX_3;


       dValue_30:= qry_Mode1.FieldByName(FLD_BANDWIDTH_TX_30).AsFloat;
       dValue_3 := qry_Mode1.FieldByName(FLD_BANDWIDTH_TX_3).AsFloat;

       if (dValue_3 <> 0) and (dValue_30 <> 0) then
       begin
         Assert(dValue_30>dValue_3);
      
         aA1:= (30-3)/log10(dValue_30/dValue_3);
         aB1:= (3*log10(dValue_30/(2*dDeltaF))-30*log10(dValue_3/(2*dDeltaF)))/
                                                      log10(dValue_30/dValue_3);
       end;
     end;

     // -------------------------
     //for RX
     // -------------------------
     dDeltaF    :=aLinkEnd_rx.BANDWIDTH;
//     iLevel_RX_a   :=aLinkEnd1.l;


     dDeltaF  := qry_Mode2.FieldByName(FLD_BANDWIDTH).AsFloat;

     iLevel_RX_a :=aLinkEnd_rx.LinkEndType.level_rx_a;

     iLinkEndType2 := qry_LinkEnd2.FieldByName(FLD_LINKENDTYPE_ID).AsInteger;
     iLevel_RX_a:= gl_DB.GetIntFieldValueByID(TBL_LINKENDTYPE,  'level_rx_a', iLinkEndType2);
     iLevel_RX_a:= Abs(dmLinkEndType.GetIntFieldValue(iLinkEndType2, 'level_rx_a'));

     dValue_A  :=aLinkEnd_rx.BANDWIDTH_RX_A;
     dValue_A := qry_Mode2.FieldByName(FLD_BANDWIDTH_RX_A).AsFloat;

     if (iLevel_RX_a <> 0) and (dValue_A <> 0) then
     begin
       dValue_30  :=aLinkEnd_rx.BANDWIDTH_RX_30;
       dValue_30:= qry_Mode2.FieldByName(FLD_BANDWIDTH_RX_30).AsFloat;

       if dValue_30 <> 0 then
       begin
         Assert (dValue_A<>dValue_30);


         aA2:= (iLevel_RX_a-30)/log10(dValue_A/dValue_30);
         aB2:= (30*log10(dValue_A/(2*dDeltaF))-iLevel_RX_a*log10(dValue_30/(2*dDeltaF)))/
                                                  log10(dValue_A/dValue_30);
       end;

     end
     else
     begin
       dValue_30  :=aLinkEnd_rx.BANDWIDTH_RX_30;
       dValue_3   :=aLinkEnd_rx.BANDWIDTH_RX_3;


       dValue_30:= qry_Mode2.FieldByName(FLD_BANDWIDTH_RX_30).AsFloat;
       dValue_3 := qry_Mode2.FieldByName(FLD_BANDWIDTH_RX_3).AsFloat;

       if (dValue_3 <> 0) and (dValue_30 <> 0) then
       begin
         Assert (dValue_3<>dValue_30);

         aA2:= (30-3)/log10(dValue_30/dValue_3);
         aB2:= (3*log10(dValue_30/(2*dDeltaF))-30*log10(dValue_3/(2*dDeltaF)))/
                                                      log10(dValue_30/dValue_3);
       end;

     end;
   end;    //
   // ---------------------------------------------------------------

const
  SQL_LINKEND = 'SELECT * FROM '+TBL_LINKEND+' WHERE id=:id';

  SQL_MODE    = 'SELECT * FROM '+TBL_LINKENDTYPE_MODE +' WHERE (id=:id)';

//                 ' WHERE (LinkEndType_id=:LinkEndType_id) '+
//                 '   and (mode=:mode)';
begin
  dmOnega_db_data.OpenQuery(qry_LinkEnd1, SQL_LINKEND, [FLD_ID, aLinkEnd1_ID]);
  dmOnega_db_data.OpenQuery(qry_LinkEnd2, SQL_LINKEND, [FLD_ID, aLinkEnd2_ID]);

  dmOnega_db_data.OpenQuery(qry_Mode1, SQL_MODE,
      [
       FLD_ID, qry_LinkEnd1.FieldValues[FLD_LINKENDTYPE_MODE_ID)
//       db_par(FLD_LINKENDTYPE_ID, qry_LinkEnd1.FieldValues[FLD_LINKENDTYPE_ID]),
  //     db_par(FLD_MODE,           qry_LinkEnd1.FieldValues[FLD_MODE])
       ]);

  dmOnega_db_data.OpenQuery(qry_Mode2, SQL_MODE,
      [
       FLD_ID, qry_LinkEnd2.FieldValues[FLD_LINKENDTYPE_MODE_ID]
//       db_par(FLD_LINKENDTYPE_ID, qry_LinkEnd2.FieldValues[FLD_LINKENDTYPE_ID]),
  //     db_par(FLD_MODE,           qry_LinkEnd2.FieldValues[FLD_MODE])
       ]);

  Do_Calc_Param_A_B();

  aBandWidth1  :=aLinkEnd_tx.BANDWIDTH;
  aBandWidth2  :=aLinkEnd_rx.BANDWIDTH;

  aBandWidth1:= qry_Mode1.FieldByName(FLD_BANDWIDTH).AsFloat;
  aBandWidth2:= qry_Mode2.FieldByName(FLD_BANDWIDTH).AsFloat;
end;

//--------------------------------------------------------------------
function TdmLinkFreqPlan_CIA.CalcInterference_Summ(aLinkFreqPlan_ID: integer): boolean;
//--------------------------------------------------------------------
const
  SQL_SELECT_LINK_FREQPLAN_NEIGHBORS_TO_RX_LINKEND_ID =
      'SELECT id, cia, ca, ci_, power_cia, power_noise '+
      ' FROM ' + TBL_LinkFreqPlan_NeighBors +
      ' WHERE (LinkFreqPlan_id=:LinkFreqPlan_id) and '+
      '       (tx_linkend_id=:tx_linkend_id) and (IsLink<>1) and (IsPropertyOne<>1)';

var
  dDegradation, dCIA, dCI, dCA, dCIA_Result, dCI_Result, dCA_Result : double;
  dPower_Neigh_Sum1, dPowerNoise: double;
  e: Double;
  e1: Double;
  e2: Double;
  e3: Double;
  i: Integer;
  iID: Integer;
  iLINKEND_ID: Integer;

// oNeighbor: TNeighbor;

 oNeighbor: TnbNeighbor;

begin
  Result := False;


 // ShowMessage('CalcInterference_Summ');

  db_OpenQuery(qry_LinkFreqPlan_LinkEnd,
                  'SELECT id, LinkEnd_id, CIA, CI, CA, threshold_degradation_cia '+
                  ' FROM ' + TBL_LinkFreqPlan_LinkEnd +
                  ' WHERE (LinkFreqPlan_id=:LinkFreqPlan_id)', //(Enabled=1) and
                   [db_par(FLD_LINKFREQPLAN_ID, aLinkFreqPlan_ID)]);

//  FData.Linkends.LoadFromDataset(qry_LinkFreqPlan_LinkEnd);

  Assert(qry_LinkFreqPlan_LinkEnd.RecordCount>0 );



  i:=qry_LinkFreqPlan_LinkEnd.RecordCount;


//  db_View (qry_LinkFreqPlan_LinkEnd);

  with qry_LinkFreqPlan_LinkEnd do
    while (not Eof) and (not Terminated) do
  begin
    DoProgress2(RecNo, RecordCount);

//    oNeighbor:=FData.Neighbors.FindIByID(FieldValues[FLD_ID]);


    dCIA:= 0;
    dCI := 0;
    dCA := 0;
    dPower_Neigh_Sum1:= 0;
    dPowerNoise     := 0;



    iLINKEND_ID:= FieldValues[FLD_LINKEND_ID];


    db_OpenQuery(qry_Neighbors, SQL_SELECT_LINK_FREQPLAN_NEIGHBORS_TO_RX_LINKEND_ID,
                          [db_par(FLD_LINKFREQPLAN_ID, aLinkFreqPlan_ID),
                           db_par(FLD_tx_linkend_id,   FieldValues[FLD_LINKEND_ID])]);

    i:=qry_Neighbors.RecordCount;

 //   db_View (qry_Neighbors);


//    FData.Neighbors.LoadFromDataset(qry_Neighbors);

 //   FData.Neighbors.SelectByRxLinkendID_and_not_IsLink(iLINKEND_ID);



    while (not qry_Neighbors.Eof) do
    begin

{
      if (not qry_Neighbors.FieldByName(FLD_IsLink).AsBoolean) and
         (not qry_Neighbors.FieldByName(FLD_IsPropertyOne).AsBoolean)

      then begin
        Next;
        Continue;
      end;
 }

      iID:= qry_Neighbors.FieldByName(FLD_ID).AsInteger;

      oNeighbor:=FData.Neighbors.FindIByID(iID);

  //    oNbNeighbor:=FData.Neighbors.Selection.FindIByID(iID);


//      oNeighbor:=TNeighbor.Create;
{
      oNeighbor.POWER_CIA  :=qry_Neighbors.FieldByName(FLD_POWER_CIA).AsFloat;
      oNeighbor.POWER_NOISE:=qry_Neighbors.FieldByName(FLD_POWER_NOISE).AsFloat;
      oNeighbor.CIA        :=qry_Neighbors.FieldByName(FLD_CIA).AsFloat;
      oNeighbor.CI_        :=qry_Neighbors.FieldByName(FLD_CI_).AsFloat;
 }

 //     oNeighbor.Free;

      //-------------------------------------------------------------------



      if dPowerNoise = 0 then
        dPowerNoise:= qry_Neighbors.FieldByName(FLD_POWER_NOISE).AsFloat;

      if not((qry_Neighbors[FLD_CIA] = null) or (qry_Neighbors[FLD_CIA] = 255)) then
      begin
       // v:=oNeighbor.CIA;

        e1:=qry_Neighbors.FieldByName(FLD_CIA).AsFloat;
        dCIA:= dCIA + Power(10, -0.1*qry_Neighbors.FieldByName(FLD_CIA).AsFloat);
      end;

      if not((qry_Neighbors[FLD_CA] = null) or (qry_Neighbors[FLD_CA] = 255)) then
      begin
       // v:=oNeighbor.CA;

        e2:=qry_Neighbors.FieldByName(FLD_CA).AsFloat;
        dCA:= dCA + Power(10, -0.1*qry_Neighbors.FieldByName(FLD_CA).AsFloat);
      end;

      if not((qry_Neighbors[FLD_CI_] = null) or (qry_Neighbors[FLD_CI_] = 255)) then
      begin
       // v:=oNeighbor.CI_;

        e3:=qry_Neighbors.FieldByName(FLD_CI_).AsFloat;
        dCI:= dCI + Power(10, -0.1*qry_Neighbors.FieldByName(FLD_CI_).AsFloat);
      end;

      if not(qry_Neighbors[FLD_POWER_CIA] = null) then
      begin
        //v:=oNeighbor.POWER_CIA;

        e:=qry_Neighbors.FieldByName(FLD_POWER_CIA).AsFloat;

        e:=Power(10, 0.1*e);

        dPower_Neigh_Sum1:= dPower_Neigh_Sum1 +
                 Power(10, 0.1*qry_Neighbors.FieldByName(FLD_POWER_CIA).AsFloat);
      end;

      qry_Neighbors.Next;
    end;


    if dCIA<>0 then dCIA_Result:= -10*log10(dCIA)
               else dCIA_Result:= 0;

    if dCI<>0  then dCI_Result := -10*log10(dCI)
               else dCI_Result:= 0;

    if dCA<>0  then dCA_Result := -10*log10(dCA)
               else dCA_Result:= 0;

   // try
    if dPower_Neigh_Sum1<>0 then
    begin
      dPower_Neigh_Sum1:=10*log10(dPower_Neigh_Sum1);

      dDegradation:=10*log10(Power(10, 0.1*dPowerNoise) +
                    Power(10, 0.1*dPower_Neigh_Sum1)) - dPowerNoise;

      dDegradation:=dDegradation;

    end else
      dDegradation:=0;

 {   except
    end;
}

    db_UpdateRecord(qry_LinkFreqPlan_LinkEnd,
          [db_par(FLD_CIA, IIF(((dCIA_Result<>0) or (dCI_Result<>0) or (dCA_Result<>0)),
                                    dCIA_Result, null)),
           db_par(FLD_CI,  IIF(dCI_Result<>0,  dCI_Result, null)),
           db_par(FLD_CA,  IIF(dCA_Result<>0,  dCA_Result, null)),

           db_par(FLD_THRESHOLD_DEGRADATION_CIA, IIF(dDegradation<>0,  dDegradation, null) )
          ]);

    Next;
  end;

  if (not Terminated) then
    Result := True;


//  ShowMessage('CalcInterference_Summ   done');

end;

//--------------------------------------------------------------------
procedure TdmLinkFreqPlan_CIA.ExecuteProc;
//--------------------------------------------------------------------
var
  sInterference_ChannelsKind: string;
begin
  Assert(Params.LinkFreqPlan_ID>0);

//  if Params.LinkFreqPlan_ID = 0 then
//    Exit;

 //  ShowMessage('OpenData();');

  //  ShowMessage('OpenData(); before');


  OpenData();


 //  ShowMessage('OpenData();  done ');



//   ShowMessage(' dmOnega_DB_data.LinkFreqPlan_Tools(Params.LinkFreqPlan_ID, DEF_Clear_Interference);    1 ');

  dmOnega_DB_data.LinkFreqPlan_Tools(Params.LinkFreqPlan_ID, DEF_Clear_Interference);


 //  ShowMessage(' dmOnega_DB_data.LinkFreqPlan_Tools(Params.LinkFreqPlan_ID, DEF_Clear_Interference);    done ');

//  Exit;


//  Clear_Interference (Params.LinkFreqPlan_ID);

  sInterference_ChannelsKind:= IIF(Params.ChannelsKind=opUseDistrChannels,
                                                    STR_DISTRIBUTED, STR_FIXED);

  gl_db.UpdateRecord(TBL_LINKFREQPLAN, Params.LinkFreqPlan_ID,
       [db_par(FLD_Interference_ChannelsKind1, sInterference_ChannelsKind)]);


  if CalcInterference_Pair (Params.LinkFreqPlan_ID) then
    CalcInterference_Summ (Params.LinkFreqPlan_ID);
end;

//--------------------------------------------------------------------
procedure TdmLinkFreqPlan_CIA.OpenData;
//--------------------------------------------------------------------
const

(*  SQL_SELECT_LinkFreqPlan_LINKEND =
      'SELECT  id, LinkEnd_id, threshold_degradation '+  //Enabled,
      ' FROM ' + TBL_LinkFreqPlan_LinkEnd +
      ' WHERE (LinkFreqPlan_id=:LinkFreqPlan_id)';

  SQL_SELECT_LinkFreqPlan =
      'SELECT  * FROM ' + TBL_LinkFreqPlan +
      ' WHERE (id=:id)';
*)

{
  view_LinkFreqPlan_LinkEnd_for_calc_NB =
       'view_LinkFreqPlan_LinkEnd_for_calc_NB';

  SQL_SELECT_view_LinkFreqPlan_LinkEnd_for_calc_NB =
      'SELECT * FROM ' + view_LinkFreqPlan_LinkEnd_for_calc_NB +
      ' WHERE (LinkFreqPlan_id=:LinkFreqPlan_id)';
 }


  SQL_SELECT_LinkFreqPlan_LinkEnd =
      'SELECT * FROM ' + view_LinkFreqPlan_LinkEnd +
      ' WHERE (LinkFreqPlan_id=:LinkFreqPlan_id)';



  view_LINKFREQPLAN_Calc_NEIGHBORS = 'view_LINKFREQPLAN_Calc_NEIGHBORS';
                                              
  SQL_SELECT_LINK_FREQPLAN_NEIGHBORS =
      'SELECT *  FROM ' + view_LINKFREQPLAN_Calc_NEIGHBORS +
      ' WHERE (LinkFreqPlan_id=:LinkFreqPlan_id) ';
//      '       (rx_linkend_id=:rx_linkend_id) and (IsLink<>1) ';



(*/ -------------------------
  view_LinkFreqPlan_LinkEnd_antennas_for_calc_NB = 'view_LinkFreqPlan_LinkEnd_antennas_for_calc_NB';

  SQL_SELECT_LinkEnd_antennas =
      'SELECT * FROM ' + view_LinkFreqPlan_LinkEnd_antennas_for_calc_NB +
      ' WHERE (LinkFreqPlan_id=:LinkFreqPlan_id)';
*)

begin
(*// ---------------------------------------------------------------
  db_OpenQuery (qry_Antennas,
                SQL_SELECT_LinkEnd_antennas,
                [db_Par (FLD_LINKFREQPLAN_ID, Params.LinkFreqPlan_ID)]);

  FAntennaList.LoadFromDataset(qry_Antennas);
*)

//  ShowMessage('  dmOnega_DB_data.OpenQuery(qry_LinkFreqPlan_LinkEnd,');

//  qry_LinkFreqPlan_LinkEnd.SQL.Text:=

  dmOnega_DB_data.OpenQuery(qry_LinkFreqPlan_LinkEnd,
                SQL_SELECT_LinkFreqPlan_LinkEnd,
//                SQL_SELECT_view_LinkFreqPlan_LinkEnd_for_calc_NB,
                [db_Par (FLD_LINKFREQPLAN_ID, Params.LinkFreqPlan_ID)]);

 // FLinkFreqPlan_Linkends.LoadFromDataset(qry_LinkFreqPlan_LinkEnd);

  FData.Linkends.LoadFromDataset(qry_LinkFreqPlan_LinkEnd);

 // ShowMessage('  dmOnega_DB_data.OpenQuery(qry_LinkFreqPlan_LinkEnd,   - done');


//  ShowMessage('  dmOnega_DB_data.OpenQuery(qry_Neighbors,');



  dmOnega_DB_data.OpenQuery(qry_Neighbors,
                    SQL_SELECT_LINK_FREQPLAN_NEIGHBORS,
                        [db_par(FLD_LINKFREQPLAN_ID, Params.LinkFreqPlan_ID)]);
//                         db_par(FLD_rx_linkend_id,   FieldValues[FLD_LINKEND_ID])]);

//  i:=qry_Neighbors.RecordCount;

  FData.Neighbors.LoadFromDataset(qry_Neighbors);


//  ShowMessage('  dmOnega_DB_data.OpenQuery(qry_Neighbors,   - done');


// ---------------------------------------------------------------


(*
  // -------------------------
  oErrorList:=TStringList.Create;
  FLinkFreqPlan_Linkends.Validate(oErrorList);

  if oErrorList.Count>0 then
  begin
    if ConfirmDlg('���������� ������ ������. �������� ������?') then
      ShellExec_Notepad_temp(oErrorList.Text);

    if not ConfirmDlg('���������� ������?') then
      Exit;

  end;

  FreeAndNil(oErrorList);
  // -------------------------
*)


(*
  db_OpenQuery (qry_LinkFreqPlan,
                SQL_SELECT_LinkFreqPlan,
                [db_Par (FLD_ID, Params.LinkFreqPlan_ID)]);

  FLinkFreqPlan.LoadFromDataset(qry_LinkFreqPlan);
*)
end;



begin

end.



//-------------------------------------------------------------------------
//procedure TdmLinkFreqPlan_CIA.Clear_Interference(aLinkFreqPlan_ID: integer);
//--------------------------------------------------------------------
//begin
//  dmOnega_DB_data.LinkFreqPlan_Tools(aLinkFreqPlan_ID, DEF_Clear_Interference);
//end;