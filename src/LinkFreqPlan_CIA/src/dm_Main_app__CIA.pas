unit dm_Main_app__CIA;

interface

uses
  SysUtils, Classes, Dialogs, 

  u_Ini_LinkFreqPlan_CIA_params,

  u_vars,

  u_log,

  u_func,


  dm_LinkFreqPlan_CIA,

  dm_Main
  ;

type
  TdmMain_app__CIA = class(TDataModule)
    procedure DataModuleCreate(Sender: TObject);

  end;


var
  dmMain_app__CIA: TdmMain_app__CIA;

//===================================================================
implementation {$R *.DFM}     
//===================================================================


//--------------------------------------------------------------
procedure TdmMain_app__CIA.DataModuleCreate(Sender: TObject);
//--------------------------------------------------------------
const

  DEF_TEMP_FILENAME = 'LinkFreqPlan_CIA1.ini';


var
 // iProjectID: Integer;
  sChannelsKind,sIniFileName: string;


  iLinkFreqPlanID: integer;
  bUseFixedChannels: boolean;

 // oIniFile:   TIniFile;
  oParams: TIni_LinkFreqPlan_CIA_params;

begin



  sIniFileName:=ParamStr(1);
  if sIniFileName='' then
    sIniFileName:=g_ApplicationDataDir+  DEF_TEMP_FILENAME;


  if not FileExists(sIniFileName) then
  begin
    ShowMessage(sIniFileName);
    Exit;
  end;





 // ShowMessage(sIniFileName);


 // ---------------------------------------------------------------
  oParams:=TIni_LinkFreqPlan_CIA_params.Create;
  oParams.LoadFromFile (sIniFileName);

  iLinkFreqPlanID:= oParams.LinkFreqPlanID;
  sChannelsKind:=oParams.ChannelsKind;

  FreeAndNil(oParams);
  // ---------------------------------------------------------------

 {
  oIniFile:=TIniFile.Create (sIniFileName);

  with oIniFile do
  begin
    iLinkFreqPlanID :=ReadInteger('main', 'LinkFreqPlanID',  0);
    sChannelsKind   :=ReadString ('main', 'ChannelsKind',    '');
//    iProjectID      :=ReadInteger('main', 'ProjectID',       0);
//    Free;
  end;

  FreeAndNil(oIniFile);
  }

  if// (iProjectID=0) or
     (iLinkFreqPlanID=0)
  then begin
    ShowMessage('(iLinkFreqPlanID=0) ');
    Exit;
  end;


  //ShowMessage('1');


  TdmMain.Init;

//  if not dmMain.OpenDB_dlg then
  if not dmMain.OpenDB_reg then
    Exit;

 // dmMain.ProjectID:=iProjectID;


 // TdmMain_CIA.Init;


//  if not db_OpenFromReg_( dmMain_CIA.ADOConnection1, dm_Main.REGISTRY_LOGIN_) then
  //  Exit;

 // dmMain_CIA.OpenData(iLinkFreqPlanID);

  TdmLinkFreqPlan_CIA.Init;




//  ShowMessage('dmMain.OpenDB_reg    done');


  with dmLinkFreqPlan_CIA do
  begin
    Params.LinkFreqPlan_ID:= iLinkFreqPlanID;

    if Eq(sChannelsKind, 'Fixed')       then  Params.ChannelsKind := opUseFixedChannels else
    if Eq(sChannelsKind, 'Distributed') then  Params.ChannelsKind := opUseDistrChannels
      else
        ShowMessage('sChannelsKind=?');


//    Params.ChannelsKind := IIF (sChannelType = bUseFixedChannels, opUseDistrChannels, opUseFixedChannels);

  end;


// dmLinkFreqPlan_CIA1.ExecuteProc();

 dmLinkFreqPlan_CIA.ExecuteDlg ('������ �������������');


end;


end.
