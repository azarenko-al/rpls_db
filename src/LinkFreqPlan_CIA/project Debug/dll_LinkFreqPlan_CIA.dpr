program dll_LinkFreqPlan_CIA;

uses
  Forms,
  dm_LinkFreqPlan_CIA in '..\src\dm_LinkFreqPlan_CIA.pas' {dmLinkFreqPlan_CIA: TDataModule},
  dm_Main_app__CIA in '..\src\dm_Main_app__CIA.pas' {dmMain_app__CIA: TDataModule},
  u_LinkFreqPlan_classes in '..\..\LinkFreqPlan_NB_Calc\src\u_LinkFreqPlan_classes.pas',
  d_LinkFreqPlan_calc_data_test in '..\src\d_LinkFreqPlan_calc_data_test.pas' {dlg_LinkFreqPlan_CIA_data_test};

{$R *.RES}




begin
  Application.Initialize;
  Application.CreateForm(TdmMain_app__CIA, dmMain_app__CIA);
  Application.CreateForm(Tdlg_LinkFreqPlan_CIA_data_test, dlg_LinkFreqPlan_CIA_data_test);
  Application.Run;
end.
