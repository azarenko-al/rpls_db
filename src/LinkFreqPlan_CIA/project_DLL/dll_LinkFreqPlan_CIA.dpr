library dll_LinkFreqPlan_CIA;

{ Important note about DLL memory management: ShareMem must be the
  first unit in your library's USES clause AND your project's (select
  Project-View Source) USES clause if your DLL exports any procedures or
  functions that pass strings as parameters or function results. This
  applies to all strings passed to and from your DLL--even those that
  are nested in records and classes. ShareMem is the interface unit to
  the BORLNDMM.DLL shared memory manager, which must be deployed along
  with your DLL. To avoid using BORLNDMM.DLL, pass string information
  using PChar or ShortString parameters. }

uses
  Classes,
  d_LinkFreqPlan_calc_data_test in '..\src\d_LinkFreqPlan_calc_data_test.pas' {dlg_LinkFreqPlan_CIA_data_test},
  dm_LinkFreqPlan_CIA in '..\src\dm_LinkFreqPlan_CIA.pas' {dmLinkFreqPlan_CIA: TDataModule},
  dm_Main_app__CIA in '..\src\dm_Main_app__CIA.pas' {dmMain_app__CIA: TDataModule},
  i_LinkFreqPlan_CIA in '..\src shared\i_LinkFreqPlan_CIA.pas',
  SysUtils,
  x_LinkFreqPlan_CIA in '..\src_dll\x_LinkFreqPlan_CIA.pas',
  u_ini_LinkFreqPlan_CIA_params in '..\src shared\u_ini_LinkFreqPlan_CIA_params.pas',
  u_LinkFreqPlan_classes in '..\..\LinkFreqPlan_NB_Calc\src\u_LinkFreqPlan_classes.pas';

{$R *.res}

begin
end.
