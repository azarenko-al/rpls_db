object dlg_Settings_WMS: Tdlg_Settings_WMS
  Left = 1016
  Top = 444
  BorderStyle = bsDialog
  BorderWidth = 5
  Caption = 'dlg_Settings_WMS'
  ClientHeight = 346
  ClientWidth = 788
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poDefault
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object cxGrid2: TcxGrid
    Left = 0
    Top = 0
    Width = 788
    Height = 137
    Align = alTop
    TabOrder = 0
    LookAndFeel.Kind = lfFlat
    LookAndFeel.NativeStyle = False
    LookAndFeel.SkinName = ''
    object cxGridDBTableView1: TcxGridDBTableView
      NavigatorButtons.ConfirmDelete = False
      DataController.DataSource = DataSource1
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsBehavior.ImmediateEditor = False
      OptionsCustomize.ColumnFiltering = False
      OptionsView.GroupByBox = False
      OptionsView.Indicator = True
      object cxGridDBTableView1id: TcxGridDBColumn
        DataBinding.FieldName = 'id'
        Visible = False
        Width = 46
      end
      object cxGridDBTableView1enabled: TcxGridDBColumn
        DataBinding.FieldName = 'enabled'
        PropertiesClassName = 'TcxCheckBoxProperties'
        Properties.NullStyle = nssUnchecked
      end
      object cxGridDBTableView1is_default: TcxGridDBColumn
        DataBinding.FieldName = 'is_default'
        PropertiesClassName = 'TcxCheckBoxProperties'
        Properties.ImmediatePost = True
        Properties.NullStyle = nssUnchecked
        Properties.OnEditValueChanged = cxGridDBTableView1is_defaultPropertiesEditValueChanged
        Width = 37
      end
      object cxGridDBTableView1Topic: TcxGridDBColumn
        DataBinding.FieldName = 'caption'
        Width = 110
      end
      object cxGridDBTableView1Name: TcxGridDBColumn
        DataBinding.FieldName = 'Name'
        Width = 120
      end
      object cxGridDBTableView1URL: TcxGridDBColumn
        DataBinding.FieldName = 'URL'
        Width = 251
      end
      object cxGridDBTableView1Matrix: TcxGridDBColumn
        DataBinding.FieldName = 'Matrix'
        Visible = False
        Width = 246
      end
      object cxGridDBTableView1EPSG: TcxGridDBColumn
        DataBinding.FieldName = 'EPSG'
      end
      object col_max_Z: TcxGridDBColumn
        DataBinding.FieldName = 'max_Z'
      end
    end
    object cxGridLevel1: TcxGridLevel
      GridView = cxGridDBTableView1
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 311
    Width = 788
    Height = 35
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 500
      Top = 0
      Width = 288
      Height = 35
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 0
      object Button1: TButton
        Left = 205
        Top = 5
        Width = 73
        Height = 25
        Cancel = True
        Caption = #1054#1090#1084#1077#1085#1072
        ModalResult = 2
        TabOrder = 0
      end
      object Button2: TButton
        Left = 120
        Top = 5
        Width = 75
        Height = 25
        Cancel = True
        Caption = 'Ok'
        ModalResult = 1
        TabOrder = 1
        OnClick = Button2Click
      end
    end
  end
  object FormPlacement1: TFormPlacement
    IniFileName = 'Software\Onega\'
    Left = 320
    Top = 160
  end
  object ADOConnection1: TADOConnection
    ConnectionString = 
      'Provider=SQLOLEDB.1;Password=sa;Persist Security Info=True;User ' +
      'ID=sa;Initial Catalog=onega_link;Data Source=server1'
    LoginPrompt = False
    Provider = 'SQLOLEDB.1'
    Left = 48
    Top = 160
  end
  object DataSource1: TDataSource
    DataSet = sp_WMS_SEL
    Left = 192
    Top = 221
  end
  object sp_WMS_SEL: TADOStoredProc
    LockType = ltBatchOptimistic
    AfterPost = t_WMSAfterPost
    Parameters = <>
    Left = 192
    Top = 168
  end
  object ADOStoredProc1: TADOStoredProc
    LockType = ltBatchOptimistic
    AfterPost = t_WMSAfterPost
    Parameters = <>
    Left = 320
    Top = 224
  end
end
