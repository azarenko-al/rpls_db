unit d_Settings_WMS;

interface

uses
  dm_Onega_db_data,
  u_db,
  u_func_msg,


  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, rxPlacemnt, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, dxSkinsDefaultPainters,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData,
  cxCheckBox, cxGridLevel, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxClasses, cxGridCustomView, cxGrid, ADODB, StdCtrls,
  ExtCtrls;

type
  Tdlg_Settings_WMS = class(TForm)
    FormPlacement1: TFormPlacement;
    cxGrid2: TcxGrid;
    cxGridDBTableView1: TcxGridDBTableView;
    cxGridLevel1: TcxGridLevel;
    ADOConnection1: TADOConnection;
    DataSource1: TDataSource;
    Panel1: TPanel;
    cxGridDBTableView1id: TcxGridDBColumn;
    cxGridDBTableView1enabled: TcxGridDBColumn;
    cxGridDBTableView1is_default: TcxGridDBColumn;
    cxGridDBTableView1Topic: TcxGridDBColumn;
    cxGridDBTableView1Name: TcxGridDBColumn;
    cxGridDBTableView1URL: TcxGridDBColumn;
    cxGridDBTableView1Matrix: TcxGridDBColumn;
    cxGridDBTableView1EPSG: TcxGridDBColumn;
    col_max_Z: TcxGridDBColumn;
    Panel3: TPanel;
    Button1: TButton;
    Button2: TButton;
    sp_WMS_SEL: TADOStoredProc;
    ADOStoredProc1: TADOStoredProc;
    procedure Button2Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure t_WMSAfterPost(DataSet: TDataSet);
    procedure cxGridDBTableView1is_defaultPropertiesEditValueChanged(
      Sender: TObject);
  private
    FDisableControls: boolean;
    { Private declarations }
  public
    class procedure ExecDlg;
    { Public declarations }
  end;

  
implementation


{$R *.dfm}


//------------------------------------------------------------------------------
class procedure Tdlg_Settings_WMS.ExecDlg;
//------------------------------------------------------------------------------
begin

  with Tdlg_Settings_WMS.Create(Application) do
  begin
//    FFolderGUID:=aFolderGUID;

  //  OpenData;

    ShowModal;
 //   if Result then
 //     Save;

    Free;
  end;

end;

//----------------------------------------------------------------
procedure Tdlg_Settings_WMS.FormCreate(Sender: TObject);
//----------------------------------------------------------------
begin
  Caption:='��������� WMS';

  if ADOConnection1.Connected then
    ShowMessage ('procedure Tdlg_Tdlg_Settings_WMSLinkFreqPlan_Add_from_map.FormCreate(Sender: TObject);');


  dmOnega_DB_data.OpenStoredProc(sp_WMS_SEL, 'map.sp_wms_SEL',['IS_SHOW_ALL', 1]);

//  dmOnega_DB_data.TableOpen(t_WMS, 'map.wms');


  db_SetFieldCaptions(sp_WMS_SEL, [
     'enabled',    '���',
     'is_default', '��������',
     'caption',    '��������' ,
     'name',       '���',
     'max_Z',      'max Z'

    // 'URL',         'URL',


  ]);


 cxGrid2.Align:=alClient;

end;

//----------------------------------------------------------------
procedure Tdlg_Settings_WMS.t_WMSAfterPost(DataSet: TDataSet);
//----------------------------------------------------------------
var
  iID: Integer;
  k: Integer;

begin
  if FDisableControls then
    Exit;

  FDisableControls:=true;

  iID:=DataSet.FieldByName('id').AsInteger;
  DataSet.DisableControls;

//  @ID int,
//  @ENABLED bit= NULL,

    k:=dmOnega_DB_data.ExecStoredProc(ADOStoredProc1, 'map.sp_wms_UPD',
    [
      'id',        DataSet['id'],
      'ENABLED',   DataSet['ENABLED'],
      'is_default',DataSet['is_default']

    ]);
//  @IS_DEFAULT  bit= NULL


  //-----------------------------------------
  // ��� �������� ������� - ������ ��������� �������
  //-----------------------------------------
  if DataSet.FieldByName('is_default').AsBoolean then
  begin

    DataSet.First;

    with DataSet do
      while not EOF do
      begin
        if FieldByName('id').AsInteger<>iID then
          db_UpdateRecord__(DataSet, ['is_default', False]);

        Next;
      end;

  end;

  //-----------------------------------------
  //  �������� ������� - ��� �� �������
  //-----------------------------------------


//exit_:
  DataSet.Locate('id', iID, []);
  DataSet.EnableControls;

  FDisableControls:=False;

end;


procedure Tdlg_Settings_WMS.cxGridDBTableView1is_defaultPropertiesEditValueChanged(
  Sender: TObject);
begin
  db_PostDataset(sp_WMS_SEL);

end;


procedure Tdlg_Settings_WMS.Button2Click(Sender: TObject);
begin
  db_PostDataset(sp_WMS_SEL);

  g_EventManager.PostEvent_(et_MAP_REFRESH_WMS,[]);
end;



end.

{

procedure Tdlg_Settings_WMS.Save;
begin


end;

