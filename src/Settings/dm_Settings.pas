unit dm_Settings;

interface

uses
  dm_Onega_DB_data,
  u_db,


  Classes, DB, Forms, ADODB;

type
  TdmSettings = class(TDataModule)
    t_WMS: TADOTable;
    t_Settings: TADOTable;
    ds_WMS: TDataSource;
    sp_WMS_SEL: TADOStoredProc;
    procedure DataModuleCreate(Sender: TObject);
  private
    procedure Refresh_Data;
  public
    function ParamByName(aParam: string): integer;
    class function Init: TdmSettings;

    procedure Open_WMS;
    function WMS_Get_Default_Map_name: string;
  end;

const
  DEF_LIST_delete_max_count = 'list_delete_max_count';
  DEF_link_min_length_m     = 'link_min_length_m';

var
  dmSettings: TdmSettings;

implementation

{$R *.dfm}

// ---------------------------------------------------------------
class function TdmSettings.Init: TdmSettings;
// ---------------------------------------------------------------
begin
  if not Assigned(dmSettings) then
    dmSettings := TdmSettings.Create(Application);

  Result:=dmSettings;
end;


procedure TdmSettings.DataModuleCreate(Sender: TObject);
begin
//  dmOnega_DB_data.TableOpen(t_WMS, 'map.wms');
  Refresh_Data
//  dmOnega_DB_data.TableOpen(t_Settings, 'Settings');
end;


procedure TdmSettings.Refresh_Data;
begin
//  dmOnega_DB_data.TableOpen(t_WMS, 'map.wms');
  dmOnega_DB_data.TableOpen(t_Settings, 'Settings');
end;


procedure TdmSettings.Open_WMS;
begin
//  dmOnega_DB_data.TableOpen(t_WMS, 'map.wms');
  //dmOnega_DB_data.OpenQuery (q_WMS, 'select * from map.view_wms');


  dmOnega_DB_data.OpenStoredProc(sp_WMS_SEL, 'map.sp_wms_SEL',[]);

end;

//----------------------------------------------------------------
function TdmSettings.WMS_Get_Default_Map_name: string;
//----------------------------------------------------------------
begin
   Assert(sp_WMS_SEL.RecordCount>0);

   sp_WMS_SEL.Locate('is_default',true,[]);
   Result:=sp_WMS_SEL['name']

end;


function TdmSettings.ParamByName(aParam: string): integer;
begin
  if t_Settings.Locate(FLD_NAME, aParam, []) then
    Result :=t_Settings.FieldByName('value').AsInteger

//  else
 //   raise;
 //   dmOnega_DB_data.o

//  Result := 999;
end;


end.
