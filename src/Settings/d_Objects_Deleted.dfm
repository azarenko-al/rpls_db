object dlg_Objects_Deleted: Tdlg_Objects_Deleted
  Left = 803
  Top = 355
  Width = 814
  Height = 395
  BorderWidth = 5
  Caption = 'dlg_Objects_Deleted'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poDefault
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object cxGrid2: TcxGrid
    Left = 0
    Top = 0
    Width = 788
    Height = 137
    Align = alTop
    PopupMenu = PopupMenu1
    TabOrder = 0
    LookAndFeel.Kind = lfFlat
    LookAndFeel.NativeStyle = False
    LookAndFeel.SkinName = ''
    object cxGridDBTableView1: TcxGridDBTableView
      NavigatorButtons.ConfirmDelete = False
      DataController.DataSource = DataSource1
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsBehavior.ImmediateEditor = False
      OptionsCustomize.ColumnsQuickCustomization = True
      OptionsData.Deleting = False
      OptionsData.Editing = False
      OptionsSelection.MultiSelect = True
      OptionsView.GroupByBox = False
      OptionsView.Indicator = True
      object col_ID: TcxGridDBColumn
        DataBinding.FieldName = 'id'
        Visible = False
      end
      object col_table_name: TcxGridDBColumn
        DataBinding.FieldName = 'table_name'
        Width = 136
      end
      object cxGridDBTableView1name: TcxGridDBColumn
        DataBinding.FieldName = 'name'
        Width = 162
      end
      object cxGridDBTableView1deleted_date: TcxGridDBColumn
        DataBinding.FieldName = 'deleted_date'
      end
      object cxGridDBTableView1deleted_user: TcxGridDBColumn
        DataBinding.FieldName = 'deleted_user'
        Width = 111
      end
    end
    object cxGridLevel1: TcxGridLevel
      GridView = cxGridDBTableView1
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 311
    Width = 788
    Height = 35
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 500
      Top = 0
      Width = 288
      Height = 35
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 0
      object Button1: TButton
        Left = 205
        Top = 5
        Width = 73
        Height = 25
        Cancel = True
        Caption = #1054#1090#1084#1077#1085#1072
        ModalResult = 2
        TabOrder = 0
      end
    end
  end
  object FormPlacement1: TFormPlacement
    IniFileName = 'Software\Onega\'
    Left = 40
    Top = 224
  end
  object ADOConnection1: TADOConnection
    ConnectionString = 
      'Provider=SQLOLEDB.1;Password=sa;Persist Security Info=True;User ' +
      'ID=sa;Initial Catalog=onega_link;Data Source=server1'
    LoginPrompt = False
    Provider = 'SQLOLEDB.1'
    Left = 48
    Top = 160
  end
  object DataSource1: TDataSource
    DataSet = sp_SEL
    Left = 192
    Top = 221
  end
  object sp_SEL: TADOStoredProc
    Connection = ADOConnection1
    CursorType = ctStatic
    LockType = ltReadOnly
    ProcedureName = 'db_access.sp_Objects_deleted_SEL'
    Parameters = <>
    Left = 192
    Top = 168
  end
  object ADOStoredProc1: TADOStoredProc
    LockType = ltBatchOptimistic
    Parameters = <>
    Left = 320
    Top = 224
  end
  object PopupMenu1: TPopupMenu
    Left = 472
    Top = 168
    object actRestore1: TMenuItem
      Action = act_Restore
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Del1: TMenuItem
      Action = act_Del
    end
  end
  object ActionList1: TActionList
    Left = 472
    Top = 224
    object act_Restore: TAction
      Caption = 'act_Restore'
      OnExecute = act_RestoreExecute
    end
    object act_Del: TAction
      Caption = 'Del'
      OnExecute = act_DelExecute
    end
  end
end
