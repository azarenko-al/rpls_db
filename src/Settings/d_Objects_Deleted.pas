unit d_Objects_Deleted;

interface

uses
  dm_Onega_db_data,
  u_db,
  u_cx,
  u_dlg,
  u_const,


  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, rxPlacemnt, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, dxSkinsDefaultPainters,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData,
  cxCheckBox, cxGridLevel, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxClasses, cxGridCustomView, cxGrid, ADODB, StdCtrls,
  ExtCtrls, Menus, ActnList;

type
  Tdlg_Objects_Deleted = class(TForm)
    FormPlacement1: TFormPlacement;
    cxGrid2: TcxGrid;
    cxGridDBTableView1: TcxGridDBTableView;
    cxGridLevel1: TcxGridLevel;
    ADOConnection1: TADOConnection;
    DataSource1: TDataSource;
    Panel1: TPanel;
    Panel3: TPanel;
    Button1: TButton;
    sp_SEL: TADOStoredProc;
    ADOStoredProc1: TADOStoredProc;
    col_ID: TcxGridDBColumn;
    cxGridDBTableView1name: TcxGridDBColumn;
    col_table_name: TcxGridDBColumn;
    cxGridDBTableView1deleted_date: TcxGridDBColumn;
    cxGridDBTableView1deleted_user: TcxGridDBColumn;
    PopupMenu1: TPopupMenu;
    ActionList1: TActionList;
    act_Restore: TAction;
    actRestore1: TMenuItem;
    act_Del: TAction;
    Del1: TMenuItem;
    N1: TMenuItem;
    procedure act_DelExecute(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure act_RestoreExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    FRegPath : string;
    FRefresh: boolean;

    procedure Refresh_Data;
  public
    class function ExecDlg: Boolean;

  end;


implementation
{$R *.dfm}



//------------------------------------------------------------------------------
class function Tdlg_Objects_Deleted.ExecDlg: Boolean;
//------------------------------------------------------------------------------
begin

  with Tdlg_Objects_Deleted.Create(Application) do
  begin

    ShowModal;
    Result:=FRefresh;

 //   if Result then
 //     Save;

    Free;
  end;

end;

//----------------------------------------------------------------
procedure Tdlg_Objects_Deleted.FormCreate(Sender: TObject);
//----------------------------------------------------------------
begin
  Caption:='��������� �������';

  if ADOConnection1.Connected then
    ShowMessage ('procedure Tdlg_Objects_Deleted.FormCreate(Sender: TObject);');

  Refresh_Data;

//  dmOnega_DB_data.OpenStoredProc(sp_WMS_SEL, 'db_access.sp_Objects_deleted_SEL',[]);

//  dmOnega_DB_data.TableOpen(t_WMS, 'map.wms');

  act_Del.Caption:='�������';

  db_SetFieldCaptions(sp_SEL, [
     'table_name',      '������',
     'name',         '��������',
     'deleted_date', '����',
     'deleted_user', '������������'

  ]);

  act_Restore.Caption:='������������';

  cxGrid2.Align:=alClient;

  FRegPath := REGISTRY_ROOT + ClassName + '\v1'; //   = 'Software\Onega\RPLS_DB_LINK\';
  cxGridDBTableView1.RestoreFromRegistry(FRegPath);

end;


procedure Tdlg_Objects_Deleted.FormDestroy(Sender: TObject);
begin
  cxGridDBTableView1.StoreToRegistry(FRegPath, True, [gsoUseFilter]);
end;


procedure Tdlg_Objects_Deleted.Refresh_Data;
begin
  dmOnega_DB_data.OpenStoredProc(sp_SEL, 'db_access.sp_Objects_deleted_SEL',[]);


end;

//----------------------------------------------------------------
procedure Tdlg_Objects_Deleted.act_RestoreExecute(Sender: TObject);
//----------------------------------------------------------------
var
  i: Integer;
  iID: Integer;
  k: Integer;
  sTableName: string;
begin
  with cxGridDBTableView1.Controller do
    for i := 0 to SelectedRowCount - 1 do
    begin
      iID       :=SelectedRows[i].Values[col_ID.Index];
      sTableName:=SelectedRows[i].Values[col_table_name.Index];


      k:=dmOnega_DB_data.ExecStoredProc(ADOStoredProc1, 'db_access.sp_Delete_restore_UPD',
          [
            'id', iID,
            'table_name', sTableName
          ]);

    end;



  Refresh_Data;
  FRefresh:=True;

end;

//----------------------------------------------------------------
procedure Tdlg_Objects_Deleted.act_DelExecute(Sender: TObject);
//----------------------------------------------------------------
var
  i: Integer;
  iID: Integer;
  k: Integer;
  sTableName: string;
begin
  if not ConfirmDlg('������� ?') then
    Exit;

  with cxGridDBTableView1.Controller do
    for i := 0 to SelectedRowCount - 1 do
    begin
      iID       :=SelectedRows[i].Values[col_ID.Index];
      sTableName:=SelectedRows[i].Values[col_table_name.Index];


      k:=dmOnega_DB_data.ExecStoredProc(ADOStoredProc1, 'db_access.sp_Delete_DEL',
          [
            'id', iID,
            'table_name', sTableName
          ]);

    end;



  Refresh_Data;
  FRefresh:=True;
end;



end.

{

procedure Tdlg_Settings_WMS.Save;
begin


end;

