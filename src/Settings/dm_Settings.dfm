object dmSettings: TdmSettings
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Left = 1453
  Top = 645
  Height = 369
  Width = 334
  object t_WMS: TADOTable
    CursorType = ctStatic
    TableName = 'map.wms'
    Left = 96
    Top = 48
  end
  object t_Settings: TADOTable
    CursorType = ctStatic
    TableName = 'Settings'
    Left = 96
    Top = 136
  end
  object ds_WMS: TDataSource
    DataSet = sp_WMS_SEL
    Left = 208
    Top = 104
  end
  object sp_WMS_SEL: TADOStoredProc
    LockType = ltBatchOptimistic
    Parameters = <>
    Left = 208
    Top = 48
  end
end
