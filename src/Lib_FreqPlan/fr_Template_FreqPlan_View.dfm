inherited frame_Template_FreqPlan_View: Tframe_Template_FreqPlan_View
  Left = 1170
  Top = 317
  Width = 490
  Height = 536
  Caption = 'frame_Template_FreqPlan_View'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited Panel1: TPanel
    Width = 474
    TabOrder = 1
  end
  inherited pn_Caption: TPanel
    Width = 474
    TabOrder = 0
  end
  inherited pn_Main: TPanel
    Width = 474
    Height = 243
    object cxSplitter1: TcxSplitter
      Left = 1
      Top = 120
      Width = 472
      Height = 8
      HotZoneClassName = 'TcxSimpleStyle'
      AlignSplitter = salBottom
      Control = PageControl1
    end
    object pn_Inspector: TPanel
      Left = 1
      Top = 1
      Width = 472
      Height = 100
      Align = alTop
      BevelOuter = bvLowered
      Caption = 'pn_Inspector'
      Constraints.MinHeight = 100
      TabOrder = 1
    end
    object PageControl1: TPageControl
      Left = 1
      Top = 128
      Width = 472
      Height = 114
      ActivePage = ts_Bands
      Align = alBottom
      MultiLine = True
      Style = tsFlatButtons
      TabOrder = 2
      object ts_Bands: TTabSheet
        Caption = #1063#1072#1089#1090#1086#1090#1085#1099#1077' '#1087#1083#1072#1085#1099
        ImageIndex = 1
      end
    end
  end
  inherited MainActionList: TActionList
    Left = 20
    Top = 4
  end
  inherited ImageList1: TImageList
    Left = 48
    Top = 4
  end
  inherited PopupMenu1: TPopupMenu
    Left = 112
    Top = 4
  end
  inherited cxPropertiesStore: TcxPropertiesStore
    Left = 226
    Top = 2
  end
  inherited cxLookAndFeelController1: TcxLookAndFeelController
    Left = 80
    Top = 368
  end
  inherited dxBarManager1: TdxBarManager
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    Left = 80
    Top = 416
    DockControlHeights = (
      0
      0
      27
      0)
  end
end
