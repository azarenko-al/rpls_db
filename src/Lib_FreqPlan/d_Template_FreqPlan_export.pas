unit d_Template_FreqPlan_export;

interface

uses
  dm_Onega_DB_data,
  dm_main,
  u_cx,

  u_LinkEndType_str,

  u_db,
  u_debug,

  u_const_db,
  u_const_str,

 // dm_LinkEndType_Band_View,
//  dm_LinkEndType,

  u_link_const,

  u_LinkEndType_const,

//  u_LinkFreqPlan_const,


  SysUtils, Variants, Classes, Controls, Forms,
  StdCtrls, DB, DBGrids, ADODB,  cxGridLevel,
  cxGridCustomTableView, cxGridDBTableView,
  cxGrid, ComCtrls, dialogs,
  CheckLst, dxmdaset, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, cxDBData, cxGridTableView, cxClasses,
  cxGridCustomView, Grids, dxSkinsCore, dxSkinsDefaultPainters, ActnList,
  ExtCtrls, rxPlacemnt;

type
  Tdlg_Template_FreqPlan_export_rec_ = record
    LinkEndType_id: integer;

//    Band: string;
//    Recommendation: string;
  end;


  Tdlg_Template_FreqPlan_export = class(TForm)
    ADOConnection1: TADOConnection;
    DBGrid1: TDBGrid;
    q_Band: TADOQuery;
    ds_Band: TDataSource;
    DBGrid2: TDBGrid;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    DBGrid3: TDBGrid;
    PageControl1: TPageControl;
    TabSheet_Bands: TTabSheet;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView_bands: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    ds_Channels: TDataSource;
    sp_Band_Channels: TADOStoredProc;
    col_checked: TcxGridDBColumn;
    col_id: TcxGridDBColumn;
    cxGrid1DBTableView_bandsLinkEndType_id: TcxGridDBColumn;
    col_name: TcxGridDBColumn;
    cxGrid1DBTableView_bandslow: TcxGridDBColumn;
    cxGrid1DBTableView_bandshigh: TcxGridDBColumn;
    cxGrid1DBTableView_bandschannel_count: TcxGridDBColumn;
    cxGrid1DBTableView_bandschannel_width: TcxGridDBColumn;
    cxGrid1DBTableView_bandsfreq_min_low: TcxGridDBColumn;
    cxGrid1DBTableView_bandsfreq_max_low: TcxGridDBColumn;
    cxGrid1DBTableView_bandsfreq_min_high: TcxGridDBColumn;
    cxGrid1DBTableView_bandsfreq_max_high: TcxGridDBColumn;
    cxGrid1DBTableView_bandstxrx_shift: TcxGridDBColumn;
    col_bandwidth: TcxGridDBColumn;
    q_Recommendation: TADOQuery;
    ds_Recommendation: TDataSource;
    q_Vendor: TADOQuery;
    ds_Vendor: TDataSource;
    col_name_vendor: TcxGridDBColumn;
    Label4: TLabel;
    Label5: TLabel;
    CheckListBox_SubBand: TCheckListBox;
    q_LinkEndType: TADOQuery;
    ADOStoredProc1: TADOStoredProc;
    CheckListBox_bandwidth: TCheckListBox;
    ActionList1: TActionList;
    act_Copy: TAction;
    Panel1: TPanel;
    Button3: TButton;
    Button4: TButton;
    Button1: TButton;
    Button2: TButton;
    b_Filter: TButton;
    FormStorage1: TFormStorage;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure b_FilterClick(Sender: TObject);
    procedure CheckListBox_bandwidthClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure q_VendorAfterScroll(DataSet: TDataSet);
  private
    FParams: Tdlg_Template_FreqPlan_export_rec_;

    FIsDataExported: boolean;

    procedure Export_Data;
    procedure Filter;
    procedure OpenData;
    { Private declarations }
  public
    class function ExecDlg(var aRec: Tdlg_Template_FreqPlan_export_rec_): Boolean;

  end;

//var
//  dlg_Template_FreqPlan_export: Tdlg_Template_FreqPlan_export;

implementation
{$R *.dfm}

{
const
  CAPTION_NAME   = '������������';

  CAPTION_High = '������������ ������������ High';
  CAPTION_Low  = '������������ ������������ Low';

//  CAPTION_DUPLEX_FREQ_DELTA = '������ ������ ���/��� [MHz]';
//  CAPTION_CHANNEL_WIDTH     = '��� ����� ������ [MHz]'; //'������ ���������� ������ [MHz]';
  CAPTION_CHANNEL_WIDTH     = '������ ������� ������ [MHz]'; //'������ ���������� ������ [MHz]';
  CAPTION_DUPLEX_FREQ_DELTA = '���������� ������ ������ ���/��� [MHz]';

//������� ��������� �����:
//- ��� ����� ������ [MHz]  = ������ ������� ������ [MHz]
//- ������ ������ ���/��� [MHz] = ���������� ������ ������ ���/��� [MHz]



  CAPTION_FREQ_MIN          = 'Min ������� ������� %s [MHz]';
  CAPTION_FREQ_MAX          = 'Max ������� ������� %s [MHz]';
  CAPTION_CH_COUNT          = '���-�� ������';

  CAPTION_CHANNEL            = '�����';

  CAPTION_CH_LOW            = '����� Low';
  CAPTION_CH_HIGH           = '����� High';
  CAPTION_LOW_TX_FREQ       = '������� ��� [MHz]';
  CAPTION_LOW_RX_FREQ       = '������� ��� [MHz]';

  CAPTION_HIGH_TX_FREQ       = '������� ��� [MHz]';
  CAPTION_HIGH_RX_FREQ       = '������� ��� [MHz]';


  CAPTION_SPEED            = '�������� �������� [Mbit/s]';
  CAPTION_CH_SPACING       = '������ ������� [MHz]';
  CAPTION_MODULATION_TYPE  = '��� ���������';
  CAPTION_MODE             = '�����';

}

procedure Tdlg_Template_FreqPlan_export.Button1Click(Sender: TObject);
begin
  OpenData
end;

procedure Tdlg_Template_FreqPlan_export.Button2Click(Sender: TObject);
begin
  Export_Data
end;



//----------------------------------------------------------------
procedure Tdlg_Template_FreqPlan_export.OpenData;
//----------------------------------------------------------------
var
  i: Integer;
  k: Integer;
  sName: string;
begin
  dmOnega_DB_data.OpenStoredProc(sp_Band_Channels, sp_Band_Channels.ProcedureName,
  [
    'band',            q_Band.FieldByName('band').AsString,
    'recommendation',  q_Recommendation.FieldByName('recommendation').AsString,
    'vendor_id',       q_Vendor.FieldByName('vendor_id').AsInteger
  ]);


  sp_Band_Channels.DisableControls;

  CheckListBox_SubBand.Items.Clear;
  CheckListBox_bandwidth.Items.Clear;


  with sp_Band_Channels do
    while not EOF do
    begin
      sName:=FieldByName('bandwidth').AsString;
      if (sName<>'') and (CheckListBox_bandwidth.Items.IndexOf(sName)<0) then
        CheckListBox_bandwidth.Items.Add(sName);

      sName:=FieldByName('name_vendor').AsString;
      if (sName<>'') and (CheckListBox_SubBand.Items.IndexOf(sName)<0) then
        CheckListBox_SubBand.Items.Add(sName);

      Next;
    end;


  for i:=0 to CheckListBox_SubBand.Items.Count-1 do
    CheckListBox_SubBand.Checked[i]:=True;

  for i:=0 to CheckListBox_bandwidth.Items.Count-1 do
    CheckListBox_bandwidth.Checked[i]:=True;


  sp_Band_Channels.First;
  sp_Band_Channels.EnableControls;


end;

//  CAPTION_BANDWIDTH        = '������ ������ [MHz]';


// ---------------------------------------------------------------
class function Tdlg_Template_FreqPlan_export.ExecDlg(var aRec:
    Tdlg_Template_FreqPlan_export_rec_): Boolean;
// ---------------------------------------------------------------
var
  sBand: string;
  sRecommendation: string;
begin
  with Tdlg_Template_FreqPlan_export.Create(Application) do
  begin
    FParams:=aRec;

//    q_Band.Filter:='band=' + aRec.Band; // + '''' ;
//    q_Band.Filtered:=true;

     dmOnega_DB_data.OpenQuery(q_LinkEndType, 'select * from LinkEndType where id=:id', ['id', arec.LinkEndType_id]);

     sBand          :=q_LinkEndType.FieldByName('band').AsString;
     sRecommendation:=q_LinkEndType.FieldByName('Recommendation').AsString;

    if sBand<>'' then
    begin
      q_Band.Filter:='band=''' + sBand + '''' ;
      q_Band.Filtered:=true;
    end;

    if sRecommendation<>'' then
    begin
      q_Recommendation.Filter:='Recommendation=''' + sRecommendation + '''' ;
      q_Recommendation.Filtered:=true;
    end;


//    if sRecommendation<>'' then
//      q_Recommendation.Locate ('Recommendation', sRecommendation, []);

//    q_Recommendation.Filtered:=true;

 //   dmOnega_DB_data.OpenQuery_(ADOQuery1, 'select * from status where band is not null', []);
//    dmOnega_DB_data.OpenQuery_(ADOQuery1, 'select * from status where band=:band', ['band',aRec.Band]);

//    db_TableOpen1(t_Recommendation);
//    db_TableOpen1(t_lib_Vendor);

//     t_Recommendation.Filter:='band='+ aRec.Band;
//     t_Recommendation.Filtered:=true;

//    if aRec.Vendor_ID>0 then
//     if t_Recommendation.RecordCount>0 then
//     ADOQuery1.Locate(FLD_NAME, aRec.Recommendation, []);

//    if aRec.Vendor_Equipment_ID>0 then
//      t_lib_Vendor_Equipment.Locate(FLD_ID, aRec.Vendor_Equipment_ID, []);


    ShowModal;

    Result := FIsDataExported=true;

    {
    Result := ModalResult=mrOk;

    if ModalResult=mrOk then
    begin
      aRec.Band           := ADOQuery1.FieldByName(FLD_Band).AsString;
      aRec.Recommendation := ADOQuery1.FieldByName(FLD_name).AsString;

      aRec.freq_min_MHz := ADOQuery1.FieldByName(FLD_freq_min_MHz).AsFloat;
      aRec.freq_max_MHz := ADOQuery1.FieldByName(FLD_freq_max_MHz).AsFloat;
    end;
     }
    Free;
  end;

end;

//----------------------------------------------------------------
procedure Tdlg_Template_FreqPlan_export.FormCreate(Sender: TObject);
//----------------------------------------------------------------
begin
  if ADOConnection1.Connected then
    ShowMessage('ADOConnection1.Connected');

  Caption:='������ ...';

  cxGrid1.Align:=alClient;

  db_SetComponentADOConnection(Self, dmMain.ADOConnection1 );

  q_Band.Close;
  q_Recommendation.Close;
  q_Vendor.Close;

  q_Band.Open;
  q_Recommendation.Open;
  q_Vendor.Open;

  TabSheet_Bands.Caption:= '�����������';


//  col_name1.Caption:= '������������';

//  col_name_vendor.Caption := '������������ ������������ �������������';




 cx_SetColumnCaptions(cxGrid1DBTableView_bands,
     [
      'checked', '���',

      'name', '������������',
      'name_vendor', '������������ ������������ �������������',

       FLD_High,  CAPTION_High,
       FLD_Low,   CAPTION_LOW,

       FLD_freq_min_low,  Format(CAPTION_FREQ_MIN, ['Low']),
       FLD_freq_max_low,  Format(CAPTION_FREQ_MAX, ['Low']),
       FLD_freq_min_high, Format(CAPTION_FREQ_MIN, ['High']),
       FLD_freq_max_high, Format(CAPTION_FREQ_MAX, ['High']),

       FLD_Channel_width, CAPTION_CHANNEL_WIDTH,
       FLD_Channel_count, CAPTION_CH_COUNT,

       FLD_TxRx_Shift,    CAPTION_DUPLEX_FREQ_DELTA,
       FLD_BANDWIDTH,     CAPTION_Bandwidth_signal

     ] );


end;

procedure Tdlg_Template_FreqPlan_export.q_VendorAfterScroll(DataSet: TDataSet);
begin
  OpenData;
end;

procedure Tdlg_Template_FreqPlan_export.b_FilterClick(Sender: TObject);
begin
  Filter;
end;

procedure Tdlg_Template_FreqPlan_export.CheckListBox_bandwidthClick(Sender:
    TObject);
begin
  Filter
end;

 //----------------------------------------------------------------
procedure Tdlg_Template_FreqPlan_export.Filter;
//----------------------------------------------------------------
var
  i: Integer;
  s: string;
//  s: string;
//  oDataSet: TDataSet;
  sFilter: string;
  sFilter_name: string;
begin


//  for i:=0 to CheckListBox_SubBand.Items.Count-1 do
//    CheckListBox_SubBand.Checked[i]:=True;
  sFilter:='';
  s:='';

  for i:=0 to CheckListBox_bandwidth.Items.Count-1 do
    if CheckListBox_bandwidth.Checked[i] then
    begin
      s :=s + IIF(s='','', ',') + CheckListBox_bandwidth.Items[i];
      sFilter :=sFilter + IIF(sFilter='','', ' or ') + '(bandwidth='+CheckListBox_bandwidth.Items[i] +')';
    end;

  sFilter_name:='';
  s:='';

  for i:=0 to CheckListBox_SubBand.Items.Count-1 do
    if CheckListBox_SubBand.Checked[i] then
    begin
      s :=s + IIF(s='','', ',') + CheckListBox_SubBand.Items[i];

      sFilter_name :=sFilter_name + IIF(sFilter_name='','', ' or ') + Format('(name=''%s'')',[CheckListBox_SubBand.Items[i]]);
    end;


Debug (sFilter);
Debug (sFilter_name);

//  if (sFilter<>'') and (sFilter_name<>'') then
//    sFilter:=Format('(%s) and (%s)',[sFilter,sFilter_name]);
//    sFilter:=Format('(%s) and (%s)',[sFilter,sFilter_name]);


  sp_Band_Channels.Filter:=sFilter;
  sp_Band_Channels.Filtered:=True;

{
  dmOnega_DB_data.OpenStoredProc(sp_Band_Channels, sp_Band_Channels.ProcedureName,
  [
    'band',            q_Band.FieldByName('band').AsString,
    'recommendation',  q_Recommendation.FieldByName('recommendation').AsString,
    'vendor_id',       q_Vendor.FieldByName('vendor_id').AsInteger
  ]);


exit;


  oDataset:= mem_Bandwidth;

  oDataset.First;
  sFilter:='';

  with oDataset do
    while not EOF do
    begin
      if FieldValues[FLD_CHECKED] then
        sFilter :=IIF(sFilter='','',' or ') + '(bandwidth='+FieldByName(FLD_BANDWIDTH).AsString +')';

//          db_UpdateRecord__(DataSet, ['checked', False]);

      Next;
    end;


  sp_Band_Channels.Filter:=sFilter;
  sp_Band_Channels.Filtered:=True;
  }
end;


//----------------------------------------------------------------
procedure Tdlg_Template_FreqPlan_export.Export_Data;
//----------------------------------------------------------------
var
  i: Integer;
  oStrList: TStringList;
begin
  oStrList:=TStringList.Create;

  cx_GetCheckedItemsToStrList(cxGrid1DBTableView_bands, oStrList);


  for i:=0 to oStrList.Count-1 do
    dmOnega_DB_data.ExecStoredProc (ADOStoredProc1, 'lib_freqplan.sp_LinkEndType_Band_Export_INS',
    [
      'LinkEndType_ID',               FParams.LinkEndType_id,
      'Template_LinkEndType_Band_ID', oStrList[i]
    ]);

  freeAndNil (oStrList);


  FIsDataExported:=true;

  {
  procedure cx_GetCheckedItemsToStrList(aGrid: TcxGridDBTableView; aOutStrList:   TStringList; aKeyFieldName: string = 'id'; aCheckFieldName: string =  'checked'); overload;



CREATE PROCEDURE lib_freqplan.sp_LinkEndType_Band_Export_INS
(
	@LinkEndType_ID int,
	@Template_LinkEndType_Band_ID int

)
   }

end;

end.

{
  procedure cx_GetCheckedItemsToStrList(aGrid: TcxGridDBTableView; aOutStrList:
      TStringList; aKeyFieldName: string = 'id'; aCheckFieldName: string =  'checked'); overload;


      
CREATE PROCEDURE lib_freqplan.sp_LinkEndType_Band_Export_INS
(
	@LinkEndType_ID int,
	@Template_LinkEndType_Band_ID int

)





//----------------------------------------------------------------
procedure Tdlg_Filter.sp_Filter_items_selAfterPost(DataSet: TDataSet);
//----------------------------------------------------------------
var
  iID: Integer;
//label
//  exit_;
begin
  if FDisableControls then
    Exit;

  FDisableControls:=true;

  iID:=DataSet.FieldByName('id').AsInteger;
  DataSet.DisableControls;

  //-----------------------------------------
  // ��� �������� ������� - ������ ��������� �������
  //-----------------------------------------
  if DataSet.FieldByName('checked').AsBoolean and (DataSet.FieldByName('values').AsString='') then
  begin
    DataSet.First;

    with DataSet do
      while not EOF do
      begin
        if FieldByName('id').AsInteger<>iID then
          db_UpdateRecord__(DataSet, ['checked', False]);

        Next;
      end;

  end else

  //-----------------------------------------
  //  �������� ������� - ��� �� �������
  //-----------------------------------------
  if DataSet.FieldByName('checked').AsBoolean and (DataSet.FieldByName('values').AsString<>'') then
    if DataSet.Locate('name','���',[]) then
      db_UpdateRecord__(DataSet, ['checked', False]);


//exit_:
  DataSet.Locate('id', iID, []);
  DataSet.EnableControls;

  FDisableControls:=False;

end;

procedure Tdlg_Filter.cxGrid1DBTableView1Column1PropertiesEditValueChanged(
  Sender: TObject);
begin
//  ShowMessage (Sender.className);
  db_PostDataset(sp_Filter_items_sel);

end;

end.


