object dlg_Template_FreqPlan_export: Tdlg_Template_FreqPlan_export
  Left = 498
  Top = 254
  Width = 1065
  Height = 642
  Caption = 'dlg_Template_FreqPlan_export'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 4
    Width = 84
    Height = 13
    Caption = #1044#1080#1072#1087#1072#1079#1086#1085' [GHz] '
  end
  object Label2: TLabel
    Left = 104
    Top = 4
    Width = 115
    Height = 13
    Caption = #1056#1077#1082#1086#1084#1077#1085#1076#1072#1094#1080#1103' '#1052#1057#1069'-R '
  end
  object Label3: TLabel
    Left = 256
    Top = 4
    Width = 79
    Height = 13
    Caption = #1055#1088#1086#1080#1079#1074#1086#1076#1080#1090#1077#1083#1100
  end
  object Label4: TLabel
    Left = 408
    Top = 4
    Width = 76
    Height = 13
    Caption = #1055#1086#1076#1076#1080#1072#1087#1072#1079#1086#1085#1099
  end
  object Label5: TLabel
    Left = 520
    Top = 4
    Width = 124
    Height = 13
    Caption = #1064#1080#1088#1080#1085#1072' '#1087#1086#1083#1086#1089#1099' '#1089#1080#1075#1085#1072#1083#1072
  end
  object DBGrid1: TDBGrid
    Left = 8
    Top = 24
    Width = 81
    Height = 140
    DataSource = ds_Band
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
  end
  object DBGrid2: TDBGrid
    Left = 103
    Top = 24
    Width = 138
    Height = 140
    DataSource = ds_Recommendation
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
  end
  object DBGrid3: TDBGrid
    Left = 256
    Top = 24
    Width = 138
    Height = 140
    DataSource = ds_Vendor
    TabOrder = 2
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
  end
  object PageControl1: TPageControl
    Left = 0
    Top = 184
    Width = 1049
    Height = 378
    ActivePage = TabSheet_Bands
    Align = alBottom
    Style = tsFlatButtons
    TabOrder = 3
    object TabSheet_Bands: TTabSheet
      Caption = 'Bands'
      object cxGrid1: TcxGrid
        Left = 0
        Top = 0
        Width = 1041
        Height = 169
        Align = alTop
        TabOrder = 0
        LookAndFeel.Kind = lfFlat
        object cxGrid1DBTableView_bands: TcxGridDBTableView
          NavigatorButtons.ConfirmDelete = False
          DataController.DataSource = ds_Channels
          DataController.Filter.MaxValueListCount = 1000
          DataController.KeyFieldNames = 'id'
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          Filtering.ColumnPopup.MaxDropDownItemCount = 12
          OptionsCustomize.ColumnFiltering = False
          OptionsCustomize.ColumnsQuickCustomization = True
          OptionsView.CellAutoHeight = True
          OptionsView.GroupByBox = False
          OptionsView.GroupFooters = gfVisibleWhenExpanded
          OptionsView.HeaderAutoHeight = True
          OptionsView.Indicator = True
          Preview.AutoHeight = False
          Preview.MaxLineCount = 2
          object col_checked: TcxGridDBColumn
            DataBinding.FieldName = 'checked'
          end
          object col_id: TcxGridDBColumn
            DataBinding.FieldName = 'id'
            Visible = False
            Width = 51
          end
          object cxGrid1DBTableView_bandsLinkEndType_id: TcxGridDBColumn
            DataBinding.FieldName = 'LinkEndType_id'
            Visible = False
          end
          object col_name_vendor: TcxGridDBColumn
            DataBinding.FieldName = 'name_vendor'
            Options.Editing = False
            Width = 80
          end
          object col_name: TcxGridDBColumn
            DataBinding.FieldName = 'name'
            Options.Editing = False
            Width = 71
          end
          object cxGrid1DBTableView_bandslow: TcxGridDBColumn
            DataBinding.FieldName = 'low'
            Options.Editing = False
          end
          object cxGrid1DBTableView_bandshigh: TcxGridDBColumn
            DataBinding.FieldName = 'high'
            Options.Editing = False
          end
          object cxGrid1DBTableView_bandschannel_count: TcxGridDBColumn
            DataBinding.FieldName = 'channel_count'
            Options.Editing = False
          end
          object cxGrid1DBTableView_bandschannel_width: TcxGridDBColumn
            DataBinding.FieldName = 'channel_width'
            Options.Editing = False
          end
          object cxGrid1DBTableView_bandsfreq_min_low: TcxGridDBColumn
            DataBinding.FieldName = 'freq_min_low'
            Options.Editing = False
          end
          object cxGrid1DBTableView_bandsfreq_max_low: TcxGridDBColumn
            DataBinding.FieldName = 'freq_max_low'
            Options.Editing = False
          end
          object cxGrid1DBTableView_bandsfreq_min_high: TcxGridDBColumn
            DataBinding.FieldName = 'freq_min_high'
            Options.Editing = False
          end
          object cxGrid1DBTableView_bandsfreq_max_high: TcxGridDBColumn
            DataBinding.FieldName = 'freq_max_high'
            Options.Editing = False
          end
          object cxGrid1DBTableView_bandstxrx_shift: TcxGridDBColumn
            DataBinding.FieldName = 'txrx_shift'
            Options.Editing = False
          end
          object col_bandwidth: TcxGridDBColumn
            DataBinding.FieldName = 'bandwidth'
            Options.Editing = False
          end
        end
        object cxGrid1Level1: TcxGridLevel
          GridView = cxGrid1DBTableView_bands
        end
      end
    end
  end
  object CheckListBox_SubBand: TCheckListBox
    Left = 408
    Top = 24
    Width = 105
    Height = 140
    ItemHeight = 13
    TabOrder = 4
    OnClick = CheckListBox_bandwidthClick
  end
  object CheckListBox_bandwidth: TCheckListBox
    Left = 520
    Top = 24
    Width = 111
    Height = 140
    ItemHeight = 13
    TabOrder = 5
    OnClick = CheckListBox_bandwidthClick
  end
  object Panel1: TPanel
    Left = 0
    Top = 562
    Width = 1049
    Height = 41
    Align = alBottom
    TabOrder = 6
    object Button3: TButton
      Left = 8
      Top = 8
      Width = 75
      Height = 25
      Caption = 'Ok'
      ModalResult = 1
      TabOrder = 0
      OnClick = Button2Click
    end
    object Button4: TButton
      Left = 96
      Top = 8
      Width = 75
      Height = 25
      Cancel = True
      Caption = #1047#1072#1082#1088#1099#1090#1100
      ModalResult = 2
      TabOrder = 1
    end
    object Button1: TButton
      Left = 312
      Top = 8
      Width = 75
      Height = 25
      Caption = 'Button1'
      TabOrder = 2
      Visible = False
      OnClick = Button1Click
    end
    object Button2: TButton
      Left = 408
      Top = 8
      Width = 75
      Height = 25
      Caption = 'Copy'
      TabOrder = 3
      Visible = False
      OnClick = Button2Click
    end
    object b_Filter: TButton
      Left = 520
      Top = 8
      Width = 75
      Height = 25
      Caption = 'Filter'
      TabOrder = 4
      Visible = False
      OnClick = b_FilterClick
    end
  end
  object ADOConnection1: TADOConnection
    ConnectionString = 
      'Provider=SQLOLEDB.1;Password=sa;Persist Security Info=True;User ' +
      'ID=sa;Initial Catalog=onega_link;Data Source=server1'
    LoginPrompt = False
    Provider = 'SQLOLEDB.1'
    Left = 888
    Top = 120
  end
  object q_Band: TADOQuery
    Connection = ADOConnection1
    CursorType = ctStatic
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      #9'select distinct band from lib_freqplan.view_LinkEndType_band')
    Left = 688
    Top = 8
  end
  object ds_Band: TDataSource
    DataSet = q_Band
    Left = 688
    Top = 64
  end
  object ds_Channels: TDataSource
    DataSet = sp_Band_Channels
    Left = 972
    Top = 52
  end
  object sp_Band_Channels: TADOStoredProc
    Connection = ADOConnection1
    CursorType = ctStatic
    LockType = ltBatchOptimistic
    ProcedureName = 'lib_freqplan.sp_LinkEndType_Band_SEL'
    Parameters = <>
    Left = 972
    Top = 4
  end
  object q_Recommendation: TADOQuery
    Connection = ADOConnection1
    CursorType = ctStatic
    LockType = ltReadOnly
    DataSource = ds_Band
    Parameters = <
      item
        Name = 'band'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = '11'
      end>
    SQL.Strings = (
      'select distinct recommendation   , band'
      'from lib_freqplan.view_LinkEndType_band'
      'where '
      ''
      'band=:band')
    Left = 792
  end
  object ds_Recommendation: TDataSource
    DataSet = q_Recommendation
    Left = 792
    Top = 64
  end
  object q_Vendor: TADOQuery
    Connection = ADOConnection1
    CursorType = ctStatic
    LockType = ltReadOnly
    AfterScroll = q_VendorAfterScroll
    DataSource = ds_Recommendation
    Parameters = <
      item
        Name = 'band'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = '11'
      end
      item
        Name = 'recommendation'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = 'R-REC-F.387-13'
      end>
    SQL.Strings = (
      'select distinct vendor_name, vendor_id'
      'from lib_freqplan.view_LinkEndType_band'
      'where '
      ''
      'band=:band  and'
      'recommendation=:recommendation'
      ''
      'and  vendor_id is not null')
    Left = 888
  end
  object ds_Vendor: TDataSource
    DataSet = q_Vendor
    Left = 888
    Top = 64
  end
  object q_LinkEndType: TADOQuery
    Connection = ADOConnection1
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      #9'select distinct band from lib_freqplan.view_LinkEndType_band')
    Left = 688
    Top = 120
  end
  object ADOStoredProc1: TADOStoredProc
    Connection = ADOConnection1
    CursorType = ctStatic
    LockType = ltBatchOptimistic
    ProcedureName = 'lib_freqplan.sp_LinkEndType_Band_SEL'
    Parameters = <>
    Left = 900
    Top = 516
  end
  object ActionList1: TActionList
    Left = 768
    Top = 120
    object act_Copy: TAction
      Caption = 'act_Copy'
    end
  end
  object FormStorage1: TFormStorage
    IniFileName = 'Software\Onega\'
    StoredValues = <>
    Left = 92
    Top = 419
  end
end
