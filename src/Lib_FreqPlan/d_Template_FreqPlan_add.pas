unit d_Template_FreqPlan_add;

interface

uses

  dm_Folder,
  I_Shell,
  u_shell_new,

  d_Wizard_Add_with_params,

  d_LinkEndType_Select_Vendor,


  dm_Onega_DB_data,

 // d_Wizard,
  u_types,

  u_const_str,

  dm_Library,

  u_func,
  u_cx_VGrid,
  u_cx_VGrid_export,

//  dm_LinkEndType,
  dm_Template_FreqPlan,

  Classes, Controls, Forms,   StdCtrls, cxVGrid, cxDropDownEdit,
  cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, cxEdit, cxButtonEdit, cxTextEdit,
  cxInplaceContainer, cxPropertiesStore, rxPlacemnt, ActnList, ComCtrls,
  ExtCtrls, dxSkinsCore, dxSkinsDefaultPainters;

type
  Tdlg_Template_FreqPlan_add = class(Tdlg_Wizard_add_with_params)
    Button2: TButton;
    cxVerticalGrid1: TcxVerticalGrid;
    Row_Folder: TcxEditorRow;
    cxVerticalGrid1CategoryRow2: TcxCategoryRow;
    row_Band: TcxEditorRow;
    r_Freq_Min: TcxEditorRow;
    r_Freq_Max: TcxEditorRow;
    cxVerticalGrid1EditorRow4: TcxEditorRow;
    r_Power_max: TcxEditorRow;
    r_power_loss: TcxEditorRow;
    cxVerticalGrid1CategoryRow1: TcxCategoryRow;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure act_OkExecute(Sender: TObject);
    procedure ActionList1Update(Action: TBasicAction; var Handled: Boolean);
    procedure Button1Click(Sender: TObject);
//    procedure Button2Click(Sender: TObject);
  private
    FID,FFolderID: integer;
  public
    class function ExecDlg (aFolderID: integer =0): integer;
  end;


//==================================================================
implementation{$R *.DFM}
//==================================================================

const
//  STR_DEF_SPEED         = '4;8;16;34';
  STR_DEF_POWER         = '25';
  STR_DEF_POWER_LOSS    = '25';
  STR_DEF_BAND         = '13';
  STR_DEF_FREQ_MIN     = '12750';
  STR_DEF_FREQ_MAX     = '13250';
  STR_DEF_FAILURE_PERIOD = '10000';


//  STR_DEF_SENS3         = '-92';
//  STR_DEF_SENS6         = '-90';
//  STR_DEF_CHANNELSPACING = '3.5';
//  STR_DEF_MINFREQ       = '12752.75';
//  STR_DEF_CHANNELNUMBER = '64';
//  STR_DEF_FREQSPACING   = '266';



//--------------------------------s------------------------------------
class function Tdlg_Template_FreqPlan_add.ExecDlg (aFolderID: integer =0): integer;
//--------------------------------------------------------------------
begin
  with Tdlg_Template_FreqPlan_add.Create(Application) do
  begin
    FFolderID:=aFolderID;
    ed_Name_.Text:='������';//dmLinkEndType.GetNewName;

    row_Folder.Properties.Value:=dmOnega_DB_data.Folder_GetFullPath(aFolderID);

    ShowModal;
    Result:=FID;

    Free;
  end;
end;


//--------------------------------------------------------------------
procedure Tdlg_Template_FreqPlan_add.FormCreate(Sender: TObject);
//--------------------------------------------------------------------
begin
  inherited;

  SetActionName(STR_DLG_ADD_LINKEND_TYPE);

  row_Folder.Properties.Caption:='�����';



(*  row_Power_max.Caption := '����������� �������� ����������� [dBm]';
  row_power_loss.Caption:= '�������� ���������� �������� (�� 0 �� ..) [dB]';
//  row_Speed.Caption     := '��� ��������� �������� [Mbit/s]';
//  row_Equaliser.Caption := '�������, �������� ������������ [dB] (0-��� ����������)';
//  row_kng.Caption       := '��� ������������ [%]';
  row_Freq_Max.Caption  := '������� max [MHz]';
  row_Freq_Min.Caption  := '������� min [MHz]';
  row_range.Caption     := '�������� [GHz]';
  row_failure_period.Caption := '����� ��������� �� ����� [h]';

*)

  r_Power_max.Properties.Caption := '����������� �������� ����������� [dBm]';
  r_power_loss.Properties.Caption:= '�������� ���������� �������� (�� 0 �� ..) [dB]';
//  row_Speed.Caption     := '��� ��������� �������� [Mbit/s]';
//  row_Equaliser.Caption := '�������, �������� ������������ [dB] (0-��� ����������)';
//  row_kng.Caption       := '��� ������������ [%]';
  r_Freq_Max.Properties.Caption  := '������� max [MHz]';
  r_Freq_Min.Properties.Caption  := '������� min [MHz]';
  row_Band.Properties.Caption     := '�������� [GHz]';
//  r_failure_period.Properties.Caption := '����� ��������� �� ����� [h]';

  TcxComboBoxProperties(row_Band.Properties.EditProperties).Items.Assign (dmLIbrary.Bands);


  cx_InitVerticalGrid (cxVerticalGrid1);


 // cx_VerticalGrid_LoadFromReg(FRe);


  cx_VerticalGrid_LoadFromReg(cxVerticalGrid1, FRegPath);


{  AddComponentProp(r_Power_max, DEF_PropertiesValue);
  AddComponentProp(r_power_loss, DEF_PropertiesValue);
  AddComponentProp(r_Freq_Max, DEF_PropertiesValue);
  AddComponentProp(r_Freq_Min, DEF_PropertiesValue);
  AddComponentProp(r_range, DEF_PropertiesValue);
  AddComponentProp(r_failure_period, DEF_PropertiesValue);
  cxPropertiesStore.RestoreFrom;
}
  {  row_Sens3.Caption         := '���������������� ��� BER 10^-3 [dBm]';
  row_Sens6.Caption         := '���������������� ��� BER 10^-6 [dBm]';
  row_ChannelSpacing.Caption:= '������ ������� [MHz]';
  row_MinFreq.Caption       := 'Min ������� [MHz]';
  row_ChannelNumber.Caption := '���-�� ��������� �������';
  row_FreqSpacing.Caption   := '������ ������ ���/��� [MHz]';
 }

(*  with TRegIniFile.Create(FRegPath) do
  begin

//    row_Speed.Text:=          ReadString ('', row_Speed.Name, '4;8;16;34');
    row_power_loss.Text:=     ReadString ('', row_power_loss.Name, '25');
    row_Power_max.Text:=      ReadString ('', row_Power_max.Name, '25');
    row_Range.Text:=          ReadString ('', row_Range.Name, '13');
    row_Freq_Min.Text:=       ReadString ('', row_Freq_Min.Name, '12750');
    row_Freq_Max.Text:=       ReadString ('', row_Freq_Max.Name, '13250');
    row_failure_period.Text:= ReadString ('', row_failure_period.Name, '10000');

  {  row_Sens3.Text:=          ReadString ('', row_Sens3.Name, '-92');
    row_Sens6.Text:=          ReadString ('', row_Sens6.Name, '-90');
    row_ChannelSpacing.Text:= ReadString ('', row_ChannelSpacing.Name, '3.5');
    row_MinFreq.Text:=        ReadString ('', row_MinFreq.Name, '12752.75');
    row_ChannelNumber.Text:=  ReadString ('', row_ChannelNumber.Name, '64');
    row_FreqSpacing.Text:=    ReadString ('', row_FreqSpacing.Name, '266');
    }
  end;
*)

  cxVerticalGrid1.Visible := True;
  cxVerticalGrid1.Align:=alClient;

end;

// -------------------------------------------------------------------
procedure Tdlg_Template_FreqPlan_add.FormDestroy(Sender: TObject);
// -------------------------------------------------------------------
begin
  cx_VerticalGrid_SaveToReg(cxVerticalGrid1, FRegPath);


 (* with TRegIniFile.Create(FRegPath) do
  begin
 //   CurrentRegPath := FRegPath;

//    WriteString ('', row_Speed.Name,          row_Speed.Text);
    WriteString ('', row_power_loss.Name,     row_power_loss.Text );
    WriteString ('', row_Power_max.Name,      row_Power_max.Text  );
    WriteString ('', row_Range.Name,          row_Range.Text  );
    WriteString ('', row_Freq_Min.Name,       row_Freq_Min.Text);
    WriteString ('', row_Freq_Max.Name,       row_Freq_Max.Text);
    WriteString ('', row_failure_period.Name, row_failure_period.Text);

{    WriteString ('', row_Sens3.Name,          row_Sens3.Text);
    WriteString ('', row_Sens6.Name,          row_Sens6.Text );
    WriteString ('', row_ChannelSpacing.Name, row_ChannelSpacing.Text  );
    WriteString ('', row_MinFreq.Name,        row_MinFreq.Text  );
    WriteString ('', row_ChannelNumber.Name,  row_ChannelNumber.Text);
    WriteString ('', row_FreqSpacing.Name,    row_FreqSpacing.Text);
    }

  end;*)

  inherited;
end;

//-------------------------------------------------------------------
procedure Tdlg_Template_FreqPlan_add.act_OkExecute(Sender: TObject);
//-------------------------------------------------------------------
var rec: TdmTemplate_FreqPlanAddRec;
  s: string;
  sFolderGUID: string;
  sGUID: string;
begin
  FillChar (rec, SizeOf(rec), 0);

//  ��s:=row_band.Properties.Value;


  with rec do
  begin
    Name    :=ed_Name_.Text;
    FolderID:=FFolderID;

 //////   Range      := AsInteger(r_range.Properties.Value);
    Power_max_dBm  := AsFloat(r_Power_max.Properties.Value);
    Power_loss_dBm := AsFloat(r_power_loss.Properties.Value);
//    Speed_row  := row_Speed.Text;

    Band:=AsString(row_band.Properties.Value);

//    FreqMin:=AsFloat(r_Freq_Min.Properties.Value);
//    FreqMax:=AsFloat(r_Freq_Max.Properties.Value);

 //   failure_period := AsFloat(r_failure_period.Properties.Value);

{    Sens3 :=          AsFloat(row_Sens3.Text);
    Sens6 :=          AsFloat(row_Sens6.Text);
    ChannelSpacing := AsFloat(row_ChannelSpacing.Text);
    MinFreq :=        AsFloat(row_MinFreq.Text);
    ChannelNumber :=  AsFloat(row_ChannelNumber.Text);
    FreqSpacing :=    AsFloat(row_FreqSpacing.Text);
    }
  end;

  FID:=dmTemplate_FreqPlan.Add (rec);



//  if aFolderID=0 then
  if FFolderID>0 then
    sFolderGUID:=dmFolder.GetGUIDByID(FFolderID)
  else
    sFolderGUID:=g_Obj.ItemByName[OBJ_LINKEND_TYPE].RootFolderGUID;
 // else
  //  sFolderGUID:=dmFolder.GetGUIDByID(aFolderID);

  g_Shell.UpdateNodeChildren_ByGUID (sFolderGUID);


 // else
  //  sFolderGUID:=dmFolder.GetGUIDByID(aFolderID);

//  g_ShellEvents.Shell_UpdateNodeChildren_ByGUID (sFolderGUID);

  sGUID:=dmTemplate_FreqPlan.GetGUIDByID(FID);
  g_ShellEvents.Shell_PostFocusNode (sGUID);
end;


// -------------------------------------------------------------------
procedure Tdlg_Template_FreqPlan_add.ActionList1Update(Action: TBasicAction; var Handled: Boolean);
// -------------------------------------------------------------------
begin

(*  act_ok.Enabled:=
    (row_Range.Text          <> '') and
    (row_Freq_Min.Text       <> '') and
    (row_Freq_Max.Text       <> '') and
//    (row_Speed.Text          <> '') and
    (row_Power_max.Text      <> ''); // and
*)

//    (row_Sens3.Text          <> '') and
//    (row_Sens6.Text          <> '') and
//    (row_ChannelSpacing.Text <> '') and
//    (row_MinFreq.Text        <> '') and
//    (row_ChannelNumber.Text  <> '') and
//    (row_FreqSpacing.Text    <> '');

end;

//-------------------------------------------------------------
procedure Tdlg_Template_FreqPlan_add.Button1Click(Sender: TObject);
//-------------------------------------------------------------
begin

//  row_Speed.Text:=            STR_DEF_SPEED;
{
  r_power_loss.Properties.Value:=     STR_DEF_POWER_LOSS;
  r_Power_max.Properties.Value:=      STR_DEF_POWER;
  row_Band.Properties.Value :=        STR_DEF_BAND;
  r_Freq_Min.Properties.Value:=       STR_DEF_FREQ_MIN;
  r_Freq_Max.Properties.Value:=       STR_DEF_FREQ_MAX;
  r_failure_period.Properties.Value:= STR_DEF_FAILURE_PERIOD;
}

{  row_Sens3.Text:=            STR_DEF_SENS3;
  row_Sens6.Text:=            STR_DEF_SENS6;
  row_ChannelSpacing.Text:=   STR_DEF_CHANNELSPACING;
  row_MinFreq.Text:=          STR_DEF_MINFREQ;
  row_ChannelNumber.Text:=    STR_DEF_CHANNELNUMBER;
  row_FreqSpacing.Text:=      STR_DEF_FREQSPACING;
  }
end;


{
procedure Tdlg_Template_FreqPlan_add.Button2Click(Sender: TObject);
var
  iVendor_ID,iVendor_Equipment_ID: Integer;
  sVendor_name, sVendor_Equipment_name: string;

  rec: TLinkEndType_Select_Vendor_rec;

begin

  if Tdlg_LinkEndType_Select_Vendor.ExecDlg(rec)
  then
  begin


  end;

end;
 }

end.