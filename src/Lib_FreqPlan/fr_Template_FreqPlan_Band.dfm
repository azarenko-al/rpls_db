object frame_Template_FreqPlan_Band: Tframe_Template_FreqPlan_Band
  Left = 1164
  Top = 502
  Width = 1198
  Height = 592
  Caption = 'frame_Template_FreqPlan_Band'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = True
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl1: TPageControl
    Left = 0
    Top = 0
    Width = 1182
    Height = 233
    ActivePage = TabSheet_Bands
    Align = alTop
    Style = tsFlatButtons
    TabOrder = 0
    object TabSheet_Bands: TTabSheet
      Caption = 'Bands'
      object cxGrid1: TcxGrid
        Left = 0
        Top = 0
        Width = 1174
        Height = 169
        Align = alTop
        TabOrder = 0
        LookAndFeel.Kind = lfFlat
        object cxGrid1DBTableView_bands_: TcxGridDBTableView
          NavigatorButtons.ConfirmDelete = False
          DataController.DataSource = ds_Bands
          DataController.Filter.MaxValueListCount = 1000
          DataController.KeyFieldNames = 'id'
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          Filtering.ColumnPopup.MaxDropDownItemCount = 12
          OptionsBehavior.ImmediateEditor = False
          OptionsCustomize.ColumnFiltering = False
          OptionsCustomize.ColumnsQuickCustomization = True
          OptionsView.Navigator = True
          OptionsView.CellAutoHeight = True
          OptionsView.GroupByBox = False
          OptionsView.GroupFooters = gfVisibleWhenExpanded
          OptionsView.HeaderAutoHeight = True
          OptionsView.Indicator = True
          Preview.AutoHeight = False
          Preview.MaxLineCount = 2
          Styles.OnGetContentStyle = cxGrid1DBTableView_bandsStylesGetContentStyle
          object col_id: TcxGridDBColumn
            Tag = -1
            DataBinding.FieldName = 'id'
            PropertiesClassName = 'TcxTextEditProperties'
            Properties.Alignment.Horz = taLeftJustify
            Properties.MaxLength = 0
            Properties.ReadOnly = False
            Visible = False
            Options.Filtering = False
            Width = 25
          end
          object col_name1: TcxGridDBColumn
            DataBinding.FieldName = 'name'
            Options.Filtering = False
            Width = 76
          end
          object col_name_vendor: TcxGridDBColumn
            DataBinding.FieldName = 'name_vendor'
            Width = 84
          end
          object col__High: TcxGridDBColumn
            DataBinding.FieldName = 'High'
            Options.Filtering = False
            Width = 55
          end
          object col__Low: TcxGridDBColumn
            DataBinding.FieldName = 'Low'
            Options.Filtering = False
            Width = 66
          end
          object col__TxRx_Shift: TcxGridDBColumn
            DataBinding.FieldName = 'TxRx_Shift'
            Options.Filtering = False
            Width = 69
          end
          object col__freq_min_low: TcxGridDBColumn
            DataBinding.FieldName = 'freq_min_low'
            Options.Filtering = False
            Width = 77
          end
          object col__freq_max_low: TcxGridDBColumn
            DataBinding.FieldName = 'freq_max_low'
            HeaderAlignmentHorz = taCenter
            Options.Filtering = False
            Width = 84
          end
          object col__freq_min_high: TcxGridDBColumn
            DataBinding.FieldName = 'freq_min_high'
            Options.Filtering = False
            Width = 72
          end
          object col__freq_max_high: TcxGridDBColumn
            DataBinding.FieldName = 'freq_max_high'
            Options.Filtering = False
            Width = 75
          end
          object col__Channel_width: TcxGridDBColumn
            Caption = 'ch width'
            DataBinding.FieldName = 'Channel_width'
            Options.Filtering = False
            Width = 69
          end
          object col_Bandwidth: TcxGridDBColumn
            DataBinding.FieldName = 'Bandwidth'
          end
          object col__Channel_count: TcxGridDBColumn
            Caption = 'N '#1082#1072#1085#1072#1083#1086#1074
            DataBinding.FieldName = 'Channel_count'
            Options.Filtering = False
            Width = 78
          end
          object cxGrid1DBTableView_bands_Column1: TcxGridDBColumn
            Caption = #1050#1086#1076' '#1087#1088#1086#1076#1091#1082#1090#1072' High'
            DataBinding.FieldName = 'item_code_high'
            Visible = False
            Options.Filtering = False
            Width = 85
          end
          object cxGrid1DBTableView_bands_Column2: TcxGridDBColumn
            Caption = #1050#1086#1076' '#1087#1088#1086#1076#1091#1082#1090#1072' Low'
            DataBinding.FieldName = 'item_code_low'
            Visible = False
            Options.Filtering = False
            Width = 88
          end
          object cxGrid1DBTableView_bands_Column3: TcxGridDBColumn
            Caption = #1050#1086#1076' '#1087#1088#1086#1076#1091#1082#1090#1072' High '#1082#1086#1088#1087#1086#1088#1072#1090#1080#1074#1085#1099#1081
            DataBinding.FieldName = 'item_code_high_internal'
            Visible = False
            Options.Filtering = False
            Width = 107
          end
          object cxGrid1DBTableView_bands_Column4: TcxGridDBColumn
            Caption = #1050#1086#1076' '#1087#1088#1086#1076#1091#1082#1090#1072' Low '#1082#1086#1088#1087#1086#1088#1072#1090#1080#1074#1085#1099#1081
            DataBinding.FieldName = 'item_code_low_internal'
            Visible = False
            Options.Filtering = False
            Width = 111
          end
        end
        object cxGrid1Level1: TcxGridLevel
          GridView = cxGrid1DBTableView_bands_
        end
      end
    end
  end
  object ds_Bands: TDataSource
    DataSet = qry_Bands
    Left = 48
    Top = 308
  end
  object qry_Bands: TADOQuery
    ConnectionString = 
      'Provider=SQLOLEDB.1;Password=sa;Persist Security Info=True;User ' +
      'ID=sa;Initial Catalog=onega_link;Data Source=SERVER1'
    CursorType = ctStatic
    OnNewRecord = qry_BandsNewRecord
    Parameters = <>
    SQL.Strings = (
      'select top 100 * from LinkendType_band')
    Left = 48
    Top = 260
  end
  object ActionList222: TActionList
    Left = 452
    Top = 273
  end
  object FormStorage1: TFormStorage
    Active = False
    IniFileName = 'Software\Onega\'
    Options = [fpPosition]
    StoredProps.Strings = (
      'PageControl1.ActivePage')
    StoredValues = <>
    Left = 44
    Top = 436
  end
  object ADOConnection1: TADOConnection
    ConnectionString = 
      'Provider=SQLOLEDB.1;Password=sa;Persist Security Info=True;User ' +
      'ID=sa;Initial Catalog=onega_link;Data Source=server1'
    LoginPrompt = False
    Provider = 'SQLOLEDB.1'
    Left = 640
    Top = 272
  end
  object cxStyleRepository111111111: TcxStyleRepository
    Left = 632
    Top = 352
    PixelsPerInch = 96
    object cxStyle_clRed_111: TcxStyle
      AssignedValues = [svColor]
      Color = clRed
    end
  end
  object cxStyleRepository21111111: TcxStyleRepository
    Left = 760
    Top = 352
    PixelsPerInch = 96
    object cxStyle_Edit: TcxStyle
    end
    object cxStyle_ReadOnly_111: TcxStyle
      AssignedValues = [svColor]
      Color = clInactiveBorder
    end
  end
  object cxStyleRepository_row: TcxStyleRepository
    Left = 912
    Top = 328
    PixelsPerInch = 96
    object cxStyle_row: TcxStyle
      AssignedValues = [svColor]
      Color = clInfoBk
    end
  end
end
