unit fr_Template_FreqPlan_View;

interface

uses

dm_User_Security,

  d_LinkEndType_Select_Vendor,
  d_LinkEndType_Recommendation_Select,

  fr_View_base,

  u_func,
  
  
  u_reg,

  
  u_const_DB,
  u_types,

  

  fr_DBInspector_Container,

//  fr_Template_FreqPlan_Mode,
  fr_Template_FreqPlan_Band,

  Classes, Controls, Forms,
    ComCtrls, ExtCtrls,
     cxSplitter,
  cxPropertiesStore, cxCheckBox, cxButtonEdit, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, dxBar, cxBarEditItem, cxClasses,
  Menus, ImgList, ActnList, dxSkinsCore, dxSkinsDefaultPainters;


type
  Tframe_Template_FreqPlan_View = class(Tframe_View_Base)
    cxSplitter1: TcxSplitter;
    pn_Inspector: TPanel;
    PageControl1: TPageControl;
    ts_Bands: TTabSheet;

    procedure FormCreate(Sender: TObject);
//    procedure PageControl1Click(Sender: TObject);

//    procedure DoAction (Sender: TObject);


  private

  private
    FActivPage: integer;

    Ffrm_DBInspector_Container: Tframe_DBInspector_Container;
    Fframe_LinkEndType_Band: Tframe_Template_FreqPlan_Band;
//    Fframe_LinkEndType_MOde: Tframe_Template_FreqPlan_Mode;

    procedure DoOnButtonFieldClick(Sender: TObject; aFieldName: string; aNewValue: Variant);

  public
    procedure View(aID: integer; aGUID: string); override;

  end;


//=====================================================================
//implementation
//=====================================================================
implementation {$R *.dfm}



//--------------------------------------------------------------------
procedure Tframe_Template_FreqPlan_View.FormCreate(Sender: TObject);
//--------------------------------------------------------------------
begin
  inherited;

  ObjectName:=OBJ_Template_FreqPlan;

//  TableName := TBL_LinkEndType;

  pn_Inspector.Align:=alClient;

//  act_Update_modes.Caption:= '�������� ������';

//   ������ ������
//  dxBand.Align:= alClient;
//  dxBandEditor.Align:= alClient;

  CreateChildForm(Tframe_DBInspector_Container, Ffrm_DBInspector_Container, pn_Inspector);
  Ffrm_DBInspector_Container.PrepareViewForObject (ObjectName);
  Ffrm_DBInspector_Container.OnButtonClick:= DoOnButtonFieldClick;


  CreateChildForm(Tframe_Template_FreqPlan_Band, Fframe_LinkEndType_Band, ts_Bands);
//  CreateChildForm(Tframe_Template_FreqPlan_Mode, Fframe_LinkEndType_Mode, ts_Modes);


  AddComponentProps(PageControl1, [PROP_HEIGHT, PROP_ACTIVE_PAGE]);
  cxPropertiesStore.RestoreFrom;

//  SetActionsExecuteProc ([act_Update_modes ], DoAction);

  cxSplitter1.Realign;

end;


//--------------------------------------------------------------------
procedure Tframe_Template_FreqPlan_View.View(aID: integer; aGUID: string);
//--------------------------------------------------------------------
var
  bReadOnly: Boolean;
begin

  inherited;

  bReadOnly:=dmUser_Security.Lib_Edit_ReadOnly();


  Ffrm_DBInspector_Container.View (aID);
  Ffrm_DBInspector_Container.SetReadOnly(bReadOnly);


//  Fframe_LinkEndType_Mode.SetReadOnly(bReadOnly);
  Fframe_LinkEndType_Band.SetReadOnly(bReadOnly);



  FActivPage:= -1;
//  PageControl1Click (nil);

  Fframe_LinkEndType_Band.View(ID);

{ TODO : �������� view �� ��������� ��������. }
end;





// ---------------------------------------------------------------
procedure Tframe_Template_FreqPlan_View.DoOnButtonFieldClick(Sender: TObject; aFieldName:
    string; aNewValue: Variant);
// ---------------------------------------------------------------
var
  sVendor_name, sVendor_Equipment_name: string;
  rec: TLinkEndType_Select_Vendor_rec;

  rec_: Tdlg_LinkEndType_Recommendation_Select_rec;
begin
  //----------------------------------------------------------------
  if
     Eq(aFieldName, FLD_Vendor_NAME) or
     Eq(aFieldName, FLD_Vendor_ID) or
     Eq(aFieldName, FLD_Vendor_Equipment_ID) then
  //----------------------------------------------------------------
  begin
    rec.Vendor_ID:= Ffrm_DBInspector_Container.GetIntFieldValue(FLD_Vendor_ID);
    rec.Vendor_Equipment_ID:= Ffrm_DBInspector_Container.GetIntFieldValue(FLD_Vendor_Equipment_ID);


    if Tdlg_LinkEndType_Select_Vendor.ExecDlg(rec) then
    begin
      Ffrm_DBInspector_Container.SetFieldValue(FLD_Vendor_ID,           rec.Vendor_ID);
      Ffrm_DBInspector_Container.SetFieldValue(FLD_Vendor_Equipment_ID, IIF_NULL(rec.Vendor_Equipment_ID));

      Ffrm_DBInspector_Container.SetStrFieldValue(FLD_Vendor_NAME, rec.Vendor_NAME, False);
      Ffrm_DBInspector_Container.SetStrFieldValue(FLD_Vendor_Equipment_NAME, rec.Vendor_Equipment_NAME, False);

    end;
  end;

  //----------------------------------------------------------------
  if Eq(aFieldName, FLD_recommendation) or
     Eq(aFieldName, FLD_band) then
  //----------------------------------------------------------------
  begin
    rec_.band          := Ffrm_DBInspector_Container.GetStrFieldValue(FLD_BAND);
    rec_.Recommendation:= Ffrm_DBInspector_Container.GetStrFieldValue(FLD_recommendation);


    if Tdlg_LinkEndType_Recommendation_Select.ExecDlg(rec_) then
    begin
      Ffrm_DBInspector_Container.SetFieldValue(FLD_band,           rec_.Band);
      Ffrm_DBInspector_Container.SetFieldValue(FLD_recommendation, rec_.Recommendation);
      Ffrm_DBInspector_Container.SetFieldValue(FLD_FREQ_MIN,   rec_.FREQ_MIN_MHZ);
      Ffrm_DBInspector_Container.SetFieldValue(FLD_FREQ_MAX,   rec_.FREQ_MAX_MHZ);
    end;
  end;



end;

end.


{

// ---------------------------------------------------------------
procedure Tframe_LinkEndType_View.DoOnButtonFieldClick(Sender: TObject; aFieldName:
    string; aNewValue: Variant);
// ---------------------------------------------------------------
var
  sVendor_name, sVendor_Equipment_name: string;
  rec: TLinkEndType_Select_Vendor_rec;

  rec_: Tdlg_LinkEndType_Recommendation_Select_rec;
begin
  if Eq(aFieldName, FLD_Vendor_ID) or
     Eq(aFieldName, FLD_Vendor_Equipment_ID) then
  begin
    rec.Vendor_ID:= Ffrm_DBInspector_Container.GetIntFieldValue(FLD_Vendor_ID);
    rec.Vendor_Equipment_ID:= Ffrm_DBInspector_Container.GetIntFieldValue(FLD_Vendor_Equipment_ID);


    if Tdlg_LinkEndType_Select_Vendor.ExecDlg(rec) then
    begin
      Ffrm_DBInspector_Container.SetFieldValue(FLD_Vendor_ID,           rec.Vendor_ID);
      Ffrm_DBInspector_Container.SetFieldValue(FLD_Vendor_Equipment_ID, IIF_NULL(rec.Vendor_Equipment_ID));

      Ffrm_DBInspector_Container.SetStrFieldValue(FLD_Vendor_NAME, rec.Vendor_NAME, False);
      Ffrm_DBInspector_Container.SetStrFieldValue(FLD_Vendor_Equipment_NAME, rec.Vendor_Equipment_NAME, False);

    end;
  end;


  if Eq(aFieldName, FLD_recommendation) then
  begin
    rec_.band          := Ffrm_DBInspector_Container.GetStrFieldValue(FLD_BAND);
    rec_.Recommendation:= Ffrm_DBInspector_Container.GetStrFieldValue(FLD_recommendation);


    if Tdlg_LinkEndType_Select_Vendor.ExecDlg(rec) then
    begin
      Ffrm_DBInspector_Container.SetFieldValue(FLD_recommendation, rec_.Recommendation);

    end;
  end;



end;


//--------------------------------------------------------------------
// actions
//--------------------------------------------------------------------
procedure Tframe_Template_FreqPlan_View.DoAction (Sender: TObject);
//var
 // iMode: Integer;
begin{
  //-------------------------------------------------------------------
  if Sender=act_Update_modes then
  //-------------------------------------------------------------------
  begin
//    dmLinkEndType.Bands_Update (FID);
    View(FID);
  end else}


end;

//--------------------------------------------------------------------
procedure Tframe_Template_FreqPlan_View.PageControl1Click(Sender: TObject);
//--------------------------------------------------------------------
begin
  inherited;
  if ID=0 then Exit;

  if FActivPage=PageControl1.ActivePageIndex then
    Exit;

  FActivPage:= PageControl1.ActivePageIndex;

  case PageControl1.ActivePageIndex of
//    0: Fframe_LinkEndType_Mode.View(ID,0);
    0: Fframe_LinkEndType_Band.View(ID);
  end;
end;

