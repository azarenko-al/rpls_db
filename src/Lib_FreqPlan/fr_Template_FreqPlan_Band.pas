unit fr_Template_FreqPlan_Band;

interface

uses


//  d_Audit,
  u_LinkEndType_str,


  u_storage,

  

 // f_Custom,

  dm_Main,
  u_db,

  u_cx,

  u_const_db,
  u_const_str,

 // dm_LinkEndType_Band_View,

  SysUtils, Classes, Controls, Forms, Dialogs,  ADODB, Db, ActnList, ComCtrls,
  cxGridCustomTableView, cxGridDBTableView, cxGrid,  cxGridLevel,  rxPlacemnt,
  cxStyles, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxDBData,
  cxTextEdit, cxGridTableView, cxClasses, cxGridCustomView, dxSkinsCore,
  dxSkinsDefaultPainters;


//  cxGridBandedTableView, cxGridDBBandedTableView, ComCtrls;

type
  Tframe_Template_FreqPlan_Band = class(TForm)
    ds_Bands: TDataSource;
    qry_Bands: TADOQuery;
    ActionList222: TActionList;
    FormStorage1: TFormStorage;
    PageControl1: TPageControl;
    TabSheet_Bands: TTabSheet;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView_bands_: TcxGridDBTableView;
    col__High: TcxGridDBColumn;
    col__Low: TcxGridDBColumn;
    col__TxRx_Shift: TcxGridDBColumn;
    col__freq_min_low: TcxGridDBColumn;
    col__freq_max_low: TcxGridDBColumn;
    col__freq_min_high: TcxGridDBColumn;
    col__freq_max_high: TcxGridDBColumn;
    col__Channel_width: TcxGridDBColumn;
    col__Channel_count: TcxGridDBColumn;
    col_id: TcxGridDBColumn;
    cxGrid1Level1: TcxGridLevel;
    col_name1: TcxGridDBColumn;
    ADOConnection1: TADOConnection;
    cxStyleRepository111111111: TcxStyleRepository;
    cxStyle_clRed_111: TcxStyle;
    cxStyleRepository21111111: TcxStyleRepository;
    cxStyle_Edit: TcxStyle;
    cxStyle_ReadOnly_111: TcxStyle;
    cxStyleRepository_row: TcxStyleRepository;
    cxStyle_row: TcxStyle;
    cxGrid1DBTableView_bands_Column1: TcxGridDBColumn;
    cxGrid1DBTableView_bands_Column2: TcxGridDBColumn;
    cxGrid1DBTableView_bands_Column3: TcxGridDBColumn;
    cxGrid1DBTableView_bands_Column4: TcxGridDBColumn;
    col_Bandwidth: TcxGridDBColumn;
    col_name_vendor: TcxGridDBColumn;

//    procedure ActionList222Update(Action: TBasicAction; var Handled: Boolean);
//    procedure act_channels_forbidden_checkExecute(Sender: TObject);
    procedure act_channels_forbidden_uncheckExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
   // procedure GSPagesClick(Sender: TObject);

   // procedure PageControl1Change(Sender: TObject);
//    procedure qry_BandsBeforePost(DataSet: TDataSet);
    procedure qry_BandsNewRecord(DataSet: TDataSet);

//    procedure qry_BandsBeforeEdit(DataSet: TDataSet);

//    procedure act_Copy_recordExecute(Sender: TObject);
//    procedure act_duplicateExecute(Sender: TObject);
//    procedure col__TxRx_ShiftStylesGetContentStyle(
 //     Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
 //     AItem: TcxCustomGridTableItem; out AStyle: TcxStyle);
    procedure cxGrid1DBTableView_bandsStylesGetContentStyle(
      Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
      AItem: TcxCustomGridTableItem; out AStyle: TcxStyle);
//    procedure sp_Band_ChannelsAfterPost(DataSet: TDataSet);
  private
    FRegPath : string;
    FReadOnly: boolean;
    FID: integer;

{
    FTxFreq,
    FRxFreq: double;

    //��������� ��� ��������� ��������� ���������� � ������������
    FChWidth,
    FTxRxShift : double;
    FFreqMin_low,
    FFreqMax_low,
    FFreqMin_high,
    FFreqMax_high : double;
    FChCount: integer;
 }

//    function Copy_Band(aBand_ID, aLinkEndType_ID: integer): integer;
//    procedure DelBand();
//    procedure Dlg_Select_LinkendType;
//    procedure GetMessage(aMsg: TEventType; aParams: TEventParamList; var aHandled:
//        boolean);
  //  procedure DoAction (Sender: TObject);
  public
    procedure View (aLinkEndTypeID: integer);

  //  class
//    procedure ViewEx(aLinkEndTypeID: integer; aTxFreq,
//                     aRxFreq: Double; aChannel_Type:  string);
//
//    function GetBandID(var aChannel_Type: string): integer;
    procedure OpenDB_Band(aADOQuery: TADOQuery; aID: integer);
 //   procedure OpenDB_Channels;
    procedure SetReadOnly(aValue: boolean);
//    procedure SetReadOnly(aValue: boolean);

                   //!!!!!!!! var iChannel_Type: integer): integer;

  end;

//=================================================================
// implementation
//=================================================================
implementation
{$R *.DFM}

{
const
  CAPTION_NAME   = '������������';

  CAPTION_High = '������������ ������������ High';
  CAPTION_Low  = '������������ ������������ Low';

//  CAPTION_DUPLEX_FREQ_DELTA = '������ ������ ���/��� [MHz]';
//  CAPTION_CHANNEL_WIDTH     = '��� ����� ������ [MHz]'; //'������ ���������� ������ [MHz]';
  CAPTION_CHANNEL_WIDTH     = '������ ������� ������ [MHz]'; //'������ ���������� ������ [MHz]';
  CAPTION_DUPLEX_FREQ_DELTA = '���������� ������ ������ ���/��� [MHz]';

//������� ��������� �����:
//- ��� ����� ������ [MHz]  = ������ ������� ������ [MHz]
//- ������ ������ ���/��� [MHz] = ���������� ������ ������ ���/��� [MHz]



  CAPTION_FREQ_MIN          = 'Min ������� ������� %s [MHz]';
  CAPTION_FREQ_MAX          = 'Max ������� ������� %s [MHz]';
  CAPTION_CH_COUNT          = '���-�� ������';

  CAPTION_CHANNEL            = '�����';

  CAPTION_CH_LOW            = '����� Low';
  CAPTION_CH_HIGH           = '����� High';
  CAPTION_LOW_TX_FREQ       = '������� ��� [MHz]';
  CAPTION_LOW_RX_FREQ       = '������� ��� [MHz]';

  CAPTION_HIGH_TX_FREQ       = '������� ��� [MHz]';
  CAPTION_HIGH_RX_FREQ       = '������� ��� [MHz]';


  CAPTION_SPEED            = '�������� �������� [Mbit/s]';
  CAPTION_CH_SPACING       = '������ ������� [MHz]';
  CAPTION_MODULATION_TYPE  = '��� ���������';
  CAPTION_MODE             = '�����';
//  CAPTION_BANDWIDTH        = '������ ������ [MHz]';



 }

//--------------------------------------------------------------
procedure Tframe_Template_FreqPlan_Band.FormCreate(Sender: TObject);
//--------------------------------------------------------------
begin
  inherited;

  if ADOConnection1.Connected then
    ShowMessage ('ADOConnection1.Connected');

//  Assert(not ADOConnection1.Connected);

//  pn_RadioButtons.Visible:= False;
 // GSPages.TabHidden      := False;

  PageControl1.Align:= alClient;


//  cxGrid1DBTableView_bands.OptionsSelection.MultiSelect := True;


  cxGrid1.Align:= alClient;
//  cxGrid2.Align:= alClient;

 // pn_Top.Align:= alClient;

  PageControl1.ActivePageIndex:=0;

(*  GSPage1.Caption:= '�����������';
  GSPage2.Caption:= '������';
*)

  TabSheet_Bands.Caption:= '�����������';
//  TabSheet_Channels.Caption:= '������';


//  act_Audit.Caption:=STR_Audit;
//  act_Audit.Enabled:=False;


//  act_Copy_to_LinkendType.Caption:='���������� � ������ ������������';


//  act_channels_forbidden_check.Caption:='��������� ������';
//9  act_channels_forbidden_uncheck.Caption:='��������� ������';


 //zzzzzz dmLinkEndType_Band_View.InitDB_Channels (mem_Channels);

 // AddComponentProp(PageControl1, PROP_ACTIVE_PAGE);
 // cxPropertiesStore.RestoreFrom;

 // SetActionsExecuteProc([act_Field_Add, act_Field_Del], DoAction);

  // -------------------------------------------------------------------
 // Assert(FRegPath<>'');

  FRegPath := g_Storage.GetPathByClass(ClassName);

//  FRegPath :=FRegPath+ '2';

 // cxGrid1DBTableView11.restoreFromRegistry (FRegPath+ cxGrid1DBTableView11.Name);
 // cxGrid2DBBandedTableView11.restoreFromRegistry (FRegPath+ cxGrid2DBBandedTableView11.Name);

  g_Storage.StoreToRegistry(cxGrid1DBTableView_bands_, className+'_default');
//  g_Storage.StoreToRegistry(cxGrid2DBBandedTableView_channels, className+'_default');


  g_Storage.restoreFromRegistry(cxGrid1DBTableView_bands_, className);
//  g_Storage.restoreFromRegistry(cxGrid2DBBandedTableView_channels, className);



//  col__Band_name.Caption:= '����������';

  col_name1.Caption:= '������������';

  col_name_vendor.Caption := '������������ ������������ �������������';


  col__High.Caption := CAPTION_High;//'����������� High';
  col__Low.Caption  := CAPTION_LOW; //'����������� Low';

{  col__High.Visible         := True;
  col__freq_min_high.Visible:= True;
  col__freq_max_high.Visible:= True;
  col__Low.Visible          := True;
  col__freq_min_low.Visible := True;
  col__freq_max_low.Visible := True;

}
  col_name_vendor.Caption := '������������ ������������ �������������';


//  col_forbidden_freqs_str.Caption:='����������� �������';

  col__freq_min_low.Caption  := Format(CAPTION_FREQ_MIN, ['Low']);
  col__freq_max_low.Caption  := Format(CAPTION_FREQ_MAX, ['Low']);;
  col__freq_min_high.Caption := Format(CAPTION_FREQ_MIN, ['High']);
  col__freq_max_high.Caption := Format(CAPTION_FREQ_MAX, ['High']);;


{
  col__Channel_low.Caption := CAPTION_CH_LOW;
//  col__Channel_high.Caption:= CAPTION_CH_HIGH;
  col__tx_freq_low.Caption := CAPTION_low_TX_FREQ;
  col__rx_freq_low.Caption := CAPTION_low_RX_FREQ;
  col__tx_freq_high.Caption:= CAPTION_low_TX_FREQ;
  col__rx_freq_high.Caption:= CAPTION_low_RX_FREQ;

  col_name.Caption          := CAPTION_NAME;

  col__Channel_width.Caption := CAPTION_CHANNEL_WIDTH;
  col__TxRx_Shift.Caption    := CAPTION_DUPLEX_FREQ_DELTA;
  col__Channel_count.Caption := CAPTION_CH_COUNT;


  col_Channel.Caption := CAPTION_CHANNEL;
 }

  col__TxRx_Shift.Caption    := CAPTION_DUPLEX_FREQ_DELTA;
  col__Channel_width.Caption := CAPTION_CHANNEL_WIDTH;

  col_Bandwidth.Caption := CAPTION_Bandwidth_signal;
//  col_Bandwidth_.Caption := CAPTION_Bandwidth_signal;

//  act_Copy_record.Caption  := STR_Copy_record;
//  act_Restore_default.Caption:=STR_GRID_Restore_default;  // '������������ �������';
//  act_Copy_record.Caption  := STR_Copy_record;

 // cxSplitter1.Realign;


// ������� ��������� �����:
//- ��� ����� ������ [MHz]  = ������ ������� ������ [MHz]
//- ������ ������ ���/��� [MHz] = ���������� ������ ������ ���/��� [MHz]



  Assert (Assigned (OnDestroy));

end;


//--------------------------------------------------------------
procedure Tframe_Template_FreqPlan_Band.FormDestroy(Sender: TObject);
//--------------------------------------------------------------
begin
 // if FIsBandSave_to_Reg then
 // dx_Bands.SaveToRegistry (FRegPath+ dx_Bands.Name);

//  g_Storage.StoreToRegistry(cxGrid1DBTableView_bands), className);
//  g_Storage.StoreToRegistry(cxGrid2DBBandedTableView_channels, className);


//  cxGrid1DBTableView11.StoreToRegistry (FRegPath+ cxGrid1DBTableView11.Name);
//  cxGrid2DBBandedTableView11.StoreToRegistry (FRegPath+ cxGrid2DBBandedTableView11.Name);

  inherited;
end;




//--------------------------------------------------------------
procedure Tframe_Template_FreqPlan_Band.View (aLinkEndTypeID: integer);
//--------------------------------------------------------------
var iID: integer;
begin
  FID:=aLinkEndTypeID;

  OpenDB_Band (qry_Bands, aLinkEndTypeID);
//  dmLinkEndType_Band_View.OpenDB_Band (qry_Bands, aLinkEndTypeID);


//  PageControl1Change(nil);

(*  case PageControl1.ActivePageIndex of
//  case GSPages.ActivePageIndex of
    1: dmLinkEndType_Band_View.OpenDB_Channels (qry_Channels , aLinkEndTypeID);
//    1: dmLinkEndType_Band_View.OpenDB_Channels (mem_Channels, aLinkEndTypeID);
  end;
*)

end;




//procedure Tframe_LinkEndType_Band.SetReadOnly(aValue: boolean);
//begin
 // { check : !!! }
 // cxGrid1DBBandedTableView1.OptionsData.Editing:=aValue;
//
//end;

//-----------------------------------------------------------------
procedure Tframe_Template_FreqPlan_Band.qry_BandsNewRecord(DataSet: TDataSet);
//-----------------------------------------------------------------
begin
  DataSet[FLD_LINKENDType_ID]:=FID;
end;



//--------------------------------------------------------------------
procedure Tframe_Template_FreqPlan_Band.OpenDB_Band(aADOQuery: TADOQuery; aID: integer);
//--------------------------------------------------------------------
begin
  aADOQuery.Connection:=dmMain.ADOConnection;

  db_OpenQuery (aADOQuery,
               'SELECT * FROM ' + 'lib_freqplan.LINKENDTYPE_BAND' +
               ' WHERE LinkEndType_id=:LinkEndType_id ORDER BY low',
               [db_par(FLD_LINKENDType_ID, aID)]);

end;







procedure Tframe_Template_FreqPlan_Band.act_channels_forbidden_uncheckExecute(Sender:
    TObject);
begin

end;


// ---------------------------------------------------------------
procedure Tframe_Template_FreqPlan_Band.SetReadOnly(aValue: boolean);
// ---------------------------------------------------------------
begin
  FReadOnly:=aValue;

  { check : !!! }
  cx_SetGridReadOnly (cxGrid1DBTableView_bands_, aValue);

{
  if aValue then
    cxGrid1DBTableView_bands_.Styles.Background:=cxStyle_ReadOnly
  else
    cxGrid1DBTableView_bands_.Styles.Background:=cxStyle_Edit
 }

end;



procedure Tframe_Template_FreqPlan_Band.cxGrid1DBTableView_bandsStylesGetContentStyle(
  Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
  AItem: TcxCustomGridTableItem; out AStyle: TcxStyle);
begin
  if ARecord.RecordIndex mod 2 = 0 then
      AStyle:=cxStyle_row;
end;




end.

(*


//--------------------------------------------------------------
procedure Tframe_Template_FreqPlan_Band.GSPagesClick(Sender: TObject);
//--------------------------------------------------------------
begin
  case PageControl1.ActivePageIndex of

//  case GSPages.ActivePageIndex of
    1: OpenDB_Channels();// (AdoStoredProc_Band_Channels, FID);
//    1: dmLinkEndType_Band_View.OpenDB_Channels (qry_Channels , FID);
//    1: dmLinkEndType_Band_View.OpenDB_Channels (mem_Channels, FID);
  end;

end;




//--------------------------------------------------------------------
procedure Tframe_Template_FreqPlan_Band.OpenDB_Channels;
//--------------------------------------------------------------------
begin
  dmOnega_DB_data.LinkEndType_Band_Channel_Select(sp_Band_Channels, FID);

  cxGrid2DBBandedTableView_channels.ViewData.Expand(True);

//  dmOnega_DB_data.OpenStoredProc(sp_Band_Channels, 'LinkEndType.sp_Band_Channel_SEL', [FLD_ID, FID] );



//  OpenDB_Channels (AdoStoredProc_Band_Channels, FID);



  db_SetFieldCaptions(aADOQuery,
     [
      SArr(FLD_Channel,  CAPTION_CH_LOW),
      SArr(FLD_tx_freq_low, CAPTION_TX_FREQ),
      SArr(FLD_rx_freq_low, CAPTION_RX_FREQ)

      ]);
      *)



(*  col__Channel_low.Caption := CAPTION_CH_LOW;
//  col__Channel_high.Caption:= CAPTION_CH_HIGH;
  col__tx_freq_low.Caption := CAPTION_TX_FREQ;
  col__rx_freq_low.Caption := CAPTION_RX_FREQ;
  col__tx_freq_high.Caption:= CAPTION_TX_FREQ;
  col__rx_freq_high.Caption:= CAPTION_RX_FREQ;

en




procedure Tframe_Template_FreqPlan_Band.col__TxRx_ShiftStylesGetContentStyle(
  Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
  AItem: TcxCustomGridTableItem; out AStyle: TcxStyle);
begin
//  if ARecord.Values[col__TxRx_Shift.Index]=0 then
//    AStyle:=cxStyle_clRed;

end;





//----------------------------------------------------------------
procedure Tframe_Template_FreqPlan_Band.act_channels_forbidden_checkExecute(Sender: TObject);
//----------------------------------------------------------------
var
  bChecked: Boolean;
  i: Integer;
  oStrList: TStringList;
begin
  oStrList:=TStringList.Create;

  //
 //   procedure cx_GetCheckedItemsToStrList(aGrid: TcxGridDBBandedTableView;
//      aOutStrList: TStrings; aKeyFieldName: string = 'id'; aCheckFieldName:   string = 'checked'); overload;
  cx_GetCheckedItemsToStrList (cxGrid2DBBandedTableView_channels, oStrList, 'id', 'checked');


 // k:=oStrList.count;

  bChecked:=Sender=act_channels_forbidden_check;

  for i:=0 to oStrList.count-1 do
    if sp_Band_Channels.Locate('id',oStrList[i],[]) then
      db_UpdateRecord__(sp_Band_Channels, ['checked', bChecked]);


{
  if Sender=act_channels_forbidden_check then
  ;

  if Sender=act_channels_forbidden_uncheck then
  ;
 }

  FreeAndNil(oStrList);

end;



procedure Tframe_Template_FreqPlan_Band.PageControl1Change(Sender: TObject);
begin
  case PageControl1.ActivePageIndex of
//  case GSPages.ActivePageIndex of

    1: OpenDB_Channels();// (qry_Band_Channels , FID);
//    1: dmLinkEndType_Band_View.OpenDB_Channels (qry_Channels , FID);
//    1: dmLinkEndType_Band_View.OpenDB_Channels (mem_Channels, FID);
  end;

end;


function Tframe_Template_FreqPlan_Band.Copy_Band(aBand_ID, aLinkEndType_ID: integer):
    integer;
begin
  Result:=dmOnega_DB_data.ExecStoredProc_('sp_LinkEndType_func',
     [
       FLD_ACTION, 'copy_band',
       FLD_ID, aBand_ID,
       FLD_LinkEndType_ID, aLinkEndType_id
     ]);

end;


//----------------------------------------------------------------
procedure Tframe_Template_FreqPlan_Band.sp_Band_ChannelsAfterPost(DataSet: TDataSet);
//----------------------------------------------------------------
begin
  dmOnega_DB_data.ExecStoredProc_('LinkEndType.sp_Band_Channel_Set_Forbidden_UPD',
     [
       'LinkEndType_Band_ID', DataSet['LinkEndType_Band_ID'],
       'Channel_number',      DataSet['Channel_number'],
       'CHECKED',             DataSet['CHECKED']
     ]);

   qry_Bands.Requery([]);

end;




procedure Tframe_Template_FreqPlan_Band.GetMessage(aMsg: TEventType; aParams:
    TEventParamList; var aHandled: boolean);
begin
  if aMsg= et_Refresh_Modes then
    if qry_Bands.Active then
      qry_Bands.Requery([]);
end;
//-----------------------------------------------------------------
procedure Tframe_Template_FreqPlan_Band.DelBand;
//-----------------------------------------------------------------
begin
  gl_DB.DeleteRecordByID(TBL_LINKENDTYPE_BAND, qry_Bands[FLD_ID]);
  View(FID);
end;





//-----------------------------------------------------------------
procedure Tframe_Template_FreqPlan_Band.qry_BandsBeforeEdit(DataSet: TDataSet);
//-----------------------------------------------------------------
begin
  inherited;

  with DataSet do
  begin
    FChCount     := FieldByName(FLD_CHANNEL_COUNT).AsInteger;
    FTxRxShift   := FieldByName(FLD_TXRX_SHIFT).AsFloat;
    FChWidth     := FieldByName(FLD_CHANNEL_WIDTH).AsFloat;
    FFreqMin_low := FieldByName(FLD_FREQ_MIN_LOW).AsFloat;
    FFreqMax_low := FieldByName(FLD_FREQ_MAX_LOW).AsFloat;
    FFreqMin_high:= FieldByName(FLD_FREQ_MIN_HIGH).AsFloat;
    FFreqMax_high:= FieldByName(FLD_FREQ_MAX_HIGH).AsFloat;
  end;
end;

//-----------------------------------------------------------------
procedure Tframe_Template_FreqPlan_Band.qry_BandsBeforePost(DataSet: TDataSet);
//-----------------------------------------------------------------
var dChannelWidth, dTxRx_Shift : double;
    dFreqMin_low, dFreqMax_low, dFreqMin_high, dFreqMax_high : double;
    iChannelCount: integer;
begin
  inherited;

  with DataSet do
  begin
    dTxRx_Shift    := FieldByName(FLD_TXRX_SHIFT).AsFloat;
    iChannelCount := FieldByName(FLD_CHANNEL_COUNT).AsInteger;
    dChannelWidth := FieldByName(FLD_CHANNEL_WIDTH).AsFloat;
    dFreqMin_low  := FieldByName(FLD_FREQ_MIN_LOW).AsFloat;
    dFreqMax_low  := FieldByName(FLD_FREQ_MAX_LOW).AsFloat;
    dFreqMin_high := FieldByName(FLD_FREQ_MIN_HIGH).AsFloat;
    dFreqMax_high := FieldByName(FLD_FREQ_MAX_HIGH).AsFloat;

    // ���������, ���� ��������� ���� �� ���������
    if (dFreqMin_low > 0) and (dFreqMax_low > 0) and (dTxRx_Shift > 0) and
       (dFreqMin_high = 0) and (dFreqMax_high = 0) then
    begin
      FieldValues[FLD_FREQ_MIN_HIGH]:= dFreqMin_low + dTxRx_Shift;
      FieldValues[FLD_FREQ_MAX_HIGH]:= dFreqMax_low + dTxRx_Shift;
    end

    else
    if (dFreqMin_high > 0) and (dFreqMax_high > 0) and (dTxRx_Shift > 0) and
       (dFreqMin_low = 0) and (dFreqMax_low = 0) then
    begin
      FieldValues[FLD_FREQ_MIN_LOW]:= dFreqMin_high - dTxRx_Shift;
      FieldValues[FLD_FREQ_MAX_LOW]:= dFreqMax_high - dTxRx_Shift;
    end;

    if (dChannelWidth = 0) and (iChannelCount > 0) and (dFreqMin_low > 0) and (dFreqMax_low > 0) then
      FieldValues[FLD_CHANNEL_WIDTH]:= (dFreqMax_low-dFreqMin_low)/(iChannelCount-1)
    else

    if (dFreqMax_low = 0) and (iChannelCount > 0) and (dFreqMin_low > 0) and (dChannelWidth > 0) then
      FieldValues[FLD_FREQ_MAX_LOW]:=  dFreqMin_low+((iChannelCount-1)*dChannelWidth)
    else

    if (dFreqMin_low = 0) and (iChannelCount > 0) and (dChannelWidth > 0) and (dFreqMax_low > 0) then
      FieldValues[FLD_FREQ_MIN_LOW]:= dFreqMax_low-((iChannelCount-1)*dChannelWidth)
    else

    if (iChannelCount = 0) and (dChannelWidth > 0) and (dFreqMin_low > 0) and (dFreqMax_low > 0) then
      FieldValues[FLD_CHANNEL_COUNT]:= (Floor((dFreqMax_low-dFreqMin_low)/dChannelWidth))+1 ;

    // ���������, ���� ��������� ���� ����������
    if (FTxRxShift <> dTxRx_Shift) and (dFreqMin_low > 0) and (dFreqMax_low > 0) then
    begin
      FieldValues[FLD_FREQ_MIN_HIGH]:= dFreqMin_low + dTxRx_Shift;
      FieldValues[FLD_FREQ_MAX_HIGH]:= dFreqMax_low + dTxRx_Shift;
    end

    else
    if ((FFreqMin_low <> dFreqMin_low) and (dFreqMax_low > 0) and (dChannelWidth > 0)) or
       ((FFreqMax_low <> dFreqMax_low) and (dFreqMin_low > 0) and (dChannelWidth > 0)) then
    begin
      FieldValues[FLD_CHANNEL_COUNT]:= (Floor((dFreqMax_low-dFreqMin_low)/dChannelWidth))+1 ;

      if dTxRx_Shift > 0 then
      begin
        FieldValues[FLD_FREQ_MIN_HIGH]:= dFreqMin_low + dTxRx_Shift;
        FieldValues[FLD_FREQ_MAX_HIGH]:= dFreqMax_low + dTxRx_Shift;
      end;
    end

    else
    if (FChWidth <> dChannelWidth) and (dChannelWidth > 0) and (dFreqMin_low > 0) and (dFreqMax_low > 0) then
    begin
      FieldValues[FLD_CHANNEL_COUNT]:= (Floor((dFreqMax_low-dFreqMin_low)/dChannelWidth))+1 ;
    end

    else
    if (FChCount <> iChannelCount) and (iChannelCount <> 0) and (dFreqMin_low > 0) and (dChannelWidth > 0) then
    begin
      FieldValues[FLD_FREQ_MAX_LOW]:=  dFreqMin_low+((iChannelCount-1)*dChannelWidth);
      FieldValues[FLD_FREQ_MIN_HIGH]:= dFreqMin_low + dTxRx_Shift;
      FieldValues[FLD_FREQ_MAX_HIGH]:= dFreqMin_low+((iChannelCount-1)*dChannelWidth) + dTxRx_Shift;
    end;

  end;    // with

end;



//-------------------------------------------------------------------
procedure Tframe_Template_FreqPlan_Band.Dlg_Select_LinkendType;
//-------------------------------------------------------------------
var
  I: Integer;
  iLinkEndTypeID: Integer;
  sLinkEndTypeName: Widestring;

  oList: TList;
begin
  oList:=TList.Create;

  cx_GetSelectedList_TableView(cxGrid1DBTableView_bands, oList);

  iLinkEndTypeID:=0;

  if dmAct_Explorer.Dlg_Select_Object (otLinkEndType, iLinkEndTypeID, sLinkEndTypeName) then
  begin

    for I := 0 to oList.Count - 1 do    // Iterate
      Copy_Band( Integer (oList[i]), iLinkEndTypeID );

//    Assert(iLinkEndID>0, 'Value <=0');

{    dmOnega_DB_data.Object_update_LinkEndType
      (sObjName, iLinkEndID, iLinkEndTypeID, mode_rec.ID);

}
 end;

 FreeAndNil (oList);

end;



// ---------------------------------------------------------------
procedure Tframe_Template_FreqPlan_Band.act_Copy_recordExecute(Sender: TObject);
// ---------------------------------------------------------------
var
  iID: Integer;
  r: Integer;
  sName: string;
begin
{
  if Sender=act_Audit then
    Tdlg_Audit1.ExecDlg (TBL_LinkEndType_Band, FID)
  else
}

{
  // -------------------------
  if Sender = act_Restore_default then
  // -------------------------
  begin
    if PageControl1.ActivePageIndex=0 then
      g_Storage.RestoreFromRegistry(cxGrid1DBTableView_bands, className+'_default')
    else
      g_Storage.RestoreFromRegistry(cxGrid2DBBandedTableView_channels, className+'_default');


   // dxDBTree.LoadFromRegistry (FRegPath +  dxDBTree.Name + '_default');
  end else


 // -------------------------
  if Sender=act_Copy_to_LinkendType then
  // -------------------------
  begin
    Dlg_Select_LinkendType();
  end
 }

(*
 // -------------------------
  if Sender=act_Copy_record then
  // -------------------------
  begin
    sName:='';

    if InputQuery('������� ��������','',sName) then
    begin
      iID:=qry_Bands.FieldByName(FLD_ID).AsInteger;

   //   iMode:=StrToInt(sMode);

      r:=dmOnega_DB_data.ExecStoredProc(sp_LinkEndType_Band_Copy,
             [db_Par(FLD_ID,   iID),
              db_Par(FLD_NAME, sName)
             ]);

      qry_Bands.Requery();

    end;*)

//
//    raise Exception.Create('');
end;




procedure Tframe_Template_FreqPlan_Band.ActionList222Update(Action: TBasicAction; var
    Handled: Boolean);
begin
  act_Copy_to_LinkendType.Enabled:= (not FReadOnly) and (cxGrid1DBTableView_bands.Controller.SelectedRowCount>0) ;
  act_duplicate.Enabled          := (not FReadOnly) and (cxGrid1DBTableView_bands.Controller.SelectedRowCount>0) ;

end;



// ---------------------------------------------------------------
procedure Tframe_Template_FreqPlan_Band.act_duplicateExecute(Sender: TObject);
// ---------------------------------------------------------------
var
  k: Integer;
  sValue: string;
begin
{
 if not inputquery('Test program', 'Please type your name', value)
    then ShowMessage('User cancelled the dialog');
  until value <> '';
}
  sValue:= qry_Bands[FLD_NAME];

  if inputquery('������������ ������', '������� ����� �������� ���������', sValue) then
  begin
    k:=dmOnega_DB_data.ExecStoredProc_('sp_LinkEndType_band_copy',
       [
         FLD_ID, qry_Bands[FLD_ID],
         FLD_NAME, sValue
       ]);

    qry_Bands.Resync([]) ;

//    qry_Bands.Close;
//    qry_Bands.Open;
  end;
end;

