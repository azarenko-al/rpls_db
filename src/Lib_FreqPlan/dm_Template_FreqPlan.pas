unit dm_Template_FreqPlan;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, Variants,
  Db, ADODB, Math,

  dm_Onega_DB_data,


  dm_Main,
  dm_Object_base,

 // u_const_str,
  u_const_db,

  u_link_const,
  u_types,

  u_geo,
  u_db,
  u_radio,
  u_func

  ;


type
  //-------------------------------------------------------------------
  TdmTemplate_FreqPlanAddRec = record
  //-------------------------------------------------------------------
    Name            : string;
    FolderID        : integer;

    Band            : string; //old - range

    Power_max_dBm    : double; // ����������� ��������
    Power_loss_dBm   : double; // ���������� ��������

    FreqMin_MHz         : double;
    FreqMax_MHz         : double;

//    Equaliser_Profit: double; // �������� �����������

//    KNG             : double;

   // failure_period  : double; // ����� ��������� �� �����

    Vendor_id           : Integer;
  //  Vendor_Equipment_id1 : Integer;

  end;


  //-------------------------------------------------------------------
  TdmTemplate_FreqPlan = class(TdmObject_base)
    procedure DataModuleCreate(Sender: TObject);

  private
  public
    function Add(aRec: TdmTemplate_FreqPlanAddRec): integer;

//    function Copy_Band(aBand_ID, aLinkEndType_ID: integer): integer;
//    function Copy_Mode(aMode_ID, aLinkEndType_ID: integer): integer;


//    function GetInfoRec     (aID: integer; var aRec: TdmLinkEndTypeInfoRec): boolean;

//    function GetBandID_by_Name (aBandName: string; aLinkEndType_ID: integer): integer;


  end;



function dmTemplate_FreqPlan: TdmTemplate_FreqPlan;


//================================================================
implementation {$R *.DFM}
//================================================================


var
  FdmTemplate_FreqPlan: TdmTemplate_FreqPlan;


// ---------------------------------------------------------------
function dmTemplate_FreqPlan: TdmTemplate_FreqPlan;
// ---------------------------------------------------------------
begin
  if not Assigned(FdmTemplate_FreqPlan) then
    FdmTemplate_FreqPlan := TdmTemplate_FreqPlan.Create(Application);

  Result := FdmTemplate_FreqPlan;
end;


//--------------------------------------------------------------------
procedure TdmTemplate_FreqPlan.DataModuleCreate(Sender: TObject);
//--------------------------------------------------------------------
begin
  inherited;

  TableName:=TBL_lib_freqplan_LinkEndType;// 'lib_freqplan.LinkEndType';
//  ObjectName:=OBJ_LINKEND_TYPE;
end;


//--------------------------------------------------------------------
function TdmTemplate_FreqPlan.Add(aRec: TdmTemplate_FreqPlanAddRec): integer;
//--------------------------------------------------------------------
begin
 // Assert(aRec.Band<>'', 'aRec.Band<>''''');

  with aRec do
    Result:=dmOnega_DB_data.ExecStoredProc(ADOStoredProc1,
              'lib_freqplan.sp_LinkEndType_INS',

             [FLD_NAME,           Name,
              FLD_FOLDER_ID,      IIF_NULL(FolderID),

              FLD_BAND,           IIF_NULL(Band),

              FLD_POWER_MAX,      IIF_NULL(Power_max_dBm),
              FLD_POWER_LOSS,     IIF_NULL(Power_loss_dBm),

           //   db_Par(FLD_FAILURE_PERIOD, IIF_NULL(FAILURE_PERIOD)),

              FLD_FREQ_MIN,       IIF_NULL(FreqMin_MHz),
              FLD_FREQ_MAX,       IIF_NULL(FreqMax_MHz)

//              FLD_Vendor_ID,           IIF_NULL(aRec.Vendor_ID1)
       //     //  db_Par(FLD_Vendor_Equipment_ID, IIF_NULL(aRec.Vendor_Equipment_ID1))

              ]);
end;

{
function TdmLinkEndType.Copy_Band(aBand_ID, aLinkEndType_ID: integer): integer;
begin
  Result:=dmOnega_DB_data.ExecStoredProc_('sp_LinkEndType_func',
     [
       FLD_ACTION, 'copy_band',
       FLD_ID, aBand_ID,
       FLD_LinkEndType_ID, aLinkEndType_id
     ]);

end;
}


{
function TdmTemplate_FreqPlan.Copy_Mode(aMode_ID, aLinkEndType_ID: integer): integer;
begin
  Result:=dmOnega_DB_data.ExecStoredProc_('sp_LinkEndType_func',
     [
       FLD_ACTION, 'copy_mode',
       FLD_ID, aMode_ID,
       FLD_LinkEndType_ID, aLinkEndType_id
     ]);

end;




//--------------------------------------------------------------------
function TdmTemplate_FreqPlan.GetBandID_by_Name(aBandName: string; aLinkEndType_ID: integer): integer;
//--------------------------------------------------------------------
const
  SQL_SELECT_LINKEND_TYPE_BAND =
     'SELECT id,low,high FROM ' + TBL_LINKENDTYPE_BAND +
     ' WHERE (LinkEndType_id=:LinkEndType_id) and'+
     '       ((low=:name) or (high=:name))';
begin
  if (aBandName='') and (aLinkEndType_ID=0) then
    begin Result:=0; Exit; end;

  db_OpenQuery (qry_LinkEndType_Band, SQL_SELECT_LINKEND_TYPE_BAND,
               [db_par(FLD_LINKENDType_ID, aLinkEndType_ID),
                db_par(FLD_NAME,            aBandName) ]);

  if not qry_LinkEndType_Band.IsEmpty then
    Result:= qry_LinkEndType_Band.FieldByName(FLD_ID).AsInteger
  else
    Result:=0;

end;

//-------------------------------------------------------------------
function TdmTemplate_FreqPlan.GetInfoRec (aID: integer; var aRec: TdmLinkEndTypeInfoRec): boolean;
//-------------------------------------------------------------------
var
  i: integer;
begin
  FillChar (aRec, SizeOf(aRec), 0);

  db_OpenTableByID (qry_Temp, TBL_LinkEndType, aID);
  Result:=not qry_Temp.IsEmpty;

//  db_ViewDataSet(qry_Temp);
  if Result then

    with qry_Temp  do
    begin
  //     RANGE             := FieldByName(FLD_RANGE).AsInteger;
       aRec.Band              := FieldByName(FLD_band).AsString;
       aRec.Power_Max         := FieldByName(FLD_POWER_MAX).AsFloat;
       aRec.KNG               := FieldByName(FLD_KNG).AsFloat;

       aRec.failure_period    := FieldByName(FLD_FAILURE_PERIOD).AsFloat;

       aRec.freq_channel_count:= FieldByName(FLD_freq_channel_count).AsInteger;

       if aRec.freq_channel_count < 1 then
          aRec.freq_channel_count := 1;

  //     Assert (aRec.freq_channel_count >=1);

       aRec.Freq_Min := FieldByName(FLD_Freq_Min).AsFloat;
       aRec.Freq_Max := FieldByName(FLD_Freq_Max).AsFloat;

//       aRec.Freq_Ave_MHz:=(aRec.Freq_Max - aRec.Freq_Min) / 2;
       aRec.Freq_Ave_MHz:=(aRec.Freq_Max + aRec.Freq_Min) / 2;


       aRec.KRATNOST_BY_FREQ  := FieldByName(FLD_KRATNOST_BY_FREQ).AsInteger;
       aRec.KRATNOST_BY_SPACE := FieldByName(FLD_KRATNOST_bY_SPACE).AsInteger;
   //    EQUALISER_PROFIT  := FieldByName(FLD_EQUALISER_PROFIT).AsFloat;

      // Threshold_ber_3:=FieldByName(FLD_Threshold_ber_3).AsFloat;
     //  Threshold_ber_6:=FieldByName(FLD_Threshold_ber_6).AsFloat;
             
    end;

end;

}

begin
end.


