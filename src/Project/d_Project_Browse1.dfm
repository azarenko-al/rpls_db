object dlg_Project_Browse_new: Tdlg_Project_Browse_new
  Left = 631
  Top = 162
  Width = 1232
  Height = 706
  BorderIcons = [biSystemMenu, biMaximize]
  BorderWidth = 3
  Caption = 'dlg_Project_Browse_new'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 629
    Width = 1210
    Height = 32
    Align = alBottom
    BevelOuter = bvNone
    Constraints.MaxHeight = 32
    Constraints.MinHeight = 32
    TabOrder = 0
    DesignSize = (
      1210
      32)
    object Bevel2: TBevel
      Left = 0
      Top = 0
      Width = 1210
      Height = 2
      Align = alTop
      Shape = bsTopLine
    end
    object btn_Ok: TButton
      Left = 1057
      Top = 8
      Width = 75
      Height = 23
      Action = act_Ok
      Anchors = [akTop, akRight]
      Default = True
      ModalResult = 1
      TabOrder = 0
    end
    object Button2: TButton
      Left = 1141
      Top = 8
      Width = 75
      Height = 23
      Anchors = [akTop, akRight]
      Cancel = True
      Caption = #1054#1090#1084#1077#1085#1072
      ModalResult = 2
      TabOrder = 1
    end
  end
  object cxGrid1: TcxGrid
    Left = 0
    Top = 73
    Width = 1210
    Height = 510
    Align = alTop
    Anchors = [akLeft, akTop, akRight, akBottom]
    PopupMenu = PopupMenu1
    TabOrder = 1
    LookAndFeel.Kind = lfFlat
    object cxGrid1DBBandedTableView11: TcxGridDBBandedTableView
      PopupMenu = PopupMenu1
      OnDblClick = cxGrid1DBBandedTableView11DblClick
      NavigatorButtons.ConfirmDelete = False
      DataController.DataSource = ds_Project
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsCustomize.ColumnsQuickCustomization = True
      OptionsCustomize.BandsQuickCustomization = True
      OptionsSelection.CellSelect = False
      OptionsView.GroupByBox = False
      OptionsView.Indicator = True
      Styles.OnGetContentStyle = cxGrid1DBBandedTableView1StylesGetContentStyle
      OnColumnPosChanged = cxGrid1DBBandedTableView11ColumnSizeChanged
      OnColumnSizeChanged = cxGrid1DBBandedTableView11ColumnSizeChanged
      Bands = <
        item
          Width = 448
        end
        item
          Width = 95
        end
        item
          Caption = #1056#1056
          Width = 216
        end
        item
          Caption = 'PMP'
          Visible = False
          Width = 173
        end
        item
          Caption = #1040#1085#1090#1077#1085#1085#1099
          Width = 104
        end
        item
          Caption = '2G'
          Visible = False
          Width = 91
        end>
      object col_id: TcxGridDBBandedColumn
        DataBinding.FieldName = 'id'
        Visible = False
        Options.Filtering = False
        Position.BandIndex = 0
        Position.ColIndex = 4
        Position.RowIndex = 0
      end
      object cxGrid1DBBandedTableView11name: TcxGridDBBandedColumn
        DataBinding.FieldName = 'name'
        Width = 197
        Position.BandIndex = 0
        Position.ColIndex = 0
        Position.RowIndex = 0
      end
      object cxGrid1DBBandedTableView11category: TcxGridDBBandedColumn
        DataBinding.FieldName = 'category'
        Visible = False
        Width = 64
        Position.BandIndex = 0
        Position.ColIndex = 1
        Position.RowIndex = 0
      end
      object cxGrid1DBBandedTableView11property_count: TcxGridDBBandedColumn
        DataBinding.FieldName = 'property_count'
        Width = 53
        Position.BandIndex = 1
        Position.ColIndex = 0
        Position.RowIndex = 0
      end
      object cxGrid1DBBandedTableView11link_count: TcxGridDBBandedColumn
        DataBinding.FieldName = 'link_count'
        Width = 72
        Position.BandIndex = 2
        Position.ColIndex = 0
        Position.RowIndex = 0
      end
      object cxGrid1DBBandedTableView11last_datetime: TcxGridDBBandedColumn
        DataBinding.FieldName = 'last_datetime'
        PropertiesClassName = 'TcxDateEditProperties'
        Properties.SaveTime = False
        Properties.ShowTime = False
        Options.Filtering = False
        SortIndex = 0
        SortOrder = soDescending
        Width = 72
        Position.BandIndex = 0
        Position.ColIndex = 2
        Position.RowIndex = 0
      end
      object cxGrid1DBBandedTableView11DBBandedColumn1: TcxGridDBBandedColumn
        DataBinding.FieldName = 'pmp_site_count'
        Width = 52
        Position.BandIndex = 3
        Position.ColIndex = 1
        Position.RowIndex = 0
      end
      object cxGrid1DBBandedTableView11DBBandedColumn2: TcxGridDBBandedColumn
        DataBinding.FieldName = 'pmp_terminal_count'
        Width = 47
        Position.BandIndex = 3
        Position.ColIndex = 0
        Position.RowIndex = 0
      end
      object cxGrid1DBBandedTableView11DBBandedColumn3: TcxGridDBBandedColumn
        DataBinding.FieldName = 'pmp_calcregion_count'
        Width = 49
        Position.BandIndex = 3
        Position.ColIndex = 2
        Position.RowIndex = 0
      end
      object cxGrid1DBBandedTableView11DBBandedColumn4: TcxGridDBBandedColumn
        DataBinding.FieldName = 'linkfreqplan_count'
        Position.BandIndex = 2
        Position.ColIndex = 1
        Position.RowIndex = 0
      end
      object cxGrid1DBBandedTableView11DBBandedColumn5: TcxGridDBBandedColumn
        DataBinding.FieldName = 'linknet_count'
        Position.BandIndex = 2
        Position.ColIndex = 3
        Position.RowIndex = 0
      end
      object col_linkline_count: TcxGridDBBandedColumn
        DataBinding.FieldName = 'linkline_count'
        Position.BandIndex = 2
        Position.ColIndex = 2
        Position.RowIndex = 0
      end
      object col_date_created: TcxGridDBBandedColumn
        DataBinding.FieldName = 'date_created'
        PropertiesClassName = 'TcxDateEditProperties'
        Properties.SaveTime = False
        Properties.ShowTime = False
        Options.Filtering = False
        Width = 58
        Position.BandIndex = 0
        Position.ColIndex = 3
        Position.RowIndex = 0
      end
      object col_linkend_antenna_count: TcxGridDBBandedColumn
        Caption = #1040#1085#1090#1077#1085#1085#1099
        DataBinding.FieldName = 'linkend_antenna_count'
        Position.BandIndex = 4
        Position.ColIndex = 0
        Position.RowIndex = 0
      end
      object col_linkend_count: TcxGridDBBandedColumn
        Caption = #1056#1056#1057
        DataBinding.FieldName = 'linkend_count'
        Position.BandIndex = 2
        Position.ColIndex = 4
        Position.RowIndex = 0
      end
      object cxGrid1DBBandedTableView11DBBandedColumn6: TcxGridDBBandedColumn
        DataBinding.FieldName = 'host'
        Width = 36
        Position.BandIndex = 0
        Position.ColIndex = 7
        Position.RowIndex = 0
      end
      object cxGrid1DBBandedTableView11DBBandedColumn7: TcxGridDBBandedColumn
        Caption = 'Cells'
        DataBinding.FieldName = 'Cell_count'
        Position.BandIndex = 5
        Position.ColIndex = 1
        Position.RowIndex = 0
      end
      object cxGrid1DBBandedTableView11DBBandedColumn8: TcxGridDBBandedColumn
        Caption = 'Site'
        DataBinding.FieldName = 'Site_count'
        Position.BandIndex = 5
        Position.ColIndex = 0
        Position.RowIndex = 0
      end
      object col_ReadOnly: TcxGridDBBandedColumn
        Caption = #1058#1086#1083#1100#1082#1086' '#1095#1090#1077#1085#1080#1077
        DataBinding.FieldName = 'ReadOnly'
        PropertiesClassName = 'TcxCheckBoxProperties'
        Properties.NullStyle = nssUnchecked
        Visible = False
        Position.BandIndex = 0
        Position.ColIndex = 5
        Position.RowIndex = 0
      end
      object col_link_Repeater_count: TcxGridDBBandedColumn
        DataBinding.FieldName = 'link_Repeater_count'
        Position.BandIndex = 2
        Position.ColIndex = 5
        Position.RowIndex = 0
      end
      object col_user_created: TcxGridDBBandedColumn
        DataBinding.FieldName = 'user_created'
        Width = 85
        Position.BandIndex = 0
        Position.ColIndex = 6
        Position.RowIndex = 0
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1DBBandedTableView11
    end
  end
  object ToolBar1: TToolBar
    Left = 0
    Top = 0
    Width = 1210
    Height = 73
    ButtonHeight = 135
    Caption = 'ToolBar1'
    Color = clBackground
    ParentColor = False
    TabOrder = 2
    Visible = False
  end
  object ActionList1: TActionList
    Left = 336
    Top = 8
    object act_Ok: TAction
      Category = 'Main'
      Caption = #1042#1099#1073#1088#1072#1090#1100
      OnExecute = act_OkExecute
    end
    object act_Cancel: TAction
      Category = 'Main'
      Caption = #1054#1090#1084#1077#1085#1072
    end
    object act_Del: TAction
      Caption = #1059#1076#1072#1083#1080#1090#1100
    end
    object act_Table_align: TAction
      Caption = #1042#1099#1088#1072#1074#1085#1080#1074#1072#1085#1080#1077
      OnExecute = act_Table_alignExecute
    end
    object act_Refresh: TAction
      Caption = #1054#1073#1085#1086#1074#1080#1090#1100
      OnExecute = act_RefreshExecute
    end
  end
  object FormStorage1: TFormStorage
    IniFileName = 'Software\Onega\'
    Options = []
    StoredValues = <>
    Left = 264
    Top = 7
  end
  object ds_Project: TDataSource
    DataSet = ADOStoredProc1
    Left = 144
    Top = 8
  end
  object PopupMenu1: TPopupMenu
    Left = 392
    Top = 8
    object N3: TMenuItem
      Action = act_Refresh
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object N2: TMenuItem
      Caption = #1058#1072#1073#1083#1080#1094#1072
      object actTablealign1: TMenuItem
        Action = act_Table_align
      end
    end
    object actDel1: TMenuItem
      Action = act_Del
      Visible = False
    end
  end
  object cxStyleRepository1: TcxStyleRepository
    Left = 528
    Top = 8
    PixelsPerInch = 96
    object cxStyle1: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clLime
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
    end
    object cxStyle_ReadOnly: TcxStyle
      AssignedValues = [svColor]
      Color = clYellow
    end
  end
  object ADOStoredProc1: TADOStoredProc
    Parameters = <>
    Left = 112
    Top = 8
  end
end
