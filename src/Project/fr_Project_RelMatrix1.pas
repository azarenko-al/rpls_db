unit fr_Project_RelMatrix1;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, ADODB, Menus, ActnList, ToolWin, ComCtrls,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView, cxGraphics, dxBar,


 // I_Act_RelFile, //  dm_Act_RelFile;


  dm_User_Security,
  u_const_msg,
  u_func_msg,

  u_Storage,

  dm_Onega_DB_data,

 // f_Custom,

  dm_Main,
//  I_RelFile,

  dm_RelMatrixFile,
  dm_Rel_Engine,

  u_func,
  u_db,
  u_cx,
  u_classes,

  u_const_str,
  u_const_db,

  ActnMan, cxStyles, ExtCtrls, AppEvnts, cxGridLevel, cxGridDBTableView,
  cxClasses, cxControls, cxGrid;

type
  Tframe_Project_RelMatrix1 = class(TForm, IMessageHandler)
    MainActionList: TActionList;
    act_Add: TAction;
    act_Del: TAction;
    act_AddFromDir: TAction;
    ds_Relief: TDataSource;
    act_CheckExistance: TAction;
    PopupMenu1: TPopupMenu;
    actAdd1: TMenuItem;
    actAddFromDir1: TMenuItem;
    actCheckExistance1: TMenuItem;
    actDel1: TMenuItem;
    act_Export_Frames_to_MIF: TAction;
    N1: TMenuItem;
    act_UnCheck_all: TAction;
    act_Check_all: TAction;
    N2: TMenuItem;
    N3: TMenuItem;
    cxGrid1DBTableView11: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    col_checked: TcxGridDBColumn;
    col_name: TcxGridDBColumn;
    col_FileSize: TcxGridDBColumn;
    col_comment: TcxGridDBColumn;
    col__id: TcxGridDBColumn;
    act_Dlg_ShowMatrixList: TAction;
    N4: TMenuItem;
    col_Exists: TcxGridDBColumn;
    dxBarManager1: TdxBarManager;
    dxBarSubItem1: TdxBarSubItem;
    dxBarButton1: TdxBarButton;
    dxBarButton2: TdxBarButton;
    dxBarButton3: TdxBarButton;
    dxBarButton4: TdxBarButton;
    dxBarButton5: TdxBarButton;
    dxBarButton6: TdxBarButton;
    col_x_min: TcxGridDBColumn;
    col_y_min: TcxGridDBColumn;
    col_x_max: TcxGridDBColumn;
    col_y_max: TcxGridDBColumn;
    col_Step_X: TcxGridDBColumn;
    col_Step_y: TcxGridDBColumn;
    col_B_min: TcxGridDBColumn;
    col_L_min: TcxGridDBColumn;
    col_B_max: TcxGridDBColumn;
    col_L_max: TcxGridDBColumn;
    col_FileDate: TcxGridDBColumn;
    col_Relief_id: TcxGridDBColumn;
    ADOStoredProc1: TADOStoredProc;
    cxStyleRepository1: TcxStyleRepository;
    cxStyle1_Strikeout: TcxStyle;
    Panel1: TPanel;
    act_Check: TAction;
    act_UnCheck: TAction;
    actCheck1: TMenuItem;
    actUnCheck1: TMenuItem;
//    procedure ApplicationEvents1Message(var Msg: tagMSG; var Handled: Boolean);
    procedure qry_ReliefBeforePost(DataSet: TDataSet);
    procedure FormDestroy(Sender: TObject);
    procedure FormCreate(Sender: TObject);

    procedure _Actions(Sender: TObject);
    procedure col_checkedPropertiesChange(Sender: TObject);
    procedure cxGrid1DBTableView11ColumnPosChanged(Sender: TcxGridTableView; AColumn:
        TcxGridColumn);
    procedure qry_ReliefAfterPost(DataSet: TDataSet);
    procedure cxGrid1DBTableView11StylesGetContentStyle(
      Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
      AItem: TcxCustomGridTableItem; out AStyle: TcxStyle);

  private
    FRegPath : string;

    FProjectID: Integer;
    FDataSet: TDataSet;

    procedure GetMessage(aMsg: TEventType; aParams: TEventParamList; var aHandled:
        boolean);
  public
    procedure SetReadOnly(Value: Boolean);

    procedure View(aProjectID: Integer);
  end;



//=============================================================
// implementation
//=============================================================
implementation

uses dm_Act_RelFile;

 {$R *.DFM}


                  
const
  STR_ADDFROMDIR = '�������� �� �����';
  STR_ADD        = '��������';
  STR_DEL        = '�������';



procedure Tframe_Project_RelMatrix1.qry_ReliefBeforePost(DataSet: TDataSet);
begin

end;

//////  STR_CHECKED    = '������������ ��� �������';


//-------------------------------------------------------------
procedure Tframe_Project_RelMatrix1.FormCreate(Sender: TObject);
//-------------------------------------------------------------
begin
  inherited;

 // Assert(Assigned(IRelFileAct));

//  FRegPath:=FRegPath+'1';
  FRegPath:=g_Storage.GetPathByClass(ClassName);


  act_Check.Caption:='��������';
  act_UnCheck.Caption:='���������';

  act_Check_all.Caption:='�������� ���';
  act_UnCheck_all.Caption:='��������� ���';


  act_Add.Caption:=         STR_ADD;
  act_Del.Caption:=         STR_DEL;
  act_AddFromDir.Caption:=  STR_ADDFROMDIR;
  act_CheckExistance.Caption:= '��������� ������������� ������';

//  act_Check.Caption:='';
 // act_UnCheck.Caption:='';



  col_Name.Caption:=    STR_NAME;
  col_Comment.Caption:= STR_COMMENT;
//  col_Checked.Caption:=     STR_CHECKED;

  cxGrid1.Align:= alClient;


//  cxGrid1DBTableView11.RestoreFromRegistry(FRegPath + cxGrid1DBTableView11.Name+'2');


  g_Storage.RestoreFromRegistry(cxGrid1DBTableView11, className);


  col_x_min.Visible := False;
  col_y_min.Visible := False;
  col_x_max.Visible := False;
  col_y_max.Visible := False;

  col_b_min.Visible := False;
  col_b_min.Visible := False;
  col_l_max.Visible := False;
  col_l_max.Visible := False;

  col_step_x.Visible := False;
  col_step_y.Visible := False;

  FDataSet:=ADOStoredProc1;

end;

procedure Tframe_Project_RelMatrix1.FormDestroy(Sender: TObject);
begin
  db_PostDataset(adoStoredProc1);

  g_Storage.StoreToRegistry(cxGrid1DBTableView11, className);

//  cxGrid1DBTableView11.StoreToRegistry(FRegPath + cxGrid1DBTableView11.Name+'2');

  inherited;
end;


procedure Tframe_Project_RelMatrix1.SetReadOnly(Value: Boolean);
begin
  ADOStoredProc1.Close;

//  ADOStoredProc1.LockType := IIF(dmUser_Security.ProjectIsReadOnly, ltReadOnly, ltBatchOptimistic);

//  SetFormActionsEnabled(Self, not dmUser_Security.ProjectIsReadOnly);


(*  ADOStoredProc1.LockType := IIF(dmUser_Security.ProjectIsReadOnly, ltReadOnly, ltBatchOptimistic);

  SetFormActionsEnabled(Self, not dmUser_Security.ProjectIsReadOnly);

  cxGrid1DBTableView1.OptionsData.Editing := not Value;
  FReadOnly := Value;
*)
end;

//-------------------------------------------------------------
procedure Tframe_Project_RelMatrix1.View(aProjectID: Integer);
//-------------------------------------------------------------
var
  iRes: Integer;
begin
  FProjectID:= aProjectID;

(*  ADOStoredProc1.LockType := IIF(dmUser_Security.ProjectIsReadOnly, ltReadOnly, ltBatchOptimistic);

  SetFormActionsEnabled(Self, not dmUser_Security.ProjectIsReadOnly);

*)

//  if dmUser_Security.ProjectIsReadOnly then
//    ADOStoredProc1.LockType :=ltReadOnly
//  else
//    ADOStoredProc1.LockType :=ltBatchOptimistic;


 /////// dmOnega_DB_data.Relief_Init(aProjectID);
  iRes:=dmOnega_DB_data.Relief_Select(ADOStoredProc1, aProjectID);


//  db_ViewDataSet(qry_Relief);
end;

//-------------------------------------------------------------
procedure Tframe_Project_RelMatrix1._Actions(Sender: TObject);
//-------------------------------------------------------------
var
  bChecked: Boolean;
  I, iID: Integer;
  oIDList1: TIDList;
begin

  if Sender=act_Export_Frames_to_MIF then
    dmAct_RelFile.Dlg_SaveRelFramesToMIF

  else
  //  dmRelMatrixFile.SaveBoundFramesToMIF(aFileName: string);


  if Sender=act_Add then
  begin
     dmAct_RelFile.Dlg_Add();// then
//    dmAct_RelFile.act_Add.Execute;
      View(FProjectID);
  end else


  // -------------------------------------------------------------------
  if Sender=act_Del then
  // -------------------------------------------------------------------
  begin
    oIDList1:= TIDList.Create;
  //  oIDList.Clear;

   // cx_UpdateSelectedItems_Table (cxGrid1DBTableView1, FLD_CHECKED, bChecked);


    with cxGrid1DBTableView11.Controller do
      for i := 0 to SelectedRowCount - 1 do
      begin
        iID:=SelectedRows[i].Values[col_Relief_id.Index];
//        iID:=SelectedRows[i].Values[col__id.Index];
     //   iID:=cxGrid1DBTableView1.DataController.Values[SelectedRows[i].RecordIndex, col__id.Index];
        oIDList1.AddID(iID);
      end;


{
    for I := 0 to dx_Matrix.SelectedCount-1 do
    begin
      v:=aGrid.DataController.Values[SelectedRows[i].RecordIndex, iKeyFieldInd];


      iID:= dx_Matrix.SelectedNodes[i].Values[col_id.Index];
      oIDList.AddID(iID);
    end;
}


    if oIDList1.Count>0 then
      dmAct_RelFile.Del_List(oIDList1);

    FreeAndNil(oIDList1);


    View(FProjectID);
  end else

  // -------------------------------------------------------------------
  if Sender=act_CheckExistance then
  // -------------------------------------------------------------------
  begin
  //  FIsMOdify:=True;
//    dmRelMatrixFile.SaveExistanceToReg(mem_Items);

    dmRelMatrixFile.SaveFileExistToDB(ADOStoredProc1);


  end else


  // -------------------------------------------------------------------
  if (Sender = act_Check_All) or (Sender = act_UnCheck_All) then
  // -------------------------------------------------------------------
  begin
    bChecked:=(Sender = act_Check_All);

    cxGrid1DBTableView11.Controller.SelectAll;

   // FIsMOdify:=True;
    cx_UpdateSelectedItems_Table (cxGrid1DBTableView11, FLD_CHECKED, bChecked);

  ///  FIsMOdify:=False;

 //   cx_SetValuesForSelectedItems (cxGrid1DBTableView1, FLD_CHECKED,  bChecked);


{    dx_Matrix.SelectAll;
    dx_SetValuesForSelectedItems (dx_Matrix, FLD_CHECKED,  bChecked);
    dx_Matrix.ClearSelection;
}
 // ��  IsChanged:= true;

 //   DoMapChecked();

  end  else

 // -------------------------------------------------------------------
  if (Sender = act_Check) or (Sender = act_UnCheck) then
  // -------------------------------------------------------------------
  begin
    bChecked:=(Sender = act_Check);

//    cxGrid1DBTableView11.Controller.SelectAll;

   // FIsMOdify:=True;
    cx_UpdateSelectedItems_Table (cxGrid1DBTableView11, FLD_CHECKED, bChecked);

  ///  FIsMOdify:=False;

 //   cx_SetValuesForSelectedItems (cxGrid1DBTableView1, FLD_CHECKED,  bChecked);


{    dx_Matrix.SelectAll;
    dx_SetValuesForSelectedItems (dx_Matrix, FLD_CHECKED,  bChecked);
    dx_Matrix.ClearSelection;
}
 // ��  IsChanged:= true;

 //   DoMapChecked();

  end  else



  if Sender=act_Dlg_ShowMatrixList then
    dmRel_Engine.Dlg_ShowMatrixList
  else

  //-------------------------------------------------------------------
  if Sender=act_AddFromDir then
  //-------------------------------------------------------------------
  begin
    if dmAct_RelFile.Dlg_LoadReliefFromDir() then
      View(FProjectID);
  end;


end;

//-------------------------------------------------------------------
procedure Tframe_Project_RelMatrix1.col_checkedPropertiesChange( Sender: TObject);
//-------------------------------------------------------------------
var
  b: boolean;
  s: string;
begin

  b:=FDataSet.FieldByName(FLD_CHECKED).AsBoolean;
  s:=FDataSet.FieldByName(FLD_NAME).AsString;

  if not b then
    dmRel_Engine.CloseFile(s);

end;

procedure Tframe_Project_RelMatrix1.cxGrid1DBTableView11ColumnPosChanged(Sender:  TcxGridTableView; AColumn: TcxGridColumn);
begin
  Sender.Tag := 1;
end;


procedure Tframe_Project_RelMatrix1.cxGrid1DBTableView11StylesGetContentStyle(
  Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
  AItem: TcxCustomGridTableItem; out AStyle: TcxStyle);
var
  v: Variant;
begin
  v:=ARecord.Values[col_Exists.Index];

  if v=False then
    AStyle:=cxStyle1_Strikeout;
end;

// ---------------------------------------------------------------
procedure Tframe_Project_RelMatrix1.qry_ReliefAfterPost(DataSet: TDataSet);
// ---------------------------------------------------------------
var
  k: Integer;
begin
  k:=dmOnega_DB_data.Relief_XREF_Update1 (
       DataSet.FieldByName(FLD_RELIEF_ID).AsInteger,
   //    DataSet.FieldByName(FLD_ID).AsInteger,
       DataSet.FieldByName(FLD_CHECKED).AsBoolean);

end;


procedure Tframe_Project_RelMatrix1.GetMessage(aMsg: TEventType; aParams:
    TEventParamList; var aHandled: boolean);
begin
 case aMsg of

    et_PROJECT_UPDATE_MATRIX_LIST: //begin
//      Fframe_RelMatrix.SetViewObjects([otRelMatrixFile]);
  //    Fframe_RelMatrix.Load;
      View(FProjectID);

   // end;
  end;
end;

end.




(*
//-------------------------------------------------------------------
procedure Tframe_Project_view.GetMessage(aMsg: integer; aParams: TEventParamList; var aHandled:
    Boolean);
//-------------------------------------------------------------------
begin
  inherited;

  case aMsg of

    WE_PROJECT_UPDATE_MATRIX_LIST: begin
//      Fframe_RelMatrix.SetViewObjects([otRelMatrixFile]);
  //    Fframe_RelMatrix.Load;
      Fframe_RelMatrix.View(ID);

    end;
  end;
end;

*)


procedure Tframe_Project_RelMatrix1.ApplicationEvents1Message(var Msg: tagMSG;  var Handled: Boolean);
begin

 case Msg.message of

    WE_PROJECT_UPDATE_MATRIX_LIST1: begin
//      Fframe_RelMatrix.SetViewObjects([otRelMatrixFile]);
  //    Fframe_RelMatrix.Load;
      View(FProjectID);

    end;
  end;
end;

