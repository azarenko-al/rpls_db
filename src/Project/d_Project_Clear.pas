unit d_Project_Clear;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls, rxPlacemnt, ActnList, cxGridDBTableView, cxGrid,
  cxPropertiesStore, cxGridLevel, cxClasses, cxControls, ADODB, DB,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView, ComCtrls,

  d_Wizard,


  I_Object,
  I_Shell,
  u_shell_new,


  dm_MapEngine_store,

  u_log,

  dm_Onega_DB_data,

  dm_Main,

  d_Progress,


  u_func,
  u_func_msg,

  u_db,
  u_files,
 // u_reg,

 u_types,

  u_const_db,
  u_const_msg,

  u_const,
 // u_run,
  ToolWin, cxLookAndFeels, cxGraphics, cxLookAndFeelPainters, cxStyles,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxDBData,
  dxSkinsCore, dxSkinsDefaultPainters

  ;



type
  Tdlg_Project_Clear = class(Tdlg_Wizard)
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    DataSource1: TDataSource;
    col_Table: TcxGridDBColumn;
    col_Count: TcxGridDBColumn;
    col_category: TcxGridDBColumn;
    col_caption: TcxGridDBColumn;
    StatusBar1: TStatusBar;
    ADOStoredProc1: TADOStoredProc;
    ADOQuery1: TADOQuery;
    procedure act_OkExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    procedure ExecuteProc(Sender: TObject);
  public
    class function ExecDlg: Boolean;
  end;



implementation {$R *.DFM}


//--------------------------------------------------------------------
class function Tdlg_Project_Clear.ExecDlg: Boolean;
//--------------------------------------------------------------------
begin
  with Tdlg_Project_Clear.Create(Application) do
  begin
    Result :=  ShowModal=mrOK;

    Free;
  end;

 // if iIndex<0 then
 //   Exit;

end;

// -------------------------------------------------------------------
procedure Tdlg_Project_Clear.FormCreate(Sender: TObject);
// -------------------------------------------------------------------
var
  i: Integer;
begin
  inherited;
  SetActionName ('');

  HideHeader;

  db_SetComponentADOConnection(Self, dmMain.ADOConnection);

  cxGrid1.Align:=alClient;

  i:=dmOnega_DB_data.Project_GetTables(ADOStoredProc1, dmMain.ProjectID);

  Assert(ADOStoredProc1.Active);


//  db_Ope nQuery(ADOQuery1, Format('exec %s %d',[sp_Project_GetTables, dmMain.ProjectID]));

// .. col_caption



{  ADODataSet1.Active:=False;
  ADODataSet1.CommandText:= Format('exec %s %d',[sp_Project_GetTables, dmMain.ProjectID]);
  ADODataSet1.Active:=True;
}

  StatusBar1.SimpleText:=Format('project: %d',[dmMain.ProjectID]);

end;


//--------------------------------------------------------------------
procedure Tdlg_Project_Clear.act_OkExecute(Sender: TObject);
//--------------------------------------------------------------------
begin
  Progress_ExecDlg('��������...', ExecuteProc);
end;


//-------------------------------------------------------------------
procedure Tdlg_Project_Clear.ExecuteProc(Sender: TObject);
//-------------------------------------------------------------------
var
  s: string;
  sSQL: string;
  sSQL_check: string;
  bTerminated: boolean;
  k: Integer;
begin
  bTerminated:=False;


  k:=dmOnega_DB_data.Project_Clear_(dmMain.ProjectID);


   (*

  with ADOStoredProc1 do
  begin
    First;

    while (not EOF) and (not bTerminated)do
    begin
      dlg_Progress.DoOnProgress1(RecNo, RecordCount, bTerminated);

      s:=FieldValues[FLD_NAME];


      if Eq(s, 'linknet') then
      begin
        sSQL:=Format('DELETE FROM linkfreqplan WHERE linknet_id in  (SELECT id FROM linknet  where Project_ID = %d)', [ dmMain.ProjectID]);
        g_log.Add(sSQL);

        dmMain.ADOConnection.Execute(sSQL);
      end;

      {

      if Eq(s, 'map') or  Eq(s, 'calcMap') then
      begin
        sSQL:=Format('DELETE FROM Map_XREF WHERE map_desktop_id in  (SELECT id FROM Map_Desktops  where Project_ID = %d)', [ dmMain.ProjectID]);
        g_log.Add(sSQL);

        dmMain.ADOConnection.Execute(sSQL);
      end;
       }


      sSQL:=Format('DELETE FROM %s WHERE project_id=%d', [s, dmMain.ProjectID]);

      g_log.Add(sSQL);


      sSQL_check:=Format('SELECT * FROM %s WHERE project_id=%d   ', [s, dmMain.ProjectID]);


   //   dmMain.ADOConnection.BeginTrans;
     try
      dmMain.ADOConnection.Execute(sSQL);

//      dmOnega_DB_data.e OpenQuery(ADOQuery1, sSQL_check);

      dmOnega_DB_data.OpenQuery(ADOQuery1, sSQL_check);

      Assert(ADOQuery1.RecordCount=0, sSQL_check + IntToStr(ADOQuery1.RecordCount));

     except // wrap up
       // g_log.AddError(sSQL);
     end;    // try/finally

   //   dmMain.ADOConnection.CommitTrans;

      Next;
    end;


    First;
  end;

  *)

 /// dmOnega_DB_data.Project_Clear_(dmMain.ProjectID);




//  dmMapEngine_store.Reload_Object_Layers;// Feature_Add (OBJ_PMP_CALC_REGION, iID);

  dmMapEngine_store.CloseAllData;

  DirClear( dmMain.Dirs.Dir );

  g_Log.Add('Clear dir: '+ dmMain.Dirs.Dir);

  g_Shell.UpdateNodeChildren_ByGUID(GUID_PROJECT);

  g_EventManager.PostEvent_(et_PROJECT_CLEAR,[]);

//  PostUserMessage(WM__PROJECT_CLEAR1);

end;



end.