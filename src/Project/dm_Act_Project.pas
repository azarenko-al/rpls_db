unit dm_Act_Project;

interface

{$I ver.inc}


uses
  SysUtils, Classes, Forms, Menus, ActnList, 

  i_Audit,
  dm_Main,

 // d_Audit,

  dm_MapEngine_store,

  dm_Onega_DB_data,

  I_Object,

  I_Shell,
  u_shell_new,

  d_Project_Clear,

  //u_func_reg_lib,

  dm_act_Base,

  dm_Project,

  u_const_db,

 // dm_MapEngine,

  u_func_msg,
  


  u_const_str,
  u_const,

 // u_map_vars,

  d_Project_Browse1,
//  d_Project_Browse,
  d_Project_add,

  fr_Project_view,

  u_db,
  u_func,
  u_Log,
  u_reg,

  u_types, DB, ADODB
  ;


type
  TdmAct_Project = class(TdmAct_Base)
    act_Clear: TAction;
    act_LocalDir: TAction;
    act_NetDir: TAction;
    act_Service_Map_Rebuild1111: TAction;
    act_Clear_all____: TAction;
    act_Export: TAction;
    act_Import: TAction;
    act_ReloadFromDir: TAction;
    act_ShowInExplorer: TAction;
    ActionList2: TActionList;
    act_Audit: TAction;
    ADOStoredProc1: TADOStoredProc;
    procedure DataModuleCreate(Sender: TObject);

  private
    FOnProjectChanged1: TNotifyEvent;
    FRegPath: string;
  
    procedure DoAction (Sender: TObject);
// TODO: DoRefreshAll
//  procedure DoRefreshAll();

  protected
    procedure GetPopupMenu (aPopupMenu: TPopupMenu; aMenuType: TMenuType); override;
    function  GetViewForm  (aOwnerForm: TForm): TForm; override;

    function ItemDel(aID: integer; aName: string = ''): boolean; override;
// TODO: ItemAdd
//  function ItemAdd (aFolderID: integer): integer; override;

  public
    function Dlg_Open(aID: integer): boolean;

    procedure NewDlg();

    procedure LoadLastProject;
    function  GetLastProjectID: integer;
//    procedure SaveLastProject;

    procedure Dlg_Browse;
    procedure Close (aAskConfirm: boolean=False);

    property OnProjectChanged1: TNotifyEvent read FOnProjectChanged1 write
        FOnProjectChanged1;

    class procedure Init;
  end;


var
  dmAct_Project: TdmAct_Project;

//====================================================================
//====================================================================
implementation

{$R *.dfm}


//--------------------------------------------------------------------
class procedure TdmAct_Project.Init;
//--------------------------------------------------------------------
begin
  if not Assigned(dmAct_Project) then
    dmAct_Project:=TdmAct_Project.Create(Application);
end;


//-------------------------------------------------------------------
procedure TdmAct_Project.DataModuleCreate(Sender: TObject);
//-------------------------------------------------------------------
begin
  inherited;

  ObjectName:=OBJ_PROJECT;

  FRegPath:=REGISTRY_ROOT +ClassName+'\';
  
// new ----------------
  act_LocalDir.Caption :='������� ��������� �������';
  act_NetDir.Caption   :='������� ������� �������';

  act_Clear.Caption   :='�������� ������';
//  act_Clear_all.Caption   :='�������� DB';

 // act_Service_Map_Rebuild.Caption :='����������� �����';

  act_Add.Caption :='������� ������';

  act_ShowInExplorer.Caption:= '�������� � ����������';

  act_Clear.Visible:= true;

  act_Audit.Caption:=STR_Audit;
  //act_Audit.Enabled:=False;


  SetActionsExecuteProc
    ([//act_LocalDir,
     // act_NetDir,
       act_Clear,
//      act_Clear_all,
      act_Import,
      act_Export,
    //  act_Service_Map_Rebuild,
      act_ReloadFromDir,

      act_Audit,


      act_ShowInExplorer

     ], DoAction);


  SetCheckedActionsArr([
        act_Del_,
//        act_Clear_all,
        act_Clear
     ]);


(*  SetCheckedActionsArr([
        act_Add,
        act_Del_,
        act_Del_list,

        act_UpdateAntennaAzimuth,

        act_Copy,
        act_Update_Name_by_linkends,

        act_Calc,
        act_Calc_Adaptive,
        act_Del_with_LinkEnds,

        act_Del_with_Property,
        act_Add_LinkLine,
        act_Create_Net,

        act_Link_profile_Load_XML,
        act_Link_profile_Load_from_CSV,

        act_Move_By_GeoRegions,

        act_GroupAssign
     ]);

*)

end;





//-------------------------------------------------------------------
procedure TdmAct_Project.DoAction (Sender: TObject);
//-------------------------------------------------------------------
var
  sGUID: string;
begin
  if Sender=act_Audit then
    Init_IAuditX.ExecDlg( dmMain.GetConnectionString, TBL_Project, FFocusedID) else

                                 
{  if Sender=act_LocalDir  then Select_LocalDir (FFocusedID) else
  if Sender=act_NetDir    then Select_NetDir (FFocusedID) else
}

  // ---------------------------------------------------------------
  if Sender = act_ShowInExplorer then
  // ---------------------------------------------------------------
  begin
//    sFile:=  FDataSet.FieldByName(FLD_FILENAME).AsString;

  //  sDir := dmMain.Dirs.ProjectMapDir;

  //  if (sFile <> '') then //and   //   (mem_CalcMaps.FieldByName(FLD_EXIST).AsBoolean)
    ShellExec('explorer.exe', dmMain.Dirs.Dir);
  end else


  //--------------------------------------------------------------
  if Sender=act_Clear then
  //--------------------------------------------------------------
  begin
    if not Tdlg_Project_Clear.ExecDlg then
      Exit;

 // zz//  if not ConfirmDlg('��������?') then
 //     exit;


  //  dmMapEngine1.CloseDB();
//    dmMapEngine_MDB.CloseDB();
//    dmMapEngine.CloseDB();
    dmProject.Clear (FFocusedID);

 (*   dmAct_Map.MAP_SAVE_AND_CLOSE_WORKSPACE;

    dmMapEngine1.Rebuild1;
*)
//    dmMapEngine1.ChangeProject1;

    sGUID := dmProject.GetGUIDByID(dmMain.ProjectID);
    g_Shell.UpdateNodeChildren_ByGUID(sGUID);

//    sh_PostUpdateNodeChildren(dmProject.GetGUIDByID(dmMain.ProjectID));
  end  else

{  if Sender=act_Clear_all then
    dmProject.ClearAll()

  else}

//  if Sender=act_Export    then ExportDlg()  else
//  if Sender=act_Import    then ImportDlg()  else

(*  if Sender=act_ReloadFromDir    then
    dmCalcMap_import.ReloadFromDir ()

    else*)


(*  if Sender=act_Service_Map_Rebuild  then
  begin
    dmAct_Map.MAP_SAVE_AND_CLOSE_WORKSPACE;

    dmMapEngine_store.CloseAllData;

////////    dmMapEngine.Rebuild1;

    dmAct_Map.MAP_RESTORE_WORKSPACE;
  end;
*)
    //PostEvent (WE_MAP_ENGINE_REBUILD);

end;


//--------------------------------------------------------------------
function TdmAct_Project.ItemDel(aID: integer; aName: string = ''): boolean;
//--------------------------------------------------------------------
begin
  Result:=dmProject.Del (aID);
end;


//--------------------------------------------------------------------
procedure TdmAct_Project.Close (aAskConfirm: boolean=False);
//--------------------------------------------------------------------
begin
  if aAskConfirm and
     (not ConfirmDlg ('������� ������ ?'))
  then
    Exit;

  with dmMain do begin
    ProjectID      :=0;
   // LoginRec.
    ProjectName    :='';
//    ProjectLocalDir:='';
//    ProjectNetDir  :='';
  end;

//  _PostMessage11111(WE_PROJECT_CLOSED);

  g_EventManager.postEvent_ (et_PROJECT_CHANGED, []);

//  PostEvent (WE_PROJECT_CHANGED_);
  g_ShellEvents.PostEvent(WE__EXPLORER_ReLoad_Project);

end;

//--------------------------------------------------------------------
function TdmAct_Project.GetViewForm (aOwnerForm: TForm): TForm;
//--------------------------------------------------------------------
begin
  Result:=Tframe_Project_View.Create(aOwnerForm);
end;

//--------------------------------------------------------------------
procedure TdmAct_Project.NewDlg();
//--------------------------------------------------------------------
var iID: integer;
begin
  iID:=Tdlg_Project_add.ExecDlg ();
  if iID>0 then
  begin
    Dlg_Open (iID);

    reg_WriteInteger(FRegPath, FLD_PROJECT_ID, iID);

  end;

//  Result:=Tdlg_Project_add.ExecDlg (aFolderID);

 // iProjectID:=



/// / iID:=Tdlg_Project_Add.ExecDlg;
end;


//--------------------------------------------------------------------
procedure TdmAct_Project.Dlg_Browse;
//--------------------------------------------------------------------
var iID: integer;
 sName: string;

begin
  iID:=Tdlg_Project_Browse_new.ExecDlg(dmMain.Projectid, sName);


//  iID:=Tdlg_Project_Browse.ExecDlg;

  if iID>0 then
  begin
    reg_WriteInteger(FRegPath, FLD_PROJECT_ID, iID);

    Dlg_Open (iID);

//    FRegPath
  //  gl_Reg.RegIni.WriteInteger (ClassName, FLD_PROJECT_ID, dmMain.ProjectID);

  end;
end;

//--------------------------------------------------------------------
function TdmAct_Project.Dlg_Open(aID: integer): boolean;
//--------------------------------------------------------------------
const
  SP_Project_Open = 'sp_Project_Open';

var
//  b: Boolean;
  iResult: Integer;
  S: string;
  sName: string;
begin
  if aID=0 then
    Exit;

  iResult := dmOnega_DB_data.OpenStoredProc(
        ADOStoredProc1, SP_Project_Open, [FLD_ID,aID]);

  if iResult<=0 then

//  if not gl_DB.RecordExists (TBL_PROJECT, [db_Par(FLD_ID, aID)])
  begin
    g_Log.AddError (TdmAct_Project.ClassName, Format('id not correct: %d', [aID]));

  //  aID:= Tdlg_Project_Browse1.ExecDlg;

  //aProject_ID, aName


//    class function ExecDlg(aProject_ID: Integer; var aName: string): integer;

    aID:= Tdlg_Project_Browse_new.ExecDlg(aID, sName);

    if aID>0 then
    begin

    end else
      Exit;
  end;

  iResult := dmOnega_DB_data.OpenStoredProc(ADOStoredProc1, SP_Project_Open, [FLD_ID,aID]);
  if iResult<=0 then
    Exit;


   reg_WriteInteger(FRegPath, FLD_PROJECT_ID, aID);


 // dmOnega_DB_data.Activity_Log('project.open',aID);

  with dmMain do
  begin
    ProjectID   :=aID;
    ProjectName :=ADOStoredProc1.FieldByName(FLD_NAME).AsString;

//    dmUser_Security.ProjectIsReadOnly :=ADOStoredProc1.FieldByName(FLD_READONLY).AsBoolean;


/////////    ProjectCalcDir:= dmProject.GetCalcDir (aID);
//    ProjectLocalDir:=dmProject.GetLocalDir (aID);
//    ProjectNetDir  :=dmProject.GetSharedDir (aID);
  end;

//  TdmUser_Security.Init;
//  dmUser_Security.Open;

  if assigned(FOnProjectChanged1) then
    FOnProjectChanged1(nil);


 /////// dmProject_Tools.Update_Blank_LatLonStr;



 /////// dmProject_Tools.UpdateGeoRegions();

 // g_Obj.ReAssign;
////////  dmMapEngine.MapFilteredObjectList.Clear;

{  dmObjects.Dlg_Open;

  dmOptions.LoadDefault (dmMain.ADOConnection);
//  dmOptions.SyncXrefFieldsWithOptions (otSite, dmMain.ADOConnection);
  dmOptions.LoadFromReg;}

 // Log ('check integrity...');
 // dmProject_Tools.RestoreChildCount();
  // create dirs

{  try
   // FileCtrl.
    ForceDirectories (dmMain.Dirs.ProjectCalcDir);
  except
    ErrorDlg ('���������� ������� �������: '+ dmMain.Dirs.ProjectCalcDir);
    Exit;
  end;
}
  CursorHourGlass;

 // dmAct_Map_Engine.ChangeProject;


    dmMapEngine_store.CloseAllData;


//  _PostMessage(WE_PROJECT_CHANGED);


////  PostMessage(Application.Handle, WE_PROJECT_CLOSED, 0, 0);

 // SendMessage(Application.Handle, WM_USER, WE_PROJECT_CHANGED_, 0);
//  Application.ProcessMessages;


 ///// PostMessage(Application.Handle, WM_USER, WE_PROJECT_CHANGED_, 0);




 // SendMessage(Application.Handle, WE_PROJECT_CHANGED_, 0, 0);
  g_EventManager.postEvent_ (et_PROJECT_CHANGED, []);

//  PostMessage(Application.Handle, WE_PROJECT_CHANGED_, 0, 0);


 // PostMessage(Application.Handle, WM_USER, WE_PROJECT_CHANGED, 0);


//  PostEvent (WE_PROJECT_CHANGED_);

  g_ShellEvents.PostEvent(WE__EXPLORER_ReLoad_Project);

  CursorDefault;

end;

//-------------------------------------------------------------------
procedure TdmAct_Project.LoadLastProject;
//-------------------------------------------------------------------
var iProjectID: integer;
 // s: string;
begin
 // s:=ClassName;
 Assert(Assigned(dmAct_Project));


  iProjectID:=reg_ReadInteger(FRegPath, FLD_PROJECT_ID, 0);

//  iProjectID:=gl_Reg.RegIni.ReadInteger (ClassName, FLD_PROJECT_ID, 0);
  Dlg_Open (iProjectID);

//  dmUser.ClearCashedObjects();
end;

//-------------------------------------------------------------------
function  TdmAct_Project.GetLastProjectID: integer;
//-------------------------------------------------------------------
begin
//  Result:=gl_Reg.RegIni.ReadInteger (ClassName, FLD_PROJECT_ID, 0);
  Result:=reg_ReadInteger(FRegPath, FLD_PROJECT_ID, 0);
//  Result:=gl_Reg.RegIni.ReadInteger (ClassName, FLD_PROJECT_ID, 0);
end;




//--------------------------------------------------------------------
procedure TdmAct_Project.GetPopupMenu;
//--------------------------------------------------------------------
begin
 // CheckActionsEnable();

  case aMenuType of
    mtFolder: begin
                AddFolderMenu_Create (aPopupMenu);
              //  AddMenuItem (aPopupMenu, act_Add);
                AddFolderPopupMenu (aPopupMenu);

                AddMenuItem (aPopupMenu, nil);
             //   AddMenuItem (aPopupMenu, act_Clear_all);
             end;

    mtList: begin
      AddMenuItem (aPopupMenu, act_Del_List);
    end;

    mtItem: begin
    //          AddMenuItem (aPopupMenu, act_LocalDir);
    ///          AddMenuItem (aPopupMenu, act_NetDir);
           //   AddMenuItem (aPopupMenu, act_Service_Map_Rebuild);

//              AddMenuItem (aPopupMenu, nil);
 //             AddMenuItem (aPopupMenu, act_ReloadFromDir);
       //       AddMenuItem (aPopupMenu, act_Export);
              AddMenuItem (aPopupMenu, act_CLear);

              AddMenuItem (aPopupMenu, nil);
           //   AddMenuItem (aPopupMenu, act_Clear);
              AddMenuItem (aPopupMenu, act_Del_);
              AddMenuItem (aPopupMenu, act_ShowInExplorer);

              AddMenuItem (aPopupMenu, act_Audit);


            end;
//      mtList: begin
//        AddMenuItem (aPopupMenu, act_MapFile_Del);
//      end;
  end;
end;


begin


end.



