inherited dlg_Project_add: Tdlg_Project_add
  Left = 708
  Top = 274
  Width = 500
  Height = 499
  Caption = 'dlg_Project_add'
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  inherited pn_Buttons: TPanel
    Top = 425
    Width = 484
    inherited Bevel1: TBevel
      Width = 484
    end
    inherited Panel3: TPanel
      Left = 292
      inherited btn_Ok: TButton
        Left = 329
      end
      inherited btn_Cancel: TButton
        Left = 413
      end
    end
  end
  inherited pn_Top_: TPanel
    Width = 484
    inherited Bevel2: TBevel
      Width = 484
    end
    inherited pn_Header: TPanel
      Width = 484
      Height = 55
    end
  end
  inherited Panel1: TPanel
    Width = 484
    inherited ed_Name_: TEdit
      Width = 480
    end
  end
  inherited Panel2: TPanel
    Width = 484
    Height = 244
    inherited PageControl1: TPageControl
      Width = 474
      Height = 234
      inherited TabSheet1: TTabSheet
        object cxVerticalGrid1: TcxVerticalGrid
          Left = 0
          Top = 0
          Width = 466
          Height = 203
          BorderStyle = cxcbsNone
          Align = alClient
          LookAndFeel.Kind = lfFlat
          OptionsView.CellTextMaxLineCount = 3
          OptionsView.AutoScaleBands = False
          OptionsView.PaintStyle = psDelphi
          OptionsView.GridLineColor = clBtnShadow
          OptionsView.RowHeaderMinWidth = 30
          OptionsView.RowHeaderWidth = 176
          OptionsView.ValueWidth = 92
          TabOrder = 0
          Version = 1
          object row_Map_Dir1: TcxEditorRow
            Expanded = False
            Properties.Caption = #1055#1072#1087#1082#1072': '#1082#1072#1088#1090#1099' MapInfo 1'
            Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
            Properties.EditProperties.Alignment.Horz = taLeftJustify
            Properties.EditProperties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            Properties.EditProperties.OnButtonClick = row_Map_Dir1_EditPropertiesButtonClick
            Properties.DataBinding.ValueType = 'String'
            Properties.Value = Null
            ID = 0
            ParentID = -1
            Index = 0
            Version = 1
          end
          object row_Map_Dir2: TcxEditorRow
            Expanded = False
            Properties.Caption = #1055#1072#1087#1082#1072': '#1082#1072#1088#1090#1099' MapInfo 2'
            Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
            Properties.EditProperties.Alignment.Horz = taLeftJustify
            Properties.EditProperties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            Properties.EditProperties.OnButtonClick = row_Map_Dir1_EditPropertiesButtonClick
            Properties.DataBinding.ValueType = 'String'
            Properties.Value = Null
            ID = 1
            ParentID = -1
            Index = 1
            Version = 1
          end
          object row_Map_Dir3: TcxEditorRow
            Expanded = False
            Properties.Caption = #1055#1072#1087#1082#1072': '#1082#1072#1088#1090#1099' MapInfo 3'
            Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
            Properties.EditProperties.Alignment.Horz = taLeftJustify
            Properties.EditProperties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            Properties.EditProperties.OnButtonClick = row_Map_Dir1_EditPropertiesButtonClick
            Properties.DataBinding.ValueType = 'String'
            Properties.Value = Null
            ID = 2
            ParentID = -1
            Index = 2
            Version = 1
          end
          object cxVerticalGrid1CategoryRow1: TcxCategoryRow
            Expanded = False
            ID = 3
            ParentID = -1
            Index = 3
            Version = 1
          end
          object row_RelMatrix_Dir1: TcxEditorRow
            Expanded = False
            Properties.Caption = #1055#1072#1087#1082#1072': '#1084#1072#1090#1088#1080#1094#1099' '#1074#1099#1089#1086#1090' 1'
            Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
            Properties.EditProperties.Alignment.Horz = taLeftJustify
            Properties.EditProperties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            Properties.EditProperties.OnButtonClick = row_Map_Dir1_EditPropertiesButtonClick
            Properties.DataBinding.ValueType = 'String'
            Properties.Value = Null
            ID = 4
            ParentID = -1
            Index = 4
            Version = 1
          end
          object row_RelMatrix_Dir2: TcxEditorRow
            Expanded = False
            Properties.Caption = #1055#1072#1087#1082#1072': '#1084#1072#1090#1088#1080#1094#1099' '#1074#1099#1089#1086#1090' 2'
            Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
            Properties.EditProperties.Alignment.Horz = taLeftJustify
            Properties.EditProperties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            Properties.EditProperties.OnButtonClick = row_Map_Dir1_EditPropertiesButtonClick
            Properties.DataBinding.ValueType = 'String'
            Properties.Value = Null
            ID = 5
            ParentID = -1
            Index = 5
            Version = 1
          end
          object row_RelMatrix_Dir3: TcxEditorRow
            Expanded = False
            Properties.Caption = #1055#1072#1087#1082#1072': '#1084#1072#1090#1088#1080#1094#1099' '#1074#1099#1089#1086#1090' 3'
            Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
            Properties.EditProperties.Alignment.Horz = taLeftJustify
            Properties.EditProperties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            Properties.EditProperties.OnButtonClick = row_Map_Dir1_EditPropertiesButtonClick
            Properties.DataBinding.ValueType = 'String'
            Properties.Value = Null
            ID = 6
            ParentID = -1
            Index = 6
            Version = 1
          end
          object cxVerticalGrid1CategoryRow2: TcxCategoryRow
            ID = 7
            ParentID = -1
            Index = 7
            Version = 1
          end
          object col_project_Folder: TcxEditorRow
            Properties.Caption = #1055#1072#1087#1082#1072' '#1076#1083#1103' '#1093#1088#1072#1085#1077#1085#1080#1103' '#1076#1072#1085#1085#1099#1093
            Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
            Properties.EditProperties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            Properties.EditProperties.OnButtonClick = row_Map_Dir1_EditPropertiesButtonClick
            Properties.DataBinding.ValueType = 'String'
            Properties.Value = Null
            ID = 8
            ParentID = -1
            Index = 8
            Version = 1
          end
        end
      end
    end
  end
  inherited ActionList1: TActionList
    Left = 448
    Top = 6
  end
end
