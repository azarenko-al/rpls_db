unit d_Project_Browse;

{ TODO : main menu - copy to popup }

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,

  d_Wizard,
  dm_Main,

  I_DBExplorerForm,

  fr_Explorer_Container,

  u_Explorer,

  u_func,
  u_db,
  u_dlg,

  u_types,

  u_const,
  u_const_str,
  u_const_db,


  dm_Project,

  cxPropertiesStore,     rxPlacemnt, ActnList, StdCtrls,
  ExtCtrls, cxLookAndFeels
  ;

type
  Tdlg_Project_Browse1 = class(Tdlg_Wizard)
    pn_Main: TPanel;
    pn_Projects: TPanel;
    pn_Info: TPanel;

    procedure FormDestroy(Sender: TObject);
    procedure ActionList1Update(Action: TBasicAction; var Handled: Boolean);
    procedure FormCreate(Sender: TObject);
//    procedure act_OkExecute(Sender: TObject);
//    procedure FormDestroy(Sender: TObject);

  private

    FProjectID  : integer;
    FProjectName  : string;
//    FProjectCalcDir : string;

    FFocusedPIDL: TrpPIDL;

    Fframe_Projects: Tframe_Explorer_Container;

    procedure DoOnDblClick (aPIDL: TrpPIDL);

    procedure DoOnNodeFocused (aPIDL: TrpPIDL);
    procedure SelectProject (aProjectID: integer; aName: string);

  public
    class function ExecDlg (): integer;
  end;


implementation {$R *.dfm}


//--------------------------------------------------------------------
class function Tdlg_Project_Browse1.ExecDlg (): integer;
//--------------------------------------------------------------------
begin
  with Tdlg_Project_browse1.Create(Application) do
  try
    ShowModal;

    if ModalResult=mrOk then
      Result:=FProjectID
    else
      Result:=0;

  finally
    Free;
  end;
end;

//-----------------------------------------------------
procedure Tdlg_Project_Browse1.FormCreate(Sender: TObject);
//-----------------------------------------------------
begin
  inherited;

  Caption:=DLG_CAPTION_PROJECT_SELECT;

  pn_Main.Align:=alClient;
  pn_Projects.Align:=alClient;

  CreateChildForm(Tframe_Explorer_Container, Fframe_Projects, pn_Projects);
  Fframe_Projects.Style := esObject;
  Fframe_Projects.SetViewObjects([otProject]);
  Fframe_Projects.Load;

  Fframe_Projects.OnNodeFocused:=DoOnNodeFocused;
  Fframe_Projects.OnItemDblClick:=DoOnDblClick;

 //////// dx_Folders.Items[0].Values[1]:= dmMain.GetCalcDir (FProjectID);

//////////  dx_Folders.Items[1].Values[1]:='';
end;


procedure Tdlg_Project_Browse1.FormDestroy(Sender: TObject);
begin
  FreeAndNil(Fframe_Projects);
end;


procedure Tdlg_Project_Browse1.DoOnNodeFocused (aPIDL: TrpPIDL);
begin
  FFocusedPIDL:=aPIDL;
  if aPIDL.IsFolder then SelectProject(0, aPIDL.Name)
                    else SelectProject(aPIDL.ID, aPIDL.Name);
end;

//--------------------------------------------------------
procedure Tdlg_Project_Browse1.SelectProject (aProjectID: integer; aName: string);
//--------------------------------------------------------
begin
  FProjectID:=aProjectID;

  if aProjectID > 0 then
  begin
    FProjectName:=aName;

//    FProjectCalcDir:= dmMain.GetCalcDir (FProjectID);
//    FProjectNetDir  :=dmProject.GetSharedDir (FProjectID);
  end else begin
  //  FProjectCalcDir:='';
//    FProjectNetDir  :='';
  end;

 //// dx_Folders.Items[0].Values[1]:=FProjectCalcDir;

//  dx_Folders.Items[1].Values[1]:=FProjectNetDir;
end;

//-----------------------------------------------------
procedure Tdlg_Project_Browse1.ActionList1Update(Action: TBasicAction; var Handled: Boolean);
//-----------------------------------------------------
begin
  act_Ok.Enabled:=(FProjectID > 0);
end;


procedure Tdlg_Project_Browse1.DoOnDblClick(aPIDL: TrpPIDL);
begin
  if not aPIDL.IsFolder then
    ModalResult:=mrOk;
//    btn_Ok.Click;
end;


end.
