inherited frame_Project_view: Tframe_Project_view
  Left = 736
  Top = 244
  Width = 548
  Height = 534
  Caption = 'frame_Project_view'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited Panel1: TPanel
    Width = 532
    Height = 57
  end
  inherited pn_Caption: TPanel
    Top = 84
    Width = 532
  end
  inherited pn_Main: TPanel
    Top = 101
    Width = 532
    Height = 311
    object pn_Inspector: TPanel
      Left = 1
      Top = 1
      Width = 530
      Height = 88
      Align = alTop
      BevelOuter = bvLowered
      Caption = 'pn_Inspector'
      TabOrder = 0
    end
    object cxSplitter1: TcxSplitter
      Left = 1
      Top = 136
      Width = 530
      Height = 8
      HotZoneClassName = 'TcxSimpleStyle'
      AlignSplitter = salBottom
      Control = PageControl1
    end
    object PageControl1: TPageControl
      Left = 1
      Top = 144
      Width = 530
      Height = 166
      ActivePage = ts_Map1
      Align = alBottom
      Style = tsFlatButtons
      TabOrder = 2
      object ts_Map1: TTabSheet
        Caption = #1050#1072#1088#1090#1099
      end
      object ts_Matrix1: TTabSheet
        Caption = #1052#1072#1090#1088#1080#1094#1099' '#1074#1099#1089#1086#1090
        ImageIndex = 1
      end
      object TabSheet3: TTabSheet
        Caption = #1055#1091#1090#1080
        ImageIndex = 2
        object cxVerticalGrid1: TcxVerticalGrid
          Left = 0
          Top = 0
          Width = 530
          Height = 57
          Align = alTop
          LookAndFeel.Kind = lfStandard
          OptionsView.CellTextMaxLineCount = 3
          OptionsView.AutoScaleBands = False
          OptionsView.PaintStyle = psDelphi
          OptionsView.GridLineColor = clBtnShadow
          OptionsView.RowHeaderMinWidth = 30
          OptionsView.RowHeaderWidth = 165
          OptionsView.ValueWidth = 92
          TabOrder = 0
          Version = 1
          object row_CalcFolder: TcxEditorRow
            Expanded = False
            Properties.Caption = #1055#1072#1087#1082#1072' '#1089' '#1088#1072#1089#1095#1105#1090#1072#1084#1080
            Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
            Properties.EditProperties.Alignment.Horz = taLeftJustify
            Properties.EditProperties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            Properties.EditProperties.OnButtonClick = row_CalcFolder1EditPropertiesButtonClick
            Properties.DataBinding.ValueType = 'String'
            Properties.Value = Null
            ID = 0
            ParentID = -1
            Index = 0
            Version = 1
          end
        end
      end
    end
  end
  inherited MainActionList: TActionList
    Left = 8
    Top = 0
  end
  inherited ImageList1: TImageList
    Left = 40
    Top = 0
  end
  inherited PopupMenu1: TPopupMenu
    Left = 164
    Top = 4
  end
  inherited cxPropertiesStore: TcxPropertiesStore
    Left = 196
    Top = 4
  end
  inherited cxLookAndFeelController1: TcxLookAndFeelController
    Left = 176
    Top = 440
  end
  inherited dxBarManager1: TdxBarManager
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    Top = 432
    DockControlHeights = (
      0
      0
      27
      0)
  end
  object FormStorage1: TFormStorage
    IniFileName = 'Software\Onega\'
    StoredValues = <>
    Left = 276
    Top = 4
  end
end
