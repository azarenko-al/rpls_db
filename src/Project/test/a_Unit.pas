unit a_Unit;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  u_const,
//  u_const_msg,

  I_Options,

  d_Project_Clear,

  dm_Act_RelFile,

  dm_Act_Map_Engine,

  u_types,
  u_func,
  u_db,
  u_dlg,
  u_reg,

//  u_func_msg,


  dm_Main,
  dm_Main,

  d_Project_add,

  dm_Act_Explorer,
  dm_Act_Project,

  fr_Explorer_Container,
  f_Log,

  fr_Project_view,
  u_const_db,

  StdCtrls, ComCtrls, rxPlacemnt;

type
  TForm12228908 = class(TForm)
    Select: TButton;
    Add: TButton;
    View: TButton;
    MapListView: TButton;
    RelView: TButton;
    PageControl1: TPageControl;
    ts_Project: TTabSheet;
    FormStorage1: TFormStorage;
    TabSheet1: TTabSheet;
    procedure SelectClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    id: integer;

    Fframe_Project : Tframe_Explorer_Container;
    Fframe_Expl    : Tframe_Explorer_Container;
  public
    { Public declarations }
  end;

var
  Form12228908: TForm12228908;

  
//==================================================================
implementation {$R *.DFM}
//==================================================================


var
  id: integer;


procedure TForm12228908.SelectClick(Sender: TObject);
begin
 // dmAct_Project.Browse;
end;


procedure TForm12228908.FormCreate(Sender: TObject);
var
  i: Integer;
begin
 // gl_Reg:=TRegStore_Create(REGISTRY_ROOT);

  Tfrm_Log.CreateForm;

 // Init_EventManager();

  IOptions_Init;

  TdmMain.Init;

  dmAct_Map_Engine.DebugMode:=True;

  TdmAct_Project.Init;

  TdmAct_Explorer.Init;
//  IShellFactory_Init;// (dmMain);

  dmMain.OpenDlg;


///////////////////  Tdlg_Project_add.ExecDlg(0);



 //////////////
  dmAct_Project.LoadLastProject;

  TdmAct_RelFile.Init;


 // Tdlg_Project_Clear.ExecDlg;

  i:=gl_DB.GetMaxID1(TBL_Project);



  with Tframe_Project_View.Create(Self) do
  begin
    View(i,'');
    ShowModal;
  end;



{
//  if not dmMain.LoginDlg then
 //   Exit;// Open;

  CreateChildForm (Tframe_Explorer_Container,  Fframe_Project, ts_Project);
//  Fframe_Project := Tframe_Explorer_Container.CreateChildForm(ts_Project);
  Fframe_Project.SetViewObjects([otProject ]);
  Fframe_Project.RegPath:='_Project';
  Fframe_Project.Load;

  CreateChildForm(Tframe_Explorer_Container, Fframe_Expl, TabSheet1);
//  Fframe_Expl := Tframe_Explorer_Container.CreateChildForm(TabSheet1);
  Fframe_Expl.SetViewObjects([otRelMatrixFile ]);
//  Fframe_Expl.Style:= evsReport;

  Fframe_Expl.RegPath:='_Project';
//  Fframe_Project.Load;

}


//  dmMain.Test;
//  PostEvent (WE_PROJECT_CHANGED);

end;


procedure TForm12228908.FormDestroy(Sender: TObject);
begin
//  Fframe_Project.Free;
//  Fframe_Expl.Free;

end;


end.
