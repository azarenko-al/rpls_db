object Form12228908: TForm12228908
  Left = 317
  Top = 189
  Width = 556
  Height = 401
  Caption = 'Form12228908'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object Select: TButton
    Left = 8
    Top = 8
    Width = 75
    Height = 25
    Caption = 'select'
    TabOrder = 0
    OnClick = SelectClick
  end
  object Add: TButton
    Left = 8
    Top = 40
    Width = 75
    Height = 25
    Caption = 'add'
    TabOrder = 1
  end
  object View: TButton
    Left = 8
    Top = 72
    Width = 75
    Height = 25
    Caption = 'View'
    TabOrder = 2
  end
  object MapListView: TButton
    Left = 12
    Top = 128
    Width = 75
    Height = 25
    Caption = 'MapListView'
    TabOrder = 3
  end
  object RelView: TButton
    Left = 12
    Top = 164
    Width = 75
    Height = 25
    Caption = 'RelView'
    TabOrder = 4
  end
  object PageControl1: TPageControl
    Left = 136
    Top = 0
    Width = 412
    Height = 374
    ActivePage = TabSheet1
    Align = alRight
    Anchors = [akLeft, akTop, akRight, akBottom]
    TabOrder = 5
    object ts_Project: TTabSheet
      Caption = 'ts_Project'
    end
    object TabSheet1: TTabSheet
      Caption = 'TabSheet1'
      ImageIndex = 1
    end
  end
  object FormStorage1: TFormStorage
    IniFileName = 'Software\Onega\'
    StoredValues = <>
    Left = 24
    Top = 236
  end
end
