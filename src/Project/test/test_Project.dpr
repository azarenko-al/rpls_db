program test_Project;

uses
  a_Unit1221 in 'a_Unit1221.pas' {Form12228908},
  d_Project_add in '..\d_Project_add.pas' {dlg_Project_add},
  d_Project_Browse in '..\d_Project_Browse.pas' {dlg_Project_Browse},
  d_Project_Clear in '..\d_Project_Clear.pas' {dlg_Project_Clear},
  d_Project_Import in '..\d_Project_Import.pas' {dlg_Project_Import},
  dm_act_Base in '..\..\Common\dm_act_Base.pas' {dmAct_Base: TDataModule},
  dm_act_Explorer in '..\..\Explorer\dm_act_Explorer.pas' {dmAct_Explorer: TDataModule},
  dm_Act_MapFile in '..\..\MapFile\dm_Act_MapFile.pas' {dmAct_MapFile: TDataModule},
  dm_Act_RelFile in '..\..\RelMatrixFile\dm_Act_RelFile.pas' {dmAct_RelFile: TDataModule},
  dm_Folder in '..\..\Folder\dm_Folder.pas' {dmFolder: TDataModule},
  dm_MapFile in '..\..\MapFile\dm_MapFile.pas' {dmMapFile: TDataModule},
  dm_Object_base in '..\..\Common\dm_Object_base.pas' {dmObject_base: TDataModule},
  dm_Project in '..\dm_Project.pas' {dmProject: TDataModule},
  dm_Project_tools in '..\dm_Project_tools.pas' {dmProject_tools: TDataModule},
  dm_Property in '..\..\Property\dm_Property.pas' {dmProperty: TDataModule},
  dm_RelMatrixFile in '..\..\RelMatrixFile\dm_RelMatrixFile.pas' {dmRelMatrixFile: TDataModule},
  Forms,
  fr_Project_RelMatrix in '..\..\RelMatrixFile\fr_Project_RelMatrix.pas' {frame_Project_RelMatrix},
  fr_Project_view in '..\fr_Project_view.pas' {frame_Project_view},
  dm_Act_Project in '..\dm_Act_Project.pas' {dmAct_Project: TDataModule};

{$R *.RES}
 


begin
  Application.Initialize;
  Application.CreateForm(TForm12228908, Form12228908);
  Application.CreateForm(TdmAct_Project, dmAct_Project);
  Application.Run;
end.
