unit d_Project_add;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  rxPlacemnt, cxPropertiesStore, cxVGrid, cxControls, cxInplaceContainer,
   ActnList,    StdCtrls, ExtCtrls,  Registry, cxButtonEdit,

  d_Wizard_add_with_Inspector,

  dm_MapFile,
  dm_RelMatrixFile,
  dm_Project,

  u_const_str,

  u_func,
  u_cx_vgrid,

  u_Geo, ComCtrls, cxLookAndFeels, cxGraphics, cxLookAndFeelPainters,
  cxStyles, cxEdit, dxSkinsCore, dxSkinsDefaultPainters
  ;

type
  Tdlg_Project_add = class(Tdlg_Wizard_add_with_Inspector)
    cxVerticalGrid1: TcxVerticalGrid;
    row_Map_Dir1: TcxEditorRow;
    row_Map_Dir2: TcxEditorRow;
    row_Map_Dir3: TcxEditorRow;
    cxVerticalGrid1CategoryRow1: TcxCategoryRow;
    row_RelMatrix_Dir1: TcxEditorRow;
    row_RelMatrix_Dir2: TcxEditorRow;
    row_RelMatrix_Dir3: TcxEditorRow;
    cxVerticalGrid1CategoryRow2: TcxCategoryRow;
    col_project_Folder: TcxEditorRow;
    procedure FormCreate(Sender: TObject);
    procedure act_OkExecute(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
//    procedure row_Shared_DirButtonClick(Sender: TObject; AbsoluteIndex: Integer);
    procedure row_Map_Dir1_EditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);

  private
    FRec: TdmProjectAddRec;   
    FID: integer;
 //   FLastMapsFileDir, FLastMatrixFileDir: string;

//    Fframe_RelMatrix : Tframe_FileList;
 //   Fframe_Maps      : Tframe_FileList;

    procedure Append;
  public
    class function ExecDlg: integer;
  end;


//====================================================================
implementation {$R *.DFM}
//====================================================================

//--------------------------------------------------------------------
class function Tdlg_Project_add.ExecDlg: integer;
//--------------------------------------------------------------------
begin
  with Tdlg_Project_Add.Create(Application) do
  begin
  //  FRec.FolderID:=aFolderID;
    ed_Name_.Text:=dmProject.GetNewName();

    ShowModal;
    Result:=FID;

    Free;
  end;
end;

//--------------------------------------------------------------------
procedure Tdlg_Project_add.FormCreate(Sender: TObject);
//--------------------------------------------------------------------
begin
  inherited;

  SetActionName(STR_DLG_ADD_PROJECT);

  cx_InitVerticalGrid (cxVerticalGrid1);


 // ts_Maps.Caption       := STR_MAPS;
 // ts_Rel_Matrix.Caption := STR_REL_MATRIX;

{  row_Shared_Dir.Caption:= STR_PROJECT_SHARED_DIR;
  row_Local_Dir.Caption := STR_PROJECT_LOCAL_DIR;
}

 with TRegIniFile.Create(FRegPath) do
 // with gl_Reg do
  begin
 //   CurrentRegPath := FRegPath;

//    row_Shared_Dir.Text:= ReadString (Name, row_Shared_Dir.Name, '');
 //   row_Local_Dir.Text := ReadString (Name, row_Local_Dir.Name, '');

    row_Map_Dir1.Properties.Value       := ReadString ('', row_Map_Dir1.Name, 'C:\Program Files\ONEGA\RPLS_DB\MAPS');
    row_Map_Dir2.Properties.Value       := ReadString ('', row_Map_Dir2.Name, '');
    row_Map_Dir3.Properties.Value       := ReadString ('', row_Map_Dir3.Name, '');

    row_RelMatrix_Dir1.Properties.Value := ReadString ('', row_RelMatrix_Dir1.Name, '');
    row_RelMatrix_Dir2.Properties.Value := ReadString ('', row_RelMatrix_Dir2.Name, '');
    row_RelMatrix_Dir3.Properties.Value := ReadString ('', row_RelMatrix_Dir3.Name, '');

    Free;
//    FLastMapsFileDir   := ReadString ('', 'LastMapsFileDir', '');
  //  FLastMatrixFileDir := ReadString ('', 'LastMatrixFileDir', '');
  end;



{  Fframe_Maps := Tframe_FileList.CreateChildForm (ts_Maps);
  Fframe_Maps.Filter:= FILTER_MIF;
  Fframe_Maps.OpenDialog1.InitialDir:=FLastMapsFileDir;

  Fframe_RelMatrix := Tframe_FileList.CreateChildForm( ts_Rel_Matrix);
  Fframe_RelMatrix.Filter := FILTER_RLF_MATRIX;
  Fframe_RelMatrix.OpenDialog1.InitialDir:=FLastMatrixFileDir;
}


 // GSPages1.ActivePageIndex:=0;


  AddComponentProp(row_Map_Dir1,       DEF_PropertiesValue);
  AddComponentProp(row_Map_Dir2,       DEF_PropertiesValue);
  AddComponentProp(row_Map_Dir3,       DEF_PropertiesValue);

  AddComponentProp(row_RelMatrix_Dir1, DEF_PropertiesValue);
  AddComponentProp(row_RelMatrix_Dir2, DEF_PropertiesValue);
  AddComponentProp(row_RelMatrix_Dir3, DEF_PropertiesValue);

  cx_InitVerticalGrid (cxVerticalGrid1);


{
  AddComponentProp(row_Map_Dir1,'Text');
  AddComponentProp(row_Map_Dir2,'Text');
  AddComponentProp(row_Map_Dir3,'Text');

  AddComponentProp(row_RelMatrix_Dir1,'Text');
  AddComponentProp(row_RelMatrix_Dir2,'Text');
  AddComponentProp(row_RelMatrix_Dir3,'Text');

}
  cxVerticalGrid1.Align:=alClient;


{
  btn_Ok.Action:=act_Ok;
  act_Ok.OnExecute:=act_OkExecute;
  }


//  row_Shared_Dir.Free;
 // row_Local_Dir.Free;

end;

//--------------------------------------------------------------------
procedure Tdlg_Project_add.FormDestroy(Sender: TObject);
//--------------------------------------------------------------------
begin
  with TRegIniFile.Create(FRegPath) do
  begin
  // CurrentRegPath := FRegPath;

    //WriteString (Name, row_Shared_Dir.Name, row_Shared_Dir.Text);
  //  WriteString (Name, row_Local_Dir.Name, row_Local_Dir.Text);

    WriteString ('', row_Map_Dir1.Name, AsString(row_Map_Dir1.Properties.Value));
    WriteString ('', row_Map_Dir2.Name, AsString(row_Map_Dir2.Properties.Value));
    WriteString ('', row_Map_Dir3.Name, AsString(row_Map_Dir3.Properties.Value));

    WriteString ('', row_RelMatrix_Dir1.Name, AsString(row_RelMatrix_Dir1.Properties.Value));
    WriteString ('', row_RelMatrix_Dir2.Name, AsString(row_RelMatrix_Dir2.Properties.Value));
    WriteString ('', row_RelMatrix_Dir3.Name, AsString(row_RelMatrix_Dir3.Properties.Value));

    Free;
  //  WriteString ('', 'LastMapsFileDir',   FLastMapsFileDir);
  //  WriteString ('', 'LastMatrixFileDir', FLastMatrixFileDir);
  end;

 // Fframe_Maps.Free;
//  Fframe_RelMatrix.Free;

  inherited;
end;

//--------------------------------------------------------------------
procedure Tdlg_Project_add.Append;
//--------------------------------------------------------------------
begin
  Frec.Name:=ed_Name_.Text;

  FRec.MapFileDir1:= AsString(row_Map_Dir1.Properties.Value);
  FRec.MapFileDir2:= AsString(row_Map_Dir2.Properties.Value);
  FRec.MapFileDir3:= AsString(row_Map_Dir3.Properties.Value);

  FRec.RelMatrixDir1:= AsString(row_RelMatrix_Dir1.Properties.Value);
  FRec.RelMatrixDir2:= AsString(row_RelMatrix_Dir2.Properties.Value);
  FRec.RelMatrixDir3:= AsString(row_RelMatrix_Dir3.Properties.Value);


  FID:=dmProject.Add (FRec);

end;


procedure Tdlg_Project_add.act_OkExecute(Sender: TObject);
begin
  inherited;
  Append;
end;



procedure Tdlg_Project_add.row_Map_Dir1_EditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  cx_BrowseFileFolder(Self, Sender as TcxButtonEdit);

end;

end.