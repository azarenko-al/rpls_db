unit d_Project_Browse1;

interface

uses

  u_storage,

  u_vars,

  u_const,
  u_const_str,
  u_const_db,

  u_cx,

  dm_Onega_DB_data,

  u_Func_arrays,
  u_func,
  u_db,

  dm_Main,


  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxGridLevel, cxClasses, cxControls, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  StdCtrls, ExtCtrls, rxPlacemnt, ActnList, DB, ADODB, ToolWin, ComCtrls,
  cxGridBandedTableView, cxGridDBBandedTableView, Menus, cxGraphics,
  cxStyles, cxLookAndFeels, cxLookAndFeelPainters, dxSkinsCore,
  dxSkinsDefaultPainters, dxSkinscxPCPainter, cxCustomData, cxFilter,
  cxData, cxDataStorage, cxEdit, cxDBData, cxCheckBox, cxCalendar;

type
  Tdlg_Project_Browse_new = class(TForm)
    ActionList1: TActionList;
    act_Ok: TAction;
    act_Cancel: TAction;
    FormStorage1: TFormStorage;
    Panel1: TPanel;
    Bevel2: TBevel;
    btn_Ok: TButton;
    Button2: TButton;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    ds_Project: TDataSource;
    ToolBar1: TToolBar;
    cxGrid1DBBandedTableView11: TcxGridDBBandedTableView;
    col_id: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView11name: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView11category: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView11property_count: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView11link_count: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView11last_datetime: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView11DBBandedColumn1: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView11DBBandedColumn2: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView11DBBandedColumn3: TcxGridDBBandedColumn;
    act_Del: TAction;
    PopupMenu1: TPopupMenu;
    actDel1: TMenuItem;
    cxGrid1DBBandedTableView11DBBandedColumn4: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView11DBBandedColumn5: TcxGridDBBandedColumn;
    col_linkline_count: TcxGridDBBandedColumn;
    col_date_created: TcxGridDBBandedColumn;
    col_linkend_antenna_count: TcxGridDBBandedColumn;
    col_linkend_count: TcxGridDBBandedColumn;
    cxStyleRepository1: TcxStyleRepository;
    cxStyle1: TcxStyle;
    cxGrid1DBBandedTableView11DBBandedColumn6: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView11DBBandedColumn7: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView11DBBandedColumn8: TcxGridDBBandedColumn;
    N1: TMenuItem;
    N2: TMenuItem;
    act_Table_align: TAction;
    actTablealign1: TMenuItem;
    ADOStoredProc1: TADOStoredProc;
    col_ReadOnly: TcxGridDBBandedColumn;
    cxStyle_ReadOnly: TcxStyle;
    col_link_Repeater_count: TcxGridDBBandedColumn;
    col_user_created: TcxGridDBBandedColumn;
    act_Refresh: TAction;
    N3: TMenuItem;
    procedure FormDestroy(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure act_OkExecute(Sender: TObject);
    procedure act_RefreshExecute(Sender: TObject);
    procedure act_Table_alignExecute(Sender: TObject);
    procedure cxGrid1DBBandedTableView11ColumnSizeChanged(Sender: TcxGridTableView; AColumn:
        TcxGridColumn);
    procedure cxGrid1DBBandedTableView11DblClick(Sender: TObject);
    procedure cxGrid1DBBandedTableView1StylesGetContentStyle(
      Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
      AItem: TcxCustomGridTableItem; out AStyle: TcxStyle);
  private
    FID  : integer;
    FName: string;


    FRegPath: string;

    FProject_ID : Integer; //current

  public
    class function ExecDlg(aProject_ID: Integer; var aName: string): integer;

  end;


implementation

{$R *.dfm}


// ---------------------------------------------------------------
procedure Tdlg_Project_Browse_new.FormCreate(Sender: TObject);
// ---------------------------------------------------------------
begin
  Caption:=DLG_CAPTION_PROJECT_SELECT;

//  db_SetComponentADOConnection(Self, dmMain.ADOConnection1);
  cxGrid1.Align:=alClient;


  FRegPath:=vars_Form_GetRegPath(ClassName);


  dmOnega_DB_data.PROJECT_SUMMARY(adoStoredProc1);


//  dmOnega_DB_data.PROJECT_SUMMARY(adoStoredProc1);
//          'SELECT * from '+ VIEW_PROJECT_SUMMARY_);

  col_ReadOnly.Visible := False;


  cx_SetColumnCaptions(cxGrid1DBBandedTableView11,
      [
          FLD_Name,           '��������',

          'date_created',     '������',

          'user_created', '�����',

          'last_datetime',    '������',
          'property_count',   '��������',

          'link_count',         '�� ���������',
          'linkfreqplan_count', '�� ���',
          'linkline_count',     '�� �����',
          'linknet_count',      '�� ����',
          'linkend_count',      '��C',

          'link_Repeater_count',  '������',

          'site_count',   'Site',
          'cell_count',   'Cell',

          'pmp_terminal_count',   'PMP ���������',
          'pmp_site_count',       'PMP �������',
          'pmp_calcregion_count', 'PMP ������'

      //    SArr('category',         '���������')

        ]);


//  cxGrid1DBBandedTableView1.RestoreFromRegistry(FRegPath + cxGrid1DBBandedTableView1.Name);

  g_Storage.RestoreFromRegistry(cxGrid1DBBandedTableView11, className);


  cxGrid1DBBandedTableView11.ApplyBestFit();


 // cxGrid1DBBandedTableView1.RestoreFromRegistry(FRegPath + cxGrid1DBBandedTableView1.Name);


end;


procedure Tdlg_Project_Browse_new.FormDestroy(Sender: TObject);
begin
//  cxGrid1DBBandedTableView1.StoreToRegistry(FRegPath + cxGrid1DBBandedTableView1.Name, True,[gsoUseFilter]);

  g_Storage.StoreToRegistry(cxGrid1DBBandedTableView11, className, True);


 // cxGrid1DBBandedTableView1.StoreToRegistry(FRegPath + cxGrid1DBBandedTableView1.Name, True,[gsoUseFilter]);


//REGISTRY_COMMON_FORMS
//   cxGrid1DBBandedTableView1.StoreToRegistry(FRegPath + cxGrid_LnkEndTypeDBBandedTableView_LnkEndType.Name, True,  [gsoUseFilter]);

end;


//--------------------------------------------------------------------
class function Tdlg_Project_Browse_new.ExecDlg(aProject_ID: Integer; var aName: string):
    integer;
//--------------------------------------------------------------------
begin
  with Tdlg_Project_Browse_new.Create(Application) do
  try
    FProject_ID :=  aProject_ID;

    adoStoredProc1.Locate(FLD_ID,  aProject_ID, [] );


    ShowModal;

    if ModalResult=mrOk then
    begin
      Result   := adoStoredProc1[FLD_ID];
      aName := adoStoredProc1[FLD_Name];

    //  Result:=FID;
    //  aName :=FName;
    end
    else
      Result:=0;

  finally
    Free;
  end;
end;


procedure Tdlg_Project_Browse_new.act_OkExecute(Sender: TObject);
begin
 (* if Sender=act_Ok then
  begin
    FID   := adoStoredProc1[FLD_ID];
    FName := adoStoredProc1[FLD_Name];

  end;*)
end;


procedure Tdlg_Project_Browse_new.act_RefreshExecute(Sender: TObject);
begin
  adoStoredProc1.Close;
  adoStoredProc1.Open;
end;



procedure Tdlg_Project_Browse_new.act_Table_alignExecute(Sender: TObject);
begin
  cxGrid1DBBandedTableView11.ApplyBestFit();

end;

procedure Tdlg_Project_Browse_new.cxGrid1DBBandedTableView11ColumnSizeChanged(Sender:  TcxGridTableView; AColumn: TcxGridColumn);
begin
  Sender.Tag :=1;
end;


// ---------------------------------------------------------------
procedure Tdlg_Project_Browse_new.cxGrid1DBBandedTableView11DblClick(Sender: TObject);
// ---------------------------------------------------------------
var
  s: string;
begin
  s:=Sender.ClassName;

 if not cx_CheckCursorInGrid(cxGrid1, cxGrid1DBBandedTableView11) then
   exit;

//function cx_CheckCursorInGrid(Sender: TcxGrid; aGridView: TcxCustomGridView): Boolean;


 // cxGrid1.ScreenToClient()

  btn_Ok.Click;
//  act_Ok.Execute;
end;


// ---------------------------------------------------------------
procedure Tdlg_Project_Browse_new.cxGrid1DBBandedTableView1StylesGetContentStyle(
  Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
  AItem: TcxCustomGridTableItem; out AStyle: TcxStyle);
// ---------------------------------------------------------------
begin
  if ARecord.Values[col_id.Index] = FProject_ID then
    AStyle := cxStyle1;

  if ARecord.Values[col_readOnly.Index] = True then
    AStyle := cxStyle_readOnly;


end;

end.



(*
// ---------------------------------------------------------------
procedure Tframe_Options_AntType.FormCreate(Sender: TObject);
// ---------------------------------------------------------------
begin
  FRegPath:=REGISTRY_ROOT+ClassName+'1';


  cxGrid_Ant.Align:=alClient;

  cxGridDBBandedTableView_AntennaTypes.DataController.DataSource :=dmMDB.ds_update_AntennaTypes;
  DBStatusLabel1.DataSource:=dmMDB.ds_update_AntennaTypes;

  cxGridDBBandedTableView_AntennaTypes.RestoreFromRegistry(FRegPath + cxGridDBBandedTableView_AntennaTypes.Name);


//  db_ViewDataSet(dmMDB.ds_update_AntennaTypes.DataSet);

  Assert(cx_Grid_Check (cxGridDBBandedTableView_AntennaTypes), 'cx_Grid_Check');

end;


procedure Tframe_Options_AntType.FormDestroy(Sender: TObject);
begin
  cxGridDBBandedTableView_AntennaTypes.StoreToRegistry(FRegPath + cxGridDBBandedTableView_AntennaTypes.Name, True,[gsoUseFilter]);

end;
*)