unit dm_Project;

interface

uses
  SysUtils, Classes, DB, ADODB, Forms, Windows,   Registry, Variants,

  dm_Onega_DB_data,

  dm_MapEngine_store,

//  dm_Act_Map,

 // dm_MapEngine,
  dm_Map_Desktop,

//  u_func_msg,
  u_const_msg,


  dm_Main,
  dm_Object_base,

  dm_MapFile,
  dm_RelMatrixFile,

  u_types,

  u_const,
  u_const_str,
  u_const_db,

  u_func_msg,

  u_log,

  u_db,
  u_func,
  u_files,
  u_dlg,
  u_reg
  ;



type
  //------------------------------------------------------------------
  TdmProjectAddRec = packed record
  //------------------------------------------------------------------
    Name      : string;
  //  FolderID  : integer;

  //  NetDir    : string;
  //  LocalDir  : string;

    MapFileDir1,
    MapFileDir2,
    MapFileDir3: string;

    RelMatrixDir1,
    RelMatrixDir2,
    RelMatrixDir3: string;



//    IsSaveFilesToDB: boolean;

 //   MapFileList1    : TStrings;
//    RelMatrixFileList1: TStrings;


    Result : record
                ID   : Integer;
             //   GUID : string;
              end;
  end;


 // TCalcStoreType = (cst_Local, cst_Shared);


  TdmProject = class(TdmObject_base)
    qry_Temp: TADOQuery;
    ADOStoredProc_Add: TADOStoredProc;
    procedure DataModuleCreate(Sender: TObject);
  private
//    procedure UpdateCalcMaps(aProjectID: integer; aFileDir: string);
  public
////////////    ParentFolderID: integer;

    function Add(var aRec: TdmProjectAddRec): integer;

    function  AddByName(aName: string): integer;


    function  Del (aID: integer): boolean;
    procedure Clear  (aID: integer);
// TODO: ClearAllProjects
//  procedure ClearAllProjects;


  end;


function dmProject: TdmProject;


// ==================================================================
implementation    {$R *.DFM}
// ==================================================================

//uses
//  dm_Project_tools;

var
  FdmProject: TdmProject;


// ---------------------------------------------------------------
function dmProject: TdmProject;
// ---------------------------------------------------------------
begin
  if not Assigned(FdmProject) then
    FdmProject := TdmProject.Create(Application);

  Result := FdmProject;
end;


procedure TdmProject.DataModuleCreate(Sender: TObject);
begin
  inherited;
  TableName := TBL_PROJECT;
  ObjectName:=OBJ_PROJECT;
end;


function TdmProject.Del(aID: integer): boolean;
begin
////  Clear (aID);
//  Result:=gl_DB.DeleteRecordByID (TBL_PROJECT, aID);

  Result:=dmOnega_DB_data.Project_Del(aID)>0;

end;


//--------------------------------------------------------------------
function TdmProject.AddByName(aName: string): integer;
//--------------------------------------------------------------------
var
  rec: TdmProjectAddRec;
begin
  FillChar(rec, SizeOf(rec), 0);

  rec.name :=aName;

  result:=Add(rec);
end;


//--------------------------------------------------------------------
function TdmProject.Add(var aRec: TdmProjectAddRec): integer;
//--------------------------------------------------------------------
var
  iOldID: integer;
  sDir: string;

begin
  Assert(arec.name <> '');

 // Result:=-1;

//  ADOStoredProc_Add.Connection :=dmMain.ADOConnection;


  Result:=dmOnega_DB_data.OpenStoredProc(ADOStoredProc_Add,  SP_PROJECT_ADD,
//  dmOnega_DB_data.ExecStoredProc(SP_PROJECT_ADD,
              [FLD_NAME, aRec.Name
            //   db_Par(FLD_FOLDER_ID, IIF_NULL(aRec.FolderID))
              ]);


//ClearDirRecursive(dmMain.Dirs.ProjectCalcDir);

(*
  Result:=db_OpenStoredProc(ADOStoredProc_Add, SP_PROJECT_ADD,
//  dmOnega_DB_data.ExecStoredProc(SP_PROJECT_ADD,
              [db_Par(FLD_NAME, aRec.Name),
               db_Par(FLD_FOLDER_ID, IIF_NULL(aRec.FolderID))
              ]);
*)
//  db_ViewDataSet(ADOStoredProc_Add);

(*  if ADOStoredProc_Add.RecordCount>0 then
    with ADOStoredProc_Add do
    begin
      aRec.Results1.ID   :=FieldValues[FLD_ID];
      aRec.Results1.GUID :=FieldValues[FLD_GUID];
    end;
*)
(*
  Result:=dmOnega_DB_data.ExecStoredProc(SP_PROJECT_ADD,
              [db_Par(FLD_NAME, aRec.Name),
               db_Par(FLD_FOLDER_ID, IIF_NULL(aRec.FolderID))
              ]);
*)


(*  with aRec do
    Result:= gl_DB.AddRecordID(TableName,
                  [db_Par(FLD_NAME, Name),
                   db_Par(FLD_FOLDER_ID, IIF_NULL(FolderID)),

                   db_Par(FLD_FILEDIR_NETWORK, NetDir)   ]
                   );
*)

  if Result<=0 then
  begin
    aRec.Result.ID := ADOStoredProc_Add[FLD_ID];

    Exit;
  end;

  sDir := dmMain.Dirs.Dir;

  g_Log.Add('�������� ������ �� ��������: '+sDir);
//  ClearDirRecursive(sDir);
  DirClear(sDir);


///  Result:=gl_DB.GetIdentCurrent(TableName);

  iOldID:=dmMain.ProjectID;
  dmMain.ProjectID:=Result;

   //  procedure BuildFileList  (aFileMask: string; aStrings: TStrings);

(*
   if Assigned(aRec.MapFileList1) then
    dmMapFile.AddList (aRec.MapFileList1);

  if Assigned(aRec.RelMatrixFileList1) then
    dmRelMatrixFile.AddList (aRec.RelMatrixFileList1, 0);
*)

//  dmMain.SetLocalDir (Result, aRec.LocalDir);


  dmMapFile.AddFromDir (aRec.MapFileDir1);
  dmMapFile.AddFromDir (aRec.MapFileDir2);
  dmMapFile.AddFromDir (aRec.MapFileDir3);


  dmRelMatrixFile.AddFromDir (aRec.RelMatrixDir1);
  dmRelMatrixFile.AddFromDir (aRec.RelMatrixDir2);
  dmRelMatrixFile.AddFromDir (aRec.RelMatrixDir3);

 ///////// dmOnega_DB_data.Relief_Init(Result);

 // end;

 // if aRec.RelMatrixDir <> '' then
 // begin
{ TODO : reduce }
  // -------------------------------------------------------------------

 // end;

 // oStrList.Free;

  dmMain.ProjectID:=iOldID;
end;


//-------------------------------------------------------------------
procedure TdmProject.Clear (aID: integer);
//-------------------------------------------------------------------
begin
//  g_EventManager.PostEvent(WE_MAP_CLOSE, []);

//  dmAct_Map.CLEAR111;

  g_EventManager.PostEvent_(et_MAP_CLEAR, []);
 // PostMessage (Application.Handle, MSG_MAP_CLEAR, 0, 0);

 // dmAct_Map.MAP_SAVE_AND_CLOSE_WORKSPACE;

  if Assigned(dmMap_Desktop) then
    dmMap_Desktop.ADOStoredProc_MapDesktops.Close;

  dmOnega_DB_data.Project_Clear_(aID);

  if Assigned(dmMapEngine_store) then
    dmMapEngine_store.CloseAllData;

////////  dmMapEngine.Rebuild1();


  dmMain.ProjectID:= aID;
 // ClearDirRecursive(dmMain.Dirs.ProjectMapDir);
 // ClearDirRecursive(dmMain.Dirs.ProjectCalcDir);
  DirClear(dmMain.Dirs.Dir);

//  ClearDir(dmMain.Dirs.ProjectCalcDir);


end;



begin

end.





// TODO: ClearAllProjects
////-------------------------------------------------------------------
//procedure TdmProject.ClearAllProjects;
////-------------------------------------------------------------------
//begin
//if ConfirmDlg('������� �� �������� �� ����� �������� ���� ������ �� ���� �������� � ������������.'+
//              '�� ������������� ������ ��������� �������� ��?') then
//begin
//  db_OpenQuery (qry_Temp, 'SELECT id FROM '+ TBL_PROJECT);
//
//  with qry_Temp do
//    while not EOF do
//    begin
//      dmProject.Clear (FieldValues[FLD_ID]);
//      Next;
//    end;
//end;
// // dmProject_tools.ClearAllProjects;
//end;


