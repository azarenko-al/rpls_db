unit dm_Project_tools;


interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,


  dm_Onega_DB_data,

  dm_Main,

  dm_Custom,

  dm_Project,
  dm_GeoRegion,

  u_geo,
  u_geo_convert_new,

  u_func,
  u_db,
  u_const_db,

  Db, ADODB
  ;

type
  TdmProject_tools = class(TdmCustom)
    ADOCommand1: TADOCommand;
    qry_Temp: TADOQuery;
//    procedure DataModuleCreate(Sender: TObject);

  private
    procedure Update_Blank_LatLonStr;

//    procedure ClearAll ();

  public
// TODO: UpdateGeoRegions
//  procedure UpdateGeoRegions();

  // procedure Clear (aID: integer);

  end;

function dmProject_tools: TdmProject_tools;


implementation {$R *.DFM}



var
  FdmProject_tools: TdmProject_tools;


// ---------------------------------------------------------------
function dmProject_tools: TdmProject_tools;
// ---------------------------------------------------------------
begin
  if not Assigned(FdmProject_tools) then
      FdmProject_tools := TdmProject_tools.Create(Application);

   Result := FdmProject_tools;
end;




//-------------------------------------------------------------------
procedure TdmProject_tools.Update_Blank_LatLonStr;
//-------------------------------------------------------------------
const
  SQL_UPDATE ='update property                              '+
              '  set lat_str=dbo.FormatDegree(lat)+'' N'',  '+
              '      lon_str=dbo.FormatDegree(lon)+'' E''   '+
              '  WHERE (lat_str IS NULL) and (project_id=%d)';

var
  bl,bl_WGS: TBLPoint;
begin
  dmOnega_DB_data.OpenQuery (qry_Temp,

//  db_OpenQuery(qry_Temp,
               'SELECT * FROM  Property '+
               'WHERE (lat_wgs IS NULL) and (project_id=:project_id)',
               [db_Par(FLD_PROJECT_ID, dmMain.ProjectID)]);


{  if not qry_Temp.IsEmpty then
    gLog.Msg ();
}

  with qry_Temp do
    while not EOF do
  begin
    bl:=db_ExtractBLPoint(qry_Temp);
    bl_WGS:=geo_BL_to_BL_new (bl, EK_CK_42, EK_WGS84);

    db_UpdateRecord(qry_Temp,
                    [db_Par(FLD_LAT_WGS, bl_WGS.B),
                     db_Par(FLD_LON_WGS, bl_WGS.L)
                    ]);

    Next;
  end;


  dmMain.ADOConnection.Execute(Format(SQL_UPDATE, [dmMain.ProjectID]));

end;


begin
  
end.


// TODO: UpdateGeoRegions
////-------------------------------------------------------------------
//procedure TdmProject_tools.UpdateGeoRegions();
////-------------------------------------------------------------------
//const
//SQL_SEL_EMPTY_PROPERTIES =
//  'SELECT id,lat,lon FROM property WHERE (georegion_id is null) and (project_id=:project_id)';
//var
//blPoint: TBLPoint;
//iGeoRegionID: Integer;
//begin
//try
//  CursorHourGlass;
//
//  db_OpenQuery(qry_Temp, SQL_SEL_EMPTY_PROPERTIES,
//              [db_Par(FLD_PROJECT_ID, dmMain.ProjectID)]);
//
//  with qry_Temp do
//    while not EOF do
//    begin
//      blPoint:= db_ExtractBLPoint(qry_Temp);
//
//
//      iGeoRegionID:= dmGeoRegion.GetID_ByBLPoint(blPoint);
//
//      if iGeoRegionID > 0 then
//        gl_DB.UpdateRecord(TBL_PROPERTY, qry_Temp.FieldByName(FLD_ID).AsInteger,
//                          [db_Par(FLD_GEOREGION_ID, iGeoRegionID)]);
//
//      Next;
//    end;
//
//finally
//  CursorDefault;
//end;
//end;