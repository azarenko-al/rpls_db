inherited dlg_Project_Clear: Tdlg_Project_Clear
  Left = 926
  Top = 231
  Width = 633
  Height = 594
  Anchors = [akTop, akRight]
  Caption = #1055#1088#1086#1077#1082#1090': '#1086#1095#1080#1089#1090#1080#1090#1100' '#1076#1072#1085#1085#1099#1077
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited pn_Buttons: TPanel
    Top = 520
    Width = 617
    inherited Bevel1: TBevel
      Width = 617
    end
    inherited Panel3: TPanel
      Left = 440
      Width = 177
      inherited btn_Ok: TButton
        Left = 7
      end
      inherited btn_Cancel: TButton
        Left = 91
      end
    end
  end
  inherited pn_Top_: TPanel
    Width = 617
    inherited Bevel2: TBevel
      Width = 617
    end
    inherited pn_Header: TPanel
      Width = 617
      Visible = False
      inherited lb_Action: TLabel
        Top = 12
        Width = 169
        AutoSize = False
        Color = clGray
        ParentColor = False
      end
    end
  end
  object cxGrid1: TcxGrid [2]
    Left = 0
    Top = 60
    Width = 617
    Height = 378
    Align = alTop
    Anchors = [akLeft, akTop, akRight, akBottom]
    TabOrder = 2
    LookAndFeel.Kind = lfFlat
    object cxGrid1DBTableView1: TcxGridDBTableView
      NavigatorButtons.ConfirmDelete = False
      DataController.DataSource = DataSource1
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsCustomize.ColumnFiltering = False
      OptionsCustomize.ColumnsQuickCustomization = True
      OptionsData.CancelOnExit = False
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      object col_category: TcxGridDBColumn
        Caption = #1050#1072#1090#1077#1075#1086#1088#1080#1103
        DataBinding.FieldName = 'category'
        Width = 65
      end
      object col_Table: TcxGridDBColumn
        Caption = #1058#1072#1073#1083#1080#1094#1072
        DataBinding.FieldName = 'name'
        Visible = False
        Width = 200
      end
      object col_Count: TcxGridDBColumn
        Caption = #1050#1086#1083'-'#1074#1086
        DataBinding.FieldName = 'count'
      end
      object col_caption: TcxGridDBColumn
        Caption = #1053#1072#1079#1074#1072#1085#1080#1077
        DataBinding.FieldName = 'caption'
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1DBTableView1
    end
  end
  object StatusBar1: TStatusBar [3]
    Left = 0
    Top = 501
    Width = 617
    Height = 19
    Panels = <>
    SimplePanel = True
  end
  object DataSource1: TDataSource
    DataSet = ADOStoredProc1
    Left = 352
    Top = 160
  end
  object ADOStoredProc1: TADOStoredProc
    Parameters = <>
    Left = 352
    Top = 112
  end
  object ADOQuery1: TADOQuery
    Parameters = <>
    Left = 432
    Top = 112
  end
end
