unit dm_Act_Project1;

interface
{$I ver.inc}

 {$DEFINE use_dll__}


uses
  SysUtils, Classes, Forms, 

  dm_Main,

  dm_MapEngine_store,

  u_ini_Google_params,


  u_ini_RPLS_XML_to_DB_params,

  u_DataExport_run,

  I_Shell,
  u_Shell_new,

  u_vars,

  u_reg,

  u_func,
  u_log,


 dm_MapFile,
 dm_RelMatrixFile,


  u_run,

  
  i_Options_,

  u_const,

  u_types
  ;


type
{ TODO -oalex -c : TdmAct_Project1!!!   07.05.2010 3:33:19 }

  TdmAct_Project1 = class(TDataModule)
     procedure DataModuleCreate(Sender: TObject);
  private
     procedure DoRefreshAll();
    procedure ShowErrorLog;


  protected
  public
    procedure ChangeCoordSystDlg;

//    procedure ImportFromSDB;

    procedure ImportFromRplsXmlProject;

    procedure ImportFromRplsDbProject;
    procedure ImportFromRplsDbGuides;
    procedure ImportFromExcel;

    procedure ExportToRplsDbProject;
    procedure ExportToRplsDbGuides;

    procedure Dlg_ExportToGoogle;

    procedure ExecuteOnProjectImportDone;

    procedure ImportKML;

    procedure Import_SXF;

    class procedure Init;
  end;


var
  dmAct_Project1: TdmAct_Project1;

//====================================================================
implementation {$R *.dfm}
//====================================================================

uses
  dm_Act_Map ;

const
  TEMP_FILENAME = 'project_export.ini';


procedure TdmAct_Project1.DataModuleCreate(Sender: TObject);
begin

end;

//--------------------------------------------------------------------
class procedure TdmAct_Project1.Init;
//--------------------------------------------------------------------
begin
  if not Assigned(dmAct_Project1) then
    dmAct_Project1:=TdmAct_Project1.Create(Application);
end;



//--------------------------------------------------------------------
procedure TdmAct_Project1.ChangeCoordSystDlg;
//--------------------------------------------------------------------
//var iCoordSyst: integer;
begin
  IOptions.Dlg_ChangeCoordSys;

{  iCoordSyst:=TDlg_ChangeCoordSystem.ExecDlg();

  if iCoordSyst>0 then
    IOptions.GeoCoord:=iCoordSyst;
}
//  dmOptions.SaveToReg;
end;



//-------------------------------------------------------------------
procedure TdmAct_Project1.Import_SXF;
//-------------------------------------------------------------------
var
  b: Boolean;
  i: Integer;
  sDir_Maps: string;
  sDir_Rel: string;
  sFile : string;
  sPath: string;
begin
  sFile := g_ApplicationDataDir + TEMP_FILENAME;

  SysUtils.DeleteFile(sFile);

  ini_WriteInteger(sFile, 'main', 'project_id', dmMain.ProjectID);

//  if not RunApp (PROG_BEE_TN_IMPORT, TEMP_EXPORT_FILENAME) then

 // ShellExec (PROG_BEE_TN_IMPORT, TEMP_EXPORT_FILENAME);

// GetApplicationDir() +


///////  {$DEFINE use_dll111}



// PROG_SXF_TO_RLF
//  ShellExec (GetApplicationDir()+PROG_SXF_TO_RLF, DoubleQuotedStr(sFile));

  sPath:=GetApplicationDir()+'SXF_to_RLF\' + 'SXF_to_RLF.exe';


  b:=RunApp(sPath, DoubleQuotedStr(sFile) );
//  b:=RunApp('C:\ONEGA\RPLS_DB\bin1\SXF_to_RLF.exe', DoubleQuotedStr(sFile) );


//  ShellExec ('C:\ONEGA\RPLS_DB\bin1\SXF_to_RLF.exe', DoubleQuotedStr(sFile));

//  ShellExec (GetApplicationDir()+PROG_SXF_TO_RLF, DoubleQuotedStr(sFile));


   sDir_Maps :=ini_ReadString(sFile, 'main', 'Dir_Maps', '');
   sDir_Rel  :=ini_ReadString(sFile, 'main', 'Dir_Rel', '');


   if sDir_Maps<>'' then
     dmMapFile.AddFromDir (sDir_Maps);

   if sDir_Rel<>'' then
     dmRelMatrixFile.AddFromDir (sDir_Rel);


    g_Shell.UpdateNodeChildren_ByGUID(GUID_REL_MATRIX);
    g_Shell.UpdateNodeChildren_ByGUID(GUID_MAP);




end;

(*
//-------------------------------------------------------------------
procedure TdmAct_Project1.ImportFromSDB;
//-------------------------------------------------------------------
var
  i: Integer;
  sFile : string;
begin
  sFile := g_ApplicationDataDir + TEMP_FILENAME;

  SysUtils.DeleteFile(sFile);

  ini_WriteInteger(sFile, 'main', 'project_id', dmMain.ProjectID);

//  if not RunApp (PROG_BEE_TN_IMPORT, TEMP_EXPORT_FILENAME) then

 // ShellExec (PROG_BEE_TN_IMPORT, TEMP_EXPORT_FILENAME);

// GetApplicationDir() +


///////  {$DEFINE use_dll111}

  {$IFDEF use_dll}

//  i_bee_sdb_import
    {
  // -----------------------------
  if Load_Ibee_sdb_import() then
  begin
    Ibee_sdb_import.InitADO(dmMain.ADOConnection1.ConnectionObject);
    Ibee_sdb_import.Execute(sFile);

    UnLoad_Ibee_sdb_import();
  end;
     }
  {$ELSE}
  ShellExec (GetApplicationDir()+PROG_BEE_SDB_IMPORT, DoubleQuotedStr(sFile));

  i:=1;

//  ShellExec (GetApplicationDir()+PROG_BEE_TN_IMPORT, DoubleQuotedStr(sFile));
  {$ENDIF}


////////  PostMessage(Application.Handle, MSG_MAP_SAVE_AND_CLOSE_WORKSPACE, 0,0);

end;
*)


//-------------------------------------------------------------------
procedure TdmAct_Project1.ImportFromRplsDbProject;
//-------------------------------------------------------------------
//var
//  sFile : string;
begin
  TDataExport_run.ImportFromRplsDbProject;

end;

//-------------------------------------------------------------------
procedure TdmAct_Project1.ImportFromRplsDbGuides;
//-------------------------------------------------------------------
var
  I: Integer;
begin
  TDataExport_run.ImportFromRplsDbGuides;


  for I := 0 to High(DICT_TYPES) do
    g_Shell.UpdateNodeChildren_ByGUID (g_Obj.ItemByType[DICT_TYPES[i]].RootFolderGUID);

end;

//-------------------------------------------------------------------
procedure TdmAct_Project1.ImportFromRplsXmlProject;
//-------------------------------------------------------------------
var
  sFile : string;

  oParams: TIni_RPLS_XML_to_DB_params;

begin
  sFile := g_ApplicationDataDir + TEMP_FILENAME;

//  DeleteFile(sFile);

  oParams:=TIni_RPLS_XML_to_DB_params.Create;
  oParams.ProjectID := dmMain.ProjectID;

  oParams.SaveToIni(sFile);

  FreeAndNil(oParams);


  ShellExec (GetApplicationDir()+PROG_RPLS_XML_to_DB_exe, DoubleQuotedStr(sFile));

end;


// -------------------------------------------------------------------
// Export
// -------------------------------------------------------------------

//-------------------------------------------------------------------
procedure TdmAct_Project1.ExportToRplsDbProject;
//-------------------------------------------------------------------
begin
  TDataExport_run.ExportToRplsDbProject1;
end;

//-------------------------------------------------------------------
procedure TdmAct_Project1.ExportToRplsDbGuides;
//-------------------------------------------------------------------

begin
  TDataExport_run.ExportToRplsDbGuides;
end;


//-------------------------------------------------------------------
procedure TdmAct_Project1.Dlg_ExportToGoogle;
//-------------------------------------------------------------------
const
  TEMP_FILENAME_GOOGLE = 'projecst_google.ini';

var
  sFile : string;

  oParams: TIni_Google_Params;
begin
  sFile := g_ApplicationDataDir + TEMP_FILENAME_GOOGLE;

  SysUtils.DeleteFile(sFile);


  oParams:=TIni_Google_Params.Create;
  oParams.ProjectID := dmMain.ProjectID;

  oParams.SaveToFile(sFile);
  FreeAndNil(oParams);

//  ini_WriteInteger(sFile, 'main', 'project_id', dmMain.ProjectID);

//
//  if assigned(aIDList) then
////    oParams.
//    aIDList.SaveIDsToIniFile(sFile, 'List');



  {$IFDEF use_dll}

//  i_bee_sdb_import

  // -----------------------------
  if Load_IGoogle() then
  begin
    IGoogle.InitADO(dmMain.ADOConnection1.ConnectionObject);
    IGoogle.Execute(sFile);

    UnLoad_IGoogle();
  end;

  {$ELSE}
  ShellExec (GetApplicationDir()+PROG_Google, DoubleQuotedStr(sFile));
//  ShellExec(GetApplicationDir()+PROG_Google, DoubleQuotedStr(sFile));
  {$ENDIF}


end;



//-------------------------------------------------------------------
procedure TdmAct_Project1.DoRefreshAll();
//-------------------------------------------------------------------
begin


  dmAct_Map.MAP_SAVE_AND_CLOSE_WORKSPACE;

  //dmMapEngine.Rebuild1;

  dmAct_Map.MAP_RESTORE_WORKSPACE;



//  dmAct_Project.act_Service_Map_Rebuild.Execute;

end;



procedure TdmAct_Project1.ExecuteOnProjectImportDone;
begin
 //   ShowMessage('ExecuteOnProjectImportDone');

  DoRefreshAll;

  g_ShellEvents.Shell_ReLoad_Project;

  dmMapEngine_store.Reload_Object_Layers;

//  dmAct_Explorer.Shell_UpdateNodeChildren (GUID_PROJECT);

  //  ShowMessage('..ExecuteOnProjectImportDone');

end;


procedure TdmAct_Project1.ImportFromExcel;
begin
  TDataExport_run.ImportFromExcel();

//  for I := 0 to High(DICT_TYPES) do
 //   g_ShellEvents.Shell_UpdateNodeChildren_ByGUID (g_Obj.ItemByType[DICT_TYPES[i]].RootFolderGUID);

end;


procedure TdmAct_Project1.ImportKML;
begin
  TDataExport_run.ImportKML();

//  for I := 0 to High(DICT_TYPES) do
 //   g_ShellEvents.Shell_UpdateNodeChildren_ByGUID (g_Obj.ItemByType[DICT_TYPES[i]].RootFolderGUID);

end;



//-------------------------------------------------------------------
procedure TdmAct_Project1.ShowErrorLog;
//-------------------------------------------------------------------
begin
  g_Log.ShowFile;
end;


end.


