object frame_Project_RelMatrix1: Tframe_Project_RelMatrix1
  Left = 718
  Top = 442
  Width = 709
  Height = 375
  Caption = 'frame_Project_RelMatrix1'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = True
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object cxGrid1: TcxGrid
    Left = 0
    Top = 157
    Width = 701
    Height = 114
    Align = alTop
    Anchors = [akLeft, akTop, akRight, akBottom]
    PopupMenu = PopupMenu1
    TabOrder = 0
    LookAndFeel.Kind = lfFlat
    object cxGrid1DBTableView11: TcxGridDBTableView
      PopupMenu = PopupMenu1
      NavigatorButtons.ConfirmDelete = False
      DataController.DataSource = ds_Relief
      DataController.KeyFieldNames = 'id'
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsCustomize.ColumnFiltering = False
      OptionsCustomize.ColumnsQuickCustomization = True
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Inserting = False
      OptionsSelection.MultiSelect = True
      OptionsView.GroupByBox = False
      OptionsView.Indicator = True
      Styles.OnGetContentStyle = cxGrid1DBTableView11StylesGetContentStyle
      OnColumnPosChanged = cxGrid1DBTableView11ColumnPosChanged
      OnColumnSizeChanged = cxGrid1DBTableView11ColumnPosChanged
      object col__id: TcxGridDBColumn
        DataBinding.FieldName = 'id'
        Visible = False
        Options.Editing = False
        Width = 41
      end
      object col_checked: TcxGridDBColumn
        Caption = #1042#1082#1083
        DataBinding.FieldName = 'checked'
        PropertiesClassName = 'TcxCheckBoxProperties'
        Properties.ImmediatePost = True
        Properties.OnChange = col_checkedPropertiesChange
        Width = 39
      end
      object col_name: TcxGridDBColumn
        DataBinding.FieldName = 'name'
        PropertiesClassName = 'TcxTextEditProperties'
        Options.Editing = False
        Width = 313
      end
      object col_comment: TcxGridDBColumn
        DataBinding.FieldName = 'comment'
        Options.Editing = False
        Width = 140
      end
      object col_FileSize: TcxGridDBColumn
        Caption = #1056#1072#1079#1084#1077#1088
        DataBinding.FieldName = 'FileSize'
        PropertiesClassName = 'TcxCurrencyEditProperties'
        Properties.DisplayFormat = ',0;-,0'
        Options.Editing = False
        Width = 86
      end
      object col_Exists: TcxGridDBColumn
        Caption = #1053#1072' '#1076#1080#1089#1082#1077
        DataBinding.FieldName = 'EXIST'
        PropertiesClassName = 'TcxCheckBoxProperties'
        Visible = False
      end
      object col_x_min: TcxGridDBColumn
        Caption = 'x min'
        DataBinding.FieldName = 'x_min'
        PropertiesClassName = 'TcxCurrencyEditProperties'
        Properties.DisplayFormat = ',0'
        Visible = False
        Width = 54
      end
      object col_y_min: TcxGridDBColumn
        Caption = 'y min'
        DataBinding.FieldName = 'y_min'
        PropertiesClassName = 'TcxCurrencyEditProperties'
        Properties.DisplayFormat = ',0'
        Visible = False
        Width = 60
      end
      object col_x_max: TcxGridDBColumn
        Caption = 'x max'
        DataBinding.FieldName = 'x_max'
        PropertiesClassName = 'TcxCurrencyEditProperties'
        Properties.DisplayFormat = ',0'
        Visible = False
        Width = 54
      end
      object col_y_max: TcxGridDBColumn
        Caption = 'y max'
        DataBinding.FieldName = 'y_max'
        PropertiesClassName = 'TcxCurrencyEditProperties'
        Properties.DisplayFormat = ',0'
        Visible = False
        Width = 55
      end
      object col_Step_X: TcxGridDBColumn
        Caption = 'step x'
        DataBinding.FieldName = 'step_x'
        Visible = False
        Width = 47
      end
      object col_Step_y: TcxGridDBColumn
        Caption = 'step y'
        DataBinding.FieldName = 'step_y'
        Visible = False
        Width = 50
      end
      object col_B_min: TcxGridDBColumn
        Caption = 'B min'
        DataBinding.FieldName = 'B_min'
        Visible = False
        Width = 32
      end
      object col_L_min: TcxGridDBColumn
        Caption = 'L min'
        DataBinding.FieldName = 'L_min'
        Visible = False
        Width = 31
      end
      object col_B_max: TcxGridDBColumn
        Caption = 'B max'
        DataBinding.FieldName = 'B_max'
        Visible = False
        Width = 35
      end
      object col_L_max: TcxGridDBColumn
        Caption = 'L max'
        DataBinding.FieldName = 'L_max'
        Visible = False
        Width = 34
      end
      object col_FileDate: TcxGridDBColumn
        Caption = #1057#1086#1079#1076#1072#1085' '#1085#1072' '#1076#1080#1089#1082#1077
        DataBinding.FieldName = 'FileDate'
        Options.Editing = False
        Width = 98
      end
      object col_Relief_id: TcxGridDBColumn
        DataBinding.FieldName = 'Relief_id'
        Visible = False
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1DBTableView11
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 701
    Height = 129
    Align = alTop
    Caption = 'Panel1'
    Color = clGreen
    TabOrder = 5
    Visible = False
  end
  object MainActionList: TActionList
    Left = 22
    Top = 54
    object act_Add: TAction
      Caption = 'Add'
      OnExecute = _Actions
    end
    object act_Del: TAction
      Caption = 'Del'
      ShortCut = 46
      OnExecute = _Actions
    end
    object act_AddFromDir: TAction
      Caption = 'AddFromDir'
      OnExecute = _Actions
    end
    object act_CheckExistance: TAction
      Caption = 'act_CheckExistance'
      OnExecute = _Actions
    end
    object act_Export_Frames_to_MIF: TAction
      Caption = #1069#1082#1089#1087#1086#1088#1090' '#1075#1088#1072#1085#1080#1094' '#1074' '#1082#1072#1088#1090#1091
      OnExecute = _Actions
    end
    object act_UnCheck_all: TAction
      Caption = #1057#1085#1103#1090#1100' '#1086#1090#1084#1077#1090#1082#1080' '#1089#1086' '#1074#1089#1077#1093
      OnExecute = _Actions
    end
    object act_Check_all: TAction
      Caption = #1054#1090#1084#1077#1090#1080#1090#1100' '#1074#1089#1077
      OnExecute = _Actions
    end
    object act_Dlg_ShowMatrixList: TAction
      Caption = #1057#1087#1080#1089#1086#1082' '#1086#1090#1082#1088#1099#1090#1099#1093' '#1084#1072#1090#1088#1080#1094
      OnExecute = _Actions
    end
    object act_Check: TAction
      Caption = 'act_Check'
      OnExecute = _Actions
    end
    object act_UnCheck: TAction
      Caption = 'act_UnCheck'
      OnExecute = _Actions
    end
  end
  object ds_Relief: TDataSource
    DataSet = ADOStoredProc1
    Left = 275
    Top = 60
  end
  object PopupMenu1: TPopupMenu
    Left = 188
    Top = 55
    object actAdd1: TMenuItem
      Action = act_Add
    end
    object actDel1: TMenuItem
      Action = act_Del
    end
    object actAddFromDir1: TMenuItem
      Action = act_AddFromDir
    end
    object actCheckExistance1: TMenuItem
      Action = act_CheckExistance
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object N2: TMenuItem
      Action = act_Check_all
    end
    object N3: TMenuItem
      Action = act_UnCheck_all
    end
    object N4: TMenuItem
      Caption = '-'
    end
    object actCheck1: TMenuItem
      Action = act_Check
    end
    object actUnCheck1: TMenuItem
      Action = act_UnCheck
    end
  end
  object dxBarManager1: TdxBarManager
    Scaled = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    CanCustomize = False
    Categories.Strings = (
      'Default')
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    PopupMenuLinks = <>
    SunkenBorder = True
    UseSystemFont = True
    Left = 205
    Top = 302
    DockControlHeights = (
      0
      0
      28
      0)
    object dxBarManager1Bar1: TdxBar
      Caption = 'Custom 1'
      CaptionButtons = <>
      DockedDockingStyle = dsTop
      DockedLeft = 0
      DockedTop = 0
      DockingStyle = dsTop
      FloatLeft = 1003
      FloatTop = 767
      FloatClientWidth = 23
      FloatClientHeight = 22
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarSubItem1'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarButton1'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarButton3'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarButton2'
        end>
      NotDocking = [dsNone, dsLeft, dsTop, dsRight, dsBottom]
      OldName = 'Custom 1'
      OneOnRow = True
      Row = 0
      ShowMark = False
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarSubItem1: TdxBarSubItem
      Caption = #1054#1087#1077#1088#1072#1094#1080#1080
      Category = 0
      Visible = ivAlways
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarButton1'
        end
        item
          Visible = True
          ItemName = 'dxBarButton3'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarButton2'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarButton4'
        end
        item
          Visible = True
          ItemName = 'dxBarButton6'
        end
        item
          Visible = True
          ItemName = 'dxBarButton5'
        end>
    end
    object dxBarButton1: TdxBarButton
      Action = act_Add
      Category = 0
    end
    object dxBarButton2: TdxBarButton
      Action = act_Del
      Category = 0
    end
    object dxBarButton3: TdxBarButton
      Action = act_AddFromDir
      Category = 0
    end
    object dxBarButton4: TdxBarButton
      Action = act_Export_Frames_to_MIF
      Category = 0
    end
    object dxBarButton5: TdxBarButton
      Action = act_Dlg_ShowMatrixList
      Category = 0
    end
    object dxBarButton6: TdxBarButton
      Action = act_CheckExistance
      Category = 0
    end
  end
  object ADOStoredProc1: TADOStoredProc
    LockType = ltReadOnly
    AfterPost = qry_ReliefAfterPost
    Parameters = <>
    Left = 272
    Top = 8
  end
  object cxStyleRepository1: TcxStyleRepository
    Left = 448
    Top = 8
    PixelsPerInch = 96
    object cxStyle1_Strikeout: TcxStyle
      AssignedValues = [svFont]
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsStrikeOut]
    end
  end
end
