inherited dlg_Project_Browse1: Tdlg_Project_Browse1
  Left = 902
  Top = 308
  Width = 494
  Height = 371
  Caption = 'dlg_Project_Browse1'
  OldCreateOrder = True
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  inherited pn_Buttons: TPanel
    Top = 308
    Width = 486
    inherited Bevel1: TBevel
      Width = 486
    end
    inherited Panel3: TPanel
      Left = 307
      inherited btn_Ok: TButton
        Left = 325
      end
      inherited btn_Cancel: TButton
        Left = 409
      end
    end
  end
  inherited pn_Top_: TPanel
    Width = 486
    inherited Bevel2: TBevel
      Width = 486
    end
    inherited pn_Header: TPanel
      Width = 486
      Visible = False
    end
  end
  object pn_Main: TPanel [2]
    Left = 0
    Top = 60
    Width = 486
    Height = 209
    Align = alTop
    Anchors = [akLeft, akTop, akRight, akBottom]
    BevelOuter = bvNone
    TabOrder = 2
    object pn_Projects: TPanel
      Left = 0
      Top = 0
      Width = 486
      Height = 138
      Align = alTop
      Anchors = [akLeft, akTop, akRight, akBottom]
      BevelOuter = bvLowered
      Caption = 'pn_Projects'
      TabOrder = 0
    end
    object pn_Info: TPanel
      Left = 0
      Top = 161
      Width = 486
      Height = 48
      Align = alBottom
      BevelOuter = bvNone
      BorderWidth = 4
      TabOrder = 1
      Visible = False
    end
  end
  inherited ActionList1: TActionList
    OnUpdate = ActionList1Update
    Left = 144
    inherited act_Ok: TAction
      Caption = #1042#1099#1073#1088#1072#1090#1100
    end
  end
  inherited FormStorage1: TFormStorage
    Options = []
    Left = 176
  end
end
