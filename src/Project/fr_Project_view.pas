unit fr_Project_view;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ActnList, Menus, ToolWin, Grids, DBGrids, ComCtrls,  ExtCtrls,  rxPlacemnt,
  ImgList,  RXSplit, cxControls, cxSplitter,  cxPropertiesStore,

  fr_View_base,

  u_cx_vgrid,
  u_Explorer,

  u_func,
  u_db,
 // u_reg,
 // u_dlg,
  u_func_msg,

  u_const_db,
  u_const_msg,

  dm_Project,
  dm_Main,

 // d_ProjectPath,

  fr_Project_RelMatrix1,

  fr_Explorer_Container,

  u_types,

  u_cx,

  fr_DBInspector_Container,

  cxPC, cxVGrid, cxInplaceContainer, AppEvnts, dxBar, cxBarEditItem,
  cxClasses, cxLookAndFeels, cxCheckBox, cxButtonEdit, cxGraphics,
  cxLookAndFeelPainters, cxStyles, cxEdit
  ;


type
  Tframe_Project_view = class(Tframe_View_Base, IMessageHandler)
    FormStorage1: TFormStorage;
    pn_Inspector: TPanel;
    cxSplitter1: TcxSplitter;
    PageControl1: TPageControl;
    ts_Map1: TTabSheet;
    ts_Matrix1: TTabSheet;
    TabSheet3: TTabSheet;
    cxVerticalGrid1: TcxVerticalGrid;
    row_CalcFolder: TcxEditorRow;
//    procedure ApplicationEvents1Message(var Msg: tagMSG; var Handled: Boolean);
    procedure FormCreate(Sender: TObject);
  //  procedure FormDestroy(Sender: TObject);
    procedure row_CalcFolder1EditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
//    procedure row_CalcFolderEditPropertiesEditValueChanged(
//      Sender: TObject);
  //  procedure row_CalcFolderButtonClick(Sender: TObject; AbsoluteIndex: Integer);
  private
   // FID: Integer;

    Ffrm_DBInspector: Tframe_DBInspector_Container;

  //  Ffrm_DBInspector:   Tframe_DBInspector;


    Fframe_RelMatrix:      Tframe_Project_RelMatrix1;
    Fframe_Maps:           Tframe_Explorer_Container;


/////////    Fframe_ProjectPath: Tframe_ProjectPath;

//    procedure DoOnExplorerCustomDrawCellByGUID (Sender: TObject; aPIDL: TrpPIDL;
     //                                           var AColor: TColor; AFont: TFont);
     //
 //   procedure GetMessage(aMsg: integer; aParams: TEventParamList; var aHandled: Boolean); override;
    procedure GetMessage(aMsg: TEventType; aParams: TEventParamList ; var aHandled:
        Boolean);

  public
    procedure View(aID: integer; aGUID: string); override;
  end;


//===================================================================
implementation

uses dm_User_Security; {$R *.dfm}


//===================================================================


//--------------------------------------------------------------------
procedure Tframe_Project_view.FormCreate(Sender: TObject);
//--------------------------------------------------------------------
begin
  inherited;

//  FProcIndex:=RegisterProc (GetMessage, Name);

  pn_Inspector.Align:=alClient;

  Caption:='������';

  ObjectName:=OBJ_PROJECT;


  CreateChildForm(Tframe_DBInspector_Container, Ffrm_DBInspector, pn_Inspector);
  Ffrm_DBInspector.PrepareViewForObject(OBJ_PROJECT);

//  Ffrm_DBInspector:=Tframe_DBInspector_Container.CreateChildForm(pn_Inspector, OBJ_PROJECT);


 /////////// Fframe_ProjectPath:= Tframe_ProjectPath.CreateChildForm( ts_Path);
  row_CalcFolder.properties.Value:= dmMain.GetCalcDir(dmMain.ProjectID);

  CreateChildForm(Tframe_Explorer_Container, Fframe_Maps, ts_Map1);

 // Fframe_Maps := Tframe_Explorer_Container.CreateChildForm (ts_Map);
  Fframe_Maps.SetViewObjects([otMapFile]);
  Fframe_Maps.Load;

  CreateChildForm(Tframe_Project_RelMatrix1, Fframe_RelMatrix, ts_Matrix1);

{  Fframe_RelMatrix:= Tframe_Project_RelMatrix.Create(Self);
  CopyControls (Fframe_RelMatrix, ts_Matrix);
}

(*  Fframe_RelMatrix := Tframe_Explorer.CreateChildForm( ts_Matrix);
  Fframe_RelMatrix.SetViewObjects([otRelMatrixFile]);
  Fframe_RelMatrix.Load;
  Fframe_RelMatrix.SetViewStyle (evsReport);
  Fframe_RelMatrix.OnExplorerCustomDrawCellByGUID:= DoOnExplorerCustomDrawCellByGUID;*)

 // AddControl(GSPages1, [PROP_HEIGHT, PROP_ACTIVE_PAGE_INDEX]);

  AddComponentProp(PageControl1, 'Height');
  AddComponentProp(PageControl1, 'ActivePage');
  cxPropertiesStore.RestoreFrom;



  cx_InitVerticalGrid(cxverticalGrid1);

end;



//-------------------------------------------------------------------
procedure Tframe_Project_view.View(aID: integer; aGUID: string);
//-------------------------------------------------------------------
//var
//  bReadOnly: Boolean;
begin
//  FID:= aID;

  inherited;

  // View(aID);

  Ffrm_DBInspector.View(aID);


//  bReadOnly := dmUser_Security.ProjectIsReadOnly;

//  Ffrm_DBInspector.SetReadOnly(bReadOnly);
//  Fframe_RelMatrix.SetReadOnly(bReadOnly);

  Fframe_RelMatrix.View(aID);


//  Fframe_Maps.View (aID);
//  Fframe_RelMatrix.View (aID);

end;



procedure Tframe_Project_view.row_CalcFolder1EditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  //
end;



procedure Tframe_Project_view.GetMessage(aMsg: TEventType; aParams:
    TEventParamList ; var aHandled: Boolean);
begin
  case aMsg of
    et_Explorer_REFRESH_MAP_FILES: begin
                                     Fframe_Maps.SetViewObjects([otMapFile]);
                                     Fframe_Maps.Load;
                                   end;

    et_PROJECT_CLEAR:  Fframe_Maps.Load;

//    ShowMessage('Tframe_Project_view: WM__PROJECT_CLEAR');


  end;
end;



end.

{


// ---------------------------------------------------------------
procedure Tframe_Project_view.ApplicationEvents1Message(var Msg: tagMSG; var
    Handled: Boolean);
// ---------------------------------------------------------------
begin
  case Msg.message of

    WM_Explorer_REFRESH_MAP_FILES: begin
      Fframe_Maps.SetViewObjects([otMapFile]);
      Fframe_Maps.Load;
    end;


  //  WM_USER + Integer(WM__PROJECT_CLEAR1):
//       Fframe_Maps.Load;

//    ShowMessage('Tframe_Project_view: WM__PROJECT_CLEAR');


  end;

end;


//-------------------------------------------------------------------
procedure Tframe_Project_view.GetMessage(aMsg: integer; aParams: TEventParamList; var aHandled:
    Boolean);
//-------------------------------------------------------------------
begin
  inherited;

//  case aMsg of
//
//    WE_PROJECT_UPDATE_MAP_LIST: begin
//      Fframe_Maps.SetViewObjects([otMapFile]);
//      Fframe_Maps.Load;
//    end;
//
//   (* WE_PROJECT_UPDATE_MATRIX_LIST: begin
////      Fframe_RelMatrix.SetViewObjects([otRelMatrixFile]);
//  //    Fframe_RelMatrix.Load;
//      Fframe_RelMatrix.View(ID);
//
//    end;
//*)
//
//  end;



end;


