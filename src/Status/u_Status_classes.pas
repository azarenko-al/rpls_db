unit u_Status_classes;

interface

uses
  Classes, DB, SysUtils,

  u_Log,

  u_const_db
  ;

type

  // -------------------------------------------------------------------
  TStatusItem = class
  // -------------------------------------------------------------------
  public
    StatusName: string;
    Values    : TStringList;

    procedure AddValue(aName: string; aID: Integer);
  end;


  TStatusList = class(TStringList)
  private
    function GetItems(Index: Integer): TStatusItem;
  public
    constructor Create;
    function AddItem(aStatusName: string): TStatusItem;

    function FindByName(aStatusName: string): TStatusItem;
    function GetNameByID(aStatusName: string; aID: Integer): string;
    function GetIDbyName(aStatusName: string; aName: string): Integer;

    procedure LoadFromDataset(aDataSet: TDataSet);

    property Items[Index: Integer]: TStatusItem read GetItems; default;
  end;


implementation



constructor TStatusList.Create;
begin
  inherited;
  CaseSensitive:=False;
end;


//-------------------------------------------------------------------
function TStatusList.GetIDbyName(aStatusName: string; aName: string): Integer;
//-------------------------------------------------------------------
var
  I: integer;
  oStrList: TStringList;
  oItem: TStatusItem;

begin
  oItem:=FindByName(aStatusName);


  i:=oItem.Values.Count;


  i:=oItem.Values.IndexOf(aName);
  if i>=0 then
    Result := Integer(oItem.Values.Objects[i])
  else
    Result := -1;
end;



function TStatusList.AddItem(aStatusName: string): TStatusItem;
begin
  Result := TStatusItem.Create;
  Result.StatusName:=aStatusName;
  Result.Values:=TStringList.Create;
  Result.Values.CaseSensitive:=False;

  AddObject(aStatusName, Result);
end;


function TStatusList.FindByName(aStatusName: string): TStatusItem;
var
  ind: Integer;
begin
  ind:=IndexOf(aStatusName);
  if ind>=0 then
    Result := Items[ind]
  else
    Result := AddItem(aStatusName);
end;


function TStatusList.GetItems(Index: Integer): TStatusItem;
begin
  Result := TStatusItem(inherited Objects[Index]);
end;


function TStatusList.GetNameByID(aStatusName: string; aID: Integer): string;
var
  iID: Integer;
  ind: Integer;
  oItem: TStatusItem;
begin
  oItem:=FindByName(aStatusName);

  ind:=oItem.Values.IndexOfObject(Pointer(aID));
  if ind>=0 then
    Result := oItem.Values[ind]
  else
    Result := '';
end;


procedure TStatusList.LoadFromDataset(aDataSet: TDataSet);
var
  iID: Integer;
  oItem: TStatusItem;
  sName: string;
  sStatusName: string;
begin
  Clear;

  with aDataSet do
  begin
    First;

    while not EOF do
    begin
      sStatusName :=FieldByName(FLD_status_name).AsString;

      oItem:=FindByName(sStatusName);

      iID   :=FieldByName(FLD_ID).AsInteger;
      sName :=FieldByName(FLD_NAME).AsString;

      oItem.AddValue(sName, iID);

      Next;
    end;
  end;
end;

procedure TStatusItem.AddValue(aName: string; aID: Integer);
begin
  Values.AddObject(aName, Pointer(aID));
end;

end.
