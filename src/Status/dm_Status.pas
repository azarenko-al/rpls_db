unit dm_Status;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ADODB,

  dm_Main,

  u_db,
  u_Log,

  u_const_db,

  u_Status_classes;

type
  TdmStatus = class(TDataModule)
    qry_Status: TADOQuery;
    procedure DataModuleDestroy(Sender: TObject);
    procedure DataModuleCreate(Sender: TObject);
  private

  public
    StatusList: TStatusList;

    function AssignList(aStatusName : string; aStrings: TStrings): Boolean;

    function AssignDataset(aDataset: TDataset; aStatusName: string): Boolean;


    procedure Load;
    procedure ReLoad;
  end;


function dmStatus: TdmStatus;

//==================================================================
//==================================================================
implementation

uses dm_Onega_db_data;
{$R *.DFM}

var
  FdmStatus: TdmStatus;


// ---------------------------------------------------------------
function dmStatus: TdmStatus;
// ---------------------------------------------------------------
begin
 if not Assigned(FdmStatus) then
     FdmStatus := TdmStatus.Create(Application);

  Result := FdmStatus;
end;



function TdmStatus.AssignDataset(aDataset: TDataset; aStatusName: string):
    Boolean;

var
  i: Integer;
  j: Integer;
  oStatusItem: TStatusItem;
begin
  oStatusItem:=StatusList.FindByName(aStatusName);

  Result := Assigned(oStatusItem);

  if Result then
    for I := 0 to oStatusItem.Values.Count - 1 do
    begin
      j:=Integer( oStatusItem.Values.Objects[i]);

      db_AddRecord_(aDataset,
         [
          FLD_NAME, oStatusItem.Values[i],
          FLD_ID,   Integer(oStatusItem.Values.Objects[i])
         ]);

    end;

//  db_View(aDataset);
 // end;

end;


procedure TdmStatus.DataModuleCreate(Sender: TObject);
begin
  inherited;
  StatusList := TStatusList.Create();

  db_SetComponentADOConnection(Self, dmMain.AdoConnection);

  Load();
end;


procedure TdmStatus.DataModuleDestroy(Sender: TObject);
begin
  FreeAndNil(StatusList);
  inherited;
end;


procedure TdmStatus.ReLoad;
begin
  StatusList.Clear;
  Load();
end;

// ---------------------------------------------------------------
procedure TdmStatus.Load;
// ---------------------------------------------------------------
begin
  if StatusList.Count>0 then
    Exit;


  g_Log.Add('Open table: '+ VIEW_Status_Values);

  dmOnega_DB_data.OpenQuery (qry_Status, 'SELECT * FROM '+ VIEW_Status_Values);

//  db_view(qry_Status);


  StatusList.LoadFromDataset(qry_Status);

end;


function TdmStatus.AssignList(aStatusName : string; aStrings: TStrings):
    Boolean;
var
  i: Integer;
  oStatusItem: TStatusItem;
begin
  oStatusItem:=StatusList.FindByName(aStatusName);

  Result := Assigned(oStatusItem);

  if Result then
  begin
    i:=oStatusItem.Values.Count;

    if Assigned(oStatusItem) then
      aStrings.Assign(oStatusItem.Values);
  end;

end;



end.



