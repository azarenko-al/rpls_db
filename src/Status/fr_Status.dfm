object frame_Status: Tframe_Status
  Left = 510
  Top = 476
  Width = 1039
  Height = 531
  Caption = 'frame_Status'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object cxGrid1: TcxGrid
    Left = 0
    Top = 0
    Width = 225
    Height = 438
    Align = alLeft
    TabOrder = 0
    LookAndFeel.Kind = lfFlat
    object cxGrid1DBTableView1: TcxGridDBTableView
      NavigatorButtons.ConfirmDelete = False
      OnFocusedRecordChanged = cxGrid1DBTableView1FocusedRecordChanged
      DataController.DataSource = ds_Status_Items
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsCustomize.ColumnFiltering = False
      OptionsCustomize.ColumnGrouping = False
      OptionsCustomize.ColumnsQuickCustomization = True
      OptionsData.Appending = True
      OptionsData.Deleting = False
      OptionsView.ColumnAutoWidth = True
      OptionsView.GroupByBox = False
      object col__status_name: TcxGridDBColumn
        DataBinding.FieldName = 'name'
        Visible = False
        Options.Filtering = False
        Width = 125
      end
      object col_Object_Caption: TcxGridDBColumn
        Caption = #1054#1073#1098#1077#1082#1090
        DataBinding.FieldName = 'object_caption'
        Visible = False
        Width = 58
      end
      object col_status_caption: TcxGridDBColumn
        Caption = #1053#1072#1079#1074#1072#1085#1080#1077
        DataBinding.FieldName = 'caption'
        Options.Editing = False
        SortIndex = 0
        SortOrder = soDescending
        Width = 152
      end
      object col_obj_name: TcxGridDBColumn
        DataBinding.FieldName = 'object_name'
        Visible = False
        Width = 89
      end
      object col__id: TcxGridDBColumn
        Visible = False
        Options.Filtering = False
        Width = 21
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1DBTableView1
    end
  end
  object cxSplitter1: TcxSplitter
    Left = 225
    Top = 0
    Width = 8
    Height = 438
    HotZoneClassName = 'TcxSimpleStyle'
    InvertDirection = True
    Control = cxGrid1
  end
  object pn_Values: TPanel
    Left = 689
    Top = 0
    Width = 334
    Height = 438
    Align = alRight
    BevelOuter = bvNone
    Caption = 'pn_Values'
    TabOrder = 2
    object cxGrid2: TcxGrid
      Left = 0
      Top = 67
      Width = 334
      Height = 371
      Align = alBottom
      Anchors = [akLeft, akTop, akRight, akBottom]
      TabOrder = 0
      LookAndFeel.Kind = lfFlat
      object cxGridDBTableView1: TcxGridDBTableView
        PopupMenu = PopupMenu1
        NavigatorButtons.ConfirmDelete = False
        DataController.DataSource = ds_Status_values
        DataController.KeyFieldNames = 'id'
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        OptionsBehavior.ImmediateEditor = False
        OptionsCustomize.ColumnFiltering = False
        OptionsCustomize.ColumnSorting = False
        OptionsCustomize.ColumnsQuickCustomization = True
        OptionsData.Deleting = False
        OptionsView.ColumnAutoWidth = True
        OptionsView.GroupByBox = False
        OptionsView.Indicator = True
        object col_Band: TcxGridDBColumn
          Caption = #1044#1080#1072#1087#1072#1079#1086#1085
          DataBinding.FieldName = 'band'
          SortIndex = 1
          SortOrder = soAscending
          Width = 95
        end
        object cxGridDBColumn1_name: TcxGridDBColumn
          Caption = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077
          DataBinding.FieldName = 'name'
          Width = 72
        end
        object col_values: TcxGridDBColumn
          Caption = #1059#1089#1083#1086#1074#1080#1077
          DataBinding.FieldName = 'values'
          Width = 33
        end
        object col_index_: TcxGridDBColumn
          Caption = #1048#1085#1076#1077#1082#1089
          DataBinding.FieldName = 'index_'
          PropertiesClassName = 'TcxTextEditProperties'
          Properties.Alignment.Horz = taLeftJustify
          SortIndex = 0
          SortOrder = soAscending
          Width = 54
        end
        object cxGridDBColumn3_id: TcxGridDBColumn
          DataBinding.FieldName = 'id'
          Visible = False
        end
        object col_FREQ_MIN_MHZ: TcxGridDBColumn
          DataBinding.FieldName = 'FREQ_MIN_MHZ'
          PropertiesClassName = 'TcxCurrencyEditProperties'
          Properties.DisplayFormat = ',0'
          Width = 33
        end
        object col_FREQ_Max_MHZ: TcxGridDBColumn
          DataBinding.FieldName = 'FREQ_Max_MHZ'
          PropertiesClassName = 'TcxCurrencyEditProperties'
          Properties.DisplayFormat = ',0'
          Width = 33
        end
        object col_comment: TcxGridDBColumn
          DataBinding.FieldName = 'comment'
        end
      end
      object cxGridLevel1: TcxGridLevel
        GridView = cxGridDBTableView1
      end
    end
    object ToolBar1: TToolBar
      Left = 0
      Top = 0
      Width = 334
      Height = 29
      ButtonHeight = 21
      ButtonWidth = 57
      Caption = 'ToolBar1'
      ShowCaptions = True
      TabOrder = 1
      object ToolButton1: TToolButton
        Left = 0
        Top = 2
        Action = act_Add
      end
      object ToolButton2: TToolButton
        Left = 57
        Top = 2
        Action = act_Del
      end
    end
  end
  object DBGrid1: TDBGrid
    Left = 0
    Top = 438
    Width = 1023
    Height = 54
    Align = alBottom
    DataSource = ds_Status_values
    TabOrder = 3
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Visible = False
  end
  object qry_Status_values: TADOQuery
    Connection = ADOConnection1_test
    CursorType = ctStatic
    Filter = 'status_name='#39'link_status'#39
    Filtered = True
    OnNewRecord = qry_Status_valuesNewRecord
    Parameters = <>
    SQL.Strings = (
      'SELECT * FROM view_Status_values')
    Left = 369
    Top = 192
  end
  object ds_Status_values: TDataSource
    DataSet = qry_Status_values
    Left = 368
    Top = 248
  end
  object ds_Status_Items: TDataSource
    DataSet = qry_Status_Items
    Left = 272
    Top = 248
  end
  object qry_Status_Items: TADOQuery
    Connection = ADOConnection1_test
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'SELECT * FROM  view_status_items'
      '')
    Left = 272
    Top = 192
  end
  object ADOConnection1_test: TADOConnection
    ConnectionString = 
      'Provider=MSDASQL.1;Persist Security Info=False;User ID=sa;Data S' +
      'ource=onega_;'
    LoginPrompt = False
    Provider = 'MSDASQL.1'
    Left = 268
    Top = 115
  end
  object ActionList1: TActionList
    OnUpdate = ActionList1Update
    Left = 268
    Top = 56
    object act_Add: TAction
      Category = 'Main'
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100
      OnExecute = _Action
    end
    object act_Del: TAction
      Category = 'Main'
      Caption = #1059#1076#1072#1083#1080#1090#1100
      OnExecute = _Action
    end
    object act_Save: TAction
      Category = 'Main'
      Caption = 'act_Save'
      OnExecute = _Action
    end
    object act_Cancel_local: TAction
      Category = 'Main'
      Caption = 'act_Cancel_local'
      OnExecute = _Action
    end
  end
  object PopupMenu1: TPopupMenu
    Left = 336
    Top = 56
    object N1: TMenuItem
      Action = act_Add
    end
    object N3: TMenuItem
      Action = act_Del
    end
    object actDown2: TMenuItem
      Action = act_Save
      Visible = False
    end
  end
  object cxStyleRepository1: TcxStyleRepository
    Left = 448
    Top = 64
    PixelsPerInch = 96
    object cxStyle_Edit: TcxStyle
    end
    object cxStyle_ReadOnly: TcxStyle
      AssignedValues = [svColor]
      Color = clInactiveBorder
    end
  end
  object StrHolder1: TStrHolder
    Macros = <>
    Left = 368
    Top = 360
    InternalVer = 1
  end
end
