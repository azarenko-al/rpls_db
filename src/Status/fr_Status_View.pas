unit fr_Status_View;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
  Dialogs, ActnList, Menus, DBCtrls, StdCtrls, rxPlacemnt, ToolWin,

  fr_View_base,

  dm_Main,

  fr_Status,

  u_types,
  u_const,

  u_func,
  u_reg,
  u_db,
  u_dlg, cxControls, cxSplitter, cxPropertiesStore, ImgList, 
   ExtCtrls, dxBar, cxBarEditItem, cxClasses, cxLookAndFeels
  ;


type
  Tframe_Status_View = class(Tframe_View_Base)
    FormStorage1: TFormStorage;
    cxSplitter1: TcxSplitter;
    procedure FormCreate(Sender: TObject);

  private
    Fframe_Status: Tframe_Status;
  public
  end;


//====================================================================
implementation{$R *.dfm}


//--------------------------------------------------------------------
procedure Tframe_Status_View.FormCreate(Sender: TObject);
//--------------------------------------------------------------------

begin
  inherited;

  ObjectName:=OBJ_Status_LIST;

  CreateChildForm(Tframe_Status, Fframe_Status, pn_Main);

end;


end.


