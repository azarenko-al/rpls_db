unit dm_Act_Status;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
  Dialogs, AppEvnts, Menus, ActnList, IniFiles,

  dm_act_Base,

  u_Types,

  fr_Status_view

  ;

type
  TdmAct_Status = class(TdmAct_Base)
    procedure DataModuleCreate(Sender: TObject);
  private
    function  GetViewForm  (aOwnerForm: TForm): TForm; override;

  public
    class procedure Init;
  end;

var
  dmAct_Status: TdmAct_Status;

//==================================================================
implementation {$R *.dfm}
//==================================================================



class procedure TdmAct_Status.Init;
begin
  if not Assigned(dmAct_Status) then
    dmAct_Status:= TdmAct_Status.Create(Application);
end;

//-------------------------------------------------------------------
procedure TdmAct_Status.DataModuleCreate(Sender: TObject);
//-------------------------------------------------------------------
begin
  inherited;

  ObjectName:=OBJ_Status_LIST;

end;      


//--------------------------------------------------------------------
function TdmAct_Status.GetViewForm (aOwnerForm: TForm): TForm;
//--------------------------------------------------------------------
begin
  Result:= Tframe_Status_View.Create(aOwnerForm);
end;


begin

end.