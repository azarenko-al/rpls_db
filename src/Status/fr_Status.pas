unit fr_Status;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxSplitter, cxGridLevel, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxClasses, cxControls, cxGridCustomView, cxGrid, DB,
  ADODB, ActnList, ImgList,
  Menus, ExtCtrls, ComCtrls, ToolWin, Grids,

  dm_User_Security,

  dm_Main,

  dm_Status,

  u_cx,

  u_types,
  u_const,
  u_const_db,

  u_func,
  u_reg,
  u_db,
  u_dlg,

  DBGrids, cxStyles, rxStrHlder,  cxGraphics,
  cxLookAndFeels, cxLookAndFeelPainters, dxSkinsCore,
  dxSkinsDefaultPainters, dxSkinscxPCPainter, cxCustomData, cxFilter,
  cxData, cxDataStorage, cxEdit, cxDBData, cxTextEdit, cxCurrencyEdit

  ;

type
  Tframe_Status = class(TForm)
    qry_Status_values: TADOQuery;
    ds_Status_values: TDataSource;
    ds_Status_Items: TDataSource;
    qry_Status_Items: TADOQuery;
    ADOConnection1_test: TADOConnection;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    col_Object_Caption: TcxGridDBColumn;
    col_status_caption: TcxGridDBColumn;
    col_obj_name: TcxGridDBColumn;
    col__id: TcxGridDBColumn;
    col__status_name: TcxGridDBColumn;
    cxGrid1Level1: TcxGridLevel;
    cxSplitter1: TcxSplitter;
    ActionList1: TActionList;
    act_Add: TAction;
    act_Del: TAction;
    act_Save: TAction;
    act_Cancel_local: TAction;
    pn_Values: TPanel;
    cxGrid2: TcxGrid;
    cxGridDBTableView1: TcxGridDBTableView;
    cxGridDBColumn1_name: TcxGridDBColumn;
    col_index_: TcxGridDBColumn;
    cxGridDBColumn3_id: TcxGridDBColumn;
    cxGridLevel1: TcxGridLevel;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    N3: TMenuItem;
    actDown2: TMenuItem;
    DBGrid1: TDBGrid;
    cxStyleRepository1: TcxStyleRepository;
    cxStyle_Edit: TcxStyle;
    cxStyle_ReadOnly: TcxStyle;
    StrHolder1: TStrHolder;
    col_values: TcxGridDBColumn;
    col_Band: TcxGridDBColumn;
    col_FREQ_MIN_MHZ: TcxGridDBColumn;
    col_FREQ_Max_MHZ: TcxGridDBColumn;
    col_comment: TcxGridDBColumn;
    ToolBar1: TToolBar;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    procedure ActionList1Update(Action: TBasicAction; var Handled: Boolean);
    procedure cxGrid1DBTableView1FocusedRecordChanged(Sender:
        TcxCustomGridTableView; APrevFocusedRecord, AFocusedRecord:
        TcxCustomGridRecord; ANewItemRecordFocusingChanged: Boolean);
    procedure FormDestroy(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure qry_Status_valuesNewRecord(DataSet: TDataSet);
    procedure _Action(Sender: TObject);
  private

    FReadOnly: Boolean;


    procedure Select_Status_Items;
    procedure SetReadOnly(aValue: boolean);
// TODO: ViewStatusValues
//  procedure ViewStatusValues;

  public

  end;

  
implementation

{$R *.dfm}


procedure Tframe_Status.ActionList1Update(Action: TBasicAction; var Handled:
    Boolean);
var
  b: Boolean;
begin
  act_Add.Enabled:=not  FReadOnly;
  act_Del.Enabled:=not  FReadOnly;

  b:=Eq(qry_Status_Items.FieldByName(FLD_NAME).AsString, 'Property_PLACE_DEVICE_TYPE');
  col_values.Visible:=b;

  b:=Eq(qry_Status_Items.FieldByName(FLD_NAME).AsString, 'recommendation');
  col_Band.Visible        :=b;
  col_FREQ_Min_MHZ.Visible:=b;
  col_FREQ_Max_MHZ.Visible:=b;
  col_index_.Visible      :=not b;

end;

// ---------------------------------------------------------------
procedure Tframe_Status.FormCreate(Sender: TObject);
// ---------------------------------------------------------------
var
  bReadOnly: Boolean;
  s: string;
begin

  bReadOnly:=dmUser_Security.Lib_Edit_ReadOnly();

  SetReadOnly(bReadOnly);

//  --------------------


  db_SetComponentADOConnection (Self, dmMain.AdoConnection);

//  cxGrid1.Align:= alClient;

  pn_Values.Align:= alClient;
  cxGrid2.Align:=alClient;

  col_FREQ_Min_MHZ.Caption:='������ ������� ��������� [MHz]';
  col_FREQ_Max_MHZ.Caption:='������� ������� ��������� [MHz]';

  col_Band.Caption       :='�������� ������';
  col_comment.Caption        :='����������';

//  col_FREQ_Min_MHZ.Caption:='������ ������� ��������� [MHz]';
//  col_FREQ_Max_MHZ.Caption:='������� ������� ��������� [MHz]';


 // qry_Status.AfterScroll:=nil;


  db_OpenQuery(qry_Status_Items,  'SELECT * FROM '+ view_status_items);

  s:='SELECT * FROM '+ TBL_status + ' WHERE parent_id is not null';
  db_OpenQuery(qry_Status_values, s);


    {
    Result:= dmUser_Security.Modify_lib in [mtAllow, mtAllowMy];

  SetActionsEnabled1( [
          act_Del_,
          act_Del_list,
          act_Copy,
        //  act_Export_MDB,
          act_Import_MDB,
          act_Import,

          act_LoadFromFile,
          act_Update_Band,

          act_Restore_bands,
          act_Set_Band
      ],

   Result );
   }


//  qry_Status.AfterScroll:=qry_StatusAfterScroll;

 // iID :=reg_ReadInteger(FRegPath, 'id', 0);

 // qry_Status_Items.Locate(FLD_ID, iID, []);

end;



procedure Tframe_Status.FormDestroy(Sender: TObject);
var
  iID: integer;
begin
 // iID :=qry_Status.FieldByName(FLD_ID).AsInteger;
  //reg_WriteInteger(FRegPath, 'id', iID);

end;


procedure Tframe_Status.Select_Status_Items;
var
  iID: Integer;
begin
  iID := qry_Status_Items.FieldByName(FLD_ID).AsInteger;
//  qry_Status_values.Filter := Format('status_name=''%s''', [sName]);
  qry_Status_values.Filter := Format('parent_id=%d', [iID]);
end;


procedure Tframe_Status.qry_Status_valuesNewRecord(DataSet: TDataSet);
begin
  qry_Status_values[FLD_parent_id] := qry_Status_Items[FLD_ID];
  qry_Status_values[FLD_index_]    := qry_Status_Items.RecordCount+1;

//

end;




//--------------------------------------------------------------------
procedure Tframe_Status._Action(Sender: TObject);
//--------------------------------------------------------------------
var
  iOldIndex, iNewIndex: Integer;
  sTable, sSql: string;
  bDirectionIsUp: boolean;
begin
  if Sender = act_Add then
  begin
    qry_Status_values.Append;
  end else


  if sender = act_Del then
  begin
//    if ConfirmDlg('�� ������������� ������ ������� ��� �������� �� �� (�������������� ������ ����� ����������)?') then
    if ConfirmDlg('�������?') then
       qry_Status_values.Delete;

  end else

{



  if sender = act_Del then begin
    if ConfirmDlg('�� ������������� ������ ������� ��� �������� �� �� (�������������� ������ ����� ����������)?') then begin
      case FType of
        otProperty: sTable:= TBL_PROPERTY;
        otSite    : sTable:= TBL_SITE;
        otCell    : sTable:= TBL_CELL;
        otLink    : sTable:= TBL_LINK;
        otLinkEnd : sTable:= TBL_LINKEND;
        otPMPSite : sTable:= TBL_PMP_SITE;
     //zzzzz   otSite_3G : sTable:= TBL_CDMA_SITE;
      end;
      sSql:= Format('UPDATE %s SET %s=NULL WHERE %s=%d',
        [sTable, FFieldName, FFieldName, qry_Items.FieldByName(FLD_ID).AsInteger]);
      if gl_DB.ExecCommand(sSql) then
        qry_Items.Delete;
    end;
  end else

  }


  {
  //-----------------------------------------------
  if (Sender=act_Down) or (Sender=act_Up) then
  //-----------------------------------------------
  begin
    bDirectionIsUp:= (Sender=act_Up);

    db_MoveRecordUpDown (qry_Status_values, FLD_INDEX_,  bDirectionIsUp); //FLD_ID,
  end else

  //-----------------------------------------------
  if (Sender=act_Top) or (Sender=act_Bottom) then
  //-----------------------------------------------
  begin
    bDirectionIsUp:= (Sender=act_Top);

    db_MoveRecordTopBottom (qry_Status_values, FLD_INDEX_, bDirectionIsUp); // FLD_ID,
  end ;

  }

end;



procedure Tframe_Status.cxGrid1DBTableView1FocusedRecordChanged(Sender:
    TcxCustomGridTableView; APrevFocusedRecord, AFocusedRecord:
    TcxCustomGridRecord; ANewItemRecordFocusingChanged: Boolean);
begin
  Select_Status_Items();
end;



// ---------------------------------------------------------------
procedure Tframe_Status.SetReadOnly(aValue: boolean);
// ---------------------------------------------------------------
begin
  FReadOnly:=aValue;

  { check : !!! }
  cx_SetGridReadOnly (cxGridDBTableView1, aValue);

  //--------------------------------------------

  if aValue then
    cxGrid1DBTableView1.Styles.Background:=cxStyle_ReadOnly
  else
    cxGrid1DBTableView1.Styles.Background:=cxStyle_Edit;

  //--------------------------------------------
  if aValue then
    cxGridDBTableView1.Styles.Background:=cxStyle_ReadOnly
  else
    cxGridDBTableView1.Styles.Background:=cxStyle_Edit;


end;




end.

//
//
//(*
//
//
//procedure Tframe_Status_View.qry_Status_valuesBeforeInsert(DataSet: TDataSet);
//begin
//  qry_Status_values.Last;
//  FNewIndex:= qry_Status_values.FieldByName(FLD_INDEX_).AsInteger + 1;
//end;*)
//
//
//
//{
//  b:= FType<>otNone;
//
//  if b then begin
//    act_Up.Enabled        := IIF (qry_Items.Active, qry_Items.Recno<>1, false); // (not qry_Items.BOF);
//    act_Top.Enabled       := act_Up.Enabled; // (not qry_Items.BOF);
//    act_Down.Enabled      := IIF (qry_Items.Active, qry_Items.Recno<>qry_Items.RecordCount, false); // (not qry_Items.EOF);
//    act_Bottom.Enabled    := act_Down.Enabled;
//    act_Del.Enabled       := IIF (qry_Items.Active, not qry_Items.IsEmpty, false);
//    act_Add.Enabled       := true;
//  end else begin
//    act_Up.Enabled   := false;
//    act_Down.Enabled := false;
//    act_Top.Enabled  := false;
//    act_Bottom.Enabled  := false;
//    act_Del.Enabled  := false;
//    act_Add.Enabled  := false;
//  end;
//
//}
//
//
////--------------------------------------------------------------------
//procedure Tframe_Status_View._Action(Sender: TObject);
////--------------------------------------------------------------------
//var
//  iOldIndex, iNewIndex: Integer;
//  sTable, sSql: string;
//  bDirectionIsUp: boolean;
//begin
//  if Sender = act_Add then
//  begin
//    qry_Status_values.Append;
//  end else
//
//
//  if sender = act_Del then begin
////    if ConfirmDlg('�� ������������� ������ ������� ��� �������� �� �� (�������������� ������ ����� ����������)?') then
//    if ConfirmDlg('�������?') then
//       qry_Status_values.Delete;
//
//  end else
//
//{
//
//
//
//  if sender = act_Del then begin
//    if ConfirmDlg('�� ������������� ������ ������� ��� �������� �� �� (�������������� ������ ����� ����������)?') then begin
//      case FType of
//        otProperty: sTable:= TBL_PROPERTY;
//        otSite    : sTable:= TBL_SITE;
//        otCell    : sTable:= TBL_CELL;
//        otLink    : sTable:= TBL_LINK;
//        otLinkEnd : sTable:= TBL_LINKEND;
//        otPMPSite : sTable:= TBL_PMP_SITE;
//     //zzzzz   otSite_3G : sTable:= TBL_CDMA_SITE;
//      end;
//      sSql:= Format('UPDATE %s SET %s=NULL WHERE %s=%d',
//        [sTable, FFieldName, FFieldName, qry_Items.FieldByName(FLD_ID).AsInteger]);
//      if gl_DB.ExecCommand(sSql) then
//        qry_Items.Delete;
//    end;
//  end else
//
//  }
//
//
//
//  //-----------------------------------------------
//  if (Sender=act_Down) or (Sender=act_Up) then
//  //-----------------------------------------------
//  begin
//    bDirectionIsUp:= (Sender=act_Up);
//
//    db_MoveRecordUpDown (qry_Status_values, FLD_INDEX_,  bDirectionIsUp); //FLD_ID,
//  end else
//
//  //-----------------------------------------------
//  if (Sender=act_Top) or (Sender=act_Bottom) then
//  //-----------------------------------------------
//  begin
//    bDirectionIsUp:= (Sender=act_Top);
//
//    db_MoveRecordTopBottom (qry_Status_values, FLD_INDEX_, bDirectionIsUp); // FLD_ID,
//  end ;
//
//end;
//
//
//
//
//  db_SetComponentADOConnection (Self, dmMain.AdoConnection);
//
//
////  cxGrid1.Align:= alClient;
//
//  pn_Values.Align:= alClient;
//  cxGrid2.Align:=alClient;
//
//  qry_Status.AfterScroll:=nil;
//
//(*  db_OpenQuery(qry_Objects, 'SELECT id,name,display_name FROM '
//                            +TBL_OBJECTS+' WHERE display_name<>'''' ORDER BY display_name');
//
//*)
//
//
//  db_OpenQuery(qry_Status, 'SELECT * FROM '+TBL_status);
//
//(*  db_OpenQuery(qry_Status, 'SELECT * FROM '+TBL_status+
//                           ' WHERE (parent_id IS NULL) and (caption<>'''')');
//*)
//
////  qry_Status.AfterScroll:=qry_StatusAfterScroll;
//
//  iID :=reg_ReadInteger(FRegPath, 'id', 0);
//
//  qry_Status.Locate(FLD_ID, iID, []);
//
//
//  // TODO: ViewStatusValues
////// ---------------------------------------------------------------
////procedure Tframe_Status.ViewStatusValues;
////// ---------------------------------------------------------------
////var
////iID: Integer;
////begin
////(*
////iID := qry_Status.FieldByName(FLD_ID).AsInteger;
////
////db_OpenQuery(qry_Status_values,
////    'SELECT * FROM '+TBL_Status_Values+' WHERE status_id=:status_id ORDER BY index_',
////    [db_Par(FLD_status_id, iID)]);
////*)
////
////(*
////db_OpenQuery(qry_Status_values,
////            'SELECT * FROM '+TBL_status+' WHERE parent_id=:parent_id ORDER BY index_',
////            [db_Par(FLD_parent_id, qry_Status.FieldByName(FLD_ID).AsInteger)]);
////*)
////end;

