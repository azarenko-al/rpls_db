inherited dlg_Template_PMP_site_add: Tdlg_Template_PMP_site_add
  Left = 426
  Top = 296
  Caption = 'dlg_Template_PMP_site_add'
  PixelsPerInch = 96
  TextHeight = 13
  inherited Panel2: TPanel
    inherited PageControl1: TPageControl
      Height = 105
      inherited TabSheet1: TTabSheet
        object cxVerticalGrid1: TcxVerticalGrid
          Left = 0
          Top = 0
          Width = 536
          Height = 74
          Align = alClient
          LookAndFeel.Kind = lfOffice11
          OptionsView.CellTextMaxLineCount = 3
          OptionsView.AutoScaleBands = False
          OptionsView.PaintStyle = psDelphi
          OptionsView.GridLineColor = clBtnShadow
          OptionsView.RowHeaderMinWidth = 30
          OptionsView.RowHeaderWidth = 247
          OptionsView.ValueWidth = 40
          TabOrder = 0
          Version = 1
          object row_CalcRadius_km: TcxEditorRow
            Expanded = False
            Properties.Caption = #1056#1072#1076#1080#1091#1089' '#1088#1072#1089#1095#1077#1090#1072' [km]'
            Properties.EditPropertiesClassName = 'TcxSpinEditProperties'
            Properties.EditProperties.Alignment.Horz = taLeftJustify
            Properties.EditProperties.Alignment.Vert = taVCenter
            Properties.EditProperties.AssignedValues.MinValue = True
            Properties.EditProperties.MaxValue = 100.000000000000000000
            Properties.EditProperties.ReadOnly = False
            Properties.DataBinding.ValueType = 'Float'
            Properties.Value = Null
            ID = 0
            ParentID = -1
            Index = 0
            Version = 1
          end
        end
      end
    end
  end
end
