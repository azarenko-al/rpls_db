inherited dlg_Template_Linkend_add: Tdlg_Template_Linkend_add
  Left = 1303
  Top = 390
  Height = 531
  Caption = 'dlg_Template_Linkend_add'
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  inherited pn_Buttons: TPanel
    Top = 469
  end
  inherited pn_Top_: TPanel
    inherited pn_Header: TPanel
      Height = 55
    end
  end
  inherited Panel2: TPanel
    Height = 292
    inherited PageControl1: TPageControl
      Height = 244
      inherited TabSheet1: TTabSheet
        object cxVerticalGrid1: TcxVerticalGrid
          Left = 0
          Top = 0
          Width = 536
          Height = 213
          Align = alClient
          LookAndFeel.Kind = lfOffice11
          LookAndFeel.NativeStyle = False
          OptionsView.CellTextMaxLineCount = 3
          OptionsView.AutoScaleBands = False
          OptionsView.PaintStyle = psDelphi
          OptionsView.GridLineColor = clBtnShadow
          OptionsView.RowHeaderMinWidth = 30
          OptionsView.RowHeaderWidth = 253
          OptionsView.ValueWidth = 40
          TabOrder = 0
          Version = 1
          object row_Cell_Layer: TcxEditorRow
            Expanded = False
            Properties.Caption = #1057#1090#1072#1085#1076#1072#1088#1090' '#1089#1077#1090#1080
            Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
            Properties.EditProperties.Alignment.Horz = taLeftJustify
            Properties.EditProperties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            Properties.EditProperties.OnButtonClick = row_Cell_LayerEditPropertiesButtonClick
            Properties.DataBinding.ValueType = 'String'
            Properties.Value = Null
            ID = 0
            ParentID = -1
            Index = 0
            Version = 1
          end
          object row_CalcModel: TcxEditorRow
            Expanded = False
            Properties.Caption = #1052#1086#1076#1077#1083#1100' '#1088#1072#1089#1095#1077#1090#1072
            Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
            Properties.EditProperties.Alignment.Horz = taLeftJustify
            Properties.EditProperties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            Properties.EditProperties.OnButtonClick = row_Cell_LayerEditPropertiesButtonClick
            Properties.DataBinding.ValueType = 'String'
            Properties.Value = Null
            ID = 1
            ParentID = -1
            Index = 1
            Version = 1
          end
          object row_CalcRadius_km: TcxEditorRow
            Expanded = False
            Properties.Caption = 'CalcRadius_km'
            Properties.EditPropertiesClassName = 'TcxTextEditProperties'
            Properties.DataBinding.ValueType = 'Float'
            Properties.Value = Null
            ID = 2
            ParentID = -1
            Index = 2
            Version = 1
          end
          object cxVerticalGrid1CategoryRow2: TcxCategoryRow
            Expanded = False
            ID = 3
            ParentID = -1
            Index = 3
            Version = 1
          end
          object row_equipment_type: TcxEditorRow
            Properties.Caption = 'Equipment_type'
            Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
            Properties.EditProperties.Alignment.Horz = taLeftJustify
            Properties.EditProperties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            Properties.EditProperties.OnButtonClick = row_Cell_LayerEditPropertiesButtonClick
            Properties.DataBinding.ValueType = 'String'
            Properties.Value = Null
            ID = 4
            ParentID = -1
            Index = 4
            Version = 1
          end
          object row_Power_dBm: TcxEditorRow
            Expanded = False
            Properties.Caption = 'Power_dBm'
            Properties.EditPropertiesClassName = 'TcxTextEditProperties'
            Properties.EditProperties.Alignment.Horz = taLeftJustify
            Properties.EditProperties.Alignment.Vert = taVCenter
            Properties.EditProperties.MaxLength = 0
            Properties.EditProperties.ReadOnly = False
            Properties.DataBinding.ValueType = 'String'
            Properties.Value = Null
            ID = 5
            ParentID = 4
            Index = 0
            Version = 1
          end
          object row_Threshold: TcxEditorRow
            Expanded = False
            Properties.Caption = 'Threshold'
            Properties.EditPropertiesClassName = 'TcxTextEditProperties'
            Properties.EditProperties.Alignment.Horz = taLeftJustify
            Properties.EditProperties.Alignment.Vert = taVCenter
            Properties.EditProperties.MaxLength = 0
            Properties.EditProperties.ReadOnly = False
            Properties.DataBinding.ValueType = 'String'
            Properties.Value = Null
            ID = 6
            ParentID = 4
            Index = 1
            Version = 1
          end
          object cxVerticalGrid1CategoryRow3: TcxCategoryRow
            Expanded = False
            ID = 7
            ParentID = -1
            Index = 5
            Version = 1
          end
          object row_Combiner: TcxEditorRow
            Properties.Caption = #1050#1086#1084#1073#1072#1081#1085#1077#1088
            Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
            Properties.EditProperties.Alignment.Horz = taLeftJustify
            Properties.EditProperties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            Properties.EditProperties.OnButtonClick = row_Cell_LayerEditPropertiesButtonClick
            Properties.DataBinding.ValueType = 'String'
            Properties.Value = Null
            ID = 8
            ParentID = -1
            Index = 6
            Version = 1
          end
          object row_Combiner_Loss: TcxEditorRow
            Expanded = False
            Properties.Caption = #1055#1086#1090#1077#1088#1080' '#1040#1060#1059
            Properties.EditPropertiesClassName = 'TcxTextEditProperties'
            Properties.EditProperties.Alignment.Horz = taLeftJustify
            Properties.EditProperties.Alignment.Vert = taVCenter
            Properties.EditProperties.MaxLength = 0
            Properties.EditProperties.ReadOnly = False
            Properties.DataBinding.ValueType = 'String'
            Properties.Value = Null
            ID = 9
            ParentID = 8
            Index = 0
            Version = 1
          end
          object cxVerticalGrid1CategoryRow4: TcxCategoryRow
            Expanded = False
            ID = 10
            ParentID = -1
            Index = 7
            Version = 1
          end
          object row_Antenna: TcxEditorRow
            Properties.Caption = #1040#1085#1090#1077#1085#1085#1072
            Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
            Properties.EditProperties.Alignment.Horz = taLeftJustify
            Properties.EditProperties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            Properties.EditProperties.OnButtonClick = row_Cell_LayerEditPropertiesButtonClick
            Properties.DataBinding.ValueType = 'String'
            Properties.Value = Null
            ID = 11
            ParentID = -1
            Index = 8
            Version = 1
          end
          object row_Height: TcxEditorRow
            Expanded = False
            Properties.Caption = #1042#1099#1089#1086#1090#1072' [m]'
            Properties.EditPropertiesClassName = 'TcxTextEditProperties'
            Properties.EditProperties.Alignment.Horz = taLeftJustify
            Properties.EditProperties.Alignment.Vert = taVCenter
            Properties.EditProperties.MaxLength = 0
            Properties.EditProperties.ReadOnly = False
            Properties.DataBinding.ValueType = 'String'
            Properties.Value = ''
            ID = 12
            ParentID = 11
            Index = 0
            Version = 1
          end
          object row_Azimuth: TcxEditorRow
            Expanded = False
            Properties.Caption = #1040#1079#1080#1084#1091#1090' ['#1075#1088']'
            Properties.EditPropertiesClassName = 'TcxTextEditProperties'
            Properties.EditProperties.Alignment.Horz = taLeftJustify
            Properties.EditProperties.Alignment.Vert = taVCenter
            Properties.EditProperties.MaxLength = 0
            Properties.EditProperties.ReadOnly = False
            Properties.DataBinding.ValueType = 'String'
            Properties.Value = ''
            ID = 13
            ParentID = 11
            Index = 1
            Version = 1
          end
        end
      end
    end
  end
  inherited ActionList1: TActionList
    OnUpdate = ActionList1Update
  end
end
