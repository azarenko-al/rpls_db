unit fr_Template_PMP_Site_View;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,cxControls, cxSplitter,
  Dialogs, ActnList, Menus, DBCtrls, StdCtrls, rxPlacemnt, ToolWin,
  ComCtrls, ExtCtrls, Db, ADODB,  AppEvnts, ImgList, 

  dm_User_Security,

  fr_View_base,

  fr_DBInspector_Container,
  fr_Template_PMP_Site_Cells,


  u_const_db,

  u_reg,
  u_dlg,
  u_func,

  u_types,

   cxPropertiesStore, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, dxSkinsCore, dxSkinsDefaultPainters, dxBar,
  cxBarEditItem, cxClasses
  ;


type
  Tframe_Template_PMP_Site_View = class(Tframe_View_Base)
    FormStorage1: TFormStorage;
    pn_Inspector: TPanel;
    cxSplitter1: TcxSplitter;
    pn_Cells: TPanel;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    Ffrm_Inspector: Tframe_DBInspector_Container;
    Ffrm_SiteTemplate_Cells: Tframe_SiteTemplate_Cells;

  public

    procedure View(aID: integer; aGUID: string); override;
  end;



//====================================================================
implementation {$R *.dfm}
//====================================================================

//--------------------------------------------------------------------
procedure Tframe_Template_PMP_Site_View.FormCreate(Sender: TObject);
//--------------------------------------------------------------------
begin
  inherited;

  ObjectName:=OBJ_TEMPLATE_PMP_SITE;
  TableName :=TBL_TEMPLATE_PMP_SITE;


  pn_Inspector.Align:=alClient;


 // Ffrm_SiteTemplate_Cells :=Tframe_SiteTemplate_Cells.Create (pn_Cells);

  //////////////////
  CreateChildForm(Tframe_SiteTemplate_Cells, Ffrm_SiteTemplate_Cells, pn_Cells);
  CreateChildForm(Tframe_DBInspector_Container,  Ffrm_Inspector,          pn_Inspector);

  Ffrm_Inspector.PrepareViewForObject(OBJ_TEMPLATE_PMP_SITE);

  pn_Cells.Height:=reg_ReadInteger(FRegPath, pn_Cells.Name,  pn_Cells.Height);

end;

//--------------------------------------------------------------------
procedure Tframe_Template_PMP_Site_View.FormDestroy(Sender: TObject);
//--------------------------------------------------------------------
begin
  reg_WriteInteger(FRegPath, pn_Cells.name,  pn_Cells.Height);

  inherited;
end;


//--------------------------------------------------------------------
procedure Tframe_Template_PMP_Site_View.View(aID: integer; aGUID: string);
//--------------------------------------------------------------------
var
  bReadOnly: Boolean;
begin
  inherited;

  Ffrm_Inspector.View (aID);
  Ffrm_SiteTemplate_Cells.View(aID);


  bReadOnly:=dmUser_Security.Lib_Edit_ReadOnly();

  Ffrm_Inspector.SetReadOnly(bReadOnly);

//  Fframe_LinkEndType_Band.SetReadOnly(bReadOnly);

end;



end.
