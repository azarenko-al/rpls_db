unit fr_Template_Linkend_view;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Menus, AppEvnts, ImgList, ActnList,
   ExtCtrls,

  dm_User_Security,

  fr_View_base,

  u_func,
  u_const_str,

  u_types,

   fr_Template_Linkend_inspector, 

//  fr_DBInspector_Container,

  cxLookAndFeels, dxBar, cxBarEditItem, cxClasses, cxPropertiesStore;

type
  Tframe_Template_Linkend_view = class(Tframe_View_Base)
    procedure FormCreate(Sender: TObject);
 //   procedure FormDestroy(Sender: TObject);
  private
 //   Finspector: Tframe_DBInspector_Container;

    Finspector: Tframe_Template_Linkend_inspector;


  public
    procedure View(aID: integer; aGUID: string); override;
  end;


//===============================================================
implementation{$R *.dfm}
//===============================================================

//--------------------------------------------------------------------
procedure Tframe_Template_Linkend_view.FormCreate(Sender: TObject);
//--------------------------------------------------------------------
begin
  inherited;
  ObjectName:=OBJ_TEMPLATE_LINKEND;



  CreateChildForm(Tframe_Template_Linkend_inspector, Finspector, pn_Main);
 // Finspector.PrepareViewForObject ( OBJ_TEMPLATE_LINKEND);


//  CreateChildForm(Tframe_DBInspector_Container, Finspector, pn_Main);
//  Finspector.PrepareViewForObject ( OBJ_TEMPLATE_LINKEND);
//  Finspector.


end;


//--------------------------------------------------------------------
procedure Tframe_Template_Linkend_view.View(aID: integer; aGUID: string);
//--------------------------------------------------------------------
var
  bReadOnly: Boolean;
begin
  inherited;

//  Fframe_LinkEnd_Inspector.View (aID, OBJ_LINKEND);


  Finspector.View (aID);

  bReadOnly:=dmUser_Security.Lib_Edit_ReadOnly();

  Finspector.SetReadOnly(bReadOnly);

end;


end.

