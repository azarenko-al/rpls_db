inherited dmTemplate_PMP_Site: TdmTemplate_PMP_Site
  Left = 673
  Top = 417
  Height = 335
  Width = 510
  object qry_Site: TADOQuery
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'id'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 5
      end>
    SQL.Strings = (
      'select   id,name'
      ' from property'
      'where id=:id')
    Left = 119
    Top = 155
  end
  object qry_TempCell: TADOQuery
    MarshalOptions = moMarshalModifiedOnly
    EnableBCD = False
    Parameters = <>
    Left = 34
    Top = 95
  end
  object qry_TempAntenna: TADOQuery
    MarshalOptions = moMarshalModifiedOnly
    EnableBCD = False
    Parameters = <>
    Left = 120
    Top = 96
  end
  object qry_Template_Cells: TADOQuery
    LockType = ltReadOnly
    Parameters = <>
    Left = 248
    Top = 188
  end
  object qry_Template_Ant: TADOQuery
    LockType = ltReadOnly
    Parameters = <>
    Left = 312
    Top = 140
  end
  object qry_Temp: TADOQuery
    LockType = ltReadOnly
    Parameters = <>
    Left = 322
    Top = 39
  end
end
