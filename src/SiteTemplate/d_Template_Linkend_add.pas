unit d_Template_Linkend_add;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  rxPlacemnt, ActnList,     StdCtrls, ExtCtrls,
    Registry, cxPropertiesStore, cxVGrid, cxControls, cxInplaceContainer,  cxButtonEdit,
  ComCtrls, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters,
  dxSkinsCore, dxSkinsDefaultPainters, cxStyles, cxEdit, cxSpinEdit,

  dm_Main,

  u_cx_vgrid,

  u_cx_VGrid_export,


  d_Wizard_Add_with_params,

  dm_Template_Linkend,

  dm_LinkEndType,
  dm_TrxType,
  dm_Template_Ant,

 // u_const_3G_str,
  //u_const_db_3g,

  u_const_db,
  u_const_str,

  u_SiteTemplate_types,

  u_GEO,
  u_db,

  u_dlg,
  u_img,
  u_func,

  u_Types,


  cxTextEdit
  ;

type
  Tdlg_Template_Linkend_add = class(Tdlg_Wizard_add_with_params)
    cxVerticalGrid1: TcxVerticalGrid;
    row_Cell_Layer: TcxEditorRow;
    row_CalcModel: TcxEditorRow;
    row_CalcRadius_km: TcxEditorRow;
    cxVerticalGrid1CategoryRow2: TcxCategoryRow;
    row_equipment_type: TcxEditorRow;
    row_Power_dBm: TcxEditorRow;
    row_Threshold: TcxEditorRow;
    cxVerticalGrid1CategoryRow3: TcxCategoryRow;
    row_Combiner: TcxEditorRow;
    row_Combiner_Loss: TcxEditorRow;
    cxVerticalGrid1CategoryRow4: TcxCategoryRow;
    row_Antenna: TcxEditorRow;
    row_Height: TcxEditorRow;
    row_Azimuth: TcxEditorRow;
    procedure FormCreate(Sender: TObject);
  //  procedure row_ButtonClick(Sender: TObject;
   //   AbsoluteIndex: Integer);
    procedure FormDestroy(Sender: TObject);
    procedure act_OkExecute(Sender: TObject);
    procedure ActionList1Update(Action: TBasicAction;
      var Handled: Boolean);
    procedure row_Cell_LayerEditPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
  protected

  private
    FTemplate_Linkend_ID: Integer;

    FTemplate_pmp_Site_ID: Integer;

    FLINKENDTYPE_ID: Integer;


    FAntTypeID: Integer;
    FCombinerID: Integer;
    FCellLayerID: Integer;
    FCalcModelID: Integer;

    FColor  : integer;

   // FType: TSiteTemplate_Type;
//..    FStrType: string;
//    FBLPoint: TBLPoint;

    function Append: Boolean;
    procedure Antenna_Append;
    procedure GetEquipmentParams();

    procedure LoadFromReg;

//    procedure ReadFromRegisry;
  public
    class function ExecDlg(aTemplate_PMP_Site_ID: integer): integer;
  end;


//==============================================================================
implementation {$R *.dfm}
//==============================================================================


//------------------------------------------------------------------------------
class function Tdlg_Template_Linkend_add.ExecDlg(aTemplate_PMP_Site_ID:
    integer): integer;
//------------------------------------------------------------------------------
begin
  Assert(aTemplate_PMP_Site_ID<>0);


  with Tdlg_Template_Linkend_add.Create(Application) do
  try
    FTemplate_pmp_Site_ID := aTemplate_PMP_Site_ID;


    LoadFromReg();

    //----------------------------------
//     s := dmOnega_DB_data.Antenna_GetNewName(ObjectName, aSiteID);

//    ed_Name_.Text := dmTemplate_Linkend.GetNewName_for_Cell1(FTemplate_pmp_Site_ID);
    ed_Name_.Text := dmTemplate_Linkend.GetNewName(FTemplate_pmp_Site_ID);


    row_equipment_type.Properties.Value:= gl_DB.GetNameByID(TBL_LINKENDTYPE, FLINKENDTYPE_ID);

    //----------------------------------

    ShowModal;

    Result:=FTemplate_Linkend_ID;

  finally
    Free;
  end;
end;


//------------------------------------------------------------------------------
procedure Tdlg_Template_Linkend_add.FormCreate(Sender: TObject);
//------------------------------------------------------------------------------
begin
  inherited;
  SetActionName(STR_DLG_ADD_CELL_TEMPLATE);

  row_CalcRadius_km.Properties.Caption := STR_ROW_RADIUS_km;
  row_Power_dBm.Properties.Caption := STR_POWER_dBm;
//  row_Freq_MHz.Properties.Caption  := STR_FREQ_MHz;
 // row_Site.Caption      := '������ ��';

  row_equipment_type.Properties.Caption:= '��� �����������������';
 // row_bs_type.Caption       := '��� �������';

  row_Threshold.Properties.Caption:= STR_SENSITIVITY_dBm;

  cx_InitVerticalGrid(cxVerticalGrid1);

//    SetDefaultSize();


//S  FRegPath:=FRegPath + 'aaaa\';



 //   ReadFromRegisry;

end;


//-------------------------------------------------------------------
procedure Tdlg_Template_Linkend_add.LoadFromReg;
//-------------------------------------------------------------------
begin
  cx_VerticalGrid_LoadFromReg (cxVerticalGrid1, FRegPath);


//  Assert(FStrType<>'');

  with TRegIniFile.Create(FRegPath) do
  begin
     FLINKENDTYPE_ID     := ReadInteger('', FLD_LINKENDTYPE_ID     ,0);
     FCellLayerID          := ReadInteger('', FLD_CELL_LAYER_ID      ,0);
     FCalcModelID          := ReadInteger('', FLD_CALC_MODEL_ID      ,0);
     FCombinerID           := ReadInteger('', FLD_COMBINER_ID        ,0);
     FAntTypeID            := ReadInteger('', FLD_ANTENNATYPE_ID     ,0);

     row_CalcRadius_km.Properties.Value    := ReadString ('', row_CalcRadius_km.Name      ,'10');
     row_Height.Properties.Value       := ReadString ('', row_Height.Name         ,'30');
     row_Power_dBm.Properties.Value    := ReadString ('', row_Power_dBm.Name      ,'');
     row_Threshold.Properties.Value  := ReadString ('', row_Threshold.Name    ,'');
//     row_Freq_MHz.Properties.Value     := ReadString ('', row_Freq_MHz.Name       ,'');
     row_Combiner_Loss.Properties.Value:= ReadString ('', row_Combiner_Loss.Name  ,'');

     Free;
  end;

  row_Combiner.Properties.Value   := gl_DB.GetNameByID(TBL_PASSIVE_COMPONENT, FCombinerID);
  row_Cell_Layer.Properties.Value:= gl_DB.GetNameByID(TBL_CELLLAYER, FCellLayerID);
  row_CalcModel.Properties.Value := gl_DB.GetNameByID(TBL_CALCMODEL, FCalcModelID);
  row_Antenna.Properties.Value    := gl_DB.GetNameByID(TBL_ANTENNATYPE,      FAntTypeID);

  if row_Combiner.Properties.Value=''   then  FCombinerID:=0;
  if row_Cell_Layer.Properties.Value='' then  FCellLayerID:=0;
  if row_CalcModel.Properties.Value=''  then  FCalcModelID:=0;
  if row_Antenna.Properties.Value=''    then  FAntTypeID:=0;

end;

//------------------------------------------------------------------------------
procedure Tdlg_Template_Linkend_add.FormDestroy(Sender: TObject);
//------------------------------------------------------------------------------
begin
  cx_VerticalGrid_SaveToReg (cxVerticalGrid1, FRegPath);


  with TRegIniFile.Create(FRegPath) do
  begin

    WriteInteger('', FLD_CELL_LAYER_ID  ,  FCellLayerID);
    WriteInteger('', FLD_LINKENDTYPE_ID ,  FLINKENDTYPE_ID);
    WriteInteger('', FLD_CALC_MODEL_ID  ,  FCalcModelID);
    WriteInteger('', FLD_COMBINER_ID    ,  FCombinerID);
    WriteInteger('', FLD_ANTENNATYPE_ID ,  FAntTypeID);

    WriteString ('', row_CalcRadius_km.Name ,  AsString(row_CalcRadius_km.Properties.Value));
    WriteString ('', row_Height.Name        ,  AsString(row_Height.Properties.Value));
    WriteString ('', row_Power_dBm.Name     ,  AsString(row_Power_dBm.Properties.Value));
    WriteString ('', row_Threshold.Name   ,  AsString(row_Threshold.Properties.Value));
//    WriteString ('', row_Freq_MHz.Name     ,  row_Freq_MHz.Properties.Value);
    WriteString ('', row_Combiner_Loss.Name,  AsString(row_Combiner_Loss.Properties.Value));

    Free;
  end;

  inherited;
end;


//------------------------------------------------------------------------------
procedure Tdlg_Template_Linkend_add.act_OkExecute(Sender: TObject);
//------------------------------------------------------------------------------
begin
  inherited;
 // dxInspector2.PostEditor;

  Append();
  Antenna_Append();
end;




//------------------------------------------------------------------------------
procedure Tdlg_Template_Linkend_add.GetEquipmentParams();
//------------------------------------------------------------------------------
var recTRX: TdmTrxTypeAddRec;
    rRecLinkEndType: TdmLinkEndTypeInfoRec;
begin
//  with dxInspector2 do
//  begin
  //-------------------------------------
  if FLINKENDTYPE_ID <= 0 then
  //-------------------------------------
  begin
    row_Power_dBm.Properties.Value    := '';
    row_Threshold.Properties.Value  := '';
   // row_Freq_MHz.Properties.Value     := '';

    Exit;
  end;

  //--------------------------
//  if FType = stPMP then
    if dmLinkEndType.GetInfoRec(FLINKENDTYPE_ID, rRecLinkEndType) then
    begin
      if rRecLinkEndType.Threshold_ber_3=0 then
        rRecLinkEndType.Threshold_ber_3:=-90;


      row_Power_dBm.Properties.Value  := rRecLinkEndType.Power_Max;
      row_Threshold.Properties.Value:= rRecLinkEndType.Threshold_ber_3;

// Added by alex 06.05.2010 11:43:48
  ///////    row_freq.Text       := AsString(rRecLinkEndType.RANGE * 1000);
    end;

 // end;
end;


//------------------------------------------------------------------------------
procedure Tdlg_Template_Linkend_add.Antenna_Append;
//------------------------------------------------------------------------------
var
  rec: TdmTemplate_Antenna_Rec;
  
begin
//  if FAntTypeID=0 then Exit;
  FillChar(rec, SizeOf(rec), 0);

  rec.NewName :='�������1';
  rec.Height  := AsFloat(row_Height.Properties.Value);
  rec.Azimuth := AsFloat(row_Azimuth.Properties.Value);

  rec.AntennaType_ID     := FAntTypeID;
  rec.Template_Linkend_ID:= FTemplate_Linkend_ID;

  dmTemplate_ant.Add (rec);
end;


//------------------------------------------------------------------------------
function Tdlg_Template_Linkend_add.Append: Boolean;
//------------------------------------------------------------------------------
var
  rec: TdmTemplate_Linkend_Rec1;
  
begin
  FillChar (rec, SizeOf(rec), 0);

  with rec do
  begin
    Template_pmp_Site_ID := FTemplate_pmp_Site_ID;

    NewName          := ed_Name_.Text;
 //   BS_Type        := FType;

    CellLayer_ID    := FCellLayerID;
    CalcModel_ID    := FCalcModelID;

 //   TrxID           := FLINKENDTYPE_ID;
    LinkEndType_ID  := FLINKENDTYPE_ID;

    CalcRadius_km  := AsFloat(row_CalcRadius_km.Properties.Value);

    Combiner_ID    := FCombinerID;
    CombinerLoss   := AsFloat(row_Combiner_Loss.Properties.Value);

    POWER_dBm      := AsFloat(row_Power_dBm.Properties.Value);
   // Tx_FREQ_MHz    := AsFloat(row_Freq_MHz.Properties.Value);

//    Threshold_dBm:= AsFloat(row_Sense_Noise.Text);

    SENSE_dBm:= AsFloat(row_Threshold.Properties.Value);
  //  Noise:= SENSE_dBm;

//    ChannelCount   := gl_DB.GetIntFieldValueByID(TBL_CELLLAYER, FLD_CHANNEL_LOGIC_COUNT, FCellLayerID);

(*    maxLoadingUL   := 50;
    PowerPercentPS := 10;
*)
  end;

  FTemplate_Linkend_ID := dmTemplate_linkend.Add (rec);

  Result := FTemplate_Linkend_ID>0;
end;


//------------------------------------------------------------------------------
procedure Tdlg_Template_Linkend_add.ActionList1Update(Action: TBasicAction; var Handled: Boolean);
//------------------------------------------------------------------------------
begin
  act_Ok.Enabled:=(FLINKENDTYPE_ID>0) and
                  (FCellLayerID>0);// and
                 // (FAntTypeID>0)  //     and
                 // (FTemplate_pmp_Site_ID>0);
end;


procedure Tdlg_Template_Linkend_add.row_Cell_LayerEditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin


  //-------------------------------
  if cxVerticalGrid1.FocusedRow=row_equipment_type then
  begin
    if Dlg_SelectObject_cxButtonEdit (Sender as TcxButtonEdit, otLinkEndType, FLINKENDTYPE_ID) then
      GetEquipmentParams();
  end else

  //-------------------------------
  if cxVerticalGrid1.FocusedRow=row_CalcModel then
    Dlg_SelectObject_cxButtonEdit(Sender as TcxButtonEdit, otCalcModel, FCalcModelID) else

  //-------------------------------
  if cxVerticalGrid1.FocusedRow=row_Cell_Layer then
    Dlg_SelectObject_cxButtonEdit(Sender as TcxButtonEdit, otCellLayer, FCellLayerID) else

  //-------------------------------
  if cxVerticalGrid1.FocusedRow=row_Antenna then
     Dlg_SelectObject_cxButtonEdit(Sender as TcxButtonEdit, otAntennaType, FAntTypeID) else

  //-------------------------------
  if cxVerticalGrid1.FocusedRow=row_Combiner then begin
     if Dlg_SelectObject_cxButtonEdit(Sender as TcxButtonEdit, otCombiner, FCombinerID) then
       row_Combiner_Loss.Properties.Value:=
          gl_DB.GetDoubleFieldValueByID(TBL_PASSIVE_COMPONENT, FLD_LOSS, FCombinerID);
  end else
    raise Exception.Create('');

end;

end.

