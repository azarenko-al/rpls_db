inherited dlg_Template_Antenna_add: Tdlg_Template_Antenna_add
  Left = 502
  Top = 332
  Height = 428
  Caption = 'dlg_Template_Antenna_add'
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  inherited pn_Buttons: TPanel
    Top = 366
  end
  inherited Panel2: TPanel
    Height = 202
    inherited PageControl1: TPageControl
      Height = 141
      inherited TabSheet1: TTabSheet
        object cxVerticalGrid1: TcxVerticalGrid
          Left = 0
          Top = 0
          Width = 536
          Height = 110
          Align = alClient
          LookAndFeel.Kind = lfOffice11
          OptionsView.CellTextMaxLineCount = 3
          OptionsView.AutoScaleBands = False
          OptionsView.PaintStyle = psDelphi
          OptionsView.GridLineColor = clBtnShadow
          OptionsView.RowHeaderMinWidth = 30
          OptionsView.RowHeaderWidth = 144
          OptionsView.ValueWidth = 56
          TabOrder = 0
          Version = 1
          object row_Antenna_Model: TcxEditorRow
            Expanded = False
            Properties.Caption = #1052#1086#1076#1077#1083#1100' '#1072#1085#1090#1077#1085#1085#1099
            Properties.EditPropertiesClassName = 'TcxButtonEditProperties'
            Properties.EditProperties.Alignment.Horz = taLeftJustify
            Properties.EditProperties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            Properties.EditProperties.OnButtonClick = row_Antenna_Model_EditPropertiesButtonClick
            Properties.DataBinding.ValueType = 'String'
            Properties.Value = Null
            ID = 0
            ParentID = -1
            Index = 0
            Version = 1
          end
          object cxVerticalGrid1CategoryRow1: TcxCategoryRow
            Expanded = False
            ID = 1
            ParentID = -1
            Index = 1
            Version = 1
          end
          object row_Height: TcxEditorRow
            Expanded = False
            Properties.Caption = #1042#1099#1089#1086#1090#1072' [m]'
            Properties.EditPropertiesClassName = 'TcxTextEditProperties'
            Properties.EditProperties.Alignment.Horz = taLeftJustify
            Properties.EditProperties.Alignment.Vert = taVCenter
            Properties.EditProperties.MaxLength = 0
            Properties.EditProperties.ReadOnly = False
            Properties.DataBinding.ValueType = 'Float'
            Properties.Value = 30.000000000000000000
            ID = 2
            ParentID = -1
            Index = 2
            Version = 1
          end
          object row_Azimuth: TcxEditorRow
            Expanded = False
            Properties.Caption = #1040#1079#1080#1084#1091#1090' ['#1075#1088']'
            Properties.EditPropertiesClassName = 'TcxTextEditProperties'
            Properties.EditProperties.Alignment.Horz = taLeftJustify
            Properties.EditProperties.Alignment.Vert = taVCenter
            Properties.EditProperties.MaxLength = 0
            Properties.EditProperties.ReadOnly = False
            Properties.DataBinding.ValueType = 'String'
            Properties.Value = '0'
            ID = 3
            ParentID = -1
            Index = 3
            Version = 1
          end
          object cxVerticalGrid1CategoryRow2: TcxCategoryRow
            Visible = False
            ID = 4
            ParentID = -1
            Index = 4
            Version = 1
          end
        end
      end
    end
  end
  inherited ActionList1: TActionList
    Left = 452
    Top = 8
  end
  inherited FormStorage1: TFormStorage
    Left = 424
    Top = 8
  end
end
