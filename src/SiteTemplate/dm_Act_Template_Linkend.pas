unit dm_Act_Template_Linkend;

interface

uses
  Classes, Forms, ActnList,

  dm_Onega_DB_data,
  dm_User_Security,

  dm_act_Base,

  I_Audit,

  I_Object,

  d_Template_Linkend_add,

  u_const_str,
  u_const_db,

  
  fr_Template_Linkend_view,

  u_Types,
  

  
  u_func,
  
   Menus;

type
  TdmAct_Template_Linkend = class(TdmAct_Base)
    ActionList2: TActionList;
    act_Antenna_Add: TAction;
    act_Audit: TAction;
    procedure DataModuleCreate(Sender: TObject);
  private
    procedure DoAction(Sender: TObject);
  protected
    procedure GetPopupMenu(aPopupMenu: TPopupMenu; aMenuType: TMenuType); override;
    function GetViewForm (aOwnerForm: TForm): TForm; override;

    function CheckActionsEnable: Boolean;


    function ItemDel(aID: integer; aName: string = ''): boolean; override;

  public
//    function Dlg_Edit(aID: integer): boolean; //override;

    class procedure Init;
  end;

var
  dmAct_Template_Linkend: TdmAct_Template_Linkend;

//====================================================================
implementation

uses dm_Main; {$R *.dfm}
//====================================================================

//--------------------------------------------------------------------
class procedure TdmAct_Template_Linkend.Init;
//--------------------------------------------------------------------
begin
  if not Assigned(dmAct_Template_Linkend) then
    dmAct_Template_Linkend:=TdmAct_Template_Linkend.Create(Application);
end;

//--------------------------------------------------------------------
procedure TdmAct_Template_Linkend.DataModuleCreate(Sender: TObject);
//--------------------------------------------------------------------
begin
  inherited;

  ObjectName:=OBJ_TEMPLATE_LINKEND;


  act_Antenna_Add.Caption:='�������� �������';

     act_Audit.Caption:=STR_Audit;


 SetActionsExecuteProc ([

           act_Antenna_Add,

           act_Audit

         //  act_Copy

         ], DoAction);

end;


// ---------------------------------------------------------------
procedure TdmAct_Template_Linkend.DoAction(Sender: TObject);
// ---------------------------------------------------------------
var
  iID: integer;

begin
  if Sender=act_Audit then
    Init_IAuditX.ExecDlg( dmMain.GetConnectionString, TBL_Template_Linkend, FFocusedID) else



  // -----------------------------
  if Sender = act_Antenna_Add then
  // -----------------------------
  begin
    iID:= Tdlg_Template_Linkend_add.ExecDlg (FFocusedID);

   // if iID > 0 then
    //  View();
  end else


end;

{
//--------------------------------------------------------------------
function TdmAct_Template_Linkend.Dlg_Edit(aID: integer): boolean;
//--------------------------------------------------------------------
begin
  Result:=Tframe_Template_Linkend_inspector.ExecDlg (aID);
end;
}


//--------------------------------------------------------------------
function TdmAct_Template_Linkend.GetViewForm (aOwnerForm: TForm): TForm;
//--------------------------------------------------------------------
begin
  Result:=Tframe_Template_Linkend_View.Create(aOwnerForm);
end;


// ---------------------------------------------------------------
function TdmAct_Template_Linkend.CheckActionsEnable: Boolean;
// ---------------------------------------------------------------
begin
  Result:= dmUser_Security.Modify_lib in [mtAllow, mtAllowMy];

  SetActionsEnabled1( [
          act_Del_,
          act_Audit,
          act_Del_list
  //        act_Copy
        //  act_Export_MDB,
//
      ],

   Result );


  //  TModify_lib = (mtAllow=1, mtAllowMy=2, mtForbidden=0 );


end;



//--------------------------------------------------------------------
procedure TdmAct_Template_Linkend.GetPopupMenu(aPopupMenu: TPopupMenu;
    aMenuType: TMenuType);
//--------------------------------------------------------------------
var
  bEnable: Boolean;
begin
  bEnable:=CheckActionsEnable();

  case aMenuType of
    mtFolder: begin
               // AddFolderMenu_Create (aPopupMenu);
                //AddFolderPopupMenu (aPopupMenu);


                AddMenuItem (aPopupMenu, act_Add);
              end;
    mtItem:   begin
                AddMenuItem (aPopupMenu, act_Antenna_Add);

//                AddMenuItem (aPopupMenu, act_Cell_Add);
 //               AddMenuItem (aPopupMenu, nil);
                AddMenuItem (aPopupMenu, act_Del_);

                AddMenuItem (aPopupMenu, act_Audit);

              //  AddFolderMenu_Tools (aPopupMenu);
             //   AddMenuItem (aPopupMenu, act_Export_MDB);

              end;
    mtList:   begin
                AddMenuItem (aPopupMenu, act_Del_list);
              //  AddFolderMenu_Tools (aPopupMenu);
               // AddMenuItem (aPopupMenu, act_Export_MDB);

              end;
  end;

end;

function TdmAct_Template_Linkend.ItemDel(aID: integer; aName: string = ''):
    boolean;
begin
   dmOnega_DB_data.Template_Linkend_del (aID);
   Result:=True;
   
//  Result:=dmTemplate_Pmp_Site.Del (aID);
 // Result := inherited ItemDel(aID, aName);
end;


end.






