unit d_Template_PMP_site_add;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  rxPlacemnt, ActnList,     Registry,
  StdCtrls, cxVGrid, cxControls, cxInplaceContainer, ExtCtrls, 
    cxPropertiesStore,

  d_Wizard_Add_with_params,

  u_cx_vgrid,

  u_cx_VGrid_export,


 // dm_Folder,
  dm_Template_pmp_Site,

  u_SiteTemplate_types,

  u_const_str,
  u_const_db,

  u_Geo,
  u_dlg,
  u_reg,
  u_db,
  u_cx,
  u_func,

  //u_cx_vgrid,

  u_types,

  ComCtrls, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters,
  dxSkinsCore, dxSkinsDefaultPainters, cxStyles, cxEdit, cxSpinEdit
  ;


type
  Tdlg_Template_PMP_site_add = class(Tdlg_Wizard_add_with_params)
    cxVerticalGrid1: TcxVerticalGrid;
    row_CalcRadius_km: TcxEditorRow;
//    procedure row_FolderButtonClick(Sender: TObject; AbsoluteIndex: Integer);
    procedure act_OkExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
   private
    FID: Integer;


    procedure Append;
  public
    class function ExecDlg(aFolderID: integer): integer;
  end;


//==============================================================================
implementation {$R *.dfm}
//==============================================================================


//------------------------------------------------------------------------------
class function Tdlg_Template_PMP_site_add.ExecDlg(aFolderID: integer): integer;
//------------------------------------------------------------------------------
begin
  with Tdlg_Template_PMP_site_add.Create(Application) do
  begin
    ed_Name_.Text:= dmTemplate_PMP_Site.GetNewName;

    Result:= IIF(ShowModal=mrOk, FID, 0);

    Free;
  end;
end;


//------------------------------------------------------------------------------
procedure Tdlg_Template_PMP_site_add.FormCreate(Sender: TObject);
//------------------------------------------------------------------------------
begin
  inherited;
  SetActionName (STR_DLG_ADD_SITE_TEMPLATE);
//  row_Folder.Caption:=STR_FOLDER;


  with TRegIniFile.Create(FRegPath) do
  begin
//    row_type.Text      := ReadString('', 'BS_TYPE', BS_GSM_TYPE);
    row_CalcRadius_km.Properties.Value:= AsFloat(ReadString('', 'CalcRadius_km', '10'));

    Free;
  end;

  SetDefaultSize();

  cx_InitVerticalGrid (cxVerticalGrid1);


  cx_VerticalGrid_LoadFromReg (cxVerticalGrid1, FRegPath);

end;

//------------------------------------------------------------------------------
procedure Tdlg_Template_PMP_site_add.FormDestroy(Sender: TObject);
//------------------------------------------------------------------------------
begin
  cx_VerticalGrid_SaveToReg (cxVerticalGrid1, FRegPath);


  with TRegIniFile.Create(FRegPath) do
  begin
 //   WriteString(Name, 'BS_TYPE', row_type.Text);
    WriteString(Name, 'CalcRadius_km', FloatToStr(row_CalcRadius_km.Properties.Value));

    Free;
  end;

  inherited;
end;
 

//------------------------------------------------------------------------------
procedure Tdlg_Template_PMP_site_add.act_OkExecute(Sender: TObject);
//------------------------------------------------------------------------------
begin
  inherited;
  Append;
end;

//------------------------------------------------------------------------------
procedure Tdlg_Template_PMP_site_add.Append;
//------------------------------------------------------------------------------
var
  rec: TdmTemplate_Pmp_Site_Rec;

begin
  FillChar(rec, SizeOf(rec), 0);

  rec.NewName      := ed_Name_.Text;
  rec.CalcRadius_km:= AsFloat(row_CalcRadius_km.Properties.Value);

  FID:=dmTemplate_PMP_Site.Add (rec);
end;




end.