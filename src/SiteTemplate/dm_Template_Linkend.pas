unit dm_Template_Linkend;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Db, ADODB, Forms, Dialogs,Variants,

  dm_Main,

//  I_CalcModel,

  dm_Onega_DB_data,

  u_SiteTemplate_types,

  u_const_str,
  u_const_db,

  u_types,

  u_func,
  u_db,
  u_img,

  dm_Object_base
  ;

type

  TdmTemplate_Linkend = class(TdmObject_base)
    procedure DataModuleCreate(Sender: TObject);
  private
  public
//    function Del (aID: integer): boolean; override;

    function Add(aRec: TdmTemplate_Linkend_Rec1): integer;
    function GetNewName(aParentID: integer): string;
  end;


function dmTemplate_Linkend: TdmTemplate_Linkend;


//==============================================================================
implementation {$R *.dfm}
//==============================================================================

var
  FdmTemplate_Linkend : TdmTemplate_Linkend ;


// ---------------------------------------------------------------
function dmTemplate_Linkend: TdmTemplate_Linkend;
// ---------------------------------------------------------------
begin
  if not Assigned(FdmTemplate_Linkend) then
    FdmTemplate_Linkend := TdmTemplate_Linkend.Create(Application);

  Result := FdmTemplate_Linkend;
end;


//------------------------------------------------------------------------------
procedure TdmTemplate_Linkend.DataModuleCreate(Sender: TObject);
//------------------------------------------------------------------------------
begin
  inherited;
  TableName:=TBL_TEMPLATE_LINKEND;
  DisplayName:=STR_CELL;
end;


function TdmTemplate_Linkend.GetNewName(aParentID: integer): string;
var
  rec: TObject_GetNewName_Params;
begin
  FillChar(rec,SizeOf(rec),0);
  rec.ObjName    := OBJ_TEMPLATE_LINKEND;
  rec.Project_ID := dmMain.ProjectID;
  rec.Parent_ID  := aParentID;


  Result := dmOnega_DB_data.Object_GetNewName_new
     (rec, OBJ_TEMPLATE_LINKEND, dmMain.ProjectID, 0, aParentID);
end;



//------------------------------------------------------------------------------
function TdmTemplate_Linkend.Add(aRec: TdmTemplate_Linkend_Rec1): integer;
//------------------------------------------------------------------------------
begin
  Assert(aRec.Template_pmp_Site_ID>0, 'Value <=0');

  // with aRec do

     Result:=dmOnega_DB_data.ExecStoredProc_(SP_TEMPLATE_LINKEND_ADD,
            [
             FLD_OBJNAME,           'pmp_sector',

             FLD_NAME,              aRec.NewName,
             FLD_TEMPLATE_pmp_SITE_ID,  aRec.Template_pmp_Site_ID,


             FLD_COMBINER_ID,       IIF_NULL(aRec.Combiner_ID),
             FLD_COMBINER_LOSS,     aRec.CombinerLoss,

             FLD_CALC_MODEL_ID,     IIF_NULL(aRec.CalcModel_ID),

             FLD_CELL_LAYER_ID,     IIF_NULL(aRec.CellLayer_ID),

             FLD_LINKENDTYPE_ID,    IIF_NULL(aRec.LinkEndType_ID),

             FLD_CALC_RADIUS_km,     aRec.CalcRadius_km,

             FLD_POWER_dBm,         aRec.POWER_dBm,
                 
             'THRESHOLD',           aRec.SENSE_dBm,

             FLD_TX_FREQ_MHZ,       aRec.tx_FREQ_MHz

             ] );

  Assert(Result>0, 'Value <=0');


end;

(*
//------------------------------------------------------------------------------
function TdmTemplate_Linkend.Del (aID: integer): boolean;
//------------------------------------------------------------------------------
begin
 // dmAntTemplate.Cell_Del_Antennas (aID);
  Result:= inherited Del (aID);
end;
*)


begin
end.