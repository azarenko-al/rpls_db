unit dm_Template_Ant;

interface

uses
  SysUtils, Classes, DB, ADODB, Forms,

  dm_Onega_DB_data,

  dm_AntType,

  u_const,
  u_const_db,
  u_const_str,

  u_types,
  u_func,
  u_db,
  u_Geo,

  dm_Main,

  dm_Object_base
  ;


type

  //-------------------------------------------------------------------
  TdmTemplate_Antenna_Rec = record
  //-------------------------------------------------------------------
    NewName          : string;

    Template_Linkend_ID: integer;

    Height        : double;
    Azimuth       : double;
    Loss          : double;
    Tilt          : double;
    Freq_MHZ      : double;

    Gain          : double;
    Diameter      : double;
    Polarization  : integer;
    Vert_Width    : double;
    Horz_Width    : double;

    AntennaType_ID : Integer;
  end;


  TdmTemplate_Ant = class(TdmObject_base)
    qry_Antenna: TADOQuery;
    qry_Temp: TADOQuery;
    procedure DataModuleCreate(Sender: TObject);

  private
// TODO: GetAntTypeID
//  function GetAntTypeID(aID: integer): integer;

  public
    function Add (aRec: TdmTemplate_Antenna_Rec): integer; overload;

    procedure Del_Antenna (aID: integer);

    function  GetNewName (aCellTemplateID: integer): string;

  end;

function dmTemplate_Ant: TdmTemplate_Ant;

//=============================================================
implementation    {$R *.DFM}
//=============================================================

var
  FdmTemplate_Ant: TdmTemplate_Ant;


// ---------------------------------------------------------------
function dmTemplate_Ant: TdmTemplate_Ant;
// ---------------------------------------------------------------
begin
  if not Assigned(FdmTemplate_Ant) then
      FdmTemplate_Ant := TdmTemplate_Ant.Create(Application);

    Result := FdmTemplate_Ant;
end;

//------------------------------------------------------------------------------
procedure TdmTemplate_Ant.DataModuleCreate(Sender: TObject);
//------------------------------------------------------------------------------
begin
  inherited;

  TableName   := TBL_TEMPLATE_LINKEND_ANTENNA;
  ObjectName  := OBJ_TEMPLATE_ANT;
end;



//------------------------------------------------------------------------------
function TdmTemplate_Ant.GetNewName(aCellTemplateID: integer): string;
//------------------------------------------------------------------------------
begin
  Result := dmOnega_DB_data.Linkend_Antenna_GetNewName (OBJ_TEMPLATE_LINKEND, aCellTemplateID);
end;


//-------------------------------------------------------------------
procedure TdmTemplate_Ant.Del_Antenna (aID: integer);
//-------------------------------------------------------------------
begin
  gl_DB.DeleteRecordByID (TBL_TEMPLATE_LINKEND_ANTENNA, aID);
end;


//-------------------------------------------------------------------
function TdmTemplate_Ant.Add (aRec: TdmTemplate_Antenna_Rec): integer;
//-------------------------------------------------------------------
begin
  Assert(aRec.Template_Linkend_ID>0, 'Value <=0');

//  with aRec do
//    if (Template_Linkend_ID=0)  then
  //    raise Exception.Create('');

  if aRec.NewName='' then
    aRec.NewName:='�������';

  with aRec do
  begin   
    Result:=dmOnega_DB_data.ExecStoredProc_(SP_TEMPLATE_LINKEND_ANTENNA_ADD,

            [
             FLD_NAME, NewName,
             FLD_TEMPLATE_LINKEND_ID, Template_Linkend_ID,

             FLD_ANTENNATYPE_ID,   IIF_NULL(AntennaType_ID),

             FLD_HEIGHT,  Height,
             FLD_AZIMUTH, Azimuth,

             FLD_TILT,    Tilt,
             FLD_LOSS,    Loss

            ]);


  end;

  Assert(Result>0, 'Value <=0');
end;


end.

