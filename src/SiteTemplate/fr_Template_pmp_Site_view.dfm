inherited frame_Template_PMP_Site_View: Tframe_Template_PMP_Site_View
  Left = 1392
  Top = 334
  Width = 470
  Height = 457
  Caption = 'frame_Template_PMP_Site_View'
  FormStyle = fsStayOnTop
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited Panel1: TPanel
    Width = 462
    Height = 57
  end
  inherited pn_Caption: TPanel
    Top = 82
    Width = 462
  end
  inherited pn_Main: TPanel
    Top = 99
    Width = 462
    Height = 250
    object pn_Inspector: TPanel
      Left = 1
      Top = 1
      Width = 460
      Height = 104
      Align = alTop
      BevelOuter = bvLowered
      Caption = 'pn_Inspector'
      Constraints.MinHeight = 100
      TabOrder = 0
    end
    object cxSplitter1: TcxSplitter
      Left = 1
      Top = 128
      Width = 460
      Height = 8
      HotZoneClassName = 'TcxSimpleStyle'
      AlignSplitter = salBottom
      Control = pn_Cells
    end
    object pn_Cells: TPanel
      Left = 1
      Top = 136
      Width = 460
      Height = 113
      Align = alBottom
      BevelOuter = bvLowered
      Caption = 'pn_Cells'
      TabOrder = 2
    end
  end
  inherited MainActionList: TActionList
    Left = 8
    Top = 0
  end
  inherited ImageList1: TImageList
    Left = 36
    Top = 0
  end
  inherited PopupMenu1: TPopupMenu
    Left = 64
    Top = 0
  end
  inherited cxLookAndFeelController1: TcxLookAndFeelController
    Left = 136
    Top = 368
  end
  inherited dxBarManager1: TdxBarManager
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    Left = 64
    Top = 368
    DockControlHeights = (
      0
      0
      25
      0)
  end
  object FormStorage1: TFormStorage
    IniFileName = 'Software\Onega\'
    StoredValues = <>
    Left = 244
  end
end
