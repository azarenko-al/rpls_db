unit dm_Act_Template_PMP_Site;

interface

uses
  Classes, Menus, ActnList, Forms,

  u_DataExport_run,

  i_Audit,
  dm_User_Security,

  I_Object,
  

  dm_act_Base,

  
  u_func_msg,

  
  u_func,
  

  u_Types,
  
  u_const_str,
  u_const_db,

  

  dm_Template_pmp_Site,
  

  fr_Template_pmp_Site_view,

  d_Template_pmp_Site_add,
  d_Template_Linkend_add

  ;

type
  TdmAct_Template_PMP_Site = class(TdmAct_Base)
    act_Cell_Add: TAction;
    ActionList2: TActionList;
    act_Export_MDB: TAction;
    act_Audit: TAction;
    procedure DataModuleCreate(Sender: TObject);
  private
    function CheckActionsEnable: Boolean;
//    FBLPoint: TBLPoint;

    procedure DoAction (Sender: TObject);

  protected
    function  GetViewForm (aOwnerForm: TForm): TForm; override;
    procedure GetPopupMenu(aPopupMenu: TPopupMenu; aMenuType: TMenuType); override;

    function ItemDel(aID: integer; aName: string = ''): boolean; override;
    function ItemAdd (aFolderID: integer): integer; override;


  public
    procedure Add (aFolderID: integer); override;

    class procedure Init;
  end;


var
  dmAct_Template_PMP_Site: TdmAct_Template_PMP_Site;

//==================================================================
implementation

uses dm_Main; {$R *.dfm}
//==================================================================


//--------------------------------------------------------------------
class procedure TdmAct_Template_PMP_Site.Init;
//--------------------------------------------------------------------
begin
  Assert(not Assigned(dmAct_Template_Pmp_Site));

  Application.CreateForm(TdmAct_Template_Pmp_Site, dmAct_Template_Pmp_Site);

//  dmAct_TemplateSite:=TdmAct_TemplateSite.Create(Application);

end;

//--------------------------------------------------------------------
procedure TdmAct_Template_PMP_Site.DataModuleCreate(Sender: TObject);
//--------------------------------------------------------------------
begin
  inherited;

  ObjectName:=OBJ_TEMPLATE_PMP_SITE;

  act_Cell_Add.Caption:=STR_ADD_CELL;

  act_Export_MDB.Caption := DEF_STR_Export_MDB;
  act_Audit.Caption:=STR_Audit;


  SetActionsExecuteProc
    ([
      act_Cell_Add,
      act_Audit,
      act_Export_MDB
     ],

    DoAction);

end;

//--------------------------------------------------------------------
procedure TdmAct_Template_PMP_Site.Add (aFolderID: integer);
//--------------------------------------------------------------------
begin
  inherited Add (aFolderID);
end;

//--------------------------------------------------------------------
function TdmAct_Template_PMP_Site.ItemAdd (aFolderID: integer): integer;
//--------------------------------------------------------------------
begin
  Result:=Tdlg_Template_Pmp_Site_add.ExecDlg (aFolderID);
end;

//--------------------------------------------------------------------
function TdmAct_Template_PMP_Site.ItemDel(aID: integer; aName: string = ''):
    boolean;
//--------------------------------------------------------------------
begin
  Result:=dmTemplate_Pmp_Site.Del (aID);
end;

//--------------------------------------------------------------------
function TdmAct_Template_PMP_Site.GetViewForm (aOwnerForm: TForm): TForm;
//--------------------------------------------------------------------
begin
  Result:=Tframe_Template_Pmp_Site_View.Create(aOwnerForm);
end;



// ---------------------------------------------------------------
function TdmAct_Template_PMP_Site.CheckActionsEnable: Boolean;
// ---------------------------------------------------------------
begin
  Result:= dmUser_Security.Modify_lib in [mtAllow, mtAllowMy];

  SetActionsEnabled1( [
          act_Del_,
          act_Del_list,
//          act_Copy,

          act_Cell_Add
        //  act_Export_MDB,
      ],

   Result );


  //  TModify_lib = (mtAllow=1, mtAllowMy=2, mtForbidden=0 );


end;


//--------------------------------------------------------------------
procedure TdmAct_Template_PMP_Site.GetPopupMenu(aPopupMenu: TPopupMenu; aMenuType: TMenuType);
//--------------------------------------------------------------------
var
  bEnable: Boolean;
begin
  bEnable:=CheckActionsEnable();


  case aMenuType of
    mtFolder: begin
                AddFolderMenu_Create (aPopupMenu, bEnable);
                AddFolderPopupMenu (aPopupMenu, bEnable);
              end;
    mtItem:   begin
                AddMenuItem (aPopupMenu, act_Cell_Add);
                AddMenuItem (aPopupMenu, nil);
                AddMenuItem (aPopupMenu, act_Del_);

                AddFolderMenu_Tools (aPopupMenu);
                AddMenuItem (aPopupMenu, act_Export_MDB);
                AddMenuItem (aPopupMenu, act_Audit);

              end;
    mtList:   begin
                AddMenuItem (aPopupMenu, act_Del_list);
                AddFolderMenu_Tools (aPopupMenu);
                AddMenuItem (aPopupMenu, act_Export_MDB);

              end;
  end;
end;


//--------------------------------------------------------------------
// actions
//--------------------------------------------------------------------
procedure TdmAct_Template_PMP_Site.DoAction (Sender: TObject);
begin
  if Sender=act_Export_MDB then
//    TDataExport_run.ExportToRplsDbGuides1 (OBJ_ANTENNA_TYPE);
    TDataExport_run.ExportToRplsDb_selected (TBL_Template_PMP_Site, FSelectedIDList);


  if Sender=act_Audit then
    Init_IAuditX.ExecDlg( dmMain.GetConnectionString, TBL_Template_PMP_Site, FFocusedID) else



  if Sender=act_Cell_Add then
  begin
    Tdlg_Template_Linkend_add.ExecDlg (FFocusedID); //, oBS_Type);
  end;


  g_EventManager.postEvent_ (WM_REFRESH_SITE, []);
//  PostWMsg (WM_REFRESH_SITE);

end;



end.