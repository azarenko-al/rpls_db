program test_SiteTemplate;

uses
  a_Unit in 'a_Unit.pas' {Form1},
  d_Template_Antenna_add in '..\d_Template_Antenna_add.pas' {dlg_Template_Antenna_add},
  d_Template_Linkend_add in '..\d_Template_Linkend_add.pas' {dlg_Template_Linkend_add},
  d_Template_PMP_site_add in '..\d_Template_PMP_site_add.pas' {dlg_Template_PMP_site_add},
  dm_act_Base in '..\..\Common\dm_act_Base.pas' {dmAct_Base: TDataModule},
  dm_Act_Template_Linkend in '..\dm_Act_Template_Linkend.pas' {dmAct_Template_Linkend: TDataModule},
  dm_Act_Template_Linkend_Antenna in '..\dm_Act_Template_Linkend_Antenna.pas' {dmAct_Template_Linkend_Antenna: TDataModule},
  dm_Act_Template_PMP_Site in '..\dm_Act_Template_PMP_Site.pas' {dmAct_Template_PMP_Site: TDataModule},
  dm_Template_Site_linkend_Ant in '..\..\Template_Site\dm_Template_Site_linkend_Ant.pas' {dmTemplate_Site_Linkend_Antenna},
  dm_Template_Linkend in '..\dm_Template_Linkend.pas',
  dm_Template_PMP_Site in '..\dm_Template_PMP_Site.pas',
  Forms,
  fr_Template_Linkend_Antenna_view in '..\fr_Template_Linkend_Antenna_view.pas' {frame_Template_Linkend_Antenna_view},
  fr_Template_Linkend_inspector in '..\fr_Template_Linkend_inspector.pas',
  fr_Template_Linkend_view in '..\fr_Template_Linkend_view.pas' {frame_Template_Linkend_view},
  fr_Template_PMP_Site_Cells in '..\fr_Template_PMP_Site_Cells.pas' {frame_SiteTemplate_Cells},
  fr_Template_PMP_Site_View in '..\fr_Template_PMP_Site_View.pas' {frame_Template_PMP_Site_View},
  fr_View_base in '..\..\Explorer\fr_View_base.pas' {frame_View_Base},
  ShareMem,
  u_SiteTemplate_types in '..\u_SiteTemplate_types.pas',
  u_types in '..\..\u_types.pas';

{$R *.RES}

 

begin
  Application.Initialize;
  Application.CreateForm(TForm1, Form1);
  Application.Run;
end.
