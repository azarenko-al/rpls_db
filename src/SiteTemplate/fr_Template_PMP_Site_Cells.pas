unit fr_Template_PMP_Site_Cells;

interface

uses
  Windows, SysUtils, Classes, Controls, Forms,
  ADODB, cxStyles, cxTL,
   Variants,

  cxDBTL, DB, Menus, ActnList, 
  


 // I_Act_Explorer, //dm_Act_Explorer,

  dm_Onega_DB_data,

  u_storage,

  dm_LinkEndType,



  
  

//  I_LinkEndType,
  dm_Act_LinkEndType,

  

  
  u_const_str,
  u_const_db,
  

  u_db,

  u_cx,
  u_cx_treeList,

  u_func,
  
  


  

  u_types,
 // d_expl_object_Edit,

  d_Template_Linkend_add,
  d_Template_Antenna_add,


  

  dm_Template_Linkend,
  dm_Template_Ant, dxBar, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxCustomData, cxMaskEdit, cxButtonEdit,
  cxTLdxBarBuiltInMenu, cxClasses, cxInplaceContainer, cxTLData;

 type
  Tframe_SiteTemplate_Cells = class(TForm)
    ActionList1: TActionList;
    act_Edit: TAction;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    act_Add_cell: TAction;
    act_Add_Antenna: TAction;
    act_Del: TAction;
    N2: TMenuItem;
    N3: TMenuItem;
    N4: TMenuItem;
    N5: TMenuItem;
    DataSource2: TDataSource;
    ADOStoredProc1: TADOStoredProc;
    cxDBTreeList1: TcxDBTreeList;
    col_name: TcxDBTreeListColumn;
    col_id: TcxDBTreeListColumn;
    col_objname: TcxDBTreeListColumn;
    col_cell_layer_name: TcxDBTreeListColumn;
    col_LinkEndType_Name: TcxDBTreeListColumn;
    col_antennatype_name: TcxDBTreeListColumn;
    col_calc_radius_km: TcxDBTreeListColumn;
    col_THRESHOLD_BER_3: TcxDBTreeListColumn;
    col_THRESHOLD_BER_6: TcxDBTreeListColumn;
    col_POWER_dBm: TcxDBTreeListColumn;
    col_TX_FREQ_MHz: TcxDBTreeListColumn;
    col_combiner_name: TcxDBTreeListColumn;
    col_combiner_loss_dB: TcxDBTreeListColumn;
    col_Antenna_Height: TcxDBTreeListColumn;
    col_POLARIZATION_str: TcxDBTreeListColumn;
    col_Antenna_Azimuth: TcxDBTreeListColumn;
    col_Antenna_tilt: TcxDBTreeListColumn;
    col_antenna_loss_dB: TcxDBTreeListColumn;
    col_calc_model_Name: TcxDBTreeListColumn;
    col_k0: TcxDBTreeListColumn;
    col_k0_open: TcxDBTreeListColumn;
    col_k0_closed: TcxDBTreeListColumn;
    col_Band: TcxDBTreeListColumn;
    cxStyleRepository1: TcxStyleRepository;
    cxStyle1: TcxStyle;
    dxBarManager1: TdxBarManager;
    dxBarManager1Bar1: TdxBar;
    dxBarButton1: TdxBarButton;
    dxBarSubItem1: TdxBarSubItem;
    dxBarButton2: TdxBarButton;
    dxBarButton3: TdxBarButton;
    dxBarButton4: TdxBarButton;
    procedure FormCreate(Sender: TObject);
    procedure act_EditExecute(Sender: TObject);
    procedure ActionList1Update(Action: TBasicAction; var Handled: Boolean);
//    procedure act_DelUpdate(Sender: TObject);
//    procedure ApplicationEvents1Message(var Msg: tagMSG; var Handled: Boolean);
    procedure FormDestroy(Sender: TObject);
    procedure col_ButtonClick(Sender: TObject;
      AbsoluteIndex: Integer);

 //   procedure dxDBTreeEditing(Sender: TObject; Node: TdxTreeListNode;
//      var Allow: Boolean);
  
    procedure ADOQuery1AfterPost(DataSet: TDataSet);
  //  procedure Button1Click(Sender: TObject);
    procedure dxDBTreeKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure col_LinkEndType_Name_PropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure cxDBTreeList1Editing(Sender: TcxCustomTreeList; AColumn: TcxTreeListColumn; var
        Allow: Boolean);



    procedure cxDBTreeList1KeyDown(Sender: TObject; var Key: Word; Shift:
        TShiftState);
  //  procedure dxDBTreeHotTrackNode(Sender: TObject;  AHotTrackInfo: TdxTreeListHotTrackInfo; var ACursor: TCursor);
  private
    FRegPath : string;

    FDataSet: TDataSet;

    FSiteTemplateID: integer;
//    FUpdated: boolean;
    FRecNo: Integer;
  public
    procedure View(aID: Integer = 0);
  end;


//==================================================================
implementation {$R *.dfm}
//==================================================================

uses
  dm_Act_Explorer;

const

  INDEX_CELL        = 1;
  INDEX_TRX         = 2;
  INDEX_ANTENNA     = 3;
  INDEX_CALC_MODEL  = 4;
  INDEX_COMBINER    = 5;
  


//-------------------------------------------------------------------
procedure Tframe_SiteTemplate_Cells.FormCreate(Sender: TObject);
//-------------------------------------------------------------------
begin
  inherited;

  cx_Validate_Tree (cxDBTreeList1);


  cxDBTreeList1.OnKeyDown:=cxDBTreeList1KeyDown;
  cxDBTreeList1.OnEditing:=cxDBTreeList1Editing;

  FRegPath := g_Storage.GetPathByClass(ClassName);

  //zzz:=FRegPath+'3';

 // Assert(Assigned(IShell));
//  Assert(Assigned(ICalcModel));


(*//  col_Type.Tag:= 1;
  col_ID.Tag  := 1;
  col_tree_id.Tag  := 1;
  col_tree_parent_id.Tag  := 1;
*)

//  col_Combiner_Channels.tag:=1;

  cxDBTreeList1.Align := alClient;
//  GSPages1.Align := alClient;

  cxDBTreeList1.Bands[INDEX_CELL].Caption.Text      := STR_CELL;
  cxDBTreeList1.Bands[INDEX_TRX].Caption.Text       := STR_TRX;
  cxDBTreeList1.Bands[INDEX_ANTENNA].Caption.Text   := STR_ANTENNA;
  cxDBTreeList1.Bands[INDEX_COMBINER].Caption.Text  := STR_COMBINER;

 // act_setup_TreeList.Caption:= STR_SETUP_TABLE;

  col_Name.Caption.Text           := STR_NAME;
  col_Calc_Radius_km.Caption.Text    := STR_CALC_RADIUS_km; // '������ ������� [km]'; //'������ [v]';

//  col_THRESHOLD_BER_3.Caption := STR_THRESHOLD_BER_3;
 // col_THRESHOLD_BER_6.Caption := STR_THRESHOLD_BER_6;

  col_Power_dBm.Caption.Text  := STR_TRANS_POWER_DBM;
  col_TX_FREQ_MHz.Caption.Text:= STR_TRANS_FREQ_MHz;

  col_LinkEndType_Name.Caption.Text:= STR_TYPE;
  col_cell_layer_name.Caption.Text:= STR_CELL_LAYER_NAME;

  col_combiner_loss_dB.Caption.Text  := STR_LOSS;
  col_Combiner_Name.Caption.Text  := STR_TYPE;

  col_k0_Closed.Caption.Text      := STR_K0_CLOSED;
  col_k0_open.Caption.Text        := STR_K0_OPEN;
  col_k0.Caption.Text             := STR_K0;

  col_AntennaType_Name.Caption.Text:= STR_TYPE;
  col_Antenna_Height.Caption.Text := STR_HEIGHT;
  col_Antenna_Azimuth.Caption.Text:= STR_AZIMUTH;
  col_Antenna_tilt.Caption.Text   := STR_TILT;
  col_Antenna_Loss_dB.Caption.Text:= STR_ANTENNA_LOSS;

  col_THRESHOLD_BER_3.Caption.Text   := STR_THRESHOLD_BER_3;
  col_THRESHOLD_BER_6.Caption.Text   := STR_THRESHOLD_BER_6;


  // ---------------------------------------------------------------

  cxDBTreeList1.Bands[INDEX_CELL].Caption.Text      := STR_CELL;
  cxDBTreeList1.Bands[INDEX_TRX].Caption.Text       := STR_TRX;
  cxDBTreeList1.Bands[INDEX_ANTENNA].Caption.Text   := STR_ANTENNA;
  cxDBTreeList1.Bands[INDEX_COMBINER].Caption.Text  := STR_COMBINER;

 // act_setup_TreeList.Caption:= STR_SETUP_TABLE;

  col_Name.Caption.Text           := STR_NAME;
  col_Calc_Radius_km.Caption.Text := STR_CALC_RADIUS_km; // '������ ������� [km]'; //'������ [v]';

//  col_THRESHOLD_BER_3.Caption := STR_THRESHOLD_BER_3;
 // col_THRESHOLD_BER_6.Caption := STR_THRESHOLD_BER_6;

  col_Power_dBm.Caption.Text  := STR_TRANS_POWER_DBM;
  col_TX_FREQ_MHz.Caption.Text:= STR_TRANS_FREQ_MHz;

  col_LinkEndType_Name.Caption.Text:= STR_TYPE;
  col_cell_layer_name.Caption.Text:= STR_CELL_LAYER_NAME;

  col_combiner_loss_dB.Caption.Text  := STR_LOSS;
  col_Combiner_Name.Caption.Text  := STR_TYPE;

  col_k0_Closed.Caption.Text      := STR_K0_CLOSED;
  col_k0_open.Caption.Text        := STR_K0_OPEN;
  col_k0.Caption.Text             := STR_K0;

  col_AntennaType_Name.Caption.Text:= STR_TYPE;
  col_Antenna_Height.Caption.Text := STR_HEIGHT;
  col_Antenna_Azimuth.Caption.Text:= STR_AZIMUTH;
  col_Antenna_tilt.Caption.Text   := STR_TILT;
  col_Antenna_Loss_dB.Caption.Text:= STR_ANTENNA_LOSS;

  col_THRESHOLD_BER_3.Caption.Text   := STR_THRESHOLD_BER_3;
  col_THRESHOLD_BER_6.Caption.Text   := STR_THRESHOLD_BER_6;


  //cxDBTreeList1.Columns[1].DataBinding.FieldName;


  cxDBTreeList1.RestoreFromRegistry (FRegPath + cxDBTreeList1.Name);

 // dmSiteTemplate_View.InitDB (mem_Data);

  FDataSet := ADOStoredProc1; // mem_Data;


 // dx_CheckColumnSizes_DBTreeList (dxDBTree);
end;

//-------------------------------------------------------------------
procedure Tframe_SiteTemplate_Cells.FormDestroy(Sender: TObject);
//-------------------------------------------------------------------
begin
  cxDBTreeList1.StoreToRegistry (FRegPath+ cxDBTreeList1.Name);

  inherited;
end;

//-------------------------------------------------------------------
procedure Tframe_SiteTemplate_Cells.View(aID: Integer = 0);
//-------------------------------------------------------------------
var
  i: Integer;
begin
  if aID>0 then
    FSiteTemplateID := aID;

  i:=dmOnega_DB_data.Template_PMP_Site_select_item(ADOStoredProc1, FSiteTemplateID);

//  db_View (ADOStoredProc1);


 // dxDBTree.FullExpand;
  cxDBTreeList1.FullExpand;

  db_SetFieldCaptions(ADOStoredProc1,
     [
     // SArr(FLD_NAME, '��������'),
        FLD_POLARIZATION_str,  '�����������'
     ]);

end;


//-------------------------------------------------------------------
procedure Tframe_SiteTemplate_Cells.act_EditExecute(Sender: TObject);
//-------------------------------------------------------------------
var
  iID: Integer;
  sObjName: string;

begin
  sObjName:=FDataSet.FieldBYName(FLD_ObjName).AsString;
  iID     :=FDataSet.FieldBYName(FLD_ID).AsInteger;


  //---------------------------------
  if Sender = act_Add_Cell then
  //---------------------------------
  begin
    iID:= Tdlg_Template_Linkend_add.ExecDlg (FSiteTemplateID);

    if iID > 0 then
      View();
  end else

  //---------------------------------
  if Sender = act_Add_Antenna then begin
  //---------------------------------
    if Tdlg_Template_Antenna_add.ExecDlg(iID)>0 then
      View();

  end else

  //---------------------------------
  if Sender = act_Del then begin
  //---------------------------------
    if FDataSet.RecordCount=0 then
      Exit;


    if Eq(sObjName, OBJ_PMP_SECTOR) then
    begin
      if dmTemplate_Linkend.Del(iID) then
        View();
    end else

    if Eq(sObjName, OBJ_LINKEND_ANTENNA) or Eq(sObjName, 'ANTENNA')
      then
    begin
      if dmTemplate_Ant.Del(iID) then
        View();
    end else

      raise Exception.Create('');

  ////  View(FSiteTemplateID);
  end;
end;

//-------------------------------------------------------------------
procedure Tframe_SiteTemplate_Cells.ActionList1Update(Action: TBasicAction; var Handled: Boolean);
//-------------------------------------------------------------------
var sObjName: string;
begin
//  sObjName := mem_Data.FieldByName(FLD_TYPE).AsString;

  sObjName:=FDataSet.FieldBYName(FLD_ObjName).AsString;


//  act_Edit.Enabled := Eq(sObjName <> '';
 // act_Del.Enabled  := act_Edit.Enabled;

//  act_Add_cell.Enabled := (sObjName = '');

  act_Add_Antenna.Enabled := Eq(sObjName, OBJ_PMP_SECTOR);

end;


//-------------------------------------------------------------------
procedure Tframe_SiteTemplate_Cells.col_ButtonClick(Sender: TObject;
  AbsoluteIndex: Integer);
//-------------------------------------------------------------------
var
  iID,
  iLINKENDTYPE_ID,
  iAntTypeID, iCellLayerID, iCombinerID: integer;

  sName: Widestring;
  sName1: string;
  eLoss,K0, k0_Open, k0_Closed: double;
  iCalcModelID: Integer;
  sObjName: string;

  mode_rec: TdmLinkEndTypeModeInfoRec;

begin
  iID     := FDataSet.FieldByName(FLD_ID).AsInteger;
  sObjName:=FDataSet.FieldBYName(FLD_ObjName).AsString;


//  FRecNo:= FDataSet.RecNo;


  //--------------------------------
  if Sender=col_AntennaType_Name then
  //--------------------------------
    if Eq(sObjName,OBJ_LINKEND_ANTENNA) or
       Eq(sObjName,'ANTENNA')
  then
  begin
    iAntTypeID:= FDataSet.FieldBYName(FLD_ANTENNATYPE_ID).AsInteger ;

    if dmAct_Explorer.Dlg_Select_Object (otAntennaType, iAntTypeID, sName) then
    begin
      dmOnega_DB_data.Object_Update_AntType('Template_Antenna', iID, iAntTypeID);

      View();
    end;
  end;

  //--------------------------------
  if Sender=col_LinkEndType_Name then
  //--------------------------------
    if Eq(sObjName, OBJ_PMP_SECTOR) then
  begin
    iLINKENDTYPE_ID:= FDataSet.FieldBYName(FLD_LINKENDTYPE_ID).AsInteger;

    if dmAct_Explorer.Dlg_Select_Object (otLinkEndType, iLINKENDTYPE_ID, sName) then
      if dmAct_LinkEndType.Dlg_GetMode (iLINKENDTYPE_ID, 0, mode_rec) then  //iMode, iLinkEndType_Mode_ID,
      begin
        dmOnega_DB_data.Object_Update_LinkEndType
            (OBJ_Template_LinkEnd, iID, iLINKENDTYPE_ID, mode_rec.ID);

        View();
      end;
  end;

  //--------------------------------
  if Sender=col_cell_layer_name then
  //--------------------------------
    if Eq(sObjName,OBJ_PMP_SECTOR) then
  begin
    iCellLayerID:= FDataSet.FieldBYName(FLD_CELL_LAYER_ID).AsInteger ;

    if dmAct_Explorer.Dlg_Select_Object (otCellLayer, iCellLayerID, sName) then
    begin
      dmTemplate_Linkend.Update_(iID, [db_par(FLD_CELL_LAYER_ID, iCellLayerID)]);
      View();
    end;
  end;


  //--------------------------------
  if Sender=col_Calc_Model_Name then
  //--------------------------------
    if Eq(sObjName,OBJ_PMP_SECTOR) then
  begin
    case AbsoluteIndex of
   {   1: begin
        dmCellTemplate.Update(iID, [db_par(FLD_CALC_MODEL_ID, NULL),
                                    db_par(FLD_K0,            NULL),
                                    db_par(FLD_K0_OPEN,       NULL),
                                    db_par(FLD_K0_CLOSED,     NULL)  ]);
        View(FSiteTemplateID);
      end; // 1
                 }
      0:  begin
            iCalcMOdelID := FDataSet.FieldBYName(FLD_CALC_MODEL_ID).AsInteger ;


            if dmAct_Explorer.Dlg_Select_Object (otCalcMOdel, iCalcMOdelID, sName) then
            begin
              dmOnega_DB_data.Object_Update_CalcModel(OBJ_Template_LinkEnd, iID, iCalcMOdelID);

              View();
            end;
        end;

    end;
  end;


  //--------------------------------
  if Sender=col_Combiner_Name then
  //--------------------------------
    if Eq(sObjName, OBJ_PMP_SECTOR) then
  begin
    case AbsoluteIndex of
    {  1: begin
        dmCellTemplate.Update(iID, [db_par(FLD_COMBINER_ID,   NULL),
                                    db_par(FLD_COMBINER_LOSS, NULL)   ]);

        View(FSiteTemplateID);
      end;      }

      0: begin
          iCombinerID:= FDataSet.FieldByName(FLD_COMBINER_ID).AsInteger ;

          if dmAct_Explorer.Dlg_Select_Object (otCombiner, iCombinerID, sName) then
          begin
            dmOnega_DB_data.Object_Update_Combiner(OBJ_Template_LinkEnd, iID, iCalcMOdelID);

            View();
        end;
    end;
    end; // case
  end;

//  FDataSet.RecNo:= FRecNo;
end;


//-------------------------------------------------------------------
procedure Tframe_SiteTemplate_Cells.dxDBTreeKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
//-------------------------------------------------------------------
var
  sObjName: string;
  iID: integer;
  k: Integer;
begin
//  sObjName:=mem_Data.FieldBYName(FLD_type).AsString;
{
  sObjName:=FDataSet.FieldBYName(FLD_ObjName).AsString;
  iID:=FDataSet.FieldBYName(FLD_ID).AsInteger;


  if ((Key = VK_DELETE) or (Key = VK_BACK)) and
     Eq(sObjName, OBJ_PMP_SECTOR) then
  begin
    if (dxDBTree.FocusedAbsoluteIndex = col_CalcModelName.Index) then
      if (dxDBTree.FocusedNode.Values[col_CalcModelName.Index] <> '') then
      begin
        FRecNo:= FDataSet.RecNo;

        k:=dmOnega_DB_data.Object_Update_CalcModel(OBJ_Template_LinkEnd, iID, 0);


        View();

        FDataSet.RecNo:= FRecNo;
      end;


    if (dxDBTree.FocusedAbsoluteIndex = col_Combiner_Name.Index) then
      if (dxDBTree.FocusedNode.Values[col_Combiner_Name.Index] <> '') then
      begin
        FRecNo:= FDataSet.RecNo;

        dmTemplate_Linkend.Update_(iID,
             [db_par(FLD_COMBINER_ID,   NULL),
              db_par(FLD_COMBINER_LOSS, NULL) ]);

        View();

        FDataSet.RecNo:= FRecNo;
      end;
  end;
  }

end;




//-------------------------------------------------------------------
procedure Tframe_SiteTemplate_Cells.ADOQuery1AfterPost(DataSet: TDataSet);
//-------------------------------------------------------------------
var
  iID: Integer;
  k: Integer;
  s: string;
begin
  inherited;

  iID:=DataSet[FLD_ID];
  s:=DataSet[FLD_OBJNAME];


  k:=dmOnega_DB_data.ExecStoredProc (sp_Template_PMP_Site_update_item,
        [
          db_Par(FLD_ID,      DataSet[FLD_ID]),
          db_Par(FLD_OBJNAME, DataSet[FLD_OBJNAME]),




          //cector
        //  db_Par(FLD_COLOR,     DataSet[FLD_COLOR]),
          db_Par(FLD_Calc_Radius_km, DataSet[FLD_Calc_Radius_km]),

          db_Par(FLD_THRESHOLD_BER_3,  DataSet[FLD_THRESHOLD_BER_3]),
          db_Par(FLD_THRESHOLD_BER_6,  DataSet[FLD_THRESHOLD_BER_6]),


          db_Par(FLD_POWER_dBm,  DataSet[FLD_POWER_dBm]),
          db_Par(FLD_TX_FREQ_MHZ,DataSet[FLD_TX_FREQ_MHZ]),

          db_Par(FLD_COMBINER_LOSS,  DataSet[FLD_COMBINER_LOSS]),

          db_Par(FLD_K0,        DataSet[FLD_K0]),
          db_Par(FLD_K0_OPEN,   DataSet[FLD_K0_OPEN]),
          db_Par(FLD_K0_CLOSED, DataSet[FLD_K0_CLOSED]),


          //antenna
          db_Par(FLD_HEIGHT,  DataSet[FLD_HEIGHT]),
          db_Par(FLD_AZIMUTH, DataSet[FLD_AZIMUTH]),
          db_Par(FLD_TILT,    DataSet[FLD_TILT]),
          db_Par(FLD_ANTENNA_LOSS,    DataSet[FLD_ANTENNA_LOSS])
         ]);

end;


// ---------------------------------------------------------------
procedure Tframe_SiteTemplate_Cells.col_LinkEndType_Name_PropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
// ---------------------------------------------------------------

var
  iID,
  iLINKENDTYPE_ID,
  iAntTypeID, iCellLayerID, iCombinerID: integer;

  sName: Widestring;
  sName1: string;
  eLoss,K0, k0_Open, k0_Closed: double;
  iCalcModelID: Integer;
  sObjName: string;

  mode_rec: TdmLinkEndTypeModeInfoRec;
  s: string;

begin
  iID     := FDataSet.FieldByName(FLD_ID).AsInteger;
  sObjName:=FDataSet.FieldBYName(FLD_ObjName).AsString;


  s:=Sender.ClassName;


//  FRecNo:= FDataSet.RecNo;


//  if (cxDBTreeList1.FocusedColumn = col__calc_model_name) then


  //--------------------------------
  if cxDBTreeList1.FocusedColumn = col_AntennaType_Name then
  //--------------------------------
    if Eq(sObjName,OBJ_LINKEND_ANTENNA) or
       Eq(sObjName,'ANTENNA')
  then
  begin
    iAntTypeID:= FDataSet.FieldBYName(FLD_ANTENNATYPE_ID).AsInteger ;

    if dmAct_Explorer.Dlg_Select_Object (otAntennaType, iAntTypeID, sName) then
    begin
      dmOnega_DB_data.Object_Update_AntType('Template_Antenna', iID, iAntTypeID);

      View();
    end;
  end;

  //--------------------------------
  if cxDBTreeList1.FocusedColumn = col_LinkEndType_Name then
  //--------------------------------
    if Eq(sObjName, OBJ_PMP_SECTOR) then
  begin
    iLINKENDTYPE_ID:= FDataSet.FieldBYName(FLD_LINKENDTYPE_ID).AsInteger;

    if dmAct_Explorer.Dlg_Select_Object (otLinkEndType, iLINKENDTYPE_ID, sName) then
      if dmAct_LinkEndType.Dlg_GetMode (iLINKENDTYPE_ID, 0, mode_rec) then  //iMode, iLinkEndType_Mode_ID,
      begin
        dmOnega_DB_data.Object_Update_LinkEndType
            (OBJ_Template_LinkEnd, iID, iLINKENDTYPE_ID, mode_rec.ID);

        View();
      end;
  end;

  //--------------------------------
  if cxDBTreeList1.FocusedColumn = col_cell_layer_name then
  //--------------------------------
    if Eq(sObjName,OBJ_PMP_SECTOR) then
  begin
    iCellLayerID:= FDataSet.FieldBYName(FLD_CELL_LAYER_ID).AsInteger ;

    if dmAct_Explorer.Dlg_Select_Object (otCellLayer, iCellLayerID, sName) then
    begin
      dmTemplate_Linkend.Update_(iID, [db_par(FLD_CELL_LAYER_ID, iCellLayerID)]);
      View();
    end;
  end;


  //--------------------------------
  if cxDBTreeList1.FocusedColumn=col_Calc_Model_Name then
  //--------------------------------
    if Eq(sObjName,OBJ_PMP_SECTOR) then
  begin
    case AButtonIndex of
   {   1: begin
        dmCellTemplate.Update(iID, [db_par(FLD_CALC_MODEL_ID, NULL),
                                    db_par(FLD_K0,            NULL),
                                    db_par(FLD_K0_OPEN,       NULL),
                                    db_par(FLD_K0_CLOSED,     NULL)  ]);
        View(FSiteTemplateID);
      end; // 1
                 }
      0:  begin
            iCalcMOdelID := FDataSet.FieldBYName(FLD_CALC_MODEL_ID).AsInteger ;


            if dmAct_Explorer.Dlg_Select_Object (otCalcMOdel, iCalcMOdelID, sName) then
            begin
              dmOnega_DB_data.Object_Update_CalcModel(OBJ_Template_LinkEnd, iID, iCalcMOdelID);

              View();
            end;
        end;

    end;
  end;


  //--------------------------------
  if cxDBTreeList1.FocusedColumn=col_Combiner_Name then
  //--------------------------------
    if Eq(sObjName, OBJ_PMP_SECTOR) then
  begin
    case AButtonIndex of
    {  1: begin
        dmCellTemplate.Update(iID, [db_par(FLD_COMBINER_ID,   NULL),
                                    db_par(FLD_COMBINER_LOSS, NULL)   ]);

        View(FSiteTemplateID);
      end;      }

      0: begin
          iCombinerID:= FDataSet.FieldByName(FLD_COMBINER_ID).AsInteger ;

          if dmAct_Explorer.Dlg_Select_Object (otCombiner, iCombinerID, sName) then
          begin
            dmOnega_DB_data.Object_Update_Combiner(OBJ_Template_LinkEnd, iID, iCombinerID);

            View();
        end;
    end;
    end; // case
  end

  else
    Assert(1<>1, Sender.ClassName);


//  FDataSet.RecNo:= FRecNo;
end;

//-------------------------------------------------------------------
procedure Tframe_SiteTemplate_Cells.cxDBTreeList1Editing(Sender: TcxCustomTreeList;
    AColumn: TcxTreeListColumn; var Allow: Boolean);
//-------------------------------------------------------------------
(*   function cx_SetColumnReadOnly (aColumns: array of TcxDBTreeListColumn; aReadOnly : boolean): TcxDBTreeListColumn;
   var
     I: Integer;
   begin
     for I := 0 to High(aColumns) - 1 do
       aColumns[i].Options.Editing := not aReadOnly;
   end;
*)

var b: boolean;
  sObjName: string;
begin
  Exit;


//  sObjName:=mem_Data.FieldBYName(FLD_type).AsString;
  sObjName:=FDataSet.FieldBYName(FLD_ObjName).AsString;

  // -------------------------------------------------------------------

  b:=not Eq(sObjName,OBJ_PMP_SECTOR);


  cx_SetColumnReadOnly (
    [

      col_Calc_Radius_km,
      col_LinkEndType_Name,

      col_cell_layer_name,
      col_Combiner_Name,
      col_combiner_loss_dB,

      col_calc_model_Name,
      col_k0,
      col_k0_open,
      col_k0_closed,

      col_ThRESHOLD_BER_3,
      col_ThRESHOLD_BER_6,
      col_TX_FREQ_MHz,
      col_Power_dBm


  ], b);
 {
  col_Calc_Radius_km.Properties.ReadOnly :=  b;
  col_LinkEndType_Name.Properties.ReadOnly:= b;
//  col_Cell_Calc_Model.DisableEditor:=  b;
  col_cell_layer_name.Properties.ReadOnly:=  b;
  col_Combiner_Name.Properties.ReadOnly:=    b;
  col_combiner_loss_dB.Properties.ReadOnly:=    b;

  col_calc_model_Name.Properties.ReadOnly:=    b;
  col_k0.Properties.ReadOnly:=               b;
  col_k0_open.Properties.ReadOnly:=          b;
  col_k0_closed.Properties.ReadOnly:=        b;

  col_ThRESHOLD_BER_3.Properties.ReadOnly:=  b;
  col_ThRESHOLD_BER_6.Properties.ReadOnly:=  b;
  col_TX_FREQ_MHz.Properties.ReadOnly :=     b;
  col_Power_dBm.Properties.ReadOnly:=        b;
 }

  // -------------------------------------------------------------------

  b:=not (Eq(sObjName,OBJ_LINKEND_ANTENNA) or Eq(sObjName,'ANTENNA'));


  cx_SetColumnReadOnly (
    [
     col_AntennaType_Name,
     col_Antenna_Height,
     col_Antenna_Azimuth,
     col_Antenna_tilt,
     col_Antenna_Loss_db
    ], b);

     {
  col_AntennaType_Name.Properties.ReadOnly:= b;
  col_Antenna_Height.Properties.ReadOnly  := b;
  col_Antenna_Azimuth.Properties.ReadOnly := b;
  col_Antenna_tilt.Properties.ReadOnly    := b;
  col_Antenna_Loss_db.Properties.ReadOnly := b;
   }

end;

// ---------------------------------------------------------------
procedure Tframe_SiteTemplate_Cells.cxDBTreeList1KeyDown(Sender: TObject; var
    Key: Word; Shift: TShiftState);
// ---------------------------------------------------------------
var
  sObjName: string;
  iID: integer;
  k: Integer;
begin
//  sObjName:=mem_Data.FieldBYName(FLD_type).AsString;

  sObjName:=FDataSet.FieldBYName(FLD_ObjName).AsString;
  iID:=FDataSet.FieldBYName(FLD_ID).AsInteger;


  if ((Key = VK_DELETE) or (Key = VK_BACK)) and
     Eq(sObjName, OBJ_PMP_SECTOR) then
  begin
    if (cxDBTreeList1.FocusedColumn = col_Calc_model_Name) then
      if (cxDBTreeList1.FocusedNode.Values[col_Calc_model_Name.ItemIndex] <> '') then
      begin
        FRecNo:= FDataSet.RecNo;

        k:=dmOnega_DB_data.Object_Update_CalcModel(OBJ_Template_LinkEnd, iID, 0);

        View();

        FDataSet.RecNo:= FRecNo;
      end;


    if (cxDBTreeList1.FocusedColumn = col_Combiner_Name) then
      if (cxDBTreeList1.FocusedNode.Values[col_Combiner_Name.ItemIndex] <> '') then
      begin
        FRecNo:= FDataSet.RecNo;

        dmTemplate_Linkend.Update_(iID,
             [db_par(FLD_COMBINER_ID,   NULL),
              db_par(FLD_COMBINER_LOSS, NULL) ]);

        View();

        FDataSet.RecNo:= FRecNo;
      end;
  end;


end;

end.

  {

//-------------------------------------------------------------------
procedure Tframe_SiteTemplate_Cells.dxDBTreeEditing(Sender: TObject;
  Node: TdxTreeListNode; var Allow: Boolean);
//-------------------------------------------------------------------
   function cx_HideColumns (aColumns: array of TcxDBTreeListColumn): TcxDBTreeListColumn;
   var
     I: Integer;
   begin
     for I := 0 to High(aColumns) - 1 do

     begin

     end;
   end;   


var b: boolean;
  sObjName: string;
begin
//  sObjName:=mem_Data.FieldBYName(FLD_type).AsString;
  sObjName:=FDataSet.FieldBYName(FLD_ObjName).AsString;

  // -------------------------------------------------------------------

  b:=not Eq(sObjName,OBJ_PMP_SECTOR);


  cx_HideColumns (
  [
  
 col_Calc_Radius_km,
 col_LinkEndType_Name,

 col_cell_layer_name,
 col_Combiner_Name,
 col_combiner_loss_dB

  ]);

  col_Calc_Radius_km.Properties.ReadOnly :=  b;
  col_LinkEndType_Name.Properties.ReadOnly:= b;
//  col_Cell_Calc_Model.DisableEditor:=  b;
  col_cell_layer_name.Properties.ReadOnly:=  b;
  col_Combiner_Name.Properties.ReadOnly:=    b;
  col_combiner_loss_dB.Properties.ReadOnly:=    b;

  col_calc_model_Name.Properties.ReadOnly:=    b;
  col_k0.Properties.ReadOnly:=               b;
  col_k0_open.Properties.ReadOnly:=          b;
  col_k0_closed.Properties.ReadOnly:=        b;

  col_ThRESHOLD_BER_3.Properties.ReadOnly:=  b;
  col_ThRESHOLD_BER_6.Properties.ReadOnly:=  b;
  col_TX_FREQ_MHz.Properties.ReadOnly :=     b;
  col_Power_dBm.Properties.ReadOnly:=        b;

  // -------------------------------------------------------------------

  b:=not (Eq(sObjName,OBJ_LINKEND_ANTENNA) or Eq(sObjName,'ANTENNA'));

  col_AntennaType_Name.Properties.ReadOnly:= b;
  col_Antenna_Height.Properties.ReadOnly  := b;
  col_Antenna_Azimuth.Properties.ReadOnly := b;
  col_Antenna_tilt.Properties.ReadOnly    := b;
  col_Antenna_Loss_db.Properties.ReadOnly := b;

end;





begin

end
