unit u_SiteTemplate_types;

interface

type
//  TSiteTemplate_Type = (stGSM, stPMP, st3G);
//  TSiteTemplate_Type = (stPMP);

  //-----------------------------

  TdmTemplate_Linkend_Rec1 = record
    NewName         : string;

    Template_pmp_Site_ID : integer;

    CellLayer_ID    : integer;
    CalcModel_ID    : integer;
    Combiner_ID     : integer;
//    TrxID          : integer;
    LinkEndType_ID : integer;
  //  CELLID         : integer;

    Color          : integer;
    CalcRadius_km  : double;
    CombinerLoss   : double;

    POWER_dBm       : double;
    Tx_FREQ_MHz        : double;

    SENSE_dBm      : double;    // ������ GSM/PMP
  end;

  //-----------------------------

  TdmTemplate_Pmp_Site_Rec = record
    NewName: string;
    CalcRadius_km: double;
//    FolderID  : integer;
 //   BS_Type   : TSiteTemplate_Type;
  end;

implementation

end.
