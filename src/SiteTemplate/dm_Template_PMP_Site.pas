unit dm_Template_PMP_Site;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, ADODB, Variants,

  dm_Onega_DB_data,

  dm_Object_base,

  dm_Main,
 // I_CalcModel,

  u_SiteTemplate_types,
  u_const_db,

//  u_const_db_3g,

  u_GEO,
  u_dlg,
  u_reg,
  u_db,
  u_img,
  u_func,
  u_types,

 // dm_Template_Linkend,

  u_classes
  ;


type

  TdmTemplate_PMP_Site = class(TdmObject_base)
    qry_Site: TADOQuery;
    qry_TempCell: TADOQuery;
    qry_TempAntenna: TADOQuery;
    qry_Template_Cells: TADOQuery;
    qry_Template_Ant: TADOQuery;
    qry_Temp: TADOQuery;
    procedure DataModuleCreate(Sender: TObject);
  private
  public
    function Add(aRec: TdmTemplate_Pmp_Site_Rec): integer;
    function Del (aID: integer): boolean; override;
  end;


function dmTemplate_PMP_Site: TdmTemplate_PMP_Site;


//==============================================================================
implementation  {$R *.dfm}
//==============================================================================
var
  FdmTemplate_PMP_Site: TdmTemplate_PMP_Site;


// -----------------------------------------------------------------------------
function dmTemplate_PMP_Site: TdmTemplate_PMP_Site;
// -----------------------------------------------------------------------------
begin
  if not Assigned(FdmTemplate_PMP_Site) then
    FdmTemplate_PMP_Site := TdmTemplate_PMP_Site.Create(Application);

  Result := FdmTemplate_PMP_Site;
end;


//------------------------------------------------------------------------------
procedure TdmTemplate_PMP_Site.DataModuleCreate(Sender: TObject);
//------------------------------------------------------------------------------
begin
  inherited;
  TableName  :=TBL_TEMPLATE_PMP_SITE;
  ObjectName:=OBJ_TEMPLATE_PMP_SITE;

  DisplayName:= '������ �� PMP';
end;


//------------------------------------------------------------------------------
function TdmTemplate_PMP_Site.Add(aRec: TdmTemplate_Pmp_Site_Rec): integer;
//------------------------------------------------------------------------------
begin
  Result := dmOnega_DB_data.ExecStoredProc ('sp_TEMPLATE_Pmp_site_Add',
          [
           db_Par(FLD_Name,             aRec.NewName),
           db_Par(FLD_CALC_RADIUS_KM,   aRec.CalcRadius_km)
          ]);
end;


//------------------------------------------------------------------------------
function TdmTemplate_PMP_Site.Del (aID: integer): boolean;
//------------------------------------------------------------------------------
begin
  dmOnega_DB_data.Template_PMP_Site_Del (aID);

  Result:=True;

end;


//------------------------------------------------------------------------------
begin

end.

