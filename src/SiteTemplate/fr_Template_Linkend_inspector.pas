unit fr_Template_Linkend_inspector;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,  Variants,
  ActnList, rxPlacemnt, StdCtrls, ExtCtrls,  

  fr_DBInspector_Container,

  dm_LinkEndType,
//  dm_LinkEnd_View,

   u_func,

 // fr_Object_inspector,



//  dm_User_Security,

  d_LinkEndType_Mode,


 // I_Act_Explorer, //
 dm_Act_Explorer,


//  fr_DBInspector_Container,
  dm_Onega_DB_data,

  dm_Main,

  d_LinkEnd_Get_Band,

  I_Shell,
  I_LinkEndType,


  u_const,
  u_const_db,
  u_const_str,
  u_const_msg,
  u_types

  ;


type
 // Tframe_CalcRegion_inspector = class(Tframe_DBInspector_Container)

  Tframe_Template_Linkend_inspector = class(Tframe_DBInspector_Container)
    procedure FormCreate(Sender: TObject);
  private
    function Dlg_UpdateMode: Boolean;

    procedure DoOnButtonFieldClick (Sender: TObject; aFieldName: string; aNewValue: Variant); override;
    procedure DoOnFieldChanged (Sender: TObject; aFieldName: string; aNewValue: Variant); override;


    procedure UpdateLinkEndTypeInfo (aLinkEndTypeID: integer );

  public
    procedure View (aID: integer); override;

//    class function ExecDlg (aID: integer): boolean;
//    class function CreateChildForm ( aDest: TWinControl): Tframe_CellTemplate_inspector;

  end;


//==================================================================
implementation {$R *.dfm}
//==================================================================

//-------------------------------------------------------------------
procedure Tframe_Template_Linkend_inspector.FormCreate(Sender: TObject);
//-------------------------------------------------------------------
begin
  inherited;
 // Ffrm_DBInspector.
  PrepareViewForObject (OBJ_TEMPLATE_LINKEND);
end;

//-------------------------------------------------------------------
procedure Tframe_Template_Linkend_inspector.View(aID: integer);
//-------------------------------------------------------------------

begin
//  Ffrm_DBInspector.
//  PrepareView(OBJ_CELL_TEMPLATE);
//  inherited;
  inherited View(aID);


//  raise Exception.Create('procedure Tframe_Template_Linkend_inspector.View(aID: integer);');

end;

//-------------------------------------------------------------------
function Tframe_Template_Linkend_inspector.Dlg_UpdateMode: Boolean;
//-------------------------------------------------------------------
var
  iID: Integer;
  rec: TdmLinkEndTypeModeInfoRec;

  iLinkEndType_Mode_ID: integer;
  iLinkEndTypeID: Integer;

begin
  iLinkEndType_Mode_ID:=GetIntFieldValue (FLD_LinkEndType_Mode_ID);
  iLinkEndTypeID      :=GetIntFieldValue (FLD_LINKENDTYPE_ID);

 // Assert(iLinkEndTypeID>0);

  iID:= Tdlg_LinkEndType_Mode.Dlg_GetMode1 (iLinkEndTypeID, iLinkEndType_Mode_ID, rec);

  if iID>0 then
  begin
     SetFieldValue (FLD_MODE,                rec.Mode);
     SetFieldValue (FLD_LinkEndType_Mode_ID, rec.ID);

     SetFieldValue (FLD_BitRate_Mbps,        rec.BitRate_Mbps_);

     SetFieldValue (FLD_MODULATION_TYPE,     rec.MODULATION_TYPE);
     SetFieldValue (FLD_SIGNATURE_HEIGHT,    rec.SIGNATURE_HEIGHT);
     SetFieldValue (FLD_SIGNATURE_WIDTH,     rec.SIGNATURE_WIDTH);
     SetFieldValue (FLD_THRESHOLD_BER_3,     rec.THRESHOLD_BER_3);
     SetFieldValue (FLD_THRESHOLD_BER_6,     rec.THRESHOLD_BER_6);

//     SetFieldValue (FLD_SUBBAND, null);
//     SetFieldValue (FLD_CHAnnel_number,     null);


     SetFieldValue (FLD_channel_width_MHz,   rec.bandwidth_MHz);


     SetFieldValue (FLD_POWER_dBm,           rec.Power_max_dBm);
     SetFieldValue (FLD_POWER_MAX,           rec.Power_max_dBm);
     SetFieldValue (FLD_POWER_LOSS,          0);

//     SetFieldValue (FLD_MODULATION_COUNT,      rec.MODULATION_level_COUNT);
//     SetFieldValue (FLD_GOST_53363_modulation, rec.GOST_Modulation);

     HideEdit;

//     FIsPostEnabled := True;

//     UpdateFreqSpacing;

     Result := True;

  end else
     Result := False;
end;



//-------------------------------------------------------------------
procedure Tframe_Template_Linkend_inspector.DoOnButtonFieldClick (Sender: TObject; aFieldName: string;
                                                aNewValue: Variant);
//-------------------------------------------------------------------
begin

  if Eq(aFieldName, FLD_MODE) then
    Dlg_UpdateMode
  else

 {
  //------------------------------
  if Eq(aFieldName, FLD_CHANNEL_NUMBER) or
     Eq(aFieldName, FLD_TX_FREQ_MHz) or
     Eq(aFieldName, FLD_RX_FREQ_MHz) or
     Eq(aFieldName, FLD_CHANNEL_TYPE) or
     Eq(aFieldName, FLD_SUBBAND)
  then begin
    Dlg_GetBand;
  end;

  }

end;


//-------------------------------------------------------------------
procedure Tframe_Template_Linkend_inspector.DoOnFieldChanged (Sender: TObject; aFieldName: string;
                                                aNewValue: Variant);
//-------------------------------------------------------------------
var
  bXPIC: Boolean;
  dPowerLoss,dPowerMax,dPower,dAllowedPowerLoss: double;

  iValue: integer;
  dPassiveElementLoss, dLoss: double;
  eBitRate_Mbps: Double;

  sProtectionType: string;
  iLinkEndTypeID: integer;
  iLinkEndType_Mode_ID: Integer;
  k,iID: integer;
begin
  if aFieldName='' then
    Exit;

  //------------------------------
  if Eq(aFieldName, FLD_LINKENDTYPE_ID) then
  //------------------------------
  begin
  {
    if aNewValue = 0 then
    begin
      SetFieldValue(FLD_EQUIPMENT_TYPE, DEF_EQUIPMENT_NON_STANDARD);

//      SetFieldValue (FLD_LINKENDTYPE_BAND_ID, null);
    end
    else
      SetFieldValue(FLD_EQUIPMENT_TYPE, DEF_EQUIPMENT_STANDARD);
    }

    if aNewValue>0 then
    begin
      UpdateLinkEndTypeInfo(aNewValue);
      Dlg_UpdateMode();
    end;


//    Clear_Band();

(*
    ShowEquipmentType(); //equipment_standard, non equipment_standard

    //clear fields
    UpdateLinkEndTypeInfo(0);
    //set new LinkEndType
    UpdateLinkEndTypeInfo(aNewValue);
    CollapseRowByFieldName (FLD_SUBBAND);

    if aNewValue>0 then
      Dlg_UpdateMode();
      *)

  end;

end;

//-------------------------------------------------------------------
procedure Tframe_Template_Linkend_inspector.UpdateLinkEndTypeInfo (aLinkEndTypeID: integer );
//-------------------------------------------------------------------
var
  rec: TdmLinkEndTypeInfoRec;
  iValue: integer;
  sValue: string;
  eFreq_MHZ: Double;
begin
  if //(aLinkEndTypeID>0) and
      dmLinkEndType.GetInfoRec (aLinkEndTypeID, rec) then
  begin
    SetFieldValue (FLD_POWER_dBm,  rec.Power_Max);
    SetFieldValue (FLD_POWER_MAX,  rec.Power_Max);
    SetFieldValue (FLD_POWER_LOSS, 0);
  //  SetFieldValue (FLD_KNG,        0);

//    if rec.failure_period<0 then
//      rec.failure_period:=0;

  //  SetFieldValue (FLD_FAILURE_PERIOD,  rec.failure_period);
 //   SetFieldValue (FLD_RESTORE_PERIOD,  0);

    SetFieldValue (FLD_BAND,     rec.BAND);
//    SetFieldValue (FLD_RANGE,     rec.RANGE);

//    if GetIntFieldValue(FLD_CHANNEL_NUMBER)) = 0 then
//    begin

    SetFieldValue (FLD_CHANNEL_NUMBER, null);

 //   SetFieldValue (FLD_RX_FREQ,   0);
  ////////////////  SetFieldValue (FLD_TX_FREQ,   rec.RANGE*1000);

//    eFreq_MHZ:=dmLibrary.GetBandAveFreq_MHZ(rec.BAND);
    eFreq_MHZ:=rec.Freq_Ave_MHz;

    SetFieldValue (FLD_RX_FREQ_MHz, eFreq_MHZ);
    SetFieldValue (FLD_TX_FREQ_MHz, eFreq_MHZ);

  //  SetFieldValue (FLD_LINKENDTYPE_BAND_ID, null);


    ///
//    SetFieldValue (FLD_MODULATION_COUNT,      rec.MODULATION_level_COUNT);
 //   SetFieldValue (FLD_GOST_53363_modulation, rec.GOST_Modulation);


  end
;

//  UpdateBand;
end;



end.

(*

     {

//--------------------------------------------------------------------
class function Tframe_Template_Linkend_inspector.ExecDlg (aID: integer): boolean;
//--------------------------------------------------------------------
begin
  with Tframe_Template_Linkend_inspector.Create(Application) do
  try
(*
    View (aID);
    pn_Buttons.Visible:=True;

    Result:=(ShowModal=mrOK);

*)
  finally
    Free;
  end;

end;

  FIsPostEnabled:=True;


  // -------------------------------------------------------------------
  if Eq(aFieldName, FLD_kratnost_By_Space)  //or
    // Eq(aFieldName, FLD_kratnost_By_Freq)
  // -------------------------------------------------------------------

  then begin
    k:=GetIntFieldValue(FLD_kratnost_By_Space);

    bXPIC:=k>1;

    SetFieldValue (FLD_XPIC_use_2_stvola_diff_pol, IIF(bXPIC, 1, 0 ));

    iLinkEndType_Mode_ID:=GetIntFieldValue (FLD_LinkEndType_Mode_ID);

    if iLinkEndType_Mode_ID>0 then
    begin
      dmOnega_DB_data.OpenQuery(dmOnega_DB_data.ADOQuery1,
                  Format('select BitRate_Mbps from LinkEndType_Mode where id=%d', [iLinkEndType_Mode_ID]));

  //    eBitRate_Mbps := GetFloatFieldValue (FLD_BitRate_Mbps);
      eBitRate_Mbps := dmOnega_DB_data.ADOQuery1.fieldByName(FLD_BitRate_Mbps).AsFloat;

      SetFieldValue (FLD_BitRate_Mbps, IIF(bXPIC, 2*eBitRate_Mbps, eBitRate_Mbps ));

    end;



//    SetFieldValue (FLD_XPIC_use_2_stvola_diff_pol, IIF((k=0) or (k=1), 0, 1 ));


//    SetFieldValue (FLD_XPIC_use_2_stvola_diff_pol, IIF((k=0) or (k=1), 0, 1 ));

//  FBitRate_Mbps := GetFloatFieldValue (FLD_BitRate_Mbps);


//    k:=GetIntFieldValue(FLD_kratnost_By_Space);



    //���������� ��������������
  //  sProtectionType:='';

   (* if GetIntFieldValue(FLD_redundancy) = 1 then
    begin
      //��������� ���������� �� ������������
      if (GetIntFieldValue(FLD_kratnost_By_Space)=0 ) and
         (GetIntFieldValue(FLD_kratnost_By_Freq)=0 )
      then sProtectionType:='HSBY' else

      if (GetIntFieldValue(FLD_kratnost_By_Space)=0 ) and
         (GetIntFieldValue(FLD_kratnost_By_Freq)>0 )
      then sProtectionType:='FD' else

      if (GetIntFieldValue(FLD_kratnost_By_Space)=1 )
//         ( FieldBYName(kratnostByFreq).AsInteger>0 )
      then sProtectionType:='HSBY SD' else

      if (GetIntFieldValue(FLD_kratnost_By_Space)=1 ) and
         (GetIntFieldValue(FLD_kratnost_By_Freq)>0 )
      then sProtectionType:='FD SD';
    end;*)

////////    SetFieldValue (FLD_PROTECTION_TYPE, sProtectionType);
  end else



  //------------------------------
  if Eq(aFieldName, FLD_CALC_RADIUS_KM) then
  begin
    FIsPostEnabled:=True;
  end else

  //------------------------------
  if Eq(aFieldName, FLD_EQUIPMENT_TYPE) then
  begin
   // iValue:= GetIntFieldValue(FLD_EQUIPMENT_TYPE);

    ChangeEquipmentType(aNewValue);
  end else

  //------------------------------
  if Eq(aFieldName, FLD_CHANNEL_SPACING) then
    UpdateFreqSpacing() else

  //------------------------------
  if Eq(aFieldName, FLD_PASSIVE_ELEMENT_ID) then
  begin
    iID :=AsInteger(aNewValue);
    UpdatePassiveElement(iID);
  end else


  //------------------------------
  if Eq(aFieldName, FLD_REDUNDANCY) then
    UpdateKNG ()
  else

  //------------------------------
  if (Eq(aFieldName, FLD_KNG)) or
     (Eq(aFieldName, FLD_REDUNDANCY)) then
    UpdateRestorePeriod ()
  else

  //------------------------------
  if Eq(aFieldName, FLD_RESTORE_PERIOD) then
  begin
    if aNewValue <> '' then
      UpdateKNG ()
    else
      SetFieldValue (FLD_KNG, NULL);
  end
     else

  //------------------------------
  if Eq(aFieldName, FLD_POWER_LOSS) then
  //------------------------------
  begin
      if aNewValue = '' then
        aNewValue := 0;

{      if (aNewValue < 0) then
    begin
        SetFieldValue (FLD_POWER_LOSS, 0);;
      exit;
    end;  }

    iLinkEndTypeID := GetIntFieldValue (FLD_LINKENDTYPE_ID);
    dAllowedPowerLoss:=
      gl_DB.GetIntFieldValueByID(TBL_LINKENDTYPE, FLD_POWER_LOSS, iLinkEndTypeID);

    if (aNewValue > dAllowedPowerLoss) then
    begin
      SetFieldValue (FLD_POWER_LOSS, 0);;
      exit;
    end;

    dPowerMax:=GetFloatFieldValue (FLD_POWER_MAX);
    dPowerLoss:=aNewValue;

    SetFieldValue (FLD_POWER_dBm, dPowerMax - dPowerLoss);

  end else

  //------------------------------
  if Eq(aFieldName, FLD_POWER_dBm) then
  //------------------------------
  begin
    if aNewValue = '' then
      aNewValue := 0;

    aNewValue := GetFloatFieldValue (FLD_POWER_MAX) - aNewValue;

    SetFieldValue (FLD_POWER_LOSS, aNewValue);
  end else


