unit fr_Template_Linkend_Antenna_view;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Menus, AppEvnts, ImgList, ActnList,
   ExtCtrls,

  fr_View_base,

  fr_DBInspector_Container,

  u_func,
  u_const_str,

  u_types,

  cxPropertiesStore, dxBar, cxBarEditItem, cxClasses, cxLookAndFeels ;

//  fr_Template_Linkend_inspector;

type
  Tframe_Template_Linkend_Antenna_view = class(Tframe_View_Base)
    procedure FormCreate(Sender: TObject);
 //   procedure FormDestroy(Sender: TObject);
  private
    FInspector: Tframe_DBInspector_Container;
  public
    procedure View(aID: integer; aGUID: string); override;
  end;


//===============================================================
implementation{$R *.dfm}
//===============================================================

//--------------------------------------------------------------------
procedure Tframe_Template_Linkend_Antenna_view.FormCreate(Sender: TObject);
//--------------------------------------------------------------------
begin
  inherited;
  ObjectName:=OBJ_TEMPLATE_LINKEND_Antenna;

//  CreateChildForm(Tframe_Template_LINKEND_inspector, FInspector, pn_Main);

  CreateChildForm(Tframe_DBInspector_Container, FInspector, pn_Main);
  FInspector.PrepareViewForObject(OBJ_TEMPLATE_LINKEND_Antenna);


end;


//--------------------------------------------------------------------
procedure Tframe_Template_Linkend_Antenna_view.View(aID: integer; aGUID:
    string);
//--------------------------------------------------------------------
begin
  inherited;

  FInspector.View (aID);
end;


end.


{
 


            
//--------------------------------------------------------------------
procedure Tframe_ClutterModel_view.FormCreate(Sender: TObject);
//--------------------------------------------------------------------
begin
  inherited;

  pn_Inspector.Align:=alClient;

  ObjectName:=OBJ_CLUTTER_MODEL;

  CreateChildForm(Tframe_DBInspector_Container, Ffrm_DBInspector, pn_Inspector);
  Ffrm_DBInspector.PrepareViewForObject(OBJ_CLUTTER_MODEL);

  CreateChildForm(Tframe_ClutterModel_Items, Fframe_Clutter_Items, pn_Items);

 // AddControl(pn_Items, [PROP_HEIGHT]);

  AddComponentProp(pn_Items, PROP_HEIGHT);
  cxPropertiesStore.RestoreFrom;


  Fframe_Clutter_Items.SetReadOnly(False);


  pn_Inspector.Constraints.MinHeight:=20;

end;


procedure Tframe_ClutterModel_view.View (aID: integer; aGUID: string);
begin
  inherited;

  Ffrm_DBInspector.View (aID);
  Fframe_Clutter_Items.View (aID);
end;




