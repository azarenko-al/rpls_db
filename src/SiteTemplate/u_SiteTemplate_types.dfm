inherited dmSiteTemplate: TdmSiteTemplate
  Left = 285
  Top = 161
  Height = 355
  Width = 459
  inherited qry_Temp: TADOQuery
    Left = 116
    Top = 12
  end
  inherited qry_Temp2: TADOQuery
    Top = 12
  end
  object qry_Site: TADOQuery
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'id'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 5
      end>
    SQL.Strings = (
      'select   id,name'
      ' from property'
      'where id=:id')
    Left = 76
    Top = 148
  end
  object qry_TempCell: TADOQuery
    MarshalOptions = moMarshalModifiedOnly
    EnableBCD = False
    Parameters = <>
    Left = 32
    Top = 72
  end
  object qry_TempAntenna: TADOQuery
    MarshalOptions = moMarshalModifiedOnly
    EnableBCD = False
    Parameters = <>
    Left = 120
    Top = 72
  end
end
