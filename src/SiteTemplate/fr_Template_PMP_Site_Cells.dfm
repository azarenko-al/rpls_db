object frame_SiteTemplate_Cells: Tframe_SiteTemplate_Cells
  Left = 733
  Top = 513
  Width = 1482
  Height = 360
  Caption = 'frame_SiteTemplate_Cells'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = True
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object cxDBTreeList1: TcxDBTreeList
    Left = 0
    Top = 27
    Width = 1466
    Height = 112
    Align = alTop
    Bands = <
      item
        Caption.AlignHorz = taCenter
        FixedKind = tlbfLeft
      end
      item
        Caption.AlignHorz = taCenter
        Caption.Text = 'Cell'
        Width = 209
      end
      item
        Caption.AlignHorz = taCenter
        Caption.Text = 'Trx'
      end
      item
        Caption.AlignHorz = taCenter
        Caption.Text = #1040#1085#1090#1077#1085#1085#1072
      end
      item
        Caption.AlignHorz = taCenter
        Caption.Text = #1050#1072#1083#1080#1073#1088#1086#1074#1082#1072
      end
      item
        Caption.AlignHorz = taCenter
        Caption.Text = #1050#1086#1084#1073#1072#1081#1085#1077#1088
      end>
    DataController.DataSource = DataSource2
    DataController.ParentField = 'parent_guid'
    DataController.KeyField = 'guid'
    LookAndFeel.Kind = lfFlat
    OptionsBehavior.GoToNextCellOnTab = True
    OptionsBehavior.AutoDragCopy = True
    OptionsBehavior.DragCollapse = False
    OptionsBehavior.ExpandOnIncSearch = True
    OptionsBehavior.ShowHourGlass = False
    OptionsCustomizing.BandCustomizing = False
    OptionsCustomizing.BandVertSizing = False
    OptionsCustomizing.ColumnVertSizing = False
    OptionsSelection.HideFocusRect = False
    OptionsSelection.InvertSelect = False
    OptionsView.CellTextMaxLineCount = -1
    OptionsView.ShowEditButtons = ecsbFocused
    OptionsView.Bands = True
    OptionsView.Indicator = True
    ParentColor = False
    PopupMenu = PopupMenu1
    Preview.AutoHeight = False
    Preview.MaxLineCount = 2
    RootValue = -1
    Styles.Preview = cxStyle1
    TabOrder = 0
    OnEditing = cxDBTreeList1Editing
    OnKeyDown = cxDBTreeList1KeyDown
    object col_name: TcxDBTreeListColumn
      DataBinding.FieldName = 'name'
      Options.Editing = False
      Width = 124
      Position.ColIndex = 1
      Position.RowIndex = 0
      Position.BandIndex = 0
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object col_id: TcxDBTreeListColumn
      Visible = False
      DataBinding.FieldName = 'id'
      Options.Editing = False
      Width = 32
      Position.ColIndex = 0
      Position.RowIndex = 0
      Position.BandIndex = 0
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object col_cell_layer_name: TcxDBTreeListColumn
      PropertiesClassName = 'TcxButtonEditProperties'
      Properties.Alignment.Horz = taLeftJustify
      Properties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end>
      Properties.OnButtonClick = col_LinkEndType_Name_PropertiesButtonClick
      DataBinding.FieldName = 'cell_layer_name'
      Width = 92
      Position.ColIndex = 0
      Position.RowIndex = 0
      Position.BandIndex = 1
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object col_LinkEndType_Name: TcxDBTreeListColumn
      PropertiesClassName = 'TcxButtonEditProperties'
      Properties.Alignment.Horz = taLeftJustify
      Properties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end>
      Properties.OnButtonClick = col_LinkEndType_Name_PropertiesButtonClick
      DataBinding.FieldName = 'LinkEndType_Name'
      Width = 69
      Position.ColIndex = 0
      Position.RowIndex = 0
      Position.BandIndex = 2
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object col_antennatype_name: TcxDBTreeListColumn
      PropertiesClassName = 'TcxButtonEditProperties'
      Properties.Alignment.Horz = taLeftJustify
      Properties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end>
      Properties.OnButtonClick = col_LinkEndType_Name_PropertiesButtonClick
      DataBinding.FieldName = 'antennatype_name'
      Width = 98
      Position.ColIndex = 0
      Position.RowIndex = 0
      Position.BandIndex = 3
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object col_calc_radius_km: TcxDBTreeListColumn
      DataBinding.FieldName = 'calc_radius_km'
      Width = 71
      Position.ColIndex = 1
      Position.RowIndex = 0
      Position.BandIndex = 1
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object col_THRESHOLD_BER_3: TcxDBTreeListColumn
      DataBinding.FieldName = 'THRESHOLD_BER_3'
      Width = 45
      Position.ColIndex = 1
      Position.RowIndex = 0
      Position.BandIndex = 2
      SortOrder = soAscending
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object col_THRESHOLD_BER_6: TcxDBTreeListColumn
      DataBinding.FieldName = 'THRESHOLD_BER_6'
      Width = 45
      Position.ColIndex = 2
      Position.RowIndex = 0
      Position.BandIndex = 2
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object col_POWER_dBm: TcxDBTreeListColumn
      DataBinding.FieldName = 'POWER_dBm'
      Width = 78
      Position.ColIndex = 3
      Position.RowIndex = 0
      Position.BandIndex = 2
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object col_TX_FREQ_MHz: TcxDBTreeListColumn
      DataBinding.FieldName = 'TX_FREQ_MHz'
      Width = 53
      Position.ColIndex = 4
      Position.RowIndex = 0
      Position.BandIndex = 2
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object col_combiner_name: TcxDBTreeListColumn
      PropertiesClassName = 'TcxButtonEditProperties'
      Properties.Alignment.Horz = taLeftJustify
      Properties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end>
      Properties.OnButtonClick = col_LinkEndType_Name_PropertiesButtonClick
      DataBinding.FieldName = 'combiner_name'
      Width = 54
      Position.ColIndex = 0
      Position.RowIndex = 0
      Position.BandIndex = 5
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object col_combiner_loss_dB: TcxDBTreeListColumn
      Caption.Text = ' [dB]'
      DataBinding.FieldName = 'combiner_loss_dB'
      Options.Editing = False
      Width = 54
      Position.ColIndex = 1
      Position.RowIndex = 0
      Position.BandIndex = 5
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object col_Antenna_Height: TcxDBTreeListColumn
      DataBinding.FieldName = 'height'
      Width = 69
      Position.ColIndex = 1
      Position.RowIndex = 0
      Position.BandIndex = 3
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object col_POLARIZATION_str: TcxDBTreeListColumn
      DataBinding.FieldName = 'POLARIZATION_str'
      Width = 102
      Position.ColIndex = 2
      Position.RowIndex = 0
      Position.BandIndex = 3
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object col_Antenna_Azimuth: TcxDBTreeListColumn
      DataBinding.FieldName = 'azimuth'
      Width = 68
      Position.ColIndex = 3
      Position.RowIndex = 0
      Position.BandIndex = 3
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object col_Antenna_tilt: TcxDBTreeListColumn
      DataBinding.FieldName = 'tilt'
      Width = 69
      Position.ColIndex = 4
      Position.RowIndex = 0
      Position.BandIndex = 3
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object col_antenna_loss_dB: TcxDBTreeListColumn
      Caption.Text = 'Loss'
      DataBinding.FieldName = 'antenna_loss'
      Width = 28
      Position.ColIndex = 5
      Position.RowIndex = 0
      Position.BandIndex = 3
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object col_calc_model_Name: TcxDBTreeListColumn
      PropertiesClassName = 'TcxButtonEditProperties'
      Properties.Alignment.Horz = taLeftJustify
      Properties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end>
      Properties.OnButtonClick = col_LinkEndType_Name_PropertiesButtonClick
      Caption.Text = #1052#1086#1076#1077#1083#1100' '#1088#1072#1089#1095#1105#1090#1072
      DataBinding.FieldName = 'calc_model_Name'
      Width = 88
      Position.ColIndex = 0
      Position.RowIndex = 0
      Position.BandIndex = 4
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object col_k0: TcxDBTreeListColumn
      DataBinding.FieldName = 'k0'
      Width = 20
      Position.ColIndex = 1
      Position.RowIndex = 0
      Position.BandIndex = 4
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object col_k0_open: TcxDBTreeListColumn
      DataBinding.FieldName = 'k0_open'
      Width = 48
      Position.ColIndex = 2
      Position.RowIndex = 0
      Position.BandIndex = 4
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object col_k0_closed: TcxDBTreeListColumn
      DataBinding.FieldName = 'k0_closed'
      Width = 55
      Position.ColIndex = 3
      Position.RowIndex = 0
      Position.BandIndex = 4
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object col_Band: TcxDBTreeListColumn
      DataBinding.FieldName = 'Band'
      Options.Editing = False
      Width = 33
      Position.ColIndex = 2
      Position.RowIndex = 0
      Position.BandIndex = 1
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object col_objname: TcxDBTreeListColumn
      Visible = False
      DataBinding.FieldName = 'objname'
      Options.Editing = False
      Width = 53
      Position.ColIndex = 2
      Position.RowIndex = 0
      Position.BandIndex = 5
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
  end
  object ActionList1: TActionList
    OnUpdate = ActionList1Update
    Left = 167
    Top = 180
    object act_Add_cell: TAction
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1089#1077#1082#1090#1086#1088
      OnExecute = act_EditExecute
    end
    object act_Add_Antenna: TAction
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1072#1085#1090#1077#1085#1085#1091
      OnExecute = act_EditExecute
    end
    object act_Edit: TAction
      Caption = #1048#1079#1084#1077#1085#1080#1090#1100
      Visible = False
      OnExecute = act_EditExecute
    end
    object act_Del: TAction
      Caption = #1059#1076#1072#1083#1080#1090#1100
      OnExecute = act_EditExecute
    end
  end
  object PopupMenu1: TPopupMenu
    Left = 124
    Top = 180
    object N2: TMenuItem
      Action = act_Add_cell
    end
    object N5: TMenuItem
      Action = act_Add_Antenna
    end
    object N1: TMenuItem
      Action = act_Edit
    end
    object N4: TMenuItem
      Caption = '-'
    end
    object N3: TMenuItem
      Action = act_Del
    end
  end
  object DataSource2: TDataSource
    DataSet = ADOStoredProc1
    Left = 257
    Top = 220
  end
  object ADOStoredProc1: TADOStoredProc
    AfterPost = ADOQuery1AfterPost
    Parameters = <>
    Left = 256
    Top = 176
  end
  object cxStyleRepository1: TcxStyleRepository
    Left = 40
    Top = 176
    PixelsPerInch = 96
    object cxStyle1: TcxStyle
      AssignedValues = [svFont, svTextColor]
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlue
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      TextColor = clBlue
    end
  end
  object dxBarManager1: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = []
    Categories.Strings = (
      'Default')
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    PopupMenuLinks = <>
    SunkenBorder = True
    UseSystemFont = True
    Left = 456
    Top = 176
    DockControlHeights = (
      0
      0
      27
      0)
    object dxBarManager1Bar1: TdxBar
      AllowQuickCustomizing = False
      Caption = 'Custom 1'
      CaptionButtons = <>
      DockedDockingStyle = dsTop
      DockedLeft = 0
      DockedTop = 0
      DockingStyle = dsTop
      FloatLeft = 541
      FloatTop = 546
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarSubItem1'
        end>
      OneOnRow = True
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarButton1: TdxBarButton
      Action = act_Add_Antenna
      Category = 0
    end
    object dxBarSubItem1: TdxBarSubItem
      Caption = #1054#1087#1077#1088#1072#1094#1080#1080
      Category = 0
      Visible = ivAlways
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarButton2'
        end
        item
          Visible = True
          ItemName = 'dxBarButton1'
        end
        item
          Visible = True
          ItemName = 'dxBarButton4'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarButton3'
        end>
    end
    object dxBarButton2: TdxBarButton
      Action = act_Add_cell
      Category = 0
    end
    object dxBarButton3: TdxBarButton
      Action = act_Del
      Category = 0
    end
    object dxBarButton4: TdxBarButton
      Action = act_Edit
      Category = 0
    end
  end
end
