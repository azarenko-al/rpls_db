unit dm_Terminal;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, Db, ADODB, Variants,

  dm_Main,

  dm_Object_base,

  u_func,
  u_db,
         
  u_types,

  u_const,
  u_const_db,
  u_const_str

   ;


type
  TdmTerminalTypeAddRec = record
    NewName         : string;
    FolderID        : integer;

    Height          : double;
    Power_W         : double;
    Sensitivity: integer;
    Loss: double;
    Gain: double;

  end;


  TdmTerminalType = class(TdmObject_base)
    qry_Temp: TADOQuery;
    procedure DataModuleCreate(Sender: TObject);
  private
  public
    function Add (aRec: TdmTerminalTypeAddRec): integer;
    function GetInfo(aID: integer; var aRec: TdmTerminalTypeAddRec): boolean;

  end;



function dmTerminalType: TdmTerminalType;


//================================================================
implementation {$R *.dfm}
//================================================================

var
  FdmTerminalType: TdmTerminalType;


// ---------------------------------------------------------------
function dmTerminalType: TdmTerminalType;
// ---------------------------------------------------------------
begin
  if not Assigned(FdmTerminalType) then
    FdmTerminalType := TdmTerminalType.Create(Application);

  Result := FdmTerminalType;
end;


procedure TdmTerminalType.DataModuleCreate(Sender: TObject);
begin
  inherited;
  TableName :=TBL_TerminalType;
  ObjectName:=OBJ_TERMINAL_TYPE;

//  DisplayName:=STR_TERMINAL_TYPE;

end;


//--------------------------------------------------------------------
function TdmTerminalType.Add (aRec: TdmTerminalTypeAddRec): integer;
//--------------------------------------------------------------------
begin
   Result:=gl_DB.AddRecordID (TableName,
               [db_Par(FLD_NAME, aRec.NewName),
                db_Par(FLD_FOLDER_ID, IIF_NULL(aRec.FolderID)),

                db_Par(FLD_HEIGHT,           aRec.Height),
                db_Par(FLD_POWER,            aRec.Power_W),
                db_Par(FLD_SENSE,            aRec.Sensitivity),

                db_Par(FLD_LOSS,             aRec.Loss)

               ] );

 //  ;
end;

//--------------------------------------------------------------------
function TdmTerminalType.GetInfo(aID: integer; var aRec: TdmTerminalTypeAddRec):
    boolean;
//--------------------------------------------------------------------
begin
  db_OpenTableByID (qry_Temp, TBL_TerminalType, aID);
  Result:=not qry_Temp.IsEmpty;

  if Result then
    with qry_Temp do
  begin
    aRec.NewName   :=FieldValues[FLD_NAME];

    aRec.FolderID  :=FieldBYName(FLD_FOLDER_ID).AsInteger;

    aRec.Height    :=FieldBYName(FLD_HEIGHT).AsFloat;

    aRec.Power_W        :=FieldBYName(FLD_POWER).AsFloat;
    aRec.Sensitivity:=FieldBYName(FLD_Sense).AsInteger;
    aRec.Loss        :=FieldBYName(FLD_LOSS).AsFloat;

    aRec.Gain        :=FieldBYName('ANTENNAGain').AsFloat;

  end;
end;

begin 

end.

