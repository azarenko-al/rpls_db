unit dm_act_Terminal;

interface

uses
  Classes, Forms,  Menus,  ActnList,

  i_Audit,

  dm_User_Security,

  u_DataExport_run,

  I_Object,

  dm_act_Base,

  d_Terminal_add,
  
  dm_Terminal,

  u_func,

 u_const_str,

  u_const_db,
  u_types

  ;

type
  TdmAct_Terminal = class(TdmAct_Base)
    ActionList2: TActionList;
    act_Export_MDB: TAction;
    act_Audit: TAction;

    procedure DataModuleCreate(Sender: TObject);
  private
    function CheckActionsEnable: Boolean;

  protected
    procedure GetPopupMenu (aPopupMenu: TPopupMenu; aMenuType: TMenuType); override;
    function  GetViewForm (aOwnerForm: TForm): TForm; override;

    procedure DoAction (Sender: TObject);

    function ItemDel(aID: integer; aName: string = ''): boolean; override;
    function ItemAdd (aFolderID: integer): integer; override;

  public
//    procedure Add (aFolderID: integer);
//    procedure Del (aID: integer);

    class procedure Init;

  end;

var
  dmAct_Terminal: TdmAct_Terminal;

//==================================================================
implementation

uses dm_Main; {$R *.dfm}
//==================================================================

//--------------------------------------------------------------------
class procedure TdmAct_Terminal.Init;
//--------------------------------------------------------------------
begin                                          
  if not Assigned(dmAct_Terminal) then
    dmAct_Terminal:=TdmAct_Terminal.Create(Application);
end;


//--------------------------------------------------------------------
procedure TdmAct_Terminal.DataModuleCreate(Sender: TObject);
//--------------------------------------------------------------------
begin
  inherited;

  ObjectName:=OBJ_TERMINAL_TYPE;

  act_Export_MDB.Caption:=DEF_STR_Export_MDB;

  act_Audit.Caption:=STR_Audit;


  SetActionsExecuteProc ([
                          act_Export_MDB ,
                          act_Audit
                          ], DoAction);

//  TdmTerminal.Init;

//  act_Add.Caption:=STR_ADD;
//  act_Del.Caption:=STR_DEl;

//  dlg_SetAllActionsExecuteProc ([act_Add,act_Del], DoAction);

end;

//--------------------------------------------------------------------
procedure TdmAct_Terminal.DoAction (Sender: TObject);
//--------------------------------------------------------------------
begin
  if Sender=act_Audit then
     Init_IAuditX.ExecDlg( dmMain.GetConnectionString, TBL_TerminalType, FFocusedID) else


  if Sender=act_Export_MDB then
//    TDataExport_run.ExportToRplsDbGuides1 (OBJ_ANTENNA_TYPE);
    TDataExport_run.ExportToRplsDb_selected (TBL_TerminalType, FSelectedIDList);

//  if Sender=act_Add then Add (FFocusedID) else
//  if Sender=act_Del then Del (FFocusedID) else
end;

//--------------------------------------------------------------------
function TdmAct_Terminal.ItemAdd (aFolderID: integer): integer;
//--------------------------------------------------------------------
begin
  Result:=Tdlg_Terminal_add.ExecDlg (aFolderID);
end;

//--------------------------------------------------------------------
function TdmAct_Terminal.ItemDel(aID: integer; aName: string = ''): boolean;
//--------------------------------------------------------------------
begin
  Result:=dmTerminalType.Del (aID);
end;

//--------------------------------------------------------------------
function TdmAct_Terminal.GetViewForm (aOwnerForm: TForm): TForm;
//--------------------------------------------------------------------
begin
  Result:=nil;
end;


// ---------------------------------------------------------------
function TdmAct_Terminal.CheckActionsEnable: Boolean;
// ---------------------------------------------------------------
begin
  Result:= dmUser_Security.Modify_lib in [mtAllow, mtAllowMy];

  SetActionsEnabled1( [
          act_Del_,
          act_Del_list
      ],

   Result );

  //  TModify_lib = (mtAllow=1, mtAllowMy=2, mtForbidden=0 );

end;



//--------------------------------------------------------------------
procedure TdmAct_Terminal.GetPopupMenu;
//--------------------------------------------------------------------
var
  bEnable: Boolean;
begin
  bEnable:=CheckActionsEnable();


  case aMenuType of
    mtFolder: begin
                AddFolderMenu_Create (aPopupMenu, bEnable);
                AddFolderPopupMenu (aPopupMenu, bEnable);
              end;


    mtItem:   begin
                AddMenuItem (aPopupMenu, act_Del_);
                AddMenuItem (aPopupMenu, nil);

                AddFolderMenu_Tools (aPopupMenu);
                AddMenuItem (aPopupMenu, act_Export_MDB);

                AddMenuItem (aPopupMenu, act_Audit);

              end;

    mtList:   begin
                AddMenuItem (aPopupMenu, act_Del_list);
                AddFolderMenu_Tools (aPopupMenu);
                AddMenuItem (aPopupMenu, act_Export_MDB);
              end;

  end;
end;

begin


end.


