object frm_test_TrxType: Tfrm_test_TrxType
  Left = 269
  Top = 158
  Width = 543
  Height = 407
  Caption = 'frm_test_TrxType'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 29
    Width = 535
    Height = 332
    Align = alTop
    Anchors = [akLeft, akTop, akRight, akBottom]
    BevelOuter = bvNone
    BorderWidth = 5
    TabOrder = 0
    object Splitter1: TSplitter
      Left = 185
      Top = 5
      Width = 3
      Height = 322
      Cursor = crHSplit
    end
    object PageControl1: TPageControl
      Left = 5
      Top = 5
      Width = 180
      Height = 322
      ActivePage = TabSheet1
      Align = alLeft
      TabOrder = 0
      object TabSheet1: TTabSheet
        Caption = 'TabSheet1'
      end
    end
    object pn_Browser: TPanel
      Left = 188
      Top = 5
      Width = 342
      Height = 322
      Align = alClient
      Caption = 'pn_Browser'
      TabOrder = 1
    end
  end
  object ToolBar1: TToolBar
    Left = 0
    Top = 0
    Width = 535
    Height = 29
    ButtonHeight = 25
    Caption = 'ToolBar1'
    TabOrder = 1
    object Add: TButton
      Left = 0
      Top = 2
      Width = 73
      Height = 25
      Caption = 'Add'
      TabOrder = 0
    end
    object Button1: TButton
      Left = 73
      Top = 2
      Width = 75
      Height = 25
      Caption = 'Button1'
      TabOrder = 3
    end
    object Import_: TButton
      Left = 148
      Top = 2
      Width = 75
      Height = 25
      Caption = 'Import_'
      TabOrder = 2
    end
    object View: TButton
      Left = 223
      Top = 2
      Width = 75
      Height = 25
      Caption = 'View'
      TabOrder = 1
      OnClick = ViewClick
    end
  end
  object FormStorage1: TFormStorage
    StoredValues = <>
    Left = 324
    Top = 6
  end
end
