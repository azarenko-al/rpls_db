program test_Terminal;

uses
  Forms,
  a_Unit in 'a_Unit.pas' {frm_test_TrxType},
  dm_Main_ in '..\..\COM_main\src\dm_Main_.pas' {dmMain_: TDataModule},
  dm_act_Terminal in '..\dm_act_Terminal.pas' {dmAct_Terminal: TDataModule},
  dm_Terminal in '..\dm_Terminal.pas' {dmTerminal: TDataModule},
  d_Terminal_add in '..\d_Terminal_add.pas' {dlg_Terminal_add},
  dm_act_Base in '..\..\Common\dm_act_Base.pas' {dmAct_Base: TDataModule};

{$R *.RES}

begin
  Application.Initialize;
  Application.CreateForm(Tfrm_test_TrxType, frm_test_TrxType);
  Application.Run;
end.
