inherited dlg_Terminal_add: Tdlg_Terminal_add
  Left = 540
  Top = 211
  Height = 465
  Caption = 'dlg_Terminal_add'
  PixelsPerInch = 96
  TextHeight = 13
  inherited pn_Buttons: TPanel
    Top = 403
  end
  inherited Panel2: TPanel
    inherited PageControl1: TPageControl
      inherited TabSheet1: TTabSheet
        object cxVerticalGrid1: TcxVerticalGrid
          Left = 0
          Top = 0
          Width = 542
          Height = 181
          Align = alClient
          LookAndFeel.Kind = lfStandard
          OptionsView.CellTextMaxLineCount = 3
          OptionsView.AutoScaleBands = False
          OptionsView.PaintStyle = psDelphi
          OptionsView.GridLineColor = clBtnShadow
          OptionsView.RowHeaderMinWidth = 30
          OptionsView.RowHeaderWidth = 108
          OptionsView.ValueWidth = 92
          TabOrder = 0
          Version = 1
          object row_Loss: TcxEditorRow
            Expanded = False
            Properties.Caption = 'Loss'
            Properties.EditPropertiesClassName = 'TcxTextEditProperties'
            Properties.EditProperties.Alignment.Horz = taLeftJustify
            Properties.EditProperties.Alignment.Vert = taVCenter
            Properties.EditProperties.MaxLength = 0
            Properties.EditProperties.ReadOnly = False
            Properties.DataBinding.ValueType = 'String'
            Properties.Value = Null
            ID = 0
            ParentID = -1
            Index = 0
            Version = 1
          end
          object row_Height: TcxEditorRow
            Expanded = False
            Properties.Caption = 'Height'
            Properties.EditPropertiesClassName = 'TcxTextEditProperties'
            Properties.EditProperties.Alignment.Horz = taLeftJustify
            Properties.EditProperties.Alignment.Vert = taVCenter
            Properties.EditProperties.MaxLength = 0
            Properties.EditProperties.ReadOnly = False
            Properties.DataBinding.ValueType = 'String'
            Properties.Value = '5'
            ID = 1
            ParentID = -1
            Index = 1
            Version = 1
          end
          object row_Sense: TcxEditorRow
            Expanded = False
            Properties.Caption = 'Sense'
            Properties.EditPropertiesClassName = 'TcxTextEditProperties'
            Properties.EditProperties.Alignment.Horz = taLeftJustify
            Properties.EditProperties.Alignment.Vert = taVCenter
            Properties.EditProperties.MaxLength = 0
            Properties.EditProperties.ReadOnly = False
            Properties.DataBinding.ValueType = 'String'
            Properties.Value = '-100'
            ID = 2
            ParentID = -1
            Index = 2
            Version = 1
          end
          object row_Power: TcxEditorRow
            Expanded = False
            Properties.Caption = 'Power'
            Properties.EditPropertiesClassName = 'TcxTextEditProperties'
            Properties.EditProperties.Alignment.Horz = taLeftJustify
            Properties.EditProperties.Alignment.Vert = taVCenter
            Properties.EditProperties.MaxLength = 0
            Properties.EditProperties.ReadOnly = False
            Properties.DataBinding.ValueType = 'String'
            Properties.Value = Null
            ID = 3
            ParentID = -1
            Index = 3
            Version = 1
          end
        end
      end
    end
  end
end
