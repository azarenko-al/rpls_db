unit d_Terminal_add;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  rxPlacemnt, ActnList,     StdCtrls, ExtCtrls,  
  cxPropertiesStore,

  d_Wizard_add_with_Inspector,

  u_cx_vgrid,

  u_const_str,
  u_const_db,
  u_const_msg,

  u_func,
  u_reg,
  u_func_msg,

//  dm_Main,
  dm_Terminal, cxVGrid, cxControls, cxInplaceContainer, ComCtrls;

type
  Tdlg_Terminal_add = class(Tdlg_Wizard_add_with_Inspector)
    cxVerticalGrid1: TcxVerticalGrid;
    row_Loss: TcxEditorRow;
    row_Height: TcxEditorRow;
    row_Sense: TcxEditorRow;
    row_Power: TcxEditorRow;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure act_OkExecute(Sender: TObject);
  private
    FID: Integer;
    FFolderID: integer;

    procedure Append;
  public
    class function ExecDlg (aFolderID: integer =0): integer;
  end;


//====================================================================
implementation {$R *.DFM}
//====================================================================

//--------------------------------------------------------------------
class function Tdlg_Terminal_add.ExecDlg (aFolderID: integer =0): integer;
//--------------------------------------------------------------------
begin
  with Tdlg_Terminal_add.Create(Application) do
  begin                                           
    FFolderID:=aFolderID;                      
    ed_Name_.Text:=dmTerminalType.GetNewName;

    ShowModal;
    Result:=FID;

    Free;
  end;
end;

//--------------------------------------------------------------------
procedure Tdlg_Terminal_add.FormCreate(Sender: TObject);
//--------------------------------------------------------------------
begin
  inherited;

  SetActionName (STR_DLG_ADD_TERMINAL_TYPE);

  row_Height.Properties.Caption:=STR_HEIGHT;
  row_Power.Properties.Caption:=STR_POWER_W;
  row_Sense.Properties.Caption:=STR_Sensitivity_dBm;
  row_Loss.Properties.Caption:=STR_LOSS;

  AddComponentProp(row_Height, 'Text');
  AddComponentProp(row_Power,  'Text');
  AddComponentProp(row_Sense,  'Text');
  AddComponentProp(row_Loss,   'Text');

  RestoreFrom();


  SetDefaultSize();

  cx_InitVerticalGrid (cxVerticalGrid1);


{  with gl_Reg.RegIni do
  begin
    row_Height.Text:=ReadString(Name, FLD_HEIGHT, '2');
    row_Power.Text :=ReadString(Name, FLD_POWER, '1');
    row_Sense.Text :=ReadString(Name, FLD_SENSE, '-98');
    row_Loss.Text := ReadString(Name, FLD_LOSS, '0');
  end;
}
end;


procedure Tdlg_Terminal_add.FormDestroy(Sender: TObject);
begin
 { with gl_Reg.RegIni do
  begin
    ReadString(Name, FLD_HEIGHT, row_Height.Text);
    ReadString(Name, FLD_POWER,  row_Power.Text);
    ReadString(Name, FLD_SENSE,  row_Sense.Text);
    ReadString(Name, FLD_LOSS,   row_Loss.Text);
  end;}
  
  inherited;
end;

//-------------------------------------------------------------------
procedure Tdlg_Terminal_add.act_OkExecute(Sender: TObject);
//-------------------------------------------------------------------
begin
  Append();
end;

//-------------------------------------------------------------------
procedure Tdlg_Terminal_add.Append;
//-------------------------------------------------------------------
var rec: TdmTerminalTypeAddRec;
begin
  rec.NewName   :=ed_Name_.Text;
  rec.FolderID:=FFolderID;
  rec.Height  :=AsFloat(row_Height.Properties.Value);
  rec.Power_W   :=AsFloat(row_Power.Properties.Value);
  rec.Sensitivity   :=AsInteger(row_Sense.Properties.Value);
  rec.Loss    :=AsFloat(row_Loss.Properties.Value);

  FID:=dmTerminalType.Add (rec);
end;


end.