object dlg_Audit1: Tdlg_Audit1
  Left = 545
  Top = 252
  Width = 818
  Height = 473
  Caption = 'dlg_Audit1'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poDefault
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object cxGrid1: TcxGrid
    Left = 0
    Top = 0
    Width = 810
    Height = 233
    Align = alTop
    TabOrder = 0
    LookAndFeel.Kind = lfFlat
    object cxGrid1DBTableView2: TcxGridDBTableView
      DataController.DataSource = DataSource1
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      NavigatorButtons.ConfirmDelete = False
      OptionsData.Deleting = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsSelection.CellSelect = False
      object col_Date: TcxGridDBColumn
        DataBinding.FieldName = 'Date'
        Width = 122
      end
      object col_Host: TcxGridDBColumn
        DataBinding.FieldName = 'Host'
        Width = 68
      end
      object col_User: TcxGridDBColumn
        DataBinding.FieldName = 'User'
        Width = 62
      end
      object col_TableName: TcxGridDBColumn
        DataBinding.FieldName = 'TableName'
        Visible = False
        Width = 152
      end
      object cxGrid1DBTableView2Record_id: TcxGridDBColumn
        DataBinding.FieldName = 'Record_id'
        Visible = False
        Width = 72
      end
      object cxGrid1DBTableView2Operation: TcxGridDBColumn
        DataBinding.FieldName = 'Operation'
        Visible = False
        Width = 53
      end
      object col_ColumnName: TcxGridDBColumn
        DataBinding.FieldName = 'ColumnName'
        Visible = False
        Width = 92
      end
      object col_caption: TcxGridDBColumn
        DataBinding.FieldName = 'caption'
        Width = 114
      end
      object col_OldValue: TcxGridDBColumn
        DataBinding.FieldName = 'OldValue'
        Width = 188
      end
      object col_NewValue: TcxGridDBColumn
        DataBinding.FieldName = 'NewValue'
        Width = 147
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1DBTableView2
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 404
    Width = 810
    Height = 35
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    object Panel2: TPanel
      Left = 624
      Top = 0
      Width = 186
      Height = 35
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 0
      object Button1: TButton
        Left = 8
        Top = 5
        Width = 75
        Height = 25
        Caption = 'Ok'
        Default = True
        ModalResult = 1
        TabOrder = 0
      end
      object Button2: TButton
        Left = 96
        Top = 5
        Width = 75
        Height = 25
        Cancel = True
        Caption = 'Cancel'
        ModalResult = 2
        TabOrder = 1
      end
    end
  end
  object DataSource1: TDataSource
    DataSet = ADOStoredProc1
    Left = 112
    Top = 256
  end
  object ADOStoredProc1: TADOStoredProc
    Connection = ADOConnection1
    CursorType = ctStatic
    LockType = ltReadOnly
    ProcedureName = 'sp_Audit_select;1'
    Parameters = <>
    Left = 112
    Top = 304
  end
  object ADOConnection1: TADOConnection
    ConnectionString = 
      'Provider=MSDASQL.1;Persist Security Info=False;User ID=sa;Data S' +
      'ource=onega_1'
    LoginPrompt = False
    Provider = 'MSDASQL.1'
    Left = 32
    Top = 256
  end
  object FormPlacement1: TFormPlacement
    IniFileName = 'Software\Onega\'
    Left = 264
    Top = 264
  end
end
