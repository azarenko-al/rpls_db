unit d_ShowQry;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, rxPlacemnt, ActnList, ExtCtrls, cxPropertiesStore,

  cxGridLevel, cxClasses, cxControls, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid, DB,
  TB2Item, TB2Dock, TB2Toolbar,

  d_Wizard,
  u_func

  ;

type
  Tdlg_ShowQry = class(Tdlg_Wizard)
    ds: TDataSource;
    grid: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    TBDock1: TTBDock;
    tb_Main: TTBToolbar;
    TBItem2: TTBItem;
    procedure FormCreate(Sender: TObject);
    procedure TBItem2Click(Sender: TObject);
  public
    class function CreateSingleForm (aCaption: string; aQuery: TDataSet;
                   aComments: string=''; aShowButtonsPanel: boolean=false;
                   aOkBtnCaption: string='Ok'; aCancelBtnCaption: string='�������';
                   aReadOnly: boolean=true): boolean;
  end;


//==============================================================================
implementation {$R *.DFM}
//==============================================================================



//------------------------------------------------------------------------------
class function Tdlg_ShowQry.CreateSingleForm;
//------------------------------------------------------------------------------
begin
  with Tdlg_ShowQry.Create(Application) do
  try
    Caption:= aCaption;

    CursorHourGlass;
    ds.DataSet:= aQuery;
    grid.ClearItems;
    grid.DataController.CreateAllItems;
    CursorDefault;

    SetActionName(aComments);
    if aComments='' then pn_Top_.Visible:=false;

    pn_Buttons.Visible:= aShowButtonsPanel;

    btn_Ok__.Caption:= aOkBtnCaption;
    btn_Cancel__.Caption:= aCancelBtnCaption;

    Result:= (ShowModal=mrOk);
  finally
    Free;
  end;
end;

//------------------------------------------------------------------------------
procedure Tdlg_ShowQry.FormCreate(Sender: TObject);
//------------------------------------------------------------------------------
begin
  inherited;

  cxGrid1.Align:=alClient;
end;

//------------------------------------------------------------------------------
procedure Tdlg_ShowQry.TBItem2Click(Sender: TObject);
//------------------------------------------------------------------------------
begin
  inherited;

  grid.CopyToClipboard(true);
end;

end.
