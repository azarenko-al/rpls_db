inherited dlg_ShowQry: Tdlg_ShowQry
  Left = 421
  Top = 308
  Height = 393
  Caption = 'dlg_ShowQry'
  FormStyle = fsStayOnTop
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited pn_Buttons: TPanel
    Top = 331
    Visible = False
    inherited pn_but_aligned: TPanel
      inherited btn_Ok__: TButton
        Caption = #1054#1050
        ParentBiDiMode = False
      end
    end
  end
  object cxGrid1: TcxGrid [2]
    Left = 0
    Top = 85
    Width = 497
    Height = 200
    Align = alTop
    TabOrder = 2
    object grid: TcxGridDBTableView
      DataController.DataSource = ds
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      NavigatorButtons.ConfirmDelete = False
      OptionsBehavior.CellHints = True
      OptionsCustomize.ColumnsQuickCustomization = True
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsView.ColumnAutoWidth = True
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = grid
    end
  end
  object TBDock1: TTBDock [3]
    Left = 0
    Top = 60
    Width = 497
    Height = 25
    BoundLines = [blTop, blBottom, blLeft]
    object tb_Main: TTBToolbar
      Left = 0
      Top = 0
      AutoResize = False
      Caption = #1054#1087#1077#1088#1072#1094#1080#1080
      DockMode = dmCannotFloat
      DockPos = 0
      TabOrder = 0
      object TBItem2: TTBItem
        Caption = #1050#1086#1087#1080#1088#1086#1074#1072#1090#1100' '#1074' '#1073#1091#1092#1077#1088' '#1086#1073#1084#1077#1085#1072
        OnClick = TBItem2Click
      end
    end
  end
  inherited FormStorage1: TFormStorage
    Left = 380
  end
  inherited cxPropertiesStore: TcxPropertiesStore
    Left = 319
  end
  object ds: TDataSource
    Left = 165
    Top = 10
  end
end
