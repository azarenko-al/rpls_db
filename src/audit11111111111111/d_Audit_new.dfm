object dlg_Audit_new: Tdlg_Audit_new
  Left = 311
  Top = 327
  Width = 772
  Height = 409
  Caption = 'dlg_Audit_new'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poDefault
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 347
    Width = 764
    Height = 35
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 0
    object Panel2: TPanel
      Left = 578
      Top = 0
      Width = 186
      Height = 35
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 0
      object Button1: TButton
        Left = 8
        Top = 5
        Width = 75
        Height = 25
        Caption = 'Ok'
        Default = True
        ModalResult = 1
        TabOrder = 0
      end
      object Button2: TButton
        Left = 96
        Top = 5
        Width = 75
        Height = 25
        Cancel = True
        Caption = 'Cancel'
        ModalResult = 2
        TabOrder = 1
      end
    end
    object Button3: TButton
      Left = 8
      Top = 5
      Width = 75
      Height = 25
      Caption = 'Button3'
      TabOrder = 1
      Visible = False
      OnClick = Button3Click
    end
  end
  object PageControl1: TPageControl
    Left = 0
    Top = 0
    Width = 449
    Height = 347
    ActivePage = TabSheet_Inspector
    Align = alLeft
    TabOrder = 1
    object TabSheet_Grid: TTabSheet
      Caption = 'TabSheet_Grid'
      object cxGrid1: TcxGrid
        Left = 0
        Top = 0
        Width = 353
        Height = 312
        Align = alLeft
        TabOrder = 0
        LookAndFeel.Kind = lfFlat
        object cxGrid1DBTableView2: TcxGridDBTableView
          NavigatorButtons.ConfirmDelete = False
          DataController.DataSource = ds_Data
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          OptionsCustomize.ColumnsQuickCustomization = True
          OptionsData.Deleting = False
          OptionsData.Editing = False
          OptionsData.Inserting = False
          OptionsSelection.CellSelect = False
          object cxGrid1DBTableView2record_id: TcxGridDBColumn
            DataBinding.FieldName = 'record_id'
            Width = 95
          end
          object cxGrid1DBTableView2record_action: TcxGridDBColumn
            DataBinding.FieldName = 'record_action'
            Width = 85
          end
          object cxGrid1DBTableView2record_modify_date: TcxGridDBColumn
            DataBinding.FieldName = 'record_modify_date'
            Width = 114
          end
          object cxGrid1DBTableView2record_modify_user: TcxGridDBColumn
            DataBinding.FieldName = 'record_modify_user'
            Width = 115
          end
          object cxGrid1DBTableView2id: TcxGridDBColumn
            DataBinding.FieldName = 'id'
          end
          object cxGrid1DBTableView2folder_id: TcxGridDBColumn
            DataBinding.FieldName = 'folder_id'
          end
          object cxGrid1DBTableView2name: TcxGridDBColumn
            DataBinding.FieldName = 'name'
            Width = 87
          end
          object cxGrid1DBTableView2gain: TcxGridDBColumn
            DataBinding.FieldName = 'gain'
          end
          object cxGrid1DBTableView2gain_type: TcxGridDBColumn
            DataBinding.FieldName = 'gain_type'
          end
          object cxGrid1DBTableView2tilt_type: TcxGridDBColumn
            DataBinding.FieldName = 'tilt_type'
          end
          object cxGrid1DBTableView2freq: TcxGridDBColumn
            DataBinding.FieldName = 'freq'
          end
          object cxGrid1DBTableView2diameter: TcxGridDBColumn
            DataBinding.FieldName = 'diameter'
          end
          object cxGrid1DBTableView2omni: TcxGridDBColumn
            DataBinding.FieldName = 'omni'
          end
          object cxGrid1DBTableView2vert_width: TcxGridDBColumn
            DataBinding.FieldName = 'vert_width'
          end
          object cxGrid1DBTableView2horz_width: TcxGridDBColumn
            DataBinding.FieldName = 'horz_width'
          end
          object cxGrid1DBTableView2fronttobackratio: TcxGridDBColumn
            DataBinding.FieldName = 'fronttobackratio'
          end
          object cxGrid1DBTableView2polarization_ratio: TcxGridDBColumn
            DataBinding.FieldName = 'polarization_ratio'
          end
          object cxGrid1DBTableView2polarization_str: TcxGridDBColumn
            DataBinding.FieldName = 'polarization_str'
            Width = 88
          end
          object cxGrid1DBTableView2weight: TcxGridDBColumn
            DataBinding.FieldName = 'weight'
          end
          object cxGrid1DBTableView2band: TcxGridDBColumn
            DataBinding.FieldName = 'band'
            Width = 60
          end
          object cxGrid1DBTableView2vert_mask: TcxGridDBColumn
            DataBinding.FieldName = 'vert_mask'
          end
          object cxGrid1DBTableView2horz_mask: TcxGridDBColumn
            DataBinding.FieldName = 'horz_mask'
          end
          object cxGrid1DBTableView2electrical_tilt: TcxGridDBColumn
            DataBinding.FieldName = 'electrical_tilt'
          end
          object cxGrid1DBTableView2comment: TcxGridDBColumn
            DataBinding.FieldName = 'comment'
            Width = 66
          end
          object cxGrid1DBTableView2file_content: TcxGridDBColumn
            DataBinding.FieldName = 'file_content'
          end
        end
        object cxGrid1Level1: TcxGridLevel
          GridView = cxGrid1DBTableView2
        end
      end
    end
    object TabSheet_Inspector: TTabSheet
      Caption = 'TabSheet_Inspector'
      ImageIndex = 1
      object cxDBVerticalGrid1: TcxDBVerticalGrid
        Left = 0
        Top = 0
        Width = 353
        Height = 319
        Align = alLeft
        LayoutStyle = lsMultiRecordView
        LookAndFeel.Kind = lfFlat
        OptionsView.RowHeaderWidth = 146
        OptionsData.Editing = False
        OptionsData.Deleting = False
        OptionsData.Inserting = False
        Styles.OnGetContentStyle = cxDBVerticalGrid1StylesGetContentStyle
        Styles.Header = cxStyle2
        TabOrder = 0
        DataController.DataSource = ds_Data
        Version = 1
        object cxDBVerticalGrid1record_id: TcxDBEditorRow
          Properties.DataBinding.FieldName = 'record_id'
          ID = 0
          ParentID = -1
          Index = 0
          Version = 1
        end
        object cxDBVerticalGrid1record_action: TcxDBEditorRow
          Properties.DataBinding.FieldName = 'record_action'
          ID = 1
          ParentID = -1
          Index = 1
          Version = 1
        end
        object cxDBVerticalGrid1record_modify_date: TcxDBEditorRow
          Properties.DataBinding.FieldName = 'record_modify_date'
          ID = 2
          ParentID = -1
          Index = 2
          Version = 1
        end
        object cxDBVerticalGrid1record_modify_user: TcxDBEditorRow
          Properties.DataBinding.FieldName = 'record_modify_user'
          ID = 3
          ParentID = -1
          Index = 3
          Version = 1
        end
        object cxDBVerticalGrid1id: TcxDBEditorRow
          Properties.DataBinding.FieldName = 'id'
          ID = 4
          ParentID = -1
          Index = 4
          Version = 1
        end
        object cxDBVerticalGrid1folder_id: TcxDBEditorRow
          Properties.DataBinding.FieldName = 'folder_id'
          ID = 5
          ParentID = -1
          Index = 5
          Version = 1
        end
        object cxDBVerticalGrid1name: TcxDBEditorRow
          Properties.DataBinding.FieldName = 'name'
          ID = 6
          ParentID = -1
          Index = 6
          Version = 1
        end
        object cxDBVerticalGrid1gain: TcxDBEditorRow
          Properties.DataBinding.FieldName = 'gain'
          ID = 7
          ParentID = -1
          Index = 7
          Version = 1
        end
        object cxDBVerticalGrid1gain_type: TcxDBEditorRow
          Properties.DataBinding.FieldName = 'gain_type'
          ID = 8
          ParentID = -1
          Index = 8
          Version = 1
        end
        object cxDBVerticalGrid1tilt_type: TcxDBEditorRow
          Properties.DataBinding.FieldName = 'tilt_type'
          ID = 9
          ParentID = -1
          Index = 9
          Version = 1
        end
        object cxDBVerticalGrid1freq: TcxDBEditorRow
          Properties.DataBinding.FieldName = 'freq'
          ID = 10
          ParentID = -1
          Index = 10
          Version = 1
        end
        object cxDBVerticalGrid1diameter: TcxDBEditorRow
          Properties.DataBinding.FieldName = 'diameter'
          ID = 11
          ParentID = -1
          Index = 11
          Version = 1
        end
        object cxDBVerticalGrid1omni: TcxDBEditorRow
          Properties.DataBinding.FieldName = 'omni'
          ID = 12
          ParentID = -1
          Index = 12
          Version = 1
        end
        object cxDBVerticalGrid1vert_width: TcxDBEditorRow
          Properties.DataBinding.FieldName = 'vert_width'
          ID = 13
          ParentID = -1
          Index = 13
          Version = 1
        end
        object cxDBVerticalGrid1horz_width: TcxDBEditorRow
          Properties.DataBinding.FieldName = 'horz_width'
          ID = 14
          ParentID = -1
          Index = 14
          Version = 1
        end
        object cxDBVerticalGrid1fronttobackratio: TcxDBEditorRow
          Properties.DataBinding.FieldName = 'fronttobackratio'
          ID = 15
          ParentID = -1
          Index = 15
          Version = 1
        end
        object cxDBVerticalGrid1polarization_ratio: TcxDBEditorRow
          Properties.DataBinding.FieldName = 'polarization_ratio'
          ID = 16
          ParentID = -1
          Index = 16
          Version = 1
        end
        object cxDBVerticalGrid1polarization_str: TcxDBEditorRow
          Properties.DataBinding.FieldName = 'polarization_str'
          ID = 17
          ParentID = -1
          Index = 17
          Version = 1
        end
        object cxDBVerticalGrid1weight: TcxDBEditorRow
          Properties.DataBinding.FieldName = 'weight'
          ID = 18
          ParentID = -1
          Index = 18
          Version = 1
        end
        object cxDBVerticalGrid1band: TcxDBEditorRow
          Properties.DataBinding.FieldName = 'band'
          ID = 19
          ParentID = -1
          Index = 19
          Version = 1
        end
        object cxDBVerticalGrid1vert_mask: TcxDBEditorRow
          Properties.DataBinding.FieldName = 'vert_mask'
          ID = 20
          ParentID = -1
          Index = 20
          Version = 1
        end
        object cxDBVerticalGrid1horz_mask: TcxDBEditorRow
          Properties.DataBinding.FieldName = 'horz_mask'
          ID = 21
          ParentID = -1
          Index = 21
          Version = 1
        end
        object cxDBVerticalGrid1electrical_tilt: TcxDBEditorRow
          Properties.DataBinding.FieldName = 'electrical_tilt'
          ID = 22
          ParentID = -1
          Index = 22
          Version = 1
        end
        object cxDBVerticalGrid1comment: TcxDBEditorRow
          Properties.DataBinding.FieldName = 'comment'
          ID = 23
          ParentID = -1
          Index = 23
          Version = 1
        end
        object cxDBVerticalGrid1file_content: TcxDBEditorRow
          Properties.DataBinding.FieldName = 'file_content'
          ID = 24
          ParentID = -1
          Index = 24
          Version = 1
        end
        object cxDBVerticalGrid1CategoryRow1: TcxCategoryRow
          ID = 25
          ParentID = -1
          Index = 25
          Version = 1
        end
        object cxDBVerticalGrid1CategoryRow2: TcxCategoryRow
          ID = 26
          ParentID = -1
          Index = 26
          Version = 1
        end
      end
    end
  end
  object ds_Data: TDataSource
    DataSet = sp_Data
    Left = 568
    Top = 226
  end
  object sp_Data: TADOStoredProc
    Connection = ADOConnection1
    CursorType = ctStatic
    LockType = ltReadOnly
    ProcedureName = 'sp_Audit_select_new;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@TABLENAME'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 128
        Value = 'AntennaType'
      end
      item
        Name = '@ID'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 22393
      end>
    Left = 568
    Top = 168
  end
  object ADOConnection1: TADOConnection
    ConnectionString = 
      'Provider=MSDASQL.1;Persist Security Info=False;User ID=sa;Data S' +
      'ource=SQL_ONEGA_1'
    LoginPrompt = False
    Provider = 'MSDASQL.1'
    Left = 568
    Top = 88
  end
  object FormPlacement1: TFormPlacement
    IniFileName = 'Software\Onega\'
    Left = 576
    Top = 16
  end
  object cxStyleRepository1: TcxStyleRepository
    Left = 672
    Top = 16
    PixelsPerInch = 96
    object cxStyle1: TcxStyle
      AssignedValues = [svColor]
      Color = clLime
    end
    object cxStyle2: TcxStyle
      AssignedValues = [svColor]
      Color = clActiveBorder
    end
  end
  object ds_Object_fields: TDataSource
    DataSet = sp_Object_fields
    Left = 656
    Top = 224
  end
  object sp_Object_fields: TADOStoredProc
    Connection = ADOConnection1
    CursorType = ctStatic
    LockType = ltReadOnly
    ProcedureName = 'sp_Audit_select_new;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@TABLENAME'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 128
        Value = 'AntennaType'
      end
      item
        Name = '@ID'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 22393
      end>
    Left = 656
    Top = 168
  end
  object q_Columns: TADOQuery
    Connection = ADOConnection1
    LockType = ltReadOnly
    Parameters = <>
    Left = 480
    Top = 168
  end
end
