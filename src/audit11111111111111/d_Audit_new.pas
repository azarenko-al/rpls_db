unit d_Audit_new;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxControls, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxClasses, cxGridLevel, cxGrid, DB,
  ADODB, Grids, DBGrids,
  rxPlacemnt, StdCtrls, ExtCtrls, cxInplaceContainer, cxVGrid, cxDBVGrid,


  u_Inspector_common,

  u_ViewEngine_classes,


  dm_Onega_DB_data,

  u_Storage,

  u_const_str,
  u_const_db,

  u_db,

  U_LOG,

  cxGraphics, cxStyles, ComCtrls ;

type
  Tdlg_Audit_new = class(TForm)
    ds_Data: TDataSource;
    sp_Data: TADOStoredProc;
    ADOConnection1: TADOConnection;
    FormPlacement1: TFormPlacement;
    Panel1: TPanel;
    Panel2: TPanel;
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    cxStyleRepository1: TcxStyleRepository;
    cxStyle1: TcxStyle;
    cxStyle2: TcxStyle;
    PageControl1: TPageControl;
    TabSheet_Grid: TTabSheet;
    TabSheet_Inspector: TTabSheet;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView2: TcxGridDBTableView;
    cxGrid1DBTableView2record_id: TcxGridDBColumn;
    cxGrid1DBTableView2record_action: TcxGridDBColumn;
    cxGrid1DBTableView2record_modify_date: TcxGridDBColumn;
    cxGrid1DBTableView2record_modify_user: TcxGridDBColumn;
    cxGrid1DBTableView2id: TcxGridDBColumn;
    cxGrid1DBTableView2folder_id: TcxGridDBColumn;
    cxGrid1DBTableView2name: TcxGridDBColumn;
    cxGrid1DBTableView2gain: TcxGridDBColumn;
    cxGrid1DBTableView2gain_type: TcxGridDBColumn;
    cxGrid1DBTableView2tilt_type: TcxGridDBColumn;
    cxGrid1DBTableView2freq: TcxGridDBColumn;
    cxGrid1DBTableView2diameter: TcxGridDBColumn;
    cxGrid1DBTableView2omni: TcxGridDBColumn;
    cxGrid1DBTableView2vert_width: TcxGridDBColumn;
    cxGrid1DBTableView2horz_width: TcxGridDBColumn;
    cxGrid1DBTableView2fronttobackratio: TcxGridDBColumn;
    cxGrid1DBTableView2polarization_ratio: TcxGridDBColumn;
    cxGrid1DBTableView2polarization_str: TcxGridDBColumn;
    cxGrid1DBTableView2weight: TcxGridDBColumn;
    cxGrid1DBTableView2band: TcxGridDBColumn;
    cxGrid1DBTableView2vert_mask: TcxGridDBColumn;
    cxGrid1DBTableView2horz_mask: TcxGridDBColumn;
    cxGrid1DBTableView2electrical_tilt: TcxGridDBColumn;
    cxGrid1DBTableView2comment: TcxGridDBColumn;
    cxGrid1DBTableView2file_content: TcxGridDBColumn;
    cxGrid1Level1: TcxGridLevel;
    cxDBVerticalGrid1: TcxDBVerticalGrid;
    cxDBVerticalGrid1record_id: TcxDBEditorRow;
    cxDBVerticalGrid1record_action: TcxDBEditorRow;
    cxDBVerticalGrid1record_modify_date: TcxDBEditorRow;
    cxDBVerticalGrid1record_modify_user: TcxDBEditorRow;
    cxDBVerticalGrid1id: TcxDBEditorRow;
    cxDBVerticalGrid1folder_id: TcxDBEditorRow;
    cxDBVerticalGrid1name: TcxDBEditorRow;
    cxDBVerticalGrid1gain: TcxDBEditorRow;
    cxDBVerticalGrid1gain_type: TcxDBEditorRow;
    cxDBVerticalGrid1tilt_type: TcxDBEditorRow;
    cxDBVerticalGrid1freq: TcxDBEditorRow;
    cxDBVerticalGrid1diameter: TcxDBEditorRow;
    cxDBVerticalGrid1omni: TcxDBEditorRow;
    cxDBVerticalGrid1vert_width: TcxDBEditorRow;
    cxDBVerticalGrid1horz_width: TcxDBEditorRow;
    cxDBVerticalGrid1fronttobackratio: TcxDBEditorRow;
    cxDBVerticalGrid1polarization_ratio: TcxDBEditorRow;
    cxDBVerticalGrid1polarization_str: TcxDBEditorRow;
    cxDBVerticalGrid1weight: TcxDBEditorRow;
    cxDBVerticalGrid1band: TcxDBEditorRow;
    cxDBVerticalGrid1vert_mask: TcxDBEditorRow;
    cxDBVerticalGrid1horz_mask: TcxDBEditorRow;
    cxDBVerticalGrid1electrical_tilt: TcxDBEditorRow;
    cxDBVerticalGrid1comment: TcxDBEditorRow;
    cxDBVerticalGrid1file_content: TcxDBEditorRow;
    cxDBVerticalGrid1CategoryRow1: TcxCategoryRow;
    cxDBVerticalGrid1CategoryRow2: TcxCategoryRow;
    ds_Object_fields: TDataSource;
    sp_Object_fields: TADOStoredProc;
    q_Columns: TADOQuery;
    procedure Button3Click(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure cxDBVerticalGrid1StylesGetContentStyle(Sender: TObject;
      AEditProp: TcxCustomEditorRowProperties; AFocused: Boolean;
      ARecordIndex: Integer; var AStyle: TcxStyle);
  private
    FRegPath : string;

    ObjectList: TViewEngineObjectList;


    procedure HideColumns;
    procedure PrepareObject(aObjectName: string);
    procedure View(aObjName: string; aID: Integer);

  public
    class procedure ExecDlg(aTableName: string; aID: Integer = 0);

  end;


implementation

{$R *.dfm}

const
  SP_AUDIT_SELECT = 'SP_AUDIT_SELECT';


const
  REGISTRY_COMMON_FORMS  = 'Software\Onega\Common\Forms\1\';


// ---------------------------------------------------------------
class procedure Tdlg_Audit_new.ExecDlg(aTableName: string; aID: Integer = 0);
// ---------------------------------------------------------------
begin

  ShowMessage('������� ��������� ���������.');
  Exit;



   with Tdlg_Audit_new.Create(Application) do
   begin
     View(aTableName, aID);

     ShowModal;

     Free;
   end;

end;

// ---------------------------------------------------------------
procedure Tdlg_Audit_new.FormCreate(Sender: TObject);
// ---------------------------------------------------------------
begin
  if ADOConnection1.Connected  then
    ShowMessage('ADOConnection1.Connected');

  PageControl1.Align:=alClient;
  cxDBVerticalGrid1.Align:=alClient;

  TabSheet_Grid.Caption :='�������';
  TabSheet_Inspector.Caption :='���������';


  Caption :=STR_Audit;

  FRegPath :=REGISTRY_COMMON_FORMS + ClassName + '\';

  cxGrid1.Align:=alClient;

//  cxGrid1DBTableView2.RestoreFromRegistry(FRegPath + cxGrid1DBTableView2.name);

  cxGrid1DBTableView2.ClearItems;




(*
  col_Date.Caption        :='����';
  col_Host.Caption        :='���������';
  col_caption.Caption     :='��������';
  col_ColumnName.Caption  :='����';
  col_NewValue.Caption    :='����� ��������';
  col_OldValue.Caption    :='������ ��������';
  col_TableName.Caption   :='�������';
  col_User.Caption        :='������������';

*)

  ObjectList := TViewEngineObjectList.Create();
end;


procedure Tdlg_Audit_new.FormDestroy(Sender: TObject);
begin
  FreeAndNil(ObjectList);

 // cxGrid1DBTableView2.StoreToRegistry(FRegPath + cxGrid1DBTableView2.name);

end;

// ---------------------------------------------------------------
procedure Tdlg_Audit_new.View(aObjName: string; aID: Integer);
// ---------------------------------------------------------------
var
  I: Integer;
  r: Integer;

  ft: TFieldType;

  oField: TField;
begin
 // r:=dmOnega_DB_data.OpenStoredProc(sp_Data,  'SP_AUDIT_SELECT_NEW',
  r:=dmOnega_DB_data.OpenStoredProc(sp_Data,  'SP_AUDIT_SELECT_record',
        [db_Par(FLD_OBJNAME, aObjName),
         db_Par(FLD_ID, aID)
         ]);

//  db_SetFieldCaptions(sp_Data,

//  );


(*
  for I := 0 to ADOStoredProc1.Fields.Count - 1 do
  begin
    oField:=ADOStoredProc1.Fields[i];

    ft := oField.DataType;

    case ft of
       ftMemo, ftBlob:  ShowMessage('ftMemo, ftBlob' + oField.FieldName) ;
    end;
  end;*)


  cxGrid1DBTableView2.ClearItems;
  cxGrid1DBTableView2.DataController.CreateAllItems;
  cxGrid1DBTableView2.ApplyBestFit();

  cxdbVerticalGrid1.ClearRows;
  cxdbVerticalGrid1.DataController.CreateAllItems;

  PrepareObject(aObjName);

end;



procedure Tdlg_Audit_new.HideColumns;
begin

end;      


procedure Tdlg_Audit_new.Button3Click(Sender: TObject);
var
  I: Integer;
  r: Integer;

  ft: TFieldType;

  oField: TField;
begin

  for I := 0 to sp_Data.Fields.Count - 1 do
  begin
    oField:=sp_Data.Fields[i];

    ft := oField.DataType;

    case ft of
       ftMemo, ftBlob:  ShowMessage('ftMemo, ftBlob' + oField.FieldName) ;
    end;
  end;

end;

// ---------------------------------------------------------------
procedure Tdlg_Audit_new.cxDBVerticalGrid1StylesGetContentStyle(
  Sender: TObject; AEditProp: TcxCustomEditorRowProperties;
  AFocused: Boolean; ARecordIndex: Integer; var AStyle: TcxStyle);
// ---------------------------------------------------------------
var
  v_new: Variant;
  v_old: Variant;
begin
  if ARecordIndex>0 then
    if AEditProp.ItemIndex>3 then
  begin
    v_old:=cxDBVerticalGrid1.DataController.Values[ARecordIndex-1, AEditProp.ItemIndex];
    v_new:=cxDBVerticalGrid1.DataController.Values[ARecordIndex, AEditProp.ItemIndex];

    if v_old<>v_new then
      AStyle :=cxStyle1;

  end;



//  if  then

end;


//-------------------------------------------------------------------
procedure Tdlg_Audit_new.PrepareObject(aObjectName: string);
//-------------------------------------------------------------------
var
  oRowInfo: TViewEngFieldInfo;
  oRow: TcxCustomRow;
  I: Integer;

var
  oObject: TViewEngineObject;

const
  SQL_OBJECT_FIELDS =
     'SELECT * FROM ' + view__object_fields_enabled_
      +' WHERE objname=:objname';

//     ' WHERE (enabled <> 0) ORDER BY parent_id, index_ ';

begin

//////  oObject:=ObjectList.FindByObjectName(aObjectName);

  db_OpenQuery (q_Columns,  SQL_OBJECT_FIELDS, [db_Par(FLD_objname, aObjectName)]);

  oObject:=ObjectList.AddItem;
  oObject.ObjectName := aObjectName;

 // oObject.ID := q_Columns.FieldByName(FLD_Object_ID).AsInteger;

  // -------------------------

//  ADOQuery1.TableName  := TBL_OBJECT_FIELDS;
  ObjectList.LoadFromDB_ObjectFields(q_Columns);

//  ADOQuery1.Close;

//
//
//    for I := 0 to ObjectList.Count - 1 do
//    begin
//
//    (*  oRowInfo:=ObjectList[i];
//      oRow :=AddRow (aParentRow, oRowInfo);
//
//      Assert(Assigned(oRow));
//
//      oRow.Tag := Integer(oRowInfo);
//      DoAddListCX(oRow, oRowInfo.SubFields);
//*)
//    end


end;



end.


   {

//------------------------------------------------------------------------------
class function Tdlg_ShowQry.CreateSingleForm;
//------------------------------------------------------------------------------
begin
  with Tdlg_ShowQry.Create(Application) do
  try
    Caption:= aCaption;

    CursorHourGlass;
    ds.DataSet:= aQuery;
    grid.ClearItems;
    grid.DataController.CreateAllItems;
    CursorDefault;

    SetActionName(aComments);
    if aComments='' then pn_Top_.Visible:=false;

    pn_Buttons.Visible:= aShowButtonsPanel;

    btn_Ok__.Caption:= aOkBtnCaption;
    btn_Cancel__.Caption:= aCancelBtnCaption;

    Result:= (ShowModal=mrOk);
  finally
    Free;
  end;
end;

//------------------------------------------------------------------------------
procedure Tdlg_ShowQry.FormCreate(Sender: TObject);
//------------------------------------------------------------------------------
begin
  inherited;

  cxGrid1.Align:=alClient;
end;

//------------------------------------------------------------------------------
procedure Tdlg_ShowQry.TBItem2Click(Sender: TObject);
//------------------------------------------------------------------------------
begin
  inherited;

  grid.CopyToClipboard(true);
end;

end.



cxGridDBBandedTableView_Link_LinkEnds.ApplyBestFit();



//--------------------------------------------------------------------
procedure Tframe_DBInspector.LoadFieldsFromDB ();
//--------------------------------------------------------------------

    //--------------------------------------------------------------------
    procedure DoAddListCX(aParentRow: TcxCustomRow; aList: TViewEngFieldInfoList);
    //--------------------------------------------------------------------
    var oRowInfo: TViewEngFieldInfo;
        oRow: TcxCustomRow;
        I: integer;
    begin
      //������� move to !!!!!!!!!!!!!

      if not Assigned(aParentRow) then
        for I := 0 to aList.Count - 1 do
        begin
          oRowInfo:=aList[i];
          oRow :=AddRow (aParentRow, oRowInfo);

          Assert(Assigned(oRow));

          oRow.Tag := Integer(oRowInfo);
          DoAddListCX(oRow, oRowInfo.SubFields);
        end

      else
        for I := aList.Count - 1 downto 0 do
        begin
          oRowInfo:=aList[i];
          oRow :=AddRow (aParentRow, oRowInfo);

          Assert(Assigned(oRow));

          oRow.Tag := Integer(oRowInfo);
          DoAddListCX(oRow, oRowInfo.SubFields);
        end;
    end;

    //--------------------------------------------------------------------
    procedure DoLoadFields();
    //--------------------------------------------------------------------
    var
      I: Integer;
      oRowInfo: TViewEngFieldInfo;
    begin
      FFieldList.Clear;

      for I := 0 to FViewObject.ViewFields.Count - 1 do
        if not FViewObject.ViewFields[i].IsHidden then
      begin
        oRowInfo:=dmViewEngine.CreateFieldInfo (FViewObject.ViewFields[i]);
        FFieldList.Add(oRowInfo);

      end;

      FFieldList.ReOrder;
    end;

begin
  Assert(Assigned(FViewObject), 'Value not assigned');

  cxDBVerticalGrid1.BeginUpdate;

  ClearRows();

  DoLoadFields();

  DoAddListCX(nil, FFieldList);

  cxDBVerticalGrid1.EndUpdate;

end;

