unit d_Audit;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxControls, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxClasses, cxGridLevel, cxGrid, DB,
  ADODB, Grids, DBGrids,

  dm_Onega_DB_data,

  u_const_str,  

  u_db,


  U_LOG,


   rxPlacemnt, StdCtrls, ExtCtrls
  ;

type
  Tdlg_Audit1 = class(TForm)
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView2: TcxGridDBTableView;
    DataSource1: TDataSource;
    ADOStoredProc1: TADOStoredProc;
    ADOConnection1: TADOConnection;
    col_caption: TcxGridDBColumn;
    col_ColumnName: TcxGridDBColumn;
    col_Date: TcxGridDBColumn;
    col_Host: TcxGridDBColumn;
    col_NewValue: TcxGridDBColumn;
    col_OldValue: TcxGridDBColumn;
    col_TableName: TcxGridDBColumn;
    col_User: TcxGridDBColumn;
    cxGrid1DBTableView2Operation: TcxGridDBColumn;
    cxGrid1DBTableView2Record_id: TcxGridDBColumn;
    FormPlacement1: TFormPlacement;
    Panel1: TPanel;
    Panel2: TPanel;
    Button1: TButton;
    Button2: TButton;
    procedure FormDestroy(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    FRegPath : string;
    { Private declarations }
  public
    class procedure ExecDlg(aTableName: string = ''; aID: Integer = 0);

  end;


implementation

{$R *.dfm}

const
  SP_AUDIT_SELECT = 'SP_AUDIT_SELECT';


const
  REGISTRY_COMMON_FORMS  = 'Software\Onega\Common\Forms\1\';


// ---------------------------------------------------------------
class procedure Tdlg_Audit1.ExecDlg(aTableName: string = ''; aID: Integer = 0);
// ---------------------------------------------------------------
begin
   with Tdlg_Audit1.Create(Application) do
   begin
     dmOnega_DB_data.OpenStoredProc(ADOStoredProc1,  SP_AUDIT_SELECT,
        [db_Par(FLD_TableName, aTableName),
         db_Par(FLD_ID, aID)
         ]);

     ShowModal;

     Free;
   end;

end;

// ---------------------------------------------------------------
procedure Tdlg_Audit1.FormCreate(Sender: TObject);
// ---------------------------------------------------------------
begin
  if ADOConnection1.Connected  then
    ShowMessage('ADOConnection1.Connected');



  Caption :=STR_Audit;

  FRegPath :=REGISTRY_COMMON_FORMS + ClassName + '\';

  cxGrid1.Align:=alClient;

  cxGrid1DBTableView2.RestoreFromRegistry(FRegPath + cxGrid1DBTableView2.name);

  col_Date.Caption        :='����';
  col_Host.Caption        :='���������';
  col_caption.Caption     :='��������';
  col_ColumnName.Caption  :='����';
  col_NewValue.Caption    :='����� ��������';
  col_OldValue.Caption    :='������ ��������';
  col_TableName.Caption   :='�������';
  col_User.Caption        :='������������';


end;


procedure Tdlg_Audit1.FormDestroy(Sender: TObject);
begin
  cxGrid1DBTableView2.StoreToRegistry(FRegPath + cxGrid1DBTableView2.name);

end;



end.
